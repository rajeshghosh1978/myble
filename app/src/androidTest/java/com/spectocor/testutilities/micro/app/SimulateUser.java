package com.spectocor.testutilities.micro.app;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.spectocor.testutilities.IntentUtility;

/**
 * Created by qwang on 3/1/2016.
 *
 * Simulate micro application user behaviour
 */
public class SimulateUser {

    private static String TAG = "SimulateUser";

    public static boolean ActivateSession(Context context,String SessionStateJSON){

        boolean rc = true;

        return rc;
    }

    public static boolean EndSession(Context context,String SessionID){

        boolean rc = true;

        // JOINING BOTH BROADCASTS AND SENDING AS ONE
        Intent intent = IntentUtility.createEndSessionIntent();

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        return rc;
    }



}
