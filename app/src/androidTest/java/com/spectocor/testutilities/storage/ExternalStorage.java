package com.spectocor.testutilities.storage;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
/**
 * Created by qwang on 3/8/2016.
 *
 * reference from http://stackoverflow.com/questions/5694933/find-an-external-sd-card-location
 *
 */
public class ExternalStorage {

    private static String TAG = "ExternalStorage";

    public static final String SD_CARD = "sdCard";
    public static final String EXTERNAL_SD_CARD = "externalSdCard";

    /**
     * @return True if the external storage is available. False otherwise.
     */
    public static boolean isAvailable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static String getSdCardPath() {
        return Environment.getExternalStorageDirectory().getPath() + "/";
    }

    /**
     * @return True if the external storage is writable. False otherwise.
     */
    public static boolean isWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;

    }

    public static boolean isWritable(final File file) {
        String state = Environment.getExternalStorageState(file);
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;

    }

    /**
     * @return A map of all storage locations available
     */
    public static Map<String, File> getAllStorageLocations() {
        Map<String, File> map = new HashMap<String, File>(10);

        List<String> mMounts = new ArrayList<String>(10);
        List<String> mVold = new ArrayList<String>(10);
        mMounts.add("/mnt/sdcard");
        mVold.add("/mnt/sdcard");

        try {
            File mountFile = new File("/proc/mounts");
            if(mountFile.exists()){
                Scanner scanner = new Scanner(mountFile);
                while (scanner.hasNext()) {
                    String line = scanner.nextLine();
                    if (line.startsWith("/dev/block/vold/")) {
                        String[] lineElements = line.split(" ");
                        String element = lineElements[1];

                        // don't add the default mount path
                        // it's already in the list.
                        if (!element.equals("/mnt/sdcard"))
                            mMounts.add(element);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            File voldFile = new File("/system/etc/vold.fstab");
            if(voldFile.exists()){
                Scanner scanner = new Scanner(voldFile);
                while (scanner.hasNext()) {
                    String line = scanner.nextLine();
                    if (line.startsWith("dev_mount")) {
                        String[] lineElements = line.split(" ");
                        String element = lineElements[2];

                        if (element.contains(":"))
                            element = element.substring(0, element.indexOf(":"));
                        if (!element.equals("/mnt/sdcard"))
                            mVold.add(element);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        for (int i = 0; i < mMounts.size(); i++) {
            String mount = mMounts.get(i);
            if (!mVold.contains(mount))
                mMounts.remove(i--);
        }
        mVold.clear();

        List<String> mountHash = new ArrayList<String>(10);

        for(String mount : mMounts){
            File root = new File(mount);
            if (root.exists() && root.isDirectory() && root.canWrite()) {
                File[] list = root.listFiles();
                String hash = "[";
                if(list!=null){
                    for(File f : list){
                        hash += f.getName().hashCode()+":"+f.length()+", ";
                    }
                }
                hash += "]";
                if(!mountHash.contains(hash)){
                    String key = SD_CARD + "_" + map.size();
                    if (map.size() == 0) {
                        key = SD_CARD;
                    } else if (map.size() == 1) {
                        key = EXTERNAL_SD_CARD;
                    }
                    mountHash.add(hash);
                    map.put(key, root);
                }
            }
        }

        mMounts.clear();

        if(map.isEmpty()){
            map.put(SD_CARD, Environment.getExternalStorageDirectory());
        }
        return map;
    }

    /**
     * reference code from Savio AMainApplication
     *
     */
    public static String determineDbPathProgrammatically(Context context) {

        // detect writeable directories
        String APP_STORAGE_PATH = "";
        String DB_PATH;

        File[] writeableDirs = context.getExternalFilesDirs(null);

        if(writeableDirs.length == 2) {

            Log.d(TAG,"SD CARD SLOT PRESENT");
            File sdDir = writeableDirs[1];
            if (sdDir == null) {
                APP_STORAGE_PATH = writeableDirs[0].getAbsolutePath(); // primary storage
                Log.d(TAG, "SD CARD NOT INSERTED");
            } else {
                APP_STORAGE_PATH = writeableDirs[1].getAbsolutePath(); // sd storage if not null
                Log.d(TAG, "SD CARD INSERTED. APP PATH: " + APP_STORAGE_PATH);
            }

//            log("Storage State: " + Environment.getExternalStorageState(new File(APP_STORAGE_PATH)));
        }
        else if(writeableDirs.length == 1) {
            APP_STORAGE_PATH = writeableDirs[0].getAbsolutePath(); // primary storage
            Log.d(TAG,"NO SD CARD SLOT");
        }
        else {
            APP_STORAGE_PATH = ""; // app internal private storage
        }


        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // create database folder
        if (APP_STORAGE_PATH.isEmpty()) {
            DB_PATH = "";
        }
        else
        {
            DB_PATH = APP_STORAGE_PATH + File.separator + "databases";

            File dbPathF = new File(DB_PATH);

            if(!dbPathF.exists())
            {
                Log.d(TAG,"Creating recursive folders for: " + dbPathF.getAbsolutePath());

                boolean pathCreated = dbPathF.mkdirs();

                if(pathCreated && new File(dbPathF.getAbsolutePath()).exists()) {
                    Log.d(TAG,"Created ... " + dbPathF.getAbsolutePath() + " ... ");
                }
                else {
                    Log.d(TAG,"Could not create databases/ folder inside " + APP_STORAGE_PATH);

                }
            }
            else
            {
                Log.d(TAG,dbPathF.getAbsolutePath() + " ... ALREADY EXISTS!!!");
            }
        }


        return DB_PATH;
    }

    /**
     * Raturns all available SD-Cards in the system (include emulated)
     *
     * Warning: Hack! Based on Android source code of version 4.3 (API 18)
     * Because there is no standart way to get it.
     * Test on future Android versions 5.1
     *
     * @return paths to all available SD-Cards in the system (include emulated)
     */
    public static String[] getStorageDirectories()
    {
        // Final set of paths
        final Set<String> rv = new HashSet<String>();
        // Primary physical SD-CARD (not emulated)
        final String rawExternalStorage = System.getenv("EXTERNAL_STORAGE");
        // All Secondary SD-CARDs (all exclude primary) separated by ":"
        final String rawSecondaryStoragesStr = System.getenv("SECONDARY_STORAGE");
        // Primary emulated SD-CARD
        final String rawEmulatedStorageTarget = System.getenv("EMULATED_STORAGE_TARGET");
        if(TextUtils.isEmpty(rawEmulatedStorageTarget))
        {
            // Device has physical external storage; use plain paths.
            if(TextUtils.isEmpty(rawExternalStorage))
            {
                // EXTERNAL_STORAGE undefined; falling back to default.
                rv.add("/storage/sdcard0");
            }
            else
            {
                rv.add(rawExternalStorage);
            }
        }
        else
        {
            // Device has emulated storage; external storage paths should have
            // userId burned into them.
            final String rawUserId;
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1)
            {
                rawUserId = "";
            }
            else
            {
                final String path = Environment.getExternalStorageDirectory().getAbsolutePath();
                final String[] folders =   path.split(File.separator);   //DIR_SEPORATOR.split(path);
                final String lastFolder = folders[folders.length - 1];
                boolean isDigit = false;
                try
                {
                    Integer.valueOf(lastFolder);
                    isDigit = true;
                }
                catch(NumberFormatException ignored)
                {
                }
                rawUserId = isDigit ? lastFolder : "";
            }
            // /storage/emulated/0[1,2,...]
            if(TextUtils.isEmpty(rawUserId))
            {
                rv.add(rawEmulatedStorageTarget);
            }
            else
            {
                rv.add(rawEmulatedStorageTarget + File.separator + rawUserId);
            }
        }
        // Add all secondary storage
        if(!TextUtils.isEmpty(rawSecondaryStoragesStr))
        {
            // All Secondary SD-CARDs splited into array
            final String[] rawSecondaryStorages = rawSecondaryStoragesStr.split(File.pathSeparator);
            Collections.addAll(rv, rawSecondaryStorages);
        }
        return rv.toArray(new String[rv.size()]);
    }

    public static void createFile(Activity activity, final File path,final String fileName){

        File newFile = new File(path.getAbsolutePath(), fileName);

        String mediaTagBuffer = "new file content";

        FileOutputStream fos;

        final int REQUEST_CODE = 0x11;
        String[] permissions = {"android.permission.WRITE_EXTERNAL_STORAGE"};
        ActivityCompat.requestPermissions(activity, permissions, REQUEST_CODE); // without sdk version check



        try {
            fos = new FileOutputStream(newFile);
            fos.write(mediaTagBuffer.getBytes());
            fos.flush();
            fos.close();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static boolean checkWriteExternalPermission(Context context)
    {
        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        Log.d(TAG, context.getPackageName());
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public static boolean checkReadExternalPermission(Context context)
    {
        String permission = "android.permission.READ_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        Log.d(TAG,context.getPackageName());
        return (res == PackageManager.PERMISSION_GRANTED);
    }
}
