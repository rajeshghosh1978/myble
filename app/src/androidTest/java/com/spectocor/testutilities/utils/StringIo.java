package com.spectocor.testutilities.utils;

public class StringIo {

	public static String doubleArrToString(double[] arr, String separator) {
		String str = "";
		for (double anArr : arr) {

			str += anArr + separator;

		}
		
		return str.trim();
	}
	
}
