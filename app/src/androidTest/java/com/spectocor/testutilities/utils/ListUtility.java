package com.spectocor.testutilities.utils;

import java.util.List;
import android.util.Log;

import org.junit.Test;

/**
 * Created by qwang on 2/5/2016.
 */
public class ListUtility {

    private static final String TAG = "ListUtility";

    public static boolean CompareList(List expectedList,List actualList){

        boolean rc = true;

        int expectedLiseSize = expectedList.size();
        int actualListSize = actualList.size();

        assert(expectedLiseSize > 0);
        assert(actualListSize > 0);

        if(expectedLiseSize != actualListSize){
            Log.d(TAG,"Expected List Size:"+expectedLiseSize+"  " +"Actual List Size:" + actualListSize);
        }

        int longer = (expectedLiseSize>actualListSize?expectedLiseSize:actualListSize);

        String msg ;
        String expected;
        String actual;
        for(int i = 0;i < longer; ++i){

            msg = "index:" + i + ":Expected Value:";
            if(i < expectedLiseSize){
                expected = expectedList.get(i).toString() + "    ";
            }
            else
            {
                expected = "          " + "    ";
            }

            msg += expected;
            msg += "Actual Value:";
            if(i < actualListSize){
                actual = actualList.get(i).toString() + "    ";
            }
            else
            {
                actual = "          " + "    ";
            }

            msg += actual;
            if(expected.compareTo(actual) ==0)
                Log.d(TAG,msg);
            else
                Log.e(TAG,msg);
        }

        return rc;
    }
}
