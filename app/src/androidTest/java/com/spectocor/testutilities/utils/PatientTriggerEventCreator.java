package com.spectocor.testutilities.utils;

import android.util.Log;

import com.spectocor.micor.mobileapp.patientreportedevents.EnumPatientActivityEventType;
import com.spectocor.micor.mobileapp.patientreportedevents.EnumPatientReportedEventType;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientEvent;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by qwang on 3/7/2016.
 *
 * create random patient trigger event data
 *
 * symptoms string format : symptom1,symptom2,symptom3;event1
 *
 */
public class PatientTriggerEventCreator {

    private static String TAG = "PatientTriggerEventCreator";


    private static String[] mActivityEventTypeList = {  "Resting","Walking","Exercising","Other"};// 0 - 3

    private static String[] mEventTypeList = {  "Other","ChestDiscomfort","Fatigue","Palpitation","Dizziness","ShortnessOfBreath","Fluttering","RacingHeart","SlurredSpeech","Headache","RecentlyFainted"}; // 0 - 10;

    /**
     * convert symptom string to json string
     *
     * reference com.spectocor.micor.mobileapp.sessionactivation.api.reportWithSymptoms
     *
     * @param symptoms
     * @return
     */
    public static JSONArray SymptomString2JSON(final String symptoms){


        JSONArray logsArray = new JSONArray();
        JSONObject log;

        String[] totalDiagnosticSplit = symptoms.split(";");
        String[] symptomsSplit = totalDiagnosticSplit[0].split(",");

        try{
            for (int i = 0; i < symptomsSplit.length; i++) {
                log = new JSONObject();
                int symptomValue = EnumPatientReportedEventType.valueOf(symptomsSplit[i].replace(" ", "")).getValue();
                log.put("eventType", symptomValue + "");
                if (totalDiagnosticSplit.length > 1) {
                    log.put("activityType", EnumPatientActivityEventType.valueOf(totalDiagnosticSplit[1].replace(" ", "")).getValue());
                } else
                    log.put("activityType", "4");
                log.put("eventTimestamp", System.currentTimeMillis());
                logsArray.put(log);
            }
        }catch (JSONException je){
            Log.e(TAG,"" + je.getMessage());
            Assert.fail("Fail to convert Symptom String:"+ symptoms + ": to Json array" );
            logsArray = null;
        }

        return logsArray;

    }

    public static PatientEvent createPatientEvent(final long sessionID ){

        PatientEvent patientEvent = new PatientEvent();

        patientEvent.setSessionId(sessionID);

        patientEvent.setInsertTimestamp(System.currentTimeMillis());

        patientEvent.setTransmittedTimestamp(-1);

        JSONArray logsArray = SymptomString2JSON(createRandomSymptom());

        patientEvent.setData(logsArray.toString());

        return patientEvent;
    }

    public static String createRandomSymptom(){

        String symptoms = "";

        symptoms = createRandomEventTypes() + ";" + createRandomActivityType();

        return symptoms;
    }

    public static String createRandomEventTypes(){

        String eventTypes = "";

        int numberTypes = NumberUtils.mockRandomNumber(10);

        int i = 0;

        while(i < numberTypes){

            if(i + 1 == numberTypes)
                eventTypes += createRandomEventType();
            else
                eventTypes += createRandomEventType() + ",";

            ++i;
        }

        return eventTypes;
    }

    public static String createRandomEventType(){

        int eventTypeIndex = NumberUtils.nextInt(10);

        return mEventTypeList[eventTypeIndex];

    }

    public static String createRandomActivityType(){

        int activityTypeIndex = NumberUtils.nextInt(3);

        return mActivityEventTypeList[activityTypeIndex];

    }


}
