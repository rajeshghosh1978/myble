package com.spectocor.testutilities.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by qwang on 2/11/2016.
 *
 * static function for array
 */
public class ArrayUtility {

    /**
     * convert Short array to short array
     *
     * @param ShortArray
     * @return
     */
    public static short[] ShortArray2shortArray(Object[] ShortArray){

        short[] shortArray;
        Short aShort;
        if(ShortArray == null){
            shortArray = null;
        }else{
            shortArray = new short[ShortArray.length];
            for(int i =0; i < ShortArray.length;++i){
                aShort = (Short)ShortArray[i];
                shortArray[i] = aShort.shortValue();
            }
        }

        return shortArray;
    }

    /**
     * convert Integer array to int array
     *
     * @param IntegerArray
     * @return int[]
     */
    public static int[] IntegerArray2IntArray(List<Integer> IntegerArray){

        int[] intArray;
        Integer aInteger;
        if(IntegerArray == null){
            intArray = null;
        }else{
            intArray = new int[IntegerArray.size()];
            for(int i =0; i < IntegerArray.size();++i){
                aInteger = IntegerArray.get(i);
                intArray[i] = aInteger.intValue();
            }
        }

        return intArray;
    }

    /**
     * split one array to multiple array
     *
     * @param array
     * @param max
     * @param <T>
     * @return
     */
    public static <T extends Object> List<T[]> splitArray(T[] array, int max){

        int x = array.length / max;
        int r = (array.length % max); // remainder

        int lower = 0;
        int upper = 0;

        List<T[]> list = new ArrayList<T[]>();

        int i=0;

        for(i=0; i<x; i++){

            upper += max;

            list.add(Arrays.copyOfRange(array, lower, upper));

            lower = upper;
        }

        if(r > 0){

            list.add(Arrays.copyOfRange(array, lower, (lower + r)));

        }

        return list;
    }

    /**
     * fill a int array with default value
     * @param DefaultVal
     * @param ArraySize
     * @return int[]
     */
    public static int[] fillingIntArray(int DefaultVal,int ArraySize){

        assert(DefaultVal > 0);
        assert(ArraySize > 0 );

        int[] intArray = new int[ArraySize];

        for(int i =0 ; i < ArraySize ; ++i){
            intArray[i] = DefaultVal;
        }

        return intArray;
    }

    /**
     * fill a long array with default value
     * @param DefaultVal
     * @param ArraySize
     * @return long[]
     */
    public static long[] fillingLongArray(int DefaultVal,int ArraySize){

        assert(DefaultVal > 0);
        assert(ArraySize > 0 );

        long[] longArray = new long[ArraySize];

        for(int i =0 ; i < ArraySize ; ++i){
            longArray[i] = DefaultVal;
        }

        return longArray;
    }

    /**
     * find maxi int from array
     * @param IntArray
     * @return
     */
    public static int max(int[] IntArray){

        int maxValue;

        int i ;

        assert(IntArray != null && IntArray.length > 0);

        maxValue = IntArray[0];
        i = 1;

        while(i < IntArray.length){
            if(maxValue > IntArray[i]){
                maxValue = IntArray[i];
            }
            ++i;
        }

        return maxValue;
    }

    /**
     * find min from int array
     * @param IntArray
     * @return
     */
    public static int min(int[] IntArray){
        int minValue;

        int i ;

        assert(IntArray != null && IntArray.length > 0);

        minValue = IntArray[0];
        i = 1;

        while(i < IntArray.length){
            if(minValue > IntArray[i]){
                minValue = IntArray[i];
            }
            ++i;
        }

        return minValue;
    }
}
