package com.spectocor.testutilities.utils;

import java.util.Random;

/**
 * Created by qwang on 1/8/2016.
 * provide mock number
 */
public class NumberUtils {

    private static Random rand = new Random();

    /**
     * @param MaxiNum Maxi Num , remove negative and zero
     * @return random int num
     */
    public static int mockRandomNumber(int MaxiNum) {

        int mock_num;

        mock_num = rand.nextInt(MaxiNum);

        if (mock_num == 0) {
            mock_num = 1;
        } else {
            if (mock_num < 0) {
                mock_num = -mock_num;
            }
        }

        return mock_num;
    }

    /**
     * wrap nextInt function
     *
     * @param MaxiNum random number range is from 0 to maxi
     * @return return random int number
     */
    public static int nextInt(int MaxiNum) {
        return rand.nextInt(MaxiNum);
    }

    /**
     * mock number array between MinNum and MaxNum
     * @param MinNum
     * @param MaxNum
     * @param ListSize
     * @return
     */
    public static int[] mockRandomNumberList(int MinNum,int MaxNum,int ListSize){
        assert(MinNum > 0);
        assert(MaxNum > 0);
        assert(ListSize > 0);

        int[] int_list = new int[ListSize];
        int i = 0;
        int curr_int;

        while(i < ListSize){

            curr_int = mockRandomNumber(MaxNum);

            if(curr_int >= MinNum){

                int_list[i] = curr_int;

                ++i;
            }
        }

        return int_list;
    }

    /**
     * mock long array between MinNum and MaxNum
     * @param MinNum
     * @param MaxNum
     * @param ListSize
     * @return
     */
    public static long[] mockRandomLongList(int MinNum,int MaxNum,int ListSize){
        assert(MinNum > 0);
        assert(MaxNum > 0);
        assert(ListSize > 0);

        long[] int_list = new long[ListSize];
        int i = 0;
        long curr_long;

        while(i < ListSize){

            curr_long = mockRandomNumber(MaxNum);

            if(curr_long >= MinNum){

                int_list[i] = curr_long;

                ++i;
            }
        }

        return int_list;
    }

    /**
     * check string is double or not
     * @param value
     * @return
     */
    public static boolean isDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * check string is float or not
     * @param value
     * @return
     */
    public static boolean isFloat(String value) {
        try {
            Float.parseFloat(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * check string is integer or not
     * @param value
     * @return
     */
    public static boolean isInt(String value){
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
