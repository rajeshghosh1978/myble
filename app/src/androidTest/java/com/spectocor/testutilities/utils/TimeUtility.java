package com.spectocor.testutilities.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

/**
 * Created by qwang on 1/5/2016.
 * the function for time
 */
public class TimeUtility {

    private static int mMaxiTime = 1000000;

    // get current log time
    public static int getCurrentLogTime() {
        int curr_time;
        curr_time = (int) (System.currentTimeMillis() / 1000);
        return curr_time;
    }

    // get current time
    public static long getCurrentTime() {
        long curr_time;
        curr_time = System.currentTimeMillis();
        return curr_time;
    }

    // get random time
    public static long getRandomTime() {
        long curr_time;

        Random rand = new Random();

        curr_time = rand.nextLong();

        return curr_time;
    }

    public static String UnixTimeStamp2DateStr(long UnixTimeStamp) {
        //long unixSeconds = 1372339860;
        Date date = new Date(UnixTimeStamp * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z"); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-6")); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);
        return formattedDate;
    }
}
