package com.spectocor.testutilities.utils;

import junit.framework.Assert;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;

//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class FileIo {


	public static final int CREATE = 1;
	public static void writeShortsToFile(short[] arr, String filepath) {
		String str = "";
		for (short anArr : arr) {

			str += anArr + "\n";

		}

		str.trim();


		try {
			//Files.write(Paths.get(filepath), str.getBytes(), StandardOpenOption.CREATE);
			writeAllLines(filepath, str.getBytes(), 1);
		} catch (IOException e) {
			e.getStackTrace();
		}


	}

	public static void writeDoublesToFile(double[] arr, String filepath) {
		String str = "";
        for (double anArr : arr) {

            str += anArr + "\n";

        }
		
		str.trim();
		
		
		try {
			//Files.write(Paths.get(filepath), str.getBytes(), StandardOpenOption.CREATE);
			writeAllLines(filepath, str.getBytes(), 1);
		} catch (IOException e) {
			e.getStackTrace();
		}
		
		
	}
	
	public static void writeIntsToFile(int[] arr, String filepath) {
		String str = "";
        for (int anArr : arr) {

            str += anArr + "\n";

        }
		
		str.trim();
		
		
		try {
			//Files.write(Paths.get(filepath), str.getBytes(), StandardOpenOption.CREATE);
			writeAllLines(filepath, str.getBytes(), 1);
		} catch (IOException e) {
			e.getStackTrace();
		}
		
		
	}

    public static int[] readIntsFromFile(final String filePath) {

        int[] intArr;
        ArrayList<String> inputLines;
        try {

            //inputLines = (ArrayList<String>) Files.readAllLines(Paths.get(filePath), Charset.defaultCharset());
			inputLines = (ArrayList<String>) readAllLines(filePath, Charset.defaultCharset());
            intArr = new int[inputLines.size()];

            for(int j = 0; j < inputLines.size(); j++) {
                intArr[j] = (int) Double.parseDouble(inputLines.get(j));
            }

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return intArr;

    }
	
	public static double[] readDoublesFromFile(final String filePath) {

		double[] signal;
		ArrayList<String> inputLines;
		try {
			
			//inputLines = (ArrayList<String>) Files.readAllLines(Paths.get(filePath), Charset.defaultCharset());
			inputLines = (ArrayList<String>) readAllLines(filePath, Charset.defaultCharset());
			signal = new double[inputLines.size()];
			
			for(int j = 0; j < inputLines.size(); j++) {
				signal[j] = Double.parseDouble(inputLines.get(j));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		return signal;

	}

	public static short[] readDoublesFromFileAsShorts(final String filePath, final int factor) {

		short[] signal;
		ArrayList<String> inputLines;
		try {

			//inputLines = (ArrayList<String>) Files.readAllLines(Paths.get(filePath), Charset.defaultCharset());
			inputLines = (ArrayList<String>) readAllLines(filePath, Charset.defaultCharset());
			signal = new short[inputLines.size()];

			for (int j = 0; j < inputLines.size(); j++) {
				signal[j] = (short) (Double.parseDouble(inputLines.get(j)) * factor);
			}

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return signal;

	}

	// create a text file
	public static void writeAllLines(String FilePath,byte[] TextString , int Options) throws IOException{

		PrintWriter out = new PrintWriter(FilePath);
		out.println(new String(TextString));
		out.flush();
		out.close();

	};
	// read all text file
	public static ArrayList<String> readAllLines(String FilePath, Charset currCharSet ) throws IOException{

		FileReader fileReader = new FileReader(FilePath);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		ArrayList<String> lines = new ArrayList<String>();
		String line = null;
		while ((line = bufferedReader.readLine()) != null) {
			lines.add(line);
		}
		bufferedReader.close();

		return lines;
	}
	
}
