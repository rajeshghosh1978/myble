package com.spectocor.testutilities.utils;

import java.util.ArrayList;

/**
 * Created by qwang on 12/10/2015.
 * The class provide string utility function
 */
public class StringUtils {

    private static final String mCharSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private static int mMaxiFacilityCodeLen = 10;
    private static int mActivationCodeLen = 6;

    // mock to generate random string
    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(mCharSet.charAt(NumberUtils.nextInt(mCharSet.length())));
        return sb.toString();
    }

    // create random facility test code list
    public static ArrayList<String> mockFacilityCodeList(int MaxiNum) {
        ArrayList<String> mockFacilityCodeList = new ArrayList<>();

        if (MaxiNum > 0) {
            for (int i = 0; i < MaxiNum; ++i) {
                mockFacilityCodeList.add(mockFacilityCode());
            }
        }

        return mockFacilityCodeList;
    }

    // create random facility test code
    public static String mockFacilityCode() {

        String mock_facility_code;

        int code_len;

        code_len = NumberUtils.mockRandomNumber(mMaxiFacilityCodeLen);

        mock_facility_code = randomString(code_len);

        return mock_facility_code;
    }

    public static int[] mockArrayIndexList(int MaxiNum, int ArraySize) {
        int mockArrayIndexList[] = new int[MaxiNum];

        if (MaxiNum > 0) {
            for (int i = 0; i < MaxiNum; ++i) {
                mockArrayIndexList[i] = mockFacilityCode(ArraySize);
            }
        }

        return mockArrayIndexList;
    }

    // create random array index. from 0 --- ArraySize - 1
    public static int mockFacilityCode(int ArraySize) {

        int mock_index;

        int code_len;

        code_len = NumberUtils.mockRandomNumber(ArraySize);

        mock_index = code_len - 1;

        return mock_index;
    }

    // create random session activation test code list
    public static ArrayList<String> mockSessionActivationList(int MaxiNum) {
        ArrayList<String> mockCodeList = new ArrayList<>();

        if (MaxiNum > 0) {
            for (int i = 0; i < MaxiNum; ++i) {
                mockCodeList.add(mockSessionActivationCode());
            }
        }

        return mockCodeList;
    }

    // create random session activation test code
    public static String mockSessionActivationCode() {

        String mock_activation_code;

        mock_activation_code = randomString(mActivationCodeLen);

        return mock_activation_code;
    }
}
