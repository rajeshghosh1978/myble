package com.spectocor.testutilities.utils;

import android.os.SystemClock;

/**
 * Created by qwang on 2/11/2016.
 *
 * extra functions which used to control thread
 */
public class ThreadUtility {

    /**
     * application sleep Million seconds
     *
     * @param MillionSeconds
     */
    public static void applicationSleep(long MillionSeconds){
        SystemClock.sleep(MillionSeconds);
    }
}
