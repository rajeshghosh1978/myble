package com.spectocor.testutilities;

/**
 * Created by qwang on 2/16/2016.
 *
 * define global const variable for testing environment
 */
public class TestEnvGlobalConst {

    public static final int NORMAL_HEART_BEAT_RATE = 70;        // normal heart beat rate number

    public static final int MAX_HEART_BEAT_RATE = 30;           // min heart beat rate number

    public static final int MIN_HEART_BEAT_RATE = 300;          // max heart beat rate number

    public static final int DEFAULT_RUN_TEST_TIME = 30;         // default run test time

    public static final int SAMPLE_RATE = 250;                  // default ECG sampling data rate

    public static final long APP_SLEEP_MILLION_SECOND = 7000;   // default million second waiting time for application launch

    public static final int DEVICE_CHUNK_SIZE         = 180000;

    public static final int ECG_DATA_CHUNK_DURATION_SECOND = 5 * 60;  // five minutes

    public static final int ECG_DATA_CHUNK_SAMPLE_NUMBER = SAMPLE_RATE * ECG_DATA_CHUNK_DURATION_SECOND;    //  75000

    public static final int TELEMETRY_SESSION_DURATION_DAYS = 31;

    public static final int PER_DAY_CHUNK_NUMBER = (24 * 60 ) / 5 ; //288

    public static final int TELEMETRY_SESSION_CHUNK_NUMBER = TELEMETRY_SESSION_DURATION_DAYS * PER_DAY_CHUNK_NUMBER ;  // 8928

    public static final int WAIT_END_SESSION = 40000;  // 30 seconds

    ////////////////////////////////////////////
    // USD SERVER
    ////////////////////////////////////////////
    public static String SERVER_ADDRESS                       = "http://207.191.20.80" ;          //"https://gateway.spectocor.com";//"http://207.191.20.80" ;       //"http://208.67.179.147";

    public static String SSL_SERVER_ADDRESS                   = "https://gateway.spectocor.com";  //"http://207.191.20.80" ;       //"http://208.67.179.147";

    public static String FAIL_SERVER_ADDRESS                  = "http://208.69.179.147";          // fake server IP for test

    public static String REST_URL_DEVICE_ACTIVATION           = "/en/v1/device/activation";       // active a new session with activation code

    public static String REST_URL_SENDING_ECG_DATA_CHUNK      = "/en/v1/ecg/chunk";               // sending ecg data chunk

    public static String REST_URL_PATIENT_LOG                 = "/en/v1/log/patient";             // patient trigger events

    public static String REST_URL_DEVICE_LOG                  = "/en/v1/log/device";               // device log

    public static String REST_URL_SESSION_START               = "en/v1/session/start";            // session start

    // for test REST API
    public static String REST_URL_CREATE_ACTIVATION           = "/en/v0/activation/create";         // create a new activation code

    public static String REST_URL_SESSION_END                 = "/en/v0/session/end";               // end session

    //public static String REST_URL_SESSION_REMOVE              = "/en/v0/session/end";              // end session REST URL, not use now

    public static String BASIC_AUTHORIZATION                  = "Basic c2lyYWo6dGVzdDEyMw==";

    public static String CONTENT_TYPE                         = "application/json";
}
