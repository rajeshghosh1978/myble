package com.spectocor.testutilities.html.page;

/**
 * Created by qwang on 3/21/2016.
 */
public class EndSessionPage extends BasePage {

    public static String Title = "Your Session Has Ended";

    /**
     * check the current page which include the title string or not
     *
     * @param PageContent
     * @return
     */
    public static boolean isActivatePage(final String PageContent){

        boolean rc ;

        rc = PageContent.contains(Title);

        return rc;
    }

}
