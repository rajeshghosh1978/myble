package com.spectocor.testutilities.html.page;

/**
 * Created by qwang on 3/23/2016.
 *
 * keep information for Report Symptom Home Page
 *
 * should include
 *
 * reset   --- reset button , maybe not there
 * Welcome $PatientFirstName $FirstInitPatientLastName
 * Bluetooth
 * Internet
 * ECG
 * Battery
 * PDA
 * Battery
 * Report Symptom
 * REPORT SYMPTOM TEST
 *   S P E C T O C O R TM 1-888-563-2643
 *
 */
public class ReportSymptomHomePage extends BasePage {

    public static String Title = "Report Symptom";

    public static boolean isActivatePage(final String PageContent){

        boolean rc ;

        rc = PageContent.contains(Title);

        return rc;
    }
}
