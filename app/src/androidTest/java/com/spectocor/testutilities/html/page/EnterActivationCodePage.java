package com.spectocor.testutilities.html.page;

/**
 * Created by qwang on 3/23/2016.
 *
 * reset
 * NEXT
 * MANUAL ENROLLMENT LOOKUP?
 *   S P E C T O C O R TM 1-888-563-2643
 *
 *
 */
public class EnterActivationCodePage extends BasePage {

    public static String Title = "MANUAL ENROLLMENT LOOKUP?";

    public static String ID = "enter-activation-code-1-dialog";  // id at web page

    /**
     * check the current page which include the title string or not
     * @param PageContent
     * @return
     */
    public static boolean isActivatePage(final String PageContent){

        boolean rc ;

        rc = PageContent.contains(Title);

        return rc;
    }

}
