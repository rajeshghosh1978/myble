package com.spectocor.testutilities.html.page;

/**
 * Created by qwang on 3/21/2016.
 *
 * keep information for choose ecg device page information
 */
public class ChooseECGDevicePage extends BasePage {

    public static String Title = "Choose ECG Device";

    public static String ID = "ecg-device-list-at-facility-page";

    public static String CallFunc = "lookupEcgAtShipping()";

    public static String[] DefaultECGDeviceName = {"STL ECG 201","STL ECG 517","STL ECG 152","STL ECG 320"};

    /**
     * check the current page which include the title string or not
     * @param PageContent
     * @return
     */
    public static boolean isActivatePage(final String PageContent){

        boolean rc ;

        rc = PageContent.contains(Title);

        return rc;
    }

    /**
     * check current page include ECG device name
     * @param contentPage
     * @return
     */
    public static boolean isIncludeECGDeviceName(final String contentPage){

        boolean rc = false;

        for(String deviceName : DefaultECGDeviceName){

            rc = contentPage.contains(deviceName);

            if(rc){

                rc = true;

                break;

            }

        }

        return rc;
    }

}
