package com.spectocor.testutilities.html.page;

/**
 * Created by qwang on 3/23/2016.
 */
public class EcgSignalViewPage extends BasePage {

    public static String Title = "ECG Signal View";

    public static String ID = "admin-ecg-signal-page";

    public static String CallFunc = "displayAdminEcgSignalPage()";
}
