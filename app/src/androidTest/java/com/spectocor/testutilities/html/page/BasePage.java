package com.spectocor.testutilities.html.page;

import android.util.Log;

/**
 * Created by qwang on 3/21/2016.
 *
 * keep common information for page
 */
public class BasePage {

    private static String TAG = "BasePage";

    public static String ResetBtn = "reset"; // at the top right corner

    public static String CompanyName = "  S P E C T O C O R TM"; // at the bottom left

    public static String CompanyPhone = "1-888-563-2643";   // company phone info , at the bottom right

    /**
     * check page include correct company phone number
     * @param pageContent
     * @return
     */
    public static boolean isIncludeCompanyPhoneNumber(final String pageContent){

        return pageContent.contains(CompanyPhone);

    }

    /**
     * check page include correct company name
     * @param pageContent
     * @return
     */
    public static boolean isIncludeCompanyName(final String pageContent){

        return pageContent.contains(CompanyName);
    }

    /**
     * check page include reset button
     * @param pageContent
     * @return
     */
    public static boolean isIncludeResetBtn(final String pageContent){

        return pageContent.contains(ResetBtn);
    }

    /**
     * dump web page content
     * @param pageContent
     */
    public static void dumpPageContent(final String pageContent){

        String[] lines = pageContent.split("\n");

        for(String line : lines){
            Log.i(TAG, line);
        }

    }
}
