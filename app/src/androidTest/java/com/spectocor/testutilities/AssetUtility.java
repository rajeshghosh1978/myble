package com.spectocor.testutilities;

import android.content.Context;
import android.content.res.AssetManager;

import com.spectocor.testutilities.ecg_data.ECGBTRawData;
import com.spectocor.testutilities.utils.ArrayUtility;

import junit.framework.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qwang on 2/11/2016.
 *
 * read assets file
 */
public class AssetUtility {

    private static String TAG = "";
    private static Integer mECGDefaultFillData = new Integer("2");
    private static int mSampleRate = TestEnvGlobalConst.SAMPLE_RATE;

    /**
     * http://stackoverflow.com/questions/9544737/read-file-from-assets
     *
     * Reads the text of an asset. Should not be run on the UI thread.
     *
     * @param mgr
     *            The {@link AssetManager} obtained via {@link //Context#getAssets()}
     * @param path
     *            The path to the asset.
     * @return The plain text of the asset
     */
    public static String readAsset(AssetManager mgr, String path) {
        String contents = "";
        InputStream is = null;
        BufferedReader reader = null;
        try {
            is = mgr.open(path);
            reader = new BufferedReader(new InputStreamReader(is));
            contents = reader.readLine();
            String line = null;
            while ((line = reader.readLine()) != null) {
                contents += '\n' + line;
            }
        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignored) {
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignored) {
                }
            }
        }
        return contents;
    }

    /**
     *
     * Reads the text of an asset. Should not be run on the UI thread.
     *
     * @param context
     *            The {@link Context}
     * @param path
     *            The path to the asset.
     * @return short[] which include two channels BT raw data
     */
    public static short[] readBTRAW(Context context, String path) {
        //short[] channel_data ;

        List<Short> channel_data = new ArrayList<Short>();
        InputStream is = null;
        BufferedReader reader = null;
        String curr_data;
        try {
            is = context.getAssets().open(path);
            reader = new BufferedReader(new InputStreamReader(is));
            curr_data = reader.readLine();
            channel_data.add(Short.decode(curr_data));
            while ((curr_data = reader.readLine()) != null) {
                channel_data.add(Short.decode(curr_data));
            }
        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignored) {
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignored) {
                }
            }
        }

        return ArrayUtility.ShortArray2shortArray(channel_data.toArray());
    }

    /**
     *
     * Reads the text of an asset. Should not be run on the UI thread.
     *
     * @param context
     *            The {@link Context}
     * @param path
     *            The path to the asset.
     * @return ECGBTRawData which include two channels BT raw data
     */
    public static ECGBTRawData readBTRAWData(Context context, String path) {
        //short[] channel_data ;

        ECGBTRawData ecgbtRawData;
        int lineNo = 0;
        List<Integer> channel_data1 = new ArrayList<Integer>();
        List<Integer> channel_data2 = new ArrayList<Integer>();
        List<Integer> channel_clean_data = new ArrayList<Integer>();
        String[] curr_line_data;
        int ecg_data_channel_num = 0;

        InputStream is = null;
        BufferedReader reader = null;
        String curr_data;
        try {
            is = context.getAssets().open(path);
            reader = new BufferedReader(new InputStreamReader(is));
            //curr_data = reader.readLine();
            //channel_data.add(Short.decode(curr_data));
            while ((curr_data = reader.readLine()) != null) {
                // need to check , how many data at one line
                // split by ,
                // 12,23,34
                curr_line_data = curr_data.split(",");
                if(ecg_data_channel_num == 0){
                    ecg_data_channel_num = curr_line_data.length;
                }
                else
                {
                    Assert.assertTrue("Input data is not correct format",ecg_data_channel_num == curr_line_data.length);
                }
                switch(curr_line_data.length){
                    case 0:
                        Assert.assertTrue("Input data is not correct format",false);
                        break;
                    case 1:
                        channel_data1.add(ECGBTRawData.decodeInteger(curr_line_data[0]));
                        //channel_data2.add(mECGDefaultFillData);
                        //channel_clean_data.add(mECGDefaultFillData);
                        break;
                    case 2:
                        channel_data1.add(ECGBTRawData.decodeInteger(curr_line_data[0]));
                        channel_data2.add(ECGBTRawData.decodeInteger(curr_line_data[1]));
                        //channel_clean_data.add(mECGDefaultFillData);
                        break;
                    case 3:
                        channel_data1.add(ECGBTRawData.decodeInteger(curr_line_data[1]));
                        channel_data2.add(ECGBTRawData.decodeInteger(curr_line_data[2]));
                        channel_clean_data.add(ECGBTRawData.decodeInteger(curr_line_data[0]));
                        break;
                    default:
                        Assert.assertTrue("Input data is not correct format",false);
                }
                ++lineNo;
            }
        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignored) {
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignored) {
                }
            }
        }

        if(channel_data1.size() > 0){
            ecgbtRawData = new ECGBTRawData(channel_data1,channel_data2,channel_clean_data);
        }else
            ecgbtRawData = null;

        return ecgbtRawData;
    }


    public static void loadSplitTwoChannelData(Context context,String fileName,ArrayList<short[]> continuousSignalChannel1,ArrayList<short[]> continuousSignalChannel2){

        // prepare test data
        short[] tchannel1 ;
        short[] tchannel2 ;

        assert(continuousSignalChannel1 != null);
        assert(continuousSignalChannel2 != null);

        // read data file
        ECGBTRawData ecgbtRawData;
        ecgbtRawData = readBTRAWData(context, fileName);

        int channel1_index = 0;
        int channel2_index = 0;

        short[] channel1, channel2;

        if(ecgbtRawData.hasChannel1Data() && ecgbtRawData.hasChannel2Data()){
            channel1 = ECGBTRawData.normalization(ecgbtRawData.getChannelData(1));
            channel2 = ECGBTRawData.normalization(ecgbtRawData.getChannelData(2));
        }else {
            channel1 = ECGBTRawData.normalization(ecgbtRawData.getChannelData(1));
            channel2 = channel1;  // same as channel 1
        }

        for (int i = 0; i < channel1.length; ) {

            tchannel1 = new short[mSampleRate];
            tchannel2 = new short[mSampleRate];

            System.arraycopy(channel1, channel1_index, tchannel1, 0, mSampleRate);
            System.arraycopy(channel2, channel2_index, tchannel2, 0, mSampleRate);

            continuousSignalChannel1.add(tchannel1);
            continuousSignalChannel2.add(tchannel2);

            channel1_index += mSampleRate;
            channel2_index += mSampleRate;
            i += mSampleRate;

        }
    }

}
