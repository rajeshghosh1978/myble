package com.spectocor.testutilities;

import android.content.Context;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.DbHelper;
import com.spectocor.micor.mobileapp.ecgadapter.compression.zip.ZipCompression;
import com.spectocor.micor.mobileapp.ecgblehandler.EcgChannelBluetoothData;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.testutilities.utils.NumberUtils;

import junit.framework.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by qwang on 2/1/2016.
 *
 * create ECG mock data
 */
public class ECGDataCreator {
    public static final String TAG = "ECGDataCreator";

    public static final int ECGUpperBound = 255;    // largest ECG value

    public static final int SAMPLE_RATE = 250;      //400;   BT sample rate

    public static final int NORMAL_HR = 70;

    /**
     * mock an ECG data chunk
     * @param ChunkSize is data chunk
     * @return byte[] is ChunkSize data chunk which include two channel compression data, session id, chunk start timestamp
     */
    public static byte[] createECGDataChunk(int ChunkSize){

        short[] chunkChannelData = new short[ChunkSize * 2];
        //byte[] chunkChannel2Data = new byte[ChunkSize];
        byte[] compressionChunkData;

        // fill random number
        for(int i = 0; i < ChunkSize;++i){
            chunkChannelData[i] = (short)NumberUtils.nextInt(ECGUpperBound);
            chunkChannelData[i + ChunkSize] = (short)NumberUtils.nextInt(ECGUpperBound);
        }

        // compression
        try {
            compressionChunkData = ZipCompression.compressEcg(chunkChannelData);
        }catch (IOException ioe){
            Log.e(TAG,ioe.getMessage());
            compressionChunkData = null;
        }

        return compressionChunkData;
    }

    /**
     * mock a random session id
     *
     * @param MaxiSessionID is largest session ID number
     * @return
     */
    public static int createSessionID(int MaxiSessionID){
        return NumberUtils.mockRandomNumber(MaxiSessionID);
    }

    /**
     * create one ECG channel data
     * @param Size of one channel data
     * @return short[] array of one channel data
     */
    public static short[] createECGChannelData(int Size){
        // fill random number
        short[] chunkChannelData = new short[Size];

        for(int i = 0; i < Size;++i){
            chunkChannelData[i] = (short)NumberUtils.nextInt(ECGUpperBound);
        }

        return chunkChannelData;
    }

    /**
     * read two channel ECG BT raw data from text file under assets
     *
     * @param context
     * @param Path
     * @return
     */
    public static short[] readECGTwoChannelData(Context context,String Path){

        return AssetUtility.readBTRAW(context,Path);
    }
    /**
     * create "ElapsedSecond" seconds bluetooth raw data at StartTimeStamp (Second) with default sample rate 400
     *
     * @param ElapsedSecond
     * @param StartTimeStamp
     * @return
     */
    public static EcgChannelBluetoothData createEcgChannelBluetoothData(int ElapsedSecond,long StartTimeStamp ){

        return createEcgChannelBluetoothData(ElapsedSecond, StartTimeStamp, SAMPLE_RATE);

    }

    /**
     * create "ElapsedSecond" seconds bluetooth raw data at StartTimeStamp (Second) with sample rate
     * @param ElapsedSecond
     * @param StartTimeStamp
     * @param SampleRate
     * @return
     */
    public static EcgChannelBluetoothData createEcgChannelBluetoothData(int ElapsedSecond,long StartTimeStamp,int SampleRate ){
        short[] channel1;
        short[] channel2;

        channel1 = createECGChannelData(SampleRate*ElapsedSecond);
        channel2 = createECGChannelData(SampleRate*ElapsedSecond);

        EcgChannelBluetoothData bluetoothData  = new EcgChannelBluetoothData(StartTimeStamp,channel1,channel2);

        return bluetoothData;

    }

    public static EcgChannelBluetoothData createEcgChannelBluetoothData(int ElapsedSecond){

        return createEcgChannelBluetoothData(ElapsedSecond, System.currentTimeMillis());

    }

    /**
     * create one random ECG data chunk
     *
     * @param elapsedSecond
     * @param sessionID
     * @return
     */
    public static EcgDataChunk createRandomEcgDataChunk(long startTimeSecond,long elapsedSecond,int sessionID){

        List<EcgChannelBluetoothData> BTDataList = createBTRawDataList(startTimeSecond,elapsedSecond);

        EcgDataChunk ecgDataChunk = EcgChannelBluetoothData2EcgDataChunk(sessionID,BTDataList);

        return ecgDataChunk;

    }

    /**
     * create a list of ecgdatachunk
     *
     * @param startTimeSecond
     * @param elapsedSecond
     * @param sessionID
     * @param ListLen
     * @return
     */
    public static List<EcgDataChunk> createRandomEcgDataChunkList(long startTimeSecond,long elapsedSecond,int sessionID,long ListLen){
        List<EcgDataChunk> ecgDataChunks = new ArrayList<>();

        long currTimeSecond = startTimeSecond;

        EcgDataChunk ecgDataChunk;

        for(int i = 0; i < ListLen; ++i){

            ecgDataChunk = createRandomEcgDataChunk(currTimeSecond,elapsedSecond,sessionID);

            ecgDataChunks.add(ecgDataChunk);

            currTimeSecond += elapsedSecond;
        }

        return ecgDataChunks;

    }

    /**
     * convert a BT raw data list to one ECG data chunk
     *
     * @param SessionID
     * @param BTDataList
     * @return
     */
    public static EcgDataChunk EcgChannelBluetoothData2EcgDataChunk(int SessionID,List<EcgChannelBluetoothData> BTDataList) {

        // JOINING BOTH CHANNELS AS ONE ARRAY
        long StartTimeStamp;

        StartTimeStamp = BTDataList.get(0).timestamp;  // the first second timestamp is ECG data chunk timestamp

        short[] bothChannelsShorts = new short[BTDataList.size() * BTDataList.get(0).ch1.length * 2];

        EcgDataChunk ecgDataChunkToStore;

        // COMPRESSING
        byte[] compressedBytes = null;

        for(int i = 0;i < BTDataList.size();++i){
            for(int j = 0;j < BTDataList.get(i).ch1.length;++j){
                bothChannelsShorts[i * BTDataList.get(i).ch1.length + j] = BTDataList.get(i).ch1[j];
                bothChannelsShorts[BTDataList.size() * BTDataList.get(0).ch1.length + i * BTDataList.get(i).ch1.length + j] = BTDataList.get(i).ch2[j];
            }
        }

       // short[] Channel1 = new short[BTDataList.size() * BTDataList.get(0).ch1.length];
       // short[] Channel2 = new short[BTDataList.size() * BTDataList.get(0).ch1.length];


        /*for(int i = 0; i < 300; i++) {
            for(int j = 0; j < 400; j++) {
                Channel1[i* 400 + j] = BTDataList.get(i).ch1[j];
                Channel2[i* 400 + j] = BTDataList.get(i).ch2[j];
            }

        }

        bothChannelsShorts = ArrayConcat.concat(Channel1, Channel2);*/

        Log.d(TAG, "BOTH_CHANNEL_SHORTS_HASHCODE: " + Arrays.hashCode(bothChannelsShorts));


        try{
            compressedBytes = ZipCompression.compressEcg(bothChannelsShorts);

            ecgDataChunkToStore = new EcgDataChunk();

            // reference com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper.insertEcgChunk
            // ECGDataChunk
            ecgDataChunkToStore.monitoringSessionId = SessionID;
            ecgDataChunkToStore.chunkDataChannelCompressed = compressedBytes;
            ecgDataChunkToStore.chunkStartDateUnixMs = StartTimeStamp;
            ecgDataChunkToStore.chunkTransferTimestamp = -1;
            ecgDataChunkToStore.insertTimestamp = -1;
            ecgDataChunkToStore.timezoneId = DbHelper.TIMEZONE_NAME_ID;  //"America/Chicago";
            ecgDataChunkToStore.timezoneOffsetMillis = DbHelper.TIMEZONE_OFFSET_MILLIS;//-21600000;
            ecgDataChunkToStore.numberOfSamples = 75000; // 5 * 60 * 250


        }catch (IOException e){
            Assert.assertTrue("Exception at compression module",false);
            ecgDataChunkToStore = null;
        }


        return ecgDataChunkToStore;

    }

    /**
     * create a list of random heart beat
     *
     * @param Size
     * @return int[] heart beat list
     */
    public static int[] createHeartBeatList(int Size){

        return NumberUtils.mockRandomNumberList(30,300,Size);
    }

    /**
     * create a random heart beat
     *
     * default heart beat between [30,300]
     *
     * @param MinHeartBeat
     * @param MaxHeartBeat
     * @return
     */
    public static int createRandomHeartBeat(int MinHeartBeat,int MaxHeartBeat){

        boolean found = false;
        int randomHeartBeat = MinHeartBeat;

        while(!found){
            randomHeartBeat = NumberUtils.mockRandomNumber(MaxHeartBeat);
            if(randomHeartBeat >= MinHeartBeat){
                found = true;
            }
        }

        return randomHeartBeat;
    }

    /**
     * return normal heart beat sample interval
     * @return
     */
    public static int normalHeartBeatSampleInterval(){

        return calculateHeartBeatSampleInterval(NORMAL_HR);
    }

    /**
     * return sample interval based on heart beat rate
     * @param HR
     * @return
     */
    public static int calculateHeartBeatSampleInterval(int HR){

        return (60 * 1000/ HR);
    }

    /**
     *  create a list which include 5 minutes BT raw data
     *
     * @param startTimeStamp, start time stamp, unit second
     * @param elapsedSecond, how long for data chunk, unit second
     * @return
     */
    public static List<EcgChannelBluetoothData> createBTRawDataList(long startTimeStamp,long elapsedSecond){

        EcgChannelBluetoothData oneSecondData;

        //int data_chunk_minutes = 5; // unit is minute

        int bt_data_minutes = 1;  // unit is second

        long start_stamp = startTimeStamp;  // BT data , unit second

        long current_stamp = start_stamp;

        List<EcgChannelBluetoothData> btDataList = new ArrayList<EcgChannelBluetoothData>();

        //elapsedSecond = data_chunk_minutes * 60 = 300

        for(int i = 0; i < elapsedSecond  ; ++i){

            oneSecondData = createEcgChannelBluetoothData(bt_data_minutes,current_stamp);

            current_stamp += 1;

            btDataList.add(oneSecondData);
        }

        return btDataList;

    }
}
