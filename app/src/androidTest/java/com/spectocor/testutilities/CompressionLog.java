package com.spectocor.testutilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * This class provides methods to log ECG data compression performance information
 * There are two main methods.
 * addLogItem : add new log item
 * dumpLog :  write whole log information to a log file
 * <p/>
 * Created by qwang on 11/24/2015.
 */
public class CompressionLog {
    private ArrayList<CompressionLogItem> m_log_items; // log item list

    public CompressionLog() {
        m_log_items = new ArrayList<>();
    }

    // add a new log item to list
    public int addLogItem(String db_record, int org_data_size, int cmp_data_size, long cmp_duration, long dcp_duration, String pass_fail) {
        CompressionLogItem log_item = new CompressionLogItem(db_record, org_data_size, cmp_data_size, cmp_duration, dcp_duration, pass_fail);

        m_log_items.add(log_item);

        return m_log_items.size();
    }

    // add a new log item to list with performance field information
    public int addLogItem(String db_record, int org_data_size, int cmp_data_size, long cmp_duration, long dcp_duration, String pass_fail, String perf_pf) {
        CompressionLogItem log_item = new CompressionLogItem(db_record, org_data_size, cmp_data_size, cmp_duration, dcp_duration, pass_fail, perf_pf);

        m_log_items.add(log_item);

        return m_log_items.size();
    }

    // dump all log item to log file
    public void dumpLog(String LogFileName) {

        BufferedWriter writer = null;

        String log_field_title;  // log field title

        String curr_line; // current log linr

        log_field_title = "Record Path,Compression Ratio,Compression Time,Decompression Time, Functionality Pass/Fail, Performance Test Pass/Fail\n";

        try {
            //create a temporary file
            String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            String log_file_name = LogFileName + "_" + timeLog + ".log.csv";
            File logFile = new File(log_file_name);

            // This will output the full path where the file will be written to...
            //System.out.println(logFile.getCanonicalPath());

            writer = new BufferedWriter(new FileWriter(logFile));

            CompressionLogItem log_item;

            // write title
            writer.write(log_field_title);

            // dump all log
            for (int i = 0; i < m_log_items.size(); ++i) {
                log_item = m_log_items.get(i);
                curr_line = log_item.get_db_record() + "," + log_item.get_cmp_ratio() + "," + (float) log_item.get_cmp_duration() / 1000000 + "," + (float) log_item.get_dcp_duration() / 1000000 + "," + log_item.get_pass_fail() + "," + log_item.get_perf_pf();
                writer.write(curr_line);
                writer.newLine();
            }
            // close
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                if (writer != null)
                    writer.close();
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            }

        }

    }
}
