package com.spectocor.testutilities.netwrok;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.spectocor.testutilities.utils.NumberUtils;

import java.util.List;

/**
 * Created by qwang on 2/10/2016.
 *
 * get wifi status , turn on/off wifi
 */
public class WIFIUtility {

    private static String TAG = "WIFIUtility";
    private static String mThreadName = "WIFINetworkStatusSimulator";
    private static Thread mThread;

    private static WIFIUtility _instance;

    private static Context mContext;
    private static WifiManager mWifiManager;
    private static ConnectivityManager mConnManager;

    public static WIFIUtility getInstance(Context context)
    {
        assert(context != null);

        if (_instance == null)
        {
            _instance = new WIFIUtility(context);
        }
        return _instance;
    }

    private WIFIUtility(Context context){
        mContext = context;
        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        mConnManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public boolean getWIFIStatus(){

        boolean rc ;

//        mWifiManager.getWifiState();
//        Network[]  networkInfoList = mConnManager.getAllNetworks();
//        NetworkInfo currNetworkInfo;
//        for(Network network : networkInfoList){
//            currNetworkInfo = mConnManager.getNetworkInfo(network);
//            Log.d(TAG,currNetworkInfo.getTypeName());
//            if(currNetworkInfo.getTypeName().compareTo("WIFI")==0 && currNetworkInfo.isAvailable()){
//                rc = true;
//                break;
//            }
//        }
        assert(mWifiManager != null);
        rc = mWifiManager.isWifiEnabled();
        return rc;
    }

    public void toggleWIFIStatus(){
        boolean curr_status = getWIFIStatus();

        setWIFIStatus(!curr_status);
    }

    public void setWIFIStatus(boolean NewStatus){
        mWifiManager.setWifiEnabled(NewStatus);
    }

    /**
     * start a thread to simulate connection network and disconnection network
     */
    public void simulatorNetworkStatus(){


        mThread = new Thread(new Runnable() {
            public void run() {
                // create a random number array
                // still too fast to turn on and turn off network connection
                int[] waiting_time = NumberUtils.mockRandomNumberList(1000,5000,100);

                int i = 0;
                boolean wifi_status;
                try{

                    for(i=0;i<100;++i){
                        toggleWIFIStatus();
                        wifi_status = getWIFIStatus();
                        Log.d(TAG, "WIFI:"+wifi_status);
                        Thread.sleep(waiting_time[i]); ;
                    }

                }catch (InterruptedException e){

                    Log.d(TAG,e.getMessage());
                }

            }
        },mThreadName);

        mThread.start();
    }


    public Thread.State getThreadStatus(){
        if(mThread != null)
            return mThread.getState();
        else
            return Thread.State.TERMINATED;
    }

    public void dumpWIFIConfig(){
        List<WifiConfiguration> list = mWifiManager.getConfiguredNetworks();
        for( WifiConfiguration i : list ) {
//            if(i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
//
//                mWifiManager.disconnect();
//                mWifiManager.enableNetwork(i.networkId, true);
//                mWifiManager.reconnect();
//
//                break;
//            }
            Log.d(TAG,""+i.SSID);
            Log.d(TAG,""+i.toString());
            //Log.d(TAG, "" + i.providerFriendlyName  );
            Log.d(TAG, "" + i.networkId  );

        }
    }

}
