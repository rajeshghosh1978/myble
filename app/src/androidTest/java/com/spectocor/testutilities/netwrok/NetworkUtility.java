package com.spectocor.testutilities.netwrok;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by qwang on 2/10/2016.
 */
public class NetworkUtility {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;


    private static NetworkUtility _instance;
    private static Context mContext;
    private static ConnectivityManager mConnectivityManager;

    public static NetworkUtility getInstance(Context context)
    {
        assert(context != null);

        if (_instance == null)
        {
            _instance = new NetworkUtility(context);
        }
        return _instance;
    }

    private NetworkUtility(Context context){
        mContext = context;
        mConnectivityManager  = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public int getConnectivityStatus() {

        NetworkInfo activeNetwork = mConnectivityManager.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public String getConnectivityStatusString() {
        int conn = getConnectivityStatus();
        String status = null;
        if (conn == TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }
}
