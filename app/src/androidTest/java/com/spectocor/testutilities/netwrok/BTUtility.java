package com.spectocor.testutilities.netwrok;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;

/**
 * Created by qwang on 2/10/2016.
 *
 * control BT on/off, get BT status
 */
public class BTUtility {

    private static String TAG = "BTUtility";

    private static BTUtility _instance;
    private static Context mContext;
    private static BluetoothAdapter mBluetoothAdapter;


    public static BTUtility getInstance(Context context)
    {
        assert(context != null);

        if (_instance == null)
        {
            _instance = new BTUtility(context);
        }
        return _instance;
    }

    private BTUtility(Context context){
        mContext = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public boolean getBTStatus(){
        return mBluetoothAdapter.isEnabled();
    }

    public void toggleBTStatus(){
        boolean curr_status = getBTStatus();

        setBTStatus(!curr_status);
    }

    public void setBTStatus(boolean NewStatus){
        boolean curr_status = getBTStatus();

        if (mBluetoothAdapter.isEnabled() != NewStatus){
            if(NewStatus){
                mBluetoothAdapter.enable();
            }else {
                mBluetoothAdapter.disable();
            }
        }
    }

}
