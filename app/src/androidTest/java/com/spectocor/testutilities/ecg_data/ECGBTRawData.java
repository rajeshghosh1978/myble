package com.spectocor.testutilities.ecg_data;

import com.spectocor.testutilities.utils.ArrayUtility;
import com.spectocor.testutilities.utils.NumberUtils;

import java.sql.Array;
import java.util.List;
import android.util.Log;

import junit.framework.Assert;

/**
 * Created by qwang on 2/15/2016.
 *
 * ECG device bluetooth raw data
 */
public class ECGBTRawData {

    private static String TAG = "ECGBTRawData";

    private static int mSampleRate = 250; // default sample rate

    private int[] channel1; // ECG channel 1 data
    private int[] channel2; // ECG channel 2 data

    private int[] clean_data_channel; // // ECG clean channel data without noise

    public ECGBTRawData(List<Integer> Channel1,List<Integer> Channel2,List<Integer> CleanDataChannel){
        channel1 = ArrayUtility.IntegerArray2IntArray(Channel1);
        channel2 = ArrayUtility.IntegerArray2IntArray(Channel2);
        clean_data_channel = ArrayUtility.IntegerArray2IntArray(CleanDataChannel);
    }

    public ECGBTRawData(int[] Channel1,int[] Channel2,int[] CleanDataChannel){
        channel1 = Channel1;
        channel2 = Channel2;
        clean_data_channel = CleanDataChannel;
    }

    public ECGBTRawData(int[] Channel1,int[] Channel2){
        channel1 = Channel1;
        channel2 = Channel2;
        clean_data_channel = null;
    }

    public ECGBTRawData(int[] Channel1){
        channel1 = Channel1;
        channel2 = null;
        clean_data_channel = null;
    }

    /**
     * check int array is correct ECG BT raw data or not
     *
     * @param Channel, one ECG channel data
     * @return, True/False
     */
    public static boolean IsECGBTRawData(int[] Channel){
        boolean rc = false;

        if(Channel != null){

            if(Channel.length == mSampleRate){
                rc = true;
            }
        }
        return rc;
    }

    /**
     * get channel data based on channel index
     * 0 --- clean data
     * 1 --- channel 1
     * 2 --- channel 2
     * otherwise ---- null
     *
     * @param ChannelNum
     * @return
     */
    public int[] getChannelData(int ChannelNum){
        int[] channel_data = null;

        switch (ChannelNum){
            case 1:
                channel_data = channel1;
                break;
            case 2:
                channel_data = channel2;
                break;
            case 0:
                channel_data = clean_data_channel;
                break;
            default:
                channel_data = null;
        }

        return channel_data;
    }

    /**
     * check have channel 1 data or not
     * @return
     */
    public boolean hasChannel1Data(){

        return (channel1 != null && channel1.length > 0?true:false);

    }

    /**
     * check have channel 2 data or not
     * @return
     */
    public boolean hasChannel2Data(){

        return (channel2 != null && channel2.length > 0?true:false);

    }

    /**
     * check has clean data channel or not
     * @return
     */
    public boolean hasCleanDataChannel(){

        return (clean_data_channel != null && clean_data_channel.length > 0? true:false);
    }

    public void dumpData(int Size){
        int channelLen = channel1.length;

        String msg = "";

        if(channelLen > 0){
            Log.d(TAG,"Total:" + channelLen );
            for(int i = 0; i < channelLen && i < Size ; ++i ){
                msg = "sample index:" + i + ":";
                if(clean_data_channel != null && clean_data_channel.length > 0 && i < Size){
                    msg += clean_data_channel[i];
                    msg += ",";
                }
                if(channel1 != null && channel1.length > 0 && i < Size){
                    msg += channel1[i];
                    msg += ",";
                }
                if(channel2 != null && channel2.length > 0 && i < Size){
                    msg += channel2[i];
                    msg += ",";
                }
                Log.d(TAG,msg);
            }
        }

    }

    public static Integer decodeInteger(String Value){
        Integer integer;
        Float aFloat;

        if(NumberUtils.isInt(Value)){

            integer = new Integer(Value);

        }
        else
        {
            if(NumberUtils.isFloat(Value)){
                aFloat = new Float(Value);

                integer = new Integer((int)(aFloat.floatValue() * 1000000));
            }else {
                integer = null;
                Assert.fail("found new data type at ECG data:"+Value);
            }

        }

        return integer;
    }

    public static short[] normalization(int[] EcgRawData){

        int maxi ;

        int normalization_factor;

        assert(EcgRawData != null&& EcgRawData.length > 0);

        maxi = ArrayUtility.max(EcgRawData);

        if(maxi >= Short.MAX_VALUE){

            //normalization_factor = (int)(((float)maxi / Short.MAX_VALUE) + 1);
            normalization_factor = 643000;

        }else
            normalization_factor = 1;

        short[] ecg_normalization_data = new short[EcgRawData.length];

        for(int i =  0; i < EcgRawData.length ; i++){
            if(normalization_factor == 1){
                ecg_normalization_data[i] = (short)EcgRawData[i];
            }else
            {
                //ecg_normalization_data[i] = (short)(EcgRawData[i] / normalization_factor);
                ecg_normalization_data[i] = (short)(EcgRawData[i] - normalization_factor);
            }
        }

        return ecg_normalization_data;
    }


}
