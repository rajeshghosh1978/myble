package com.spectocor.testutilities;

/**
 * This class provides data structure to keep log information
 * <p/>
 * Created by qwang on 11/24/2015.
 */
public class CompressionLogItem {
    private String m_db_record;  // ECG data record
    private int m_org_data_size; // original data file size
    private int m_cmp_data_size; // compression data file size
    private long m_cmp_duration; // time for compression data
    private long m_dcp_duration; // time for decompression data
    private String m_pass_fail;  // pass or fail for test
    private String m_perf_pf;    // pass or fail for performance test

    // a new log item
    public CompressionLogItem(String db_record, int org_data_size, int cmp_data_size, long cmp_duration, long dcp_duration, String pass_fail) {
        m_db_record = db_record;
        m_org_data_size = org_data_size;
        m_cmp_data_size = cmp_data_size;
        m_cmp_duration = cmp_duration;
        m_dcp_duration = dcp_duration;
        m_pass_fail = pass_fail;
        m_perf_pf = "no info";
    }

    // a new log item
    public CompressionLogItem(String db_record, int org_data_size, int cmp_data_size, long cmp_duration, long dcp_duration, String pass_fail, String perf_pf) {
        m_db_record = db_record;
        m_org_data_size = org_data_size;
        m_cmp_data_size = cmp_data_size;
        m_cmp_duration = cmp_duration;
        m_dcp_duration = dcp_duration;
        m_pass_fail = pass_fail;
        m_perf_pf = perf_pf;
    }
    // data record information
    public String get_db_record() {
        return m_db_record.substring(m_db_record.lastIndexOf("\\") + 1, m_db_record.length());
    }

    // get orignal data size
    public int get_org_data_size() {
        return m_org_data_size;
    }

    // get compression data size
    public int get_cmp_data_size() {
        return m_cmp_data_size;
    }

    // get compression executing time
    public long get_cmp_duration() {
        return m_cmp_duration;
    }

    // get decompression executing time
    public long get_dcp_duration() {
        return m_dcp_duration;
    }

    // get compression ratio
    public float get_cmp_ratio() {
        float ratio;

        if (m_org_data_size == 0) {
            ratio = 0f;
        } else
            ratio = (float) get_cmp_data_size() / (float) get_org_data_size();

        return ratio;
    }

    // get test pass or fail result
    public String get_pass_fail() {
        return m_pass_fail;
    }

    // get perforamnce test result
    public String get_perf_pf() {
        return m_perf_pf;
    }
}
