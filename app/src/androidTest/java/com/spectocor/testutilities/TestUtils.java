package com.spectocor.testutilities;


import android.provider.MediaStore;

import com.google.gson.Gson;
import com.spectocor.testutilities.utils.FileIo;

import org.junit.Assert;

import java.io.IOException;
import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.nio.file.StandardOpenOption;
import java.io.File;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class TestUtils {

    public static <T> void AssertEqualArray(T[] actual, T[] expected) {
        if (actual != expected) // equal reference or null reference
        {
            Assert.assertEquals(actual.length, expected.length);

            for (int i = 0; i < expected.length; i++) {
                Assert.assertEquals(actual[i], actual[i]);
            }
        }
    }


    public static void AssertEqualArray(byte[] actual, byte[] expected) {
        if (actual != expected) // equal reference or null reference
        {
            Assert.assertEquals(actual.length, expected.length);

            for (int i = 0; i < expected.length; i++) {
                Assert.assertEquals(actual[i], actual[i]);
            }
        }
    }



    public static void writeDoublesToFile(double[] arr, String filepath) {
        String str = "";
        for (int j = 0; j < arr.length; j++) {

            str += arr[j] + "\n";

        }

        str.trim();


        try {
            //Files.write(Paths.get(filepath), str.getBytes(), StandardOpenOption.CREATE);
            FileIo.writeAllLines(filepath, str.getBytes(), FileIo.CREATE);
        } catch (IOException e) {
            e.getStackTrace();
        }


    }

    public static void writeIntsToFile(int[] arr, String filepath) {
        String str = "";
        for (int j = 0; j < arr.length; j++) {

            str += arr[j] + "\n";

        }

        str.trim();


        try {
            //Files.write(Paths.get(filepath), str.getBytes(), StandardOpenOption.CREATE);
            FileIo.writeAllLines(filepath, str.getBytes(), FileIo.CREATE);
        } catch (IOException e) {
            e.getStackTrace();
        }


    }

    public static double[] readDoublesFromFile(String filePath) {
        double[] signal;
        ArrayList<String> inputLines;
        try {

            //inputLines = (ArrayList<String>) Files.readAllLines(Paths.get(filePath), Charset.defaultCharset());
            inputLines = (ArrayList<String>) FileIo.readAllLines(filePath, Charset.defaultCharset());
            signal = new double[inputLines.size()];

            for (int j = 0; j < inputLines.size(); j++) {
                signal[j] = Double.parseDouble(inputLines.get(j));
            }

        } catch (IOException e) {
            e.printStackTrace();
            signal = null;
        }

        return signal;

    }


    public static void AssertEqualsJson(Object expected, Object actual) {
        final Gson gson = new Gson();

        Assert.assertEquals(gson.toJson(expected), gson.toJson(actual));
    }



    /*
    * author: Qian
    * date: 11/24/2015
    * function:
    *   read short data from text file to init short array
    * Input: text file name
    * Output: short array
     */
    public static short[] readShortDataFromFile(String FilePath) {
        short[] data_array = null;
        ArrayList<String> inputLines;
        try {

            //inputLines = (ArrayList<String>) Files.readAllLines(Paths.get(FilePath), Charset.defaultCharset());
            inputLines = (ArrayList<String>) FileIo.readAllLines(FilePath, Charset.defaultCharset());

            data_array = new short[inputLines.size()];

            for (int j = 0; j < inputLines.size(); j++) {
                data_array[j] = Short.parseShort(inputLines.get(j));
            }

        } catch (IOException e) {

            e.printStackTrace();
            return null;
        }

        return data_array;
    }

    /*
   * author: Qian
   * date: 11/24/2015
   * function:
   *   read record list from text file to init string array
   * Input: record text file name
   * Output: string array
    */
    public static String[] readRecordListFromFile(String FilePath) {
        String[] records = null;
        ArrayList<String> inputLines;

        //Path partent_path;
        //Path file_path;
        String partent_path;
        String file_path;
        String curr_record_name;
        File tf;

        try {
            //file_path = Paths.get(FilePath);
            file_path = FilePath;
            tf = new File(file_path);

            partent_path = tf.getParent();

            //inputLines = (ArrayList<String>) Files.readAllLines(file_path, Charset.defaultCharset());
            inputLines = (ArrayList<String>) FileIo.readAllLines(file_path, Charset.defaultCharset());
            //URL data_file_path = new URL(FilePath);
            //inputLines = (ArrayList<String>) Files.readAllLines(Paths.get(data_file_path), Charset.defaultCharset());

            records = new String[inputLines.size()];

            for (int j = 0; j < inputLines.size(); j++) {
                curr_record_name = inputLines.get(j);

                curr_record_name = curr_record_name.substring(curr_record_name.lastIndexOf("\\") + 1, curr_record_name.length());


                records[j] = partent_path.toString() + "\\" + curr_record_name;
            }

        } catch (IOException e) {

            e.printStackTrace();
            return null;
        }

        return records;
    }
}
