package com.spectocor.testutilities.session.api;

/**
 * Created by qwang on 3/11/2016.
 */
public class RESTAPIParaName {

    public static String ActivationCode            = "activationCode";
    public static String PatientFirstName          = "patientFirstName";
    public static String PatientLastName           = "patientLastName";
    public static String HolterPlus                = "holterPlus";
    public static String FacilityID                = "facilityId";
    public static String EnrollmentDate            = "enrollmentDate";

    public static String SessionID                 = "sessionId";
    public static String DataChunkFile             = "file";
    public static String EcgID                     = "ecgId";
    public static String PDAID                     = "deviceId";
    public static String ValFile                   = "data.comp";
    public static String ChunkStartTimestamp       = "chunkStartTimestamp";


}
