package com.spectocor.testutilities.session.api;

import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.http.HttpManager;
import com.spectocor.testutilities.TestEnvGlobalConst;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import android.util.Log;

/**
 * Created by qwang on 3/11/2016.
 * <p/>
 * call REST API for testing purpose
 * <p/>
 * https://gateway.spectocor.com/en/v0/session/end
 * <p/>
 * <p/>
 * start session with the following information
 * <p/>
 * {
 * "sessionId":100005824
 * }
 */
public class SessionEndRESTAPI {

    private static String TAG = "SessionEndRESTAPI";


    private static String SERVER_ADDRESS = TestEnvGlobalConst.SSL_SERVER_ADDRESS; //  TestEnvGlobalConst.SERVER_ADDRESS ;
    //private static String SSL_SERVER_ADDRESS                = TestEnvGlobalConst.SSL_SERVER_ADDRESS;
    private static String FAIL_SERVER_ADDRESS = TestEnvGlobalConst.FAIL_SERVER_ADDRESS; // fake server IP for test
    private static String REST_URL_SESSION_END = TestEnvGlobalConst.REST_URL_SESSION_END;


    ////////////////////////////////////////////////////////////
    // public API for session end REST API
    ////////////////////////////////////////////////////////////

    /**
     * call REST API to end session based on session id
     *
     * @param sessionID, session id
     * @return response status for API, Success/Error
     */
    public static String sessionEnd(final String sessionID) {
        String restResponse = "";

        try {
            Response response = SendingSessionEndRequestCall(sessionID);

            //Log.d(TAG, ObjectUtils.toJson(response));

            // return result
            if (checkResponseSuccess(response)) {
                restResponse = RESTAPIResponseMessage.SUCCESS;
            } else
                restResponse = RESTAPIResponseMessage.ERROR;

        } catch (IOException e) {
            e.printStackTrace();
            restResponse = RESTAPIResponseMessage.ERROR;
        }

        return restResponse;
    }

    /**
     * @param sessionID
     * @return
     * @throws IOException
     */
    public static Response SendingSessionEndRequestCall(final String sessionID) throws IOException {
        return SendingSessionEndRequestCall(SERVER_ADDRESS, REST_URL_SESSION_END, sessionID);
    }

    public static Response SendingSessionEndRequestCall(final String WebServiceAddress, final String ServiceURL,
                                                        final String sessionID) throws IOException {

        Response response;

        // build request
        Request request = buildSessionEndRequest(WebServiceAddress, ServiceURL, sessionID);

        OkHttpClient httpClient = HttpManager.getInstance().getHttpClient();

        // send request
        // get response
        response = httpClient.newCall(request).execute();

        return response;
    }

    /**
     * check response contain success string at message attribute or not
     *
     * @param response
     * @return
     * @throws IOException
     */
    public static boolean checkResponseSuccess(Response response) throws IOException {

        boolean rc;

        JSONObject object = null;
        String message = "";

        try {
            object = new JSONObject(response.body().string());
            message = object.getString("message");
            Log.d(TAG, "response message:" + message + ":code:" + object.getString("code"));
            if (object.getString("message").toLowerCase().compareTo("success") == 0)
                rc = true;
            else
                rc = false;
        } catch (JSONException e) {
            e.printStackTrace();
            com.spectocor.micor.mobileapp.common.Log.e(TAG + ":" + "Response JSON Exception", e.getMessage());
            rc = false;
        } finally {
            if (object != null) {
                com.spectocor.micor.mobileapp.common.Log.i(TAG + ":" + "Response JSON Message", message);
            }
        }

        return rc;

    }

    /**
     * @param WebServiceAddress
     * @param ServiceURL
     * @param sessionID
     * @return
     */
    public static Request buildSessionEndRequest(String WebServiceAddress, String ServiceURL, final String sessionID) {
        // build request body
        MultipartBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(RESTAPIParaName.SessionID, sessionID)
                .build();

        // build request
        Request request = new Request.Builder()
                //.header(RESTAPIHeadParaName.Authorization, TestEnvGlobalConst.BASIC_AUTHORIZATION)
                //.header(RESTAPIHeadParaName.ContentType, TestEnvGlobalConst.CONTENT_TYPE)
                .url(WebServiceAddress + ServiceURL)
                .post(requestBody)
                .build();

        return request;
    }

    /**
     * reset default server
     *
     * @param serverName
     */
    public static void setServerName(final String serverName) {

        SERVER_ADDRESS = serverName;

    }
}
