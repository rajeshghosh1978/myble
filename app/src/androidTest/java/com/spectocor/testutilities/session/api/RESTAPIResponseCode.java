package com.spectocor.testutilities.session.api;

/**
 * Created by qwang on 3/11/2016.
 */
public class RESTAPIResponseCode {

    public static int SUCCESS = 200;
    public static int SESSION_EXPIRED = 401;
    public static int DEVICE_NO_ACTIVATION = 410;
    public static int ERROR = 400;
    public static int INTERNAL_ERROR = 500;
}
