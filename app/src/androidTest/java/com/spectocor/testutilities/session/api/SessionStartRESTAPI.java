package com.spectocor.testutilities.session.api;

import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.http.HttpManager;
import com.spectocor.testutilities.TestEnvGlobalConst;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 2/29/2016.
 *
 * call REST API for testing purpose
 *
 * https://gateway.spectocor.com/en/v1/session/start
 *
 *
 * start session with the following information
 *
 * "sessionId":"12345",
 * "startTimestamp":1438378778000,
 * "ecgId":"<ecg device identifier>",
 * "mobileId":"<mobile device identifier>",
 * "ecgChunkSize":"300000",
 * "samplingRate":"250"
 *
 * copy of source code from SessionActivationApi.startSessionCall
 */
public class SessionStartRESTAPI {

    private static String TAG = "SessionStartRESTAPI";

    //private static String mSessionStatus1234JSON = "{\"ecgBtConfirmed\":true,\"ecgBtMac\":\"B0:B4:48:C4:A9:89\",\"ecgBtName\":\"STL ECG 201\",\"ecgChosen\":false,\"ecgChosenAtShipping\":true,\"enrollmentConfirmed\":true,\"enrollmentDate\":\"2016-01-19\",\"enrollmentId\":\"\",\"facilityId\":\"\",\"facilityName\":\"Facility A\",\"heartBeatDetectionTestComplete\":true,\"heartRateDetected\":true,\"isSalesAgent\":false,\"patientConfirmed\":true,\"patientName\":\"John D\",\"sessionActivationDate\":\"2016-02-19T20:07:14.131Z\",\"sessionActive\":true,\"sessionId\":1,\"sessionReactivation\":true,\"sessionTerminated\":false,\"signalDetected\":true,\"summaryConfirmed\":false}";
    private static String mSessionStatus1234JSON = "{\"ecgBtConfirmed\":true,\"ecgBtMac\":\"B0:B4:48:C4:A9:89\",\"ecgBtName\":\"STL ECG 201\",\"ecgChosen\":false,\"ecgChosenAtShipping\":true,\"enrollmentConfirmed\":true,\"enrollmentDate\":\"2016-01-19\",\"enrollmentId\":\"\",\"facilityId\":\"\",\"facilityName\":\"Facility A\",\"heartBeatDetectionTestComplete\":true,\"heartRateDetected\":true,\"isSalesAgent\":false,\"patientConfirmed\":true,\"patientName\":\"John D\",\"sessionActivationDate\":\"2016-02-19T20:07:14.131Z\",\"sessionActive\":true,\"sessionId\":1,\"sessionReactivation\":false,\"sessionTerminated\":false,\"signalDetected\":true,\"summaryConfirmed\":false}";
    private static String mSessionStatusReactivationJSON = "{\"ecgBtConfirmed\":true,\"ecgBtMac\":\"B0:B4:48:C4:A9:89\",\"ecgBtName\":\"STL ECG 201\",\"ecgChosen\":false,\"ecgChosenAtShipping\":true,\"enrollmentConfirmed\":true,\"enrollmentDate\":\"2016-01-19\",\"enrollmentId\":\"\",\"facilityId\":\"\",\"facilityName\":\"Facility A\",\"heartBeatDetectionTestComplete\":true,\"heartRateDetected\":true,\"isSalesAgent\":false,\"patientConfirmed\":true,\"patientName\":\"John D\",\"sessionActivationDate\":\"2016-02-19T20:07:14.131Z\",\"sessionActive\":true,\"sessionId\":1,\"sessionReactivation\":true,\"sessionTerminated\":false,\"signalDetected\":true,\"summaryConfirmed\":false}";


    public static String SUCCESS                     = "Success";
    public static String ERROR                       = "Error";

    public static String SERVER_ADDRESS                      = TestEnvGlobalConst.SERVER_ADDRESS ;
    public static String SSL_SERVER_ADDRESS                  = TestEnvGlobalConst.SSL_SERVER_ADDRESS;
    public static String FAIL_SERVER_ADDRESS                 = TestEnvGlobalConst.FAIL_SERVER_ADDRESS; // fake server IP for test
    public static String REST_URL_SENDING_ECG_DATA_CHUNK     = TestEnvGlobalConst.REST_URL_SENDING_ECG_DATA_CHUNK;
    private static int mDefaultSampleRate                    = TestEnvGlobalConst.SAMPLE_RATE;
    private static int mDeviceChunkSize                      = TestEnvGlobalConst.DEVICE_CHUNK_SIZE;

    public static String ServiceParaNameSessionID                 = "sessionId";
    public static String ServiceParaNameFile                      = "file";
    public static String ServiceNameEcgId                         = "ecgId";
    public static String ServiceNameDeviceId                      = "deviceId";
    public static String ServiceParaValFile                       = "data.comp";
    public static String ServiceParaNameChunkStartTimestamp       = "chunkStartTimestamp";

    private static int mDefaultSessionID = 1;//1234;
    private static int mDefaultChunkSize = 1024;
    private static int mDefaultMaxiSessionID = 987654;
    private static int mDefaultTestChunkNum = TestEnvGlobalConst.DEFAULT_RUN_TEST_TIME;
    private static String mDefaultEcg = DeviceIdentifiers.getEcgId();  // "SPEC-SD5F855GHJ";
    private static String mDefaultPda = DeviceIdentifiers.getPdaId();  // "AHSD4F9F4DD4SD4FF9";



    public static Response SendingSessionStartRequestCall(String WebServiceAddress,String ServiceURL ,final String sessionID, final String ecgId,
                                                          final String deviceId, boolean deviceReplacement) throws IOException {

        Response response;

        // build request
        Request request = buildSessionStartRequest(WebServiceAddress, ServiceURL, sessionID,ecgId,deviceId,deviceReplacement);

        OkHttpClient httpClient = HttpManager.getInstance().getHttpClient();

        // send request
        // get response
        response = httpClient.newCall(request).execute();

        return response;
    }

    /**
     * check response contain success string at message attribute or not
     * @param response
     * @return
     * @throws IOException
     */
    public static boolean checkResponseSuccess(Response response) throws IOException{

        boolean rc ;

        JSONObject object = null;
        String message = "";

        try{
            object = new JSONObject(response.body().string());
            message = object.getString("message");
            if(object.getString("message").toLowerCase().compareTo("success") == 0)
                rc = true;
            else
                rc = false;
        }catch (JSONException e) {
            e.printStackTrace();
            com.spectocor.micor.mobileapp.common.Log.e(TAG + ":" + "Response JSON Exception", e.getMessage());
            rc = false;
        }finally {
            if(object != null){
                com.spectocor.micor.mobileapp.common.Log.i(TAG + ":" + "Response JSON Message", message);
            }
        }

        return rc;

    }

    /**
     * build ECG data chunk request
     *
     * @param WebServiceAddress
     * @param ServiceURL
     * @param sessionID
     * @return
     */
    public static Request buildSessionStartRequest(String WebServiceAddress,String ServiceURL ,final String sessionID, final String ecgId,
                                                   final String deviceId, boolean deviceReplacement){
        // build request body
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("sessionId", "" + Integer.parseInt(sessionID))
                .addFormDataPart("startTimestamp", "" + System.currentTimeMillis())
                .addFormDataPart("ecgId", ecgId)
                .addFormDataPart("deviceId", deviceId)
                .addFormDataPart("samplingRate", mDefaultSampleRate + "")
                .addFormDataPart("deviceChunkSize", mDefaultChunkSize + "")
                .addFormDataPart("deviceReplacement", "" + deviceReplacement)
                .build();

        // build request
        Request request = new Request.Builder()
                .header("Authorization", "Basic c2lyYWo6dGVzdDEyMw==")
                .url(WebServiceAddress + ServiceURL)
                .post(requestBody)
                .build();

        return request;
    }

    public static void serServerName(final String serverName){

        SERVER_ADDRESS = serverName;

    }
    public static void setECGDeviceID(final String ecgDeviceID){

        mDefaultEcg = ecgDeviceID;
    }

    public static void setPDADeviceID(final String pdaDeviceID){
        mDefaultPda = pdaDeviceID;
    }

}
