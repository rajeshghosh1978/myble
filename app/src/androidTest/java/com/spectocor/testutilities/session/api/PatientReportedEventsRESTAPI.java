package com.spectocor.testutilities.session.api;

import android.content.Context;

import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.http.HttpManager;
import com.spectocor.micor.mobileapp.patientreportedevents.EnumPatientActivityEventType;
import com.spectocor.micor.mobileapp.patientreportedevents.EnumPatientReportedEventType;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientEvent;
import com.spectocor.testutilities.TestEnvGlobalConst;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by qwang on 2/26/2016.
 *
 * call REST API for testing purpose
 *
 * report patient trigger events
 *
 */
public class PatientReportedEventsRESTAPI {

    private static String TAG = "PatientReportedEventsRESTAPI";

    //http://project.spectocor.com:8090/confluence/display/DCS/WEB+API+Documentation

    public static String SUCCESS = "Success";
    public static String ERROR = "Error";

    public static String SERVER_ADDRESS = TestEnvGlobalConst.SERVER_ADDRESS;
    public static String SSL_SERVER_ADDRESS = TestEnvGlobalConst.SSL_SERVER_ADDRESS;
    public static String FAIL_SERVER_ADDRESS = TestEnvGlobalConst.FAIL_SERVER_ADDRESS; // fake server IP for test
    public static String REST_URL_PATIENT_LOG = TestEnvGlobalConst.REST_URL_PATIENT_LOG;

    public static String ServiceParaNameSessionID = "sessionId";
    public static String ServiceParaNameFile = "file";
    public static String ServiceNameEcgId = "ecgId";
    public static String ServiceNameDeviceId = "deviceId";
    public static String ServiceParaValFile = "data.comp";
    public static String ServiceParaNameChunkStartTimestamp = "chunkStartTimestamp";

    private static long mDefaultSessionID = 1;
    private static int mDefaultChunkSize = 1024;
    private static int mDefaultMaxiSessionID = 987654;
    private static int mDefaultTestChunkNum = TestEnvGlobalConst.DEFAULT_RUN_TEST_TIME;
    private static String mDefaultEcg = DeviceIdentifiers.getEcgId();  // "SPEC-SD5F855GHJ";
    private static String mDefaultPda = DeviceIdentifiers.getPdaId();  // "AHSD4F9F4DD4SD4FF9";

    public static Response reportWithoutSymptoms(String webServiceAddress, String serviceURL, Context context) throws IOException {

        Response response;

        // build request
        Request request = buildReportWithoutSymptomsRequest(webServiceAddress, serviceURL, context);

        // get response
        response = HttpManager.getInstance().getHttpClient().newCall(request).execute();

        return response;
    }

    public static Response reportWithSymptoms(String webServiceAddress, String serviceURL, String symptoms, Context context) throws IOException {

        Response response;

        // build request
        Request request = buildReportWithSymptomsRequest(webServiceAddress, serviceURL, symptoms, context);

        // get response
        response = HttpManager.getInstance().getHttpClient().newCall(request).execute();

        return response;
    }

    /**
     * build patient trigger event request
     *
     * @param webServiceAddress
     * @param serviceURL
     * @param symptoms
     * @return
     */
    public static Request buildReportWithSymptomsRequest(String webServiceAddress, String serviceURL, String symptoms, Context context) {
        // build request body

        String activeSessionID = SessionStateAccess.getCurrentSessionId(context) + ""; //preferenceHelper.getActiveSessionId();

        String[] totalDiagnosticSplit = symptoms.split(";");
        String[] symptomsSplit = totalDiagnosticSplit[0].split(",");

        Request request;

        try {
            JSONArray logsArray = new JSONArray();

            for (int i = 0; i < symptomsSplit.length; i++) {
                JSONObject log = new JSONObject();
                int symptomValue = EnumPatientReportedEventType.valueOf(symptomsSplit[i].replace(" ", "")).getValue();
                log.put("eventType", symptomValue + "");
                if (totalDiagnosticSplit.length > 1) {
                    log.put("activityType", EnumPatientActivityEventType.valueOf(totalDiagnosticSplit[1].replace(" ", "")).getValue());
                } else
                    log.put("activityType", "4");
                log.put("eventTimestamp", System.currentTimeMillis());
                // TODO pass unique event counter for every sessions here (like it shall restart from 1 when ever there is new session id)
                log.put("eventMessage", "1");
                logsArray.put(log);
            }

            Log.v(TAG, "Log array in string: " + logsArray.toString());

            PatientEvent patientEvent = new PatientEvent();
            patientEvent.setSessionId(Long.parseLong(activeSessionID));
            patientEvent.setInsertTimestamp(System.currentTimeMillis());
            patientEvent.setTransmittedTimestamp(-1);
            patientEvent.setData(logsArray.toString());

            Log.d(TAG, "sessionId: " + Integer.parseInt(activeSessionID + ""));
            Log.d(TAG, "log:" + logsArray.toString());
            Log.d(TAG, "ecgId: " + mDefaultEcg);
            Log.d(TAG, "deviceId: " + mDefaultPda);

            RequestBody requestBody = new MultipartBody.Builder()
                    .addFormDataPart("sessionId", activeSessionID)
                    .addFormDataPart("log", logsArray.toString())
                    .addFormDataPart("ecgId", mDefaultEcg)
                    .addFormDataPart("deviceId", mDefaultPda)
                    .setType(MultipartBody.FORM)
                    .build();

            request = new Request.Builder()
                    .post(requestBody)
                    .url(webServiceAddress + serviceURL)
                    .build();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG, "input symptoms string is not correct format:" + symptoms);
            request = null;
        }

        return request;
    }

    /**
     * build patient trigger event request without symptom
     *
     * @param webServiceAddress
     * @param serviceURL
     * @return
     */
    public static Request buildReportWithoutSymptomsRequest(String webServiceAddress, String serviceURL, Context context) {
        // build request body

        String activeSessionID = SessionStateAccess.getCurrentSessionId(context) + "";
        Request request;

        try {
            JSONObject logJson = new JSONObject();
            logJson.put("eventType", "101");
            logJson.put("activityType", "4");
            logJson.put("eventTimestamp", System.currentTimeMillis() + "");
            logJson.put("eventMessage", "No Symptoms");

            JSONArray logJsonArray = new JSONArray();
            logJsonArray.put(logJson);

            PatientEvent patientEvent = new PatientEvent();
            patientEvent.setSessionId(Long.parseLong(activeSessionID));
            patientEvent.setInsertTimestamp(System.currentTimeMillis());
            patientEvent.setTransmittedTimestamp(-1);
            patientEvent.setData(logJsonArray.toString());

            Log.d(TAG, "sessionId: " + Integer.parseInt(activeSessionID + ""));
            Log.d(TAG, "log:" + logJsonArray.toString());
            Log.d(TAG, "ecgId: " + mDefaultEcg);
            Log.d(TAG, "deviceId: " + mDefaultPda);

            RequestBody requestBody = new MultipartBody.Builder()
                    .addFormDataPart("sessionId", Integer.parseInt(activeSessionID)+"")
                    .addFormDataPart("log", logJsonArray.toString())
                    .addFormDataPart("ecgId", mDefaultEcg)
                    .addFormDataPart("deviceId", mDefaultPda)
                    .setType(MultipartBody.FORM)
                    .build();

            request = new Request.Builder()
                    .post(requestBody)
                    .url(webServiceAddress+serviceURL)
                    .build();

        } catch (JSONException e) {
            e.printStackTrace();
            request = null;
        }
        return request;
    }

    public static void setECGDeviceID(final String ecgDeviceID){
        mDefaultEcg = ecgDeviceID;
    }

    public static void setPDADeviceID(final String pdaDeviceID){
        mDefaultPda = pdaDeviceID;
    }


}