package com.spectocor.testutilities.session.api;

import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.http.HttpManager;
import com.spectocor.micor.mobileapp.sessionactivation.api.EnrollmentInfo;
import com.spectocor.testutilities.TestEnvGlobalConst;

import junit.framework.Assert;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.net.ssl.SSLProtocolException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by qwang on 2/25/2016.
 *
 * call REST API for testing purpose
 *
 * call ECG Data Chunk Sending REST API
 */
public class SendingECGDataChunkRESTAPI {

    private static String TAG = "SendingECGDataChunkRESTAPI";

    //http://project.spectocor.com:8090/confluence/display/DCS/WEB+API+Documentation

    public static String SUCCESS                     = "Success";
    public static String ERROR                       = "Error";

    public static String SERVER_ADDRESS                      = TestEnvGlobalConst.SERVER_ADDRESS ;
    public static String SSL_SERVER_ADDRESS                  = TestEnvGlobalConst.SSL_SERVER_ADDRESS;
    public static String FAIL_SERVER_ADDRESS                 = TestEnvGlobalConst.FAIL_SERVER_ADDRESS; // fake server IP for test
    public static String REST_URL_SENDING_ECG_DATA_CHUNK     = TestEnvGlobalConst.REST_URL_SENDING_ECG_DATA_CHUNK;

    public static String ServiceParaNameSessionID                 = "sessionId";
    public static String ServiceParaNameFile                      = "file";
    public static String ServiceNameEcgId                         = "ecgId";
    public static String ServiceNameDeviceId                      = "deviceId";
    public static String ServiceParaValFile                       = "data.comp";
    public static String ServiceParaNameChunkStartTimestamp       = "chunkStartTimestamp";

    private static int mDefaultSessionID = 1;//1234;
    private static int mDefaultChunkSize = 1024;
    private static int mDefaultMaxiSessionID = 987654;
    private static int mDefaultTestChunkNum = TestEnvGlobalConst.DEFAULT_RUN_TEST_TIME;
    private static String mDefaultEcg = DeviceIdentifiers.getEcgId();  // "SPEC-SD5F855GHJ";
    private static String mDefaultPda = DeviceIdentifiers.getPdaId();  // "AHSD4F9F4DD4SD4FF9";

    /**
     * send ECG data
     *
     * @param WebServiceAddress
     * @param ServiceURL
     * @param EcgDataChunkObj
     * @return
     * @throws IOException
     */
    public static Response SendingEcgChunkRequestCall(String WebServiceAddress,String ServiceURL ,EcgDataChunk EcgDataChunkObj) throws IOException{

        Response response;

        // build request
        Request request = buildEcgDataChunkRequest(WebServiceAddress,ServiceURL ,EcgDataChunkObj);

        OkHttpClient httpClient = HttpManager.getInstance().getHttpClient();

        // send request
        // get response
        response = httpClient.newCall(request).execute();


        return response;
    }

    /**
     * send ECG data chunk without ECG device ID and PDA ID
     *
     * @param WebServiceAddress
     * @param ServiceURL
     * @param EcgDataChunkObj
     * @return
     * @throws IOException
     */
    public static Response SendingEcgChunkRequestCallWithoutECGDeviceID(String WebServiceAddress,String ServiceURL ,EcgDataChunk EcgDataChunkObj) throws IOException{

        Response response;

        // build request
        Request request = buildEcgDataChunkRequestWithoutECGDevice(WebServiceAddress, ServiceURL, EcgDataChunkObj);

        OkHttpClient httpClient = HttpManager.getInstance().getHttpClient();

        // send request
        // get response
        response = httpClient.newCall(request).execute();


        return response;
    }


    /**
     * check response contain success string at message attribute or not
     * @param response
     * @return
     * @throws IOException
     */
    public static boolean checkResponseSuccess(Response response) throws IOException{

        boolean rc ;

        JSONObject object = null;
        String message = "";

        try{
            object = new JSONObject(response.body().string());
            message = object.getString("message");
            if(object.getString("message").toLowerCase().compareTo("success") == 0)
                rc = true;
            else
                rc = false;
        }catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG + ":" + "Response JSON Exception", e.getMessage());
            rc = false;
        }finally {
            if(object != null){
                Log.i(TAG + ":" + "Response JSON Message",message);
            }
        }

        return rc;

    }

    /**
     * build ECG data chunk request
     *
     * @param WebServiceAddress
     * @param ServiceURL
     * @param EcgDataChunkObj
     * @return
     */
    public static Request buildEcgDataChunkRequest(String WebServiceAddress,String ServiceURL ,EcgDataChunk EcgDataChunkObj){
        // build request body
        RequestBody requestBody = new MultipartBody.Builder()
                .addFormDataPart(ServiceParaNameSessionID, Integer.toString((EcgDataChunkObj.getMonitoringSessionId())))
                .addFormDataPart(ServiceParaNameChunkStartTimestamp, Long.toString(EcgDataChunkObj.getChunkStartDateUnixMs()))
                .addFormDataPart(ServiceNameEcgId, mDefaultEcg)
                .addFormDataPart(ServiceNameDeviceId, mDefaultPda)
                .addFormDataPart(ServiceParaNameFile, ServiceParaValFile,
                        RequestBody.create(
                                MediaType.parse("application/octet-stream"),
                                EcgDataChunkObj.getChunkDataChannelCompressed()))
                .setType(MultipartBody.FORM)
                .build();

        // build request
        Request request = new Request.Builder()
                .header("Authorization", "Basic c2lyYWo6dGVzdDEyMw==")
                .url(WebServiceAddress + ServiceURL)
                .post(requestBody)
                .build();

        return request;
    }

    public static Request buildEcgDataChunkRequestWithoutECGDevice(String WebServiceAddress,String ServiceURL ,EcgDataChunk EcgDataChunkObj){
        // build request body
        RequestBody requestBody = new MultipartBody.Builder()
                .addFormDataPart(ServiceParaNameSessionID, Integer.toString((EcgDataChunkObj.getMonitoringSessionId())))
                .addFormDataPart(ServiceParaNameChunkStartTimestamp, Long.toString(EcgDataChunkObj.getChunkStartDateUnixMs()))
                //.addFormDataPart(ServiceNameEcgId, mDefaultEcg)
                //.addFormDataPart(ServiceNameDeviceId, mDefaultPda)
                .addFormDataPart(ServiceParaNameFile, ServiceParaValFile,
                        RequestBody.create(
                                MediaType.parse("application/octet-stream"),
                                EcgDataChunkObj.getChunkDataChannelCompressed()))
                .setType(MultipartBody.FORM)
                .build();

        // build request
        Request request = new Request.Builder()
                .header("Authorization", "Basic c2lyYWo6dGVzdDEyMw==")
                .url(WebServiceAddress + ServiceURL)
                .post(requestBody)
                .build();

        return request;
    }



    public static void setECGDeviceID(final String ecgDeviceID){
        mDefaultEcg = ecgDeviceID;
    }

    public static void setPDADeviceID(final String pdaDeviceID){
        mDefaultPda = pdaDeviceID;
    }
}
