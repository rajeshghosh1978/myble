package com.spectocor.testutilities.session.api;

import com.spectocor.micor.mobileapp.amain.state.SessionInfoForState;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.http.HttpManager;
import com.spectocor.micor.mobileapp.sessionactivation.api.EnrollmentInfo;
import com.spectocor.micor.mobileapp.sessionactivation.api.SessionInfo;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;

import javax.net.ssl.SSLProtocolException;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by qwang on 2/19/2016.
 *
 * call REST API for testing purpose
 * There is no device id and ecg device id
 *
 */
public class SessionRESTAPI {

    private static String TAG = "SessionRESTAPI";

    //http://project.spectocor.com:8090/confluence/display/DCS/WEB+API+Documentation

    public static String SUCCESS                     = "Success";
    public static String ERROR                       = "Error";

    public static String SERVER_ADDRESS              = "http://207.191.20.80" ;//"https://gateway.spectocor.com";//"http://207.191.20.80" ;       //"http://208.67.179.147";
    public static String SSL_SERVER_ADDRESS          = "https://gateway.spectocor.com";//"http://207.191.20.80" ;       //"http://208.67.179.147";
    public static String FAIL_SERVER_ADDRESS         = "http://208.69.179.147";       // fake server IP for test
    public static String REST_URL_DEVICE_ACTIVATION  = "/en/v1/device/activation";    // Device Authorization


    public static EnrollmentInfo getEnrollmentByActivationCode(final String activationCode) {


        return getEnrollmentByActivationCode(SERVER_ADDRESS, REST_URL_DEVICE_ACTIVATION, activationCode);
    }

    public static EnrollmentInfo getEnrollmentByActivationCode(final String serverAddress, final String restURLDeviceAction,final String activationCode){

        EnrollmentInfo enrollmentInfo = null;

        try {
            //EnrollmentInfo enrInfo = sessionActivationApi.getEnrollmentByActivationCode(activationCode);

            Response response = SendingDeviceActivationRequestCall(serverAddress, restURLDeviceAction, activationCode);

            enrollmentInfo = initEnrollmentInfo(response);

            if(enrollmentInfo != null)
                android.util.Log.d(TAG,enrollmentInfo.toString());

        }catch (SSLProtocolException se){
            Assert.fail(TAG + "SSL handshake aborted");
            enrollmentInfo = null;
        }
        catch (IOException e){
            enrollmentInfo = null;
        }


        return enrollmentInfo;
    }


    /**
     * send device activation request
     *
     * @param webServiceAddress
     * @param serviceURL
     * @param activationCode
     * @return
     * @throws IOException
     */
    public static Response SendingDeviceActivationRequestCall(String webServiceAddress,String serviceURL ,String activationCode) throws IOException {
        Response response;

        // build request
        Request request = buildDeviceActivation(webServiceAddress,serviceURL ,activationCode);

        OkHttpClient httpClient = HttpManager.getInstance().getHttpClient();

        // send request
        // get response
        response = httpClient.newCall(request).execute();

        android.util.Log.d(TAG,response.toString());

        return response;
    }

    /**
     * build device activation request
     *
     * @param webServiceAddress
     * @param serviceURL
     * @param activationCode
     * @return
     */
    public static Request buildDeviceActivation(String webServiceAddress,String serviceURL,String activationCode){
        // build request body
        RequestBody requestBody = new MultipartBody.Builder()
                .addFormDataPart("activationCode", activationCode)
                .setType(MultipartBody.FORM)
                .build();

        // build request
        Request request = new Request.Builder()
                .url(webServiceAddress + serviceURL)
                .post(requestBody)
                .build();

        return request;
    }

    /**
     * check response contain success string at message attribute or not
     * @param response
     * @return
     * @throws IOException
     */
    public static boolean checkResponseSuccess(Response response) throws IOException{

        boolean rc ;

        JSONObject object = null;
        String message = "";

        try{
            object = new JSONObject(response.body().string());
            message = object.getString("message");
            if(object.getString("message").compareTo(SUCCESS) == 0)
                rc = true;
            else
                rc = false;
        }catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG + ":" + "Response JSON Exception", e.getMessage());
            rc = false;
        }finally {
            if(object != null){
                Log.i(TAG + ":" + "Response JSON Message",message);
            }
        }

        return rc;

    }


    /**
     * init enrollment based on response
     *
     * @param response
     * @return
     */
    public static EnrollmentInfo initEnrollmentInfo(Response response){

        EnrollmentInfo enrollmentInfo = null;

        if(response != null) {

            try{
                JSONObject jsonObject = new JSONObject(response.body().string());

                if (jsonObject.getString("code").equals("200")) {
                    enrollmentInfo = new EnrollmentInfo();

                    JSONObject jdata = jsonObject.getJSONObject("data");
                    JSONArray sessions = jdata.getJSONArray("session");

                    enrollmentInfo.setFacilityName(jsonObject.getJSONObject("data").getJSONObject("facility").getString("name"));
                    enrollmentInfo.setFirstName(jsonObject.getJSONObject("data").getJSONObject("patient").getString("firstName"));
                    enrollmentInfo.setLastName(jsonObject.getJSONObject("data").getJSONObject("patient").getString("lastName"));
                    enrollmentInfo.setLastNameInitial(jsonObject.getJSONObject("data").getJSONObject("patient").getString("lastName"));
                    String[] dateSplit = jsonObject.getJSONObject("data").getJSONObject("patient").getString("enrollmentDate").split("T");
                    enrollmentInfo.setEnrollmentDateIso8601(dateSplit[0]);

                    String[] arrdateEnd,arrdateStart;
                    SessionInfoForState si = null;



                    for (int i = 0; i< sessions.length(); i++) {

                        arrdateEnd = sessions.getJSONObject(i).getString("endDateTime").split("T");
                        arrdateStart = sessions.getJSONObject(i).getString("startDateTime").split("T");


                        arrdateEnd[1]=arrdateEnd[1].substring(0, 8);


                        arrdateStart[1]=arrdateStart[1].substring(0,8);


                        si = new SessionInfoForState(
                                Integer.parseInt(sessions.getJSONObject(i).getString("sessionId")),
                                Boolean.parseBoolean(sessions.getJSONObject(i).getString("isExpired")),
                                Long.parseLong(sessions.getJSONObject(i).getString("duration"))
                        );
                        /*
                        si.setEndDateTime(arrdateEnd[0]);// + " " + arrdateEnd[1]
                        si.setStartDateTime(arrdateStart[0]);// + " " + arrdateStart[1]
                        */
                        enrollmentInfo.addSession(si);
                    }
                }
                else {
                    android.util.Log.d(TAG,jsonObject.toString());
                    enrollmentInfo = null;
                }

            }catch (JSONException | IOException | ParseException ioe ) {
                enrollmentInfo = null;
            }
        }

        return enrollmentInfo;
    }
}
