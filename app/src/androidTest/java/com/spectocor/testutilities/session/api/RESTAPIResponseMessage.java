package com.spectocor.testutilities.session.api;

/**
 * Created by qwang on 3/11/2016.
 */
public class RESTAPIResponseMessage {

    public static String ERROR = "Error";

    public static String SUCCESS = "Success";

    public static String EXPIRED = "GONE";

    public static String SESSION_END = "Session Ended";

    public static String SESSION_EXPIRED = "UnauthorizedError";

}
