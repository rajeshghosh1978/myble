package com.spectocor.testutilities.session.api;

import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.http.HttpManager;
import com.spectocor.testutilities.TestEnvGlobalConst;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by qwang on 3/11/2016.
 *
 * call REST API for testing purpose
 *
 * https://gateway.spectocor.com/en/v0/activation/create
 *
 *
 * start session with the following information
 *
 * {
 * "activationCode":7500,
 * "patientFirstName":"Qian7501",
 * "patientLastName":"Wang7501",
 * "holterPlus":"false"
 * }
 *
 *
 *
 */
public class CreateActivationCodeRESTAPI {

    private static String TAG = "CreateActivationCodeRESTAPI";


    private static String SERVER_ADDRESS                      = TestEnvGlobalConst.SSL_SERVER_ADDRESS; //  TestEnvGlobalConst.SERVER_ADDRESS ;
    //private static String SSL_SERVER_ADDRESS                = TestEnvGlobalConst.SSL_SERVER_ADDRESS;
    private static String FAIL_SERVER_ADDRESS                 = TestEnvGlobalConst.FAIL_SERVER_ADDRESS; // fake server IP for test
    private static String REST_URL_CREATE_ACTIVATION          = TestEnvGlobalConst.REST_URL_CREATE_ACTIVATION;


    public static String createActivationCode(final String activationCode, final String patientFirstName,final String patientLastName,
                                              final boolean holsterPlus,final String facilityID,final String enrollmentDate){
        String restResponse = "";

        try {
            Response response = SendingCreateActivationCodeRequestCall(activationCode,patientFirstName,patientLastName,holsterPlus,facilityID,enrollmentDate);

            // return result
            if (checkResponseSuccess(response)) {
                restResponse = RESTAPIResponseMessage.SUCCESS;
            } else
                restResponse = RESTAPIResponseMessage.ERROR;

        } catch (IOException e) {
            e.printStackTrace();
            restResponse = RESTAPIResponseMessage.ERROR;
        }

        return restResponse;
    }

    /**
     * call REST API to create a new session with activation code with default server and URL
     *
     * @param activationCode
     * @param patientFirstName
     * @param patientLastName
     * @param holsterPlus
     * @return
     * @throws IOException
     */
    public static Response SendingCreateActivationCodeRequestCall(final String activationCode, final String patientFirstName,final String patientLastName,
                                                                  final boolean holsterPlus,final String facilityID,final String enrollmentDate) throws IOException {
        return SendingCreateActivationCodeRequestCall(SERVER_ADDRESS,REST_URL_CREATE_ACTIVATION,activationCode,patientFirstName,patientLastName,holsterPlus,facilityID,enrollmentDate);
    }

    /**
     *  call REST API to create a new session with activation code
     *
     * @param WebServiceAddress
     * @param ServiceURL
     * @param activationCode
     * @param patientFirstName
     * @param patientLastName
     * @param holsterPlus
     * @return
     * @throws IOException
     */
    public static Response SendingCreateActivationCodeRequestCall(final String WebServiceAddress,final String ServiceURL,
                                                          final String activationCode, final String patientFirstName,final String patientLastName,
                                                          final boolean holsterPlus,final String facilityID,final String enrollmentDate) throws IOException {

        Response response = null;

        // build request
        Request request = buildCreateActivationCodeRequest(WebServiceAddress, ServiceURL, activationCode, patientFirstName, patientLastName, holsterPlus, facilityID,enrollmentDate);

        OkHttpClient httpClient = HttpManager.getInstance().getHttpClient();

        // send request
        // get response
        try {
            response = httpClient.newCall(request).execute();

        }catch (IOException e){

            e.printStackTrace();

            response = null;

        }
        return response;
    }

    /**
     * check response contain success string at message attribute or not
     * @param response
     * @return
     * @throws IOException
     */
    public static boolean checkResponseSuccess(Response response) throws IOException{

        boolean rc ;

        JSONObject object = null;
        String message = "";

        try{
            object = new JSONObject(response.body().string());
            message = object.getString("message");
            if(object.getString("message").toLowerCase().compareTo("success") == 0)
                rc = true;
            else
                rc = false;
        }catch (JSONException e) {
            e.printStackTrace();
            com.spectocor.micor.mobileapp.common.Log.e(TAG + ":" + "Response JSON Exception", e.getMessage());
            rc = false;
        }finally {
            if(object != null){
                com.spectocor.micor.mobileapp.common.Log.i(TAG + ":" + "Response JSON Message", message);
            }
        }

        return rc;

    }

    /**
     *
     * @param WebServiceAddress
     * @param ServiceURL
     * @param activationCode
     * @param patientFirstName
     * @param patientLastName
     * @param holsterPlus
     * @return
     */
    public static Request buildCreateActivationCodeRequest(String WebServiceAddress,String ServiceURL ,final String activationCode, final String patientFirstName,
                                                   final String patientLastName, boolean holsterPlus, String facilityID,String enrollmentDate){
        // build request body
        String HolterPlusString = (holsterPlus?"true":"false");


        MultipartBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(RESTAPIParaName.ActivationCode, activationCode)
                .addFormDataPart(RESTAPIParaName.PatientFirstName, patientFirstName)
                .addFormDataPart(RESTAPIParaName.PatientLastName, patientLastName)
                .addFormDataPart(RESTAPIParaName.HolterPlus,HolterPlusString)
                .addFormDataPart(RESTAPIParaName.FacilityID,facilityID)
                .addFormDataPart(RESTAPIParaName.EnrollmentDate,enrollmentDate)
                .build();

        // build request
        Request request = new Request.Builder()
                .header(RESTAPIHeadParaName.Authorization, TestEnvGlobalConst.BASIC_AUTHORIZATION)
                .url(WebServiceAddress + ServiceURL)
                .post(requestBody)
                .build();

        return request;
    }

    /**
     * reset default server
     *
     * @param serverName
     */
    public static void setServerName(final String serverName){

        SERVER_ADDRESS = serverName;

    }

}
