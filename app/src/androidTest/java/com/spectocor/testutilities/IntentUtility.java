package com.spectocor.testutilities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.spectocor.micor.mobileapp.amain.AMainActivity;

import java.util.ArrayList;

/**
 * Created by qwang on 2/11/2016.
 *
 * create different kind of Intent
 */
public class IntentUtility {

    private static String TAG = "IntentUtility";

    public static String BLUETOOTH_EVENT = "BLUETOOTH_EVENT";
    public static String EVENT_TYPE = "EVENT_TYPE";
    /**
     * create Bluetooth event Intent
     * @param bundle
     * @return Intent which is BLUETOOTH_EVENT
     */
    public static Intent createBtEventIntent(Bundle bundle) {

        assert(bundle != null);
        Intent intent = new Intent(BLUETOOTH_EVENT);
        intent.putExtras(bundle);
        return intent;

    }

    /**
     * create an Intent which tell main activity to update UI heart beat
     *
     *
     * @param HeartBeatRate
     * @return Intent which render heart beat number
     */
    public static Intent createRenderHeartBeatIntent(int HeartBeatRate){
        // UPDATING HR IN UI
        Bundle bundle = new Bundle();
        bundle.putInt(EVENT_TYPE, AMainActivity.WEBVIEW_SIGNAL_VIEW_LE_DEVICE_NEW_HEART_RATE);
        bundle.putInt("newHr", HeartBeatRate);
        Intent intent = new Intent(BLUETOOTH_EVENT);
        intent.putExtras(bundle);
        return intent;
    }

    /**
     * create an Intent which tell main activity to update UI heart beat animation
     *
     *
     * @param
     * @return Intent which render heart beat animation
     */
    public static Intent createRenderHeartBeatAnimationIntent(){
        // UPDATING HR IN UI
        Intent heartBeatIntent = new Intent(BLUETOOTH_EVENT);
        heartBeatIntent.putExtra(EVENT_TYPE, AMainActivity.WEBVIEW_SIGNAL_VIEW_LE_DEVICE_POUND_THE_HEART);
        return heartBeatIntent;
    }

    /**
     * create an Intent which tell main activity to update UI ECG signal waveform
     *
     *
     * @param @filterCh1 channel one data
     * @param @filterCh2 channel two data
     *
     * @return Intent which render ECG waveform
     */
    public static Intent createRenderECGWaveformIntent(ArrayList<Integer> filterCh1,ArrayList<Integer> filterCh2){
        // UPDATING ECG waveform IN UI
        Bundle bundle = new Bundle();
        bundle.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SIGNAL_VIEW_LE_ECG_SAMPLES_READY);
        bundle.putString("samplesCh1Json", new Gson().toJson(filterCh1));
        bundle.putString("samplesCh2Json", new Gson().toJson(filterCh2));
        Intent intent = IntentUtility.createBtEventIntent(bundle);

        return intent;
    }

    public static Intent createEndSessionIntent(){

        Intent intent = new Intent("BLUETOOTH_EVENT");
        intent.putExtra("EVENT_TYPE", AMainActivity.SESSION_JUST_TERMINATED);

        return intent;
    }
}
