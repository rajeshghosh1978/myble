package com.spectocor.testutilities.testData;

/**
 * Created by qwang on 2/25/2016.
 *
 * keep test data for sessionID = 1
 */
public class TDSession1 {

    ///////////////////////////////
    // sessions
    ///////////////////////////////

    ///////////////////////////////
    // session 1
    ///////////////////////////////

    public static int sessionId = 1;
    public static String sessionStartDateTime = "2016-02-16T00:00:00.000Z";
    public static String sessionEndDateTime   = "2016-02-16T00:02:00.000Z";


    ///////////////////////////////
    // patient
    ///////////////////////////////
    public static String patientID = "11";
    public static String firstName = "John";
    public static String lastName  = "Doe";
    public static String enrollmentDate  = "2016-01-19";

    ///////////////////////////////
    // facility
    ///////////////////////////////
    public static String facilityID = "7";
    public static String facilityName = "Facility A";

    ///////////////////////////////
    // activation code
    ///////////////////////////////

    public static String activationCode = "1234";

    ///////////////////////////////
    // ecg device info
    ///////////////////////////////

    public static String ecgDeviceID = "SPEC-SD5F855GHJ";

    ///////////////////////////////
    // mobile phone/PDA info
    ///////////////////////////////
    public static String padID = "AHSD4F9F4DD4SD4FF9";

}
