package com.spectocor.testutilities.database.ecg;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.DbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qwang on 3/2/2016.
 *
 * extra functions which are used to retrieve data chunk from DB
 */
public class EcgChunkDbUtility {

    private static String TAG = "EcgChunkDbUtility";
    private static Context mContext ;

    private static EcgChunkDbUtility mEcgChunkDbUtility;
    private static DbHelper mDbHelper;
    private static EcgChunkDbHelper mEcgChunkDbHelper;

    private EcgChunkDbUtility() {

    }

    public static EcgChunkDbUtility getInstance(Context context) {

        if(mEcgChunkDbUtility == null) {
            mEcgChunkDbUtility = new EcgChunkDbUtility();
        }

        mContext = context;

        mDbHelper = DbHelper.getInstance(context);

        mEcgChunkDbHelper = EcgChunkDbHelper.getInstance(context);

        return mEcgChunkDbUtility;
    }

    public int insertEcgDataChunks( List<EcgDataChunk> ecgDataChunksList){

        int insertNumber = 0;

        if (ecgDataChunksList != null && ecgDataChunksList.size() > 0){

            try{

                for(int i = 0; i < ecgDataChunksList.size() ; ++i){

                    mEcgChunkDbHelper.insertEcgChunk(ecgDataChunksList.get(i));
                }
            }catch (SQLDataException e){

                insertNumber =  0;

                Log.e(TAG,""+e.getMessage());
            }

        }

        return insertNumber;
    }

    public List<EcgDataChunk> retrieveEcgDataChunks(int sessionId, long startTimestamp, long endTimestamp){

        List<EcgDataChunk> ecgDataChunks = null;
        EcgDataChunk ecgDataChunk;

//        String QUERY = "SELECT * FROM "
//                 + DbHelper.ECG_CHUNKS_TABLE_NAME
//                 + " WHERE " + DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID + "=?" + " and "
//                 + DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP + ">=?" + " and "
//                 + DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP + "<=?" ;

        String QUERY = "SELECT * FROM "
                   + DbHelper.ECG_CHUNKS_TABLE_NAME
                   + " ";

        String SELECTION_ARGS[] = {"" + sessionId, "" + startTimestamp, "" + endTimestamp};

        // TODO free up cursor after finished reading data
        //Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, null);
        //Log.d(TAG,QUERY + " with args " + SELECTION_ARGS.toString());
        Log.d(TAG,"Num of rows: " + c.getCount());

        if(c.getCount() == 0) {
            Log.d(TAG,"Row not found ... ");
            Log.d(TAG,"No such data found");
            ecgDataChunks = null;
        }else {
            ;

            // move cursor to first row
            if (c.moveToFirst()) {

                ecgDataChunks = new ArrayList<>();

                do {

                    // Get info from Cursor

                    ecgDataChunk = new EcgDataChunk();
                    ecgDataChunk.chunkDataChannelCompressed = c.getBlob(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_COMPRESSED_ECG_CHUNK_BLOB));
                    ecgDataChunk.chunkStartDateUnixMs = c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP));
                    ecgDataChunk.insertTimestamp = c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_INSERT_TIMESTAMP));
                    ecgDataChunk.monitoringSessionId = Integer.parseInt(c.getString(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID)));
                    ecgDataChunk.timezoneId = c.getString(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_TIMEZONE_NAME_ID));
                    ecgDataChunk.timezoneOffsetMillis = c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_TIMEZONE_OFFSET_MILLIS));
                    ecgDataChunk.numberOfSamples = c.getInt(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_NUMBER_OF_SAMPLES_IN_CHUNK));
                    ecgDataChunk.chunkTransferTimestamp = c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_TRANSMITTED_TIMESTAMP));
                    // add the bookName into the bookTitles ArrayList

                    ecgDataChunks.add(ecgDataChunk);

                    // move to next row

                } while (c.moveToNext());

            }

        }

        return ecgDataChunks;
    }

    public boolean deleteEcgDataChunk(int sessionId, long startTimestamp){

        List<EcgDataChunk> ecgDataChunks = null;
        EcgDataChunk ecgDataChunk;

        String QUERY = "DELETE FROM "
                 + DbHelper.ECG_CHUNKS_TABLE_NAME
                 + " WHERE " + DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID + "=?" + " and "
                 + DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP + "=?" ;


        String SELECTION_ARGS[] = {"" + sessionId, "" + startTimestamp};

        // TODO free up cursor after finished reading data
        Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        //Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, null);
        //Log.d(TAG,QUERY + " with args " + SELECTION_ARGS.toString());
        Log.d(TAG,"Num of rows: " + c.getCount());

        return true;
    }

    public boolean deleteUnSendEcgDataChunk(){

        List<EcgDataChunk> ecgDataChunks = null;
        EcgDataChunk ecgDataChunk;

        String QUERY = "DELETE FROM "
                + DbHelper.ECG_CHUNKS_TABLE_NAME
                + " WHERE " + DbHelper.ECG_CHUNKS_COLUMN_TRANSMITTED_TIMESTAMP + "=?" ;


        String SELECTION_ARGS[] = {"" + -1};

        // TODO free up cursor after finished reading data
        Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        //Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, null);
        //Log.d(TAG,QUERY + " with args " + SELECTION_ARGS.toString());
        Log.d(TAG,"Num of rows: " + c.getCount());

        return true;
    }

    public int dumpEcgDataChunk(){


        EcgDataChunk ecgDataChunk;

//        String QUERY = "SELECT * FROM "
//                 + DbHelper.ECG_CHUNKS_TABLE_NAME
//                 + " WHERE " + DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID + "=?" + " and "
//                 + DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP + ">=?" + " and "
//                 + DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP + "<=?" ;

        String QUERY = "SELECT * FROM "
                + DbHelper.ECG_CHUNKS_TABLE_NAME
                + " ";

        //String SELECTION_ARGS[] = {"" + sessionId, "" + startTimestamp, "" + endTimestamp};


        Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, null);
        //Log.d(TAG,QUERY + " with args " + SELECTION_ARGS.toString());
        Log.d(TAG,"Num of rows: " + c.getCount());

        String rowString = "";
        if(c.getCount() == 0) {
            Log.d(TAG,"Row not found ... ");
            Log.d(TAG,"No such data found");

        }else {
            // move cursor to first row
            if (c.moveToFirst()) {
                Log.d(TAG,"START SECOND : INSERT TIME : SESSION ID : SAMPLE NUMB : TRANSFER TIME   ");
                do {

                    // Get info from Cursor
                    rowString = "" + c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP)) + ":";
                    rowString += c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_INSERT_TIMESTAMP)) + ":";
                    rowString += Integer.parseInt(c.getString(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID))) +":";
                    rowString += c.getInt(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_NUMBER_OF_SAMPLES_IN_CHUNK)) +":";
                    rowString += c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_TRANSMITTED_TIMESTAMP));

                    Log.d(TAG,rowString);
                    // move to next row

                } while (c.moveToNext());

            }

        }

        return c.getCount();
    }
}
