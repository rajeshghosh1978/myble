package com.spectocor.testutilities.database.ecg;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.DbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientEvent;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientReportedEvent;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientReportedEventsDbHelper;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qwang on 3/4/2016.
 *
 * extra functions which are used to retrieve patient trigger event from DB
 */
public class PatientReportedEventsDbUtility {

    private static String TAG = "PatientReportedEventsDbUtility";
    private static Context mContext ;

    private static PatientReportedEventsDbUtility mPatientReportedEventsDbUtility;
    private static DbHelper mDbHelper;
    private static PatientReportedEventsDbHelper mPatientReportedEventsDbHelper;

    private PatientReportedEventsDbUtility() {

    }

    public static PatientReportedEventsDbUtility getInstance(Context context) {

        if(mPatientReportedEventsDbUtility == null) {
            mPatientReportedEventsDbUtility = new PatientReportedEventsDbUtility();
        }

        mContext = context;

        mDbHelper = DbHelper.getInstance(context);

        mPatientReportedEventsDbHelper = PatientReportedEventsDbHelper.getInstance();

        return mPatientReportedEventsDbUtility;
    }

    public int insertPatientReportedEvents( List<PatientEvent> patientReportedEventsList){

        int insertNumber = 0;

        if (patientReportedEventsList != null && patientReportedEventsList.size() > 0){

            try{

                for(int i = 0; i < patientReportedEventsList.size() ; ++i){

                    mPatientReportedEventsDbHelper.insertPatientEvent(patientReportedEventsList.get(i));
                }
            }catch (Exception e){

                insertNumber =  0;

                Log.e(TAG, "" + e.getMessage());
            }

        }

        return insertNumber;
    }

    public List<PatientEvent> retrievePatientEvents(int sessionId){

        List<PatientEvent> patientEvents = null;
        PatientEvent patientEvent;


        String QUERY = "SELECT * FROM "
                + DbHelper.PTE_TABLE_NAME
                + " ";

        //String SELECTION_ARGS[] = {"" + sessionId, "" + startTimestamp, "" + endTimestamp};

        //Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        Cursor cursor = mDbHelper.getReadableDatabase().rawQuery(QUERY, null);
        //Log.d(TAG,QUERY + " with args " + SELECTION_ARGS.toString());
        Log.d(TAG, "Num of rows: " + cursor.getCount());

        if(cursor.getCount() == 0) {
            Log.d(TAG,"Row not found ... ");
            Log.d(TAG,"No such data found");
            patientEvents = null;
        }else {
            ;

            // move cursor to first row
            if (cursor.moveToFirst()) {

                patientEvents = new ArrayList<>();

                do {

                    // Get info from Cursor

                    patientEvent = new PatientEvent();

                    patientEvent.setSessionId(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_SESSION_ID)));
                    patientEvent.setInsertTimestamp(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_INSERT_TIMESTAMP)));
                    patientEvent.setData(cursor.getString(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_DATA)));
                    patientEvent.setTransmittedTimestamp(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP)));

                    patientEvents.add(patientEvent);

                    // move to next row

                } while (cursor.moveToNext());

            }

        }

        return patientEvents;
    }

    public List<PatientEvent> retrievePatientEventsByTransferTime(int sessionId, long transferTimestamp){

        List<PatientEvent> patientEvents = null;
        PatientEvent patientEvent;

        String QUERY = "SELECT * FROM "
                + DbHelper.PTE_TABLE_NAME
                + " WHERE " + DbHelper.PTE_COLUMN_NAME_SESSION_ID + "=?" + " and "
                + DbHelper.PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP + "=?";

        String SELECTION_ARGS[] = {"" + sessionId, "" + transferTimestamp};

        Cursor cursor = mDbHelper.getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        //Log.d(TAG,QUERY + " with args " + SELECTION_ARGS.toString());
        Log.d(TAG, "Num of rows: " + cursor.getCount());

        if(cursor.getCount() == 0) {
            Log.d(TAG,"Row not found ... ");
            Log.d(TAG,"No such data found");
            patientEvents = null;
        }else {


            // move cursor to first row
            if (cursor.moveToFirst()) {

                patientEvents = new ArrayList<>();

                do {

                    // Get info from Cursor

                    patientEvent = new PatientEvent();

                    patientEvent.setSessionId(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_SESSION_ID)));
                    patientEvent.setInsertTimestamp(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_INSERT_TIMESTAMP)));
                    patientEvent.setData(cursor.getString(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_DATA)));
                    patientEvent.setTransmittedTimestamp(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP)));

                    patientEvents.add(patientEvent);

                    // move to next row

                } while (cursor.moveToNext());

            }

        }

        return patientEvents;
    }

    public PatientEvent retrievePatientEvent(int sessionId, long startTimestamp){


        PatientEvent patientEvent = null;

        String QUERY = "SELECT * FROM "
                + DbHelper.PTE_TABLE_NAME
                + " WHERE " + DbHelper.PTE_COLUMN_NAME_SESSION_ID + "=?" + " and "
                + DbHelper.PTE_COLUMN_NAME_INSERT_TIMESTAMP + "=?" ;

        String SELECTION_ARGS[] = {"" + sessionId, "" + startTimestamp};

        Cursor cursor = mDbHelper.getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        //Log.d(TAG,QUERY + " with args " + SELECTION_ARGS.toString());
        Log.d(TAG, "Num of rows: " + cursor.getCount());


        if(cursor.getCount() == 0) {
            Log.d(TAG,"Row not found ... ");
            Log.d(TAG,"No such data found");

        }else {

            // move cursor to first row
            if (cursor.moveToFirst()) {

                patientEvent = new PatientEvent();

                patientEvent.setSessionId(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_SESSION_ID)));
                patientEvent.setInsertTimestamp(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_INSERT_TIMESTAMP)));
                patientEvent.setData(cursor.getString(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_DATA)));
                patientEvent.setTransmittedTimestamp(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP)));

            }

        }

        return patientEvent;
    }

    public boolean deletePatientEvent(int sessionId, long startTimestamp){

        String QUERY = "DELETE FROM "
                + DbHelper.PTE_TABLE_NAME
                + " WHERE " + DbHelper.PTE_COLUMN_NAME_SESSION_ID + "=?" + " and "
                + DbHelper.PTE_COLUMN_NAME_INSERT_TIMESTAMP + "=?" ;


        String SELECTION_ARGS[] = {"" + sessionId, "" + startTimestamp};

        // TODO free up cursor after finished reading data
        Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        //Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, null);
        //Log.d(TAG,QUERY + " with args " + SELECTION_ARGS.toString());
        Log.d(TAG,"Num of rows: " + c.getCount());

        return true;
    }

    public boolean deleteUnSendPatientEvent(){

        String QUERY = "DELETE FROM "
                + DbHelper.PTE_TABLE_NAME
                + " WHERE " + DbHelper.PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP + "=?" ;


        String SELECTION_ARGS[] = {"" + -1};

        // TODO free up cursor after finished reading data
        Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        //Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, null);
        //Log.d(TAG,QUERY + " with args " + SELECTION_ARGS.toString());
        Log.d(TAG,"Num of rows: " + c.getCount());

        return true;
    }

    public boolean deleteAllPatientEvent(){

        String QUERY = "DELETE FROM "
                + DbHelper.PTE_TABLE_NAME;

        Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, null);
        //Cursor c = mDbHelper.getReadableDatabase().rawQuery(QUERY, null);
        //Log.d(TAG,QUERY + " with args " + SELECTION_ARGS.toString());
        Log.d(TAG,"Num of rows: " + c.getCount());

        return true;
    }

    public int dumpPatientEvent(){


        EcgDataChunk ecgDataChunk;

//        String QUERY = "SELECT * FROM "
//                 + DbHelper.ECG_CHUNKS_TABLE_NAME
//                 + " WHERE " + DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID + "=?" + " and "
//                 + DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP + ">=?" + " and "
//                 + DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP + "<=?" ;

        String QUERY = "SELECT * FROM "
                + DbHelper.PTE_TABLE_NAME
                + " ";

        //String SELECTION_ARGS[] = {"" + sessionId, "" + startTimestamp, "" + endTimestamp};


        Cursor cursor = mDbHelper.getReadableDatabase().rawQuery(QUERY, null);
        //Log.d(TAG,QUERY + " with args " + SELECTION_ARGS.toString());
        Log.d(TAG, "Num of rows: " + cursor.getCount());

        String rowString = "";
        if(cursor.getCount() == 0) {
            Log.d(TAG,"Row not found ... ");
            Log.d(TAG,"No such data found");

        }else {
            // move cursor to first row
            if (cursor.moveToFirst()) {
                Log.d(TAG,"INSERT TIME : SESSION ID : Patient Event : TRANSFER TIME   ");
                do {

                    // Get info from Cursor
                    rowString = "" + cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_INSERT_TIMESTAMP)) + ":";
                    rowString += cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_SESSION_ID)) + ":";
                    rowString += cursor.getString(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_DATA)) +":";
                    rowString += cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP));

                    Log.d(TAG,rowString);
                    // move to next row

                } while (cursor.moveToNext());

            }

        }

        return cursor.getCount();
    }
}
