package com.spectocor.micor.mobileapp.devicelogs;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.LargeTest;

import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;
import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;

import junit.framework.Assert;

/**
 * Tests Battery Log service
 */
public class BatteryLogServiceTest extends AndroidTestCase {

    DeviceLogRepository repository;
    GlobalSettings settings;
    DeviceLogQueueRepository queueRepository;
    DeviceLogLogger logger;

    BatteryLogService service;

    /**
     * instantiate test classes
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        super.setUp();

        Context context = getContext();
        context.deleteDatabase(LogEventsDatabaseHelper.DatabaseName);
        context.deleteDatabase(QueueDatabaseHelper.DatabaseName);

        LogEventsDatabaseHelper databaseHelper = new LogEventsDatabaseHelper(context);
        repository = new DeviceLogRepository(databaseHelper);

        QueueDatabaseHelper queueDatabaseHelper = new QueueDatabaseHelper(context);
        queueRepository = new DeviceLogQueueRepository(queueDatabaseHelper);

        //TODO: Try to add Mokito for unit tests. It is not acceptable to use actual class for all unit tests
        //repository = Mockito.mock(DeviceLogRepository.class);

        settings = new GlobalSettings();

        logger = new DeviceLogLogger(repository, settings, queueRepository);

        service = new BatteryLogService(logger);
    }


    /*
    * test log mobile device battery status with 10
    * Test Case ID : UTMAID000015
    */
    public void testLogMobileDeviceBatteryLevel() throws Exception {
        int time = (int) (System.currentTimeMillis() / 1000);
        DeviceLog log = service.logMobileDeviceBatteryLevel((byte) 10);
        assertEquals(log.getValueNumber(), 10.0);
        assertEquals(log.getDeviceLogInfoTypeId(), EnumDeviceLogInfoType.MobileDeviceBatteryLevel);
        assertNull(log.getValueString());
        assertTrue(log.getLogDateUnixSec() >= time);
    }


    /*
   * test log ECG transmitter battery status with 127
   */
    @LargeTest
    public void testFailLogEcgTransmitterBatteryStatus() throws Exception {
        try {
            DeviceLog log = service.logMobileDeviceBatteryLevel((byte) 127);
            Assert.assertTrue(log == null);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }


    public void testLogMobileDevicePowerPlugConnected() throws Exception {

    }


}