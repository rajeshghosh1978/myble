package com.spectocor.micor.mobileapp.ecgblehandler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.content.LocalBroadcastManager;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainService;
import com.spectocor.micor.mobileapp.ecgadapter.HeartRateHandler;
import com.spectocor.micor.mobileapp.uitestframework.UITestSimpleFramework;
import com.spectocor.micor.mobileapp.uitestframework.UIWebCmdUtility;
import com.spectocor.testutilities.ECGDataCreator;
import com.spectocor.testutilities.IntentUtility;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.utils.ArrayUtility;
import com.spectocor.testutilities.utils.ThreadUtility;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 2/12/2016.
 *
 * test case for heart beat rate view. format "XXX bpm"
 *
 * CHANGES HISTORY
 ***********************************************************
 * Date             Author          Comments
 * 02/16/2016       Qian            add UI check
 * 02/17/2016       Qian            add a test case which check resetting heart beat rate to zero when there is no data
 *
 ***********************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 02/17/2016       Qian            fail : showHeartBeatViewWithRandomGap , unable to destroy activity
 *
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class HeartBeatViewTest {

    //////////////////////////////////////////////////////////////
    // Simple Test Framework
    //////////////////////////////////////////////////////////////
    private static String TAG = "HeartBeatViewTest";

    //private UITestSimpleFramework mTestFramework;
    private static AMainActivity mActivity;
    private static Context mContext;

    private static int mSampleRate = TestEnvGlobalConst.SAMPLE_RATE;
    private static long mDefaultSleepMillionSecond = TestEnvGlobalConst.APP_SLEEP_MILLION_SECOND;

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init(); // launch the activity and get test framework
        }
    };

    @Before
    public void before(){
        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond); // waiting for activity launch

        mActivity.callWebviewJsFunction("displayAdminEcgSignalPage()");  // jump to signal view
    }

    @After
    public void after(){
        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond); // waiting for activity close
    }

    //////////////////////////////////////////////
    // test data
    //////////////////////////////////////////////
    private int mDefaultHeartBeatVal = TestEnvGlobalConst.NORMAL_HEART_BEAT_RATE;
    private int mDefaultDisplayTime = TestEnvGlobalConst.NORMAL_HEART_BEAT_RATE;

    private static String HR_BEAT_ELEMENT_ID = "ecg-view-hr";

    //////////////////////////////////////////////
    // test case
    //////////////////////////////////////////////
    /**
     *
     * test main activity receiving heart beat number to display heart beat number
     * test data: 70
     *
     * Test Case ID : UTMAID140001
     */
    @Test
    public void testDisplay70() {

        int[] mockHeartBeats;

        mockHeartBeats = ArrayUtility.fillingIntArray(mDefaultHeartBeatVal, mDefaultDisplayTime);  // prepare fill 70

        showHeartBeatView(mockHeartBeats);

    }

    /**
     *
     * test main activity receiving heart beat number to display heart beat number
     * test data: random heart beat between [30,300]
     *
     * Test Case ID : UTMAID140002
     */

    @Test
    public void testDisplayRandomHR() {

        int[] mockHeartBeats;

        mockHeartBeats = ECGDataCreator.createHeartBeatList(mDefaultDisplayTime);   // mock heart beat list

        showHeartBeatView(mockHeartBeats);

    }

    /**
     *
     * test main activity receiving heart beat number to display heart beat number
     * test data: 350
     *
     * Test Case ID : UTMAID140003
     */

    @Test
    public void testDisplayAbove350() {

        int[] mockHeartBeats;

        mockHeartBeats = ArrayUtility.fillingIntArray(350, mDefaultDisplayTime);;   // mock heart beat list

        showHeartBeatView(mockHeartBeats);

    }

    /**
     *
     * test main activity receiving heart beat number to display heart beat number
     * test data: 20
     *
     * Test Case ID : UTMAID140004
     */

    @Test
    public void testDisplayAbove20() {

        int[] mockHeartBeats;

        mockHeartBeats = ArrayUtility.fillingIntArray(20, mDefaultDisplayTime);;   // mock heart beat list

        showHeartBeatView(mockHeartBeats);

    }

    /**
     *
     * test main activity receiving heart beat number to display heart beat number with random stop
     * test data: 70
     *
     * Test Case ID : UTMAID140005
     */

    @Test
    public void testDisplay70WithStop() {

        int[] mockHeartBeats;

        mockHeartBeats = ArrayUtility.fillingIntArray(mDefaultHeartBeatVal, mDefaultDisplayTime);  // prepare fill 70

        showHeartBeatViewWithRandomGap(mockHeartBeats);

    }

    /////////////////////////////////////////////////////////////////
    // support function
    /////////////////////////////////////////////////////////////////
    /**
     * init class member variables
     */
    private void init(){

        mActivity = mActivityRule.getActivity();
        mContext  = mActivity.getApplicationContext();

        assert(mActivity != null);
        assert(mContext != null);

        Log.d(TAG, "init");
    }

    /**
     * display list of heart beat on main activity
     *
     * @param heartBeatRates, list of heart beat rate
     * @return
     */
    public boolean showHeartBeatView(int[] heartBeatRates){
        boolean rc = true ;

        Intent intent;

        for(int i = 0;i <  mDefaultDisplayTime; ++i){

               rc = showHeartBeatView(heartBeatRates[i]);

            }

        return rc;
    }

    /**
     * display list of heart beat on main activity with random gap
     *
     * expected heart beat rate reset to zero when there is no data coming
     *
     * @param heartBeatRates , list of heart beat rate
     * @return
     */
    public boolean showHeartBeatViewWithRandomGap(int[] heartBeatRates){
        boolean rc = true ;

        Intent intent;

        for(int i = 0;i <  mDefaultDisplayTime; ++i){

            rc = showHeartBeatView(heartBeatRates[i]);

            ThreadUtility.applicationSleep(3000);

            checkUIHeartBeatRate(0); // expected value 0 bpm
        }

        return rc;
    }

    /**
     * display heart beat on main activity
     * @param heartBeatRates
     * @return
     */
    public boolean showHeartBeatView(int heartBeatRates){

        long waitTime = 0;

        long start = System.currentTimeMillis();

        Integer integer; // expected hr integer

        String hr_string; // expected hr string XXX bpm

        Intent intent = IntentUtility.createRenderHeartBeatIntent(heartBeatRates);

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

        long end = System.currentTimeMillis();

        waitTime = (60000 / heartBeatRates) - (end - start);

        if(waitTime > 0)
            SystemClock.sleep(waitTime);

        // check UI
        checkUIHeartBeatRate(heartBeatRates);

        return true;
    }

    /**
     * check UI heart beat rate number
     *
     * @param ExpectedHR, expected HR number
     */
    private static void checkUIHeartBeatRate(int ExpectedHR){

        // check UI
        Integer integer = new Integer(ExpectedHR);

        String hr_string = integer.toString() + " bpm";

        Assert.assertTrue(TAG + ":fail to compare heart beat rate", UIWebCmdUtility.checkWebElementTextByID(HR_BEAT_ELEMENT_ID, hr_string));
    }
}
