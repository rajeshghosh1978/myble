package com.spectocor.micor.mobileapp.simulate.user;

import android.content.Context;
import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.uitestframework.UIMockData;
import com.spectocor.micor.mobileapp.uitestframework.UITestCommand;
import com.spectocor.micor.mobileapp.uitestframework.UITestSimpleFramework;
import com.spectocor.micor.mobileapp.uitestframework.UIWebCmdUtility;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.html.page.BasePage;
import com.spectocor.testutilities.html.page.ChooseECGDevicePage;
import com.spectocor.testutilities.html.page.EndSessionPage;
import com.spectocor.testutilities.html.page.EnterActivationCodePage;
import com.spectocor.testutilities.html.page.ReportSymptomHomePage;
import com.spectocor.testutilities.micro.app.SimulateUser;
import com.spectocor.testutilities.session.api.CreateActivationCodeRESTAPI;
import com.spectocor.testutilities.session.api.RESTAPIResponseMessage;
import com.spectocor.testutilities.session.api.SessionEndRESTAPI;
import com.spectocor.testutilities.utils.ThreadUtility;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 3/1/2016.
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class SimulateUserTest {

    private static String TAG = "SimulateUserTest";

    private static AMainActivity mActivity;
    private static Context mContext;
    private static CurrentSessionState mSessionState;
    private static UITestSimpleFramework mUITestFramework;

    //////////////////////////////////////////////////////////////
    // test data
    //////////////////////////////////////////////////////////////
    private static long mDefaultSleepMillionSecond = TestEnvGlobalConst.APP_SLEEP_MILLION_SECOND;

    private static String mDefaultActivationCode = "700";
    private static String mDefaultPatientFirstName = "700Qian";
    private static String mDefaultPatientLastName = "Wang700";
    private static boolean mDefaultHolterPlus = false;
    private static String mFacilityId = "8";
    private static String mEnrollmentDate = "2016-03-18";

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init();  // launch the activity and init static variables
        }
    };

    @Before
    public void before() {
        // waiting activity launch
        Log.i(TAG, "waiting for application launch :" + (mDefaultSleepMillionSecond )/1000 + " seconds");
        SystemClock.sleep(mDefaultSleepMillionSecond);
        // jump to ecg signal view
        //mActivity.callWebviewJsFunction("lookupEcgAtShipping()");
    }

    @After
    public void after() {
        // waiting tester to check UI
        Log.i(TAG, "waiting for application close :" + (mDefaultSleepMillionSecond )/1000 + " seconds");
        SystemClock.sleep(mDefaultSleepMillionSecond);
    }

    ///////////////////////////////////////////////////
    // create a new activation code
    // waiting for starting a new session
    // wait for couple of minutes
    // end session
    // waiting for ending application
    ///////////////////////////////////////////////////
    @Test
    public void testCompletedNewSession() {

        String rcString;

        endCurrentSession();  // end current session

        Log.i(TAG, "Start a new session with activation code :" + mDefaultActivationCode);

        rcString = CreateActivationCodeRESTAPI.createActivationCode(mDefaultActivationCode, mDefaultPatientFirstName,
                mDefaultPatientLastName, mDefaultHolterPlus,
                mFacilityId, mEnrollmentDate);

        String test_case_data_file = "td_normal_session_ac_700.json";

        mUITestFramework.runTestFile(test_case_data_file);

        mSessionState = SessionStateAccess.getCurrentSessionState(mContext);

        Log.i(TAG, "session :" + mSessionState.sessionId + ": started");

        Log.i(TAG, "transfer ECG data chunk to server :" + (mDefaultSleepMillionSecond * 5) / 1000 + " seconds");

        for(int i = 0; i < 5 ; ++i){

            reportRandomPatientTriggerEvents();

            ThreadUtility.applicationSleep(mDefaultSleepMillionSecond);

        }

        endCurrentSession(); // end current session

        checkCurrentPage(ChooseECGDevicePage.Title);

    }

    @Test
    public void testNewHolterSessionEndFirstAndSecondSession() {

        ////////////////////////////////////
        // test data
        ////////////////////////////////////
        String rcString;

        endCurrentSession();  // end current session

        String holterActivationCode = "710";
        String holterPatientFirstName = "710Qian";
        String holterPatientLastName = "Wang710";
        boolean holterType = true;
        String holterFacilityId = "8";
        String holterEnrollmentDate = "2016-03-21";

        Log.i(TAG, "Start a new session with activation code :" + holterActivationCode);

        rcString = CreateActivationCodeRESTAPI.createActivationCode(holterActivationCode, holterPatientFirstName,
                holterPatientLastName, holterType,
                holterFacilityId, holterEnrollmentDate);

        String test_case_data_file = "td_normal_session_ac_710.json";

        mUITestFramework.runTestFile(test_case_data_file);

        mSessionState = SessionStateAccess.getCurrentSessionState(mContext);

        Log.i(TAG, "session :" + mSessionState.sessionId + ": started");

        Log.i(TAG, "transfer ECG data chunk to server :" + (mDefaultSleepMillionSecond * 4) / 1000 + " seconds");

        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond * 4);

        endCurrentSession(); // end current session

        Log.i(TAG, "transfer ECG data chunk to server :" + (mDefaultSleepMillionSecond * 4) / 1000 + " seconds");

        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond * 4);

        endCurrentSession(); // end current session

        checkCurrentPage(ChooseECGDevicePage.Title);
    }

    @Test
    public void testNewHolterSessionEndSecondAndFirstSession() {

        ////////////////////////////////////
        // test data
        ////////////////////////////////////
        String rcString;

        endCurrentSession();  // end current session

        String holterActivationCode = "710";
        String holterPatientFirstName = "710Qian";
        String holterPatientLastName = "Wang710";
        boolean holterType = true;
        String holterFacilityId = "8";
        String holterEnrollmentDate = "2016-03-21";

        Log.i(TAG, "Start a new session with activation code :" + holterActivationCode);

        rcString = CreateActivationCodeRESTAPI.createActivationCode(holterActivationCode, holterPatientFirstName,
                holterPatientLastName, holterType,
                holterFacilityId, holterEnrollmentDate);

        String test_case_data_file = "td_normal_session_ac_710.json";

        mUITestFramework.runTestFile(test_case_data_file);

        mSessionState = SessionStateAccess.getCurrentSessionState(mContext);

        Log.i(TAG, "session :" + mSessionState.sessionId + ": started");

        Log.i(TAG, "transfer ECG data chunk to server :" + (mDefaultSleepMillionSecond * 4) / 1000 + " seconds");

        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond * 4);

        mSessionState = SessionStateAccess.getCurrentSessionState(mContext);

        String secondSessionID = Integer.toString(mSessionState.sessionId - 1);
        endCurrentSession(secondSessionID); // end current session

        Log.i(TAG, "transfer ECG data chunk to server :" + (mDefaultSleepMillionSecond * 4) / 1000 + " seconds");

        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond * 4);

        endCurrentSession(); // end current session

        checkCurrentPage(ChooseECGDevicePage.Title);

    }

    @Test
    public void EnterInvalidActivationCode() {

        String pageContent;

        endCurrentSession();  // end current session

        jump2WebPage(ChooseECGDevicePage.CallFunc);

        checkCurrentPage(ChooseECGDevicePage.Title);

        String test_case_data_file = "td_invalid_activation_code.json";

        mUITestFramework.runTestFile(test_case_data_file);

        checkCurrentPage(EnterActivationCodePage.Title);
    }

//    public void testEndSession() {
//
//        Context context = getContext();
//
//        CurrentSessionState sessionStateAccess = SessionStateAccess.getCurrentSessionState(context);
//
//        Assert.assertTrue(TAG + ":Fail to start test because there is no active session on mobile phone", sessionStateAccess.sessionActive);
//
//        SimulateUser.EndSession(getContext(), "1");
//
//        ThreadUtility.applicationSleep(10000);
//
//        Assert.assertTrue(TAG + ":Fail to start test because there is no active session on mobile phone", sessionStateAccess.sessionActive == false);
//    }

//    @Test
//    public void testEndSession2DeviceList() {
//
//        Log.i(TAG, "Start click upper left corn button :");
//
//        String test_case_data_file = "td_end_session_ui_to_device_list.json";
//
//        mUITestFramework.runTestFile(test_case_data_file);
//
//        Log.i(TAG, "end : device list page");
//
//    }
//
    @Test
    public void testJumpPage() {

        assert (mActivity != null);

        //mActivity.callWebviewJsFunction("prepareNewSession()");
        mActivity.callWebviewJsFunction(ChooseECGDevicePage.CallFunc);

    }

    @Test
    public void testGetPageTitle(){

        String pageContent = UIWebCmdUtility.getCurrentWebViewContent();

        String[] lines = pageContent.split("\n");

        for(String line : lines){
            Log.i(TAG,line);
        }

        boolean rc = EndSessionPage.isActivatePage(pageContent);

        Log.i(TAG, "is end session page:" + rc);
    }


    //////////////////////////////////////////////////
    // support function
    //////////////////////////////////////////////////

    /**
     * init function
     */
    private void init() {

        mActivity = mActivityRule.getActivity();
        // set context
        mContext = mActivity.getApplicationContext();  // context for activity under test

        mSessionState = SessionStateAccess.getCurrentSessionState(mContext);


        //Context context = InstrumentationRegistry.getContext().getApplicationContext();
        //mContext = InstrumentationRegistry.getTargetContext();  // context for activity under test
        if (mUITestFramework == null) {
            mUITestFramework = new UITestSimpleFramework();
        }
        mUITestFramework.setContext(mContext);

    }


    private static void endCurrentSession() {

        String sessionID;

        mSessionState = SessionStateAccess.getCurrentSessionState(mContext);

        if (mSessionState.sessionActive) {

            sessionID = Integer.toString(mSessionState.sessionId);

            endCurrentSession(sessionID);

        } else{
            Log.i(TAG, "There is no active session on the mobile phone");
            jumpSessionEnd2ChooseECGDevice();
        }


    }

    private static void endCurrentSession(final String sessionID) {

        String rc;

        Log.i(TAG, "call session end REST API to end session:" + sessionID);

        rc = SessionEndRESTAPI.sessionEnd(sessionID);  // send it

        org.junit.Assert.assertTrue("Fail to end session", rc.compareTo(RESTAPIResponseMessage.SUCCESS) == 0);

        // waiting for end session
        long startTime = SystemClock.currentThreadTimeMillis();
        long sleepTime = 1000;
        long usedTime = 0;
        mSessionState = SessionStateAccess.getCurrentSessionState(mContext);

        while(usedTime < TestEnvGlobalConst.WAIT_END_SESSION && mSessionState.sessionActive){

            ThreadUtility.applicationSleep(sleepTime);
            usedTime += sleepTime;
        }


        Log.i(TAG, "session: " + sessionID + ": is end");

        // jump to choose device name list
        jumpSessionEnd2ChooseECGDevice();

    }

    /**
     * jump from end session page to choose device name list page
     * @return
     */
    private static boolean jumpSessionEnd2ChooseECGDevice(){

        // first check current page is session end page or not
        // if Yes, then jump to Choose ECG Device page
        boolean rc = false;

        ThreadUtility.applicationSleep(2000);

        String contentPage = UIWebCmdUtility.getCurrentWebViewContent();


        if(EndSessionPage.isActivatePage(contentPage)){

            String test_case_data_file = "td_end_session_ui_to_device_list.json";

            mUITestFramework.runTestFile(test_case_data_file);

            contentPage = UIWebCmdUtility.getCurrentWebViewContent();

            if(ChooseECGDevicePage.isActivatePage(contentPage)){
                rc = true;
            }else{
                rc = false;
                BasePage.dumpPageContent(contentPage);
            }

        }else{
            Log.i(TAG,"end session page is invisible now");
            BasePage.dumpPageContent(contentPage);
        }

        return rc;
    }

    public static void checkCurrentPage(final String pageTitle){

        String contentPage = UIWebCmdUtility.getCurrentWebViewContent();

        for(int i = 0; i < 10; ++i){

            if(contentPage.contains(pageTitle)){
                break;
            }
            ++i;
            ThreadUtility.applicationSleep(1000);
        }

        Assert.assertTrue(TAG + "Fail to check current page:Expected Page Title is " + pageTitle + ":Real Page Content:" + contentPage,contentPage.contains(pageTitle));
    }

    public static boolean jump2WebPage(final String pageCallFunc ){
        boolean rc = false;

        String pageContent;

        if (mActivity != null){

            // check current web page
            pageContent = UIWebCmdUtility.getCurrentWebViewContent();

            if(EnterActivationCodePage.isActivatePage(pageContent)){
                mActivity.callWebviewJsFunction(pageCallFunc);
            }
        }

        return rc;
    }

    public static void reportRandomPatientTriggerEvents(){

        String before_test_case_data_file = "td_session_home_to_select_patient_feel_pg.json";

        List<UITestCommand> test_case_data_feels = UIMockData.mockSelectFeels(30);

        List<UITestCommand> test_case_data_activitys = UIMockData.mockSelectActivitys(20);

        String after_test_case_data_file = "td_symptom_confirmation_to_session_home.json";

        String contentPage;

        contentPage = UIWebCmdUtility.getCurrentWebViewContent();

        Assert.assertTrue(TAG + "fail to pass current page should be report patient trigger event home page",ReportSymptomHomePage.isActivatePage(contentPage));

        Log.i(TAG, "start to report patient trigger events");

        mUITestFramework.runTestFile(before_test_case_data_file);

        mUITestFramework.runTest(test_case_data_feels);

        mUITestFramework.runTest(test_case_data_activitys);

        mUITestFramework.runTestFile(after_test_case_data_file);

        contentPage = UIWebCmdUtility.getCurrentWebViewContent();

        Assert.assertTrue(TAG + "fail to pass current page should be report patient trigger event home page", ReportSymptomHomePage.isActivatePage(contentPage));

        Log.i(TAG, "end to report patient trigger events");

    }
}
