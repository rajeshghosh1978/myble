package com.spectocor.micor.mobileapp.sessionactivation;

import android.test.AndroidTestCase;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.http.HttpManager;
import com.spectocor.micor.mobileapp.http.common.ResponseCodes;
import com.spectocor.micor.mobileapp.sessionactivation.api.EnrollmentInfo;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Alternative session activation path test cases.
 *
 * Created by Rafay Ali on 3/7/2016.
 */
public class SessionActivationByFacilityCodeApiTest extends AndroidTestCase {

    public final String TAG = SessionActivationByFacilityCodeApiTest.class.getSimpleName();

    public void testGetFacilityNameByFacilityCode(){
        String facilityCode = "7";

        try {
            MultipartBody multipartBody = new MultipartBody.Builder()
                    .addFormDataPart("facilityId", facilityCode)
                    .setType(MultipartBody.FORM)
                    .build();

            Request request = new Request.Builder()
                    .post(multipartBody)
                    .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_FACILITY_INFO)
                    .build();

            Response response = HttpManager.getInstance().getHttpClient().newCall(request).execute();

            int responseCode;
            JSONObject responseJSON = new JSONObject(response.body().string());
            responseCode = responseJSON.getInt("code");

            Assert.assertEquals(ResponseCodes.OK, responseCode);
            Assert.assertNotNull(responseJSON);
        }
        catch (JSONException e) {
            Log.v(TAG, "Error while parsing json.");
            e.printStackTrace();
            Assert.fail(e.getMessage());
        } catch (IOException e) {
            Log.v(TAG, "Error while processing network request.");
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    public void testGetPatientListByEnrollmentDateAndFacilityCode(){
        String enrollmentDate = "2016/3/3";
        String facilityCode = "7";

        try {
            MultipartBody multipartBody = new MultipartBody.Builder()
                    .addFormDataPart("facilityId", facilityCode)
                    .addFormDataPart("enrollmentDate", enrollmentDate)
                    .setType(MultipartBody.FORM)
                    .build();

            Request request = new Request.Builder()
                    .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_PATIENT_LIST)
                    .post(multipartBody)
                    .build();

            Response response = HttpManager.getInstance().getHttpClient().newCall(request).execute();

            int responseCode;
            JSONObject responseJSON = new JSONObject(response.body().string());
            responseCode = responseJSON.getInt("code");

            if (response.isSuccessful()) {
                if (responseCode == ResponseCodes.OK) {
                    List<EnrollmentInfo> enrollmentInfoList = new ArrayList<>();

                    JSONObject dataJSON = responseJSON.getJSONObject("data");
                    Iterator<String> dataIterator = dataJSON.keys();

                    while (dataIterator.hasNext()) {
                        JSONObject patientDataJSON = dataJSON.getJSONObject(dataIterator.next());
                        EnrollmentInfo enrollmentInfo = new EnrollmentInfo();
                        enrollmentInfo.setEnrollmentDateIso8601(patientDataJSON.getJSONObject("patient").getString("enrollmentDate"));
                        enrollmentInfo.setFirstName(patientDataJSON.getJSONObject("patient").getString("firstName"));
                        enrollmentInfo.setLastName(patientDataJSON.getJSONObject("patient").getString("lastName"));
                        enrollmentInfo.setLastNameInitial(patientDataJSON.getJSONObject("patient").getString("lastName"));

                        JSONArray sessionJSONArray = patientDataJSON.getJSONArray("session");
                        // TODO convert this setter to accept long as we don't know how much larger session ids will get.
                        enrollmentInfo.setSessionId(sessionJSONArray.getJSONObject(0).getInt("sessionId"));
                        enrollmentInfo.setFacilityName(patientDataJSON.getJSONObject("facility").getString("name"));

                        enrollmentInfoList.add(enrollmentInfo);
                    }

                    EnrollmentInfo[] infoArray = new EnrollmentInfo[enrollmentInfoList.size()];
                    for (int i = 0; i < infoArray.length; i++) {
                        infoArray[i] = enrollmentInfoList.get(i);
                    }
                    enrollmentInfoList.clear();

                    Assert.assertNotNull(infoArray);
                    if (infoArray.length == 0){
                        Assert.fail("No patient data was found to be parsed from URL.");
                    }

                } else if (responseCode == ResponseCodes.BAD_REQUEST) {
                    throw new IOException(responseJSON.getString("message"));
                }
            }
        } catch (ParseException | IOException | JSONException e) {
            Assert.fail(e.getMessage());
        }

    }
}
