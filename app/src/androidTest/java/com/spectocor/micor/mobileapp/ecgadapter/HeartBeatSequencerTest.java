package com.spectocor.micor.mobileapp.ecgadapter;

import android.content.Context;
import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainService;
import com.spectocor.testutilities.ECGDataCreator;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.utils.ArrayUtility;
import com.spectocor.testutilities.utils.NumberUtils;
import com.spectocor.testutilities.utils.ThreadUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.web.sugar.Web.onWebView;
import android.util.Log;

import junit.framework.Assert;

/**
 * Created by qwang on 2/12/2016.
 *
 * test case for checking heart beat rate animation
 *
 * reference class
 * @link HeartBeatSequencer
 *
 * CHANGES HISTORY
 *************************************************************
 * Date             Author          Comments
 * 02/17/2016       Qian            add change history and running history
 *
 *************************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 02/17/2016       Qian            fail :
 *
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class HeartBeatSequencerTest {

    //////////////////////////////////////////////////////////////
    // Simple Test Framework
    //////////////////////////////////////////////////////////////
    private static String TAG = "HeartBeatAnimationTest";//"HeartBeatSequencerTest";

    //private UITestSimpleFramework mTestFramework;
    private static AMainActivity mActivity;
    private static Context mContext;
    private static HeartBeatSequencer mHeartBeatSequencer;

    private static int mSampleRate = ECGDataCreator.SAMPLE_RATE;
    private static long mDefaultSleepMillionSecond = TestEnvGlobalConst.APP_SLEEP_MILLION_SECOND;

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init();  // launch the activity and init static variables
        }
    };

    //////////////////////////////////////////////
    // test data
    //////////////////////////////////////////////
    private int mDefaultHeartBeatVal = TestEnvGlobalConst.NORMAL_HEART_BEAT_RATE;
    private int mDefaultDisplayTime = TestEnvGlobalConst.DEFAULT_RUN_TEST_TIME;

    private static int NEW_HEART_BEAT = 1;

    @Before
    public void before(){
        // waiting activity launch
        SystemClock.sleep(mDefaultSleepMillionSecond);

        // jump to ecg signal view
        mActivity.callWebviewJsFunction("displayAdminEcgSignalPage()");
    }

    @After
    public void after(){
        // waiting tester to check UI
        SystemClock.sleep(mDefaultSleepMillionSecond);
    }

    /**
     *
     * test main activity receiving normal heart beat interval sample number to display heart beat animation
     * test data: 70
     *
     * Test Case ID : UTMAID170001
     */
    @Test
    public void testNormalHeartBeatAnimation(){

        // prepare test data
        int normalSampleInterval = ECGDataCreator.normalHeartBeatSampleInterval();

        long[] intervalSampleNumList;

        intervalSampleNumList = ArrayUtility.fillingLongArray(normalSampleInterval,mDefaultDisplayTime);

        continuousHeartBeatAnimation(intervalSampleNumList);

    }

    /**
     *
     * test main activity receiving heart beat interval sample number to display heart beat animation
     * test data: random heart beat interval [10,1000]
     *
     * Test Case ID : UTMAID170002
     */
    @Test
    public void testRandomHeartBeatSequencer(){

        // prepare test data
        long[] intervalSampleNumList;

        intervalSampleNumList = NumberUtils.mockRandomLongList(10, 1000, mDefaultDisplayTime);

        continuousHeartBeatAnimation(intervalSampleNumList);
    }

    /**
     *
     * test main activity receiving normal heart beat interval sample number to display heart beat animation
     * test data: 300
     *
     * Test Case ID : UTMAID170003
     */
    @Test
    public void testFastHeartBeatAnimation(){

        // prepare test data
        int sampleInterval = ECGDataCreator.calculateHeartBeatSampleInterval(300);

        long[] intervalSampleNumList;

        intervalSampleNumList = ArrayUtility.fillingLongArray(sampleInterval, mDefaultDisplayTime);

        continuousHeartBeatAnimation(intervalSampleNumList);

    }

    /**
     *
     * test main activity receiving normal heart beat interval sample number to display heart beat animation
     * test data: 30
     *
     * Test Case ID : UTMAID170004
     */
    @Test
    public void testSlowHeartBeatAnimation(){

        // prepare test data
        int sampleInterval = ECGDataCreator.calculateHeartBeatSampleInterval(30);

        long[] intervalSampleNumList;

        intervalSampleNumList = ArrayUtility.fillingLongArray(sampleInterval, mDefaultDisplayTime);

        continuousHeartBeatAnimation(intervalSampleNumList);

    }
    ////////////////////////////////////////////////////////////////////////
    // support function
    ////////////////////////////////////////////////////////////////////////

    /**
     * init class member variables
     */
    private void init(){

        mActivity = mActivityRule.getActivity();
        mContext  = mActivity.getApplicationContext();
        mHeartBeatSequencer = HeartBeatSequencer.getInstance(mContext);

        assert(mActivity != null);
        assert(mContext != null);
        assert(mHeartBeatSequencer != null);

        Log.d(TAG,"init");
    }

    /**
     * show one heart beat animation
     * @param PreviousSampleNumber
     */
    private void heartBeatAnimation(long PreviousSampleNumber){

        Assert.assertTrue(TAG + "incorrect Previous Sample number",PreviousSampleNumber >=0);
        mHeartBeatSequencer.obtainMessage(NEW_HEART_BEAT, PreviousSampleNumber).sendToTarget();
    }

    /**
     * show continuous heart beat animation
     * @param IntervalSampleNumberList
     */
    private void continuousHeartBeatAnimation(long[] IntervalSampleNumberList){
        long previous_sample_num = 0;
        long start,end;
        long waitTime = previous_sample_num;

        for(int i = 0; i < IntervalSampleNumberList.length; ++i){

            if(waitTime > 0) {
                SystemClock.sleep(waitTime);
                Log.d(TAG,"interval wait time:" + waitTime + "," + "interval sample number:" + IntervalSampleNumberList[i]);
            }

            start = System.currentTimeMillis();

            previous_sample_num = IntervalSampleNumberList[i];

            //rPeakInfo = new RPeakDetectedInfo(curr_hr,previous_sample_num);

            heartBeatAnimation(previous_sample_num);

            end = System.currentTimeMillis();

            //Thread.sleep((60000 / heartBeatRates) - (end - start));
            waitTime = (1000 / mSampleRate * previous_sample_num ) - (end - start);
        }
    }
}
