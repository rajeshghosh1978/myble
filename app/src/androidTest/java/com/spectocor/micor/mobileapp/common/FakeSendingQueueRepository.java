package com.spectocor.micor.mobileapp.common;

import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.common.queuesystem.SendingQueueRepositoryBase;

/**
 * Fake class for testing base sending queue functions
 */
@SuppressWarnings("WeakerAccess")
public class FakeSendingQueueRepository extends SendingQueueRepositoryBase {


    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param queueDatabaseHelper database helper
     */
    public FakeSendingQueueRepository(QueueDatabaseHelper queueDatabaseHelper) {
        super(queueDatabaseHelper, "FakeQueue");
    }


}
