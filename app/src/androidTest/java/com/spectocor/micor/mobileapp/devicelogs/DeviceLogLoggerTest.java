package com.spectocor.micor.mobileapp.devicelogs;

import android.content.Context;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;
import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.common.queuesystem.QueueItem;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;


// Look at Mokito samples: http://gojko.net/2009/10/23/mockito-in-six-easy-examples/

/**
 * Test cases for device log service
 */
@SuppressWarnings("WeakerAccess")
public class DeviceLogLoggerTest extends AndroidTestCase {

    DeviceLogRepository repository;
    GlobalSettings settings;
    DeviceLogQueueRepository queueRepository;

    DeviceLogLogger logger;

    /**
     * instantiate test classes
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        super.setUp();

        Context context = getContext();
        context.deleteDatabase(LogEventsDatabaseHelper.DatabaseName);
        context.deleteDatabase(QueueDatabaseHelper.DatabaseName);

        LogEventsDatabaseHelper databaseHelper = new LogEventsDatabaseHelper(context);
        repository = new DeviceLogRepository(databaseHelper);

        QueueDatabaseHelper queueDatabaseHelper = new QueueDatabaseHelper(context);
        queueRepository = new DeviceLogQueueRepository(queueDatabaseHelper);

        //TODO: Try to add Mokito for unit tests. It is not acceptable to use actual class for all unit tests
        //repository = Mockito.mock(DeviceLogRepository.class);

        settings = new GlobalSettings();

        logger = new DeviceLogLogger(repository, settings, queueRepository);
    }


    /**
     * tests that insert log inserts actual log to the database
     * values hasn't changed
     * monitoring session id is set
     * it got a database row id
     * it set the log time
     * and put the log in the queue
     *
     * @throws Exception if test ended with exception
     */
    public void testInsertLog() throws Exception {
        int time = (int) (System.currentTimeMillis() / 1000);
        String valueString = "Some String";
        Double valueNumber = (double) 200;
        EnumDeviceLogInfoType logInfoType = EnumDeviceLogInfoType.ReservedUndefined;
        DeviceLog log = new DeviceLog(logInfoType, valueNumber, valueString);
        logger.insertLog(log);
        assertDeviceLog(log, settings.getMonitoringSession(), time, valueString, valueNumber, logInfoType);
    }

    /**
     * asserts that log inserted correctly
     *
     * @param log                 log data
     * @param monitoringSessionId expected monitoring session id
     * @param timeStarted         expected time (to be greater than this value)
     * @param valueString         expected value string
     * @param valueNumber         expected value number
     * @param logInfoType         expected log info type
     */
    public void assertDeviceLog(DeviceLog log, int monitoringSessionId, long timeStarted, String valueString, Double valueNumber, EnumDeviceLogInfoType logInfoType) {
        assertEquals(valueString, log.getValueString());
        assertEquals(valueNumber, log.getValueNumber());
        assertEquals(logInfoType, log.getDeviceLogInfoTypeId());
        assertEquals(monitoringSessionId, log.getMonitoringSessionId());
        assertTrue(log.getDatabaseRowId() > 0);
        assertTrue(log.getLogDateUnixSec() >= timeStarted);

       /* QueueItem item = queueRepository.getById(log.getDatabaseRowId());
        assertNotNull(item); // item should be in the queue*/
    }


//
//
//    /**
//     * test log application exceptions
//     *
//     * @throws Exception if test ended with exception
//     */
//    public void testLogApplicationException() throws Exception {
//        int time = (int) (System.currentTimeMillis() / 1000);
//        DeviceLog log = service.LogApplicationException(new Exception("This is a test exception"));
//        assertEquals(log.getValueString(), "This is a test exception");
//        assertEquals(log.getDeviceLogInfoTypeId(), EnumDeviceLogInfoType.ApplicationException);
//        assertTrue(log.getLogDateUnixSec() >= time);
//    }
//
//    /**
//     * test mobile application battery status log
//     * @throws Exception if test ended with exception
//     */
//    public void testLogMobileDeviceBatteryStatus() throws Exception {
//        int time = (int) (System.currentTimeMillis() / 1000);
//        DeviceLogLogger service = new DeviceLogLogger(repository, settings);
//        DeviceLog log = service.LogMobileDeviceBatteryStatus((byte) 10);
//        assertEquals(log.getValueNumber(), 10.0);
//        assertEquals(log.getDeviceLogInfoTypeId(), EnumDeviceLogInfoType.MobileDeviceBatteryStatus);
//        assertNull(log.getValueString());
//        assertTrue(log.getLogDateUnixSec() >= time);
//    }
//
//    /**
//     * test EcgTransmitter battery status log
//     * @throws Exception if test ended with exception
//     */
//    public void testLogEcgTransmitterBatteryStatus() throws Exception {
//        int time = (int) (System.currentTimeMillis() / 1000);
//        DeviceLogLogger service = new DeviceLogLogger(repository, settings);
//        DeviceLog log = service.LogMobileDeviceBatteryStatus((byte) 100);
//        assertEquals(log.getValueNumber(), 100.0);
//        assertNull(log.getValueString());
//        assertEquals(log.getDeviceLogInfoTypeId(), EnumDeviceLogInfoType.EcgTransmitterBatteryStatus);
//        assertTrue(log.getLogDateUnixSec() >= time);
//    }
//
//    /**
//     * test mobile BluetoothOnOff log
//     * @throws Exception if test ended with exception
//     */
//    public void testLogMobileDeviceBluetoothOnOff() throws Exception {
//        int time = (int) (System.currentTimeMillis() / 1000);
//        DeviceLogLogger service = new DeviceLogLogger(repository, settings);
//        DeviceLog log = service.LogMobileDeviceBluetoothOnOff(true);
//        assertEquals(log.getValueNumber(), 1.0);
//        assertNull(log.getValueString());
//        assertEquals(log.getDeviceLogInfoTypeId(), EnumDeviceLogInfoType.MobileDeviceBluetoothOnOff);
//        assertTrue(log.getLogDateUnixSec() > time);
//    }
//
//    /**
//     * test write log with new exception
//     * @throws Exception if test ended with exception
//     */
//    public void testLogMobileDeviceUnknownType() throws Exception {
//        int time = (int) (System.currentTimeMillis() / 1000);
//        DeviceLogLogger service = new DeviceLogLogger(repository, settings);
//        Exception en = new Exception("test mobile device log exception");
//        DeviceLog log = service.LogApplicationException(en);
//        assertEquals(log.getValueNumber(), 1.0);
//        assertNull(log.getValueString());
//        assertEquals(log.getDeviceLogInfoTypeId(), EnumDeviceLogInfoType.ApplicationException);
//        assertTrue(log.getLogDateUnixSec() > time);
//    }


}