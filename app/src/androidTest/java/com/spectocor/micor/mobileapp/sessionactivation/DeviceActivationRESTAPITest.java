package com.spectocor.micor.mobileapp.sessionactivation;

import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.sessionactivation.api.EnrollmentInfo;
import com.spectocor.testutilities.session.api.SessionRESTAPI;

import org.junit.Assert;

/**
 * Created by qwang on 2/22/2016.
 *
 * test case for active session / device activation with legal activation code and illegal activation code
 *
 * test cased for SSL server
 *
 * QA  : ALGO-927
 * DEV : ALGO-672
 *
 * CHANGES HISTORY
 *************************************************************
 * Date             Author          Comments
 * 02/22/2016       Qian            create test
 *
 *************************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 02/23/2016       Qian            fail :
 *
 */
public class DeviceActivationRESTAPITest extends AndroidTestCase{

    private static String TAG = "DeviceActivationRESTAPITest";

    private static String mSuccessActivationCod = "1031";
    private static String mIllegalActivationCod = "4001";

    private static int mDefaultSessionID = 1;//1234;

    /**
     * setup
     */
    @Override
    public void setUp(){
        mContext = getContext(); // get environment
        assert(mContext != null);
        // check test environment
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(mContext);
        // session active
        //Assert.assertTrue(TAG+":Fail to start test: there is already active session at this device",sessionState.sessionActive == false);
    }

    /**
     * using http server to activation session with activation code
     *
     * input activation code
     *
     * return successful response and message : error
     *
     * Test Case ID : UTMAID210001
     */
    public void testDeviceActivationRESTAPIWithActivationCode(){
        String activationCode = mSuccessActivationCod;

        activationCode = "7500";

        EnrollmentInfo enrollmentInfo;

        enrollmentInfo = SessionRESTAPI.getEnrollmentByActivationCode(activationCode);  // send it

        Assert.assertTrue("Fail to activation code", enrollmentInfo != null);

    }

    /**
     * using http server to activation session with illegal activation code
     *
     * input illegal activation code
     *
     * return successful response and message : error
     *
     * Test Case ID : UTMAID210002
     */
    public void testDeviceActivationRESTAPIWithIllegalActivationCode(){
        String activationCode = mIllegalActivationCod;

        EnrollmentInfo enrollmentInfo;

        enrollmentInfo = SessionRESTAPI.getEnrollmentByActivationCode(activationCode);  // send it

        Assert.assertTrue("Fail to activation code", enrollmentInfo == null);

    }

    /**
     * using SSL server to activation session with activation code
     *
     * input activation code and SSL server
     *
     * return successful response and message : enrollment information
     *
     * Test Case ID : UTMAID210003
     */
    public void testDeviceActivationSSLRESTAPIWithActivationCode(){
        String activationCode = mSuccessActivationCod;

        EnrollmentInfo enrollmentInfo;

        enrollmentInfo = SessionRESTAPI.getEnrollmentByActivationCode(SessionRESTAPI.SSL_SERVER_ADDRESS,SessionRESTAPI.REST_URL_DEVICE_ACTIVATION,activationCode);  // send it

        Assert.assertTrue("Fail to activation code", enrollmentInfo != null);
    }

    /**
     * using SSL server to activation session with illegal activation code
     *
     * input activation code and SSL server
     *
     * return successful response and message : error
     *
     * Test Case ID : UTMAID210004
     */
    public void testDeviceActivationSSLRESTAPIWithIllegalActivationCode(){
        String activationCode = mSuccessActivationCod;

        EnrollmentInfo enrollmentInfo;

        enrollmentInfo = SessionRESTAPI.getEnrollmentByActivationCode(SessionRESTAPI.SSL_SERVER_ADDRESS,SessionRESTAPI.REST_URL_DEVICE_ACTIVATION,activationCode);  // send it

        Assert.assertTrue("Fail to activation code", enrollmentInfo != null);
    }

    //////////////////////////////////////////////////////////////
    // support
    //////////////////////////////////////////////////////////////


}
