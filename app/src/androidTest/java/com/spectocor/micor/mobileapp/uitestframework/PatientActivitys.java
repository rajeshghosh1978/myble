package com.spectocor.micor.mobileapp.uitestframework;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by qwang on 1/15/2016.
 */
public class PatientActivitys {

    public static int MaxiActivity = 4;
    Map<String, String> mPatientActivityList;
    String mKeys[];

    public PatientActivitys() {
        mPatientActivityList = new HashMap<String, String>();
        mPatientActivityList.put("cb-act-resting", "Resting");
        mPatientActivityList.put("cb-act-walking", "Walking");
        mPatientActivityList.put("cb-act-exercising", "Exercising");
        mPatientActivityList.put("cb-act-other", "Other");

        mKeys = new String[MaxiActivity];
        int i = 0;
        for (String Key : mPatientActivityList.keySet()) {
            mKeys[i++] = Key;
        }

    }

    public String getText(String Key) {
        return mPatientActivityList.get(Key);
    }

    public String getText(int Key) {
        return getText(mKeys[Key]);
    }

    public String getKey(int Key) {
        return mKeys[Key];
    }
}
