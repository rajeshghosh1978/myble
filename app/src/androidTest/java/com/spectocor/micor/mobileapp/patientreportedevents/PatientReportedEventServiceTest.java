package com.spectocor.micor.mobileapp.patientreportedevents;

import android.content.Context;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;
import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.common.queuesystem.QueueItem;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;

/**
 * Test cases for PatientReportedEventService class
 */
public class PatientReportedEventServiceTest extends AndroidTestCase {


    PatientReportedEventRepository repository;
    GlobalSettings settings;
    PatientReportedEventQueueRepository queueRepository;

    PatientReportedEventService service;

    /**
     * instantiate test classes
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        super.setUp();

        Context context = getContext();
        context.deleteDatabase(LogEventsDatabaseHelper.DatabaseName);
        context.deleteDatabase(QueueDatabaseHelper.DatabaseName);

        LogEventsDatabaseHelper databaseHelper = new LogEventsDatabaseHelper(context);
        repository = new PatientReportedEventRepository(databaseHelper);

        QueueDatabaseHelper queueDatabaseHelper = new QueueDatabaseHelper(context);
        queueRepository = new PatientReportedEventQueueRepository(queueDatabaseHelper);

        //TODO: Try to add Mokito for unit tests. It is not acceptable to use actual class for all unit tests
        //repository = Mockito.mock(PatientReportedEventRepository.class);

        settings = new GlobalSettings();

        service = new PatientReportedEventService(repository, settings, queueRepository);
    }


    /**
     * tests that insert log inserts actual log to the database
     * values hasn't changed
     * monitoring session id is set
     * it got a database row id
     * and put the log in the queue
     *
     * @throws Exception if test ended with exception
     */
    public void testInsertEvent() throws Exception {
        long time = System.currentTimeMillis();
        EnumPatientReportedEventType enumPatientReportedEventType = EnumPatientReportedEventType.Other;
        String comment = "Comment";

        PatientReportedEvent obj = new PatientReportedEvent();
        obj.setPatientReportedEventTypeId(enumPatientReportedEventType);
        obj.setComment(comment);

        PatientReportedEvent ch = service.insertEvent(obj);
        assertNotNull(ch);

        PatientReportedEvent insertedObj = repository.getById(obj.getLogDateUnixSec());
        assertPatientReportedEvent(insertedObj, settings.getMonitoringSession(), time, enumPatientReportedEventType, comment);
    }


    /**
     * asserts that the object inserted correctly
     *
     * @param obj                          patient reported event object
     * @param monitoringSessionId          expected monitoring session id
     * @param timeStarted                  expected time (to be greater than this value)
     * @param enumPatientReportedEventType event type
     * @param comment                      user comment
     */
    public void assertPatientReportedEvent(PatientReportedEvent obj, int monitoringSessionId, long timeStarted, EnumPatientReportedEventType enumPatientReportedEventType, String comment) {

        assertEquals(monitoringSessionId, obj.getMonitoringSessionId());
        assertTrue(((long) obj.getLogDateUnixSec() + 1) * 1000 >= timeStarted);
        assertEquals(enumPatientReportedEventType, obj.getPatientReportedEventTypeId());
        assertEquals(comment, obj.getComment());
      /*  QueueItem item = queueRepository.getById(obj.getLogDateUnixSec());
        assertNotNull(item); // item should be in the queue*/

    }


}