package com.spectocor.micor.mobileapp.common;

import android.content.Context;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.common.exceptions.DataAccessException;
import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.common.queuesystem.QueueItem;
import com.spectocor.micor.mobileapp.common.queuesystem.SendingQueueRepositoryBase;

import junit.framework.Assert;

/**
 * General Test cases for Repository object
 */
public class RepositoryBaseTest extends AndroidTestCase {

    /**
     * repository class for testing
     */
    SendingQueueRepositoryBase repository;


    /**
     * creating classes for testing
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        Context context = getContext();
        context.deleteDatabase(QueueDatabaseHelper.DatabaseName);

        QueueDatabaseHelper queueDatabaseHelper = new QueueDatabaseHelper(getContext());
        repository = new FakeSendingQueueRepository(queueDatabaseHelper);

        // creating the fake table
        queueDatabaseHelper.getWritableDatabase().execSQL(repository.getCreateTableSqlScript());
    }


    // General Repository scenarios


    /**
     * testing insert to the database (Extreme max)
     *
     * @throws Exception if test ended with exception
     */
    public void testInsertMaxValue() throws Exception {
        /*long maxValue = Long.MAX_VALUE;
        QueueItem obj = new QueueItem(maxValue);
        long m = repository.insert(obj);
        QueueItem savedObj = repository.getById(obj.getRowId());
        Assert.assertTrue(savedObj.getRowId() == maxValue);
        Assert.assertTrue(savedObj.getRowId() == obj.getRowId());*/
    }


    /**
     * testing insert to the database twice (it should return exception)
     *
     * @throws Exception if test ended with exception
     */
    public void testInsertSamePrimaryKey() throws Exception {
      /*  long maxValue = Long.MAX_VALUE;
        QueueItem obj = new QueueItem(maxValue);
        // first one shouldn't fail
        long m = repository.insert(obj);
        try {
            repository.insert(obj);   // this one should return an exception because of its primary key
            org.junit.Assert.fail();
        } catch (DataAccessException ex) {
            assertTrue(ex.getMessage().contains("insert failed"));
        }*/
    }


}