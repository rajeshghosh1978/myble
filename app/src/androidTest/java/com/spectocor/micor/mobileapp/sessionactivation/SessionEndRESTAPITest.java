package com.spectocor.micor.mobileapp.sessionactivation;

import android.content.Context;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;

import com.spectocor.micor.mobileapp.common.queuesystem.HttpsHandler;
import com.spectocor.micor.mobileapp.sessionactivation.api.SessionActivationApiOld;
import com.spectocor.testutilities.session.api.RESTAPIResponseMessage;
import com.spectocor.testutilities.session.api.SessionEndRESTAPI;
import com.spectocor.testutilities.session.api.SessionRESTAPI;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import android.util.Log;


/**
 * Created by qwang on 3/11/2016.
 *
 * {"version":"1.0","data":"","code":401,"message":"UnauthorizedError"}
 *
 */
public class SessionEndRESTAPITest extends AndroidTestCase{

    private static String TAG = "SessionEndRESTAPITest";

    private static Context mContext;
    private static CurrentSessionState mSessionState;

    private static String mDefaultSessionID = "100005824";

    /**
     * Creates required objects for testing
     *
     * @throws Exception if it couldn't create files
     */
    @Before
    public void setUp() throws Exception {

        super.setUp();

        init();
    }


    ///////////////////////////////////////////////////////
    // test case
    ///////////////////////////////////////////////////////

    /**
     * succeed remove activation session
     */
    @Test
    public void testCurrentSessionEndRESTAPI(){

        String rc;

        String sessionID ;

        if(mSessionState.sessionActive){

            sessionID = Integer.toString(mSessionState.sessionId);

            Log.d(TAG,"call session end API to end session:"+sessionID);

            rc = SessionEndRESTAPI.sessionEnd(sessionID);  // send it

            Assert.assertTrue("Fail to end session", rc.compareTo(RESTAPIResponseMessage.SUCCESS) == 0);

        }
        else
            Log.d(TAG,"There is no active session on the mobile phone");

    }

    /*
    public void testSession1130RemoveRESTAPI(){
        String sessionID = "1130";

        String rc;

        rc = SessionEndRESTAPI.sessionEnd(sessionID);  // send it

        Assert.assertTrue("Fail to end session", rc.compareTo(RESTAPIResponseMessage.ERROR)==0);

    }

    @Test
    public void testSessionEndRESTAPI(){
        String sessionID = "1";

        String rc;

        rc = SessionEndRESTAPI.sessionEnd(sessionID);  // send it

        Assert.assertTrue("Fail to end session", rc.compareTo(RESTAPIResponseMessage.SUCCESS)==0);

        Log.d(TAG, SessionStateAccess.getCurrentActivationStateJson(mContext));
    }*/


    /////////////////////////////////////////////////////
    // init
    /////////////////////////////////////////////////////
    private void init(){

        mContext = getContext();

        mSessionState = SessionStateAccess.getCurrentSessionState(mContext);

    }

}
