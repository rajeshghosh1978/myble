package com.spectocor.micor.mobileapp.devicelogs;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.LargeTest;

import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;
import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.common.queuesystem.QueueItem;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;


/**
 * Tests Application logs service
 */
@SuppressWarnings({"SameParameterValue", "WeakerAccess"})
public class ApplicationLogServiceTest extends AndroidTestCase {

    DeviceLogRepository repository;
    GlobalSettings settings;
    DeviceLogQueueRepository queueRepository;
    DeviceLogLogger logger;

    ApplicationLogService service;

    /**
     * instantiate test classes
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        super.setUp();

        Context context = getContext();
        context.deleteDatabase(LogEventsDatabaseHelper.DatabaseName);
        context.deleteDatabase(QueueDatabaseHelper.DatabaseName);

        LogEventsDatabaseHelper databaseHelper = new LogEventsDatabaseHelper(context);
        repository = new DeviceLogRepository(databaseHelper);

        QueueDatabaseHelper queueDatabaseHelper = new QueueDatabaseHelper(context);
        queueRepository = new DeviceLogQueueRepository(queueDatabaseHelper);

        //TODO: Try to add Mokito for unit tests. It is not acceptable to use actual class for all unit tests
        //repository = Mockito.mock(DeviceLogRepository.class);

        settings = new GlobalSettings();

        logger = new DeviceLogLogger(repository, settings, queueRepository);

        service = new ApplicationLogService(logger);
    }


    public void testLogApplicationException() throws Exception {
        String exMessage = "This is a test exception.";
        DeviceLog log = service.logApplicationException(new Exception(exMessage));
        assertDeviceLog(log, settings.getMonitoringSession(), log.getValueString(), null, EnumDeviceLogInfoType.ApplicationException);
        assertTrue(log.getValueString().contains(exMessage));
    }


    public void testLogApplicationStarted() throws Exception {
        DeviceLog log = service.logApplicationStarted();
        assertDeviceLog(log, settings.getMonitoringSession(), null, null, EnumDeviceLogInfoType.ApplicationStarted);
    }


    public void testLogApplicationEnded() throws Exception {
        DeviceLog log = service.logApplicationEnded();
        assertDeviceLog(log, settings.getMonitoringSession(), null, null, EnumDeviceLogInfoType.ApplicationEnded);
    }


    public void testLogApplicationCrashed() throws Exception {
        DeviceLog log = service.logApplicationCrashed();
        assertDeviceLog(log, settings.getMonitoringSession(), null, null, EnumDeviceLogInfoType.ApplicationCrashed);
    }


    public void testLogBuildInfo() throws Exception {
        DeviceLog log = service.logBuildInfo();
        DeviceLogBuildInfo info = ObjectUtils.fromJson(log.getValueString(), DeviceLogBuildInfo.class);
        assertNotNull(info);
        assertEquals(EnumDeviceLogInfoType.BuildInfo, log.getDeviceLogInfoTypeId());
        assertNull(log.getValueNumber());
        assertTrue(log.getDatabaseRowId() > 0);
    }


    /*
    * test write log with a new user exception
    * Test Case ID : UTMAID000019
    */
    @LargeTest
    public void testLogMobileDeviceUserException() throws Exception {
        int time = (int) (System.currentTimeMillis() / 1000);
        String exception_str = "test mobile device log exception";
        Exception en = new Exception(exception_str);
        DeviceLog log = service.logApplicationException(en);
        assertTrue(log.getValueString().contains(exception_str));
        assertNull(log.getValueNumber());
        assertEquals(log.getDeviceLogInfoTypeId(), EnumDeviceLogInfoType.ApplicationException);
        assertTrue(log.getLogDateUnixSec() >= time);
    }

    /**
     * asserts that log inserted correctly
     *
     * @param log                 log data
     * @param monitoringSessionId expected monitoring session id
     * @param valueString         expected value string
     * @param valueNumber         expected value number
     * @param logInfoType         expected log info type
     */
    private void assertDeviceLog(DeviceLog log, int monitoringSessionId, String valueString, Double valueNumber, EnumDeviceLogInfoType logInfoType) {
        assertEquals(valueString, log.getValueString());
        assertEquals(valueNumber, log.getValueNumber());
        assertEquals(logInfoType, log.getDeviceLogInfoTypeId());
        assertEquals(monitoringSessionId, log.getMonitoringSessionId());
        assertTrue(log.getDatabaseRowId() > 0);

      //  QueueItem item = queueRepository.getById(log.getDatabaseRowId());
   //     assertNotNull(item); // item should be in the queue
    }


}