package com.spectocor.micor.mobileapp.patientreportedevents;

import android.content.Context;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;
import com.spectocor.micor.mobileapp.common.exceptions.DataAccessException;
import com.spectocor.testutilities.TestUtils;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by qwang on 12/29/2015.
 * Test case for patient reported event repository
 * <p/>
 * Attention: so far need sleep 1 second and then can insert a new patient event. otherwise will return -1 and fail to insert a new patient event
 * each patient event only include one symptom
 * <p/>
 * allow programmer to create a new event and insert later. maybe have potential problems.
 * For example, create an event and change event time, then insert this event to DB. Maybe will fail if time didn't set correct.
 */

public class PatientReportedEventRepositoryTest extends AndroidTestCase {

    static final int mMaxListLen = 100;  // length of list
    static final int mFailEventSessionID = 1000;
    static final int mSleepTime = 1000;  // 1000 millionsesonds is one second

    static final EnumPatientReportedEventType mTestOtherEventType = EnumPatientReportedEventType.Other;
    static final int mTestSessionID = 100;
    static final String mTestComment = "Patient Reported Event Type is Other";

    static PatientReportedEventRepository repository;
    List<PatientReportedEvent> mTestEvents; // list of patient report event

    /*
    * compare log list
     */
    public static void compareEvents(List<PatientReportedEvent> ExpectedEventList, List<PatientReportedEvent> RealEventList) {

        int expected_list_len = ExpectedEventList.size();
        int real_list_len = RealEventList.size();

        //boolean rc;

        if (expected_list_len != real_list_len) {
            //rc = false;
            assertTrue(false);
        } else {
            //rc = true;
            for (int i = 0; i < expected_list_len; ++i) {
                compareEvent(ExpectedEventList.get(i), RealEventList.get(i));
            }
        }

        //Assert.assertTrue("Fail at comparing Device Logs.", rc);

        //return rc;
    }

    /*
    * compare log
     */
    public static void compareEvent(PatientReportedEvent ExpectedEvent, PatientReportedEvent RealEvent) {

        //boolean rc ;

        assertEquals(ExpectedEvent.getMonitoringSessionId(), RealEvent.getMonitoringSessionId());
        assertEquals(ExpectedEvent.getLogDateUnixSec(), RealEvent.getLogDateUnixSec());
        assertEquals(ExpectedEvent.getPatientReportedEventTypeId(), RealEvent.getPatientReportedEventTypeId());
        assertEquals(ExpectedEvent.getComment(), RealEvent.getComment());

        System.out.println("pass " + RealEvent.getMonitoringSessionId());

        //rc = true;

        //return rc;
    }

    /**
     * setting up test environment
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        super.setUp();

        Context context = getContext();
        context.deleteDatabase(LogEventsDatabaseHelper.DatabaseName);

        LogEventsDatabaseHelper databaseHelper = new LogEventsDatabaseHelper(context);
        repository = new PatientReportedEventRepository(databaseHelper);

        mTestEvents = new ArrayList<>();
    }

    /*
    * insert a single patient trigger event item to repository
    * Test Case ID : UTMAID010006
     */
    public void testSingleSuccessInsert() throws Exception {

        PatientReportedEvent event = new PatientReportedEvent(mTestSessionID, mTestOtherEventType, mTestComment);
        long m = repository.insert(event);
        Assert.assertTrue(m != -1);
    }

    /*
    * delete a single patient trigger event item from repository
    * Test Case ID : UTMAID010007
     */
    public void testSingleSuccessDelete() throws Exception {
        PatientReportedEvent event = new PatientReportedEvent(mTestSessionID, mTestOtherEventType, mTestComment);
        long m = repository.insert(event);
        try {
            repository.delete(event.getLogDateUnixSec());
            Assert.assertTrue(true);
        } catch (DataAccessException ex) {
            Assert.assertTrue(false);
        }
    }

//    @Test
//    public void testUpdate() throws Exception {
//
//    }

    /*
    * get all patient events from repository
    * Test Case ID : UTMAID010008
     */
    public void testGetAllEvents() throws Exception {
        PatientReportedEvent event = new PatientReportedEvent(mTestSessionID, mTestOtherEventType, mTestComment);
        long m = repository.insert(event);
        Assert.assertTrue(m != -1);
        List<PatientReportedEvent> list = repository.getAll(1000);
        Assert.assertTrue(list.size() == 1);
    }

    /*
    * get a patient event from repository with Id
    * Test Case ID : UTMAID010009
     */
    public void testGetById() throws Exception {

        PatientReportedEvent event = new PatientReportedEvent(mTestSessionID, mTestOtherEventType, mTestComment);
        long m1 = repository.insert(event);
        Thread.sleep(mSleepTime);
        long m2 = repository.insert(new PatientReportedEvent(mTestSessionID, mTestOtherEventType, mTestComment));
        Thread.sleep(mSleepTime);
        long m3 = repository.insert(new PatientReportedEvent(mTestSessionID, mTestOtherEventType, mTestComment));

        Assert.assertTrue(m1 != -1);
        Assert.assertTrue(m2 != -1);
        Assert.assertTrue(m3 != -1);

        PatientReportedEvent event1 = repository.getById(m1);
        TestUtils.AssertEqualsJson(event1, event);
    }

    /*
    * random create patient event list
     */
    void createTestEventList() {

        PatientReportedEvent event;
        Random rand = new Random();
        int log_id;
        int log_num_val;

        try {
            for (int index = 1; index < mMaxListLen; ++index) {
                log_num_val = rand.nextInt(mMaxListLen) + 1;
                log_id = rand.nextInt(mMaxListLen) + 1;
                event = new PatientReportedEvent(mTestSessionID, mTestOtherEventType, mTestComment);
                Thread.sleep(mSleepTime);
                mTestEvents.add(event);
            }
        } catch (InterruptedException e) {
            Assert.fail("create event thread has interrupted exception");
        }

    }

    /*
    * insert batch events
     */
    public int batchInsertEvents(List<PatientReportedEvent> EventList) {
        int log_len = EventList.size();
        long db_row_id;
        if (log_len > 0) {
            try {
                for (int i = 0; i < log_len; ++i) {
                    db_row_id = repository.insert(EventList.get(i));
                    Assert.assertTrue(db_row_id != -1);
                }
            } catch (DataAccessException ex) {
                Assert.fail();
            }

        }

        return log_len;
    }

    /*
    * get all event
     */
    public List<PatientReportedEvent> GetAllEvents() throws Exception {
        List<PatientReportedEvent> events;
        events = repository.getAll(mMaxListLen);
        return events;
    }

    /*
    * test batch insert event
    * Test Case ID : UTMAID010010
    */
    public void testEventRepositoryInsert() throws Exception {
        createTestEventList();
        int expected_log_num = batchInsertEvents(mTestEvents);
        List<PatientReportedEvent> realEvents = GetAllEvents();
        compareEvents(mTestEvents, realEvents);
    }

    /*
    * test get event which is not at repository
    * Expect to return null
    * Test Case ID : UTMAID010011
     */
    public void testEventRepositoryGetNoFound() {
        long not_event_id = mFailEventSessionID;
        PatientReportedEvent event;

        try {
            event = repository.getById(not_event_id);
            Assert.assertEquals(null, event);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Fail at getting patient event with wrong event id:" + not_event_id);
        }
    }

    /*
    * test multiple delete vents
    * Test Case ID : UTMAID010012
    */
    public void testEventRepositoryDelete() throws Exception {
        createTestEventList();
        int expected_log_num = batchInsertEvents(mTestEvents);
        List<PatientReportedEvent> realEvents = GetAllEvents();
        PatientReportedEvent event;
        boolean rc;

        try {
            for (int i = 0; i < realEvents.size(); ++i) {
                event = realEvents.get(i);
                Assert.assertNotNull(event);
                repository.delete(event.getLogDateUnixSec());
            }
            realEvents = GetAllEvents();
            Assert.assertEquals(0, realEvents.size());
        } catch (DataAccessException ex) {
            Assert.fail();
        }

    }

    /*
    * test delete event which isn't available at repository
    * Test Case ID : UTMAID010013
     */
    public void testRepositoryDeleteNoFound() {
        long not_event_id = mFailEventSessionID;
        PatientReportedEvent event = null;
        boolean rc;

        try {
            repository.delete(not_event_id);
            Assert.assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Fail at delete device log object with wrong id:" + not_event_id);
        }
    }

    /**
     * test insert same events
     * expect to throw exception
     * test result return -1 for failing insert events
     * Test Case ID : UTMAID010014
     */
    public void testInsertDuplicateEvents() {
        PatientReportedEvent event = new PatientReportedEvent(mTestSessionID, mTestOtherEventType, mTestComment);

        long m1, m2, m3;
        m1 = 0;
        m2 = 0;
        m3 = 0;

        try {
            m1 = repository.insert(event);
            m2 = repository.insert(event);
            m3 = repository.insert(event);

            Assert.assertTrue("Fail at insert duplicate events", m1 != -1);
            Assert.assertTrue("Fail at insert duplicate events", m2 == -1);
            Assert.assertTrue("Fail at insert duplicate events", m3 == -1);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.assertTrue("", true);
        }

    }
}
