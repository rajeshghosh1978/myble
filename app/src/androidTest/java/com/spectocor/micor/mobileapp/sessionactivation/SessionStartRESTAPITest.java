package com.spectocor.micor.mobileapp.sessionactivation;

import android.content.Context;
import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.AMainJSInterface;
import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.http.HttpManager;
import com.spectocor.micor.mobileapp.http.common.ResponseCodes;
import com.spectocor.micor.mobileapp.sessionactivation.api.SessionActivationApi;
import com.spectocor.testutilities.ECGDataCreator;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.session.api.SessionStartRESTAPI;

import junit.framework.Assert;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.Response;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 2/29/2016.
 * call REST API for testing purpose
 *
 * https://gateway.spectocor.com/en/v1/session/start
 *
 *
 * start session with the following information
 *
 * "sessionId":"12345",
 * "startTimestamp":1438378778000,
 * "ecgId":"<ecg device identifier>",
 * "mobileId":"<mobile device identifier>",
 * "ecgChunkSize":"300000",
 * "samplingRate":"250"
 *
 * copy of source code from SessionActivationApi.startSessionCall
 *
 * CHANGES HISTORY
 *************************************************************
 * Date             Author          Comments
 * 02/29/2016       Qian            create test case for session start REST API
 *
 *************************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 02/29/2016       Qian            Pass :
 *
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class SessionStartRESTAPITest {

    private static String TAG = "SessionStartRESTAPITest";


    private static AMainActivity mActivity;
    private static Context mContext;
    private static AMainJSInterface mMainJSInterface;
    private static CurrentSessionState mCurrentSessionState;


    private static int mSampleRate = ECGDataCreator.SAMPLE_RATE;
    private static long mDefaultSleepMillionSecond = TestEnvGlobalConst.APP_SLEEP_MILLION_SECOND;


    private static String mJSCallbackFn = "sessionStartAPIFinished";

    //private static String mSessionStatus1234JSON = "{\"ecgBtConfirmed\":true,\"ecgBtMac\":\"B0:B4:48:C4:A9:89\",\"ecgBtName\":\"STL ECG 201\",\"ecgChosen\":false,\"ecgChosenAtShipping\":true,\"enrollmentConfirmed\":true,\"enrollmentDate\":\"2016-01-19\",\"enrollmentId\":\"\",\"facilityId\":\"\",\"facilityName\":\"Facility A\",\"heartBeatDetectionTestComplete\":true,\"heartRateDetected\":true,\"isSalesAgent\":false,\"patientConfirmed\":true,\"patientName\":\"John D\",\"sessionActivationDate\":\"2016-02-19T20:07:14.131Z\",\"sessionActive\":true,\"sessionId\":1,\"sessionReactivation\":true,\"sessionTerminated\":false,\"signalDetected\":true,\"summaryConfirmed\":false}";
    private static String mSessionStatus1234JSON = "{\"ecgBtConfirmed\":true,\"ecgBtMac\":\"B0:B4:48:C4:A9:89\",\"ecgBtName\":\"STL ECG 201\",\"ecgChosen\":false,\"ecgChosenAtShipping\":true,\"enrollmentConfirmed\":true,\"enrollmentDate\":\"2016-01-19\",\"enrollmentId\":\"\",\"facilityId\":\"\",\"facilityName\":\"Facility A\",\"heartBeatDetectionTestComplete\":true,\"heartRateDetected\":true,\"isSalesAgent\":false,\"patientConfirmed\":true,\"patientName\":\"John D\",\"sessionActivationDate\":\"2016-02-19T20:07:14.131Z\",\"sessionActive\":true,\"sessionId\":1,\"sessionReactivation\":false,\"sessionTerminated\":false,\"signalDetected\":true,\"summaryConfirmed\":false}";
    private static String mSessionStatusReactivationJSON = "{\"ecgBtConfirmed\":true,\"ecgBtMac\":\"B0:B4:48:C4:A9:89\",\"ecgBtName\":\"STL ECG 201\",\"ecgChosen\":false,\"ecgChosenAtShipping\":true,\"enrollmentConfirmed\":true,\"enrollmentDate\":\"2016-01-19\",\"enrollmentId\":\"\",\"facilityId\":\"\",\"facilityName\":\"Facility A\",\"heartBeatDetectionTestComplete\":true,\"heartRateDetected\":true,\"isSalesAgent\":false,\"patientConfirmed\":true,\"patientName\":\"John D\",\"sessionActivationDate\":\"2016-02-19T20:07:14.131Z\",\"sessionActive\":true,\"sessionId\":1,\"sessionReactivation\":true,\"sessionTerminated\":false,\"signalDetected\":true,\"summaryConfirmed\":false}";

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init();  // launch the activity and init static variables
        }
    };

    @Before
    public void before(){
        // waiting activity launch
        SystemClock.sleep(mDefaultSleepMillionSecond);

        // set session state with JSON string

        // jump to start session home page
        // mActivity.callWebviewJsFunction("displayAdminEcgSignalPage()");
    }

    @After
    public void after(){
        // waiting tester to check UI
        SystemClock.sleep(mDefaultSleepMillionSecond);
    }

    /**
     * test session start REST API with session ID 1
     *
     * Test Case ID : UTMAID250001
     */
    @Test
    public void testSessionStartRESTAPI(){

        SessionStateAccess.saveSessionStateJson(mContext, mSessionStatus1234JSON); // set application status

        String sessionID = "1";//mBasePreferenceHelper.getActiveSessionId();

        SessionActivationApi sessionActivationApi = SessionActivationApi.getInstance(mContext);

        String message = sessionActivationApi.startSessionCall(sessionID, DeviceIdentifiers.getEcgId(), DeviceIdentifiers.getPdaId(), EcgProperties.DEVICE_CHUNK_SIZE, mJSCallbackFn);

        Log.d(TAG, "response message:" + message);

        Assert.assertTrue(TAG + ": Fail to call Session Start REST API", message.compareTo("success") == 0);

    }

    /**
     * test session start REST API with exist session ID 1, Session Already started
     *
     * Test Case ID : UTMAID250002
     */
    @Test
    public void testSessionStartRESTAPIWithExistSession(){

        // check test environment
        SessionStateAccess.saveSessionStateJson(mContext, mSessionStatus1234JSON); // set application status

        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(mContext);
        // session active
        org.junit.Assert.assertTrue(TAG + ":Fail to start test: there is no active session at this device", sessionState.sessionActive);

        String sessionID = Integer.toString(sessionState.sessionId);//mBasePreferenceHelper.getActiveSessionId();

        Log.d(TAG,"Session ID:" + sessionID);

        Log.d(TAG, ObjectUtils.toJson(sessionState));

        SessionActivationApi sessionActivationApi = SessionActivationApi.getInstance(mContext);

        String message = sessionActivationApi.startSessionCall(sessionID, DeviceIdentifiers.getEcgId(), DeviceIdentifiers.getPdaId(), EcgProperties.DEVICE_CHUNK_SIZE, mJSCallbackFn);

        Log.d(TAG,"response message:" + message);

        Log.d(TAG, ObjectUtils.toJson(sessionState));

        Assert.assertTrue(TAG + ": Fail to call Session Start REST API", message.compareTo("error") == 0);

    }

    /**
     * test session start REST API with exist session ID 1, Session Already started and reactivate session --- replace device case
     *
     * Test Case ID : UTMAID250003
     */
    @Test
    public void testSessionReactivationRESTAPI(){

        // check test environment
        SessionStateAccess.saveSessionStateJson(mContext, mSessionStatusReactivationJSON); // set application status

        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(mContext);
        // session active
        org.junit.Assert.assertTrue(TAG + ":Fail to start test: there is no active session at this device", sessionState.sessionActive);

        String sessionID = Integer.toString(sessionState.sessionId);//mBasePreferenceHelper.getActiveSessionId();

        Log.d(TAG,"Session ID:" + sessionID);

        Log.d(TAG, ObjectUtils.toJson(sessionState));

        SessionActivationApi sessionActivationApi = SessionActivationApi.getInstance(mContext);

        String message = sessionActivationApi.startSessionCall(sessionID, DeviceIdentifiers.getEcgId(), DeviceIdentifiers.getPdaId(), EcgProperties.DEVICE_CHUNK_SIZE, mJSCallbackFn);

        Log.d(TAG,"response message:" + message);

        Log.d(TAG, ObjectUtils.toJson(sessionState));

        Assert.assertTrue(TAG + ": Fail to call Session Start REST API", message.compareTo("success") == 0);

    }

    /**
     * test session start REST API with non exist session ID 1
     *
     * Test Case ID : UTMAID250004
     */
    @Test
    public void testSessionNotExistRESTAPI(){

        // check test environment

        String sessionID = "9999";

        Log.d(TAG,"Session ID:" + sessionID);

        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(mContext);

        Log.d(TAG, ObjectUtils.toJson(sessionState));

        SessionActivationApi sessionActivationApi = SessionActivationApi.getInstance(mContext);

        String message = sessionActivationApi.startSessionCall(sessionID, DeviceIdentifiers.getEcgId(), DeviceIdentifiers.getPdaId(), EcgProperties.DEVICE_CHUNK_SIZE, mJSCallbackFn);

        Log.d(TAG,"response message:" + message);

        Log.d(TAG, ObjectUtils.toJson(sessionState));

        Assert.assertTrue(TAG + ": Fail to call Session Start REST API", message.compareTo("Error: SocketTimeoutException: timeout") == 0);  // Error: SocketTimeoutException: timeout

    }

    /**
     * test session start REST API without network connection
     * give another server name
     *
     * Test Case ID : UTMAID250005
     */
    @Test
    public void testSessionRESTAPIWithoutNetwork(){

        // check test environment

        String sessionID = "1";
        boolean deviceReplacement = false;

        Log.d(TAG,"Session ID:" + sessionID);

        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(mContext);

        Log.d(TAG, ObjectUtils.toJson(sessionState));

        String message = "";

        try {
            Response response = SessionStartRESTAPI.SendingSessionStartRequestCall(TestEnvGlobalConst.FAIL_SERVER_ADDRESS, TestEnvGlobalConst.REST_URL_SESSION_START, sessionID,
                    DeviceIdentifiers.getEcgId(), DeviceIdentifiers.getPdaId(), deviceReplacement);

            message = response.message();

            Log.d(TAG, "response message:" + message);

            Log.d(TAG, ObjectUtils.toJson(sessionState));

            Assert.assertTrue(TAG + ": Fail to call Session Start REST API", message.compareTo("Error: SocketTimeoutException: timeout") == 0);  // Error: SocketTimeoutException: timeout
        }catch (IOException e){
            Log.d(TAG,""+e.getMessage());
            message = e.getMessage();
            Assert.assertTrue(TAG + ":Fail to catch network exception", message.contains("Unable to resolve host"));
        }
    }

    /**
     * test rest API with SSL
     * give another server name
     *
     * * Test Case ID : UTMAID250006
     */
    @Test
    public void testSessionRESTAPIWithSSL(){

        // check test environment
        SessionStateAccess.saveSessionStateJson(mContext, mSessionStatus1234JSON); // set application status

        String sessionID = "1";//mBasePreferenceHelper.getActiveSessionId();

        boolean deviceReplacement = false;

        Log.d(TAG,"Session ID:" + sessionID);

        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(mContext);

        Log.d(TAG, ObjectUtils.toJson(sessionState));

        String message = "";

        try {
            Response response = SessionStartRESTAPI.SendingSessionStartRequestCall(TestEnvGlobalConst.SSL_SERVER_ADDRESS, TestEnvGlobalConst.REST_URL_SESSION_START, sessionID,
                    DeviceIdentifiers.getEcgId(), DeviceIdentifiers.getPdaId(), deviceReplacement);

            message = response.message();

            Log.d(TAG, "response message:" + message);

            String responseBody = response.body().string();
            JSONObject responseJSON = new JSONObject(responseBody);
            int responseCode = responseJSON.getInt("code");

            Log.d(TAG, "response message code:" + responseCode);

            Log.d(TAG, ObjectUtils.toJson(sessionState));

            Assert.assertTrue(TAG + ": Fail to call Session Start REST API", responseCode == 208 || responseCode == 200 );  // Error: SocketTimeoutException: timeout
        } catch (IOException e) {
            Log.d(TAG,""+e.getMessage());
            message = e.getMessage();
            Assert.assertTrue(TAG + ":Fail to catch network exception", message.contains("Unable to resolve host"));
        }catch (JSONException je){
            Log.d(TAG,""+je.getMessage());
            message = je.getMessage();
            Assert.fail(TAG + ":Fail to catch network exception:" + message);
        }
    }

    @Test
    public void testGetFacilityNameByFacilityCode(){
        String facilityCode = "7";

        try {
            MultipartBody multipartBody = new MultipartBody.Builder()
                    .addFormDataPart("facilityId", facilityCode)
                    .setType(MultipartBody.FORM)
                    .build();

            Request request = new Request.Builder()
                    .post(multipartBody)
                    .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_FACILITY_INFO)
                    .build();

            Response response = HttpManager.getInstance().getHttpClient().newCall(request).execute();

            int responseCode;
            JSONObject responseJSON = new JSONObject(response.body().string());
            responseCode = responseJSON.getInt("code");

            Assert.assertEquals(ResponseCodes.OK, responseCode);
            Assert.assertNotNull(responseJSON);
        }
        catch (JSONException e) {
            Log.v(TAG, "Error while parsing json.");
            e.printStackTrace();
        } catch (IOException e) {
            Log.v(TAG, "Error while processing network request.");
            e.printStackTrace();
        }
    }



    ////////////////////////////////////////////////////////////////////////
    // support function
    ////////////////////////////////////////////////////////////////////////

    /**
     * init class member variables
     */
    private void init(){

        mActivity = mActivityRule.getActivity();
        mContext  = mActivity.getApplicationContext();
        mMainJSInterface = new AMainJSInterface(mContext);
        mCurrentSessionState = SessionStateAccess.getCurrentSessionState(mContext); // get current session state


        assert(mActivity != null);
        assert(mContext != null);
        assert(mMainJSInterface != null);
        assert(mCurrentSessionState != null);

        Log.d(TAG, "init");
    }
}
