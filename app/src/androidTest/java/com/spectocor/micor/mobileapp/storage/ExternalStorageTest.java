package com.spectocor.micor.mobileapp.storage;

import android.content.Context;
import android.os.Environment;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.ecgadapter.HeartBeatSequencer;
import com.spectocor.testutilities.storage.ExternalStorage;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 3/8/2016.
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class ExternalStorageTest {

    private static String TAG = "ExternalStorageTest";

    private static AMainActivity mActivity;
    private static Context mContext;

    public void testIsExternalStorageAvailable(){

        boolean rc = ExternalStorage.isAvailable();

        Assert.assertTrue(TAG+":Fail to detect external storage",rc);
    }

    public void testExternalStoragePath(){

        String expectedExternalStoragePath = "/storage/emulated/0/";

        String realExternalStoragePath = ExternalStorage.getSdCardPath();

        Assert.assertTrue(TAG+":Fail to compare external storage path:expected path:" + expectedExternalStoragePath + ":real path:"+realExternalStoragePath,expectedExternalStoragePath.compareTo(realExternalStoragePath)==0);
    }

    public void testExternalStorageWritable(){

        boolean rc = ExternalStorage.isWritable();

        Assert.assertTrue(TAG+":Fail to pass writable check",rc);
    }

    public void testListAllExternalStoragePath(){

        Map<String, File> maps = ExternalStorage.getAllStorageLocations();

        for(String key : maps.keySet()){

            Log.d(TAG,"Key:"+key+":"+maps.get(key).getPath());

        }
    }

    public void testExternalStorageAPI(){

        Log.d(TAG, "Environment.getDataDirectory().getAbsolutePath():" + Environment.getDataDirectory().getAbsolutePath());

        Log.d(TAG, "Environment.getExternalStorageDirectory().getAbsolutePath():" + Environment.getExternalStorageDirectory().getAbsolutePath());

        Log.d(TAG, "Environment.getExternalStorageState():" + Environment.getExternalStorageState());

        Log.d(TAG, "Environment.getRootDirectory().getAbsolutePath():" + Environment.getRootDirectory().getAbsolutePath());

        Log.d(TAG, "Environment.isExternalStorageEmulated():" + Environment.isExternalStorageEmulated());

        Log.d(TAG, "Environment.isExternalStorageRemovable():" + Environment.isExternalStorageRemovable());

        Log.d(TAG, "getContext().getExternalFilesDir(null):" + mContext.getExternalFilesDir(null));

    }

    @Test
    public void testSDDBPath(){

        String dbPath = ExternalStorage.determineDbPathProgrammatically(mContext);

        Log.d(TAG,"" + dbPath);

        ExternalStorage.createFile(mActivity, new File(dbPath).getParentFile(), "ttt.txt");
    }

    public void testDumpAllExternalStoragePath(){



        File[] writeableDirs = mContext.getExternalFilesDirs(null);

        Log.d(TAG, "Path size:" + writeableDirs.length);

        for(File file : writeableDirs){
            Log.d(TAG,">>>");
            Log.d(TAG,"getExternalFilesDir():" + file.getAbsolutePath());
            Log.d(TAG,"isExternalStorageRemovable(File):" + Environment.isExternalStorageRemovable(file));
            Log.d(TAG,"isisExternalStorageEmulated(File):" + Environment.isExternalStorageEmulated(file));
            Log.d(TAG,"getExternalStorageState(File path):" + ExternalStorage.isWritable(file));
        }
    }

    public void testRetrieveSDPath(){

        String[] paths = ExternalStorage.getStorageDirectories();

        for(String path : paths){
            Log.d(TAG,">>>")    ;
            Log.d(TAG,"SD path:"+path);
        }
    }

    @Test
    public void testCheckExternalPermission(){

        boolean rc = ExternalStorage.checkWriteExternalPermission(mContext);

        Assert.assertTrue("Fail to pass external permission",rc);

        rc = ExternalStorage.checkReadExternalPermission(mContext);

        Assert.assertTrue("Fail to pass external permission",rc);

    }
    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init();  // launch the activity and init static variables
        }
    };

    /**
     * init class member variables
     */
    private void init(){

        mActivity = mActivityRule.getActivity();
        mContext  = mActivity.getApplicationContext();

        assert(mActivity != null);
        assert(mContext != null);

        Log.d(TAG,"init");
    }
}
