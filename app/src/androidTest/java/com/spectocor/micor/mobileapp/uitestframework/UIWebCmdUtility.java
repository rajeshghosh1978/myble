package com.spectocor.micor.mobileapp.uitestframework;


import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.web.deps.guava.collect.Maps;
import android.support.test.espresso.web.sugar.Web;
import android.support.test.espresso.web.webdriver.DriverAtoms;
import android.support.test.espresso.web.webdriver.Locator;

import java.util.Map;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.web.sugar.Web.onWebView;
import static android.support.test.espresso.web.webdriver.DriverAtoms.findElement;
import static android.support.test.espresso.web.webdriver.DriverAtoms.getText;
import android.util.Log;
/**
 * Created by qwang on 1/12/2016.
 */
public class UIWebCmdUtility {

    private static String TAG = "UIWebCmdUtility";

    public static String UIContextPageID = "current_page_id_placeholder";   // hidden input element which keep current active page ID


    private static long mWaitingTime = 3000;

    /**
     * get current web view content
     */
    public static String getCurrentWebViewContent() {
        String web_content;

        Web.WebInteraction element = onWebView().withElement(findElement(Locator.TAG_NAME, "body")).perform(getText());

        web_content = element.get().toString();

        return web_content;
    }

    /**
     * wait page to render on the screen until timeout
     *
     * @param PageID  web page ID
     * @param Timeout timeout
     * @return
     */
    public static boolean CheckWebPageByID(String PageID, long Timeout) {

        boolean rc = false;

        long curr_time = 0;

        // Atom page;
        // Web.WebInteraction web_page;
        String page_text;
        while (curr_time < Timeout) {
            try {
                //web_page= onWebView().withElement(findElement(Locator.ID, PageID));
                //web_page = onWebView().check(webContent(hasElementWithId(PageID)));
                page_text = getWebElementTextByID(PageID);

                if (page_text != null) {

                    if (!page_text.isEmpty()) {
                        rc = true;
                        break;
                    }
                }

                Thread.sleep(mWaitingTime);

                curr_time += mWaitingTime;

            } catch (Exception e) {
                //System.out.println("Something wrong at waiting for element");
                curr_time += mWaitingTime;
                rc = false;
            }
        }


        return rc;

    }

    /**
     * check current page contain expected text or not
     * if not, test fail
     *
     * @param ExpectedText
     * @return
     */
    public static boolean checkPage(String ExpectedText) {
        boolean rc = true;

        String current_page_text = getCurrentWebViewContent();

        rc = current_page_text.contains(ExpectedText);

        return rc;
    }

    /**
     * get current element visual text
     *
     * @param ElementID
     * @return
     */
    public static String getWebElementTextByID(String ElementID) {
        String element_text = "";

        Web.WebInteraction element;
        Web.WebInteraction element_text_obj;

        element = onWebView().withElement(findElement(Locator.ID, ElementID));  // find element

        if (element != null) {
            element_text_obj = element.perform(getText());
            if (element_text_obj != null) {
                element_text = element_text_obj.get().toString();
            }
        }

        return element_text;
    }

    /**
     * sleep Timeout
     *
     * @param Timeout
     */
    public static boolean waitTime(long Timeout) {

        boolean rc = true;

        try {
            Thread.sleep(Timeout);

        } catch (Exception e) {
            rc = false;
            UITestLog.x("Something wrong at waiting for element");
        }

        return rc;
    }

    /**
     * click web button element
     *
     * @param BtnID
     * @return button is there and click, then return true
     */
    public static boolean webClickBtnByID(String BtnID) {
        boolean rc = false;

        rc = webClickBtnByLocator(BtnID, Locator.ID);

        return rc;
    }

    /**
     * click button with LocatorID
     *
     * @param BtnID
     * @param LocatorID
     * @return
     */
    public static boolean webClickBtnByLocator(String BtnID, Locator LocatorID) {
        boolean rc = false;

        Web.WebInteraction activation_confirm_btn;

        try {

            activation_confirm_btn = onWebView().withElement(findElement(Locator.ID, BtnID));

            if (activation_confirm_btn != null) {
                activation_confirm_btn.perform(DriverAtoms.webClick());
                rc = true;
            } else
                rc = false;
        } catch (Exception e) {
            rc = false;
            UITestLog.e(e.getMessage());
        }

        if (rc == false) {
            UITestLog.x("Can't Click button " + BtnID + ".");
        }
        return rc;
    }

    public static boolean webClickLink(String PageID, String ClassID) {
        boolean rc = false;

        Web.WebInteraction link_btn;

        try {
            //link_btn = onWebView().withElement(findElement(Locator.LINK_TEXT, PageID));
            link_btn = onWebView().withElement(findElement(Locator.ID, PageID)).withContextualElement(findElement(Locator.CLASS_NAME, ClassID));

            if (link_btn != null) {
                link_btn.perform(DriverAtoms.webClick());
                rc = true;
            } else {
                rc = false;
            }

        } catch (Exception e) {
            rc = false;
            UITestLog.x(e.getMessage());
        }

        if (rc == false) {
            UITestLog.x("can't click web link " + ClassID + ".");
        }

        return rc;
    }

    /**
     * click web button based on button name
     *
     * @param Btn
     * @return
     */
    public static boolean webClickBtnByName(String Btn) {
        boolean rc = false;

        Web.WebInteraction activation_confirm_btn = onWebView().withElement(findElement(Locator.NAME, Btn));

        try {
            if (activation_confirm_btn != null) {
                activation_confirm_btn.perform(DriverAtoms.webClick());
                rc = true;
            } else
                rc = false;
        } catch (Exception e){
            rc = false;
            UITestLog.x(e.getMessage());
        }

        return rc;
    }

    private static Map<String, String> makeLocatorJSON(Locator locator, String value) {
        Map<String, String> map = Maps.newHashMap();
        map.put(locator.getType(), value);
        return map;
    }

    /**
     * enter string information to web element
     *
     * @param ElementID
     * @param Content
     * @return
     */
    public static boolean webEnterContentByID(String ElementID, String Content) {

        boolean rc = false;

        Web.WebInteraction activation_txtEdit = onWebView().withElement(findElement(Locator.ID, ElementID));
        if (activation_txtEdit != null) {
            activation_txtEdit.perform(DriverAtoms.clearElement());
            activation_txtEdit.perform(DriverAtoms.webKeys(Content));
            rc = true;
        } else
            rc = false;

        return rc;
    }

    /**
     * if current web element text can match expected text, pass check.
     *
     * @param ElementID    : Web Element ID
     * @param ExpectedText : Expected Text
     */
    public static boolean checkWebElementTextByID(String ElementID, String ExpectedText) {

        boolean rc;

        try {
            //onWebView().withElement(findElement(Locator.ID, ElementID)).check(webMatches(getText(), containsString(ExpectedText)));
            String curr_text;
            String err_msg = "";
            curr_text = getWebElementTextByID(ElementID);
            // if (curr_text.contains(ExpectedText)) {
            if (curr_text.compareTo(ExpectedText) == 0) {
                err_msg = "Check Web Element Text:" + "Expected:" + ExpectedText + ".Got:" + curr_text + ".";
                UITestLog.d(err_msg);
                rc = true;
            }
            else {
                rc = false;
                err_msg = "Check Web Element Text:" + "Expected:" + ExpectedText + ".Got:" + curr_text + ".";
                UITestLog.fail(err_msg);
            }

        } catch (Exception e) {
            rc = false;
            UITestLog.x(e.getMessage());
        }

        return rc;
    }

    /**
     * if current toast text can match expected text, pass check.
     * http://stackoverflow.com/questions/28390574/checking-toast-message-in-android-espresso
     * <p/>
     * * @param ToastText : Toast Text
     */
    public static boolean checkToastContentByID(String ToastText) {

        boolean rc;

        try {
            //onWebView().withElement(findElement(Locator.ID, ElementID)).check(webMatches(getText(), containsString(ExpectedText)));
            String err_msg = "";
            //curr_text = getWebElementTextByID(ElementID);
            //ViewInteraction toast =  onView(withText(ElementID)).inRoot(new ToastMatcher());
            //ViewInteraction toast = onView(withText(ElementID)).inRoot(new ToastMatcher()).perform(click());
            ViewInteraction toast = onView(withText(ToastText)).inRoot(new ToastMatcher());

            if (toast != null)
                rc = true;
            else {
                rc = false;
                err_msg = "Check Toast Text:" + "Expected:" + ToastText;
                UITestLog.fail(err_msg);
            }

        } catch (Exception e) {
            rc = false;
            UITestLog.x(e.getMessage());
        }

        return rc;
    }

    public static String getCurrentPageID() {
        return getWebElementTextByID(UIContextPageID);
    }


    public static String logPageVisualText(String ElementID){

        String visualText = "";

        visualText = getWebElementTextByID(ElementID);

        Log.v(TAG,visualText);

        return visualText;
    }
}
