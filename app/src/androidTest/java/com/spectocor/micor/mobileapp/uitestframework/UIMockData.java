package com.spectocor.micor.mobileapp.uitestframework;

import com.spectocor.testutilities.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by qwang on 1/12/2016.
 * provide mock data for UI test
 */
public class UIMockData {

    public static long UIWaitCommonPageTime = 1000;


    public static List<UITestCommand> mockFacilityCodes(int MaxiLength) {
        List<UITestCommand> testCommands = new ArrayList<UITestCommand>();

        ArrayList<String> mock_facility_code_list;

        //prepare mock test data
        mock_facility_code_list = StringUtils.mockFacilityCodeList(MaxiLength);
        //mock_facility_code_list.add("blueqwer");
        //mock_facility_code_list.add("Blueqwer");
        UITestCommand curr_cmd;
        curr_cmd = createWaitingPageCmd("ecg-device-list-at-facility-page", 12000);
        testCommands.add(curr_cmd);

        curr_cmd = createWebSelectItemCmd("STL ECG 1");
        testCommands.add(curr_cmd);

        curr_cmd = createWebClickButtonCmd("manual-enrollment-lookup-btn");
        testCommands.add(curr_cmd);

        for (int i = 0; i < mock_facility_code_list.size(); ++i) {

            curr_cmd = createWebEnterContentCmd("facility-id-input", mock_facility_code_list.get(i));
            testCommands.add(curr_cmd);

            curr_cmd = createWebClickButtonCmd("enter-facility-code-next-btn");
            testCommands.add(curr_cmd);

            curr_cmd = createWebCheckContentCmd("all-popup", "No Such Facility. Please try again!");
            testCommands.add(curr_cmd);

            curr_cmd = createWaitingPageCmd("begin-session-activation-enter-facility-code", 1000);
            testCommands.add(curr_cmd);

        }

        return testCommands;
    }

    // random create select patient feel code
    public static List<UITestCommand> mockSelectFeels(int MaxiLength) {
        List<UITestCommand> testCommands = new ArrayList<UITestCommand>();

        ArrayList<String> mock_feel_id_list;

        //prepare mock test data
        int mock_feel_index_list[] = StringUtils.mockArrayIndexList(MaxiLength, PatientFeels.MaxiFeels);

        UITestCommand curr_cmd;

        PatientFeels feels = new PatientFeels();

        for (int i = 0; i < mock_feel_index_list.length; ++i) {

            curr_cmd = createWebClickButtonCmd(feels.getKey(mock_feel_index_list[i]));
            testCommands.add(curr_cmd);
        }

        curr_cmd = createWebClickButtonCmd("collect-symptoms-btn");
        testCommands.add(curr_cmd);

        return testCommands;
    }

    // full select patient feels
    public static List<UITestCommand> mockFullFeels() {
        List<UITestCommand> testCommands = new ArrayList<UITestCommand>();

        ArrayList<String> mock_feel_id_list;

        //prepare mock test data

        UITestCommand curr_cmd;

        PatientFeels feels = new PatientFeels();

        for (int i = 0; i < PatientFeels.MaxiFeels; ++i) {

            curr_cmd = createWebClickButtonCmd(feels.getKey(i));
            testCommands.add(curr_cmd);
        }

        curr_cmd = createWebClickButtonCmd("collect-symptoms-btn");
        testCommands.add(curr_cmd);

        return testCommands;
    }

    // random create select patient feel code
    public static List<UITestCommand> mockSelectActivitys(int MaxiLength) {
        List<UITestCommand> testCommands = new ArrayList<UITestCommand>();

        ArrayList<String> mock_activity_id_list;

        //prepare mock test data
        int mock_activity_index_list[] = StringUtils.mockArrayIndexList(MaxiLength,PatientActivitys.MaxiActivity );

        UITestCommand curr_cmd;

        PatientActivitys activitys = new PatientActivitys();

        for (int i = 0; i < mock_activity_index_list.length; ++i) {

            curr_cmd = createWebClickButtonCmd(activitys.getKey(mock_activity_index_list[i]));
            testCommands.add(curr_cmd);
        }

        curr_cmd = createWebClickButtonCmd("collect-activities-btn");
        testCommands.add(curr_cmd);

        return testCommands;
    }

    // full select patient feels
    public static List<UITestCommand> mockFullActivitys() {
        List<UITestCommand> testCommands = new ArrayList<UITestCommand>();

        ArrayList<String> mock_activity_id_list;

        //prepare mock test data

        UITestCommand curr_cmd;

        PatientActivitys activitys = new PatientActivitys();

        for (int i = 0; i < PatientActivitys.MaxiActivity; ++i) {

            curr_cmd = createWebClickButtonCmd(activitys.getKey(i));
            testCommands.add(curr_cmd);
        }

        curr_cmd = createWebClickButtonCmd("collect-activities-btn");
        testCommands.add(curr_cmd);

        return testCommands;
    }

    ////////////////////////////////////////////////////////////
    //
    // create WaitingPage cmd
    public static UITestCommand createWaitingPageCmd(String PageID, long Timeout) {

        Map<String, String> paras = new HashMap<String, String>();

        paras.put(UITestCommand.KeyCommand, UITestCommand.CmdWaitingPage);
        paras.put(UITestCommand.KeyPageID, PageID);
        paras.put(UITestCommand.KeyTimeout, Long.toString(Timeout));

        UITestCommand cmd = new UITestCommand(paras);

        return cmd;
    }

    //create check page cmd
    public static UITestCommand createCheckPageCmd(String ExpectedText) {

        Map<String, String> paras = new HashMap<String, String>();

        paras.put(UITestCommand.KeyCommand, UITestCommand.CmdCheckPage);
        paras.put(UITestCommand.KeyExpectedText, ExpectedText);

        UITestCommand cmd = new UITestCommand(paras);

        return cmd;
    }

    //create waiting cmd
    public static UITestCommand createWaitingCmd(long Timeout) {

        Map<String, String> paras = new HashMap<String, String>();

        paras.put(UITestCommand.KeyCommand, UITestCommand.CmdWaiting);
        paras.put(UITestCommand.KeyTimeout, Long.toString(Timeout));

        UITestCommand cmd = new UITestCommand(paras);

        return cmd;
    }

    //create web click button
    public static UITestCommand createWebClickButtonCmd(String ButtonID) {

        Map<String, String> paras = new HashMap<String, String>();

        paras.put(UITestCommand.KeyCommand, UITestCommand.CmdWebClickButton);
        paras.put(UITestCommand.KeyButtonID, ButtonID);

        UITestCommand cmd = new UITestCommand(paras);

        return cmd;
    }

    //create web enter content
    public static UITestCommand createWebEnterContentCmd(String ID, String EnterText) {

        Map<String, String> paras = new HashMap<String, String>();

        paras.put(UITestCommand.KeyCommand, UITestCommand.CmdWebEnterContent);
        paras.put(UITestCommand.KeyID, ID);
        paras.put(UITestCommand.KeyEnterText, EnterText);

        UITestCommand cmd = new UITestCommand(paras);

        return cmd;
    }

    //create web select item
    public static UITestCommand createWebSelectItemCmd(String SelectItemID) {

        Map<String, String> paras = new HashMap<String, String>();

        paras.put(UITestCommand.KeyCommand, UITestCommand.CmdWebSelectItem);
        paras.put(UITestCommand.KeySelectName, SelectItemID);

        UITestCommand cmd = new UITestCommand(paras);

        return cmd;
    }

    //create web check content
    public static UITestCommand createWebCheckContentCmd(String ID, String ExpectedText) {

        Map<String, String> paras = new HashMap<String, String>();

        paras.put(UITestCommand.KeyCommand, UITestCommand.CmdWebCheckContent);
        paras.put(UITestCommand.KeyID, ID);
        paras.put(UITestCommand.KeyExpectedText, ExpectedText);

        UITestCommand cmd = new UITestCommand(paras);

        return cmd;
    }
}
