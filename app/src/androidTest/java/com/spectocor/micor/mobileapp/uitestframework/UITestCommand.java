package com.spectocor.micor.mobileapp.uitestframework;


import junit.framework.Assert;

import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by qwang on 1/12/2016.
 * <p/>
 * UI Test command
 */
public class UITestCommand {
    // JSON Tag
    public final static String KeyCommand = "Command";
    public final static String KeyCommands = "Commands";

    // Cmd name
    public final static String CmdWaitingPage = "WaitingPage";
    public final static String CmdCheckPage = "CheckPage";
    public final static String CmdWaiting = "Waiting";
    public final static String CmdWebClickButton = "WebClickButton";
    public final static String CmdWebEnterContent = "WebEnterContent";
    public final static String CmdWebSelectItem = "WebSelectItem";
    public final static String CmdWebCheckContent = "WebCheckContent";
    public final static String CmdCheckToastContent = "CheckToastContent";
    public final static String CmdWebClickLink = "WebClickLink";

    // parameter information
    public final static String KeyAction = "Action";
    public final static String KeyID = "ID";
    public final static String KeyTimeout = "Timeout";
    public final static String KeyExpectedText = "ExpectedText";
    public final static String KeyEnterText = "EnterText";
    public final static String KeyButtonID = "ButtonID";
    public final static String KeyPageID = "PageID";
    public final static String KeySelectName = "SelectItem";
    public final static String KeyToastText = "ToastText";
    public final static String KeyLinkID = "LinkID";
    public final static String KeyClassID = "ClassID";

    public final static String UIResetBtnID = "dev-reset-btn";

    private String mCmdName;
    private Map mParas;

    public UITestCommand() {
        mCmdName = "";
        mParas = new Hashtable();
    }

    public UITestCommand(JSONObject TestCmd) throws IllegalArgumentException {

        Iterator<String> keys;
        String key;
        String value;

        Assert.assertTrue(TestCmd != null);


        keys = TestCmd.keys();

        // get action name
        if (TestCmd.has(KeyCommand)) {
            mCmdName = TestCmd.optString(KeyCommand);
        } else {
            throw new IllegalArgumentException("Invalid test command: " + TestCmd);
        }

        mParas = new Hashtable();

        while (keys.hasNext()) {
            key = keys.next();
            value = TestCmd.optString(key);
            mParas.put(key, value);
        }
    }

    public UITestCommand(Map Paras) throws IllegalArgumentException {

        Assert.assertTrue(Paras != null);

        Assert.assertTrue(Paras.size() > 0);

        // get command name
        if (Paras.containsKey(KeyCommand)) {
            mCmdName = (String) Paras.get(KeyCommand);
        } else {
            throw new IllegalArgumentException("Invalid test command:" + Paras.values().toString());
        }

        mParas = new Hashtable();

        for (Object key : Paras.keySet()) {
            mParas.put(key, Paras.get(key));
        }
    }

    /**
     * click reset button
     *
     * @return
     */
    public static boolean clickResetBtn() {
        boolean rc = false;
        try {
            rc = UIWebCmdUtility.webClickBtnByID(UIResetBtnID);
        } catch (Exception e) {
            rc = false;
            UITestLog.e("Can not click reset button");
            UITestLog.x(e.getMessage());
        }

        return rc;
    }

    /**
     * return current command name
     *
     * @return
     */
    public String getCmdName() {
        return mCmdName;
    }

    /**
     * get parameter value
     *
     * @param ParaName
     * @return
     * @throws IllegalArgumentException
     */
    public String getParaValue(String ParaName) throws IllegalArgumentException {
        String value;

        if (mParas.containsKey(ParaName)) {
            value = (String) mParas.get(ParaName);
        } else {
            throw new IllegalArgumentException("can't find parameter name:" + ParaName + " from command:" + dumpCmd());
        }

        return value;
    }

    /**
     * dummp current command to string
     *
     * @return
     */
    public String dumpCmd() {
        String cmdStr;


        Assert.assertTrue(mParas != null);

        // Iterate over all para, using the keySet method.
        cmdStr = "{";
        for (Object key : mParas.keySet()) {
            cmdStr += key.toString() + ":" + mParas.get(key).toString() + ",";
        }
        cmdStr += "}";

        return cmdStr;
    }

    /**
     * execute test command
     *
     * @return
     */
    public boolean execute() {
        boolean rc = false;

        // parameter value
        String elementID;
        String buttonID;
        String pageID;
        String selectItemName;
        String enterText;
        String expectedText;
        String toastText;
        String classID;
        long timeout;

        String TestCmd;

        TestCmd = mCmdName;

        switch (TestCmd) {
            case "WaitingPage":
                pageID = getParaValue(KeyPageID);
                timeout = Long.parseLong(getParaValue(KeyTimeout));
                rc = UIWebCmdUtility.CheckWebPageByID(pageID, timeout);
                break;
            case "CheckPage":
                expectedText = getParaValue(KeyExpectedText);
                rc = UIWebCmdUtility.checkPage(expectedText);
                break;
            case "Waiting":
                timeout = Long.parseLong(getParaValue(KeyTimeout));
                rc = UIWebCmdUtility.waitTime(timeout);
                break;
            case "WebClickButton":
                buttonID = getParaValue(KeyButtonID);
                rc = UIWebCmdUtility.webClickBtnByID(buttonID);
                break;
            case "WebEnterContent":
                elementID = getParaValue(KeyID);
                enterText = getParaValue(KeyEnterText);
                rc = UIWebCmdUtility.webEnterContentByID(elementID, enterText);
                break;
            case "WebSelectItem":
                selectItemName = getParaValue(KeySelectName);
                rc = UIWebCmdUtility.webClickBtnByName(selectItemName);   // click by name
                break;
            case "WebCheckContent":
                elementID = getParaValue(KeyID);
                expectedText = getParaValue(KeyExpectedText);
                rc = UIWebCmdUtility.checkWebElementTextByID(elementID, expectedText);
                break;
            case "CheckToastContent":
                toastText = getParaValue(KeyToastText);
                rc = UIWebCmdUtility.checkToastContentByID(toastText);
                break;
            case "WebClickLink":
                pageID = getParaValue(KeyPageID);
                classID = getParaValue(KeyClassID);
                rc = UIWebCmdUtility.webClickLink(pageID, classID);
                break;
            default:
                rc = false;
                throw new IllegalArgumentException("Invalid test command: " + TestCmd);
        }

        return rc;
    }
}

