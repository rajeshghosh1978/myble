package com.spectocor.micor.mobileapp.ecgstorage;

import android.content.Context;
import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.http.HttpQueueManager;
import com.spectocor.testutilities.ECGDataCreator;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.database.ecg.EcgChunkDbUtility;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 3/3/2016.
 *
 * QA  :
 * DEV :
 *
 *
 * CHANGES HISTORY
 *************************************************************
 * Date             Author          Comments
 * 03/03/2016       Qian            create test case for ECG Data Chunk DB Capacity. 30 days, 250 two channel sample data per second, 5 minutes per data chunk
 *
 *************************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 03/03/2016       Qian            Pass :
 *
 */

@LargeTest
@RunWith(AndroidJUnit4.class)
public class EcgDataChunkDBCapacityTest  {

    private static String TAG = "EcgDataChunkDBCapacityTest";

    private static AMainActivity mActivity;
    private static Context mContext;
    private static CurrentSessionState mCurrentSessionState;
    private static EcgChunkDbUtility mEcgChunkDbUtility;

    private static int mSampleRate = TestEnvGlobalConst.SAMPLE_RATE;
    private static long mDefaultSleepMillionSecond = TestEnvGlobalConst.APP_SLEEP_MILLION_SECOND;
    private static int mDefaultECGDataChunkDurationSecond = TestEnvGlobalConst.ECG_DATA_CHUNK_DURATION_SECOND;
    private static int mDefaultECGDataChunkSampleNumber = TestEnvGlobalConst.PER_DAY_CHUNK_NUMBER;

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init();  // launch the activity and init static variables
        }
    };

    //////////////////////////////////////////////
    // test data
    //////////////////////////////////////////////
    private int mDefaultHeartBeatVal = TestEnvGlobalConst.NORMAL_HEART_BEAT_RATE;
    private int mDefaultDisplayTime = TestEnvGlobalConst.DEFAULT_RUN_TEST_TIME;

    private static int NEW_HEART_BEAT = 1;

    @Before
    public void before(){
        // waiting activity launch
        SystemClock.sleep(mDefaultSleepMillionSecond);

        Log.d(TAG, mCurrentSessionState.dbPathAndName);

        Assert.assertTrue(TAG + ": Fail to start test because there is active session on mobile device", mCurrentSessionState.sessionActive);
    }

    @After
    public void after(){
        // waiting tester to check UI
        SystemClock.sleep(mDefaultSleepMillionSecond);
    }


    ////////////////////////////////////////////////
    // test case
    ////////////////////////////////////////////////

    /**
     * per day ECG data chunk storage size
     *
     * input network trigger event
     *
     * return successful response
     *
     * Test Case ID : UTMAID250001
     */
    @Test
    public void testPerDayECGDataChunkCapacity(){

        Log.d(TAG,"" + mCurrentSessionState.sessionId);
        Log.d(TAG,mCurrentSessionState.dbPathAndName);

        // prepare test data
        long startTimeSecond = System.currentTimeMillis()/1000;
        long elapsedSecond = mDefaultECGDataChunkDurationSecond; // 300
        int sessionID = mCurrentSessionState.sessionId; //1;
        long ListLen = 10; //mDefaultECGDataChunkSampleNumber; // 288
        List<EcgDataChunk> ecgDataChunksList = ECGDataCreator.createRandomEcgDataChunkList(startTimeSecond, elapsedSecond, sessionID, ListLen);

        // insert to database
        int realInsertNumber = mEcgChunkDbUtility.insertEcgDataChunks(ecgDataChunksList);

        // check database
        Assert.assertTrue(TAG + ":Fail to pass insert ECG data chunk:expected insert number:"+ListLen+":Real insert number:"+realInsertNumber,ListLen == realInsertNumber);

    }

    ////////////////////////////////////////////////
    // support function
    ////////////////////////////////////////////////

    /**
     * init class member variables
     */
    private void init(){

        mActivity = mActivityRule.getActivity();
        mContext  = mActivity.getApplicationContext();
        mCurrentSessionState = SessionStateAccess.getCurrentSessionState(mContext);
        mEcgChunkDbUtility = EcgChunkDbUtility.getInstance(mContext);

        assert(mActivity != null);
        assert(mContext != null);
        assert(mCurrentSessionState!= null);

        Log.d(TAG, "init");
    }

}
