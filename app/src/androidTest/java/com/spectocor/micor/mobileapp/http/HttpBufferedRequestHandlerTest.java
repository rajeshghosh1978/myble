package com.spectocor.micor.mobileapp.http;

import android.content.Context;
import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;

import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.http.requests.EcgChunkHttpRequest;
import com.spectocor.testutilities.ECGDataCreator;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.database.ecg.EcgChunkDbUtility;
import com.spectocor.testutilities.utils.ThreadUtility;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 2/23/2016.
 *
 * test case for ECG buffered Chunk Transmission to USD API
 *
 * trigger event : turn off and turn on network connection
 *
 * reference class
 * @link http.HttpBufferedRequestHandler
 *
 * QA  : ALGO-965
 * DEV : ALGO-848
 *
 * CHANGES HISTORY
 *************************************************************
 * Date             Author          Comments
 * 02/23/2016       Qian            create test case for ECG buffered Chunk Transmission to USD API
 *
 *************************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 02/24/2016       Qian            fail :
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class HttpBufferedRequestHandlerTest {

    private static String TAG = "testHttpBufferedRequestHandler";

    //private UITestSimpleFramework mTestFramework;
    private static AMainActivity mActivity;
    private static Context mContext;
    private static CurrentSessionState mCurrentSessionState;
    private static EcgChunkDbHelper mEcgChunkDbHelper;
    private static EcgChunkDbUtility mEcgChunkDbUtility;


    private static int mSampleRate = ECGDataCreator.SAMPLE_RATE;
    private static long mDefaultSleepMillionSecond = TestEnvGlobalConst.APP_SLEEP_MILLION_SECOND;

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init();  // launch the activity and init static variables
        }
    };

    //////////////////////////////////////////////
    // test data
    //////////////////////////////////////////////
    private int mDefaultHeartBeatVal = TestEnvGlobalConst.NORMAL_HEART_BEAT_RATE;
    private int mDefaultDisplayTime = TestEnvGlobalConst.DEFAULT_RUN_TEST_TIME;

    private static int NEW_HEART_BEAT = 1;

    @Before
    public void before(){
        // waiting activity launch
        SystemClock.sleep(mDefaultSleepMillionSecond);

        Log.d(TAG, mCurrentSessionState.toString());

        Assert.assertTrue(TAG + ": Fail to start test because there is active session on mobile device",mCurrentSessionState.sessionActive);
    }

    @After
    public void after(){
        // waiting tester to check UI
        SystemClock.sleep(mDefaultSleepMillionSecond);
    }

    /**
     * happy path for sending buffer ecg data chunk
     *
     * input network trigger event
     *
     * return successful response
     *
     * Test Case ID : UTMAID190001
     */
    @Test
    public void testHttpBufferedRequestHandlerECGData(){


        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(mContext);

        HttpQueueManager queueManager = HttpQueueManager.getInstance();
        Log.d(TAG,sessionState.dbPathAndName);

        // session start

        // prepare test data
        long startTimeSecond = System.currentTimeMillis()/1000 + 24 * 60 * 60;  // tomorrow
        long elapsedSecond = 300;
        int sessionID = sessionState.sessionId; //1;
        long ListLen = 3;

        // delete unsend chunk
        mEcgChunkDbUtility.deleteUnSendEcgDataChunk();
        mEcgChunkDbUtility.dumpEcgDataChunk();
        // airplane mode turn on
        Log.d(TAG,"airplane mode turn on");
        ThreadUtility.applicationSleep(30000);
        List<Long> chunkStartTimestampList = sendECGDataChunkList(ListLen, startTimeSecond,elapsedSecond,sessionID);
        int pass_number;
        mEcgChunkDbUtility.dumpEcgDataChunk();
        pass_number = checkBufferEcgDataChunksTransferTime(chunkStartTimestampList,sessionID,-1);
        // buffer test data
        Assert.assertTrue(TAG + ":Fail to pass buffer ECG Data Chunk:expected buffer chunk number:" + chunkStartTimestampList.size() + ":real buffer chunk number:" + pass_number, pass_number == chunkStartTimestampList.size());
        // check FIFO
        // airplane mode turn off
        // waiting for long time to send chunk
        Log.d(TAG, "airplane mode turn off");
        ThreadUtility.applicationSleep(30000);
        ThreadUtility.applicationSleep(ListLen * 30000);
        mEcgChunkDbUtility.dumpEcgDataChunk();
        pass_number = checkBufferEcgDataChunksFIFO(chunkStartTimestampList,sessionID);
        Assert.assertTrue(TAG + ":Fail to pass buffer ECG Data Chunk FIFO :expected FIFO chunk number:" + chunkStartTimestampList.size() + ":real FIFO chunk number:" + pass_number, pass_number == chunkStartTimestampList.size());
        // delete data
        deleteBufferEcgDataChunks(chunkStartTimestampList,sessionID);
        ThreadUtility.applicationSleep(10000);
    }


    @Test
    public void testRetrieveECGDataChunk(){

        EcgChunkDbUtility ecgChunkDbUtility = EcgChunkDbUtility.getInstance(mContext);

        long curr_second_timestamp = SystemClock.currentThreadTimeMillis()/1000;

        long start_second_timestamp = 0; //curr_second_timestamp - 600;
        long end_second_timestamp =  curr_second_timestamp + 700;

        List<EcgDataChunk> ecgDataChunks = mEcgChunkDbUtility.retrieveEcgDataChunks(mCurrentSessionState.sessionId,start_second_timestamp,end_second_timestamp);

        EcgDataChunk ecgDataChunk;

        if(ecgDataChunks != null){

            for(int i = 0; i < ecgDataChunks.size() ; ++i){

                ecgDataChunk = ecgDataChunks.get(i);

                Log.d(TAG,"" + i);
                //Log.d(TAG,ecgDataChunk.getChunkDataChannelCompressed());
                Log.d(TAG,Long.toString(ecgDataChunk.getChunkStartDateUnixMs()));
                Log.d(TAG,Long.toString(ecgDataChunk.insertTimestamp));
                Log.d(TAG,Integer.toString(ecgDataChunk.monitoringSessionId)) ;
//                ecgDataChunk.timezoneId ;
//                ecgDataChunk.timezoneOffsetMillis;
//                ecgDataChunk.numberOfSamples ;

            }
        }
    }

    ////////////////////////////////////////////////
    // support function
    ////////////////////////////////////////////////

    /**
     * init class member variables
     */
    private void init(){

        mActivity = mActivityRule.getActivity();
        mContext  = mActivity.getApplicationContext();
        mCurrentSessionState = SessionStateAccess.getCurrentSessionState(mContext);
        mEcgChunkDbHelper = EcgChunkDbHelper.getInstance(mContext);
        mEcgChunkDbUtility = EcgChunkDbUtility.getInstance(mContext);

        assert(mActivity != null);
        assert(mContext != null);
        assert(mEcgChunkDbHelper != null);

        Log.d(TAG,"init");
    }

    private void sendingECGDataChunkWithBuffer(final EcgDataChunk compressedChunk){

        // Transferring
        final EcgChunkHttpRequest ecgChunkHttpRequest = new EcgChunkHttpRequest();

        ecgChunkHttpRequest.setEcgChunkData(compressedChunk);

        // TODO remove unnecessary creation of threads since creating a thread is more expensive here that the statement itself.
        new Thread(new Runnable() {
            @Override
            public void run() {

                Log.d(TAG,"Transferring Data");

                HttpQueueManager.getInstance().enqueueRequest(ecgChunkHttpRequest, true);
            }
        }).start();

    } // end: transfer-chunk


    /**
     * send numberChunk which start time second is startTimeSecond , each chunk size is elapsedSecond.
     *
     * @param NumberChunk
     * @param startTimeSecond
     * @param elapsedSecond
     * @param sessionID
     * @return
     */
    private List<Long> sendECGDataChunkList(final long NumberChunk,final long startTimeSecond,final long elapsedSecond,int sessionID){

        List<Long> startTimeStampList = null;

        EcgDataChunk ecgDataChunk;

        long currStartTimeSecond = startTimeSecond;

        if(NumberChunk > 0 ){

            startTimeStampList = new ArrayList<>();

            for(int i = 0; i < NumberChunk; ++i){

                ecgDataChunk = ECGDataCreator.createRandomEcgDataChunk(currStartTimeSecond,elapsedSecond,sessionID);

                sendingECGDataChunkWithBuffer(ecgDataChunk);

                startTimeStampList.add(new Long(currStartTimeSecond));

                ThreadUtility.applicationSleep(2000);

                currStartTimeSecond += elapsedSecond;
            }
        }

        return startTimeStampList;
    }

    /**
     * check buffer ECG data chunks or not
     * @param startTimeStampList
     * @param sessionID
     * @param expectedValue
     * @return
     */
    private int checkBufferEcgDataChunksTransferTime(final List<Long> startTimeStampList,final int sessionID,final long expectedValue){

        int pass_number=0;

        EcgDataChunk ecgDataChunk;

        try{
            for(int i = 0; i < startTimeStampList.size();++i){

                ecgDataChunk = mEcgChunkDbHelper.retrieveCompressedEcg(sessionID,startTimeStampList.get(i).longValue());

                if(ecgDataChunk.getChunkTransferTimestamp() == expectedValue){
                    pass_number += 1;
                }

            }

        }catch (SQLDataException se){
            Log.e(TAG,""+se.getMessage());
            pass_number = 0;
        }

        return pass_number;
    }

    /**
     * check buffer ECG data chunk FIFO
     * @param startTimeStampList
     * @param sessionID
     * @return
     */
    private int checkBufferEcgDataChunksFIFO(final List<Long> startTimeStampList,final int sessionID){

        int pass_number=0;

        EcgDataChunk ecgDataChunk;

        long previousChunkStartTimeStamp,previousChunkTransferTimeStamp;
        long currChunkStartTimeStamp,currChunkTransferTimeStamp;

        previousChunkStartTimeStamp = 0;
        previousChunkTransferTimeStamp = 1;

        try{
            for(int i = 0; i < startTimeStampList.size();++i){

                ecgDataChunk = mEcgChunkDbHelper.retrieveCompressedEcg(sessionID, startTimeStampList.get(i).longValue());

                Assert.assertTrue(TAG+":Fail to find chunk:"+ startTimeStampList.get(i).longValue(), ecgDataChunk != null);

                currChunkStartTimeStamp = ecgDataChunk.getChunkStartDateUnixMs();

                currChunkTransferTimeStamp = ecgDataChunk.getChunkTransferTimestamp();

//                if(currChunkTransferTimeStamp != -1
//                        && currChunkStartTimeStamp > previousChunkStartTimeStamp
//                        && currChunkTransferTimeStamp  > currChunkStartTimeStamp
//                        && currChunkTransferTimeStamp > previousChunkTransferTimeStamp
//                        ){
                Log.d(TAG,"current chunk start timestamp:"+currChunkStartTimeStamp);
                Log.d(TAG,"current chunk transfer timestamp:"+currChunkTransferTimeStamp);

                Log.d(TAG,"previous chunk start timestamp:"+previousChunkStartTimeStamp);
                Log.d(TAG,"previous chunk transfer timestamp:"+previousChunkTransferTimeStamp);

                if(currChunkTransferTimeStamp != -1
                        && currChunkStartTimeStamp > previousChunkStartTimeStamp
                        && currChunkTransferTimeStamp > previousChunkTransferTimeStamp
                        ){
                    pass_number += 1;
                }

                previousChunkStartTimeStamp = currChunkStartTimeStamp;
                previousChunkTransferTimeStamp = currChunkTransferTimeStamp;

            }

        }catch (SQLDataException se){
            Log.e(TAG,""+se.getMessage());
            pass_number = 0;
        }

        return pass_number;
    }


    private int deleteBufferEcgDataChunks(final List<Long> startTimeStampList,final int sessionID){

        int pass_number=0;

        EcgDataChunk ecgDataChunk;

        boolean rc;

            for(int i = 0; i < startTimeStampList.size();++i) {

                rc = mEcgChunkDbUtility.deleteEcgDataChunk(sessionID, startTimeStampList.get(i).longValue());

                if (rc) {
                    pass_number += 1;
                }

            }

        return pass_number;
    }
}
