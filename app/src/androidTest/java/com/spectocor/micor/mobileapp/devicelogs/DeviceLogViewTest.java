package com.spectocor.micor.mobileapp.devicelogs;

import android.content.Context;
import android.os.Environment;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by qwang on 1/7/2016.
 * view and export device log
 */
public class DeviceLogViewTest extends AndroidTestCase {

    static final int mMaxListLen = 10000;  // length of list
    private static String mDump_fileName = "\\\\QAEnv\\test_data\\unit_test\\log\\device\\device_log"; // under QA share folder;
    private static String mLogFieldTitle = "Log Session ID,DB Column ID,Log Time,Log Type, Log Number, Log String\n";
    DeviceLogRepository repository;
    List<DeviceLog> mTestLogs; // list of device log

    /**
     * setting up test environment
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        super.setUp();

        Context context = getContext();
        //context.deleteDatabase(LogEventsDatabaseHelper.DatabaseName);

        LogEventsDatabaseHelper databaseHelper = new LogEventsDatabaseHelper(context);
        repository = new DeviceLogRepository(databaseHelper);

        mTestLogs = new ArrayList<>();
    }

    /*
   * test to dump a file
    */
    public void testDumpLogFile() throws Exception {

        List<DeviceLog> realLogs = repository.getAll(mMaxListLen);
        dumpLog("device_log", mTestLogs);
    }

    // dump all log item to log file
    public void dumpLog(String LogFileName, List<DeviceLog> Logs) {

        BufferedWriter writer = null;

        String curr_line; // current log line

        DeviceLog log_item;
        try {
            //create a temporary file
            String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            String log_file_name = LogFileName + "_" + timeLog + ".log.csv";
            File logFile = new File(getContext().getFilesDir(), log_file_name);

            // This will output the full path where the file will be written to...
            System.out.println(logFile.getCanonicalPath());

            writer = new BufferedWriter(new FileWriter(logFile));

            // write title
            writer.write(mLogFieldTitle);

            // dump all log
            for (int i = 0; i < Logs.size(); ++i) {
                log_item = Logs.get(i);
                curr_line = log_item.getMonitoringSessionId() + "," + log_item.getDatabaseRowId() + "," + log_item.getLogDateUnixSec() + "," + log_item.getDeviceLogInfoTypeId() + "," + log_item.getValueNumber() + "," + log_item.getValueString();
                writer.write(curr_line);
                writer.newLine();
            }
            // close
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                if (writer != null)
                    writer.close();
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            }

        }

    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }
}
