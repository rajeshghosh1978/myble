package com.spectocor.micor.mobileapp.patienttriggerevent;

import android.content.Context;
import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.uitestframework.UIMockData;
import com.spectocor.micor.mobileapp.uitestframework.UITestCommand;
import com.spectocor.micor.mobileapp.uitestframework.UITestSimpleFramework;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 1/14/2016.
 * test patient trigger event UI test case
 *
 *
 *
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class PatientTriggerEventActivityTest {

    // define test data variable

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class, false, false) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
        }


    };
    //////////////////////////////////////////////////////////////
    // Simple Test Framework
    //////////////////////////////////////////////////////////////
    private UITestSimpleFramework mTestFramework;

    /**
     * @return start {@link Intent} for the simple web form URL.
     */
    private static Intent withWebFormIntent() {
        //Intent basicFormIntent = new Intent();
        //basicFormIntent.putExtra("KEY_URL_TO_LOAD", "file:///android_asset/micor_html/index.html");
        //return basicFormIntent;
        return null;
    }

    /**
     * launch activity and get current test framework
     *
     * @return
     */
    public UITestSimpleFramework getTestFramework() {

        //UITestSimpleFramework framework;

        mActivityRule.launchActivity(withWebFormIntent()); // launch activity

        AMainActivity activity = mActivityRule.getActivity();

        Context context = activity.getApplicationContext();

        if (mTestFramework == null) {
            mTestFramework = new UITestSimpleFramework();
        }
        mTestFramework.setContext(context);

        return mTestFramework;
    }

    /////////////////////////////////////////////////
    // test case
    /////////////////////////////////////////////////

    /**
     * test report symptom button
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID120001
     */
    @Test
    public void ClickReportSymptomButton() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String test_case_data_file = "td_report_symptom_btn.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * click report button without symptom
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * click report without symptom
     * <p/>
     * Test Case ID : UTMAID120002
     */
    @Test
    public void ClickReportWithoutSymptomButton() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String test_case_data_file = "td_report_without_symptom_btn.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * click report cancel button
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * click report cancel button
     * <p/>
     * Test Case ID : UTMAID120003
     */
    @Test
    public void ClickReportCancelButton() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String test_case_data_file = "td_report_symptom_cancel_btn.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * click report back button
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * click report back button
     * <p/>
     * Test Case ID : UTMAID120004
     */
    @Test
    public void ClickReportBackButton() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String test_case_data_file = "td_report_symptom_back_btn.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * did not select feel and activity and click report
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * click report button
     * <p/>
     * Test Case ID : UTMAID120005
     */
    @Test
    public void ReportEmptyFeelEmptyActivity() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String test_case_data_file = "td_report_empty_feel_empty_activity.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * select one feel and one activity and click report
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * click report report button
     * <p/>
     * Test Case ID : UTMAID120006
     */
    @Test
    public void ReportOneFeelOneActivity() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String test_case_data_file = "td_report_one_feel_one_activity.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * select all feel and no activity and click report
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * click report button
     * <p/>
     * Test Case ID : UTMAID120007
     */
    @Test
    public void ReportFullFeels() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String before_test_case_data_file = "td_session_home_to_select_patient_feel_pg.json";

        List<UITestCommand> test_case_data = UIMockData.mockFullFeels();

        String after_test_case_data_file = "td_select_activity_to_session_home.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(before_test_case_data_file);

        mTestFramework.runTest(test_case_data);

        mTestFramework.runTestFile(after_test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * select random feel and random activity and click report
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * click report button
     * <p/>
     * Test Case ID : UTMAID120008
     */
    @Test
    public void ReportRandomFeelsRandomActivitys() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String before_test_case_data_file = "td_session_home_to_select_patient_feel_pg.json";

        List<UITestCommand> test_case_data_feels = UIMockData.mockSelectFeels(30);

        List<UITestCommand> test_case_data_activitys = UIMockData.mockSelectActivitys(20);

        String after_test_case_data_file = "td_symptom_confirmation_to_session_home.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(before_test_case_data_file);

        mTestFramework.runTest(test_case_data_feels);

        mTestFramework.runTest(test_case_data_activitys);

        mTestFramework.runTestFile(after_test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * select all feel and all activity and click report
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * click report button
     * <p/>
     * Test Case ID : UTMAID120009
     */
    @Test
    public void ReportAllFeelAllActivity() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String before_test_case_data_file = "td_session_home_to_select_patient_feel_pg.json";

        List<UITestCommand> test_case_data_feel = UIMockData.mockFullFeels();

        List<UITestCommand> test_case_data_activity = UIMockData.mockFullActivitys();

        String after_test_case_data_file = "td_symptom_confirmation_to_session_home.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(before_test_case_data_file);

        mTestFramework.runTest(test_case_data_feel);

        mTestFramework.runTest(test_case_data_activity);

        UITestCommand.clickResetBtn();
    }

    /**
     * select blank feel and click report
     * <p/>
     * start page : select bluetooth device
     * end page: report home page
     * <p/>
     * precondition:
     * load select bluetooth device page
     * click report button
     * <p/>
     * Test Case ID : UTMAID120010
     */
    @Test
    public void CheckCleanFeelsActivitys() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String first_test_case_data_file = "td_report_symptom_btn.json";

        String second_test_case_data_file = "td_report_empty_feel_empty_activity.json";


        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(first_test_case_data_file);

        mTestFramework.runTestFile(second_test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * select a feel and one activity and click cancel report
     * <p/>
     * start page : select bluetooth device
     * end page: report home page
     * <p/>
     * precondition:
     * load select bluetooth device page
     * click cancel report button
     * <p/>
     * Test Case ID : UTMAID120011
     */
    @Test
    public void ClickReportConfirmCancelButton() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String test_case_data_file = "td_report_confirm_cancel_btn.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * select a feel and one activity and click modify report
     * <p/>
     * start page : select bluetooth device
     * end page: report home page
     * <p/>
     * precondition:
     * load select bluetooth device page
     * click modify report button
     * <p/>
     * Test Case ID : UTMAID120012
     */
    @Test
    public void ClickReportConfirmModifyButton() {

        String pre_test_case_data_file = "td_normal_activation_session.json";

        String test_case_data_file = "td_report_confirm_modify_btn.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * select Slurred Speech and check toast message and click report button
     * <p/>
     * start page : select bluetooth device
     * end page: report home page
     * <p/>
     * precondition:
     * load select bluetooth device page
     * select Slurred Speech
     * click report button
     * <p/>
     * Test Case ID : UTMAID120013
     */
    @Test
    public void ClickSlurredSpeechButton() {
        String pre_test_case_data_file = "td_normal_activation_session.json";

        String test_case_data_file = "td_report_slurred_speech_btn.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * select Recently Fainted and check toast message and click report button
     * <p/>
     * start page : select bluetooth device
     * end page: report home page
     * <p/>
     * precondition:
     * load select bluetooth device page
     * select Recently Fainted
     * click report button
     * <p/>
     * Test Case ID : UTMAID120014
     */
    @Test
    public void ClickRecentlyFaintedButton() {
        String pre_test_case_data_file = "td_normal_activation_session.json";

        String test_case_data_file = "td_report_recently_fainted_btn.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * select multiple button and click report button
     * <p/>
     * start page : select bluetooth device
     * end page: report home page
     * <p/>
     * precondition:
     * load select multiple bluetooth device page
     * <p/>
     * click report button
     * <p/>
     * Test Case ID : UTMAID120015
     */
    //@Test
    public void TestMultipleItem() {
        String pre_test_case_data_file = "td_normal_activation_session.json";

        String test_case_data_file = "td_report_recently_fainted_btn.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(pre_test_case_data_file);

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }
}
