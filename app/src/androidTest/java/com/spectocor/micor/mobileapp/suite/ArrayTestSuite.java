package com.spectocor.micor.mobileapp.suite;

import com.spectocor.micor.mobileapp.time.TimeZoneTest;
import com.spectocor.micor.mobileapp.utils.arrays.ArrayConcatTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by qwang on 4/13/2016.
 *
 * demo android test suite
 */

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                ArrayConcatTest.class,
                TimeZoneTest.class
        }
)

public class ArrayTestSuite {
}
