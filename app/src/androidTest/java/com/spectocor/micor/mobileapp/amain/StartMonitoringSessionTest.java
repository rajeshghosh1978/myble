package com.spectocor.micor.mobileapp.amain;

import android.content.Context;
import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;


import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.ecgsimulator.EcgSimulatorThread;
import com.spectocor.testutilities.ECGDataCreator;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.utils.ThreadUtility;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 2/19/2016.
 *
 * create test case for start monitoring session.
 *
 * JIRA ID: ALGO-496
 *
 * 1, bluetooth/Simulation starts
 * 2, state files are updating as expected
 *
 *
 * *********************************************************
 * CHANGES HISTORY
 *
 * Date             Author          Comments
 * 02/19/2016       Qian            init create test case
 *
 * *********************************************************
 * RUNNING HISTORY
 *
 * Date             Author          Comments
 * 02/19/2016       Qian
 *
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class StartMonitoringSessionTest {

    //////////////////////////////////////////////////////////////
    // private member variable
    //////////////////////////////////////////////////////////////

    private static String TAG = "StartMonitoringSessionTest";

    private static Context mContext;
    private static AMainActivity mActivity;
    private static AMainJSInterface mMainJSInterface;
    private static CurrentSessionState mCurrentSessionState;

    private static int mSampleRate = ECGDataCreator.SAMPLE_RATE;
    private static long mDefaultSleepMillionSecond = TestEnvGlobalConst.APP_SLEEP_MILLION_SECOND;

    private static String mSessionStatus1234JSON = "{\"ecgBtConfirmed\":true,\"ecgBtMac\":\"B0:B4:48:C4:A9:89\",\"ecgBtName\":\"STL ECG 201\",\"ecgChosen\":false,\"ecgChosenAtShipping\":true,\"enrollmentConfirmed\":true,\"enrollmentDate\":\"2016-01-19\",\"enrollmentId\":\"\",\"facilityId\":\"\",\"facilityName\":\"Facility A\",\"heartBeatDetectionTestComplete\":true,\"heartRateDetected\":true,\"isSalesAgent\":false,\"patientConfirmed\":true,\"patientName\":\"John D\",\"sessionActivationDate\":\"2016-02-19T20:07:14.131Z\",\"sessionActive\":true,\"sessionId\":1,\"sessionReactivation\":true,\"sessionTerminated\":false,\"signalDetected\":true,\"summaryConfirmed\":false}";

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init();  // launch the activity and init static variables
        }
    };

    //////////////////////////////////////////////
    // test data
    //////////////////////////////////////////////
    private int mDefaultHeartBeatVal = TestEnvGlobalConst.NORMAL_HEART_BEAT_RATE;
    private int mDefaultDisplayTime = TestEnvGlobalConst.DEFAULT_RUN_TEST_TIME;


    @Before
    public void before(){
        // waiting activity launch
        SystemClock.sleep(mDefaultSleepMillionSecond);

        // set session state with JSON string

        // jump to start session home page
        // mActivity.callWebviewJsFunction("displayAdminEcgSignalPage()");
    }

    @After
    public void after(){
        // waiting tester to check UI
        SystemClock.sleep(mDefaultSleepMillionSecond);
    }


    /**
     *
     * test normal starting monitoring session
     *
     * test data:
     *
     * Test Case ID : UTMAID180001
     */
    @Test
    public void testNormalStartMonitoringSession(){

        // prepare test data
        boolean bluetoothServerStatus = true;
        boolean sessionStatus = true;

        SessionStateAccess.saveSessionStateJson(mContext, mSessionStatus1234JSON);

        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond); // waiting for start service

        checkStartMonitoringSessionStatus(bluetoothServerStatus, sessionStatus);

    }

    /**
     *
     * test nonactive session starting monitoring session
     *
     * test data:
     *
     * Test Case ID : UTMAID180002
     */
    @Test
    public void testNonactiveStartMonitoringSession(){

        // prepare test data
        boolean bluetoothServerStatus = false;
        boolean sessionStatus = false;

        String statusString = ObjectUtils.toJson(SessionStateAccess.getCurrentSessionState(mContext));

        Log.d(TAG,statusString);

        //SessionStateAccess.saveSessionStateJson(mContext,mSessionStatus1234JSON);
        // jump to ecg signal view
        mActivity.callWebviewJsFunction("displayAdminEcgSignalPage()");


        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond); // waiting for start service

        checkStartMonitoringSessionStatus(bluetoothServerStatus, sessionStatus);

    }


    ////////////////////////////////////////////////////////////////////////
    // support function
    ////////////////////////////////////////////////////////////////////////

    /**
     * init class member variables
     */
    private void init(){

        mActivity = mActivityRule.getActivity();
        mContext  = mActivity.getApplicationContext();
        mMainJSInterface = new AMainJSInterface(mContext);
        mCurrentSessionState = SessionStateAccess.getCurrentSessionState(mContext); // get current session state


        assert(mActivity != null);
        assert(mContext != null);
        assert(mMainJSInterface != null);
        assert(mCurrentSessionState != null);


        Log.d(TAG, "init");
    }

    /**
     * check current session state
     *
     * @param expectedBluetoothServiceStatus, expected bluetooth service status
     * @param expectedSessionStatus, expected session status
     *
     */
    private boolean checkStartMonitoringSessionStatus(boolean expectedBluetoothServiceStatus,boolean expectedSessionStatus){

        boolean rc;

        rc = true;

        checkBluetoothSever(expectedBluetoothServiceStatus);

        checkSessionStatus(expectedSessionStatus);

        return rc;
    }

    private boolean checkBluetoothSever(boolean expectedBluetoothServerStatus){
        boolean rc ;

        boolean realBluetoothServerStatus = EcgSimulatorThread.getInstance(mContext).isRunning;

        Assert.assertTrue(TAG + ": fail to check bluetooth server status,expected value:" + expectedBluetoothServerStatus + ":real value:" + realBluetoothServerStatus, realBluetoothServerStatus == expectedBluetoothServerStatus);

        rc = true;

        return rc;
    }

    private boolean checkSessionStatus(boolean expectedSessionStatus){
        boolean rc ;

        boolean realSessionStatus = SessionStateAccess.getCurrentSessionState(mContext).sessionActive;

        Assert.assertTrue(TAG + ": fail to check session status,expected value:"+ expectedSessionStatus + ":real value:"+realSessionStatus,realSessionStatus == expectedSessionStatus);

        rc = true;

        return rc;
    }

    /**
     * get successful activation session json status
     */
    //@JavascriptInterface
    private void atHomeScreenJustActivatedSession()
    {

        //log("::atHomeScreenJustActivatedSession()");

        //log("====== SESSION-START-STATE ==============\n" + ObjectUtils.toJson(SessionStateAccess.getCurrentSessionState(mContext)) + "\n=========================");

        //LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.SESSION_JUST_ACTIVATED));

    }
}
