package com.spectocor.micor.mobileapp.common;

import android.test.AndroidTestCase;

import org.junit.Assert;

/**
 * Test cases for ServiceFactory class
 */
public class ServiceFactoryTest extends AndroidTestCase {


    /**
     * setting up test environment
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        ServiceFactory.createInstance(this.getContext());
    }


    /**
     * testing get instance function
     * it should return an instance of service factory class that is not null
     *
     * @throws Exception if test ended with exception
     */
    public void testGetInstance() throws Exception {
        ServiceFactory factory = ServiceFactory.getInstance();
        Assert.assertNotNull(factory);
    }


    /**
     * testing get device log service function
     * @throws Exception if test ended with exception
     */
    public void testGetDeviceLogService() throws Exception {
        ServiceFactory factory = ServiceFactory.getInstance();
        Assert.assertNotNull(factory);
        Assert.assertNotNull(ServiceFactory.getInstance().getApplicationLogService());
        Assert.assertNotNull(ServiceFactory.getInstance().getBatteryLogService());
        Assert.assertNotNull(ServiceFactory.getInstance().getDiskSpaceLogService());
        Assert.assertNotNull(ServiceFactory.getInstance().getEcgTransmitterLogService());
    }


}