package com.spectocor.micor.mobileapp.utils.arrays;

import android.util.Log;

import junit.framework.TestCase;
import org.junit.Assert;

/**
 * Created by qwang on 2/5/2016.
 */
public class ArrayConcatTest extends TestCase{

    private static final String TAG =  "ArrayConcatTest";

    public void testArrayConcatShort(){

        short[] channel1 = {1,2,3,4};

        short[] channel2 = {5,6,7,8};

        short[] expected = {1,2,3,4,5,6,7,8};

        short[] actual;

        actual = ArrayConcat.concat(channel1,channel2);

        Log.d(TAG,actual.toString());

        Assert.assertArrayEquals(TAG + ":" + "Fail short array concat ",expected,actual);
    }

    public void testArrayConcatDifferentLen(){

        short[] channel1 = {1,2,3,4,5};

        short[] channel2 = {5,6,7,8};

        short[] expected = {1,2,3,4,5,5,6,7,8};

        short[] actual;

        actual = ArrayConcat.concat(channel1,channel2);

        Log.d(TAG,actual.toString());

        Assert.assertArrayEquals(TAG + ":" + "Fail short array concat ",expected,actual);
    }

    public void testArrayConcatOneEmpty(){

        short[] channel1 = {};

        short[] channel2 = {5,6,7,8};

        short[] expected = {5,6,7,8};

        short[] actual;

        actual = ArrayConcat.concat(channel1,channel2);

        Log.d(TAG,actual.toString());

        Assert.assertArrayEquals(TAG + ":" + "Fail short array concat ",expected,actual);
    }

    public void testArrayConcatEmpty(){

        short[] channel1 = {};

        short[] channel2 = {};

        short[] expected = {};

        short[] actual;

        actual = ArrayConcat.concat(channel1,channel2);

        Log.d(TAG,actual.toString());

        Assert.assertArrayEquals(TAG + ":" + "Fail short array concat ",expected,actual);
    }

}
