package com.spectocor.micor.mobileapp.devicelogs;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.LargeTest;

import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;
import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;

/**
 * Test cases for EcgDeviceLogService class
 */
public class EcgDeviceLogServiceTest extends AndroidTestCase {


    DeviceLogRepository repository;
    GlobalSettings settings;
    DeviceLogQueueRepository queueRepository;
    DeviceLogLogger logger;

    EcgDeviceLogService service;

    /**
     * instantiate test classes
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        super.setUp();

        Context context = getContext();
        context.deleteDatabase(LogEventsDatabaseHelper.DatabaseName);
        context.deleteDatabase(QueueDatabaseHelper.DatabaseName);

        LogEventsDatabaseHelper databaseHelper = new LogEventsDatabaseHelper(context);
        repository = new DeviceLogRepository(databaseHelper);

        QueueDatabaseHelper queueDatabaseHelper = new QueueDatabaseHelper(context);
        queueRepository = new DeviceLogQueueRepository(queueDatabaseHelper);

        //TODO: Try to add Mokito for unit tests. It is not acceptable to use actual class for all unit tests
        //repository = Mockito.mock(DeviceLogRepository.class);

        settings = new GlobalSettings();

        logger = new DeviceLogLogger(repository, settings, queueRepository);

        service = new EcgDeviceLogService(logger);
    }


    /*
    * test log ECG transmitter battery status with 100
    * Test Case ID : UTMAID000016
    */
    @LargeTest
    public void testLogEcgDeviceBatteryLevel() throws Exception {
        int time = (int) (System.currentTimeMillis() / 1000);
        DeviceLog log = service.logEcgDeviceBatteryLevel((byte) 100);
        assertEquals(log.getValueNumber(), 100.0);
        assertNull(log.getValueString());
        assertEquals(log.getDeviceLogInfoTypeId(), EnumDeviceLogInfoType.EcgDeviceBatteryLevel);
        assertTrue(log.getLogDateUnixSec() >= time);
    }

    /*
   * test log ECG transmitter battery status with 127
   * Test Case ID : UTMAID000017
   */
    @LargeTest
    public void testFailLogEcgTransmitterBatteryStatus() throws Exception {
        try {
            DeviceLog log = service.logEcgDeviceBatteryLevel((byte) 127);
            assertTrue(log == null);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }


    public void testLogEcgDeviceLeadStatusChanged() throws Exception {
        int time = (int) (System.currentTimeMillis() / 1000);
        DeviceLog log = service.logEcgDeviceLeadStatusChanged(true);
        assertEquals(log.getValueNumber(), 1.0);
        assertNull(log.getValueString());
        assertEquals(log.getDeviceLogInfoTypeId(), EnumDeviceLogInfoType.EcgDeviceLeadStatusChanged);
        assertTrue(log.getLogDateUnixSec() >= time);
    }


    /*
    * test log mobile device bluetooth on/off status
    * Test Case ID : UTMAID000018
    */
    public void testLogEcgDeviceConnectionStatus() throws Exception {
        int time = (int) (System.currentTimeMillis() / 1000);
        DeviceLog log = service.logEcgDeviceConnectionStatus(true);
        assertEquals(log.getValueNumber(), 1.0);
        assertNull(log.getValueString());
        assertEquals(log.getDeviceLogInfoTypeId(), EnumDeviceLogInfoType.EcgDeviceConnectionStatus);
        assertTrue(log.getLogDateUnixSec() >= time);
    }


}