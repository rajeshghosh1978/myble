package com.spectocor.micor.mobileapp.ecgdatachunks;

import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by qwang on 12/30/2015.
 * Test case for ECG data chunk
 *
 * Test Class @LINK EcgDataChunk
 *
 * *********************************************************
 * CHANGES HISTORY
 *
 * Date             Author          Comments
 * 02/17/2016       Qian            add changes history and running history
 * *********************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 02/17/2016       Qian            fail : createEcgDataChunkWithNull, createDefaultEcgDataChunk,createEcgDataChunkWithNegativeTime
 *
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class EcgDataChunkTest {
    /*
    * define test data
     */
    public static final int mDefaultSessionId = 0;

    public static final int mFailSessionId = -1000;
    public static final int mFailTime = -1000;

    public static final int mTestMonitoringSessionId = 10;
    public static final int mMaxiChunkLen = 15000;
    private static transient byte[] mDefaultChunkDataChannelCompressed;
    private static byte[] mEmptyChunkDataChannelCompressed = {};

    private static transient byte[] mTestChunkDataChannelCompressed;

    private EcgDataChunk mEcgDataChunk;

    public EcgDataChunkTest() {
        super();
    }

    // get current log time
    public static int getCurrentTime() {
        int curr_time;
        curr_time = (int) (System.currentTimeMillis() / 1000);
        return curr_time;
    }

    /*
    * test create blank ecg data chunk
    * Test Case ID : UTMAID020001
     */
    @Test
    public void createBlankEcgDataChunk() {
        mEcgDataChunk = new EcgDataChunk();   // test default construction function
        assertEquals(0, mEcgDataChunk.getMonitoringSessionId());
        assertTrue(getCurrentTime() >= mEcgDataChunk.getChunkStartDateUnixMs());
        assertEquals(null, mEcgDataChunk.getChunkDataChannelCompressed());
    }


    /*
     * test create ecg data chunk with default value
     * Test Case ID : UTMAID020002
     */
    @Test
    public void createDefaultEcgDataChunk() {
        mEcgDataChunk = new EcgDataChunk();
        mEcgDataChunk.setMonitoringSessionId(mDefaultSessionId);
        mEcgDataChunk.setChunkDataChannelCompressed(mDefaultChunkDataChannelCompressed);
        mEcgDataChunk.setChunkStartDateUnixMs(getCurrentTime());


        assertEquals(mDefaultSessionId, mEcgDataChunk.getMonitoringSessionId());
        assertTrue(getCurrentTime() >= mEcgDataChunk.getChunkStartDateUnixMs());
        assertEquals(mDefaultChunkDataChannelCompressed, mEcgDataChunk.getChunkDataChannelCompressed());
    }

    /*
    * test create ecg data chunk with incorrect session id
    * Test Case ID : UTMAID020003
    */
    @Test
    public void createEcgDataChunkWithFailSessionID() {
        mEcgDataChunk = new EcgDataChunk();

        try {
            mEcgDataChunk.setMonitoringSessionId(mFailSessionId);
            mEcgDataChunk.setChunkDataChannelCompressed(mDefaultChunkDataChannelCompressed);
            mEcgDataChunk.setChunkStartDateUnixMs(getCurrentTime());
            Assert.fail("Fail at create ECG data Chunk with session id: " + mFailSessionId);
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }

    }

    /*
    * test create ecg data chunk with null
    * Test Case ID : UTMAID020004
    */
    @Test
    public void createEcgDataChunkWithNull() {
        mEcgDataChunk = new EcgDataChunk();

        try {
            mEcgDataChunk.setMonitoringSessionId(mDefaultSessionId);
            mEcgDataChunk.setChunkDataChannelCompressed(null);
            mEcgDataChunk.setChunkStartDateUnixMs(getCurrentTime());
            Assert.fail("Fail at create ECG data Chunk with null byte array");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
    }

    /*
    * test create ecg data chunk with empty array
    * Test Case ID : UTMAID020005
    */
    @Test
    public void createEcgDataChunkWithEmpty() {
        mEcgDataChunk = new EcgDataChunk();

        mEcgDataChunk.setMonitoringSessionId(mDefaultSessionId);
        mEcgDataChunk.setChunkDataChannelCompressed(mEmptyChunkDataChannelCompressed);
        mEcgDataChunk.setChunkStartDateUnixMs(getCurrentTime());

        Assert.assertEquals(mEmptyChunkDataChannelCompressed, mEcgDataChunk.getChunkDataChannelCompressed());

    }

    /*
    * test create ecg data chunk with negative time number
    * Test Case ID : UTMAID020006
    */
    @Test
    public void createEcgDataChunkWithNegativeTime() {
        mEcgDataChunk = new EcgDataChunk();

        try {
            mEcgDataChunk.setMonitoringSessionId(mDefaultSessionId);
            mEcgDataChunk.setChunkDataChannelCompressed(mDefaultChunkDataChannelCompressed);
            mEcgDataChunk.setChunkStartDateUnixMs(mFailTime);
            Assert.fail("Fail at create ECG data Chunk with negative time number");
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
    }
}
