package com.spectocor.micor.mobileapp.uitestframework;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by qwang on 1/15/2016.
 */
public class PatientFeels {

    public final static int MaxiFeels = 11;
    Map<String, String> mPatientFeelList;
    String mKeys[];

    public PatientFeels() {
        mPatientFeelList = new HashMap<String, String>();
        mPatientFeelList.put("cb-chest-discomfort", "Chest Discomfort");
        mPatientFeelList.put("cb-racing-heart", "Racing Heart");
        mPatientFeelList.put("cb-fatigue", "Fatigue");
        mPatientFeelList.put("cb-fluttering", "Fluttering");

        mPatientFeelList.put("cb-slurred-speech", "Slurred Speech");  // popup message, onSlurredSpeech()
        mPatientFeelList.put("cb-palpitation", "Palpitation");
        mPatientFeelList.put("cb-headache", "Headache");
        mPatientFeelList.put("cb-other", "Other");

        mPatientFeelList.put("cb-shortness-of-breath", "Shortness of breath");
        mPatientFeelList.put("cb-recently-fainted", "Recently Fainted");  // popup message, onRecentlyFainted()
        mPatientFeelList.put("cb-dizziness", "Dizziness");

        mKeys = new String[MaxiFeels];
        int i = 0;
        for (String Key : mPatientFeelList.keySet()) {
            mKeys[i++] = Key;
        }
    }

    public String getText(String Key) {
        return mPatientFeelList.get(Key);
    }

    public String getText(int Key) {
        return getText(mKeys[Key]);
    }

    public String getKey(int Key) {
        return mKeys[Key];
    }

}
