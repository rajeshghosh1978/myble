package com.spectocor.micor.mobileapp.uitestframework;

import android.content.Context;
import android.support.test.espresso.NoMatchingViewException;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qwang on 1/12/2016.
 */
public class UITestSimpleFramework {

    private static String TAG = "UITestSimpleFramework";

    public static final String mTestDataPath = "ui_test_data/";
    private static long TestCmdPauseTime = 1000;  // standard pause time after each test command

    private byte buffer[];

    private Context mAppContext;

    private List<UITestCommand> mUITestCmds;

    public UITestSimpleFramework() {
        mAppContext = null;  // keep application context
        mUITestCmds = new ArrayList<>();
    }

    public UITestSimpleFramework(Context AppContext) {
        mAppContext = AppContext;  // keep application context
        mUITestCmds = new ArrayList<>();
    }

    public static String readTestDataFile(Context AppContext, String TestDataFileName) throws IOException, JSONException {

        String cmd_data;

        byte buffer[];

        // read json file
        // http://stackoverflow.com/questions/11820142/how-to-pass-a-file-path-which-is-in-assets-folder-to-filestring-path
        Assert.assertTrue(AppContext != null);
        InputStream is = AppContext.getAssets().open(mTestDataPath + TestDataFileName);

        int size = is.available();

        buffer = new byte[size];

        is.read(buffer);

        is.close();

        cmd_data = new String(buffer);

        return cmd_data;
    }

    /**
     * convert JSON String data to UI Test command list
     * http://stackoverflow.com/questions/17098763/how-to-make-a-json-string-using-the-key-value-from-the-map
     * @param JsonCmdData
     * @return
     * @throws JSONException
     */
    public static List<UITestCommand> convertUITestCmds(String JsonCmdData) throws JSONException {

        List<UITestCommand> UITestCmds;

        // init UI test command list
        JSONObject jsonRootObject = new JSONObject(JsonCmdData);

        //Get the instance of JSONArray that contains JSONObjects
        JSONArray jsonCmds = jsonRootObject.optJSONArray(UITestCommand.KeyCommands);

        JSONObject jsonCmd;

        UITestCmds = new ArrayList<>();

        //Iterate the jsonArray and print the info of JSONObjects
        for (int i = 0; i < jsonCmds.length(); i++) {

            jsonCmd = jsonCmds.getJSONObject(i);

            UITestCmds.add(new UITestCommand(jsonCmd));

        }

        return UITestCmds;
    }

    public static boolean pauseTestRunning(UITestCommand NextCmd) {
        boolean rc;
        String curr_cmd;

        if (NextCmd == null) {
            rc = pauseTestRunning();
        } else {
            curr_cmd = NextCmd.getCmdName();

            if (curr_cmd.compareTo(UITestCommand.CmdWaitingPage) == 0 || curr_cmd.compareTo(UITestCommand.CmdWaiting) == 0) {
                rc = false;
            } else {
                rc = pauseTestRunning();
            }
        }

        return rc;
    }

    /**
     * pause test
     *
     * @return
     */
    public static boolean pauseTestRunning() {
        boolean rc = false;

        try {
            rc = UIWebCmdUtility.waitTime(getTestPauseTime());
        } catch (Exception e) {
            rc = false;
            UITestLog.e(e.getMessage());
        }

        return rc;
    }

    /**
     * get test pause time
     *
     * @return
     */
    public static long getTestPauseTime() {
        return TestCmdPauseTime;
    }

    /**
     * set test pause time
     *
     * @param PauseTime
     */
    public static void setTestPauseTime(long PauseTime) {
        TestCmdPauseTime = PauseTime;
    }

    public boolean loadTestDataFile(String TestDataFileName) throws IOException, JSONException {
        boolean rc = true;

        // read json file
        String json_cmd_data = readTestDataFile(mAppContext, TestDataFileName);

        mUITestCmds = convertUITestCmds(json_cmd_data);

        return rc;
    }

    /**
     * run test with json file
     *
     * @param FileName
     * @return
     */
    public boolean runTestFile(String FileName) {

        boolean rc = false;

        try {

            rc = loadTestDataFile(FileName);

            if (rc)
                rc = runTest();
        } catch (JSONException jsonE) {
            rc = false;
            UITestLog.x(jsonE.getMessage());
        } catch (FileNotFoundException noFile) {
            rc = false;
            UITestLog.x(noFile.getMessage());
        } catch (IOException ioe) {
            rc = false;
            UITestLog.x(ioe.getMessage());
        }

        Assert.assertTrue(UITestLog.UITestPrefix + UITestLog.UITESTFailPrefix + FileName, rc == true);

        return rc;
    }

    /**
     * run UI test commands with Json String
     *
     * @param JsonTestCmds
     * @return
     */
    public boolean runTest(String JsonTestCmds) {

        boolean rc = false;

        try {
            List<UITestCommand> cmds = convertUITestCmds(JsonTestCmds);

            if (cmds != null && cmds.size() > 0)
                rc = runTest(cmds);
        } catch (JSONException jsonE) {
            UITestLog.x(jsonE.getMessage());
        }

        return rc;
    }

    /**
     * run test command with commands
     *
     * @param UITestCommands
     * @return
     */
    public boolean runTest(List<UITestCommand> UITestCommands) {

        UITestCommand curr_test_cmd;

        boolean test_rc = false;

        Assert.assertTrue(UITestCommands != null);
        Assert.assertTrue(UITestCommands.size() > 0);

        try {
            for (int i = 0; i < UITestCommands.size(); i++) {

                curr_test_cmd = UITestCommands.get(i);

                test_rc = curr_test_cmd.execute();

                if (test_rc) {
                    UITestLog.pass(curr_test_cmd.dumpCmd());
                } else {
                    UITestLog.fail(curr_test_cmd.dumpCmd());   // dump fail
                    UITestCommand.clickResetBtn();             // click reset
                    Assert.fail(curr_test_cmd.dumpCmd());      // before raise assert
                }

                // put pause
                if (i < UITestCommands.size() - 1) {
                    pauseTestRunning(UITestCommands.get(i + 1));
                }
            }

        } catch (IllegalArgumentException ie) {
            test_rc = false;
            UITestLog.x(ie.getMessage());
            //Assert.fail(mLogPrefix + "Exception:" + ie.getMessage());
        } catch (NoMatchingViewException ne) {
            //System.out.print(mLogPrefix + "Exception:" + ne.getMessage());
            test_rc = false;
            Assert.fail(TAG + ":" + ne.getMessage());
        } catch (Exception e) {
            test_rc = false;
            Assert.fail(TAG + ":" + e.getMessage());
            //System.out.print(mLogPrefix + "Exception:" +e .getMessage());
        }

        Assert.assertTrue(UITestCommands.get(0).dumpCmd(), test_rc == true);

        return test_rc;

    }

    /**
     * run test command at current command list
     *
     * @return
     */
    public boolean runTest() {

        boolean test_rc = false;

        if (mUITestCmds != null) {
            if (mUITestCmds.size() > 0) {
                test_rc = runTest(mUITestCmds);
            }
        }

        return test_rc;
    }

    /**
     * set App context for Test Framework
     *
     * @param AppContext
     */
    public void setContext(Context AppContext) {
        Assert.assertTrue(AppContext != null);
        mAppContext = AppContext;
    }
}
