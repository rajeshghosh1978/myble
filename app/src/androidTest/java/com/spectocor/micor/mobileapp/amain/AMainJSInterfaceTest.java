package com.spectocor.micor.mobileapp.amain;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.UiThread;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import android.support.v4.content.LocalBroadcastManager;
import android.test.UiThreadTest;
import android.test.suitebuilder.annotation.LargeTest;

import com.spectocor.micor.mobileapp.uitestframework.UIWebCmdUtility;
import com.spectocor.testutilities.IntentUtility;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.netwrok.WIFIUtility;
import com.spectocor.testutilities.utils.ThreadUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;

import static android.support.test.espresso.web.sugar.Web.onWebView;
import org.junit.Test;

import android.util.Log;

/**
 * Created by qwang on 2/8/2016.
 *
 * test case for JSInterface function
 *
 * TODO: only test the following API, not all.
 *
 *     GetEnrollmentByActivationCode
 *
 *     DisplayEnrollmentInfo
 *
 *     HomeScreenJustActivatedSession
 *
 *     GetCurrentMobileBattery
 *
 *     ShowLongToast
 *
 *     isInternetAvailable()
 *
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class AMainJSInterfaceTest{

    //////////////////////////////////////////////////////////////
    // private member variable
    //////////////////////////////////////////////////////////////

    private static String TAG = "AMainJSInterfaceTest";

    private static AMainJSInterface mMainJSInterface;
    private static Context mContext;
    private static AMainActivity mActivity;

    private static long mDefaultSleepMillionSecond = TestEnvGlobalConst.APP_SLEEP_MILLION_SECOND;

    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init();
        }
    };

    @Before
    public void before(){
        // waiting activity launch
        SystemClock.sleep(mDefaultSleepMillionSecond);

        // jump to ecg signal view
        mActivity.callWebviewJsFunction("displayAdminEcgSignalPage()");
    }

    @After
    public void after(){
        // waiting tester to check UI
        SystemClock.sleep(mDefaultSleepMillionSecond);
    }

    ///////////////////////////////////////////////////////////////
    // public void API_getEnrollmentByActivationCode(final String activationCode, final String jsCallbackFn)
    ///////////////////////////////////////////////////////////////
    @Test
    public void testGetEnrollmentByActivationCode(){

        String callbackFuncName = "onEnrollmentByActivationCodeReady";

        String activationCode = "1234";

        mMainJSInterface.API_getEnrollmentByActivationCode(activationCode, callbackFuncName);


    }

    @Test
    public void testUIDisplayEnrollmentInfo(){

        String jsonStr = "{" +
                            "'sessionId':'" + "1" + "'," +
                            "'patientName':'" + "John" + " " + "Doe" + "'," +
                            "'enrollmentDate':'"+ "2016/1/22" +"'," +
                            "'facilityName':'"+ "NHS"+"'," +
                            "'facilityId':'45'" +
                            "}";


        String callbackFuncName = "onEnrollmentByActivationCodeReady";

        String fnCallbackFull = callbackFuncName + "("+jsonStr+")";

        Bundle b = new Bundle();

        b.putString("callbackFnString", fnCallbackFull);


        LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));

    }

    ///////////////////////////////////////////////////////////////
    // AtHomeScreenJustActivatedSession()
    ///////////////////////////////////////////////////////////////
    @Test
    public void testAtHomeScreenJustActivatedSession(){


        mMainJSInterface.atHomeScreenJustActivatedSession();

        // verify screen
        // there is nothing there

    }

    ///////////////////////////////////////////////////////////////
    // getCurrentMobileBattery
    ///////////////////////////////////////////////////////////////
    @Test
    public void testGetCurrentMobileBattery(){


        int battery = mMainJSInterface.getCurrentMobileBattery();

        //Log.d(TAG,"Battery:" + battery);

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent expectedBatteryStatus = mContext.registerReceiver(null, ifilter);

        int level = expectedBatteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = expectedBatteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryPct = (level / (float)scale ) * 100;

        int expectedBattery = (int)batteryPct;

        //Log.d(TAG,"Expected Battery:" + batteryPct);


        // verify value
        Assert.assertTrue(TAG + ":Fail to pass mobile device battery test", expectedBattery == battery);

    }

    ///////////////////////////////////////////////////////////////
    // MainJSInterface.showLongToast(toast_msg)
    ///////////////////////////////////////////////////////////////
    /**
     * test show toast
     */
    @Test
    public void testShowLongToast(){

        String toast_msg = "testShowLongToast";

        mMainJSInterface.showLongToast(toast_msg);

        Assert.assertTrue(TAG+":Fail to verify ShowLongToast",UIWebCmdUtility.checkToastContentByID(toast_msg));
    }
    /**
     * test show toast with null
     */
    @Test
    public void testNullShowLongToast(){

        //String toast_msg = null;

        try {
            mMainJSInterface.showLongToast(null);
        }catch (NullPointerException e){
            Assert.assertTrue(TAG + ":Fail to verify ShowLongToast with null string reference", false);
        }

    }
    /**
     * test show toast with empty string
     */
    @Test
    public void testEmptyStringShowLongToast(){

        String toast_msg = "";

        try {
            mMainJSInterface.showLongToast(toast_msg);
            Assert.assertTrue(TAG + ":Fail to verify ShowLongToast", UIWebCmdUtility.checkToastContentByID(toast_msg));
        }catch (NullPointerException e){
            Assert.assertTrue(TAG + ":Fail to verify ShowLongToast with empty string reference", false);
        }
    }
    ///////////////////////////////////////////////////////////////
    // MainJSInterface.isInternetAvailable()
    ///////////////////////////////////////////////////////////////
    /**
     * test network is available or not
     *
     */
    @Test
    public void testIsInternetAvailable(){

        checkMemberVariable();

        boolean rc = mMainJSInterface.isInternetAvailable();

        Assert.assertTrue(TAG + ":Fail to verify Internet available function",rc);
    }

    @Test
    public void testWIFISimulator(){

        WIFIUtility wifiSimulator = WIFIUtility.getInstance(mContext);

        //wifiSimulator.simulatorNetworkStatus();
        wifiSimulator.dumpWIFIConfig();

        boolean thread_status = true;

        while(thread_status){

            if(wifiSimulator.getThreadStatus() == Thread.State.TERMINATED){
                thread_status = false;
            }else {
                try{
                    Thread.sleep(100);
                    Log.d(TAG,"Sleep");
                }catch (InterruptedException e){
                    Log.d(TAG,"InterruptedException");
                }

            }

        }
    }

    ///////////////////////////////////////////////////////////////////
    // support function
    ///////////////////////////////////////////////////////////////////
    /**
     * init class member variables
     */
    private void init(){

        mActivity = mActivityRule.getActivity();
        mContext  = mActivity.getApplicationContext();
        mMainJSInterface = new AMainJSInterface(mContext);

        assert(mActivity != null);
        assert(mContext != null);
        assert(mMainJSInterface != null);

        Log.d(TAG,"init");
    }


    private boolean checkMemberVariable(){

        Assert.assertTrue(TAG + ": Fail to verify MainActivity is not null",mActivity != null);
        Assert.assertTrue(TAG + ": Fail to verify Context is not null", mContext != null);
        Assert.assertTrue(TAG + ": Fail to verify MainJSInterface is not null",mMainJSInterface != null);

        return true;
    }

    private void waitingAppLaunch(int Second){

        waitingSecond(Second);
    }

    private void waitingAppEnd(int Second){
        waitingSecond(Second);
    }

    private void waitingSecond(int Second){

        long waiting_time = 0;

        if (Second <=0 || Second >= 120){
            waiting_time = 1000;
        }else
        {
            waiting_time = Second * 1000;
        }
        SystemClock.sleep(waiting_time);
    }

    private Intent createBTIntent(int btEventType, Bundle b) {
        // createBTIntent
        Intent intent = new Intent("BLUETOOTH_EVENT");
        intent.putExtra("EVENT_TYPE", btEventType);
        intent.putExtras(b);
        return intent;
    }
}
