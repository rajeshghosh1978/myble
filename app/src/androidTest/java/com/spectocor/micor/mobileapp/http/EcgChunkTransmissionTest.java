package com.spectocor.micor.mobileapp.http;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.http.common.NetworkUtil;
import com.spectocor.testutilities.ECGDataCreator;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.session.api.SendingECGDataChunkRESTAPI;
import com.spectocor.testutilities.utils.ThreadUtility;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLSocketFactory;

import okhttp3.CertificatePinner;
import okhttp3.ConnectionSpec;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by qwang on 1/29/2016.
 * Test case for ECG Chunk data transmission from mobile phone to server
 *
 * Not test for server received correct chunk data or not
 * Not test buffering chunk data if network or service is not available
 *
 * QA : ALGO-975
 *
 * * CHANGES HISTORY
 *************************************************************
 * Date             Author          Comments
 * 01/29/2016       Qian            create test case for ecg data chunk transmission
 * 02/24/2016       Qian            update SSL
 * 02/25/2016       Qian            update basic authentication information at head, ECG device ID, PAD device ID
 *
 *************************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 02/17/2016       Qian            Pass
 * 02/25/2016       Qian            Pass
 */
public class EcgChunkTransmissionTest extends AndroidTestCase {

    public static final String TAG = "EcgChunkTransmissionTest";

    private static Context mContext;

    private static CurrentSessionState mSessionState;
    ///////////////////////////////////////////
    // test data
    ///////////////////////////////////////////
    public static String SERVER_ADDRESS = "http://207.191.20.80"; //"https://gateway.spectocor.com/" ;   //"http://208.67.179.147"; // http://207.191.20.80
    public static String SSL_SERVER_ADDRESS = "https://gateway.spectocor.com/" ;   //"http://208.67.179.147"; // http://207.191.20.80
    public static String FAIL_SERVER_ADDRESS = "http://208.69.179.147";
    public static String URL_ECG_CHUNK = "/en/v1/ecg/chunk";  // /en/v1/ecg/chunk

    public static String ServiceParaNameSessionID                 = "sessionId";
    public static String ServiceParaNameFile                      = "file";
    public static String ServiceNameEcgId                         = "ecgId";
    public static String ServiceNameDeviceId                      = "deviceId";
    public static String ServiceParaValFile                       = "data.comp";
    public static String ServiceParaNameChunkStartTimestamp       = "chunkStartTimestamp";

    private static int mDefaultSessionID = 1;//1234;
    private static int mDefaultChunkSize = 1024;
    private static int mDefaultMaxiSessionID = 987654;
    private static int mDefaultTestChunkNum = TestEnvGlobalConst.DEFAULT_RUN_TEST_TIME;
    private static String mDefaultEcg = DeviceIdentifiers.getEcgId();  // "SPEC-SD5F855GHJ";
    private static String mDefaultPda = DeviceIdentifiers.getPdaId();  // "AHSD4F9F4DD4SD4FF9";


    /**
     * setup
     */
    @Override
    public void setUp(){
        mContext = getContext(); // get environment
        assert(mContext != null);
        // check test environment
        mSessionState = SessionStateAccess.getCurrentSessionState(mContext);
        // session active
        Assert.assertTrue(TAG+":Fail to start test: there is no active session at this device",mSessionState.sessionActive);
        // session id is 1
        //Assert.assertTrue(TAG+":Fail to start test: expected session id:"+mDefaultSessionID+":real session id:" + mSessionState.getSessionId() +"  at this device",mSessionState.getSessionId() == mDefaultSessionID);

        Log.d(TAG,"session id:"+mSessionState.sessionId);

    }

    ///////////////////////////////////////////
    // test case
    ///////////////////////////////////////////

    /**
     * happy path for sending ecg data chunk
     * input ecg chunk
     * return successful response
     *
     * Test Case ID : UTMAID040001
    */
    public void testSuccessfulSendOneECGDataChunk(){

        EcgDataChunk chunkData =  createDefaultEcgDataChunk(); // prepare ECG data chunk
        Response response;
        boolean rc;

        try{
            response = SendingECGDataChunkRESTAPI.SendingEcgChunkRequestCall(SSL_SERVER_ADDRESS, URL_ECG_CHUNK, chunkData);  // send it
            Log.d(TAG,response.message());
            rc = SendingECGDataChunkRESTAPI.checkResponseSuccess(response);// check response
            Assert.assertTrue("Fail to send ECG data chunk:"+response.message(), rc);

        }catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage());
            Assert.fail(TAG + ":" + ioe.getMessage());
        }
    }

    /**
    * fail case if there is no network
    * input ecg chunk
    * return fail response or throws exception
    *
    * mock it. turn off network connection
    *
    * need manually turn on Airplane mode
    * Test Case ID : UTMAID040002
    */
    public void testFailSendOneECGDataChunkWithoutNetwork(){

        EcgDataChunk chunkData =  createDefaultEcgDataChunk(); // prepare ECG data chunk
        Response response;
        boolean rc;

        try{
            //toggleAirplaneMode();
            response = SendingECGDataChunkRESTAPI.SendingEcgChunkRequestCall(SSL_SERVER_ADDRESS, URL_ECG_CHUNK, chunkData);  // send it
            rc = SendingECGDataChunkRESTAPI.checkResponseSuccess(response);// check response
            if(NetworkUtil.getConnectivityStatus(mContext) == NetworkUtil.TYPE_MOBILE)
            {
                Assert.assertTrue(TAG + ":" + "Fail to send ECG data chunk",true);
            }else {
                Assert.assertTrue(TAG + ":" + "Fail to send ECG data chunk", false);
            }
        }catch (java.net.ConnectException nete){
            Assert.assertTrue(TAG + ":" + "Pass sending data without network connection",true);
        }
        catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage());
            Assert.fail(TAG + ":" + ioe.getMessage());
        }finally {
            //toggleAirplaneMode();
        }
    }

    /*
    * fail case if there is no service
    * input ecg chunk
    * return fail response or throws exception
    *
    * mock it. change service IP
    * Test Case ID : UTMAID040003
    */
    public void testFailSendOneECGDataChunkWithoutService(){

        EcgDataChunk chunkData =  createDefaultEcgDataChunk(); // prepare ECG data chunk
        Response response;
        boolean rc;

        try{
            response = SendingECGDataChunkRESTAPI.SendingEcgChunkRequestCall(FAIL_SERVER_ADDRESS, URL_ECG_CHUNK, chunkData);  // send it
            rc = SendingECGDataChunkRESTAPI.checkResponseSuccess(response);// check response
            Assert.assertTrue(TAG + ":" + "Fail to send ECG data chunk without service", false);

        }catch (java.net.SocketTimeoutException timroute){
            Assert.assertTrue(TAG + ":" + "Pass sending data without Service",true);
        }
        catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage());
            Assert.fail(TAG + ":" + ioe.getMessage());
        }
    }
    /**
    * fail case if sending same chunk data twice
    * input ecg chunk
    * send the first time
    * get success response
    * send the second time
    * return fail response or throws exception
    *
    * Test Case ID : UTMAID040004
    */
    public void testSendDuplicateECGDataChunk(){

        EcgDataChunk chunkData =  createDefaultEcgDataChunk(); // prepare ECG data chunk


        Response response;
        boolean rc;

        try{
            response = SendingECGDataChunkRESTAPI.SendingEcgChunkRequestCall(SSL_SERVER_ADDRESS, URL_ECG_CHUNK, chunkData);  // send it
            Log.d(TAG,response.message());
            rc = SendingECGDataChunkRESTAPI.checkResponseSuccess(response);// check response

            Assert.assertTrue("Fail to send ECG data chunk:"+ response.message(),rc);

            response = SendingECGDataChunkRESTAPI.SendingEcgChunkRequestCall(SSL_SERVER_ADDRESS, URL_ECG_CHUNK, chunkData);  // send it again
            Log.d(TAG,response.message());
            rc = SendingECGDataChunkRESTAPI.checkResponseSuccess(response);// check response
            Assert.assertFalse("Fail to send duplicate ECG data chunk:" + response.message(), rc);

        }catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage());
            Assert.fail(TAG + ":" + ioe.getMessage());
        }
    }

    /**
     * random sending ECG chunk data
     * Test Case ID : UTMAID040005
     */
    public void testRandomSendECGDataChunk(){

        List<EcgDataChunk> chunks =  createRandomEcgDataChunk(mDefaultTestChunkNum); // prepare ECG data chunks
        Response response;
        boolean rc;

        try{
            for(int i = 0; i < mDefaultTestChunkNum ; ++i){

                response = SendingECGDataChunkRESTAPI.SendingEcgChunkRequestCall(SSL_SERVER_ADDRESS, URL_ECG_CHUNK, chunks.get(i));  // send it
                Log.d(TAG,response.message());
                rc = SendingECGDataChunkRESTAPI.checkResponseSuccess(response);// check response
                Assert.assertTrue("Fail to send ECG data chunk", rc);
                ThreadUtility.applicationSleep(1000);
            }
        }catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage());
            Assert.fail(TAG + ":" + ioe.getMessage());
        }
    }

    /**
    * check using HTTPS or not
    * input ecg chunk
    * send chunk data with http
    * return fail response or throws exception
    *
    * manual review code to check using HTTPS or not
    *
    * http://www.programcreek.com/java-api-examples/index.php?api=com.squareup.okhttp.ConnectionSpec
    * http://chariotsolutions.com/blog/post/https-with-client-certificates-on/

    * Test Case ID : UTMAID040006
    */
    public void testSSLConnection(){

        boolean rc = false;

        OkHttpClient httpClient = HttpManager.getInstance().getHttpClient();

        // check ConnectionSpec
        List<ConnectionSpec> connectionSpecList = httpClient.connectionSpecs();

        Assert.assertTrue(TAG+"There is no connectionSpec",connectionSpecList.size()>0);

        for(int i = 0; i < connectionSpecList.size();++i){
            Log.i(TAG + "num:" + i,connectionSpecList.get(i).toString());
            if(connectionSpecList.get(i).isTls()){
                rc = true;
            }
        }

        Assert.assertTrue(TAG + ":" + "Fail to pass SSL connection", rc);

        List<Protocol> protocolsList = httpClient.protocols();

        for(int i = 0; i < protocolsList.size();++i){
            Log.i(TAG + "num:" + i,protocolsList.get(i).toString());
        }


        // check SSLSocketFactory
        SSLSocketFactory sl = httpClient.sslSocketFactory();

        Assert.assertTrue(TAG + ":" + "Fail to pass SSL connection", sl != null);

        // check CertificatePinner
        CertificatePinner cp = httpClient.certificatePinner();

        Assert.assertTrue(TAG + ":" + "Fail to pass SSL connection", cp != null);

        // check URL
        Assert.assertTrue(TAG + ":" + "Fail to pass SSL connection", SSL_SERVER_ADDRESS.contains("https"));

    }

    /**
     * fail to send ECG data chunk with non-active session id
     * input ecg chunk, session id
     * return successful response
     *
     * Test Case ID : UTMAID040007
     */
    public void testSendOneECGDataChunkWithIllegalSessionID(){

        long startSecond = System.currentTimeMillis()/1000;
        int sessionID = 9001;
        EcgDataChunk chunkData =  createFailEcgDataChunk(startSecond,sessionID); // prepare ECG data chunk
        Response response;
        boolean rc;

        try{
            response = SendingECGDataChunkRESTAPI.SendingEcgChunkRequestCall(SSL_SERVER_ADDRESS, URL_ECG_CHUNK, chunkData);  // send it
            Log.d(TAG,response.message());
            rc = SendingECGDataChunkRESTAPI.checkResponseSuccess(response);// check response
            Assert.assertTrue("Fail to send ECG data chunk:"+response.message(), response.message().compareTo("Unauthorized") ==0 );

        }catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage());
            Assert.fail(TAG + ":" + ioe.getMessage());
        }
    }

    /**
     * fail to send ECG data chunk with illegal ecg device id and pad device id
     * input ecg chunk, illegal ecg device id and pad device id
     * return successful response
     *
     * Test Case ID : UTMAID040008
     */
    public void testSendOneECGDataChunkWithIllegalDeviceID(){

        //long startSecond = System.currentTimeMillis()/1000;
        //int sessionID = 9001;
        EcgDataChunk chunkData =  createDefaultEcgDataChunk(); // prepare ECG data chunk
        Response response;
        boolean rc;

        String illegalECGDeviceID = "1111";
        String illegalPDADeviceID = "1111";

        try{
            SendingECGDataChunkRESTAPI.setECGDeviceID(illegalECGDeviceID);
            SendingECGDataChunkRESTAPI.setPDADeviceID(illegalPDADeviceID);
            response = SendingECGDataChunkRESTAPI.SendingEcgChunkRequestCall(SSL_SERVER_ADDRESS, URL_ECG_CHUNK, chunkData);  // send it
            Log.d(TAG,response.message());
            rc = SendingECGDataChunkRESTAPI.checkResponseSuccess(response);// check response
            Assert.assertTrue("Fail to send ECG data chunk:"+response.message(), response.message().compareTo("Gone") ==0 );

        }catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage());
            Assert.fail(TAG + ":" + ioe.getMessage());
        }
    }

    /**
     * send ECG data chunk with expired data
     *
     * input ecg chunk
     * return successful response
     *
     * Test Case ID : UTMAID040009
     */
    public void testSendOneECGDataChunkWithExpiredTimeStamp(){

        long startSecond = System.currentTimeMillis()/1000 + 31 * 24 * 60 * 60; // after 31 days

        EcgDataChunk chunkData =  createDefaultEcgDataChunk(startSecond); // prepare ECG data chunk
        Response response;
        boolean rc;

        try{
            response = SendingECGDataChunkRESTAPI.SendingEcgChunkRequestCall(SSL_SERVER_ADDRESS, URL_ECG_CHUNK, chunkData);  // send it
            Log.d(TAG,response.message());
            rc = SendingECGDataChunkRESTAPI.checkResponseSuccess(response);// check response
            Assert.assertTrue("Fail to send ECG data chunk:"+response.message(), response.message().compareTo("Gone") ==0 );

        }catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage());
            Assert.fail(TAG + ":" + ioe.getMessage());
        }
    }

    /**
     * send ECG data chunk with previous date
     *
     * input ecg chunk
     * return successful response
     *
     * Test Case ID : UTMAID040010
     */
    public void testSendOneECGDataChunkWithPreviousTimeStamp(){

        long startSecond = System.currentTimeMillis()/1000 - 31 * 24 * 60 * 60; // before 31 days

        EcgDataChunk chunkData =  createDefaultEcgDataChunk(startSecond); // prepare ECG data chunk
        Response response;
        boolean rc;

        try{
            response = SendingECGDataChunkRESTAPI.SendingEcgChunkRequestCall(SSL_SERVER_ADDRESS, URL_ECG_CHUNK, chunkData);  // send it
            Log.d(TAG,response.message());
            rc = SendingECGDataChunkRESTAPI.checkResponseSuccess(response);// check response
            Assert.assertTrue("Fail to send ECG data chunk:"+response.message(), response.message().compareTo("Gone") ==0 );

        }catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage());
            Assert.fail(TAG + ":" + ioe.getMessage());
        }
    }

    /**
     * test case for sending ecg data chunk without ECG device ID
     * input ecg chunk
     * return successful response
     *
     * Test Case ID : UTMAID040011
     */
    public void testSendOneECGDataChunkWithoutECGDeviceID(){

        EcgDataChunk chunkData =  createDefaultEcgDataChunk(); // prepare ECG data chunk
        Response response;
        boolean rc;
        String message;


        try{
            response = SendingECGDataChunkRESTAPI.SendingEcgChunkRequestCallWithoutECGDeviceID(SSL_SERVER_ADDRESS, URL_ECG_CHUNK, chunkData);  // send it
            message = response.message();
            Log.d(TAG,message);
            //rc = SendingECGDataChunkRESTAPI.checkResponseSuccess(response);// check response
            Assert.assertTrue("Fail to send ECG data chunk:"+message,message.contains("Unauthorized"));

        }catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage());
            Assert.fail(TAG + ":" + ioe.getMessage());
        }
    }

    //////////////////////////////////
    // support function
    //////////////////////////////////

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    /**
     * create a default ECG data chunk
     * @return
     */
    public static EcgDataChunk createDefaultEcgDataChunk(){
//        EcgDataChunk chunkData;
//
//        chunkData = new EcgDataChunk();
//
//        chunkData.setMonitoringSessionId(mDefaultSessionID);
//        chunkData.setChunkDataChannelCompressed(ECGDataCreator.createECGDataChunk(mDefaultChunkSize));
//        chunkData.setChunkStartDateUnixMs(System.currentTimeMillis());

        // waiting for one second
        ThreadUtility.applicationSleep(1000);
        long startSecond = System.currentTimeMillis()/1000;

        return createDefaultEcgDataChunk(startSecond);
    }

    /**
     * create a ECG data chunk
     *
     * @param startSecond
     * @return
     */
    public static EcgDataChunk createDefaultEcgDataChunk(long startSecond){

        long elapseSecond = 300;
        int sessionID = SessionStateAccess.getCurrentSessionState(mContext).getSessionId();//1; // should get current session id

        Log.d(TAG,"session id:"+sessionID);

        EcgDataChunk chunkData =  ECGDataCreator.createRandomEcgDataChunk(startSecond,elapseSecond,sessionID); // prepare ECG data chunk

        return chunkData;
    }

    public static List<EcgDataChunk> createRandomEcgDataChunk(int ListSize){

        List<EcgDataChunk> chunks = new ArrayList<>();
        EcgDataChunk chunkData;

        assert (ListSize > 0);

        long currStartSecond = System.currentTimeMillis()/1000;
        long increaseSecond = 300;

        for(int i =0 ; i < ListSize; ++i){

//            chunkData = new EcgDataChunk();
//
//            chunkData.setMonitoringSessionId(ECGDataCreator.createSessionID(mDefaultMaxiSessionID));
//            chunkData.setChunkDataChannelCompressed(ECGDataCreator.createECGDataChunk(mDefaultChunkSize));
//            chunkData.setChunkStartDateUnixMs(System.currentTimeMillis());
            chunkData = createDefaultEcgDataChunk(currStartSecond);
            chunks.add(chunkData);
            currStartSecond += increaseSecond;
        }

        return chunks;
    }

    /**
     * create fail test ECG Data Chunk
     *
     * @param startSecond
     * @param sessionID
     * @return
     */
    public static EcgDataChunk createFailEcgDataChunk(long startSecond,int sessionID){

        long elapseSecond = 300;

        Log.d(TAG,"session id:"+sessionID);

        EcgDataChunk chunkData =  ECGDataCreator.createRandomEcgDataChunk(startSecond,elapseSecond,sessionID); // prepare ECG data chunk

        return chunkData;
    }

    // http://stackoverflow.com/questions/5533881/toggle-airplane-mode-in-android
    public void toggleAirplaneMode(){
        Context context = getContext();
        // read the airplane mode setting
//        boolean isEnabled = Settings.System.getInt(
//                context.getContentResolver(),
//                Settings.System.AIRPLANE_MODE_ON, 0) == 1;

        boolean isEnabled = Settings.System.getInt(
                context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) == 1;
        // toggle airplane mode
        Settings.System.putInt(
                context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, isEnabled ? 0 : 1);

        // Post an intent to reload
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", !isEnabled);
        context.sendBroadcast(intent);
    }
}
