package com.spectocor.micor.mobileapp.mobileheartrate;

import android.content.Context;
import android.test.AndroidTestCase;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.DbHelper;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.ecgadapter.compression.zip.ZipDecompression;
import com.spectocor.micor.mobileapp.ecgblehandler.EcgChannelBluetoothData;
import com.spectocor.micor.mobileapp.ecgadapter.LiveEcgChunkInMemory;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.testutilities.ECGDataCreator;
import com.spectocor.testutilities.utils.ListUtility;
import com.spectocor.testutilities.utils.NumberUtils;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;

import java.io.IOException;
import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;

/**
 * Created by qwang on 2/3/2016.
 *
 * collect ECG data from bluetooth device
 * one second ECG data
 *
 * package two channel data, compression and storage sql lite database
 */
public class LiveEcgChunkInMemoryTest extends AndroidTestCase{

    public static final String TAG = "EcgChunkInMemoryTest";

    private Context mContext;

    private EcgChunkDbHelper mEcgChunkDb; // reference for EcgChunkDbHelper

    private List<EcgDataChunk> inputChunks;

    public static final String SD_DB_PATH ="/storage/ext_sd/Android/data/com.spectocor.micor.mobileapp/files/databases/";  // htc desire 626 SD card data path

    public static int sample_rate = 400;

    public static int default_chunk_gap_min_second = 2;

    public static int default_chunk_second_size = 5 * 60 ; //default chunk size

    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();

        mContext = getContext();

        //mDBPath = SD_DB_PATH;

        //mEcgChunkDbName = "MicorDatabase.db";

        //mEcgChunkTableName = "ECG_CHUNKS";

        //mContext.deleteDatabase(mEcgChunkDbName);  // delete database

        //EcgChunkDbHelper.dbPath = SD_DB_PATH;   // set db path


        mEcgChunkDb = EcgChunkDbHelper.getInstance(mContext);  // create DB object at SD card

        //boolean rc = isExternalStoragePresent();

        //Assert.assertTrue(TAG+":"+"Fail to create ECG data chunk database at SD ",rc);

        inputChunks = new ArrayList<EcgDataChunk>();

        mSessionID = SessionStateAccess.getCurrentSessionState(mContext).getSessionId();

    }

    @AfterClass
    public void tearDown() throws Exception{
        if(mEcgChunkDb != null) {
            //mEcgChunkDb.close();
            DbHelper.getInstance(mContext).close();
            //exportDB();
        }
        super.tearDown();
    }
    ///////////////////////////////////////////
    // test data
    ///////////////////////////////////////////
    private static int mSessionID;

    ///////////////////////////////////////////
    // test case
    ///////////////////////////////////////////

    /**
     * happy path for packaging certain amount minutes ECG and save to DB
     * five minutes BT data
     *
     *
     * Test Case ID : UTMAID050001
     */
    public void testSuccessfulSendOneECGDataChunk(){

        EcgChannelBluetoothData oneSecondData;

        int data_chunk_minutes = 5; // unit is minute

        int bt_data_minutes = 1;  // unit is second

        long start_stamp = System.currentTimeMillis()/1000;  // BT data , unit second

        long current_stamp = start_stamp;

        List<EcgChannelBluetoothData> btDataList = new ArrayList<EcgChannelBluetoothData>();

        Log.d(TAG,"Start time:"+current_stamp );

        for(int i = 0; i < data_chunk_minutes * 60 ; ++i){

            oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes,current_stamp);

            Log.d(TAG,""+oneSecondData.ch1.length);
            Log.d(TAG,""+oneSecondData.ch2.length);
            LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);

            //SystemClock.sleep(900);
            //current_stamp += getMillisecondStepBasedSampleRate();
            current_stamp += 1;

            btDataList.add(oneSecondData);
        }

        // should be empty
        EcgDataChunk emptyEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();
        Assert.assertTrue(emptyEcgDataChunk == null);

        // add extra one for get data chunk
        oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes,current_stamp);

        LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);
        //create expected ECG data chunk
        EcgDataChunk expectedEcgDataChunk = ECGDataCreator.EcgChannelBluetoothData2EcgDataChunk(mSessionID,btDataList);

        // get real ECG data chunk which will store at DB
        EcgDataChunk actualEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();

        byte[] expectedData = uncompressionEcgDataChunk(expectedEcgDataChunk);
        byte[] actualData   = uncompressionEcgDataChunk(actualEcgDataChunk);


        Assert.assertTrue(TAG + ":" + "Fail to convert BT raw data to ECG data chunk",compareEcgDataChunk(expectedEcgDataChunk,actualEcgDataChunk));

    }

    /**
     * give one second BT data which is less than sample rate data , for example 200
     * expect : log error, ignore it and create a new data chunk
     *
     * Test Case ID : UTMAID050002
     */
    public void testFeedChannelDataLessOneSecond(){

        EcgChannelBluetoothData oneSecondData;

        int data_chunk_minutes = 4; // unit is minute

        int bt_data_minutes = 1;  // unit is second

        long start_stamp = System.currentTimeMillis()/1000;  // BT data , unit second

        long current_stamp = start_stamp;

        List<EcgChannelBluetoothData> btDataList = new ArrayList<EcgChannelBluetoothData>();

        Log.d(TAG,"Start time:"+current_stamp );

        for(int i = 0; i < data_chunk_minutes * 60 ; ++i){

            oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes,current_stamp);

            Log.d(TAG,""+oneSecondData.ch1.length);
            Log.d(TAG,""+oneSecondData.ch2.length);
            LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);

            //SystemClock.sleep(900);
            //current_stamp += getMillisecondStepBasedSampleRate();
            current_stamp += 1;

            btDataList.add(oneSecondData);
        }

        // should be empty
        EcgDataChunk emptyEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();
        Assert.assertTrue(emptyEcgDataChunk == null);

        // add extra one for get data chunk with different sample rate
        // should ignore and create a new chunk
        // sample rate is 100
        oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes,current_stamp,100);

        LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);
        //create expected ECG data chunk
        EcgDataChunk expectedEcgDataChunk = ECGDataCreator.EcgChannelBluetoothData2EcgDataChunk(mSessionID,btDataList);

        // get real ECG data chunk which will store at DB
        EcgDataChunk actualEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();

        Assert.assertTrue(TAG + ":" + "Fail to get a new ECG data chunk",actualEcgDataChunk != null);

        byte[] expectedData = uncompressionEcgDataChunk(expectedEcgDataChunk);
        byte[] actualData   = uncompressionEcgDataChunk(actualEcgDataChunk);

        Assert.assertTrue(TAG + ":" + "Fail to convert BT raw data to ECG data chunk",compareEcgDataChunk(expectedEcgDataChunk,actualEcgDataChunk));

    }

    /**
     * give two BT data which timestamp is not sequence
     * for example, BTData 1000, next BTData 900
     * expect : log error, ignore it and create a new data chunk
     *
     * Test Case ID : UTMAID050003
     */
    public void testFeedChannelDataNonSequenceTimeStamp(){

        EcgChannelBluetoothData oneSecondData;

        int data_chunk_minutes = 4; // unit is minute

        int bt_data_minutes = 1;  // unit is second

        long start_stamp = System.currentTimeMillis()/1000;  // BT data , unit second

        long current_stamp = start_stamp;

        List<EcgChannelBluetoothData> btDataList = new ArrayList<EcgChannelBluetoothData>();

        Log.d(TAG,"Start time:"+current_stamp );

        for(int i = 0; i < data_chunk_minutes * 60 ; ++i){

            oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes,current_stamp);

            Log.d(TAG,""+oneSecondData.ch1.length);
            Log.d(TAG,""+oneSecondData.ch2.length);
            LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);

            //SystemClock.sleep(900);
            //current_stamp += getMillisecondStepBasedSampleRate();
            current_stamp += 1;

            btDataList.add(oneSecondData);
        }

        // should be empty
        EcgDataChunk emptyEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();
        Assert.assertTrue(emptyEcgDataChunk == null);

        // add extra one for get data chunk with old timestamp
        // should ignore and create a new chunk
        long old_stamp = current_stamp - 2;
        oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes,old_stamp);

        LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);
        //create expected ECG data chunk
        EcgDataChunk expectedEcgDataChunk = ECGDataCreator.EcgChannelBluetoothData2EcgDataChunk(mSessionID,btDataList);

        // get real ECG data chunk which will store at DB
        EcgDataChunk actualEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();

        Assert.assertTrue(TAG + ":" + "Fail to get a new ECG data chunk",actualEcgDataChunk != null);

        byte[] expectedData = uncompressionEcgDataChunk(expectedEcgDataChunk);
        byte[] actualData   = uncompressionEcgDataChunk(actualEcgDataChunk);

        Assert.assertTrue(TAG + ":" + "Fail to convert BT raw data to ECG data chunk",compareEcgDataChunk(expectedEcgDataChunk,actualEcgDataChunk));

    }

    /**
     * get five minutes and stop three minutes, and send another three minutes
     * expect get one data chunk, buffer another three minutes
     *
     * Test Case ID : UTMAID050004
     */
    public void testFeedChannelDataWithOneBigGap(){

        EcgChannelBluetoothData oneSecondData;

        int data_chunk_minutes = 5; // unit is minute

        int bt_data_minutes = 1;  // unit is second

        long start_stamp = System.currentTimeMillis()/1000;  // BT data , unit second

        long current_stamp = start_stamp;

        List<EcgChannelBluetoothData> btDataList = new ArrayList<EcgChannelBluetoothData>();

        Log.d(TAG, "Start time:" + current_stamp);

        for(int i = 0; i < data_chunk_minutes * 60 ; ++i){

            oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes,current_stamp);

            LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);

            //SystemClock.sleep(900);
            //current_stamp += getMillisecondStepBasedSampleRate();
            current_stamp += 1;

            btDataList.add(oneSecondData);
        }

        // should be empty
        EcgDataChunk emptyEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();
        Assert.assertTrue(emptyEcgDataChunk == null);

        //create expected ECG data chunk
        EcgDataChunk expectedFirstEcgDataChunk = ECGDataCreator.EcgChannelBluetoothData2EcgDataChunk(mSessionID, btDataList);

        // wait 2 minutes
        current_stamp += 2;
        long second_stamp = current_stamp;
        // create another 5 minute data
        btDataList.clear();

        // get real ECG data chunk which will store at DB
        EcgDataChunk actualFirstEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();

        for(int i = 0; i < data_chunk_minutes * 60 ; ++i){

            oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes,current_stamp);

            LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);

            //SystemClock.sleep(900);
            //current_stamp += getMillisecondStepBasedSampleRate();
            current_stamp += 1;

            btDataList.add(oneSecondData);

            actualFirstEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();

            Assert.assertFalse(TAG + ":" + "Fail to keep previous data chunk when receiving BT data:" + i, isNewEcgDataChunk(second_stamp, actualFirstEcgDataChunk));

        }

        // compare first ecg chunk data
        Assert.assertTrue(TAG + ":" + "Fail to compare first ECG chunk data", compareEcgDataChunk(expectedFirstEcgDataChunk, actualFirstEcgDataChunk));

        //create expected ECG data chunk
        EcgDataChunk expectedSecondEcgDataChunk = ECGDataCreator.EcgChannelBluetoothData2EcgDataChunk(mSessionID,btDataList);

        // send extra second data to get the second five chunk data
        oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes,current_stamp);

        LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);

        // get real ECG data chunk which will store at DB
        EcgDataChunk actualSecondEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();

        Assert.assertTrue(TAG + ":" + "Fail to get the second ECG data chunk",actualSecondEcgDataChunk != null);

        Assert.assertTrue(TAG + ":" + "Fail to convert BT raw data to ECG data chunk",compareEcgDataChunk(expectedSecondEcgDataChunk,actualSecondEcgDataChunk));

    }

    /**
     * send continue five minutes BT raw data and receive continue chunk data
     *
     * Test Case ID : UTMAID050005
     */
    public void testFeedContinueChannelData(){

        EcgChannelBluetoothData oneSecondData;

        int total_test_time = 60;  //minutes

        int data_chunk_minutes = 5; // unit is minute

        int bt_data_minutes = 1;  // unit is second

        long start_stamp = System.currentTimeMillis()/1000;  // BT data , unit second

        long current_stamp = start_stamp;

        List<EcgChannelBluetoothData> btDataList = new ArrayList<EcgChannelBluetoothData>();



        long curr_chunk_start;

        EcgDataChunk actualPreviousEcgDataChunk;
        EcgDataChunk expectedEcgDataChunk = null;
        EcgDataChunk actualEcgDataChunk;
        EcgDataChunk expectedNextEcgDataChunk = null;

        // keep feeding continue BT data
        for(int k = 0; k < 60; ++k) {

            curr_chunk_start = current_stamp;

            Log.d(TAG, "Start time for new chunk:" + k + ":" + current_stamp);

            if(expectedNextEcgDataChunk != null){
                expectedEcgDataChunk = expectedNextEcgDataChunk;
            }

            for (int i = 0; i < data_chunk_minutes * 60; ++i) {

                oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes, current_stamp);

                LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);

                current_stamp += 1;

                btDataList.add(oneSecondData);

                // check creating new chunk or not.
                // expected not
                actualPreviousEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();

                Assert.assertFalse(TAG + ":" + "Fail to keep previous data chunk when receiving BT data:" + i, isNewEcgDataChunk(curr_chunk_start, actualPreviousEcgDataChunk));
            }

            //create expected ECG data chunk
            expectedNextEcgDataChunk = ECGDataCreator.EcgChannelBluetoothData2EcgDataChunk(mSessionID, btDataList);
            btDataList.clear(); // clear it for next chunk data

            //get real ECG data chunk which will store at DB
            actualEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();

            if(actualEcgDataChunk != null){

                Assert.assertTrue(TAG + ":" + "Fail to convert BT raw data to ECG data chunk",compareEcgDataChunk(expectedEcgDataChunk,actualEcgDataChunk));

            }
        }
        // still have one chunk at buffer
    }

    /**
     * send random BT data and check data chunk
     *
     * Test Case ID : UTMAID050006
     */
    public void testFeedRandomStopChannelData(){

        EcgChannelBluetoothData oneSecondData;

        int total_test_time = 60;  //minutes

        int data_chunk_minutes = 5; // unit is minute

        int bt_data_minutes = 1;  // unit is second

        long start_stamp = System.currentTimeMillis()/1000;  // BT data , unit second

        long current_stamp = start_stamp;

        List<EcgChannelBluetoothData> btDataList = new ArrayList<EcgChannelBluetoothData>();



        long curr_chunk_start;

        List<Long> actualChunkStartStampList = new ArrayList<Long>();
        List<Long> expectedChunkStartStampList = new ArrayList<Long>();
        List<Long> BTSecondStartStampList = new ArrayList<Long>();

        EcgDataChunk expectedEcgDataChunk = null;
        EcgDataChunk actualEcgDataChunk = null;
        EcgDataChunk actualPreviousEcgDataChunk = null;
        EcgDataChunk expectedNextEcgDataChunk = null;

        int random_stop_minutes = 0;
        int continue_minutes = 0;

        // keep feeding continue BT data

        for(int k = 0; k < 3; ++k) {

            curr_chunk_start = current_stamp;


            Log.d(TAG, "Start time for new chunk:" + k + ":" + current_stamp);

            continue_minutes = NumberUtils.nextInt(100);

            for (int i = 0; i < data_chunk_minutes * 60; ++i) {

                oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes, current_stamp);
                BTSecondStartStampList.add(current_stamp);

                LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);

                if(continue_minutes == 0){
                    random_stop_minutes = NumberUtils.nextInt(1000);
                    current_stamp += random_stop_minutes;
                    Log.d(TAG,"Add a extra stop :" + random_stop_minutes);
                    continue_minutes = NumberUtils.nextInt(100);
                }else{
                    current_stamp += 1;
                    --continue_minutes;
                }

                // check creating new chunk or not.
                // expected not
                actualEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();

                if(actualEcgDataChunk != null){
                    if(actualPreviousEcgDataChunk == null){
                        // first time
                        actualPreviousEcgDataChunk = actualEcgDataChunk;
                        actualChunkStartStampList.add(actualEcgDataChunk.getChunkStartDateUnixMs());
                    }
                    else{
                        if(actualEcgDataChunk != actualPreviousEcgDataChunk){
                            // create a new package
                            actualPreviousEcgDataChunk = actualEcgDataChunk;
                            actualChunkStartStampList.add(actualEcgDataChunk.getChunkStartDateUnixMs());
                        }
                    }
                }
            }
        }
        // still have one chunk at buffer
        expectedChunkStartStampList = detectStartChunkStamp(BTSecondStartStampList);
        //Log.d(TAG,"INIT CHUNK TIMESTAMP: + " + expectedChunkStartStampList.toString());
        ListUtility.CompareList(expectedChunkStartStampList,actualChunkStartStampList);
        Assert.assertArrayEquals(TAG + ":" + "Fail to compare random package start point",expectedChunkStartStampList.toArray(),actualChunkStartStampList.toArray());
    }

    /**
     * create 10 seconds continue BT data and stop one second , then send another 10 seconds
     * expect there is no gap
     *
     *
     * Test Case ID : UTMAID050007
     */
    public void testTwoSecondGap(){

        EcgChannelBluetoothData oneSecondData;

        int data_chunk_minutes = 5; // unit is minute

        int bt_data_minutes = 1;  // unit is second

        long start_stamp = System.currentTimeMillis()/1000;  // BT data , unit second

        long current_stamp = start_stamp;

        List<EcgChannelBluetoothData> btDataList = new ArrayList<EcgChannelBluetoothData>();

        Log.d(TAG,"Start time:"+current_stamp );

        EcgDataChunk actualEcgDataChunk = null;
        EcgDataChunk actualPreviousEcgDataChunk = null;
        EcgDataChunk expectedEcgDataChunk ;

        for(int i = 0; i < data_chunk_minutes * 60 ; ++i){

            oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes,current_stamp);

            LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);

            current_stamp += 2; // increase

            // check creating new chunk or not.
            // expected not
            actualEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();

            if(actualEcgDataChunk != null){
                if(actualPreviousEcgDataChunk == null){
                    // first time
                    actualPreviousEcgDataChunk = actualEcgDataChunk;
                    //create expected
                    expectedEcgDataChunk = ECGDataCreator.EcgChannelBluetoothData2EcgDataChunk(mSessionID,btDataList);
                    Assert.assertTrue(TAG + ":" + "Fail to convert BT raw data to ECG data chunk", compareEcgDataChunk(expectedEcgDataChunk, actualEcgDataChunk));
                    btDataList.clear();
                    Assert.assertTrue(TAG+":" + "Fail to start new chunk",canStartNewChunk(actualEcgDataChunk,oneSecondData));
                    btDataList.add(oneSecondData);
                }
                else{
                    if(actualEcgDataChunk != actualPreviousEcgDataChunk){
                        // create a new package
                        actualPreviousEcgDataChunk = actualEcgDataChunk;
                        //create expected
                        expectedEcgDataChunk = ECGDataCreator.EcgChannelBluetoothData2EcgDataChunk(mSessionID,btDataList);
                        Assert.assertTrue(TAG + ":" + "Fail to convert BT raw data to ECG data chunk", compareEcgDataChunk(expectedEcgDataChunk, actualEcgDataChunk));
                        btDataList.clear();
                        Assert.assertTrue(TAG + ":" + "Fail to start new chunk", canStartNewChunk(actualEcgDataChunk,oneSecondData));
                        btDataList.add(oneSecondData);
                    }else
                    {
                        btDataList.add(oneSecondData);
                    }
                }
            }
            else{
                //
                btDataList.add(oneSecondData);
            }
        }
    }

    /**
     * create 10 seconds continue BT data and stop one second , then send another 10 seconds
     * expect there is no gap
     *
     *
     * Test Case ID : UTMAID050007
     */
    public void testThreeSecondGap(){

        EcgChannelBluetoothData oneSecondData;

        int data_chunk_minutes = 5; // unit is minute

        int bt_data_minutes = 1;  // unit is second

        long start_stamp = System.currentTimeMillis()/1000;  // BT data , unit second

        long current_stamp = start_stamp;

        List<EcgChannelBluetoothData> btDataList = new ArrayList<EcgChannelBluetoothData>();

        Log.d(TAG,"Start time:"+current_stamp );

        EcgDataChunk actualEcgDataChunk = null;
        EcgDataChunk actualPreviousEcgDataChunk = null;
        EcgDataChunk expectedEcgDataChunk ;

        for(int i = 0; i < data_chunk_minutes * 60 ; ++i){

            oneSecondData = ECGDataCreator.createEcgChannelBluetoothData(bt_data_minutes,current_stamp);

            LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(oneSecondData, mContext, false);

            current_stamp += 3; // increase

            // check creating new chunk or not.
            // expected not
            actualEcgDataChunk = LiveEcgChunkInMemory.getLastChunkCreated();

            if(actualEcgDataChunk != null){
                if(actualPreviousEcgDataChunk == null){
                    // first time
                    actualPreviousEcgDataChunk = actualEcgDataChunk;
                    //create expected
                    expectedEcgDataChunk = ECGDataCreator.EcgChannelBluetoothData2EcgDataChunk(mSessionID,btDataList);
                    Assert.assertTrue(TAG + ":" + "Fail to convert BT raw data to ECG data chunk", compareEcgDataChunk(expectedEcgDataChunk, actualEcgDataChunk));
                    btDataList.clear();
                    btDataList.add(oneSecondData);
                }
                else{
                    if(actualEcgDataChunk != actualPreviousEcgDataChunk){
                        // create a new package
                        actualPreviousEcgDataChunk = actualEcgDataChunk;
                        //create expected
                        expectedEcgDataChunk = ECGDataCreator.EcgChannelBluetoothData2EcgDataChunk(mSessionID,btDataList);
                        Assert.assertTrue(TAG + ":" + "Fail to convert BT raw data to ECG data chunk", compareEcgDataChunk(expectedEcgDataChunk, actualEcgDataChunk));
                        btDataList.clear();
                        btDataList.add(oneSecondData);
                    }else
                    {
                        btDataList.add(oneSecondData);
                    }
                }
            }
            else{
                //
                btDataList.add(oneSecondData);
            }

        }
    }

    ///////////////////////////////////////////
    // support function
    ///////////////////////////////////////////

    /**
     * compare two ECGDataChunk
     * @param expectedChunk
     * @param actualChunk
     * @return
     */
    public static boolean compareEcgDataChunk(EcgDataChunk expectedChunk,EcgDataChunk actualChunk){
        boolean rc ;

        Assert.assertEquals(TAG + ":" + "Fail session id",expectedChunk.getMonitoringSessionId(),actualChunk.getMonitoringSessionId());

        Assert.assertEquals(TAG + ":" + "Fail start timestamp", expectedChunk.getChunkStartDateUnixMs(), actualChunk.getChunkStartDateUnixMs());

        Assert.assertArrayEquals(TAG + ":" + "Fail ECG data chunk", expectedChunk.getChunkDataChannelCompressed(), actualChunk.getChunkDataChannelCompressed());

        //Assert.assertEquals(expectedChunk.insertTimestamp,actualChunk.insertTimestamp);

        rc = true;

        return rc;
    }

    /**
     * retrieve one ECG chunk data from database
     * @param SessionID
     * @param ChunkTimeStamp
     * @return
     * @throws SQLDataException
     */
    public EcgDataChunk readEcgDataChunkFromDB(int SessionID,long ChunkTimeStamp) throws SQLDataException{

        assert(mEcgChunkDb != null);
        return mEcgChunkDb.retrieveCompressedEcg(SessionID, ChunkTimeStamp);
    }

    /**
     * read all records from DB based on session ID
     * @param SessionID
     * @param InputChunks
     * @return
     */
    public List<EcgDataChunk> readEcgDataChunksFromDB(int SessionID,List<EcgDataChunk> InputChunks){

        List<EcgDataChunk> chunks = new ArrayList<EcgDataChunk>();

        EcgDataChunk chunk;

        try{
            for(EcgDataChunk inputChunk : InputChunks){
                chunk = readEcgDataChunkFromDB(SessionID,inputChunk.getMonitoringSessionId());
                chunks.add(chunk);
            }
        }catch (SQLDataException e){
            Assert.assertTrue("Can not find record from DB",false);
        }

        return chunks;
    }

    /**
     * check last ecgdata is updated or not
     * @param StartTimeStamp
     * @param LastEcgDataChunk
     * @return
     */
    public static boolean isNewEcgDataChunk(long StartTimeStamp,EcgDataChunk LastEcgDataChunk){

        boolean rc;

        if(LastEcgDataChunk == null){
            rc = false;
        }else{
            if(LastEcgDataChunk.getChunkStartDateUnixMs() > StartTimeStamp ){
                rc = true;
            }
            else {
                rc = false;
            }
        }

        return rc;
    }

    public static int getMillisecondStepBasedSampleRate(){

        return (int)(1000/sample_rate);
    }

    public static byte[] uncompressionEcgDataChunk(EcgDataChunk DataChunk){
        byte[] storedBlob = DataChunk.getChunkDataChannelCompressed();


        byte[] recovered ;

        try{
            recovered = ZipDecompression.decompress(storedBlob);
        }catch (DataFormatException  | IOException ioe){
            recovered = null;
        }

        return recovered;
    }

    /**
     * detect chunk start stamp based on BT feed start time stamp
     * so far, if great 2 second, consider there is a gap and start a new chunk
     * @param BTStartStampList
     * @return
     */
    public static List<Long> detectStartChunkStamp(List<Long> BTStartStampList){

        List<Long> chunkStartStampList = new ArrayList<Long>();

        assert(BTStartStampList.size() >= 2);

        int listSize = BTStartStampList.size();

        int i = 0;

        chunkStartStampList.add(BTStartStampList.get(0));
        long curr_start_time = BTStartStampList.get(0);

        while (i < listSize - 1){
            if(BTStartStampList.get(i+1) - BTStartStampList.get(i) > default_chunk_gap_min_second){
                // found new chunk start stamp
                chunkStartStampList.add(BTStartStampList.get(i+1));
                ++i;
                curr_start_time = BTStartStampList.get(i);
            }
            else{
                if(BTStartStampList.get(i) - curr_start_time >= default_chunk_second_size )
                {
                    // found default chunk size start stamp
                    chunkStartStampList.add(BTStartStampList.get(i));
                    curr_start_time = BTStartStampList.get(i);
                }
                ++i;
            }

        }

        // delete last one
        int chunkListSize = chunkStartStampList.size();
        chunkStartStampList.remove(chunkListSize - 1);
        return chunkStartStampList;
    }

    private static boolean canStartNewChunk(EcgDataChunk CurrentChunk,EcgChannelBluetoothData NextOneSecondBTRawData){

        boolean rc ;
        if ((NextOneSecondBTRawData.timestamp - CurrentChunk.getChunkStartDateUnixMs() >= default_chunk_second_size )
                || ( NextOneSecondBTRawData.timestamp - CurrentChunk.getChunkStartDateUnixMs() >= default_chunk_gap_min_second)){
            rc = true;
        }else
            rc = false;

        return rc;
    }
}
