package com.spectocor.micor.mobileapp.http;

import android.content.Context;
import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.http.requests.EcgChunkHttpRequest;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientEvent;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientReportedEventsDbHelper;
import com.spectocor.micor.mobileapp.sessionactivation.api.PatientReportedEventsApi;
import com.spectocor.testutilities.ECGDataCreator;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.database.ecg.EcgChunkDbUtility;
import com.spectocor.testutilities.database.ecg.PatientReportedEventsDbUtility;
import com.spectocor.testutilities.utils.NumberUtils;
import com.spectocor.testutilities.utils.PatientTriggerEventCreator;
import com.spectocor.testutilities.utils.ThreadUtility;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 3/7/2016.
 *
 * test case for buffered Patient Trigger Events to USD API
 *
 * trigger event : turn off and turn on network connection
 *
 * reference class
 *
 * @link http.HttpBufferedRequestHandler
 *
 * QA  : ALGO-973
 * DEV : ALGO-890
 *
 * CHANGES HISTORY
 *************************************************************
 * Date             Author          Comments
 * 03/07/2016       Qian            create test case for buffered Patient Trigger Events to USD API
 *
 *************************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 03/07/2016       Qian            fail :
 *
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class BufferedPTETest {
    private static String TAG = "BufferedPTETest";

    //private UITestSimpleFramework mTestFramework;
    private static AMainActivity mActivity;
    private static Context mContext;
    private static CurrentSessionState mCurrentSessionState;

    private static PatientReportedEventsDbHelper mPatientReportedEventsDbHelper;
    private static PatientReportedEventsDbUtility mPatientReportedEventsDbUtility;


    private static int mSampleRate = ECGDataCreator.SAMPLE_RATE;
    private static long mDefaultSleepMillionSecond = TestEnvGlobalConst.APP_SLEEP_MILLION_SECOND;

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init();  // launch the activity and init static variables
        }
    };

    //////////////////////////////////////////////
    // test data
    //////////////////////////////////////////////
    private int mDefaultHeartBeatVal = TestEnvGlobalConst.NORMAL_HEART_BEAT_RATE;
    private int mDefaultDisplayTime = TestEnvGlobalConst.DEFAULT_RUN_TEST_TIME;

    private static int NEW_HEART_BEAT = 1;

    @Before
    public void before(){
        // waiting activity launch
        SystemClock.sleep(mDefaultSleepMillionSecond);

        Log.d(TAG, mCurrentSessionState.toString());

        Assert.assertTrue(TAG + ": Fail to start test because there is active session on mobile device", mCurrentSessionState.sessionActive);
    }

    @After
    public void after(){
        // waiting tester to check UI
        SystemClock.sleep(mDefaultSleepMillionSecond);
    }

    /**
     * sending buffer patient trigger event
     *
     * input network trigger event
     *
     * return successful response
     *
     * Test Case ID : UTMAID260001
     */
    @Test
    public void testPTEWithInsertDB(){


        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(mContext);

        HttpQueueManager queueManager = HttpQueueManager.getInstance();
        Log.d(TAG,sessionState.dbPathAndName);

        // session start

        // prepare test data
        long startTimeSecond = System.currentTimeMillis()/1000 + 24 * 60 * 60;  // tomorrow
        long elapsedSecond = 300;
        int sessionID = sessionState.sessionId; //1;
        long ListLen = 3;

        // delete unsend PTE
        mPatientReportedEventsDbUtility.deleteUnSendPatientEvent();
        mPatientReportedEventsDbUtility.dumpPatientEvent();
        // airplane mode turn on
        Log.d(TAG,"airplane mode turn on");
        ThreadUtility.applicationSleep(30000);
        List<Long> PTEStartTimestampList = insertPTEDBList(ListLen, startTimeSecond, sessionID);
        int pass_number;
        mPatientReportedEventsDbUtility.dumpPatientEvent();
        pass_number = checkBufferPTETransferTime(PTEStartTimestampList, sessionID, -1);
        // buffer test data
        Assert.assertTrue(TAG + ":Fail to pass buffer PTE:expected buffer PTE number:" + PTEStartTimestampList.size() + ":real buffer PTE number:" + pass_number, pass_number == PTEStartTimestampList.size());
        // check FIFO
        // airplane mode turn off
        // waiting for long time to send chunk
        Log.d(TAG, "airplane mode turn off");
        ThreadUtility.applicationSleep(30000);
        ThreadUtility.applicationSleep(ListLen * 30000);
        mPatientReportedEventsDbUtility.dumpPatientEvent();
        pass_number = checkBufferPTEventsFIFO(PTEStartTimestampList,sessionID);
        Assert.assertTrue(TAG + ":Fail to pass buffer PTE FIFO :expected FIFO chunk number:" + PTEStartTimestampList.size() + ":real FIFO chunk number:" + pass_number, pass_number == PTEStartTimestampList.size());
        // delete data
        deleteBufferPTEvents(PTEStartTimestampList,sessionID);
        ThreadUtility.applicationSleep(10000);
    }

    /**
     * sending buffer patient trigger event
     *
     * input network trigger event
     *
     * return successful response
     *
     * Test Case ID : UTMAID260002
     */
    @Test
    public void testPTEWithDirectlyRESTAPI(){


        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(mContext);

        Log.d(TAG,sessionState.dbPathAndName);

        // session start

        // prepare test data
        long startTimeSecond = System.currentTimeMillis()/1000 + 24 * 60 * 60;  // tomorrow
        long elapsedSecond = 300;
        int sessionID = sessionState.sessionId; //1;
        long ListLen = 3;

        // delete All PTE
        mPatientReportedEventsDbUtility.deleteAllPatientEvent();
        mPatientReportedEventsDbUtility.dumpPatientEvent();
        // airplane mode turn on
        Log.d(TAG,"airplane mode turn on");
        ThreadUtility.applicationSleep(30000);

        List<String> PTESymptomsList = sendPTEList(ListLen, startTimeSecond, sessionID);

        int pass_number;
        mPatientReportedEventsDbUtility.dumpPatientEvent();
        pass_number = checkBufferPTETransferTime(sessionID, -1);
        // buffer test data
        Assert.assertTrue(TAG + ":Fail to pass buffer PTE:expected buffer PTE number:" + PTESymptomsList.size() + ":real buffer PTE number:" + pass_number, pass_number == PTESymptomsList.size());
        // check FIFO
        // airplane mode turn off
        // waiting for long time to send chunk
        Log.d(TAG, "airplane mode turn off");
        ThreadUtility.applicationSleep(30000);
        ThreadUtility.applicationSleep(ListLen * 30000);
        mPatientReportedEventsDbUtility.dumpPatientEvent();
        boolean rc = checkPTEventsFIFO(sessionID);
        Assert.assertTrue(TAG + ":Fail to pass buffer PTE FIFO." , rc);
        // delete data
        //deleteBufferPTEvents(PTEStartTimestampList,sessionID);
        ThreadUtility.applicationSleep(10000);
    }


    ////////////////////////////////////////////////
    // support function
    ////////////////////////////////////////////////

    /**
     * init class member variables
     */
    private void init(){

        mActivity = mActivityRule.getActivity();
        mContext  = mActivity.getApplicationContext();
        mCurrentSessionState = SessionStateAccess.getCurrentSessionState(mContext);
        mPatientReportedEventsDbHelper = PatientReportedEventsDbHelper.getInstance();
        mPatientReportedEventsDbUtility = PatientReportedEventsDbUtility.getInstance(mContext);


        assert(mActivity != null);
        assert(mContext != null);
        assert(mPatientReportedEventsDbHelper != null);
        assert(mPatientReportedEventsDbUtility != null);

        Log.d(TAG, "init");
    }

    private void insertPTEDB(final PatientEvent patientEvent){

        // insert DB

        mPatientReportedEventsDbHelper.insertPatientEvent(patientEvent);

        // end: transfer- PTE
    }

    /**
     * send patient trigger events with symptoms
     * @param symptoms
     */
    private void sendingPTEWithBuffer(final String symptoms){

        // call REST API
        PatientReportedEventsApi.getInstance().reportWithSymptoms(symptoms);
        // end: transfer- PTE
    }

    /**
     * send number events which start time second is startTimeSecond.
     *
     * @param numberEvents
     * @param startTimeSecond
     * @param sessionID
     * @return
     */
    private List<String> sendPTEList(final long numberEvents,final long startTimeSecond,final int sessionID){

        List<String> PTESymptomList = null;

        String symptom;

        long currStartTimeSecond = startTimeSecond;

        if(numberEvents > 0 ){

            PTESymptomList = new ArrayList<>();

            for(int i = 0; i < numberEvents; ++i){

                symptom = PatientTriggerEventCreator.createRandomSymptom();

                sendingPTEWithBuffer(symptom);

                PTESymptomList.add(symptom);

                ThreadUtility.applicationSleep(1000 * NumberUtils.mockRandomNumber(3));

            }
        }

        return PTESymptomList;
    }

    /**
     * send number events which start time second is startTimeSecond.
     *
     * @param numberEvents
     * @param startTimeSecond
     * @param sessionID
     * @return
     */
    private List<Long> insertPTEDBList(final long numberEvents,final long startTimeSecond,final int sessionID){

        List<Long> startTimeStampList = null;

        PatientEvent patientEvent;

        long currStartTimeSecond = startTimeSecond;

        if(numberEvents > 0 ){

            startTimeStampList = new ArrayList<>();

            for(int i = 0; i < numberEvents; ++i){

                patientEvent = PatientTriggerEventCreator.createPatientEvent(sessionID);

                insertPTEDB(patientEvent);

                startTimeStampList.add(new Long(patientEvent.getInsertTimestamp()));

                ThreadUtility.applicationSleep(1000 * NumberUtils.mockRandomNumber(3));

            }
        }

        return startTimeStampList;
    }

    /**
     * check buffer PTE or not
     * @param startTimeStampList
     * @param sessionID
     * @param expectedValue
     * @return
     */
    private int checkBufferPTETransferTime(final List<Long> startTimeStampList,final int sessionID,final long expectedValue){

        int pass_number=0;

        PatientEvent patientEvent;

            for(int i = 0; i < startTimeStampList.size();++i){

                patientEvent = mPatientReportedEventsDbUtility.retrievePatientEvent(sessionID,startTimeStampList.get(i).longValue());

                if(patientEvent.getTransmittedTimestamp() == expectedValue){
                    pass_number += 1;
                }

            }


        return pass_number;
    }

    private int checkBufferPTETransferTime(final int sessionID,final long expectedValue){

        int pass_number=0;

        PatientEvent patientEvent;

        List<PatientEvent> patientEventList = mPatientReportedEventsDbUtility.retrievePatientEventsByTransferTime(sessionID,expectedValue);

        if(patientEventList != null)
            pass_number = patientEventList.size();
        else
            pass_number = 0;

        return pass_number;
    }

    /**
     * check buffer patient trigger event FIFO
     * @param startTimeStampList
     * @param sessionID
     * @return
     */
    private int checkBufferPTEventsFIFO(final List<Long> startTimeStampList,final int sessionID){

        int pass_number=0;

        PatientEvent patientEvent;

        long previousPTEStartTimeStamp,previousPTETransferTimeStamp;
        long currPTEStartTimeStamp,currPTETransferTimeStamp;

        previousPTEStartTimeStamp = 0;
        previousPTETransferTimeStamp = 1;


            for(int i = 0; i < startTimeStampList.size();++i){

                patientEvent = mPatientReportedEventsDbUtility.retrievePatientEvent(sessionID, startTimeStampList.get(i).longValue());

                Assert.assertTrue(TAG+":Fail to find PTE:"+ startTimeStampList.get(i).longValue(), patientEvent != null);

                currPTEStartTimeStamp = patientEvent.getInsertTimestamp();

                currPTETransferTimeStamp = patientEvent.getTransmittedTimestamp();

//                if(currChunkTransferTimeStamp != -1
//                        && currChunkStartTimeStamp > previousChunkStartTimeStamp
//                        && currChunkTransferTimeStamp  > currChunkStartTimeStamp
//                        && currChunkTransferTimeStamp > previousChunkTransferTimeStamp
//                        ){
                Log.d(TAG,"current PTE start timestamp:"+currPTEStartTimeStamp);
                Log.d(TAG,"current PTE transfer timestamp:"+currPTETransferTimeStamp);

                Log.d(TAG,"previous chunk start timestamp:"+previousPTEStartTimeStamp);
                Log.d(TAG,"previous chunk transfer timestamp:"+previousPTETransferTimeStamp);

                if(currPTETransferTimeStamp != -1
                        && currPTEStartTimeStamp > previousPTEStartTimeStamp
                        && currPTETransferTimeStamp > previousPTETransferTimeStamp
                        ){
                    pass_number += 1;
                }

                previousPTEStartTimeStamp = currPTEStartTimeStamp;
                previousPTETransferTimeStamp = currPTETransferTimeStamp;

            }



        return pass_number;
    }

    private boolean checkPTEventsFIFO(final int sessionID){

        boolean rc= true;

        PatientEvent patientEvent;

        long previousPTEStartTimeStamp,previousPTETransferTimeStamp;
        long currPTEStartTimeStamp,currPTETransferTimeStamp;

        previousPTEStartTimeStamp = 0;
        previousPTETransferTimeStamp = 1;

        List<PatientEvent> patientEvents = mPatientReportedEventsDbUtility.retrievePatientEvents(sessionID);

        if(patientEvents == null){
            rc = true;
        }else{
            for(int i = 0; i < patientEvents.size();++i){

                patientEvent = patientEvents.get(i);

                if(patientEvent.getTransmittedTimestamp() != -1){

                    currPTEStartTimeStamp = patientEvent.getInsertTimestamp();

                    currPTETransferTimeStamp = patientEvent.getTransmittedTimestamp();

//                if(currChunkTransferTimeStamp != -1
//                        && currChunkStartTimeStamp > previousChunkStartTimeStamp
//                        && currChunkTransferTimeStamp  > currChunkStartTimeStamp
//                        && currChunkTransferTimeStamp > previousChunkTransferTimeStamp
//                        ){
                    Log.d(TAG,"current PTE start timestamp:"+currPTEStartTimeStamp);
                    Log.d(TAG,"current PTE transfer timestamp:"+currPTETransferTimeStamp);

                    Log.d(TAG,"previous chunk start timestamp:"+previousPTEStartTimeStamp);
                    Log.d(TAG,"previous chunk transfer timestamp:"+previousPTETransferTimeStamp);

                    if(currPTETransferTimeStamp != -1
                            && currPTEStartTimeStamp > previousPTEStartTimeStamp
                            && currPTETransferTimeStamp > previousPTETransferTimeStamp
                            ){
                        ;
                    }else
                    {
                        rc = false;
                        break;
                    }

                    previousPTEStartTimeStamp = currPTEStartTimeStamp;
                    previousPTETransferTimeStamp = currPTETransferTimeStamp;
                }

            }
        }

        return rc;
    }


    private int deleteBufferPTEvents(final List<Long> startTimeStampList,final int sessionID){

        int pass_number=0;

        PatientEvent patientEvent;

        boolean rc;

        for(int i = 0; i < startTimeStampList.size();++i) {

            rc = mPatientReportedEventsDbUtility.deletePatientEvent(sessionID, startTimeStampList.get(i).longValue());

            if (rc) {
                pass_number += 1;
            }

        }

        return pass_number;
    }
}
