package com.spectocor.micor.mobileapp.http;

import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.amain.AMainApplication;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Test case for ECG Chunk Network Call.
 *
 * Created by Rafay Ali on 1/27/2016.
 */
public class EcgChunkNetworkCallTest extends AndroidTestCase {

    public void testEcgChunkRequestCall(){
        try {
            RequestBody requestBody = new MultipartBody.Builder()
                    .addFormDataPart("sessionId", "15855")
                    .addFormDataPart("file", "data.comp",
                            RequestBody.create(
                                    MediaType.parse("application/octet-stream"),
                                    hexStringToByteArray("e04fd020ea3a6910a2d808002b30309d")))
                    .addFormDataPart("chunkStartTimestamp", "" + System.currentTimeMillis())
                    .setType(MultipartBody.FORM)
                    .build();

            Request request = new Request.Builder()
                    .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_ECG_CHUNK)
                    .post(requestBody)
                    .build();

            OkHttpClient httpClient = HttpManager.getInstance().getHttpClient();
            Response response = httpClient.newCall(request).execute();
            JSONObject object = new JSONObject(response.body().string());
            Assert.assertEquals("success", object.getString("message").toLowerCase());
        }
        catch (IOException | JSONException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    public byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

}


