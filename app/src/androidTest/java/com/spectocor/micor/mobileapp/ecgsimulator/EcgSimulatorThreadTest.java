package com.spectocor.micor.mobileapp.ecgsimulator;

import android.test.AndroidTestCase;

import com.spectocor.testutilities.utils.ThreadUtility;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Created by qwang on 3/1/2016.
 */
public class EcgSimulatorThreadTest extends AndroidTestCase {

    @Test
    public void testStopEcgSimulatorThread(){

        EcgSimulatorThread ecgSimulatorThread = EcgSimulatorThread.getInstance(getContext());

        if(ecgSimulatorThread != null && ecgSimulatorThread.isAlive())
            ecgSimulatorThread.endSimulation();

        ThreadUtility.applicationSleep(1000);

        Assert.assertTrue("Fail to pass StopEcgSimulatorThread",ecgSimulatorThread == null || !ecgSimulatorThread.isAlive());
    }

    @Test
    public void testStartEcgSimulatorThread(){

        EcgSimulatorThread ecgSimulatorThread = EcgSimulatorThread.getInstance(getContext());

        if(ecgSimulatorThread == null && !ecgSimulatorThread.isAlive())
            ecgSimulatorThread.start();

        ThreadUtility.applicationSleep(1000);

        Assert.assertTrue("Fail to pass StartEcgSimulatorThread",ecgSimulatorThread != null && ecgSimulatorThread.isAlive());
    }
}
