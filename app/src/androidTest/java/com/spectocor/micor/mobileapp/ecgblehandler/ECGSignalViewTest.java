package com.spectocor.micor.mobileapp.ecgblehandler;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.test.annotation.UiThreadTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.content.LocalBroadcastManager;
import android.test.suitebuilder.annotation.LargeTest;
import android.webkit.WebView;

import com.google.gson.Gson;
import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainService;
import com.spectocor.micor.mobileapp.ecgadapter.HeartRateHandler;
import com.spectocor.micor.mobileapp.uitestframework.UITestSimpleFramework;
import com.spectocor.testutilities.AssetUtility;
import com.spectocor.testutilities.ECGDataCreator;
import com.spectocor.testutilities.IntentUtility;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.ecg_data.ECGBTRawData;
import com.spectocor.testutilities.utils.ArrayUtility;
import com.spectocor.testutilities.utils.ThreadUtility;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static android.support.test.espresso.web.sugar.Web.onWebView;

import android.util.Log;

/**
 * Created by qwang on 2/11/2016.
 * <p/>
 * test case for ECG signal view
 *
 * CHANGES HISTORY
 ***********************************************************
 * Date             Author          Comments
 * 02/11/2016       Qian            create test case
 * 02/18/2016       Qian            add head
 *
 ***********************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 02/18/2016       Qian
 *
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class ECGSignalViewTest {

    //////////////////////////////////////////////////////////////
    // Simple Test Framework
    //////////////////////////////////////////////////////////////
    private static String TAG = "ECGSignalViewTest";

    //private UITestSimpleFramework mTestFramework;
    private static AMainActivity mActivity;
    private static Context mContext;
    private static boolean mShowFlag;

    private static int mSampleRate = TestEnvGlobalConst.SAMPLE_RATE;
    private static long mDefaultSleepMillionSecond = TestEnvGlobalConst.APP_SLEEP_MILLION_SECOND;

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init(); // launch the activity and get test framework
        }
    };

    @Before
    public void before() {
        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond); // waiting for activity launch

        AMainService.sendToSignalView = mShowFlag;

        mActivity.callWebviewJsFunction("displayAdminEcgSignalPage()");  // jump to signal view
    }

    @After
    public void after() {
        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond); // waiting for activity close
    }


    //////////////////////////////////////////////
    // test case
    //////////////////////////////////////////////

    /**
     * test main activity receiving random signal data to display it
     * <p/>
     * Test Case ID : UTMAID130001
     */
    @Test
    public void testDisplayRandomSignal() throws InterruptedException {

        int displayTime = 20;


        short[] channel1;
        short[] channel2;

        for (int i = 0; i < displayTime; ++i) {
            // prepare test data

            long start = System.currentTimeMillis();

            channel1 = ECGDataCreator.createECGChannelData(mSampleRate);
            channel2 = ECGDataCreator.createECGChannelData(mSampleRate);

            long end = System.currentTimeMillis();

            Thread.sleep(1000 - (end - start));


            // render
            showECGSignal(channel1, channel2);
        }

        // check result

    }

    /**
     * read BT raw data and test main activity receiving ECG data to display it
     * <p/>
     * Test Case ID : UTMAID130002
     */
    @Test
    public void testDisplayECGSignalViewWithDataFile() throws InterruptedException {

        String raw_data_path = "ui_test_data/signal_view/normal_ecg_signal_001.txt";


        // prepare test data
        short[] channel1 = new short[mSampleRate];
        short[] channel2 = new short[mSampleRate];

        // read data file
        short[] twoChannels = AssetUtility.readBTRAW(mContext, raw_data_path);

        // split
        if (twoChannels.length % 2 == 0) {

            // render
            int channel1_index = 0;
            int channel2_index = twoChannels.length / 2;

            while (channel2_index < twoChannels.length) {

                long start = System.currentTimeMillis();

                System.arraycopy(twoChannels, channel1_index, channel1, 0, mSampleRate);
                System.arraycopy(twoChannels, channel2_index, channel2, 0, mSampleRate);

                channel1_index += mSampleRate;
                channel2_index += mSampleRate;

                long end = System.currentTimeMillis();

                Thread.sleep(1000 - (end - start));

                showECGSignal(channel1, channel2);
            }

            // check result

        } else {
            Assert.fail(TAG + ": Input data file is not correct format and size");
        }

    }

    /**
     * read BT raw data and test main activity receiving ECG data to display it
     * <p/>
     * Test Case ID : UTMAID130003
     */
    @Test
    public void testDisplayECGSignalViewWith30bpm() throws InterruptedException {

        String raw_data_path = "ui_test_data/signal_view/30bpm.txt";
        //String raw_data_path = "ui_test_data/signal_view/baseline.txt";
        //String raw_data_path = "ui_test_data/signal_view/noise.txt";

        // prepare test data
        ArrayList<short[]> channel1DataList = new ArrayList<>();
        ArrayList<short[]> channel2DataList = new ArrayList<>();

        AssetUtility.loadSplitTwoChannelData(mContext, raw_data_path, channel1DataList, channel2DataList); // read data file

        // render
        showContinuousSignal(channel1DataList, channel2DataList);

        // check result

    }

    /**
     * read BT raw data and test main activity receiving ECG data to display it
     * <p/>
     * Test Case ID : UTMAID130004
     */
    @Test
    public void testDisplayECGSignalViewWithBaseLine() throws InterruptedException {

        String raw_data_path = "ui_test_data/signal_view/baseline.txt";

        // prepare test data
        ArrayList<short[]> channel1DataList = new ArrayList<>();
        ArrayList<short[]> channel2DataList = new ArrayList<>();

        AssetUtility.loadSplitTwoChannelData(mContext, raw_data_path, channel1DataList, channel2DataList); // read data file

        // render
        showContinuousSignal(channel1DataList,channel2DataList);
        // check result
    }

    /**
     * read BT raw data and test main activity receiving ECG data to display it
     * <p/>
     * Test Case ID : UTMAID130005
     */
    @Test
    public void testDisplayECGSignalViewWithNoise() throws InterruptedException {

        //String raw_data_path = "ui_test_data/signal_view/baseline.txt";
        String raw_data_path = "ui_test_data/signal_view/noise.txt";

        // prepare test data
        ArrayList<short[]> channel1DataList = new ArrayList<>();
        ArrayList<short[]> channel2DataList = new ArrayList<>();

        AssetUtility.loadSplitTwoChannelData(mContext, raw_data_path, channel1DataList, channel2DataList); // read data file

        // render
        showContinuousSignal(channel1DataList,channel2DataList);
        // check result
    }

    /**
     * read BT raw data and test main activity receiving ECG data to display it
     * <p/>
     * Test Case ID : UTMAID130006
     */
    @Test
    public void testDisplayECGSignalViewWith80bpm() throws InterruptedException {

        String raw_data_path = "ui_test_data/signal_view/80bpm.txt";

        // prepare test data
        ArrayList<short[]> channel1DataList = new ArrayList<>();
        ArrayList<short[]> channel2DataList = new ArrayList<>();

        AssetUtility.loadSplitTwoChannelData(mContext, raw_data_path, channel1DataList, channel2DataList); // read data file

        // render
        showContinuousSignal(channel1DataList,channel2DataList);

        // check result

    }

    /**
     * read BT raw data and test main activity receiving ECG data to display it
     * <p/>
     * Test Case ID : UTMAID130007
     */
    @Test
    public void testDisplayECGSignalViewWith300bpm() throws InterruptedException {

        String raw_data_path = "ui_test_data/signal_view/300bpm.txt";

        // prepare test data
        ArrayList<short[]> channel1DataList = new ArrayList<>();
        ArrayList<short[]> channel2DataList = new ArrayList<>();

        AssetUtility.loadSplitTwoChannelData(mContext, raw_data_path, channel1DataList, channel2DataList); // read data file

        // render
        showContinuousSignal(channel1DataList,channel2DataList);

        // check result

    }
    //////////////////////////////////////////////
    // support function
    //////////////////////////////////////////////

    /**
     * init class member variables
     */
    private void init() {

        mActivity = mActivityRule.getActivity();
        mContext = mActivity.getApplicationContext();

        assert (mActivity != null);
        assert (mContext != null);

        mShowFlag = true;

        Log.d(TAG, "init");
    }


    /**
     * create an intent to tell UI updating ECG signal waveform
     *
     * @param SampledCh1 , channel one data
     * @param SampledCh2 , channel two data
     */
    private void showECGSignal(short[] SampledCh1, short[] SampledCh2) {


        ArrayList<Integer> filtCh1 = HeartRateHandler.getInstance(mContext).applyFilter(SampledCh1, 1);
        ArrayList<Integer> filtCh2 = HeartRateHandler.getInstance(mContext).applyFilter(SampledCh2, 2);

        // JOINING BOTH BROADCASTS AND SENDING AS ONE
        Intent intent = IntentUtility.createRenderECGWaveformIntent(filtCh1, filtCh2);

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

    /**
     * show continuous signal waveform, each chunk data is one second sampling data
     *
     * @param continuousSignalChannel1
     * @param continuousSignalChannel2
     */
    private void showContinuousSignal(ArrayList<short[]> continuousSignalChannel1,ArrayList<short[]> continuousSignalChannel2 ){

        Assert.assertTrue(TAG+":Fail data format",continuousSignalChannel1.size() > 0);
        Assert.assertTrue(TAG+":Fail data format",continuousSignalChannel2.size() > 0);
        Assert.assertTrue(TAG+":Fail data format",continuousSignalChannel1.size() == continuousSignalChannel2.size());

        long start; // start time
        long end;   // end time
        long wait;  // wait time

        short[] channel1,channel2;

        for (int i = 0; i < continuousSignalChannel1.size(); ++i) {

            start = System.currentTimeMillis();

            channel1 = continuousSignalChannel1.get(i);
            channel2 = continuousSignalChannel2.get(i);

            end = System.currentTimeMillis();

            wait = 1000 - (end - start) ;

            if(wait > 0)
                ThreadUtility.applicationSleep(wait);

            //Log.d(TAG + "DATA", tchannel1.toString());

            showECGSignal(channel1, channel2);
        }
    }

}
