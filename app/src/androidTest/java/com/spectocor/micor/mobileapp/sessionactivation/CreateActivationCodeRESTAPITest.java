package com.spectocor.micor.mobileapp.sessionactivation;

import android.content.Context;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.session.api.CreateActivationCodeRESTAPI;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import okhttp3.Response;

import android.util.Log;

/**
 * Created by qwang on 3/11/2016.
 */
public class CreateActivationCodeRESTAPITest extends AndroidTestCase {

    private static String TAG = "CreateActivationCodeRESTAPITest";

    private static Context mContext;
    private static CurrentSessionState mSessionState;

    private static String mDefaultSessionID = "100005824";

    /**
     * Creates required objects for testing
     *
     * @throws Exception if it couldn't create files
     */
    @Before
    public void setUp() throws Exception {

        init();

    }


    ///////////////////////////////////////////////////////
    // test case
    ///////////////////////////////////////////////////////
    public void testCreateOneActivationCode() {

        // test data
        String activationCode = "700";
        String patientFirstName = "700Qian";
        String patientLastName = "Wang700";
        boolean holterPlus = false;
        String facilityID = "8";
        String enrollmentDate = "2016-03-18";
        JSONObject object = null;


        try {
            Response response = CreateActivationCodeRESTAPI.SendingCreateActivationCodeRequestCall(activationCode, patientFirstName, patientLastName, holterPlus,facilityID,enrollmentDate);

            //object = new JSONObject(response.body().string());

            //Log.d(TAG, ObjectUtils.toJson(object));

            Assert.assertTrue(TAG + ":fail to create activation code", response.code() == 200);

        } catch (IOException e) {

            e.printStackTrace();
            Assert.fail("fail to create activation code");
        }

    }

    /////////////////////////////////////////////////////
    // support function
    /////////////////////////////////////////////////////

    /**
     * init
     */
    private void init() {

        mContext = getContext();

        mSessionState = SessionStateAccess.getCurrentSessionState(mContext);

        assert (mContext != null);
        assert (mSessionState != null);
    }
}
