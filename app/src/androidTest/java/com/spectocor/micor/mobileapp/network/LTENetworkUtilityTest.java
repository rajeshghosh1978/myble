package com.spectocor.micor.mobileapp.network;

import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.http.common.NetworkUtil;
import com.spectocor.testutilities.netwrok.LTENetworkUtility;

import junit.framework.Assert;

/**
 * Created by qwang on 3/1/2016.
 *
 * test case for turning off LTE network
 */
public class LTENetworkUtilityTest extends AndroidTestCase {

    private static String TAG = LTENetworkUtilityTest.class.getSimpleName();

    public void testSetLTENetworkOff(){

        int curr_network_connection = NetworkUtil.getConnectivityStatus(getContext());

        if(curr_network_connection == NetworkUtil.TYPE_MOBILE ){
            LTENetworkUtility.setMobileDataState(getContext(),false);
            //LTENetworkUtility.setMobileDataEnabled(getContext(),false);
        }

        curr_network_connection = NetworkUtil.getConnectivityStatus(getContext());

        Assert.assertTrue(TAG + ":Fail to pass closing LTE network",curr_network_connection == NetworkUtil.TYPE_NOT_CONNECTED);
    }
}
