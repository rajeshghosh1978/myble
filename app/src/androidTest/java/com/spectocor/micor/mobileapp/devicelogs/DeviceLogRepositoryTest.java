package com.spectocor.micor.mobileapp.devicelogs;

import android.content.Context;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;
import com.spectocor.micor.mobileapp.common.exceptions.DataAccessException;
import com.spectocor.testutilities.TestUtils;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Test class for DeviceLogRepositoryTest
 */
//@RunWith(AndroidJUnit4.class)
//public class DeviceLogRepositoryTest extends InstrumentationTestCase {    // extends AndroidTestCase

@SuppressWarnings("WeakerAccess")
public class DeviceLogRepositoryTest extends AndroidTestCase {

    static final int mMaxListLen = 100;  // length of list
    static final int mFailLogSessionID = 1000;
    DeviceLogRepository repository;
    List<DeviceLog> mTestLogs; // list of device log

    /*
    * compare log list
     */
    public static void compareDeviceLogs(List<DeviceLog> ExpectedLogList, List<DeviceLog> RealLogList) {

        int expected_list_len = ExpectedLogList.size();
        int real_list_len = RealLogList.size();

        //boolean rc;

        if (expected_list_len != real_list_len) {
            //rc = false;
            assertTrue(false);
        } else {
            //rc = true;
            for (int i = 0; i < expected_list_len; ++i) {
                compareDeviceLog(ExpectedLogList.get(i), RealLogList.get(i));
            }
        }

        //Assert.assertTrue("Fail at comparing Device Logs.", rc);

        //return rc;
    }

    /*
    * compare log
     */
    public static void compareDeviceLog(DeviceLog ExpectedLog, DeviceLog RealLog) {

        //boolean rc ;

        assertEquals(ExpectedLog.getMonitoringSessionId(), RealLog.getMonitoringSessionId());
        assertEquals(ExpectedLog.getLogDateUnixSec(), RealLog.getLogDateUnixSec());
        assertEquals(ExpectedLog.getDeviceLogInfoTypeId(), RealLog.getDeviceLogInfoTypeId());
        assertEquals(ExpectedLog.getValueNumber(), RealLog.getValueNumber());
        assertEquals(ExpectedLog.getValueString(), RealLog.getValueString());

        System.out.println("pass " + ExpectedLog.getMonitoringSessionId());

        //rc = true;

        //return rc;
    }

    /**
     * setting up test environment
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        super.setUp();

        Context context = getContext();
        context.deleteDatabase(LogEventsDatabaseHelper.DatabaseName);

        LogEventsDatabaseHelper databaseHelper = new LogEventsDatabaseHelper(context);
        repository = new DeviceLogRepository(databaseHelper);

        mTestLogs = new ArrayList<>();
    }

//    @Test
//    public void testUpdate() throws Exception {
//
//    }

    /*
    * insert single device log item to repository
    * Test Case ID : UTMAID000006
     */
    public void testSingleSuccessInsert() throws Exception {

        DeviceLog deviceLog = new DeviceLog(EnumDeviceLogInfoType.ReservedUndefined, 100, "Test String");
        long m = repository.insert(deviceLog);
        Assert.assertTrue(m != -1);
    }

    /*
    * delete single device log item from repository
    * Test Case ID : UTMAID000007
     */
    public void testSingleSuccessDelete() throws Exception {
        DeviceLog deviceLog = new DeviceLog(EnumDeviceLogInfoType.ReservedUndefined, 100, "Test String");
        long m = repository.insert(deviceLog);
        repository.delete(deviceLog.getDatabaseRowId());
        Assert.assertTrue(m != -1);
    }

    /*
    * get all device log item from repository
    * Test Case ID : UTMAID000008
     */
    public void testGetAllLogs() throws Exception {
        DeviceLog deviceLog = new DeviceLog(EnumDeviceLogInfoType.ReservedUndefined, 100, "Test String");
        long m = repository.insert(deviceLog);
        Assert.assertTrue(m != -1);
        List<DeviceLog> list = repository.getAll(1000);
        Assert.assertTrue(list.size() >= m);
    }

    /*
    * get a device log item from repository with Id
    * Test Case ID : UTMAID000009
     */
    public void testGetById() throws Exception {

        DeviceLog deviceLog = new DeviceLog(EnumDeviceLogInfoType.ReservedUndefined, 100, "Test String");
        long m1 = repository.insert(new DeviceLog(EnumDeviceLogInfoType.ReservedUndefined, 100, "Test String"));
        long m2 = repository.insert(deviceLog);
        long m3 = repository.insert(new DeviceLog(EnumDeviceLogInfoType.ReservedUndefined, 100, "Test String"));

        Assert.assertTrue(m1 != -1);
        Assert.assertTrue(m2 != -1);
        Assert.assertTrue(m3 != -1);

        DeviceLog deviceLog1 = repository.getById(m2);
        TestUtils.AssertEqualsJson(deviceLog1, deviceLog);
    }

    /*
    * random create device log list
     */
    void createTestDeviceLogList() {

        DeviceLog log;
        Random rand = new Random();
        int log_id;
        int log_num_val;

        for (int index = 1; index < mMaxListLen; ++index) {
            log_num_val = rand.nextInt(mMaxListLen) + 1;
            //log_id = rand.nextInt(mMaxListLen) + 1;
            log = new DeviceLog(EnumDeviceLogInfoType.ReservedUndefined, log_num_val, "Test String");
            mTestLogs.add(log);
        }
    }

    /*
    * insert batch log items
     */
    public int batchInsertLog(List<DeviceLog> DeviceLogList) throws Exception {
        int log_len = DeviceLogList.size();
        long db_row_id;
        if (log_len > 0) {
            for (int i = 0; i < log_len; ++i) {
                db_row_id = repository.insert(DeviceLogList.get(i));
                Assert.assertTrue(db_row_id != -1);
            }
        }

        return log_len;
    }

    /*
    * get all device log
     */
    public List<DeviceLog> GetAllDeviceLog() throws Exception {
        List<DeviceLog> deviceLogs;
        deviceLogs = repository.getAll(mMaxListLen);
        return deviceLogs;
    }

    /*
    * test batch insert device log
    * Test Case ID : UTMAID000010
    */
    public void testDeviceLogRepositoryInsert() throws Exception {
        createTestDeviceLogList();
        int expected_log_num = batchInsertLog(mTestLogs);
        List<DeviceLog> realLog = GetAllDeviceLog();
        compareDeviceLogs(mTestLogs, realLog);
    }

    /*
    * test get log item which is not at repository
    * Test Case ID : UTMAID000011
     */
    public void testDeviceLogRepositoryGetNoFound() {
        long not_log_id = mFailLogSessionID;
        DeviceLog log;

        try {
            log = repository.getById(not_log_id);
            Assert.assertEquals(null, log);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Fail at getting device log object with wrong id:" + not_log_id);
        }
    }

    /*
    * test multiple delete device logs
    * Test Case ID : UTMAID000012
    */
    public void testDeviceLogRepositoryDelete() throws Exception {
        createTestDeviceLogList();
        int expected_log_num = batchInsertLog(mTestLogs);
        List<DeviceLog> realLogs = GetAllDeviceLog();
        DeviceLog log;
        for (int i = 0; i < realLogs.size(); ++i) {
            log = realLogs.get(i);
            Assert.assertNotNull(log);
            repository.delete(log.getDatabaseRowId());
        }
        realLogs = GetAllDeviceLog();
        Assert.assertEquals(0, realLogs.size());
    }

    /*
    * test delete device log which isn't available at repository
    * Test Case ID : UTMAID000013
     */
    public void testDeviceLogRepositoryDeleteNoFound() {
        long not_log_id = mFailLogSessionID;

        try {
            repository.delete(not_log_id);
            Assert.fail("Fail at delete device log object with wrong id:" + not_log_id);
        } catch (DataAccessException e) {
            // it should return an exception, so nothing to check
        }
    }

    public void checkLogFile(EnumDeviceLogInfoType logInfoType, int StartTime, int EndTime) throws Exception {

        DeviceLog log;

        int check_start_time, check_end_time;

        check_start_time = StartTime;
        check_end_time = EndTime;

        List<DeviceLog> logs;

        createTestDeviceLogList();

        int expected_log_num = batchInsertLog(mTestLogs);

        logs = GetAllDeviceLog();

        int curr_log_time;

        boolean rc = false;

        for (int i = 0; i < logs.size(); ++i) {
            log = logs.get(i);
            curr_log_time = log.getLogDateUnixSec();
            if (curr_log_time >= StartTime && curr_log_time <= EndTime) {
                if (log.getDeviceLogInfoTypeId() == logInfoType) {
                    rc = true;
                    break;
                }
            }
        }
        Assert.assertTrue(rc);
    }

}