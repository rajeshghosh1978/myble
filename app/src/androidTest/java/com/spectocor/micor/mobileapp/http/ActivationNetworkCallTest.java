package com.spectocor.micor.mobileapp.http;

import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.sessionactivation.api.EnrollmentInfo;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;

import java.io.IOException;
import java.text.ParseException;

import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Test cases for device activation network call.
 *
 * Created by Rafay Ali on 1/27/2016.
 */
public class ActivationNetworkCallTest extends AndroidTestCase {

    public void testSessionNetworkCall(){

        try {
            RequestBody requestBody = new MultipartBody.Builder()
                    .addFormDataPart("activationCode", "1")
                    .setType(MultipartBody.FORM)
                    .build();

            Request request = new Request.Builder()
                    .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_ACTIVATION)
                    .post(requestBody)
                    .build();

            Response response = HttpManager.getInstance().getHttpClient().newCall(request).execute();
            JSONObject jsonObject = new JSONObject(response.body().string());

            EnrollmentInfo enrInfo = new EnrollmentInfo();

            if (jsonObject.getString("code").equals("200")){
                enrInfo.setFacilityName(jsonObject.getJSONObject("data").getJSONObject("facility").getString("name"));
                enrInfo.setFirstName(jsonObject.getJSONObject("data").getJSONObject("patient").getString("firstName"));
                enrInfo.setLastName(jsonObject.getJSONObject("data").getJSONObject("patient").getString("lastName"));
                // TODO parse actual enrollment date here
                enrInfo.setEnrollmentDateIso8601("2016-01-19");
                enrInfo.setSessionId(Integer.parseInt(jsonObject.getJSONObject("data").getString("sessionId")));
            }

            Assert.assertNotEquals("", enrInfo.getEnrollmentDateIso8601());
            Assert.assertNotEquals("", enrInfo.getFirstName());
            Assert.assertNotEquals("", enrInfo.getFacilityName());
            Assert.assertNotEquals(0, enrInfo.getSessionId());

        } catch (IOException | JSONException | ParseException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }
}
