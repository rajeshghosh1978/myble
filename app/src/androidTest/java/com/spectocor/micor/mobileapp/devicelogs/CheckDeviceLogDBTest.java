package com.spectocor.micor.mobileapp.devicelogs;

import android.content.Context;
import android.database.Cursor;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;
import com.spectocor.testutilities.storage.ExternalStorage;
import com.spectocor.testutilities.utils.ThreadUtility;

import junit.framework.Assert;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import android.util.Log;

/**
 * Created by qwang on 3/16/2016.
 *
 * dump device log DB info at cat log
 *
 */
public class CheckDeviceLogDBTest extends AndroidTestCase{

    private static String TAG = "CheckDeviceLogDBTest";

    private static String TABLE_NAME = "DEVICE_LOGS";

    private String DATATYPE_INTEGER = "INTEGER";
    private String DATATYPE_TEXT = "TEXT";

    private String COLUMN_NAME_SESSION_ID = "SESSION_ID";
    private String COLUMN_NAME_INSERT_TIMESTAMP = "INSERT_TIMESTAMP";
    private String COLUMN_NAME_TRANSMITTED_TIMESTAMP = "TRANSMITTED_TIMESTAMP";
    private String COLUMN_NAME_MESSAGE = "MESSAGE";
    private String COLUMN_NAME_OF_EVENT_OCCURED = "NAME_OF_EVENT_OCCURED";


    private static DeviceLogsDbHelper mRepository;

    private static Context mContext;
    private static int mMaxListLen = 100;

    /**
     * setting up test environment
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        super.setUp();

        mContext = getContext();
        //mContext.deleteDatabase(LogEventsDatabaseHelper.DatabaseName);

        //LogEventsDatabaseHelper databaseHelper = new LogEventsDatabaseHelper(mContext);

        mRepository = DeviceLogsDbHelper.getInstance(mContext);



    }

    public void tearDown()throws Exception{
        if(mRepository.getReadableDatabase().isOpen())
            mRepository.getReadableDatabase().close();

        super.tearDown();
    }

    public void testDumpDeviceLogs()throws Exception{

        ArrayList<DeviceLogEntity> realLogs = GetLogsUpTillNow();
        DeviceLogEntity log;
        String logStr;
        Date insertDate;
        Date transmitDate;

        logStr = "" + "Session ID:" + "Log Type ID:" + "Log Message:" + "Log Insert Date:" + "Log Transmit Date";

        Log.i(TAG,logStr);
        for (int i = 0; i < realLogs.size(); ++i) {
            log = realLogs.get(i);

            insertDate = new Date(log.getInsertTimestamp());
            transmitDate = new Date(log.getTransmittedTimestamp());
            logStr = "" + log.getSessionId() + ":" +  log.getEventTypeId() + ":"+ log.getEventMessage() + ":" + insertDate.toString()+ ":" + transmitDate.toString();
            Log.i(TAG,">>>>>");
            Log.i(TAG,logStr);
        }

        ThreadUtility.applicationSleep(10000);
    }


    public ArrayList<DeviceLogEntity> GetLogsUpTillNow() throws SQLDataException {

        Log.i(TAG, "Retrieving Device logs");
        String QUERY = "SELECT * FROM " + TABLE_NAME ;

        String SELECTION_ARGS[] = {};
        Log.i(TAG,QUERY + "before cursor with args " + SELECTION_ARGS.toString());
        Cursor c = mRepository.getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        Log.i(TAG,QUERY + "after cursor with args " + SELECTION_ARGS.toString());
        Log.i(TAG,"Num of rows: " + c.getCount());

        if (c.getCount() == 0) {
            Log.i(TAG,"Row not found ... ");
            c.close(); // close cursor to free up memory
            throw new SQLDataException("NONE_LEFT_TO_TRANSFER");
        } else {
            Log.i(TAG,"Row Count =" + c.getCount());
        }

        //Iterate Cursor to get all rows in ArrayList
        ArrayList<DeviceLogEntity> mArrayList = new ArrayList<>();
        DeviceLogEntity log;
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {

            log = new DeviceLogEntity();
            //Log.i(TAG,"DB:REt:SessionId=" + c.getInt(c.getColumnIndex(COLUMN_NAME_SESSION_ID)));
            log.setSessionId(c.getInt(c.getColumnIndex(COLUMN_NAME_SESSION_ID)));
            log.setEventMessage(c.getString(c.getColumnIndex(COLUMN_NAME_MESSAGE)));
            log.setInsertTimestamp(c.getLong(c.getColumnIndex(COLUMN_NAME_INSERT_TIMESTAMP)));
            log.setEventTypeId(c.getInt(c.getColumnIndex(COLUMN_NAME_OF_EVENT_OCCURED)));
            log.setTransmittedTimestamp(c.getInt(c.getColumnIndex(COLUMN_NAME_TRANSMITTED_TIMESTAMP)));
            mArrayList.add(log);
        }

        Log.i(TAG,"Before Return EcgChunks");

        if (!c.isClosed())
            c.close();

        return mArrayList;
    }
}
