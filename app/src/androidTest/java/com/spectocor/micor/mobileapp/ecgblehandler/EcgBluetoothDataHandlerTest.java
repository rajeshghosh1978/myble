package com.spectocor.micor.mobileapp.ecgblehandler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.content.LocalBroadcastManager;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.google.gson.Gson;
import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainService;
import com.spectocor.micor.mobileapp.ecgadapter.HeartBeatSequencer;
import com.spectocor.micor.mobileapp.ecgadapter.HeartRateHandler;
import com.spectocor.micor.mobileapp.ecgadapter.LiveEcgChunkInMemory;
import com.spectocor.testutilities.AssetUtility;
import com.spectocor.testutilities.ECGDataCreator;
import com.spectocor.testutilities.IntentUtility;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.ecg_data.ECGBTRawData;
import com.spectocor.testutilities.utils.ArrayUtility;
import com.spectocor.testutilities.utils.ThreadUtility;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 2/15/2016.
 *
 * @LINK EcgBluetoothDataHandler
 *       NEW_ECG_DATA
 *
 * CHANGES HISTORY
 *************************************************************
 * Date             Author          Comments
 * 02/15/2016       Qian            init
 * 02/16/2016       Qian            add change history and running history
 *
 *************************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 02/17/2016       Qian            fail :
 *
 *
 */

@LargeTest
@RunWith(AndroidJUnit4.class)
public class EcgBluetoothDataHandlerTest {

    //////////////////////////////////////////////////////////////
    // Simple Test Framework
    //////////////////////////////////////////////////////////////
    private static String TAG = "EcgBluetoothDataHandlerTest";

    //private UITestSimpleFramework mTestFramework;
    private static AMainActivity mActivity;
    private static Context mContext;
    private static EcgBluetoothDataHandler mHandler;

    private static int mSampleRate = TestEnvGlobalConst.SAMPLE_RATE;
    private static long mDefaultSleepMillionSecond = TestEnvGlobalConst.APP_SLEEP_MILLION_SECOND;

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            init(); // launch the activity and init class member variables
        }
    };

    @Before
    public void before(){
        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond); // waiting for activity launch
        AMainService.sendToSignalView = true;
        mActivity.callWebviewJsFunction("displayAdminEcgSignalPage()");  // jump to signal view
        LiveEcgChunkInMemory.initAll();  // clean data chunk at buffer
        //SessionStateAccess.saveSessionStateJson(mContext,"{sessionID=111}");
    }

    @After
    public void after(){
        LiveEcgChunkInMemory.initAll();  // clean data chunk at buffer
        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond); // waiting for activity close
    }
    //////////////////////////////////////////////
    // test data
    //////////////////////////////////////////////
    private int mDefaultHeartBeatVal = TestEnvGlobalConst.NORMAL_HEART_BEAT_RATE;
    private int mDefaultDisplayTime = TestEnvGlobalConst.DEFAULT_RUN_TEST_TIME;

    //////////////////////////////////////////////
    // test case
    //////////////////////////////////////////////
    /**
     *
     * test main activity receiving ecg two channel data and display ECG waveform, heart beat rate , heart beat animation
     * test data:
     *
     * Test Case ID : UTMAID160001
     */
    @Test
    public void testDisplayECGSignalViewWithBaseLine() throws InterruptedException{

        String raw_data_path = "ui_test_data/signal_view/baseline.txt";
        //String raw_data_path = "ui_test_data/signal_view/noise.txt";

        EcgChannelBluetoothData ecgData;
        // prepare test data
        short[] tchannel1 = new short[mSampleRate] ;
        short[] tchannel2 = new short[mSampleRate];


        // read data file
        ECGBTRawData ecgbtRawData;
        ecgbtRawData = AssetUtility.readBTRAWData(mContext, raw_data_path);

        ecgbtRawData.dumpData(10);

        // render
        int channel1_index = 0;
        int channel2_index = 0;

        short[] channel1,channel2;
        channel1 = ECGBTRawData.normalization(ecgbtRawData.getChannelData(1));
        channel2 = ECGBTRawData.normalization(ecgbtRawData.getChannelData(2));

        long start_timestamp = System.currentTimeMillis();;

        for(int i = 0; i < channel1.length; ){

            long start = System.currentTimeMillis();

            System.arraycopy(channel1, channel1_index, tchannel1, 0, mSampleRate);
            System.arraycopy(channel2, channel2_index, tchannel2, 0, mSampleRate);

            channel1_index += mSampleRate;
            channel2_index += mSampleRate;
            i += mSampleRate;

            long end = System.currentTimeMillis();

            Thread.sleep(5000 - (end - start));

            Log.d(TAG, "second:" + (i / mSampleRate));

            ecgData = new EcgChannelBluetoothData(start_timestamp,tchannel1,tchannel2);

            mHandler.obtainMessage(EcgBluetoothDataHandler.NEW_ECG_DATA, ecgData).sendToTarget();  // display heart beat rate

            showECGWaveform(tchannel1,tchannel2);   // display ECG waveform

            start_timestamp += 1000;
        }

        // check result

        //wait
    }

    /**
     *
     * test main activity receiving heart beat number to display heart beat animation
     * test data: 70
     *
     * Test Case ID : UTMAID160002
     */
    @Test
    public void testDisplayECGSignalViewWith30bpm() throws InterruptedException{

        String raw_data_path = "ui_test_data/signal_view/80bpm.txt";

        EcgChannelBluetoothData ecgData;
        // prepare test data
        short[] tchannel1 = new short[mSampleRate] ;
        short[] tchannel2 = new short[mSampleRate];

        // read data file
        ECGBTRawData ecgbtRawData;
        ecgbtRawData = AssetUtility.readBTRAWData(mContext, raw_data_path);

        ecgbtRawData.dumpData(10);

        // render
        int channel1_index = 0;
        int channel2_index = 0;

        short[] channel1,channel2;
        channel1 = ECGBTRawData.normalization(ecgbtRawData.getChannelData(1));
        channel2 = channel1;

        long start_timestamp = System.currentTimeMillis();;

        for(int i = 0; i < channel1.length; ){

            long start = System.currentTimeMillis();

            System.arraycopy(channel1, channel1_index, tchannel1, 0, mSampleRate);
            System.arraycopy(channel2, channel2_index, tchannel2, 0, mSampleRate);

            channel1_index += mSampleRate;
            channel2_index += mSampleRate;
            i += mSampleRate;

            long end = System.currentTimeMillis();

            Thread.sleep(5000 - (end - start));

            Log.d(TAG, "second:" + (i / mSampleRate));

            ecgData = new EcgChannelBluetoothData(start_timestamp,tchannel1,tchannel2);

            mHandler.obtainMessage(EcgBluetoothDataHandler.NEW_ECG_DATA, ecgData).sendToTarget();  // display heart beat rate

            showECGWaveform(tchannel1,tchannel2);   // display ECG waveform

            start_timestamp += 1000;
        }

        // check result

    }

    /**
     *
     * test main activity receiving heart beat number to display heart beat animation
     * test data:
     *
     * Test Case ID : UTMAID160003
     */
    @Test
    public void testDisplayECGSignalViewWith30bpmGap() throws InterruptedException{

        String raw_data_path = "ui_test_data/signal_view/80bpm.txt";

        EcgChannelBluetoothData ecgData;
        // prepare test data
        short[] tchannel1 = new short[mSampleRate] ;
        short[] tchannel2 = new short[mSampleRate];

        boolean showFlag = true;
        AMainService.sendToSignalView = showFlag;

        // read data file
        ECGBTRawData ecgbtRawData;
        ecgbtRawData = AssetUtility.readBTRAWData(mContext, raw_data_path);

        ecgbtRawData.dumpData(10);


        // render
        int channel1_index = 0;
        int channel2_index = 0;

        short[] channel1,channel2;
        channel1 = ECGBTRawData.normalization(ecgbtRawData.getChannelData(1));
        channel2 = channel1;

        long start_timestamp = System.currentTimeMillis();

        for(int i = 0; i < channel1.length; ){

            long start = System.currentTimeMillis();

            System.arraycopy(channel1, channel1_index, tchannel1, 0, mSampleRate);
            System.arraycopy(channel2, channel2_index, tchannel2, 0, mSampleRate);

            channel1_index += mSampleRate;
            channel2_index += mSampleRate;
            i += mSampleRate;

            long end = System.currentTimeMillis();

            Thread.sleep(5000 - (end - start));

            Log.d(TAG, "second:" + (i / mSampleRate));

            ecgData = new EcgChannelBluetoothData(start_timestamp,tchannel1,tchannel2);

            mHandler.obtainMessage(EcgBluetoothDataHandler.NEW_ECG_DATA, ecgData).sendToTarget();  // display heart beat rate

            showECGWaveform(tchannel1,tchannel2);   // display ECG waveform

            start_timestamp += 2000;
        }

        // check result

    }

    /////////////////////////////////////////////////////////////////
    // support function
    /////////////////////////////////////////////////////////////////

    /**
     * init class member variables
     */
    private void init(){

        mActivity = mActivityRule.getActivity();
        mContext  = mActivity.getApplicationContext();
        mHandler = EcgBluetoothDataHandler.getInstance(mContext);

        assert(mActivity != null);
        assert(mContext != null);
        assert(mHandler != null);

        Log.d(TAG,"init");
    }

    /**
     * display two channel ECG data, one second data
     * @param channel1
     * @param channel2
     */
    private static void showECGWaveform(short[] channel1,short[] channel2){

        ArrayList<Integer> filtCh1 = HeartRateHandler.getInstance(mContext).applyFilter(channel1, 1);
        ArrayList<Integer> filtCh2 = HeartRateHandler.getInstance(mContext).applyFilter(channel2, 2);

        // JOINING BOTH BROADCASTS AND SENDING AS ONE
        Intent intent = IntentUtility.createRenderECGWaveformIntent(filtCh1,filtCh2);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);   // display ECG waveform
    }

    private static void showHRBeat(long startTimeStamp,short[] channel1,short[] channel2){

        EcgChannelBluetoothData ecgData = new EcgChannelBluetoothData(startTimeStamp,channel1,channel2);

        mHandler.obtainMessage(EcgBluetoothDataHandler.NEW_ECG_DATA, ecgData).sendToTarget();           // display heart beat rate

    }
    private static void showContinuousECGWaveform(){

    }

    /**
     * show continuous signal waveform, each chunk data is one second sampling data
     *
     * @param continuousSignalChannel1
     * @param continuousSignalChannel2
     */
    private void showContinuousSignalWaveform(ArrayList<short[]> continuousSignalChannel1,ArrayList<short[]> continuousSignalChannel2 ){

        Assert.assertTrue(TAG + ":Fail data format", continuousSignalChannel1.size() > 0);
        Assert.assertTrue(TAG+":Fail data format",continuousSignalChannel2.size() > 0);
        Assert.assertTrue(TAG+":Fail data format",continuousSignalChannel1.size() == continuousSignalChannel2.size());

        long start; // start time
        long end;   // end time
        long wait;  // wait time

        short[] channel1,channel2;

        long start_timestamp = System.currentTimeMillis();

        for (int i = 0; i < continuousSignalChannel1.size(); ++i) {

            start = System.currentTimeMillis();

            channel1 = continuousSignalChannel1.get(i);
            channel2 = continuousSignalChannel2.get(i);

            end = System.currentTimeMillis();

            wait = 1000 - (end - start) ;

            if(wait > 0)
                ThreadUtility.applicationSleep(wait);

            //Log.d(TAG + "DATA", tchannel1.toString());

            showHRBeat(start_timestamp,channel1,channel2);

            showECGWaveform(channel1, channel2);

            ++start_timestamp;
        }
    }

}
