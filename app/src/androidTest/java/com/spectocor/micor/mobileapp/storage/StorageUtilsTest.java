package com.spectocor.micor.mobileapp.storage;

import android.test.AndroidTestCase;


import com.spectocor.testutilities.storage.StorageUtils;

import org.junit.Assert;

/**
 * Created by qwang on 3/8/2016.
 */
public class StorageUtilsTest extends AndroidTestCase{

    private static String TAG = "StorageUtilsTest";

    public void testIsExternalStorageAvailable(){

        boolean rc = StorageUtils.isSDAvailable();

        Assert.assertTrue(TAG + ":Fail to detect external storage", rc);
    }

}
