package com.spectocor.micor.mobileapp.sessionactivation.api;

import android.content.Context;
import android.test.AndroidTestCase;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.http.common.ResponseCodes;
import com.spectocor.testutilities.TestEnvGlobalConst;
import com.spectocor.testutilities.session.api.PatientReportedEventsRESTAPI;

import org.junit.Assert;

import java.io.IOException;
import java.net.SocketTimeoutException;

import okhttp3.Response;

/**
 * Created by qwang on 2/26/2016.
 *
 * test case for Patient Triggered Events transmission to USD API
 *
 * QA : ALGO-972
 * QA : ALGO-975
 *
 * DEV: ALGO-727
 *
 * reference class
 * @link com.spectocor.micor.mobileapp.sessionactivation.api.PatientReportedEventsApi
 *
 * CHANGES HISTORY
 *************************************************************
 * Date             Author          Comments
 * 02/26/2016       Qian            create test case for Patient Triggered Events transmission to USD API
 *
 *************************************************************
 * RUNNING HISTORY
 * Date             Author          Comments
 * 02/26/2016       Qian            fail : testReportWithIllegalSymptoms
 */
public class PatientReportedEventsApiTest extends AndroidTestCase {

    private static String TAG = "PatientReportedEventsApiTest";


    ////////////////////////////////////////////////////////
    // member variables
    ////////////////////////////////////////////////////////
    private static Context mContext;
    private static CurrentSessionState mSessionState;
    private static PatientReportedEventsApi mPatientReportedEventsApi;


    ///////////////////////////////////////////////////////
    // test data
    //////////////////////////////////////////////////////
    public static String SERVER_ADDRESS = TestEnvGlobalConst.SERVER_ADDRESS;
    public static String SSL_SERVER_ADDRESS = TestEnvGlobalConst.SSL_SERVER_ADDRESS;
    public static String FAIL_SERVER_ADDRESS = TestEnvGlobalConst.FAIL_SERVER_ADDRESS;
    public static String REST_URL_PATIENT_LOG = TestEnvGlobalConst.REST_URL_PATIENT_LOG;

    private static int mDefaultSessionID = 1;//1234;
    private static int mDefaultChunkSize = 1024;
    private static int mDefaultMaxiSessionID = 987654;
    private static int mDefaultTestChunkNum = TestEnvGlobalConst.DEFAULT_RUN_TEST_TIME;
    private static String mDefaultEcg = DeviceIdentifiers.getEcgId();  // "SPEC-SD5F855GHJ";
    private static String mDefaultPda = DeviceIdentifiers.getPdaId();  // "AHSD4F9F4DD4SD4FF9";

    ////////////////////////////////////////////////////////
    // setup
    ////////////////////////////////////////////////////////
    @Override
    public void setUp(){
        mContext = getContext();
        assert(mContext != null);

        // check test environment
        mSessionState = SessionStateAccess.getCurrentSessionState(mContext);
        // session active
        Assert.assertTrue(TAG + ":Fail to start test: there is no active session at this device", mSessionState.sessionActive);
        // session id is 1
        // Assert.assertTrue(TAG+":Fail to start test: expected session id:"+mDefaultSessionID+":real session id:" + mSessionState.getSessionId() +"  at this device",mSessionState.getSessionId() == mDefaultSessionID);

        Log.d(TAG,"session id:" + mSessionState.sessionId);

        mPatientReportedEventsApi = PatientReportedEventsApi.getInstance();
    }

    ////////////////////////////////////////////////////////
    // test case
    ////////////////////////////////////////////////////////

    /**
     * happy path for sending patient triggered events without symptoms
     *
     * input
     *
     * return successful response
     *
     * Test Case ID : UTMAID230001
     */
    public void testReportWithoutSymptoms(){


        // prepare test data

        // call API
        try {
            Response response = PatientReportedEventsRESTAPI.reportWithoutSymptoms(SSL_SERVER_ADDRESS, REST_URL_PATIENT_LOG, mContext);
            Assert.assertTrue(TAG + ":Fail to send patient trigger event with symptom", response.code() == ResponseCodes.OK);

        }catch (IOException e){
            Log.e(TAG,e.getMessage());
            Assert.fail(TAG + ":There is IO exception");
        }
        // check response

    }

    /**
     * happy path for sending patient triggered events with symptoms
     *
     * input
     *
     * return successful response
     *
     * Test Case ID : UTMAID230002
     */
    public void testReportWithSingleSymptom(){


        // prepare test data
        String symptoms = "ChestDiscomfort";

        // call API
        try {
            Response response = PatientReportedEventsRESTAPI.reportWithSymptoms(SSL_SERVER_ADDRESS, REST_URL_PATIENT_LOG, symptoms, mContext);
            Assert.assertTrue(TAG + ":Fail to send patient trigger event with symptom", response.code() == ResponseCodes.OK);

        }catch (IOException e){
            Log.e(TAG,e.getMessage());
            Assert.fail(TAG + ":There is IO exception");
        }
        // check response

    }

    /**
     * happy path for sending patient triggered events with symptoms
     *
     * input
     *
     * return successful response
     *
     * Test Case ID : UTMAID230003
     */
    public void testReportWithSymptoms(){


        // prepare test data
        String symptoms = "ChestDiscomfort,Palpitation,Fluttering";

        // call API
        try {
            Response response = PatientReportedEventsRESTAPI.reportWithSymptoms(SSL_SERVER_ADDRESS, REST_URL_PATIENT_LOG, symptoms, mContext);
            // check response
            Assert.assertTrue(TAG + ":Fail to send patient trigger event with symptom", response.code() == ResponseCodes.OK);

        }catch (IOException e){
            Log.e(TAG,e.getMessage());
            Assert.fail(TAG + ":There is IO exception");
        }
    }

    /**
     * fail path for sending patient triggered events with illegal symptoms
     *
     * input
     *
     * return successful response
     *
     * Test Case ID : UTMAID230004
     */
    public void testReportWithIllegalSymptoms(){


        // prepare test data
        String symptoms = "ChestDiscomfort1";

        // call API
        try {
            Response response = PatientReportedEventsRESTAPI.reportWithSymptoms(SSL_SERVER_ADDRESS, REST_URL_PATIENT_LOG, symptoms, mContext);
            // check response
            Assert.assertTrue(TAG + ":Fail to send patient trigger event with symptom", response.code() != ResponseCodes.OK);

        }catch (IOException e){
            Log.e(TAG,e.getMessage());
            Assert.fail(TAG + ":There is IO exception");
        }
    }

    /**
     * fail case for sending patient triggered events with device id and ecg id
     *
     * input
     *
     * return successful response
     *
     * Test Case ID : UTMAID230005
     */
    public void testReportWithIllegalDeviceID(){


        // prepare test data
        String symptoms = "ChestDiscomfort";
        String illegalEcgID = "111";
        String illegalPdaID = "111";

        // call API
        try {
            //PatientReportedEventsRESTAPI.setECGDeviceID(illegalEcgID);
            PatientReportedEventsRESTAPI.setPDADeviceID(illegalPdaID);
            Response response = PatientReportedEventsRESTAPI.reportWithSymptoms(SSL_SERVER_ADDRESS, REST_URL_PATIENT_LOG, symptoms, mContext);
            // check response
            Log.d(TAG, "response message:" + response.message());
            Log.d(TAG, "response code:" + response.code());
            Assert.assertTrue(TAG + ":Fail to send patient trigger event with symptom", response.message().compareTo("Gone") ==0);

        }catch (IOException e){
            Log.e(TAG,e.getMessage());
            Assert.fail(TAG + ":There is IO exception");
        }
    }



    /**
     * fail case for sending patient triggered events with device id and ecg id to wrong server
     *
     * input
     *
     * return successful response
     *
     * Test Case ID : UTMAID230006
     */
    public void testReportWithWrongServer(){


        // prepare test data
        String symptoms = "ChestDiscomfort";

        // call API
        try {
            Response response = PatientReportedEventsRESTAPI.reportWithSymptoms(FAIL_SERVER_ADDRESS, REST_URL_PATIENT_LOG, symptoms, mContext);
            // check response
            Log.d(TAG, response.message());

            Assert.assertTrue(TAG + ":Fail to send patient trigger event with symptom", response == null);

        }catch (SocketTimeoutException timeoutE){
            Log.d(TAG, "Timeout Exception:" + timeoutE.getMessage());
            Assert.assertTrue(TAG + ":Catch SocketTimeoutException",true);
        }
        catch (IOException e){
            Log.d(TAG, "IOException:" + e.getMessage());
            Assert.fail(TAG + ":Catch IO exception");
        }
    }


    /**
     * sending patient triggered events without symptoms without network
     *
     * input
     *
     * return successful response
     *
     * Test Case ID : UTMAID230007
     */
    public void testReportWithoutSymptomsWithoutNetwork(){


        // prepare test data

        // call API
        try {

            mPatientReportedEventsApi.reportWithoutSymptoms();

            // check DB;

        }catch (Exception e){
            Log.e(TAG, "" + e.getMessage());
            Assert.fail(TAG + ":There is exception." + e.getMessage());
        }
        // check response

    }


    /**
     * sending patient triggered events with correct device id and illegal ecg id
     *
     * input
     *
     * return successful response
     *
     * Test Case ID : UTMAID230008
     */
    public void testReportWithIllegalECGDeviceID(){


        // prepare test data
        String symptoms = "ChestDiscomfort";
        String illegalEcgID = "111";

        // call API
        try {
            PatientReportedEventsRESTAPI.setECGDeviceID(illegalEcgID);
            Response response = PatientReportedEventsRESTAPI.reportWithSymptoms(SSL_SERVER_ADDRESS, REST_URL_PATIENT_LOG, symptoms, mContext);
            // check response
            Log.d(TAG, "response message:" + response.message());
            Log.d(TAG, "response code:" + response.code());
            Assert.assertTrue(TAG + ":Fail to send patient trigger event with symptom", response.message().compareTo("Gone") ==0);

        }catch (IOException e){
            Log.e(TAG,e.getMessage());
            Assert.fail(TAG + ":There is IO exception");
        }
    }
    ////////////////////////////////////////////////////////
    // support function
    ////////////////////////////////////////////////////////



}
