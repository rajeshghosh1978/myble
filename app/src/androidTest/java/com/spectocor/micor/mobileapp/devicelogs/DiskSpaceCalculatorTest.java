package com.spectocor.micor.mobileapp.devicelogs;

import android.test.AndroidTestCase;

@SuppressWarnings("WeakerAccess")
/**
 * Test cases for disk space calculator
 */
public class DiskSpaceCalculatorTest extends AndroidTestCase {

    /**
     * object for testing
     */
    DiskSpaceCalculator calculator;

    /**
     * instantiates test variables
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        calculator = new DiskSpaceCalculator();
    }


    /**
     * test if free space obtained from the device is more than 0.
     *
     * @throws Exception if test ended with exception
     */
    public void testGetSdCardFreeBytes() throws Exception {
        long actual = calculator.getSdCardFreeBytes();
        assertTrue(actual > (long) 0);
    }

    /**
     * tests if the device has a SD card with SD card more than 1 MB.
     *
     * @throws Exception if test ended with exception
     */
    public void testGetSdCardTotalBytes() throws Exception {
        long actual = calculator.getSdCardTotalBytes();
        assertTrue(actual > (long) 1024 * 1024);
    }


    /**
     * Check that free bytes is less than total bytes
     *
     * @throws Exception if test ended with exception
     */
    public void testGetSdCardFreeBytesLessThanTotalBytes() throws Exception {
        long freeBytes = calculator.getSdCardFreeBytes();
        long totalBytes = calculator.getSdCardTotalBytes();
        assertTrue(freeBytes <= totalBytes);
    }


    /**
     * test if free space obtained from the device is more than 0.
     *
     * @throws Exception if test ended with exception
     */
    public void testGetInternalFreeBytes() throws Exception {
        long actual = calculator.getInternalFreeBytes();
        assertTrue(actual > 0);
        assertTrue(actual < (long) 16 * 1024 * 1024 * 1024); // less than 16 GB
    }

    /**
     * tests if the device has a SD card with SD card more than 100 MB.
     *
     * @throws Exception if test ended with exception
     */
    public void testGetInternalTotalBytes() throws Exception {
        long actual = calculator.getInternalTotalBytes();
        assertTrue(actual > (long) 100 * 1024 * 1024);
        assertTrue(actual < (long) 16 * 1024 * 1024 * 1024); // less than 16 GB
    }


    /**
     * Check that free bytes is less than total bytes
     *
     * @throws Exception if test ended with exception
     */
    public void testGetInternalFreeBytesLessThanTotalBytes() throws Exception {
        long freeBytes = calculator.getInternalFreeBytes();
        long totalBytes = calculator.getInternalTotalBytes();
        assertTrue(freeBytes < totalBytes);
    }


    /**
     * Test file size for the longest bytes possible
     *
     * @throws Exception if test ended with exception
     */
    public void testGetReadableSizeMaxValue() throws Exception {
        long sizeBytes = Long.MAX_VALUE;
        String expected = "9.2 EB";
        String actual = DiskSpaceCalculator.getReadableSize(sizeBytes);
        assertEquals(expected, actual);
    }

    /**
     * Test file size for invalid bytes possible
     *
     * @throws Exception if test ended with exception
     */
    public void testGetReadableSizeMinValue() throws Exception {
        long sizeBytes = Long.MIN_VALUE;
        String expected = "0 B";
        String actual = DiskSpaceCalculator.getReadableSize(sizeBytes);
        assertEquals(expected, actual);
    }

    /**
     * Test file size for zero bytes possible
     *
     * @throws Exception if test ended with exception
     */
    public void testGetReadableSize0() throws Exception {
        long sizeBytes = 0;
        String expected = "0 B";
        String actual = DiskSpaceCalculator.getReadableSize(sizeBytes);
        assertEquals(expected, actual);
    }


    /**
     * Test file size for zero bytes possible
     *
     * @throws Exception if test ended with exception
     */
    public void testGetReadableSizeKb() throws Exception {
        long sizeBytes = 1025;
        String expected = "1.0 kB";
        String actual = DiskSpaceCalculator.getReadableSize(sizeBytes);
        assertEquals(expected, actual);
    }


    public void testGetGetReadableSizeDic() throws Exception {
        // tests from http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
        long[] sizeBytesArray = {0, 27, 999, 1000, 1023, 1024, 1728, 110592, 7077888, 452984832, 28991029248l, 1855425871872l, 9223372036854775807l};
        String[] expectedArray = {"0 B", "27 B", "999 B", "1.0 kB", "1.0 kB", "1.0 kB", "1.7 kB", "110.6 kB", "7.1 MB", "453.0 MB", "29.0 GB", "1.9 TB", "9.2 EB"};
        for (int i = 0; i < sizeBytesArray.length; i++) {
            assertEquals("Element" + i + " expected to be: " + expectedArray[i], expectedArray[i], DiskSpaceCalculator.getReadableSize(sizeBytesArray[i]));
        }
    }

}