package com.spectocor.micor.mobileapp;

import android.content.pm.PackageInfo;
import android.test.ApplicationTestCase;
import android.test.MoreAsserts;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.common.ServiceFactory;

import org.junit.Assert;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 *
 * https://codinghard.wordpress.com/2009/06/04/a-first-look-at-the-android-testing-framework/
 *
 * Test class for main entry of the android application
 *
 */
@SuppressWarnings("ALL")
public class ApplicationTest extends ApplicationTestCase<AMainApplication> {

    private AMainApplication application;

    public ApplicationTest() {
        super(AMainApplication.class);
    }

    /**
     * instantiate test classes
     *
     * @throws Exception if test ended with exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        createApplication();
        application = getApplication();
    }

    /**
     * testing that we have chosen a correct pattern for package name
     *
     * @throws Exception if test ended with exception
     */
    public void testCorrectVersion() throws Exception {
        PackageInfo info = application.getPackageManager().getPackageInfo(application.getPackageName(), 0);
        assertNotNull(info);
        MoreAsserts.assertMatchesRegex("\\d\\.\\d", info.versionName);
    }


    /**
     * testing when application starts, service factory should be created
     * @throws Exception if test ended with exception
     */
    public void testServiceFactoryCreated() throws Exception {
        ServiceFactory factory = ServiceFactory.getInstance();
        Assert.assertNotNull(factory);
    }

}