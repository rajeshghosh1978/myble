package com.spectocor.micor.mobileapp.time;

import android.test.AndroidTestCase;

import java.sql.Time;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import android.util.Log;

import org.junit.Test;

/**
 * Created by qwang on 3/30/2016.
 *
 * test function for Date, Calendar
 *
 * http://stackoverflow.com/questions/1404210/java-date-vs-calendar
 *
 *
 * try not to mix Date and Calendar
 *
 * suggest to use Calendar or third party lib  Joda Time library
 *
 * or update JDK 8.0 with new time and date package java.time
 *
 */
public class TimeZoneTest extends AndroidTestCase{

    private static final String TAG = "TimeZoneTest";

    public void testTimezone(){

        String[] zoneID = TimeZone.getAvailableIDs();
        TimeZone timeZone = TimeZone.getDefault();

        String displayName = timeZone.getDisplayName();

        boolean daylightTimeFlag = timeZone.useDaylightTime();

        Log.i(TAG,displayName);
        Log.i(TAG,"" + daylightTimeFlag);
    }

    public void testCurrentUTC(){


        long currentUTC = System.currentTimeMillis();

        Date millis2Date = new Date(currentUTC);

        Log.d(TAG,"System.currentTimeMillis:" + currentUTC);

        Log.d(TAG,"Date.getTime:" + millis2Date.getTime());

        DateFormat df = DateFormat.getDateTimeInstance();

        //df.setTimeZone(TimeZone.getDefault());
        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date currentTime = new Date();

        String defaultTime = df.format(currentTime);

        Log.d(TAG, "UTC:" + defaultTime);

        df.setTimeZone(TimeZone.getDefault());

        defaultTime = df.format(currentTime);

        Log.d(TAG,"DEF:"+defaultTime);

        Long timeInMilliseconds = currentTime.getTime();

        Log.d(TAG, "timeInMilliseconds:" + timeInMilliseconds);

        Log.d(TAG, "DONE");
    }

    public void testCalendar(){

        // Java Calendar which is used to convert Date to a set of integer, Y,M,D,H,M,S

        Calendar rightNow = Calendar.getInstance();

        Locale[] locales = rightNow.getAvailableLocales();

        Log.i(TAG, "Total locale number:" + locales.length);

        for(Locale locale: locales){

            Log.i(TAG,locale.toString());
        }

        Log.d(TAG, "DONE");
    }


}
