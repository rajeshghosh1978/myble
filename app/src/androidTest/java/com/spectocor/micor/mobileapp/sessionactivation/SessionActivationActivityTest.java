package com.spectocor.micor.mobileapp.sessionactivation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.web.matcher.DomMatchers;
import android.support.test.espresso.web.model.Atom;
import android.support.test.espresso.web.model.Evaluation;
import android.support.test.espresso.web.model.SimpleAtom;
import android.support.test.espresso.web.sugar.Web;
import android.support.test.espresso.web.webdriver.DriverAtoms;
import android.support.test.espresso.web.webdriver.Locator;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.content.LocalBroadcastManager;
import android.test.suitebuilder.annotation.LargeTest;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.ecgblehandler.StlDeviceInfo;
import com.spectocor.micor.mobileapp.uitestframework.UIMockData;
import com.spectocor.micor.mobileapp.uitestframework.UITestCommand;
import com.spectocor.micor.mobileapp.uitestframework.UITestSimpleFramework;
import com.spectocor.micor.mobileapp.uitestframework.UIWebCmdUtility;
import com.spectocor.testutilities.utils.ThreadUtility;

import org.hamcrest.Matcher;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.support.test.espresso.web.sugar.Web.onWebView;
import static android.support.test.espresso.web.webdriver.DriverAtoms.findElement;
import static android.support.test.espresso.web.webdriver.DriverAtoms.findMultipleElements;
import static android.support.test.espresso.web.webdriver.DriverAtoms.getText;
import static android.support.test.espresso.web.webdriver.DriverAtoms.webClick;

/**
 * Created by qwang on 12/23/2015.
 * UI Test Class for Session Activation Activity
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class SessionActivationActivityTest {
    // define test data

    private static final long mWaitingTime = 1000;
    private static final long mWaitingTimeout = 3000;

    private static final long mWaitStartTestTime = 1000;
    private static final long mWaitECGSignalTime = 10000;
    private static final long mWaitCommonPageTime = 1000;
    private static final long mWaitEndTestTime = 6000;

    // ECG Device
    private static final String mDefaultEcgDevName = "STL ECG 1";
    private static final String mDefaultEcgDevMac = "ECG BT Name(MAC)";

    private static final String mSecondEcgDevName = "STL ECG 2";
    private static final String mSecondEcgDevMac = "ECG BT Name(MAC)2";

    // enrollment Activation Code, Patient information, session
    //"64F312AB", "John Smith", "2015-12-10", "Blue Mountain", "blue1234"
    private static final String mDefaultActiveCode = "1234";
    private static final String mDefaultSessionID = "64F312AB";
    private static final String mDefaultPatientName = "John Smith";
    private static final String mDefaultEnrollDate = "2015-12-10";
    private static final String mDefaultEnrollFacility = "Blue Mountain";
    private static final String mDefaultEnrollFacilityID = "blue1234";
    //"94F31832", "Jane Doe", "2015-12-10", "Green Sky", "green1234"
    private static final String mSecondActiveCode = "4567";
    private static final String mSecondSessionID = "94F31832";
    private static final String mSecondPatientName = "Jane Doe";
    private static final String mSecondEnrollDate = "2015-12-10";
    private static final String mSecondEnrollFacility = "Green Sky";
    private static final String mSecondEnrollFacilityID = "green1234";

    // fail case test data for activation code
    private static final String mFailActiveCode1 = "abce";
    private static final String mFailActiveCode2 = "34wser";
    private static final String mFailActiveCode3 = "qaW34$323232323";
    private static final String mFailActiveCode4 = "qaW34$32323#der?>2323";

    // facility name
    private static final String mDefaultFacilityName = "Blue Mountain";
    private static final String mRedRiverFacilityName = "Red River";
    private static final String mGreenSkyFacilityName = "Green Sky";
    private static final String mNoneFacilityName = "NONE";
    // admin


    // maxi fail try time
    private static final int mMaxiTryTime = 3;

    /////////////////////////////////////////////////////////////
    // test cmd
    /////////////////////////////////////////////////////////////
    private static String mTestCmdList[] = {"WaitingPage",
            "Waiting",
            "ClickButton",
            "EnterContent",
            "SelectItem",
            "CheckContent",
            "CheckPage"};
    private static String mLogPrefix = "UITest:";
    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class, false, false) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
        }


    };
    //////////////////////////////////////////////////////////////
    // Simple Test Framework
    //////////////////////////////////////////////////////////////
    private UITestSimpleFramework mTestFramework;
    private Context mContext;

    /**
     * @return start {@link Intent} for the simple web form URL.
     */
    private static Intent withWebFormIntent() {
        //Intent basicFormIntent = new Intent();
        //basicFormIntent.putExtra("KEY_URL_TO_LOAD", "file:///android_asset/micor_html/index.html");
        //return basicFormIntent;
        return null;
    }

    /////////////////////////////////////////////////////////////////////
    // test case
    /////////////////////////////////////////////////////////////////////

    /**
     * select the first available BT item from list
     */
    public static String SelectAvailableBT() {

        String selectBTName = "";

        String searchBTNameList[];

        searchBTNameList = createBTNameList(20);

        boolean found_bt = false;

        for (String currBTName : searchBTNameList) {

            found_bt = selectOneBT(currBTName);

            if (found_bt) {
                selectBTName = currBTName;
                break;
            }

        }

        return selectBTName;
    }

    /**
     * select BT
     */
    public static boolean selectOneBT(String BTName) {
        boolean rc = true;

        Atom bt_btn;
        Web.WebInteraction btn_list_element = onWebView().withElement(findElement(Locator.ID, "ecg-device-list-at-facility"));

        try {
            if (btn_list_element != null) {
                bt_btn = findElement(Locator.NAME, BTName);
                if (bt_btn != null) {
                    Web.WebInteraction btn_bt = onWebView().withElement(bt_btn);
                    if (btn_bt != null) {
                        btn_bt.reset();
                        btn_bt.perform(webClick());
                        rc = true;
                    } else
                        rc = false;
                } else
                    rc = false;

            } else
                rc = false;
        } catch (Exception e) {
            rc = false;
        }


        return rc;
    }

    /**
     * enter content
     */
    public static boolean enterContentByID(String ElementID, String Content) {

        boolean rc = false;

        Web.WebInteraction activation_txtEdit = onWebView().withElement(findElement(Locator.ID, ElementID));
        if (activation_txtEdit != null) {
            activation_txtEdit.perform(DriverAtoms.webKeys(Content));
            rc = true;
        } else
            rc = false;

        return rc;
    }

    /**
     * click button
     */
    public static boolean clickBtnByName(String Btn) {
        boolean rc = false;

        Web.WebInteraction activation_confirm_btn = onWebView().withElement(findElement(Locator.NAME, Btn));
        if (activation_confirm_btn != null) {
            activation_confirm_btn.perform(DriverAtoms.webClick());
            rc = true;
        } else
            rc = false;

        return rc;
    }

    /**
     * click button
     */
    public static boolean clickBtnByID(String BtnID) {
        boolean rc = false;

        Web.WebInteraction activation_confirm_btn = onWebView().withElement(findElement(Locator.ID, BtnID));

        try {
            activation_confirm_btn = onWebView().withElement(findElement(Locator.ID, BtnID));

            if (activation_confirm_btn != null) {
                activation_confirm_btn.perform(DriverAtoms.webClick());
                rc = true;
            } else
                rc = false;
        } catch (Exception e) {
            rc = false;
            System.out.print(e.getMessage());
        }

        return rc;
    }

    /**
     * click button
     */
    public static boolean clickActivationCodeBtn() {
        boolean rc = false;

        Web.WebInteraction activation_confirm_btn = onWebView().withElement(findElement(Locator.ID, "enrollment-by-activation-code-btn"));

        if (activation_confirm_btn != null) {
            activation_confirm_btn.perform(DriverAtoms.webClick());
            rc = true;
        } else
            rc = false;

        return rc;
    }

    /**
     * click restart button
     */
    public static void clickRestartBtn() {
        Web.WebInteraction restart_link = onWebView().withElement(findElement(Locator.LINK_TEXT, "reset"));
        if (restart_link != null) {
            restart_link.perform(webClick());
        }
    }

    /**
     * try to call simple javascript
     * status: fail
     */
    public static Atom<Evaluation> JavaScriptAtom() {
        StringBuilder js = new StringBuilder("function() {return function() {a = 123;} }");
        return new SimpleAtom(js.toString()) {
            @Override
            public void handleNoElementReference() {
                throw new RuntimeException("webJavaScript: Need an function!");
            }
        };
    }

    /*
    * waiting for certain element by ID
     */
    public static boolean waitLoadingByID(String ElementID, long Timeout) {
        boolean rc = false;

        long curr_time = 0;

        try {
            while (curr_time < Timeout) {

                Atom found_element = findElement(Locator.ID, ElementID);

                if (found_element != null) {
                    Web.WebInteraction ew = onWebView().withElement(found_element).perform(getText());

                    if (ew != null) {
                        rc = true;
                        break;
                    }
                }
                Thread.sleep(mWaitingTime);

                curr_time += mWaitingTime;
            }
        } catch (Exception e) {
            System.out.println("Something wrong at waiting for element");
            rc = false;
        }


        return rc;
    }

    /*
    * waiting for certain element by name
     */
    public static boolean waitLoadingByName(String ElementName, long Timeout) {
        boolean rc = false;

        long curr_time = 0;

        try {
            while (curr_time < Timeout) {

                Atom head_text_element = findElement(Locator.TAG_NAME, "h1");

                Object h1_content;


                if (head_text_element != null) {
                    Web.WebInteraction ew = onWebView().withElement(findElement(Locator.TAG_NAME, "h1")).perform(getText());
                    h1_content = ew.get();
                    String h1_str = h1_content.toString();

                    if (h1_str.equalsIgnoreCase(ElementName)) {
                        rc = true;
                        break;
                    }
                }
                Thread.sleep(mWaitingTime);

                curr_time += mWaitingTime;
            }
        } catch (Exception e) {
            System.out.println("Something wrong at waiting for element");
            rc = false;
        }


        return rc;
    }

    /**
     * sleep Timeout
     *
     * @param Timeout
     */
    public static boolean waittime(long Timeout) {
        boolean rc = true;

        try {
            Thread.sleep(Timeout);

        } catch (Exception e) {
            rc = false;
            System.out.println(mLogPrefix + "Exception:" + "Something wrong at waiting for element");

        }

        return rc;
    }

    /**
     * create bluetooth name list
     *
     * @param MaxNum
     * @return
     */
    public static final String[] createBTNameList(int MaxNum) {
        //String searchBTNameList[] = {"STL ECG 1","STL ECG 2","STL ECG 3","STL ECG 7","STL ECG 10","STL ECG 19"};
        String bt_name_prefix = "STL ECG ";
        List<String> bt_name_list = new ArrayList<>();
        String curr_name = "";
        for (int i = 0; i <= MaxNum; ++i) {
            curr_name = bt_name_prefix + Integer.toString(i);
            bt_name_list.add(curr_name);
        }

        return bt_name_list.toArray(new String[bt_name_list.size()]);

    }

    /**
     * test code
     */
    public static void testCode() {
        //mActivityRule.launchActivity(withWebFormIntent());
        int i;
        i = 1;
        i = 2;

        try {

            //clickRestartBtn();
            //javascript:alert("start test!");
            //javascript:document.getElementById('username-field').value = 'test@sf.com’;
            //onData(is(instanceOf(button.class))).atPosition(0).perform(webClick());
            //onWebView().perform(JavaScriptAtom());
            Matcher bt_matcher = DomMatchers.withTextContent("STL ECG");

            List<IdlingResource> idlingResources = Espresso.getIdlingResources();
            for (IdlingResource resource : idlingResources) {
                Espresso.unregisterIdlingResources(resource);
            }

            Atom head_text_element = findElement(Locator.TAG_NAME, "h1");
            String head_script = head_text_element.getScript();

            Class h1_class;

            if (head_text_element != null) {
                h1_class = head_text_element.getClass();
            }
            //onWebView().check(webMatches(getText(), containsString("Choose ECG (Clean Setup)")));
            Web.WebInteraction btn_list_element = onWebView().withElement(findElement(Locator.ID, "ecg-device-list-at-facility"));
            Object btn_script = btn_list_element.get();

            Atom bt_list_elem = findElement(Locator.ID, "ecg-device-list-at-facility");

            Atom btn_atom = findMultipleElements(Locator.PARTIAL_LINK_TEXT, "STL ECG");

            //List<> btn_list = btn_atom.getArguments();

            if (btn_list_element != null) {
                //Web.WebInteraction btn_bt = btn_list_element.withContextualElement(findElement(Locator.NAME, "STL ECG 7"));
                Web.WebInteraction btn_bt = onWebView().withElement(findElement(Locator.NAME, "STL ECG 7"));
                if (btn_bt != null) {
                    btn_bt.perform(webClick());
                } else {
                    btn_bt = onWebView().withContextualElement(findElement(Locator.NAME, "STL ECG 1"));
                    if (btn_bt != null) {
                        btn_bt.perform(webClick());
                    }
                }

            }

            //onWebView().withElement(findElement(Locator.ID,"ecg-device-list-at-facility")).perform(webClick());
            //Web.WebInteraction btn_list_element = onWebView().withElement(findElement(Locator.ID, "ecg-device-list-at-facility"));
            //Web.WebInteraction btn_bt = btn_list_element.withContextualElement(findElement(Locator.PARTIAL_LINK_TEXT, "STL ECG"));
            //btn_bt.perform(webClick());
            //onWebView().withElement(findElement(Locator.NAME,"STL ECG 1")).perform(webClick());
            //Web.WebInteraction one_view = onWebView(ViewMatchers.withText(startsWith("STL ECG 10")));
            //Matchers.allOf(ViewMatchers.withText("7"), ViewMatchers.hasSibling(ViewMatchers.withText("item: 0")));
            //onWebView(ViewMatchers.withText(startsWith("STL ECG 7"))).perform(webClick());
            //one_view.perform(webClick());
        } catch (NoMatchingViewException e) {

        }
        i = 10;
    }

    /**
     * Dump Test Commands information
     *
     * @param CmdData
     * @return
     */
    public static boolean dumpJasonCmdData(String CmdData) {
        boolean rc = false;


        String json_test_data = "{" +
                "\"Command\" :[" +
                "{" +
                "\"Action\":\"WaitingPage\"," +
                "\"PageID\":\"Choose ECG Device\"," +
                "\"Timeout\":\"3000\"" +
                "}," +
                "{" +
                "\"Action\":\"SelectItem\"," +
                "\"SelectItem\":\"STL ECG1\"" +
                "}," +
                "{" +
                "\"Action\":\"ClickButton\"," +
                "\"buttonID\":\"manual-enrollment-lookup-btn\"" +
                "}," +
                "{" +
                "\"Action\":\"Waiting\"," +
                "\"Timeout\":\"3000\"" +
                "}," +
                "{" +
                "\"Action\":\"ClickButton\"," +
                "\"buttonID\":\"reset_btn\"" +
                "}," +
                "]" +
                "}";

        Web.WebInteraction edit_element;

        if (!CmdData.isEmpty()) {
            json_test_data = CmdData;
        }

        Iterator<String> keys;
        String key;
        String value;

        try {
            JSONObject jsonRootObject = new JSONObject(json_test_data);

            //Get the instance of JSONArray that contains JSONObjects
            JSONArray jsonCmds = jsonRootObject.optJSONArray("Command");

            //Iterate the jsonArray and print the info of JSONObjects
            System.out.println("////Start Dump Json Command//////");
            for (int i = 0; i < jsonCmds.length(); i++) {

                JSONObject jsonCmd = jsonCmds.getJSONObject(i);
                keys = jsonCmd.keys();

                while (keys.hasNext()) {
                    key = keys.next();
                    value = jsonCmd.optString(key);
                    System.out.println(key + ":" + value);
                }
                //System.out.println(jsonCmd.optString("Action"));
                System.out.println("---");

            }
            System.out.println("////End Dump Json Command//////");

        } catch (NoMatchingViewException ne) {
            System.out.print(mLogPrefix + ne.getMessage());
        } catch (Exception e) {
            System.out.print(mLogPrefix + e.getMessage());
        }

        return rc;
    }


    /////////////////////////////////////////////
    // support function
    /////////////////////////////////////////////

    /**
     * happy path for activation session
     * start page : select bluetooth device
     * end page: report patient event
     * <p/>
     * precondition:
     * load select bluetooth device page
     * activation code
     * <p/>
     * Test Case ID : UTMAID110001
     */
    @Test
    public void SuccessSessionActivation() {

        String test_case_data_file = "td_normal_activation_session.json";

        getTestFramework(); // launch the activity and get test framework

        //ThreadUtility.applicationSleep(3000);
        //String devicelist = UIWebCmdUtility.logPageVisualText("ecg-device-list-at-facility-page");
        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();

    }

    /**
     * enter invalid activation code
     * start page : select bluetooth device
     * end page: enter activation code page
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid activation code
     * <p/>
     * Test Case ID : UTMAID110002
     */
    @Test
    public void EnterInvalidActivationCode() {

        String test_case_data_file = "td_invalid_activation_code.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * enter blank activation code
     * start page : select bluetooth device
     * end page: enter activation code page
     * <p/>
     * precondition:
     * load select bluetooth device page
     * <p/>
     * <p/>
     * Test Case ID : UTMAID110003
     */
    @Test
    public void EnterBlankActivationCode() {

        String test_case_data_file = "td_blank_activation_code.json";

        getTestFramework(); // launch the activity and get test framework

        // waiting application launch and start service
        //
        UIWebCmdUtility.waitTime(3000);

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(withBTDeviceIntent());

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * success path for MANUAL ENROLLMENT session
     * <p/>
     * start page : select bluetooth device
     * end page: report patient event
     * <p/>
     * precondition:
     * load select bluetooth device page
     * facility code
     * <p/>
     * Test Case ID : UTMAID110004
     */
    @Test
    public void SuccessManualEnrollment() {
        String test_case_data_file = "td_manual_enrollment.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();

    }

    /**
     * enter blank facility code
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID110005
     */
    @Test
    public void EnterBlankFacilityCode() {
        String test_case_data_file = "td_blank_facility_code.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * enter invalid facility code
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID110006
     */
    @Test
    public void EnterInvalidFacilityCode() {

        String test_case_data_file = "td_invalid_facility_code.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * test reset button
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID110007
     */
    @Test
    public void ClickResetButton() {

        String test_case_data_file = "td_reset_btn.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(test_case_data_file);

        //UITestCommand.clickResetBtn();
    }

    /**
     * enter blank facility code
     * test load json test command json file from assets/sessionactivation folder
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID110008
     */
    @Test
    public void TestLoadJSonTestCmd() {

        String td_blank_facility_code = "td_blank_facility_code.json";

        getTestFramework(); // launch the activity and get test framework

        try {

            mTestFramework.runTestFile(td_blank_facility_code);
        } catch (NoMatchingViewException ne) {
            System.out.print(ne.getMessage());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        } finally {

        }

        UITestCommand.clickResetBtn();
    }

    /**
     * change enrollment data
     * test load json test command json file from assets/sessionactivation folder
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID110009
     */
    @Test
    public void ChangeEnrollmentDate() {

        String test_case_data_file = "td_change_enrollment_date.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * change enrollment data
     * test load json test command json file from assets/sessionactivation folder
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID110010
     */
    @Test
    public void CheckBackBtnLabelEnrollmentDateSelector() {

        String test_case_data_file = "td_back_btn_enrollment_date_selector.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * confirm facility no button
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID110011
     */
    @Test
    public void ClickNoBtnConfirmFacility() {

        String test_case_data_file = "td_no_btn_confirm_facility.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * enter blue prefix facility code
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID110012
     */
    @Test
    public void EnterBluePrefixFacilityCode() {
        String test_case_data_file = "td_blue_prefix_facility_code.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * enter random mock facility code
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID110013
     */
    @Test
    public void EnterRandomFacilityCode() {

        List<UITestCommand> test_case_data_file = UIMockData.mockFacilityCodes(3);

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTest(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * click refresh button
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID110014
     */
    @Test
    public void ClickRefreshButton() {

        String test_case_data_file = "td_refresh_ecg_list_btn.json";

        getTestFramework(); // launch the activity and get test framework

        mTestFramework.runTestFile(test_case_data_file);

        UITestCommand.clickResetBtn();
    }

    /**
     * launch activity and get current test framework
     *
     * @return
     */
    public UITestSimpleFramework getTestFramework() {

        //UITestSimpleFramework framework;

        mActivityRule.launchActivity(null); // launch activity

        AMainActivity activity = mActivityRule.getActivity();

        // set context

        mContext = activity.getApplicationContext();  // context for activity under test
        //Context context = InstrumentationRegistry.getContext().getApplicationContext();
        //mContext = InstrumentationRegistry.getTargetContext();  // context for activity under test



        if (mTestFramework == null) {
            mTestFramework = new UITestSimpleFramework();
        }
        mTestFramework.setContext(mContext);

        return mTestFramework;
    }

    private static Intent withBTDeviceIntent(){
        String devicesFound = "" +
                "[" +
                "{'btMac':'B0:B4:48:C4:A9:89','btName':'STL ECG 210','rssi':-38}," +
                "{'btMac':'B0:B4:48:C4:F9:89','btName':'STL ECG 517','rssi':-38}," +
                "{'btMac':'B0:B4:48:D4:A9:A4','btName':'STL ECG 320','rssi':-38}" +
                "]";

        ArrayList<StlDeviceInfo> stlDeviceInfos = ObjectUtils.fromJson(devicesFound, ArrayList.class);

        Bundle b = new Bundle();
        b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICES_FOUND);
        b.putString("stlDevicesFoundJson", ObjectUtils.toJson(stlDeviceInfos));
        b.putString("callbackFnString", "allLeDevicesFound");

        Intent i = new Intent("BLUETOOTH_EVENT");
        i.putExtras(b);

        return i;
    }
}
