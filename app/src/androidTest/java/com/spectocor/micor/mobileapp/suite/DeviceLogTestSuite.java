package com.spectocor.micor.mobileapp.suite;

import com.spectocor.micor.mobileapp.devicelogs.ApplicationLogServiceTest;
import com.spectocor.micor.mobileapp.devicelogs.BatteryLogServiceTest;
import com.spectocor.micor.mobileapp.devicelogs.DiskSpaceCalculatorTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by qwang on 4/13/2016.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                ApplicationLogServiceTest.class,
                BatteryLogServiceTest.class,
                DiskSpaceCalculatorTest.class
        }
)
public class DeviceLogTestSuite {
}
