package com.spectocor.micor.mobileapp.patientreportedevents;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by qwang on 12/29/2015.
 * Test case for patient report event
 */
@RunWith(AndroidJUnit4.class)
public class PatientReportedEventTest {
    public static final int mDefaultSessionId = 0;
    public static final EnumPatientReportedEventType mDefaultPatientReportedEventTypeId = null;
    public static final long mDefaultDatabaseRowId = 0;
    public static final String mDefaultComment = null;

    public static final int mTestMonitoringSessionId = 10;
    public static final EnumPatientReportedEventType mTestPatientReportedChestDiscomfort = EnumPatientReportedEventType.ChestDiscomfort;
    public static final String mChestDiscomfortComment = "ChestDiscomfort";

    public static final int mUpdateMonitoringSessionId = 20;
    public static final EnumPatientReportedEventType mUpdatePatientReportedFluttering = EnumPatientReportedEventType.Fluttering;
    public static final String mUpdateComment = "Fluttering";

    private PatientReportedEvent mPatientReportedEvent;

    public PatientReportedEventTest() {
        super();
    }

    // get current log time
    public static int getCurrentTime() {
        int curr_time;
        curr_time = (int) (System.currentTimeMillis() / 1000);
        return curr_time;
    }

    /*
    * test create blank patient report event log
    * Test Case ID : UTMAID010001
     */
    @Test
    public void createPatientReportedEvent() {
        mPatientReportedEvent = new PatientReportedEvent();   // test default construction function

        assertEquals(mDefaultSessionId, mPatientReportedEvent.getMonitoringSessionId());
        assertTrue(getCurrentTime() >= mPatientReportedEvent.getLogDateUnixSec());
        assertEquals(mDefaultPatientReportedEventTypeId, mPatientReportedEvent.getPatientReportedEventTypeId());
        assertEquals(mDefaultComment, mPatientReportedEvent.getComment());
    }

    /*
    * test create ChestDiscomfort
    * Test Case ID : UTMAID010002
    */
    @Test
    public void createChestDiscomfortEvent() {
        mPatientReportedEvent = new PatientReportedEvent(mTestMonitoringSessionId, mTestPatientReportedChestDiscomfort);   // test ChestDiscomfort event

        assertEquals(mTestMonitoringSessionId, mPatientReportedEvent.getMonitoringSessionId());
        assertTrue(getCurrentTime() >= mPatientReportedEvent.getLogDateUnixSec());
        assertEquals(mTestPatientReportedChestDiscomfort, mPatientReportedEvent.getPatientReportedEventTypeId());
        assertEquals(mDefaultComment, mPatientReportedEvent.getComment());

    }

    /*
    * test create ChestDiscomfort with comment
    * Test Case ID : UTMAID010003
    */
    @Test
    public void createChestDiscomfortEventWithComment() {
        mPatientReportedEvent = new PatientReportedEvent(mTestMonitoringSessionId, mTestPatientReportedChestDiscomfort, mChestDiscomfortComment);   // test ChestDiscomfort event

        assertEquals(mTestMonitoringSessionId, mPatientReportedEvent.getMonitoringSessionId());
        assertTrue(getCurrentTime() >= mPatientReportedEvent.getLogDateUnixSec());
        assertEquals(mTestPatientReportedChestDiscomfort, mPatientReportedEvent.getPatientReportedEventTypeId());
        assertEquals(mChestDiscomfortComment, mPatientReportedEvent.getComment());
    }

    /*
    * test update session id,event,report time and comment
    * Test Case ID : UTMAID010004
    */
    @Test
    public void createPatientReportEventWithNumberString() {
        mPatientReportedEvent = new PatientReportedEvent(mTestMonitoringSessionId, mTestPatientReportedChestDiscomfort, mChestDiscomfortComment);   // test ChestDiscomfort event

        mPatientReportedEvent.setMonitoringSessionId(mUpdateMonitoringSessionId);
        mPatientReportedEvent.setLogDateUnixSec(getCurrentTime());
        mPatientReportedEvent.setPatientReportedEventTypeId(mUpdatePatientReportedFluttering);
        mPatientReportedEvent.setComment(mUpdateComment);

        assertEquals(mUpdateMonitoringSessionId, mPatientReportedEvent.getMonitoringSessionId());
        assertTrue(getCurrentTime() >= mPatientReportedEvent.getLogDateUnixSec());
        assertEquals(mUpdatePatientReportedFluttering, mPatientReportedEvent.getPatientReportedEventTypeId());
        assertEquals(mUpdateComment, mPatientReportedEvent.getComment());
    }
}
