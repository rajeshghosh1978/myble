package com.spectocor.micor.mobileapp.sessionactivation;

import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.common.exceptions.UserException;
import com.spectocor.micor.mobileapp.common.exceptions.WebApiCallException;
import com.spectocor.micor.mobileapp.common.queuesystem.HttpsHandler;
import com.spectocor.micor.mobileapp.sessionactivation.api.EnrollmentInfo;
import com.spectocor.micor.mobileapp.sessionactivation.api.SessionActivationApiOld;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;

import junit.framework.Assert;

import org.junit.Before;

/**
 * Test cases for SessionActivationApiOld class
 */
public class SessionActivationApiTest extends AndroidTestCase {


    /**
     * http handler for testing
     */
    private HttpsHandler handler;
    private SessionActivationApiOld api;


    /**
     * Creates required objects for testing
     *
     * @throws Exception if it couldn't create files
     */
    @Before
    public void setUp() throws Exception {
        GlobalSettings settings = new GlobalSettings();
        handler = new HttpsHandler(settings);
        api = new SessionActivationApiOld(handler);
    }


    /**
     * tests activation information api Happy Scenario
     *
     * @throws Exception if test ended with exception
     */
    public void testGetEnrollmentByActivationCode() throws Exception {
        EnrollmentInfo enrollmentInfo = api.getEnrollmentByActivationCode("111111");
        assertEquals(1, enrollmentInfo.getSessionId());
        assertEquals("Mohammad Ali", enrollmentInfo.getFirstName());
        assertEquals("Ghaderi", enrollmentInfo.getLastName());
        assertEquals("My Facility 1000", enrollmentInfo.getFacilityName());
        assertEquals("2015-12-12", enrollmentInfo.getEnrollmentDateIso8601());
        assertEquals(1438378779000l, enrollmentInfo.getSessionEndTimeUnixMs());
    }


    /**
     * tests activation information api with invalid activation code
     *
     * @throws Exception if test ended with exception
     */
    public void testGetEnrollmentByActivationCode2() throws Exception {
        try {
            String activationCode = "invalidActivationCode";
            api.getEnrollmentByActivationCode(activationCode);
            Assert.fail(); // it should return user exception
        } catch (UserException ex) {
            assertEquals("Invalid activation code. Please re-enter your activation code.", ex.getMessage());
        }
    }


    /**
     * tests activation information api unregistered device
     *
     * @throws Exception if test ended with exception
     */
    public void testGetEnrollmentByActivationCode3() throws Exception {
        try {
            String activationCode = "222222";
            api.getEnrollmentByActivationCode(activationCode);
            Assert.fail(); // it should return user exception
        } catch (UserException ex) {
            assertEquals("Device not found. This device may not be registered with our system.", ex.getMessage());
        }
    }


    /**
     * tests GetFacilityNameByFacilityCode Happy Scenario
     *
     * @throws Exception if test ended with exception
     */
    public void testGetFacilityNameByFacilityCode() throws Exception {
        String facilityCode = "F1000";
        //String mobileId = "some device code";
        String facilityName = api.getFacilityNameByFacilityCode(facilityCode);
        assertEquals(facilityName, "F1000 Name");
    }


    /**
     * tests GetFacilityNameByFacilityCode with incorrect facility code
     *
     * @throws Exception if test ended with exception
     */
    public void testGetFacilityNameByFacilityCode2() throws Exception {
        String facilityCode = "invalid facility code";
        //String mobileId = "some device code";
        try {
            api.getFacilityNameByFacilityCode(facilityCode);
            Assert.fail();
        } catch (UserException ex) {
            assertEquals("Facility not found. Please re-enter your facility code.", ex.getMessage());
        }
    }


    /**
     * tests GetListOfEnrolledPatients Happy Scenario
     *
     * @throws Exception if test ended with exception
     */
    public void testGetListOfEnrolledPatients() throws Exception {
        String facilityCode = "F1000";
        String enrollmentDate = "2015-12-12";

        EnrollmentInfo[] enrollments = api.getListOfEnrolledPatients(enrollmentDate, facilityCode);
        assertEquals(2, enrollments.length);


        EnrollmentInfo enrollmentInfo = enrollments[0];
        assertEquals(1, enrollmentInfo.getSessionId());
        assertEquals("Mohammad Ali", enrollmentInfo.getFirstName());
        assertEquals("Ghaderi", enrollmentInfo.getLastName());
        assertEquals("My Facility 1000", enrollmentInfo.getFacilityName());
        assertEquals("2015-12-12", enrollmentInfo.getEnrollmentDateIso8601());
        assertEquals(1438378779000l, enrollmentInfo.getSessionEndTimeUnixMs());


        EnrollmentInfo enrollmentInfo1 = enrollments[1];
        assertEquals(2, enrollmentInfo1.getSessionId());
        assertEquals("Qian", enrollmentInfo1.getFirstName());
        assertEquals("Wang", enrollmentInfo1.getLastName());
        assertEquals("My Facility 1000", enrollmentInfo1.getFacilityName());
        assertEquals("2015-12-12", enrollmentInfo1.getEnrollmentDateIso8601());
        assertEquals(1438378789000l, enrollmentInfo1.getSessionEndTimeUnixMs());
    }


    /**
     * tests GetListOfEnrolledPatients invalid facility code
     *
     * @throws Exception if test ended with exception
     */
    public void testGetListOfEnrolledPatients2() throws Exception {

        String facilityCode = "invalid facility code";
        String enrollmentDate = "2015-12-12";

        try {
            api.getListOfEnrolledPatients(enrollmentDate, facilityCode);
            Assert.fail();
        } catch (UserException ex) {
            assertEquals("Facility not found:" + facilityCode, ex.getMessage());
        }

    }

    /**
     * tests GetListOfEnrolledPatients invalid date
     *
     * @throws Exception if test ended with exception
     */
    public void testGetListOfEnrolledPatients3() throws Exception {

        String facilityCode = "F1000";
        String enrollmentDate = "2015-12-23"; // date with invalid date

        try {
            api.getListOfEnrolledPatients(enrollmentDate, facilityCode);
            Assert.fail();
        } catch (UserException ex) {
            assertEquals("No enrollment found for this date:" + enrollmentDate, ex.getMessage());
        }

    }


    /**
     * tests ActivateSession Happy Scenario
     *
     * @throws Exception if test ended with exception
     */
    public void testActivateSession() throws Exception {
        int sessionId = 1;
        long time = System.currentTimeMillis();
        String ecgBluetoothName = "Bluetooth Name";
        String ecgBluetoothMac = "Bluetooth Mac";
        //String mobileId = "mobileId";
        String facilityCode = "F1000";
        String success = api.activateSession(sessionId, time, ecgBluetoothName, ecgBluetoothMac, facilityCode);
        assertEquals("Activation Successful!", success);
    }


    /**
     * tests ActivateSession with invalid sessionid
     *
     * @throws Exception if test ended with exception
     */
    public void testActivateSession2() throws Exception {
        int sessionId = 100000; // invalid session id
        long time = System.currentTimeMillis();
        String ecgBluetoothName = "Bluetooth Name";
        String ecgBluetoothMac = "Bluetooth Mac";
        //String mobileId = "mobileId";
        String facilityCode = "F1000";

        try {
            api.activateSession(sessionId, time, ecgBluetoothName, ecgBluetoothMac, facilityCode);
            Assert.fail();
        } catch (UserException ex) {
            assertEquals("Session Id not found.", ex.getMessage());
        }
    }


    /**
     * tests ActivateSession with invalid sessionid
     *
     * @throws Exception if test ended with exception
     */
    public void testActivateSession3() throws Exception {
        int sessionId = 2; // invalid session id
        long time = System.currentTimeMillis();
        String ecgBluetoothName = "Bluetooth Name";
        String ecgBluetoothMac = "Bluetooth Mac";
        //String mobileId = "mobileId";
        String facilityCode = "invalid facility";

        try {
            api.activateSession(sessionId, time, ecgBluetoothName, ecgBluetoothMac, facilityCode);
            Assert.fail();
        } catch (UserException ex) {
            assertEquals("Activation failed! Please contact the customer service.", ex.getMessage());
        } catch (WebApiCallException we) {
            assertTrue("Pass Web Api Call exception.", true);
        }

    }

}