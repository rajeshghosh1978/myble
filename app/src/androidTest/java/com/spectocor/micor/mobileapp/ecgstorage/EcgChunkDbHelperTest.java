package com.spectocor.micor.mobileapp.ecgstorage;

import android.content.Context;
import android.os.Environment;
import android.test.AndroidTestCase;
import android.test.UiThreadTest;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.DbHelper;
import com.spectocor.micor.mobileapp.amain.StorageAndDbInfo;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.sql.SQLDataException;

//import static android.support.test.InstrumentationRegistry.getTargetContext;

/**
 * Created by qwang on 1/26/2016.
 * test case for storage ECG chunk data at external SD card
 */
public class EcgChunkDbHelperTest extends AndroidTestCase{

    public static final String TAG = "EcgChunkDbHelperTest";

    public static final String SD_DB_PATH ="/storage/ext_sd/Android/data/com.spectocor.micor.mobileapp/files/databases/";  // htc desire 626 SD card data path
    //public static final String SD_DB_PATH = "/storage/emulated/0/Android/data/com.spectocor.micor.mobileapp/files/databases/";

    private EcgChunkDbHelper mEcgChunkDb; // reference for EcgChunkDbHelper


    private String mDBPath; // current database path

    private String mEcgChunkDbName; // chunk database name

    private String mEcgChunkTableName; // chunk table name

    private Context mContext; // android application context


    // define test data

    private final int mDefaultSessionID = 1000;
    private byte[] mDefaultEcgData;
    private long mDefaultChunkTimeStamp;
    private EcgDataChunk mDefaultEcgDataChunk;
    private long mTimeStamp;

    // end

    @BeforeClass
    public void setUp() throws Exception{

        super.setUp(); // extra testAndroidTestCaseSetupProperly to check context
                       // http://stackoverflow.com/questions/29970914/why-does-testandroidtestcasesetupproperly-take-so-long-to-run

        mDBPath = SD_DB_PATH;

        mEcgChunkDbName = "MicorDatabase.db";

        mEcgChunkTableName = "ECG_CHUNKS";

        mContext = getContext();

        mContext.deleteDatabase(mEcgChunkDbName);  // delete database

        //EcgChunkDbHelper.dbPath = SD_DB_PATH;   // set db path

        //mEcgChunkDb = new EcgChunkDbHelper(mContext);        // create DB object at SD card

        mEcgChunkDb = EcgChunkDbHelper.getInstance(mContext);  // create DB object at SD card

        boolean rc = isExternalStoragePresent();

        mDefaultEcgDataChunk = createDefaultEcgDataChunk(); // create default EcgDataChunk

    }

    @AfterClass
    public void tearDown() throws Exception{
        if(mEcgChunkDb != null) {
            //mEcgChunkDb.close();
            DbHelper.getInstance(mContext).close();
            //exportDB();
        }
        super.tearDown();
    }

    ///////////////////////////////////////////////
    // test case
    ///////////////////////////////////////////////
    /*
    * test create ecg data chunk DB at SD card
    * Test Case ID : UTMAID030001
    */
    @Test
    public void testStoreSDDB(){

        //File current_db_file = mContext.getDatabasePath(mEcgChunkDbName);

        //File current_db_file =  EcgChunkDbHelper.micorEcgDbContext.getDatabasePath(mEcgChunkDbName); //  have to use wrap context to obtain database path

        String db_file_name = StorageAndDbInfo.getDbNameAndPathFromState(mContext); //EcgChunkDbHelper.DATABASE_NAME;

        boolean rc = db_file_name.contains("/storage/ext_sd/Android/data/com.spectocor.micor.mobileapp/files/databases/");
        //String expected_db_file = getExpectedSDDBName(mEcgChunkDbName);

        Log.d(TAG,db_file_name);

        Assert.assertTrue(TAG + ":Fail to test storing ECG chunk data at external SD card", rc);
    }

    /*
    * test insert a new ECG chunk data
    * Test Case ID : UTMAID030002
    */
    @UiThreadTest
    public void testInsertNewECGChunk()throws SQLDataException {


        mEcgChunkDb.insertEcgChunk( mDefaultEcgDataChunk);

        EcgDataChunk realDbChunk = mEcgChunkDb.retrieveCompressedEcg(mDefaultSessionID, mDefaultChunkTimeStamp);

        assertEquals(realDbChunk.monitoringSessionId, mDefaultSessionID);

    }

    /*
    * test insert Duplicate a ECG chunk data
    * Test Case ID : UTMAID030003
    */
    @UiThreadTest
    public void testInsertDuplicateECGChunk() {


        try{

            mEcgChunkDb.insertEcgChunk( mDefaultEcgDataChunk);

            mEcgChunkDb.insertEcgChunk( mDefaultEcgDataChunk);

            Assert.assertTrue(TAG + ":Fail to raise exception when inserting a duplicate ECG data", false);


        }catch (SQLDataException e){

            Assert.assertTrue(true);
        }


    }

    /*
    * test insert two ECG chunk data and retrieve that two ECG chunk
    *
    * Test Case ID : UTMAID030004
    */
    @UiThreadTest
    public void testInsertTwoECGChunk() {


        try{

            EcgDataChunk firstChunk = createDefaultEcgDataChunk();

            mEcgChunkDb.insertEcgChunk(firstChunk);

            EcgDataChunk secondChunk = createDefaultEcgDataChunk();

            mEcgChunkDb.insertEcgChunk(secondChunk);

            EcgDataChunk realFirstChunk = mEcgChunkDb.retrieveCompressedEcg(mDefaultSessionID, firstChunk.getChunkStartDateUnixMs());

            EcgDataChunk realSecondChunk = mEcgChunkDb.retrieveCompressedEcg(mDefaultSessionID, secondChunk.getChunkStartDateUnixMs());

            assertEquals(realFirstChunk.monitoringSessionId, firstChunk.getMonitoringSessionId());

            assertEquals(realSecondChunk.monitoringSessionId, secondChunk.getMonitoringSessionId());


        }catch (SQLDataException e){

            Assert.assertTrue(true);
        }


    }


    ///////////////////////////////////////////////
    // support function
    ///////////////////////////////////////////////

    // create default ECG chunk package
    public EcgDataChunk createDefaultEcgDataChunk(){

        EcgDataChunk chunk;

        chunk = new EcgDataChunk();

        chunk.monitoringSessionId = mDefaultSessionID;
        chunk.chunkStartDateUnixMs = getCurrentTime();
        chunk.insertTimestamp =  System.currentTimeMillis();
        chunk.timezoneId =  "UNKNOWN";
        chunk.timezoneOffsetMillis = (long)0;
        chunk.chunkDataChannelCompressed = new byte[]{1,2,3,4};

        mDefaultChunkTimeStamp = chunk.chunkStartDateUnixMs;

        return chunk;
    }

    // get current log time
    public static long getCurrentTime() {
        long curr_time;
        curr_time = System.currentTimeMillis();
        return curr_time;
    }

    private void exportDB(){

        // check file exist
        String dbName = "db.sqlite";
        String dbPath = Environment.getExternalStorageDirectory().toString();


        //
        File sd = Environment.getExternalStorageDirectory();


        File data = Environment.getDataDirectory();
        FileChannel source=null;
        FileChannel destination=null;
        String currentDBPath = mDBPath + mEcgChunkDbName;
        String backupDBPath = mEcgChunkDbName;
        //File currentDB = new File(data, currentDBPath);
        File currentDB = new File(mDBPath, mEcgChunkDbName);
        File backupDB = new File(sd, backupDBPath);

        String test_folder = "/storage/emulated/0/Android/data";

        File chunk_db = new File(test_folder);
        Assert.assertTrue(chunk_db.exists());
        Assert.assertTrue(sd.exists());
        File[] flist = sd.listFiles();

        Assert.assertTrue(currentDB.exists());
        Assert.assertTrue(currentDB.exists());

        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            Log.i(TAG, "DB Exported!" + currentDBPath + ":" + backupDBPath);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    /*
    * check DB SD path
    */
    private boolean checkDBPath(String DBName,String ExpectedDBName){

        boolean rc = false;

        File current_db_file = mContext.getDatabasePath(DBName);

        if(current_db_file.exists()){
            if(ExpectedDBName.compareTo(current_db_file.toString())==0){
                rc = true;
            }
        }

        return rc;
    }

    /* get SD file path
    *
     */
    private static String getExpectedSDDBName(String DBName){

        String sd_db_absolute_path = SD_DB_PATH + DBName;


        return sd_db_absolute_path;
    }


    private boolean isExternalStoragePresent() {

        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but
            // all we need
            // to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
        if (!((mExternalStorageAvailable) && (mExternalStorageWriteable))) {
            Log.i(TAG, "SD card not present");

        }
        return (mExternalStorageAvailable) && (mExternalStorageWriteable);
    }
}
