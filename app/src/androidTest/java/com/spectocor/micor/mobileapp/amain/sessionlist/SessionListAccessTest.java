package com.spectocor.micor.mobileapp.amain.sessionlist;

import android.content.Context;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.MLog;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *Test class for Session List Accessibility
 * Created by Savio Monteiro on 2/20/2016.
 */
@RunWith(AndroidJUnit4.class)
public class SessionListAccessTest {

    // TODO: create test cases for real modifications of the session list during session activation and termination.

    private static Context mContext;


    private static void log(String str) {
        MLog.log(SessionListAccess.class.getSimpleName() + ": " + str);
    }

    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            mContext  = getActivity().getApplicationContext();
            assert(mContext != null);
        }
    };


    @Test
    public void testAddSessionToList() {

        SessionListSessionInfo sessionInfo = new SessionListSessionInfo(1, SessionTypeEnum.SESSION_TYPE_HOLTER, System.currentTimeMillis(), System.currentTimeMillis() - (5000*60));

        SessionListAccess.addSessionToList(sessionInfo, mContext);
    }

    @Test
    public void testMaintainLastThreeSessions() {
        SessionListAccess.cleanToKeepLastThreeSessions(mContext);
    }

    @Test
    public void testGetCurrentListJson() {
        SessionListAccess.getCurrentSessionList(mContext);
    }

}
