package com.spectocor.micor.mobileapp.common;

import android.content.Context;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.common.exceptions.DataAccessException;
import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.common.queuesystem.QueueItem;
import com.spectocor.micor.mobileapp.common.queuesystem.SendingQueueRepositoryBase;

import junit.framework.Assert;

import java.util.ArrayList;

/**
 * Test class for BaseSendingQueueRepositoryTest
 */
@SuppressWarnings("WeakerAccess")
public class BaseSendingQueueRepositoryTest extends AndroidTestCase {

    /**
     * repository class for testing
     */
    SendingQueueRepositoryBase repository;


    /**
     * creating classes for testing
     *
     * @throws Exception if test ended with exception
     */
    public void setUp() throws Exception {
        Context context = getContext();
        context.deleteDatabase(QueueDatabaseHelper.DatabaseName);

        QueueDatabaseHelper queueDatabaseHelper = new QueueDatabaseHelper(getContext());
        repository = new FakeSendingQueueRepository(queueDatabaseHelper);

        // creating the fake table
        queueDatabaseHelper.getWritableDatabase().execSQL(repository.getCreateTableSqlScript());
    }


    /**
     * testing insert to the database (Happy Scenario)
     *
     * @throws Exception if test ended with exception
     */
    public void testInsert() throws Exception {
       /* QueueItem obj = new QueueItem(System.currentTimeMillis());
        long m = repository.insert(obj);
        Assert.assertTrue(m == obj.getRowId());*/
    }



    /**
     * testing delete from database (Happy scenario)
     * @throws Exception if test ended with exception
     */
    public void testDelete() throws Exception {
     /*   long itemId = 1000;
        QueueItem obj = new QueueItem(itemId);
        long m = repository.insert(obj);
        Assert.assertTrue(m == obj.getRowId());
        repository.delete(itemId); // it should delete it without any error*/
    }


    /**
     * testing delete from database
     * @throws Exception if test ended with exception
     */
    public void testDelete2() throws Exception {
      /*  long itemId = 1001;
        QueueItem obj = new QueueItem(itemId);
        long m = repository.insert(obj);
        Assert.assertTrue(m == obj.getRowId());
        try {
            repository.delete(itemId);
            repository.delete(itemId); // item shouldn't be in repository to be deleted, so it is a failure
            Assert.fail();         // it give any exception
        } catch (DataAccessException ex) {
            assertTrue(ex.getMessage().contains("failed"));
        }*/
    }


    /**
     * testing getting items from repository
     * @throws Exception if test ended with exception
     */
    public void testGetItemsFromQueue() throws Exception {
     /*   long time = System.currentTimeMillis() + 1000;
        repository.insert(new QueueItem(time));
        repository.insert(new QueueItem(time + 1));
        repository.insert(new QueueItem(time + 2));
        repository.insert(new QueueItem(time + 3));
        repository.insert(new QueueItem(time + 4));
        repository.insert(new QueueItem(time + 5));
        repository.insert(new QueueItem(time + 6));
        repository.insert(new QueueItem(time + 7));
        repository.insert(new QueueItem(time + 8));
        repository.insert(new QueueItem(time + 9));
        repository.insert(new QueueItem(time + 10));

        ArrayList<QueueItem> queueItems1 = repository.getAll(5);
        org.junit.Assert.assertEquals(5, queueItems1.size());

        ArrayList<QueueItem> queueItems2 = repository.getAll(10);
        org.junit.Assert.assertEquals(10, queueItems2.size());*/
    }


}