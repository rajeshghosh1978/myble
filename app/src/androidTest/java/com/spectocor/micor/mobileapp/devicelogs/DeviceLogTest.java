package com.spectocor.micor.mobileapp.devicelogs;

import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by qwang on 12/14/2015.
 * The class provides test case for testing DeviceLog module
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class DeviceLogTest {
    public static final int mDefaultSessionId = 0;
    public static final EnumDeviceLogInfoType mefaultDeviceLogInfoTypeId = null;
    public static final long mDefaultDatabaseRowId = 0;
    public static final int mTestMonitoringSessionId = 10;
    public static final EnumDeviceLogInfoType mTestdeviceLogInfoType = EnumDeviceLogInfoType.MobileDeviceBatteryLevel;
    public static final Double mTestbatterydefaultValue = null;
    public static final String mTestbatterydefaultStringValue = null;
    public static final double mTestbattery0 = 0;
    public static final byte mTestbattery20 = 20;
    public static final byte mTestbattery100 = 100;
    public static final byte mTestbattery200 = 127;
    public static final String mTestbatteryString = "battery";

    private DeviceLog mDeviceLog;

    public DeviceLogTest() {
        super();
    }

    // get current log time
    public static int getCurrentTime() {
        int curr_time;
        curr_time = (int) (System.currentTimeMillis() / 1000);
        return curr_time;
    }

    /*
    * test create blank device log
    * Test Case ID : UTMAID000001
     */
    @Test
    public void createBlankDeviceLog() {
        mDeviceLog = new DeviceLog();   // test default construction function
        //assertEquals(mDefaultSessionId, mDeviceLog.getMonitoringSessionId()); // no monitoring session id in the data structure
        assertTrue(getCurrentTime() >= mDeviceLog.getLogDateUnixSec());
        assertEquals(mefaultDeviceLogInfoTypeId, mDeviceLog.getDeviceLogInfoTypeId());
        assertEquals(mTestbatterydefaultValue, mDeviceLog.getValueNumber());
        assertEquals(mTestbatterydefaultStringValue, mDeviceLog.getValueString());
        assertEquals(mDefaultDatabaseRowId, mDeviceLog.getDatabaseRowId());
    }

    /*
    * test create battery status device log without any value
    * Test Case ID : UTMAID000002
    */
    @Test
    public void createDeviceLogWithoutValue() {
        mDeviceLog = new DeviceLog(mTestdeviceLogInfoType);
        //assertEquals(mTestMonitoringSessionId, mDeviceLog.getMonitoringSessionId());
        assertTrue(getCurrentTime() >= mDeviceLog.getLogDateUnixSec());
        assertEquals(mTestdeviceLogInfoType, mDeviceLog.getDeviceLogInfoTypeId());
        assertEquals(mTestbatterydefaultValue, mDeviceLog.getValueNumber());
        assertEquals(mTestbatterydefaultStringValue, mDeviceLog.getValueString());
        assertEquals(mDefaultDatabaseRowId, mDeviceLog.getDatabaseRowId());
    }

    /*
    * test create battery status device log with number value
    * Test Case ID : UTMAID000003
    */
    @Test
    public void createDeviceLogWithValue() {
        mDeviceLog = new DeviceLog(mTestdeviceLogInfoType, mTestbattery0);
        //assertEquals(mTestMonitoringSessionId, mDeviceLog.getMonitoringSessionId());
        assertTrue(getCurrentTime() >= mDeviceLog.getLogDateUnixSec());
        assertEquals(mTestdeviceLogInfoType, mDeviceLog.getDeviceLogInfoTypeId());
        assertEquals(mTestbattery0, mDeviceLog.getValueNumber());
        assertEquals(mTestbatterydefaultStringValue, mDeviceLog.getValueString());
        assertEquals(mDefaultDatabaseRowId, mDeviceLog.getDatabaseRowId());
    }

    /*
    * test create battery status device log with string value
    * Test Case ID : UTMAID000004
    */
    @Test
    public void createDeviceLogWithString() {
        mDeviceLog = new DeviceLog(mTestdeviceLogInfoType, mTestbatteryString);
        //assertEquals(mTestMonitoringSessionId, mDeviceLog.getMonitoringSessionId());
        assertTrue(getCurrentTime() >= mDeviceLog.getLogDateUnixSec());
        assertEquals(mTestdeviceLogInfoType, mDeviceLog.getDeviceLogInfoTypeId());
        assertEquals(mTestbatterydefaultValue, mDeviceLog.getValueNumber());
        assertEquals(mTestbatteryString, mDeviceLog.getValueString());
        assertEquals(mDefaultDatabaseRowId, mDeviceLog.getDatabaseRowId());
    }

    /*
    * test create battery status device log with number value and string value
    * Test Case ID : UTMAID000005
    */
    @Test
    public void createDeviceLogWithNumberString() {
        mDeviceLog = new DeviceLog(mTestdeviceLogInfoType, mTestbattery0, mTestbatteryString);
        //assertEquals(mTestMonitoringSessionId, mDeviceLog.getMonitoringSessionId());
        assertTrue(getCurrentTime() >= mDeviceLog.getLogDateUnixSec());
        assertEquals(mTestdeviceLogInfoType, mDeviceLog.getDeviceLogInfoTypeId());
        assertEquals(mTestbattery0, mDeviceLog.getValueNumber());
        assertEquals(mTestbatteryString, mDeviceLog.getValueString());
        assertEquals(mDefaultDatabaseRowId, mDeviceLog.getDatabaseRowId());
    }
}
