package com.spectocor.micor.mobileapp.uitestframework;

import android.os.IBinder;
import android.support.test.espresso.Root;
import android.view.WindowManager;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * Created by qwang on 1/14/2016.
 */
public class ToastMatcher extends TypeSafeMatcher<Root> {

    @Override
    public void describeTo(Description description) {
        description.appendText("is toast");
    }

    @Override
    public boolean matchesSafely(Root root) {
        int type = root.getWindowLayoutParams().get().type;
        //UITestLog.d("current type:" + Integer.toString(type));
        //UITestLog.d("expected toast:" + Integer.toString(WindowManager.LayoutParams.TYPE_TOAST));
        if ((type == WindowManager.LayoutParams.TYPE_TOAST)) {
            IBinder windowToken = root.getDecorView().getWindowToken();
            IBinder appToken = root.getDecorView().getApplicationWindowToken();
            //UITestLog.d("window token:" + windowToken.toString());
            //UITestLog.d("app token:" + appToken.toString());
            if (windowToken == appToken) {
                // windowToken == appToken means this window isn't contained by any other windows.
                // if it was a window for an activity, it would have TYPE_BASE_APPLICATION.

                return true;
            }
        }
        return false;
    }
}
