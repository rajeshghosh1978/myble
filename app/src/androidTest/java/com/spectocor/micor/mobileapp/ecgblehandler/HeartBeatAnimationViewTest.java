package com.spectocor.micor.mobileapp.ecgblehandler;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.content.LocalBroadcastManager;
import android.test.suitebuilder.annotation.LargeTest;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainService;
import com.spectocor.testutilities.IntentUtility;
import com.spectocor.testutilities.utils.ArrayUtility;
import com.spectocor.testutilities.utils.ThreadUtility;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 2/12/2016.
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class HeartBeatAnimationViewTest {

    //////////////////////////////////////////////////////////////
    // Simple Test Framework
    //////////////////////////////////////////////////////////////
    private static String TAG = "HeartBeatAnimationViewTest";

    //private UITestSimpleFramework mTestFramework;
    private Context mContext;
    private static int mSampleRate = 250;
    private static long mDefaultSleepMillionSecond = 7000;

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            //getTestFramework(); // launch the activity and get test framework
        }
    };

    //////////////////////////////////////////////
    // test data
    //////////////////////////////////////////////
    private int mDefaultHeartBeatVal = 70;
    private int mDefaultDisplayTime = 60;
    //////////////////////////////////////////////
    // test case
    //////////////////////////////////////////////
    /**
     *
     * test main activity receiving heart beat number to display heart beat animation
     * test data: 70
     *
     * Test Case ID : UTMAID150001
     */
    @Test
    public void testDisplayAnimation70() {

        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond);

        int[] mockHeartBeats;

        mockHeartBeats = ArrayUtility.fillingIntArray(mDefaultHeartBeatVal, mDefaultDisplayTime);

        AMainActivity tac = mActivityRule.getActivity();

        assert(tac != null);

        AMainService.sendToSignalView = true;
        tac.callWebviewJsFunction("displayAdminEcgSignalPage()");

        showHeartBeatAnimationView(mockHeartBeats);

        // check result

        //wait
        ThreadUtility.applicationSleep(mDefaultSleepMillionSecond *2 );
    }


    /////////////////////////////////////////////////////////////////
    // support function
    /////////////////////////////////////////////////////////////////
    /**
     * display list of heart beat animation on main activity
     * @param heartBeatRates
     * @return
     */
    public boolean showHeartBeatAnimationView(int[] heartBeatRates){
        boolean rc = true ;

        Intent intent;

        if(AMainService.sendToSignalView) {

            for(int i = 0;i <  mDefaultDisplayTime; ++i){

                rc = showHeartBeatAnimationView(heartBeatRates[i]);

            }

        }

        return rc;
    }

    /**
     * display heart beat animation on main activity
     * @param heartBeatRates
     * @return
     */
    public boolean showHeartBeatAnimationView(int heartBeatRates){

        long start = System.currentTimeMillis();


        Intent intent = IntentUtility.createRenderHeartBeatAnimationIntent();

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

        long end = System.currentTimeMillis();

        //Thread.sleep((60000 / heartBeatRates) - (end - start));
        SystemClock.sleep((60000 / heartBeatRates) - (end - start));
        //SystemClock.sleep((60000 / heartBeatRates));

        return true;
    }
}
