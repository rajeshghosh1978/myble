package com.spectocor.micor.mobileapp.performance;

import android.content.Context;
import android.test.AndroidTestCase;

import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;
import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.devicelogs.ApplicationLogService;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLog;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogLogger;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogQueueRepository;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogRepository;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;
import com.spectocor.testutilities.utils.StringUtils;

import org.junit.Before;

import java.io.File;
import java.util.Random;

/**
 * Created by qwang on 12/11/2015.
 * Performance test case for Device Log
 */
public class DeviceLogService extends AndroidTestCase {
    public static final int mMAXLOGSIZE = 10; //100000;
    DeviceLogRepository repository;
    DeviceLogQueueRepository queueRepository;
    DeviceLogLogger logger;
    GlobalSettings settings;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        Context context = getContext();
        context.deleteDatabase(LogEventsDatabaseHelper.DatabaseName);

        LogEventsDatabaseHelper databaseHelper = new LogEventsDatabaseHelper(context);
        QueueDatabaseHelper queueDatabaseHelper = new QueueDatabaseHelper(context);
        repository = new DeviceLogRepository(databaseHelper);
        queueRepository = new DeviceLogQueueRepository(queueDatabaseHelper);
        settings = new GlobalSettings();

        logger = new DeviceLogLogger(repository, settings, queueRepository);
    }


    /**********************************
     * limitation of log size, if great than LOG_MAX_SIZE, stop to write log
     * precondition:
     * clean DB
     * make sure there are enough storage space
     * Test Case ID : UTMAID000020
     */
    public void testLogMobileDeviceMaxiSize() throws Exception {
        int log_max_size = mMAXLOGSIZE; // maxi log row number

        //int time;
        String exception_str;
        ApplicationLogService service;
        Exception en;
        DeviceLog log;

        long end_time;

        for (int i = 0; i < log_max_size; ++i) {
            end_time = System.currentTimeMillis();
            exception_str = "test mobile device log exception number " + Integer.toString(i);
            service = new ApplicationLogService(logger);
            en = new Exception(exception_str);
            log = service.logApplicationException(en);
            assertEquals(log.getValueString(), exception_str);
            System.out.println("log:" + i + " time: " + (System.currentTimeMillis() - end_time));
            //end_time = System.currentTimeMillis();
            testDbSize();
        }

    }

    public void testDbSize() {
        File f = getContext().getDatabasePath(LogEventsDatabaseHelper.DatabaseName);
        long dbSize = f.length();
        System.out.println("databasesize:" + dbSize);
    }


    /*************************************
     * test log big data chunk
     * so far log function store data at internal storage
     * maxi internal storage size is 2G
     * int log_max_size = 8640;        // 12(data chunk number per hour) * 24(hour) * 30(day)  = 8640
     * int chunk_data_size = 150000;   // 250(sample rate) * 2(lead number) * 60(second) * 5(min) = 150000
     * precondition:
     * clean log DB
     * make sure there are enough storage space. around 1.3 GB. otherwise it will thought system exception
     * Test Case ID : UTMAID000021
     */
    public void testLogMobileDeviceMaxiBigDataChunk() throws Exception {
        int log_max_size = 100; //8640;        //8640  // 12 * 24 * 30  = 8640
        int chunk_data_size = 150000;   // 250 * 2 * 60 * 5


        String exception_str;
        ApplicationLogService service;
        Exception en;
        DeviceLog log;

        long end_time;

        for (int i = 0; i < log_max_size; ++i) {
            end_time = System.currentTimeMillis();
            exception_str = StringUtils.randomString(chunk_data_size);
            service = new ApplicationLogService(logger);
            en = new Exception(exception_str);
            log = service.logApplicationException(en);
            assertNotNull(log);
            System.out.println("chunk:" + i + " time: " + (System.currentTimeMillis() - end_time));
            testDbSize();
        }
    }

    /*
    * Test reading one log item performance
    * Test Case ID : UTMAID000022
    */
    public void testReadingPerformanceSingleLogItem() throws Exception {
        int log_id;
        Random rand = new Random(mMAXLOGSIZE);
        log_id = rand.nextInt(mMAXLOGSIZE) + 1;
        testLogMobileDeviceMaxiSize();    // need little long time to execute this function
        long time = System.currentTimeMillis();
        DeviceLog log = repository.getById(log_id);
        System.out.println("log item:" + log_id + " time: " + (System.currentTimeMillis() - time));
        assertTrue(log != null);
    }
}
