package com.spectocor.micor.mobileapp.uitestframework;

/**
 * Created by qwang on 1/12/2016.
 */
public class UITestLog {

    public static String UITestPrefix = "UITest:";
    public static String UITESTErrorPrefix = "ERROR:";
    public static String UITESTDumpPrefix = "DUMP:";
    public static String UITESTExceptionPrefix = "EXCEPTION:";
    public static String UITESTPassPrefix = "PASS:";
    public static String UITESTFailPrefix = "FAIL:";

    private static int mLogFlag = 0;  // control log or not


    private static String mNewLineSign = System.getProperty("line.separator");

    public static int getLogFlag() {
        return mLogFlag;
    }

    public static void setLogFlag(int Flag) {
        mLogFlag = Flag;
    }

    public static boolean needFlag(int LogType) {
        boolean rc;

        rc = mLogFlag < LogType;

        return rc;
    }

    public static int getLogType(String logTypeName) {
        int log_type = 0;

        switch (logTypeName) {
            case "t":
                log_type = 4;
                break;
            case "e":
                log_type = 3;
                break;
            case "x":
                log_type = 2;
                break;
            case "d":
                log_type = 1;
                break;
            default:
                log_type = 0;
        }

        return log_type;
    }

    public static void e(String Msg) {
        boolean rc;

        rc = needFlag(getLogType("e"));

        if (rc) {
            System.out.print(UITestPrefix + UITESTErrorPrefix + Msg);
            System.out.print(mNewLineSign);
        }
    }

    public static void x(String Msg) {
        boolean rc;

        rc = needFlag(getLogType("x"));

        if (rc) {
            System.out.print(UITestPrefix + UITESTExceptionPrefix + Msg);
            System.out.print(mNewLineSign);
        }
    }

    public static void d(String Msg) {
        boolean rc;

        rc = needFlag(getLogType("d"));

        if (rc) {
            System.out.print(UITestPrefix + UITESTDumpPrefix + Msg);
            System.out.print(mNewLineSign);
        }
    }

    public static void pass(String Msg) {
        boolean rc;

        rc = needFlag(getLogType("t"));

        if (rc) {
            System.out.print(UITestPrefix + UITESTPassPrefix + Msg);
            System.out.print(mNewLineSign);
        }
    }

    public static void fail(String Msg) {
        boolean rc;

        rc = needFlag(getLogType("t"));

        if (rc) {
            System.out.print(UITestPrefix + UITESTFailPrefix + Msg);
            System.out.print(mNewLineSign);
        }
    }
}
