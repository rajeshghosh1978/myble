package com.spectocor.micor.mobileapp.ecgblehandler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.content.LocalBroadcastManager;
import android.test.suitebuilder.annotation.LargeTest;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.uitestframework.UITestCommand;
import com.spectocor.micor.mobileapp.uitestframework.UITestSimpleFramework;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static android.support.test.espresso.web.sugar.Web.onWebView;

/**
 * Created by qwang on 1/27/2016.
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class SynergenBleClientSessionActivationTest {

    //////////////////////////////////////////////////////////////
    // Simple Test Framework
    //////////////////////////////////////////////////////////////
    private UITestSimpleFramework mTestFramework;
    private Context mContext;

    //////////////////////////////////////////////////////////////
    // Rule
    //////////////////////////////////////////////////////////////
    @Rule
    // launchActivity = true
    public ActivityTestRule<AMainActivity> mActivityRule = new ActivityTestRule<AMainActivity>(AMainActivity.class) {
        @Override
        protected void afterActivityLaunched() {
            onWebView().forceJavascriptEnabled();
            getTestFramework(); // launch the activity and get test framework
        }


    };

    //////////////////////////////////////////////
    // test case
    //////////////////////////////////////////////
    /**
     *
     * test main activity receiving BT HEARTBEAT Detected
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID120001
     */
    @Test
    public void testBTHeartBeatDetected() {

        //UITestSimpleFramework framework;

        String test_case_data_file = "td_detected_heart_beat_intent.json";



        //LocalBroadcastManager.getInstance(context).sendBroadcast(withSessionActivationLEDeviceHeartbeatDetectedIntent());

        // waiting loading page
        try{

            Thread.sleep(6000);

        }catch (Exception e){

        }

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(withBTDeviceIntent());

        mTestFramework.runTestFile(test_case_data_file);

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(withSessionActivationLEDeviceHeartbeatDetectedIntent());
        // waiting loading page
        try{

            Thread.sleep(10000);

        }catch (Exception e){

        }

        //UITestCommand.clickResetBtn();
    }

    /**
     *
     * test main activity receiving BT HEARTBEAT Detected
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID120002
     */
    //@Test
    public void NoBTHeartBeatDetected(){

        //UITestSimpleFramework framework;

        String test_case_data_file = "td_detected_heart_beat_intent.json";

        //getTestFramework(); // launch the activity and get test framework

        //LocalBroadcastManager.getInstance(context).sendBroadcast(withSessionActivationLEDeviceHeartbeatDetectedIntent());

        // waiting loading page

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(withBTDeviceIntent());

        mTestFramework.runTestFile(test_case_data_file);

        //LocalBroadcastManager.getInstance(mContext).sendBroadcast(withSessionActivationLEDeviceHeartbeatDetectedIntent());
        // waiting loading page
        try{

            Thread.sleep(10000);

        }catch (Exception e){

        }

        UITestCommand.clickResetBtn();
    }

    //////////////////////////////////////////////
    // test case
    //////////////////////////////////////////////
    /**
     *
     * test main activity receiving BT HEARTBEAT Detected
     * <p/>
     * start page : select bluetooth device
     * end page: enter facility code
     * <p/>
     * precondition:
     * load select bluetooth device page
     * invalid facility code
     * <p/>
     * Test Case ID : UTMAID120003
     */
    @Test
    public void testBTDeviceDetectionIntent() {

        //UITestSimpleFramework framework;

        //String test_case_data_file = "td_detected_heart_beat_intent.json";

        //getTestFramework(); // launch the activity and get test framework

        //LocalBroadcastManager.getInstance(context).sendBroadcast(withSessionActivationLEDeviceHeartbeatDetectedIntent());

        // waiting application launch and start service
        //

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(withBTDeviceIntent());

        // waiting loading page
        try{

            Thread.sleep(10000);

        }catch (Exception e){

        }

        UITestCommand.clickResetBtn();
    }

//    @Test
//    public void TestClickResetBtn() {
//
//        //UITestSimpleFramework framework;
//
//        getTestFramework(); // launch the activity and get test framework
//
//        // waiting loading page
//        try{
//
//            Thread.sleep(10000);
//
//        }catch (Exception e){
//
//        }
//
//        UITestCommand.clickResetBtn();
//    }

    //////////////////////////////////////////////
    // support function
    //////////////////////////////////////////////
    /**
     * @return start {@link Intent} for the simple web form URL.
     */
    private static Intent withNullIntent() {

        return null;
    }
    /**
     * @return start {@link Intent} for the simple web form URL.
     */
    private static Intent withSessionActivationLEDeviceHeartbeatDetectedIntent() {

        Bundle bundle = new Bundle();
        bundle.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_HEARTBEAT_DETECTED);

        Intent btIntent = new Intent("BLUETOOTH_EVENT");
        btIntent.putExtras(bundle);

        //Intent basicFormIntent = new Intent();
        //basicFormIntent.putExtra("KEY_URL_TO_LOAD", "file:///android_asset/micor_html/index.html");
        //return basicFormIntent;
        return btIntent;
    }

    private static Intent withBTDeviceIntent(){
        String devicesFound = "" +
                "[" +
                "{'btMac':'B0:B4:48:C4:A9:89','btName':'STL ECG 210','rssi':-38}," +
                "{'btMac':'B0:B4:48:C4:F9:89','btName':'STL ECG 517','rssi':-38}," +
                "{'btMac':'B0:B4:48:D4:A9:A4','btName':'STL ECG 320','rssi':-38}" +
                "]";

        ArrayList<StlDeviceInfo> stlDeviceInfos = ObjectUtils.fromJson(devicesFound, ArrayList.class);

        Bundle b = new Bundle();
        b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICES_FOUND);
        b.putString("stlDevicesFoundJson", ObjectUtils.toJson(stlDeviceInfos));
        b.putString("callbackFnString", "allLeDevicesFound");

        Intent i = new Intent("BLUETOOTH_EVENT");
        i.putExtras(b);

        return i;
    }

    /**
     * launch activity and get current test framework
     *
     * @return
     */
    public UITestSimpleFramework getTestFramework() {

        //UITestSimpleFramework framework;

        //mActivityRule.launchActivity(withNullIntent()); // launch activity

        AMainActivity activity = mActivityRule.getActivity();

        assert(activity != null);
        // set context

        mContext = activity.getApplicationContext();  // context for activity under test
        //Context context = InstrumentationRegistry.getContext().getApplicationContext();
        //mContext = InstrumentationRegistry.getTargetContext();  // context for activity under test



        if (mTestFramework == null) {
            mTestFramework = new UITestSimpleFramework();
        }
        mTestFramework.setContext(mContext);

        return mTestFramework;
    }
}
