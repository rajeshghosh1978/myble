package com.spectocor.micor.mobileapp.ecgblehandler;

/**
 * Created by Savio Monteiro on 1/23/2016.
 */

public final class StlDeviceInfo {

    public String btName = "";
    public String btMac = "";
    public int rssi = 0;

    public StlDeviceInfo(final String btName, final String btMac, final int rssi) {
        this.btName = btName;
        this.btMac = btMac;
        this.rssi = rssi;
    }
}