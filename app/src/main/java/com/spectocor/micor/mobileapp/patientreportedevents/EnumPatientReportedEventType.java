package com.spectocor.micor.mobileapp.patientreportedevents;


import android.util.SparseArray;

public enum EnumPatientReportedEventType {
    Other(101),
    ChestDiscomfort(102),
    Fatigue(103),
    Palpitation(104),
    Dizziness(105),
    ShortnessOfBreath(106),
    Fluttering(107),
    RacingHeart(108),
    SlurredSpeech(109),
    Headache(110),
    RecentlyFainted(111);

    private static SparseArray<EnumPatientReportedEventType> mappings;
    private int intValue;

    EnumPatientReportedEventType(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static SparseArray<EnumPatientReportedEventType> getMappings() {
        if (mappings == null) {
            synchronized (EnumPatientReportedEventType.class) {
                if (mappings == null) {
                    mappings = new SparseArray<>();
                }
            }
        }
        return mappings;
    }

    public static EnumPatientReportedEventType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}