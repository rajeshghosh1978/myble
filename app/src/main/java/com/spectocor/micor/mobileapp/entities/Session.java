package com.spectocor.micor.mobileapp.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 2/24/2016.
 */
public class Session {

    @SerializedName("sessionId")
    @Expose
    private int sessionId;
    @SerializedName("endDateTime")
    @Expose
    private String endDateTime;
    @SerializedName("startDateTime")
    @Expose
    private String startDateTime;
    @SerializedName("duration")
    @Expose
    private long duration;
    @SerializedName("isExpired")
    @Expose
    private boolean isExpired;

    /**
     *
     * @return
     * The sessionId
     */
    public int getSessionId() {
        return sessionId;
    }

    /**
     *
     * @param sessionId
     * The sessionId
     */
    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    /**
     *
     * @return
     * The endDateTime
     */
    public String getEndDateTime() {
        return endDateTime;
    }

    /**
     *
     * @param endDateTime
     * The endDateTime
     */
    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    /**
     *
     * @return
     * The startDateTime
     */
    public String getStartDateTime() {
        return startDateTime;
    }

    /**
     *
     * @param startDateTime
     * The startDateTime
     */
    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    /**
     *
     * @return
     * The duration
     */
    public long getDuration() {
        return duration;
    }

    /**
     *
     * @param duration
     * The duration
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }


    /**
     *
     * @return
     * Whether this session has expired or not
     */
    public boolean getIsExpired() {
        return this.isExpired;
    }

    /**
     *
     * @param isExpired Whether this session has expired or not
     */
    public void setIsExpired(boolean isExpired) {
        this.isExpired = isExpired;
    }
}
