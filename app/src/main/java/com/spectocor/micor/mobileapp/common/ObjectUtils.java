package com.spectocor.micor.mobileapp.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import java.lang.reflect.Type;

/**
 * Utilities to work with objects
 */
public class ObjectUtils {

    private static final Gson gson = new Gson(); //.setPrettyPrinting().create();

    /**
     * Converts an object to its Json representation
     *
     * @param object any java object
     * @return json value of the object as string
     */
    public static String toJson(Object object) {
        return gson.toJson(object);
    }

    /**
     * Converts a json string to an object with specified type
     *
     * @param json json value of the object as string
     * @param type java type of the returned object
     * @return java object with type specified
     */
    public static Object fromJson(String json, Type type) {
        return gson.fromJson(json, type);
    }


    /**
     * Converts a json string to an object with specified class
     *
     * @param json     json value of the object as string
     * @param classOfT class of the object, for example, MyReturnType.class
     * @return java object with the specified class
     */
    public static <T> T fromJson(String json, Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }

    /**
     * Converts a json string to an object with specified class
     * When the objects are nested and each nested object may have an unspecified type
     * gson converts everything defined as Object in the return type as JsonElement
     * @param json json element of the object as string
     * @param classOfT class of the object, for example, MyReturnType.class
     * @return java object with the specified class
     */
    public static <T> T fromJson(JsonElement json, Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }

}
