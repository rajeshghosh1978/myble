package com.spectocor.micor.mobileapp.patientreportedevents;

import com.spectocor.micor.mobileapp.common.queuesystem.HttpsHandler;
import com.spectocor.micor.mobileapp.common.queuesystem.QueueHttpSenderBase;


/**
 * Syncs device logs with the server (Sends unsent device logs to the server and removes them from sending queue)
 */
public class PatientReportedEventQueueSender extends QueueHttpSenderBase<PatientReportedEvent> {

    public static final String API_NAME = "PatientReportedEvent/insert";

    public PatientReportedEventQueueSender(PatientReportedEventRepository repository,
                                           PatientReportedEventQueueRepository sendingQueueRepository,
                                           HttpsHandler httpsHandler) {
        super(repository, sendingQueueRepository, httpsHandler, API_NAME);
    }


}
