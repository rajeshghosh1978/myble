package com.spectocor.micor.mobileapp.amain.sessionlist;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.amain.StorageAndDbInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

/**
 * This is a helper class that accesses the list of sessions that have
 * been activated by the current PDA. This list is stored as
 * JSON document in shared preferences with the key named
 * "SESSION_LIST_JSON".
 *
 * Created by Savio Monteiro on 2/20/2016.
 */
public class SessionListAccess {

    public static final String KEY_SESSION_LIST_JSON = "SESSION_LIST_JSON";
    public static final boolean ALLOW_DUPLICATE_SESSION_ID_FOR_TESTING = true;

    public static final String TAG = SessionListAccess.class.getSimpleName();


    private static void log(String str) {
        MLog.log(TAG + ": " + str);
    }





    /**
     * Gets the current session list stored in shared preferences
     * @return
     */
    public static ArrayList<String> getCurrentSessionList(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String sessionListJson = preferences.getString(KEY_SESSION_LIST_JSON, "{}");

        ArrayList<String> sessionList;

        if(sessionListJson.equals("{}")) {
            sessionList = new ArrayList<String>();
        }
        else {
            sessionList = (ArrayList<String>) ObjectUtils.fromJson(sessionListJson, ArrayList.class);
        }

        return sessionList;
    }


    /**
     * Checking if a particular session Id exists in the current session list
     * @param sessionId
     * @param context
     * @return
     */
    private static boolean sessionExistsInSessionList(final int sessionId, final long sessionStartTimestamp, final Context context) {

        ArrayList<String> currentSessionList = getCurrentSessionList(context);

        log(ObjectUtils.toJson(currentSessionList));

        if(currentSessionList.size() == 0) {
            log("Session List is empty");
            return false;
        }

        for(String sessionInfoJson:currentSessionList) {

            SessionListSessionInfo sessionInfo = ObjectUtils.fromJson(sessionInfoJson, SessionListSessionInfo.class);

            log("Found SessionInfo: " + sessionInfo.sessionId + " ("+sessionInfo.sessionStartTimestamp+")");

            if(sessionInfo.sessionId == sessionId && sessionInfo.sessionStartTimestamp == sessionStartTimestamp) {
                log("Session ID: " + sessionId + " and startTs: " + sessionStartTimestamp + " ALREADY EXISTS");
                return true;
            }
        }

        log("Session ID: " + sessionId + " and startTs: " + sessionStartTimestamp + " not in list");
        return false;
    }


    /**
     * Adds a newly activated session to the session list.
     * SessionListSessionInfo instance to be added.
     */
    public static void addSessionToList(SessionListSessionInfo sessionInfo, Context context) {

        log("Attempting to add sessionid: " + sessionInfo.sessionId + " to sessions list");

        ArrayList<String> sessionList = getCurrentSessionList(context);

        int sessionId = sessionInfo.sessionId;

        if(!sessionExistsInSessionList(sessionId, sessionInfo.sessionStartTimestamp, context)) {
            log("Adding " + sessionId);
            sessionList.add(ObjectUtils.toJson(sessionInfo));
        }
        else {
            throw new IllegalArgumentException("Session with " + sessionId + " and start timestamp = " + sessionInfo.sessionStartTimestamp + " already exists in session list");
        }

        saveSessionListToPreferences(sessionList, context);
    }


    /**
     * Save the current list
     * @param context
     */
    private static void saveSessionListToPreferences(final ArrayList<String> sessionList, Context context) {

        String jsonStr = ObjectUtils.toJson(sessionList);


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_SESSION_LIST_JSON, jsonStr);
        editor.apply();

        log("Saved session list to prefs: " + jsonStr);
    }


    /**
     * To set the sessionEndTimestamp for a particular sessionId
     * @param sessionId
     * @param context
     */
    public static void setSessionEndTimestampForSession(final int sessionId, final long sessionStartTimestamp, final long sessionEndTimestamp, Context context) {

        if(!sessionExistsInSessionList(sessionId, sessionStartTimestamp, context))
        {
            //throw new IllegalArgumentException("Session ID " + sessionId + " does not exist in session list");
            return;
        }

        if(sessionEndTimestamp <= 0) {
            //throw new IllegalArgumentException("Session End Timestamp must be a positive long type");
            return;
        }

        ArrayList<String> sessionList = getCurrentSessionList(context);

        for(String sessionInfoJson:sessionList) {

            SessionListSessionInfo sessionInfo = ObjectUtils.fromJson(sessionInfoJson, SessionListSessionInfo.class);

            if(sessionId == sessionInfo.sessionId && sessionStartTimestamp == sessionInfo.sessionStartTimestamp) {

                final SessionListSessionInfo copyOfSessionInfo = sessionInfo;

                copyOfSessionInfo.setSessionEndTimestamp(sessionEndTimestamp);
                log("Updated Session End Timestamp to " + sessionEndTimestamp);

                sessionList.remove(sessionInfoJson);
                sessionList.add(ObjectUtils.toJson(copyOfSessionInfo));

                saveSessionListToPreferences(sessionList, context);

                break;
            }
        }
    }

    /**
     * For testing to clean
     * @param context
     */
    public static void emptySessionList(Context context) {
        saveSessionListToPreferences(new ArrayList<String>(), context);
    }


    /**
     * Maintain a list of only last 3 sessions
     * @param context
     */
    public static void cleanToKeepLastThreeSessions(Context context) {


        log("Cleaning Up");

        // Fetching current List
        log("Getting list");
        ArrayList<String> sessionList = getCurrentSessionList(context);
        ArrayList<String> newSessionList = new ArrayList<>();

        if(sessionList.size() == 0) {
            log("List is empty!");
            return;
        }

        // keep only last 3 sessions in list
        int currentCount = sessionList.size();
        log(currentCount + " session in session List");

        if(currentCount <= 3)
        {
            log("Not cleaning anything");
            return;
        }

        log(">3 sessions");

        long startTimestamps[] = new long[sessionList.size()];

        int i = 0;
        for(String sessionInfoJson: sessionList) {

            SessionListSessionInfo sessionInfo = ObjectUtils.fromJson(sessionInfoJson, SessionListSessionInfo.class);

            startTimestamps[i] = sessionInfo.sessionStartTimestamp;

            i++;

        }

        Arrays.sort(startTimestamps); // ascending in-place sorting

        if(startTimestamps.length > 3)
        {
            long oldestTimestampToKeep = startTimestamps[startTimestamps.length - 3];

            Date oldestDate = new Date();
            oldestDate.setTime(oldestTimestampToKeep);
            log("Keeping sessions younger than " + oldestDate.toString());

            // all timestamps older in time (smaller in value) than oldestTimestampToKeep is deleted
            for (String sessionInfoJson : sessionList) {
                SessionListSessionInfo sessionInfo = ObjectUtils.fromJson(sessionInfoJson, SessionListSessionInfo.class);

                Date d = new Date();
                oldestDate.setTime(sessionInfo.sessionStartTimestamp);

                if (sessionInfo.sessionStartTimestamp < oldestTimestampToKeep)
                {

                    log("Removing Session: " + sessionInfo.sessionType + " which started at " + d);

                    // Database
                    // Delete database associated with wih this session-info
                    String dbFilePath = StorageAndDbInfo.formulateDbNameAndPathBySessionInfo(AMainApplication.DB_PATH, sessionInfo);
                    File fDel = new File(dbFilePath);
                    log("Deleting " + dbFilePath);
                    fDel.delete();

                    // Delete database journal file associated with wih this session-info
                    String dbJournalFilePath = dbFilePath + "-journal";
                    fDel = new File(dbJournalFilePath);
                    log("Deleting " + dbJournalFilePath);
                    fDel.delete();

                }
                else // keep this one
                {
                    log("Removing Session: " + sessionInfo.sessionType + " which started at " + d);
                    newSessionList.add(sessionInfoJson);
                }
            }
        }


        // save to preferences
        saveSessionListToPreferences(newSessionList, context);

    }


    static class SessionStartTimestampComparator implements Comparator<String> {

        public int compare(String sessionInfoJson1, String sessionInfoJson2) {

            SessionListSessionInfo sessionInfo1 = ObjectUtils.fromJson(sessionInfoJson1, SessionListSessionInfo.class);
            SessionListSessionInfo sessionInfo2 = ObjectUtils.fromJson(sessionInfoJson2, SessionListSessionInfo.class);


            return (int) (sessionInfo1.sessionStartTimestamp - sessionInfo2.sessionStartTimestamp);
        }
    }
}
