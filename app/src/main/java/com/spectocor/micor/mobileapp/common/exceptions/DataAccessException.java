package com.spectocor.micor.mobileapp.common.exceptions;

/**
 * Exceptions from data access
 */
public class DataAccessException extends Exception {

    public DataAccessException() {
    }

    public DataAccessException(String message) {
        super(message);
    }
}
