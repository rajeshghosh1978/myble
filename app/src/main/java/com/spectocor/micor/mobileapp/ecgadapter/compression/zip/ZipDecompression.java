package com.spectocor.micor.mobileapp.ecgadapter.compression.zip;

import com.spectocor.micor.mobileapp.utils.arrays.FromByteArray;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/**
 * This class provides methods to perform Lossless Compression on an input ECG Signal
 * The current implementation is java.util's Zip Deflater/Inflater.
 *
 * Created by Savio Monteiro
 */
public class ZipDecompression {


    /**
     * Performs decompression of signal compressed by @ZipCompression
     *
     * @param compressedEcgBytes The compressed signal
     * @return The decompressed signal
     * @throws DataFormatException
     * @throws IOException
     */
    public static byte[] decompress(final byte[] compressedEcgBytes) throws DataFormatException, IOException {

        if (compressedEcgBytes == null) {
            return null;
        }

        if (compressedEcgBytes.length == 0) {
            return new byte[]{};
        }

        byte[] decompressed;

        Inflater decompressor = new Inflater();
        decompressor.setInput(compressedEcgBytes, 0, compressedEcgBytes.length);

        // Create an expandable byte array to hold the decompressed data
        ByteArrayOutputStream bos = new ByteArrayOutputStream(compressedEcgBytes.length);
        byte[] buf = new byte[100];
        while (!decompressor.finished()) {
            int count = decompressor.inflate(buf);
            bos.write(buf, 0, count);
        }
        bos.close();
        decompressor.end();
        decompressed = bos.toByteArray();


        /*
        byte[] result = new byte[100];
        int resultLength = decompressor.inflate(result);
        decompressor.end();
        decompressed = Arrays.copyOfRange(result, 0, resultLength);
        */


        return decompressed;
    }

    /**
     * Performs decompression of signal compressed by @ZipCompression and returns an array of shorts
     * @param compressedEcgBytes The compressed ECG array of bytes
     * @return The uncompressed ECG array of shorts
     * @throws DataFormatException
     * @throws IOException
     */
    public static short[] decompressAsShortArray(final byte[] compressedEcgBytes) throws DataFormatException, IOException {

        if (compressedEcgBytes == null) {
            return null;
        }

        if (compressedEcgBytes.length == 0) {
            return new short[]{};
        }


        byte[] decompressedEcgSignal = decompress(compressedEcgBytes);

        // recover shorts from bytes
        return FromByteArray.toShortArray(decompressedEcgSignal);
    }





}
