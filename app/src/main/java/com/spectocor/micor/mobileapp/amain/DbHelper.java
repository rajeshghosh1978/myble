package com.spectocor.micor.mobileapp.amain;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.TimeZone;

/**
 * Created by Savio Monteiro on 2/27/2016.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static DbHelper aMainDbHelper;

    private static final int DB_VERSION = 5;

    public static String TIMEZONE_NAME_ID = "";
    public static long TIMEZONE_OFFSET_MILLIS = 0;


    private static final String INTEGER_TYPE = " INTEGER";
    private static final String TEXT_TYPE = " TEXT";
    private static final String BLOB_TYPE = " BLOB";
    private static final String COMMA_SEP = ",";



    DbHelper(Context context)
    {
        super(context, StorageAndDbInfo.getDbNameAndPathFromState(context), null, DB_VERSION);
        log("DbNameAndPath: " + StorageAndDbInfo.getDbNameAndPathFromState(context));


    }


    public static DbHelper getInstance(Context context) {

        if(aMainDbHelper == null) {
            aMainDbHelper = new DbHelper(context);
        }

        TIMEZONE_NAME_ID = TimeZone.getDefault().getID();
        TIMEZONE_OFFSET_MILLIS = TimeZone.getDefault().getOffset(System.currentTimeMillis());

        return aMainDbHelper;
    }



    private static void log(String str) {
        MLog.log(DbHelper.class.getSimpleName() + ": " + str);
    }

    public static DbHelper resetInstance(Context context) {

        log("Setting Database from Scratch");
        aMainDbHelper = null;
        return getInstance(context);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        log("onCreate()");

        db.execSQL(SQL_CREATE_TABLE_ECG_CHUNKS);
        db.execSQL(SQL_CREATE_TABLE_PTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_TABLE_ECG_CHUNKS);
        db.execSQL(SQL_DROP_TABLE_PTE);
        onCreate(db);
    }




    // ECG CHUNKS DEFINITION ==========================================================

    public static final String ECG_CHUNKS_TABLE_NAME = "ECG_CHUNKS";

    public static final String ECG_CHUNKS_COLUMN_SESSION_ID = "SESSION_ID";
    public static final String ECG_CHUNKS_COLUMN_START_TIMESTAMP = "START_TIMESTAMP";
    public static final String ECG_CHUNKS_COLUMN_INSERT_TIMESTAMP = "INSERT_TIMESTAMP";
    public static final String ECG_CHUNKS_COLUMN_TIMEZONE_NAME_ID = "TIMEZONE_NAME_ID";
    public static final String ECG_CHUNKS_COLUMN_TIMEZONE_OFFSET_MILLIS = "TIMEZONE_OFFSET_MILLIS";
    public static final String ECG_CHUNKS_COLUMN_COMPRESSED_ECG_CHUNK_BLOB = "COMPRESSED_ECG_CHUNK_BLOB";
    public static final String ECG_CHUNKS_COLUMN_TRANSMITTED_TIMESTAMP = "TIMESTAMP_OF_SUCCESSFUL_TRANSFER";
    public static final String ECG_CHUNKS_COLUMN_NUMBER_OF_SAMPLES_IN_CHUNK = "NUMBER_OF_SAMPLES_IN_CHUNK";


    private static final String SQL_CREATE_TABLE_ECG_CHUNKS =
            "CREATE TABLE " + ECG_CHUNKS_TABLE_NAME + " (" +
                    ECG_CHUNKS_COLUMN_SESSION_ID + TEXT_TYPE + COMMA_SEP +
                    ECG_CHUNKS_COLUMN_START_TIMESTAMP + INTEGER_TYPE + COMMA_SEP +
                    ECG_CHUNKS_COLUMN_INSERT_TIMESTAMP + INTEGER_TYPE + COMMA_SEP +
                    ECG_CHUNKS_COLUMN_TIMEZONE_OFFSET_MILLIS + INTEGER_TYPE + COMMA_SEP +
                    ECG_CHUNKS_COLUMN_TIMEZONE_NAME_ID + TEXT_TYPE + COMMA_SEP +
                    ECG_CHUNKS_COLUMN_COMPRESSED_ECG_CHUNK_BLOB + BLOB_TYPE + COMMA_SEP +
                    ECG_CHUNKS_COLUMN_TRANSMITTED_TIMESTAMP + INTEGER_TYPE + COMMA_SEP +
                    ECG_CHUNKS_COLUMN_NUMBER_OF_SAMPLES_IN_CHUNK + INTEGER_TYPE + COMMA_SEP +
                    "PRIMARY KEY("+ ECG_CHUNKS_COLUMN_SESSION_ID + COMMA_SEP + ECG_CHUNKS_COLUMN_START_TIMESTAMP +")" +
                    " )";

    private static final String SQL_DROP_TABLE_ECG_CHUNKS =
            "DROP TABLE IF EXISTS " + ECG_CHUNKS_TABLE_NAME;

    // END: ECG CHUNKS DEFINITION ==================================================



    // PATIENT REPORTED EVENTS DEFINITION ==========================================================

    public static String PTE_TABLE_NAME = "PATIENT_REPORTED_EVENTS";
    public static String PTE_COLUMN_NAME_SESSION_ID = "SESSION_ID";
    public static String PTE_COLUMN_NAME_INSERT_TIMESTAMP = "INSERT_TIMESTAMP";
    public static String PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP = "TRANSMITTED_TIMESTAMP";
    public static String PTE_COLUMN_NAME_DATA = "DATA";


    private static String SQL_CREATE_TABLE_PTE = "CREATE TABLE " + PTE_TABLE_NAME + " (" +
            PTE_COLUMN_NAME_SESSION_ID          + " " + INTEGER_TYPE    + COMMA_SEP +
            PTE_COLUMN_NAME_INSERT_TIMESTAMP    + " " + INTEGER_TYPE    + COMMA_SEP +
            PTE_COLUMN_NAME_DATA                + " " + TEXT_TYPE       + COMMA_SEP +
            PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP + " " + INTEGER_TYPE    + COMMA_SEP +
            "PRIMARY KEY(" + PTE_COLUMN_NAME_SESSION_ID + "," + PTE_COLUMN_NAME_INSERT_TIMESTAMP + ")" +
            ")";

    private static String SQL_DROP_TABLE_PTE = "DROP TABLE IF EXISTS " + PTE_TABLE_NAME;

    // END: PATIENT REPORTED EVENTS DEFINITION ==================================================

}
