package com.spectocor.micor.mobileapp.amain;

/**
 * Created by Savio Monteiro on 12/30/2015.
 */

import android.app.Activity;
import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.webkit.JavascriptInterface;

import com.spectocor.micor.mobileapp.amain.receivers.MobileBatteryLevelReceiver;
import com.spectocor.micor.mobileapp.amain.sessionlist.SessionListAccess;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.common.queuesystem.HttpsHandler;
import com.spectocor.micor.mobileapp.ecgblehandler.StlDeviceInfo;
import com.spectocor.micor.mobileapp.ecgblehandler.SynergenBleClientSessionActivation;
import com.spectocor.micor.mobileapp.http.HttpQueueManager;
import com.spectocor.micor.mobileapp.http.HttpRequestHandler;
import com.spectocor.micor.mobileapp.http.common.NetworkHelper;
import com.spectocor.micor.mobileapp.sessionactivation.api.EnrollmentInfo;
import com.spectocor.micor.mobileapp.sessionactivation.api.PatientReportedEventsApi;
import com.spectocor.micor.mobileapp.sessionactivation.api.SessionActivationApi;
import com.spectocor.micor.mobileapp.sessionactivation.api.SessionActivationApiOld;
import com.spectocor.micor.mobileapp.threading.DefaultAsyncListener;
import com.spectocor.micor.mobileapp.threading.DefaultAsyncTask;

import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;


final public class AMainJSInterface {

    private static final int ECG_SLEEP = 1000;

    public static final String TAG = AMainJSInterface.class.getSimpleName();

    String facilityCodeFromServer;

    AMainActivity mA;
    Context mContext;
    SessionActivationApiOld sessionActivationApiOld;
    String activeSessionID;
    HttpsHandler httpsHandler;
    GlobalSettings globalSettings;
    /**
     * Instantiate the interface and set the context
     */
    OkHttpClient httpClient;
    String sessionId;

    public AMainJSInterface(Context c) {

        this.mContext = c;
        globalSettings= new GlobalSettings();
        httpsHandler= new HttpsHandler(globalSettings);
    }

    public AMainJSInterface(AMainActivity a) {

        this.mA = a;

        this.mContext = a;
        globalSettings= new GlobalSettings();
        httpsHandler= new HttpsHandler(globalSettings);

    }

    private static void log(String str) {
        MLog.log(AMainJSInterface.class.getSimpleName() + ": " + str);
    }

    @JavascriptInterface
    public String echo(String mesg) {
        return "echo: " + mesg;
    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
    public void showLongToast(String mesg) {
        AMainActivity.activityHandler.obtainMessage(AMainActivityHandler.ACTIVITY_TOAST_LONG, mesg).sendToTarget();
    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
    public void showShortToast(String mesg) {
        AMainActivity.activityHandler.obtainMessage(AMainActivityHandler.ACTIVITY_TOAST_SHORT, mesg).sendToTarget();
    }

    @JavascriptInterface
    public void resetUrl() {

        //HttpRequestHandler.mHandler = null;
        HttpRequestHandler.isTimerSet = false;
        HttpRequestHandler.index = 0;
        log("Resetting URL");
        AMainActivity.activityHandler.obtainMessage(AMainActivityHandler.ACTIVITY_RESET_EVERYTHING, "").sendToTarget();
    }

    @JavascriptInterface
    public void resetCurrentActivationStateFile() {
        log("Reseting state file ... ");
        saveCurrentActivationState("{}");
    }

    @JavascriptInterface
    public boolean saveCurrentActivationState(String jsonStr) {
        return SessionStateAccess.saveSessionStateJson(mContext, jsonStr);
    }

    @JavascriptInterface
    public String getCurrentActivationState() {
        return SessionStateAccess.getCurrentActivationStateJson(mContext);
    }

    synchronized CurrentSessionState getCurrentSessionState() {
        //return new Gson().fromJson(getCurrentActivationState(), CurrentSessionState.class);
        return ObjectUtils.fromJson(getCurrentActivationState(), CurrentSessionState.class);
    }

    private String getRegisteredEcgDeviceMac() {
        return getCurrentSessionState().ecgBtMac;
    }

    private String getRegisteredEcgDeviceName() {
        return getCurrentSessionState().ecgBtName;
    }


    private Intent createBTIntent(int btEventType) {
        // createBTIntent
        Intent intent = new Intent("BLUETOOTH_EVENT");
        intent.putExtra("EVENT_TYPE", btEventType);
        return intent;
    }

    private Intent createBTIntent(int btEventType, Bundle b) {
        // createBTIntent
        Intent intent = new Intent("BLUETOOTH_EVENT");
        intent.putExtra("EVENT_TYPE", btEventType);
        intent.putExtras(b);
        return intent;
    }

    // At Shipping ... first screen lookup devices ...
    @JavascriptInterface
    public void lookupNewDevices(final String onLeFoundCallbackStr) {
        if (AMainApplication.DO_BLUETOOTH) // BLUETOOTH
        {
            SynergenBleClientSessionActivation.lookupStlDevices(mContext, onLeFoundCallbackStr);
        } else // SIMULATION
        {
            String devicesFound = "" +
                    "[" +
                    "{'btMac':'B0:B4:48:C4:A9:89','btName':'STL ECG 201','rssi':-38}," +
                    "{'btMac':'B0:B4:48:C4:F9:89','btName':'STL ECG 517','rssi':-38}," +
                    "{'btMac':'B0:B4:48:D4:A9:A5','btName':'STL ECG 152','rssi':-38}," +
                    "{'btMac':'B0:B4:48:D4:A9:A4','btName':'STL ECG 320','rssi':-38} " +
                    "]";

            ArrayList<StlDeviceInfo> stlDeviceInfos = ObjectUtils.fromJson(devicesFound, ArrayList.class);

            Bundle b = new Bundle();
            b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICES_FOUND);
            b.putString("stlDevicesFoundJson", ObjectUtils.toJson(stlDeviceInfos));
            b.putString("callbackFnString", onLeFoundCallbackStr);

            Intent i = new Intent("BLUETOOTH_EVENT");
            i.putExtras(b);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);
        }
    }

    @JavascriptInterface
    public void endLeDiscovery() {
        //BluetoothLE.stopDiscoverLE();
    }

    // simulate connect to ecg for session activation process ...
    @JavascriptInterface
    public void connectToEcgForSessionActivation_simulate(final String mac) {

        log("Connecting to ECG from JSInterface ... ");

        DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {
            @Override
            public Object doInBackgroundThread(DefaultAsyncTask httpAsyncTask) {

                //Do heavy/background Task here


                log("Running ...");

                //BluetoothLE.connectToBtleDevice(mac);

                //AMainActivity.webviewHandler.obtainMessage(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTING, "").sendToTarget();
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTING));

                try {
                    Thread.sleep(ECG_SLEEP * 3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTED));
                //AMainActivity.webviewHandler.obtainMessage(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTED, "").sendToTarget();


                try {
                    Thread.sleep(ECG_SLEEP);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_ELECTRODES_ATTACHED));
                //AMainActivity.webviewHandler.obtainMessage(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_ELECTRODES_ATTACHED, "").sendToTarget();

                try {
                    Thread.sleep(ECG_SLEEP);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_RECEIVING_SIGNAL));
                //AMainActivity.webviewHandler.obtainMessage(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_RECEIVING_SIGNAL, "").sendToTarget();

                try {
                    Thread.sleep(ECG_SLEEP);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_ANALYZING_HEART_RATE));
                //AMainActivity.webviewHandler.obtainMessage(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_ANALYZING_HEART_RATE, "").sendToTarget();

                try {
                    Thread.sleep(ECG_SLEEP);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_HEARTBEAT_DETECTED));
                //AMainActivity.webviewHandler.obtainMessage(AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_HEARTBEAT_DETECTED, "").sendToTarget();

            }

            @Override
            public void onCompleted(Object object) {

                //Update UI if needed OR TO do anything on completion
                if(object!=null && object instanceof  LocalBroadcastManager)
                    log(TAG + " All Done");
            }

            @Override
            public void onException(Exception exception) {
                //Deal here if you got any exception in your background Thread
                if(exception!=null && exception instanceof Exception)
                    log(TAG + exception.getCause());
            }
        });
        asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);
    }


    @JavascriptInterface
    public void connectToEcgForSessionActivation(final String mac) {

        if (AMainApplication.DO_BLUETOOTH) // BLUETOOTH
        {
            DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {
                @Override
                public Object doInBackgroundThread(DefaultAsyncTask httpAsyncTask) {

                    //Do heavy/background Task here
                    SynergenBleClientSessionActivation.connectToStlDevice(mContext, mac);
                    return mContext;
                }

                @Override
                public void onCompleted(Object object) {

                    //Update UI if needed OR TO do anything on completion
                    if(object!=null && object instanceof  Context)
                        log(TAG + " All Done");
                }

                @Override
                public void onException(Exception exception) {
                    //Deal here if you got any exception in your background Thread
                    if(exception!=null && exception instanceof Exception)
                        log(TAG + exception.getCause());
                }
            });
            asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);

        }
        else // SIMULATE
        {
            connectToEcgForSessionActivation_simulate(mac);
        }
    }

    @JavascriptInterface
    public void atHomeScreen() {
        log("At home screen at UI ... ");


        // notify internet connectivity
        Bundle b = new Bundle();
        boolean internetAvailable = isInternetAvailable();
        b.putBoolean("internetAvailable", internetAvailable);
        Intent uiIntent = AMainActivity.createBTIntent(AMainActivity.WEBVIEW_HOME_SCREEN_INTERNET_CONNECTIVITY_STATE_UPDATE, b);
        LocalBroadcastManager.getInstance(AMainApplication.getAppContext()).sendBroadcast(uiIntent);
        log("Notifying internet connectivity: internetAvailable = " + internetAvailable);

        // notify mobile battery
        int mobileBattery = getCurrentMobileBattery();
        boolean powerConnected = isMobilePowerConnected(AMainApplication.getAppContext());
        Intent notificationIntent = MobileBatteryLevelReceiver.getNotificationIntent(powerConnected, mobileBattery, null);
        LocalBroadcastManager.getInstance(AMainApplication.getAppContext()).sendBroadcast(notificationIntent);
        log("Notifying mobile battery: mobile battery = " + mobileBattery + ", poweredConnected = " + powerConnected);


    }




    @JavascriptInterface
    public void atHomeScreenJustActivatedSession() {

        log("::atHomeScreenJustActivatedSession()");

        log("====== SESSION-START-STATE ==============\n" + ObjectUtils.toJson(SessionStateAccess.getCurrentSessionState(mContext)) + "\n=========================");

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.SESSION_JUST_ACTIVATED));

    }

    @JavascriptInterface
    public void onSignalViewScreen() {

        log("Enabling passing data to UI");
        AMainService.sendToSignalView = true;

        if (!AMainApplication.DO_BLUETOOTH) {
            //LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_LE_DEVICE_EMULATE_BT_SIGNAL));
        }
    }

    @JavascriptInterface
    public void offSignalViewScreen() {

        log("Disabling passing data to UI");
        AMainService.sendToSignalView = false;

        if (!AMainApplication.DO_BLUETOOTH) {
            //LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_LE_DEVICE_STOP_EMULATE_BT_SIGNAL));
        }
    }

    @JavascriptInterface
    public boolean isInternetAvailable() {

        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @JavascriptInterface
    public int getCurrentMobileBattery() {


        int mobileBattery = -1;

        Intent batteryIntent = mContext.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            mobileBattery = 50;
        }

        mobileBattery = (int) (((float) level / (float) scale) * 100.0f);

        return mobileBattery;

    }


    public static boolean isMobilePowerConnected(Context context){
        boolean charging = false;

        final Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean batteryCharge = status==BatteryManager.BATTERY_STATUS_CHARGING;

        int chargePlug = batteryIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

        if (batteryCharge) charging=true;
        if (usbCharge) charging=true;
        if (acCharge) charging=true;

        return charging;
    }

    @JavascriptInterface
    public String getCurrentMobileNetworkInfo() {

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        /**
         * WIFI
         */

        /** Check the connection **/

        NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        // Make sure the network is available
        if (wifi != null && wifi.isAvailable() && wifi.isConnectedOrConnecting()) {
            WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            log("WiFi on");
            return "WiFi on <br />" + wifiInfo.getSSID();
        }

        NetworkInfo network = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        // Make sure the network is available
        if (network != null && network.isAvailable() && network.isConnectedOrConnecting()) {

            log("NETWORK AVAILABLE");
        } else {
            log("NO NETWORK");
            return "NO NETWORK";
        }

        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String msisdn = telephonyManager.getLine1Number();
        String carrier = telephonyManager.getNetworkOperatorName();

        String networkClass = getNetworkClass(telephonyManager.getNetworkType());

        if (networkClass.equals("Unknown"))
            return "Unknown";

        return networkClass + " on <br />" + carrier;
    }


    private static String getNetworkClass(int networkType) {

        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2G";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "3G";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "4G LTE";
            default:
                return "Unknown";
        }
    }


    @JavascriptInterface
    public void prepareForNewSession(final String jsCallbackFn) {

        DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {
            @Override
            public Object doInBackgroundThread(DefaultAsyncTask httpAsyncTask) {

                //Do heavy/background Task here
                // AMainApplication.showShortToast("Cleaning Up Old Session Data ... ");
                log("Cleaning to maintain 3 most recent sessions in list");
                SessionListAccess.cleanToKeepLastThreeSessions(mContext);


                // AMainApplication.showShortToast("Resetting State ... ");
                SessionStateAccess.saveSessionStateJson(mContext, "{}");

                Bundle b = new Bundle();
                String fnCallbackFull = jsCallbackFn + "()";
                b.putString("callbackFnString", fnCallbackFull);
                return LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
            }

            @Override
            public void onCompleted(Object object) {

                //Update UI if needed OR TO do anything on completion
                if(object!=null && object instanceof LocalBroadcastManager)
                    log(TAG + " All Done");
            }

            @Override
            public void onException(Exception exception) {
                //Deal here if you got any exception in your background Thread
                if(exception!=null && exception instanceof Exception)
                    log(TAG + exception.getCause());
            }
        });
        asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);
    }



    ////////////////////////////////////////
    ////////////////////////////////////////
    ////
    ////     SESSION ACTIVATION API CALLS
    ////
    ////////////////////////////////////////
    ////////////////////////////////////////

    @JavascriptInterface
    public void API_reportPatientWithoutSymptomsCall() {

        DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {
            @Override
            public Object doInBackgroundThread(DefaultAsyncTask httpAsyncTask) {

                //Do heavy/background Task here

                if(AMainApplication.PURE_FIFO_CHUNK_TRANSFER) {
                    PatientReportedEventsApi.getInstance().storePteWithoutSymtoms();
                    HttpQueueManager.getInstance().notifyFifoPteBufferedHandler();
                }
                else {
                    PatientReportedEventsApi.getInstance().reportWithoutSymptoms();
                    HttpQueueManager.getInstance().notifyPteBufferedHandler();
                }

                return null;
            }

            @Override
            public void onCompleted(Object object) {

                //Update UI if needed OR TO do anything on completion
                //if(object!=null && object instanceof  BasePreferenceHelper)

                log(TAG + " All Done");
            }

            @Override
            public void onException(Exception exception) {
                //Deal here if you got any exception in your background Thread
                if(exception!=null && exception instanceof Exception)
                    log(TAG + exception.getCause());
            }
        });
        asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);

    }

    @JavascriptInterface
    public void API_reportPatientSymptomsCall(final String symptoms) {

        DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {
            @Override
            public Object doInBackgroundThread(DefaultAsyncTask httpAsyncTask) {

                //Do heavy/background Task here

                if(AMainApplication.PURE_FIFO_CHUNK_TRANSFER) {
                    PatientReportedEventsApi.getInstance().storePteWithSymptoms(symptoms);
                    HttpQueueManager.getInstance().notifyFifoPteBufferedHandler();
                } else {
                    PatientReportedEventsApi.getInstance().reportWithSymptoms(symptoms);
                    HttpQueueManager.getInstance().notifyPteBufferedHandler();
                }


                return null;
            }

            @Override
            public void onCompleted(Object object) {

                //Update UI if needed OR TO do anything on completion
                //if(object!=null && object instanceof  BasePreferenceHelper)

                log(TAG + " All Done");
            }

            @Override
            public void onException(Exception exception) {
                //Deal here if you got any exception in your background Thread
                if(exception!=null && exception instanceof Exception)
                    log(TAG + exception.getCause());
            }
        });
        asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);

    }

    /**
     * API to start session.
     *
     * @param jsCallbackFn
     */
    @JavascriptInterface
    public void API_startSessionCall(final String jsCallbackFn) {


        DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {
            @Override
            public Object doInBackgroundThread(DefaultAsyncTask httpAsyncTask) {

                //Do heavy/background Task here
                int sessionID = SessionStateAccess.getCurrentSessionId(mContext);
                log("API_startSessionCall: SessionID = " + sessionID);
                SessionActivationApi sessionActivationApi = SessionActivationApi.getInstance(mContext);
                sessionActivationApi.startSessionCall(sessionID + "", DeviceIdentifiers.getEcgId(), DeviceIdentifiers.getPdaId(), EcgProperties.DEVICE_CHUNK_SIZE, jsCallbackFn);
                return sessionActivationApi;
            }

            @Override
            public void onCompleted(Object object) {

                //Update UI if needed OR TO do anything on completion
                if(object!=null && object instanceof  SessionActivationApi)
                    log("API_startSessionCall: " + " All Done");


            }

            @Override
            public void onException(Exception exception) {
                //Deal here if you got any exception in your background Thread
                if(exception!=null && exception instanceof Exception)
                    log(TAG + exception.getCause());
            }
        });
        asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Get Enrollment Information by Activation Code ...
     *
     * @param activationCode The activation code for the enrollment
     * @param jsCallbackFn   The javascript function to call after completion
     */
    @JavascriptInterface
    public void API_getEnrollmentByActivationCode(final String activationCode, final String jsCallbackFn) {

        DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {
            @Override
            public Object doInBackgroundThread(DefaultAsyncTask httpAsyncTask) {


                if(!NetworkHelper.isInternetAvailable(mContext))
                {
                    String fnCallbackFull = jsCallbackFn + "('NO_INTERNET')";

                    Bundle b = new Bundle();
                    b.putString("callbackFnString", fnCallbackFull);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
                    return "NO_INTERNET";
                }


                return SessionActivationApi.getInstance(mContext).getEnrollmentByActivationCode(activationCode, jsCallbackFn);
            }

            @Override
            public void onCompleted(Object object) {

                if(object!=null && object instanceof SessionActivationApi)
                    log(" All Done");
            }

            @Override
            public void onException(Exception exception) {

                if(exception!=null && exception instanceof Exception) {
                    exception.printStackTrace();
                    log("Exception: " + exception.getCause());
                }
            }
        });
        asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);

    }


    // ALTERNATIVE PATH

    /**
     * Get Enrollment Information by Activation Code ...
     *
     * @param facilityCode The facility code for the facility
     * @param jsCallbackFn The javascript function to call after completion
     */
    @JavascriptInterface
    public void API_getFacilityNameByFacilityCode(final String facilityCode, final String jsCallbackFn) {

        DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {
            @Override
            public Object doInBackgroundThread(DefaultAsyncTask httpAsyncTask) {


                if(!NetworkHelper.isInternetAvailable(mContext))
                {
                    String fnCallbackFull = jsCallbackFn + "('NO_INTERNET')";

                    Bundle b = new Bundle();
                    b.putString("callbackFnString", fnCallbackFull);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
                    return "NO_INTERNET";
                }



                Bundle b = null;
                String jsonStr = "";
                try {



                    if(sessionActivationApiOld==null){
                        sessionActivationApiOld= new SessionActivationApiOld(httpsHandler);
                    }
                    String facilityName = null;
                    if(facilityCode!=null) {
                        facilityName = SessionActivationApi.getInstance().getFacilityNameByFacilityCode(facilityCode);
                    }
                    if (facilityName == ""){
                        throw new IOException("Error in getting FacilityName, please try again");
                    }
                    log("Facility Name: " + facilityName);
                    String fnCallbackFull = jsCallbackFn + "('" + facilityName + "')";

                    b = new Bundle();
                    b.putString("callbackFnString", fnCallbackFull);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));

                    //return jsonStr;
                } catch (JSONException e) {
                    jsonStr = "JSONException: " + e.getMessage();
                    log(jsonStr);
                    e.printStackTrace();
                } catch (IOException e) {
                    jsonStr = "IOException: " + e.getMessage();
                    log(jsonStr);
                    b = new Bundle();
                    String fnCallbackFull = jsCallbackFn + "(" + null + ")";
                    b.putString("callbackFnString", fnCallbackFull);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
                    e.printStackTrace();
                }
                return b;
            }

            @Override
            public void onCompleted(Object object) {

                //Update UI if needed OR TO do anything on completion
                if(object!=null && object instanceof  Bundle)
                    log(TAG + " All Done");
            }

            @Override
            public void onException(Exception exception) {
                //Deal here if you got any exception in your background Thread
                if(exception != null)
                    log(TAG + exception.getCause());
            }
        });
        asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);
    }


    /**
     * Get Enrollment Information by Activation Code ...
     *
     * @param facilityCode The facility code for the facility
     * @param jsCallbackFn The javascript function to call after completion
     */
    @JavascriptInterface
    public void API_getListOfEnrolledPatients(final String enrollmentDateIso8601, final String facilityCode, final String jsCallbackFn) {

        log("Enr Date: " + enrollmentDateIso8601 + ", facilityCode: " + facilityCode);

        final DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {
            @Override
            public Object doInBackgroundThread(DefaultAsyncTask httpAsyncTask) {

                if(!NetworkHelper.isInternetAvailable(mContext))
                {
                    String fnCallbackFull = jsCallbackFn + "('NO_INTERNET')";

                    Bundle b = new Bundle();
                    b.putString("callbackFnString", fnCallbackFull);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
                    return "NO_INTERNET";
                }


                Bundle b = null;

                String jsonStr = "";
                try {

                    EnrollmentInfo[] enrollmentInfoArr = SessionActivationApi.getInstance().getPatientListByEnrollmentDate(enrollmentDateIso8601, facilityCode);

                    String fnCallbackFull = jsCallbackFn + "(" + ObjectUtils.toJson(enrollmentInfoArr) + ")";
                    b = new Bundle();
                    b.putString("callbackFnString", fnCallbackFull);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));

                } catch (ParseException | JSONException | IOException e) {
                    jsonStr = "UserException: " + e.getMessage();
                    log(jsonStr);
                    b = new Bundle();
                    String fnCallbackFull = jsCallbackFn + "(" + null + ")";
                    b.putString("callbackFnString", fnCallbackFull);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
                    e.printStackTrace();
                }

                return b;
            }

            @Override
            public void onCompleted(Object object) {

                //Update UI if needed OR TO do anything on completion
                if(object!=null && object instanceof  Bundle)
                    log(TAG + " All Done");


            }

            @Override
            public void onException(Exception exception) {
                //Deal here if you got any exception in your background Thread

                exception.printStackTrace();

                if(exception!=null)
                    log(TAG + exception.getCause());

            }
        });
        asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);
    }



    @JavascriptInterface
    public void captureSignature(final String jsString, final String jsCallbackFn) {

        DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {
            @Override
            public Object doInBackgroundThread(DefaultAsyncTask httpAsyncTask) {

                String sessionId = SessionStateAccess.getCurrentSessionId(mContext) + "";
                String patientName = SessionStateAccess.getCurrentSessionState(mContext).patientName;
                String appVersionId = DeviceIdentifiers.getAppVersionId(mContext);

                AMainApplication.currentSignature = AMainSignatureCapturer.applyWatermarkAndJpegCompress(jsString, DeviceIdentifiers.getPdaId(), patientName, appVersionId);


                /*String fName = "Session_" + sessionId + "_eSignature.jpg";
                File f = new File(AMainApplication.APP_STORAGE_PATH, fName);
                try {

                    FileOutputStream fout = new FileOutputStream(f);

                    fout.write(AMainApplication.currentSignature);
                    fout.flush();
                    fout.close();

                    log("Signature File Written to " + AMainApplication.APP_STORAGE_PATH + File.separator + fName);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    return null;
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }*/


                String fnCallbackFull = jsCallbackFn + "()";

                Bundle b = new Bundle();
                b.putString("callbackFnString", fnCallbackFull);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));


                return AMainApplication.currentSignature;

            }

            @Override
            public void onCompleted(Object object) {

                //Update UI if needed OR TO do anything on completion
                if(object!=null && object instanceof  Bundle)
                    log(" All Done");
            }

            @Override
            public void onException(Exception exception) {

                exception.printStackTrace();
                //Deal here if you got any exception in your background Thread
                if(exception!=null)
                    log("" + exception.getCause());

            }
        });
        asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);


    }



    @JavascriptInterface
    public void unlockDevice() {

        log("Unlocking Device");



    }

}
