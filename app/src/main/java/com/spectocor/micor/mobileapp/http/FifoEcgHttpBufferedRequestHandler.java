package com.spectocor.micor.mobileapp.http;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.sessionlist.SessionListAccess;
import com.spectocor.micor.mobileapp.amain.sessionlist.SessionListSessionInfo;
import com.spectocor.micor.mobileapp.amain.sessionlist.SessionTypeEnum;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLog;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogEvents;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.http.common.ResponseCodes;
import com.spectocor.micor.mobileapp.http.requests.EcgChunkHttpRequest;
import com.spectocor.micor.mobileapp.sessionactivation.api.SessionActivationApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Savio Monteiro on 3/1/2016.
 */
public class FifoEcgHttpBufferedRequestHandler extends Handler {

    public static final String TAG = FifoEcgHttpBufferedRequestHandler.class.getSimpleName();

    private boolean msgBeingHandled = false;
    public boolean sessionTerminated = false;

    public FifoEcgHttpBufferedRequestHandler(Looper looper){
        super(looper);

        sessionTerminated = false;
    }

    private void log(String str) {
        Log.v(TAG, str);
    }



    @Override
    public void handleMessage(Message msg) {

        Log.v(TAG, " ====== handling notification");

        if(msgBeingHandled) {

            Log.v(TAG, "Message While Loop is already handling current data ... returning");

            return;
        }


        Log.v(TAG, "FIFO Message is now being handled...");

        try {
            EcgDataChunk ecgDataChunk;
            Context context = AMainApplication.getAppContext();


            while (true)
            {
                msgBeingHandled = true;

                try {
                    ecgDataChunk = EcgChunkDbHelper.getInstance(AMainApplication.getAppContext()).retrieveOldestNotTransmittedChunk();
                }
                catch (SQLDataException e) {

                    if(e.getMessage().equals("NONE_LEFT_TO_TRANSFER")) {

                        Log.v(TAG, "No more chunks left to transfer");

                        msgBeingHandled = false;

                        break;
                    }
                    else {
                        ecgDataChunk = null;

                    }
                }


                // TODO: To check and see if ecgDataChunk startTimestamp is > sessionEndTimestamp


                if (ecgDataChunk == null) {

                    Log.v(TAG, "EcgChunk is null, we assume there are no remaining entries in db to be send, finishing loop.");

                    msgBeingHandled = false;

                    break;

                }

                long chunkSize = (ecgDataChunk.numberOfSamples*1000/EcgProperties.SAMPLE_RATE);

                RequestBody requestBody = new MultipartBody.Builder()
                                            .addFormDataPart("sessionId", "" + ecgDataChunk.monitoringSessionId)
                                            .addFormDataPart("chunkStartTimestamp", "" + ecgDataChunk.chunkStartDateUnixMs)
                                            .addFormDataPart("ecgId", DeviceIdentifiers.getEcgId())
                                            .addFormDataPart("deviceId", DeviceIdentifiers.getPdaId())
                                            .addFormDataPart("sampleRate", EcgProperties.SAMPLE_RATE + "")
                                            .addFormDataPart("numberOfSamples", ecgDataChunk.numberOfSamples + "")
                                            .addFormDataPart("chunkSize", chunkSize + "")
                                            .addFormDataPart("file", "data.comp", RequestBody.create(MediaType.parse("application/octet-stream"), ecgDataChunk.chunkDataChannelCompressed))
                                            .setType(MultipartBody.FORM)
                                            .build();


                String requestParamsStr =   "sessionId: " + ecgDataChunk.monitoringSessionId +
                                            ", chunkStartTimestamp: " +  ecgDataChunk.chunkStartDateUnixMs +
                                            ", ecgId: " + DeviceIdentifiers.getEcgId() +
                                            ", deviceId: " + DeviceIdentifiers.getPdaId() +
                                            ", sampleRate: " + EcgProperties.SAMPLE_RATE + "" +
                                            ", numberOfSamples: " + ecgDataChunk.numberOfSamples + "" +
                                            ", chunkSize: " + chunkSize + "";

                Log.v(TAG, "Request created with params: " + requestParamsStr);

                Log.v(TAG, "Multipart ECG Chunk request body created for " + EcgChunkHttpRequest.class.getSimpleName()
                        + " with filename data.comp \n(start ts: " + ecgDataChunk.chunkStartDateUnixMs + ") (numberOfSamples: " + ecgDataChunk.numberOfSamples + ", chunkSize: " + chunkSize + ")"
                        + " (ecgId: " + DeviceIdentifiers.getEcgId() + ", pdaId: " + DeviceIdentifiers.getPdaId());

                Request request = new Request.Builder()
                        .header("Authorization", "Basic c2lyYWo6dGVzdDEyMw==")
                        .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_ECG_CHUNK)
                        .post(requestBody)
                        .build();

                Response response;
                Call c = HttpManager.getInstance().getHttpClient().newCall(request);
                response = c.execute();

                String responseBody = response.body().string();

                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                Object obj = gson.fromJson(responseBody, Object.class);
                Log.v(TAG, "CHUNK TRANSFER RESPONSE: " + gson.toJson(obj));

                JSONObject responseJSON = new JSONObject(responseBody);
                int responseCode = responseJSON.getInt("code");

                Log.v(TAG, "StatusCode: " + responseCode + ", ResponseBody: " + responseBody);


                // TODO: Remove this
                //////////////////////////////
                /*long currTimestamp = System.currentTimeMillis();
                log(currTimestamp + " vs " + AMainApplication.TERMINATE_SESSION_AT);
                if(currTimestamp >= AMainApplication.TERMINATE_SESSION_AT && AMainApplication.TERMINATE_SESSION_AT != -1)
                {
                    log(currTimestamp + " > " + AMainApplication.TERMINATE_SESSION_AT);
                    log("Simulating session expired");
                    responseCode = ResponseCodes.SESSION_EXPIRED;
                }*/
                //////////////////////////////

                if (responseCode == ResponseCodes.OK) {

                    Log.v(TAG, "Response is successful, finishing request.");

                    // SET TO TRANSMITTED ONLY WHEN SUCCESSFUL
                    Log.v(TAG, "Setting EcgChunk as transmitted in database... ");
                    setTransmittedTimestampForChunkInDb(ecgDataChunk);

                    Log.v(TAG, "EcgChunk entry marked as sent in database.");

                } // END: 200-OK

                else if (responseCode == ResponseCodes.DEVICE_NO_LONGER_ACTIVE) {

                    // TRANSMITTED BUT INDICATES NO MORE CHUNKS
                    setTransmittedTimestampForChunkInDb(ecgDataChunk);

                    DeviceLogEvents.getInstance().putEcgTransmissionError(requestParamsStr, responseBody, responseCode);

                    // MAKE BROADCAST TO DEVICE_NO_LONGER_ACTIVE
                    Intent intent = new Intent("BLUETOOTH_EVENT");
                    intent.putExtra("EVENT_TYPE", AMainActivity.SESSION_DEVICE_NO_LONGER_ACTIVE);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                } // END: IF-DEVICE-NO-LONGER-ACTIVE


                else if (responseCode == ResponseCodes.SESSION_EXPIRED) {

                    // setting logs
                    DeviceLogEvents.getInstance().putEcgTransmissionError(requestParamsStr, responseBody, responseCode);

                    // TRANSMITTED BUT INDICATES NO MORE CHUNKS
                    setTransmittedTimestampForChunkInDb(ecgDataChunk);

                    handleSessionExpired(ecgDataChunk.monitoringSessionId, context);


                } // END: IF-SESSION-EXPIRED

                else if (responseCode == ResponseCodes.SESSION_DOES_NOT_EXIST) {

                    // TRANSMITTED BUT INDICATES NO MORE CHUNKS
                    setTransmittedTimestampForChunkInDb(ecgDataChunk);

                    DeviceLogEvents.getInstance().putEcgTransmissionError(requestParamsStr, responseBody, responseCode);

                    Log.v(TAG, "Session Does Not Exist");

                } // END: IF-SESSION-DOES-NOT-EXIST

                else if (responseCode == ResponseCodes.DUPLICATE_CHUNK_SENT) {

                    // TRANSMITTED BUT INDICATES NO MORE CHUNKS
                    setTransmittedTimestampForChunkInDb(ecgDataChunk);

                    DeviceLogEvents.getInstance().putEcgTransmissionError(requestParamsStr, responseBody, responseCode);

                    Log.v(TAG, "Duplicate detected on server. Setting as transmitted.");

                } // END: IF-DUPLICATE-CHUNK-SENT

                else {
                    Log.v(TAG, "There was an error sending buffered ecg chunk. ");

                    DeviceLogEvents.getInstance().putEcgTransmissionError(requestParamsStr, responseBody, responseCode);

                    msgBeingHandled = false;

                    break;
                }

            } // end while

            Log.v(TAG, "End of While Loop");
            msgBeingHandled = false;

        } catch (IOException e) {

            Log.v(TAG, "Error while sending buffered ecg chunk over the network!");

            DeviceLogEvents.getInstance().putEcgTransmissionError("IOException", ObjectUtils.toJson(e.getStackTrace()), -1);

            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
            DeviceLogEvents.getInstance().putEcgTransmissionError("JSONException", ObjectUtils.toJson(e.getStackTrace()), -1);
        }

        Log.v(TAG, "Message is not longer being handled! ");
        msgBeingHandled = false;

    } // end: handleMessage



    public void setTransmittedTimestampForChunkInDb(final EcgDataChunk ecgDataChunk) {

        Log.v(TAG, "Setting transmitted timestamp to chunk: (" + ecgDataChunk.getMonitoringSessionId() + ", " + ecgDataChunk.getChunkStartDateUnixMs()+ ")");

        EcgChunkDbHelper.getInstance(AMainApplication.getAppContext()).setEcgChunkAsTransmitted(
                ecgDataChunk.getMonitoringSessionId(), ecgDataChunk.getChunkStartDateUnixMs()
        );
    }





    public void switchToHolterPlus(final int nextSessionId, final long prevSessionEndTimestamp, Context context) {

        log("HOLTER SESSION JUST TERMINATED. Starting telemetry session.");

        /*final int nextSessionId = intent.getIntExtra("nextSessionId", -1);
        final long prevSessionEndTimestamp = intent.getLongExtra("prevSessionEndTimestamp", -1);*/

        log("Activating Next SessionID: " + nextSessionId);

        String ecgId = DeviceIdentifiers.getEcgId() + "";
        String pdaId = DeviceIdentifiers.getPdaId() + "";

        String response = SessionActivationApi.getInstance(context).startSessionCall(nextSessionId + "", ecgId, pdaId, EcgProperties.DEVICE_CHUNK_SIZE);

        log("Session Start Response: " + response);

        if(response != "success") {

            log("Response is not success");

            if(response.equals("session_expired")) {

                log("Handling expiry for " + nextSessionId);

                handleSessionExpired(nextSessionId, context);
            }

            return;
        }


        log(" ");
        log("=========================================================");
        log("  NEXT SESSION STARTED FOR " + nextSessionId);
        log("================================================");

        /*LiveEcgChunkInMemory.initAll();
        OldEcgChunkHandler.getInstance(getApplicationContext()).endInstance();*/


        log("Current State ... ");
        CurrentSessionState currSessionState = SessionStateAccess.getCurrentSessionState(context);


        log("Setting end timestamp for " + currSessionState.sessionId + " (started at: " + currSessionState.sessionStartTimestamp + ") to " + nextSessionId);
        SessionListAccess.setSessionEndTimestampForSession(currSessionState.sessionId, currSessionState.sessionStartTimestamp, prevSessionEndTimestamp, context);



        ///// NEW SESSION UPDATES HERE /////


        // Updating current session Id to nextSessionId
        SessionStateAccess.setSessionId(nextSessionId, context);

        log("Setting sessionID of all chunks whose startTimestamps are greater than " + prevSessionEndTimestamp + " to " + nextSessionId);
        EcgChunkDbHelper.getInstance(context).overrideEcgChunkSessionIds(prevSessionEndTimestamp, nextSessionId);


        final long nextSessionStartTimestamp = System.currentTimeMillis();
        log("Setting Next Session Start Timestamp = " + nextSessionId);

        // adding new session to session List
        SessionListSessionInfo nextSessionInfo = new SessionListSessionInfo(nextSessionId, SessionTypeEnum.SESSION_TYPE_TELEMETRY, nextSessionStartTimestamp, -1);
        SessionListAccess.addSessionToList(nextSessionInfo, context);

        // Updating database path to new session ID
        //SessionStarterKit.initDatabaseNameAndPath(AMainApplication.DB_PATH, nextSessionInfo, context);

    }






    public void handleSessionExpired(int sessionId, Context context) {

        // TODO: Check this
        if(sessionTerminated) {

            log("@handleSessionExpired(..). Session Already Terminated. Returning.");
            return;
        }


        // if session id pertaining to the chunk is NOT notified for expiration
        //if(SessionStateAccess.getSessionTerminationTimestamp(ecgDataChunk.monitoringSessionId, context) == -1) {

        long prevSessionEndTimestamp = System.currentTimeMillis() - 1;


        // notifying current session for expiration
        SessionStateAccess.setSessionTerminationTimestamp(sessionId, context);
        SessionStateAccess.setSessionExpired(sessionId, context);

        // getting next session id
        int nextSessionId = SessionStateAccess.getNextSessionId(context);
        Log.v(TAG, "NEXT SESSION ID: " + nextSessionId);

        // There is another session to activate? SWITCH TO NEW SESSION
        if(nextSessionId > 0)
        {

            Log.v(TAG, "Activating Next SessionID: " + nextSessionId);

            switchToHolterPlus(nextSessionId, prevSessionEndTimestamp, context);

        }
        else // No more sessions to activate. Terminating.
        {
            // MAKE BROADCAST TO SESSION_TERMINATED
            Log.v(TAG, "Calling SESSION_JUST_TERMINATED");
            Intent intent = new Intent("BLUETOOTH_EVENT");
            intent.putExtra("EVENT_TYPE", AMainActivity.SESSION_JUST_TERMINATED);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

            sessionTerminated = true;
        }
    }



}
