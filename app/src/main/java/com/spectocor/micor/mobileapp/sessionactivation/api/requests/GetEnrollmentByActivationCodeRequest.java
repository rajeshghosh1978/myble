package com.spectocor.micor.mobileapp.sessionactivation.api.requests;

/**
 * Gets enrollment by activation code request
 */
@SuppressWarnings("unused")
public class GetEnrollmentByActivationCodeRequest {


    /**
     * Activation code to be sent to the Server
     */
    private final String activationCode;


    public GetEnrollmentByActivationCodeRequest(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getActivationCode() {
        return activationCode;
    }

}
