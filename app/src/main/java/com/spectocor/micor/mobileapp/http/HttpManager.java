package com.spectocor.micor.mobileapp.http;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Class which contains configuration and initialization of network client (OkHttp).
 *
 * Created by Rafay Ali on 1/25/2016.
 */
public class HttpManager {

    public static final String TAG = HttpManager.class.getSimpleName();

    public static final long CONNECT_TIMEOUT = 30;
    public static final long WRITE_TIMEOUT = 30;
    public static final long READ_TIMEOUT = 30;

    private static HttpManager httpManager = null;
    private OkHttpClient okHttpClient = null;

    private HttpManager(){ }

    public static HttpManager getInstance(){
        if (httpManager == null){
            httpManager = new HttpManager();
        }

        return httpManager;
    }

    public OkHttpClient getHttpClient(){
        if (okHttpClient == null){
            okHttpClient = new OkHttpClient.Builder()
                    .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                    .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .build();
        }

        return okHttpClient;
    }
}
