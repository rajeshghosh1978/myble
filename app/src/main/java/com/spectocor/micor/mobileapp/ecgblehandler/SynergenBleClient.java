package com.spectocor.micor.mobileapp.ecgblehandler;

import android.content.Context;

import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.stl.bleservice.BluetoothComm;
import com.stl.bleservice.Device;
import com.stl.bleservice.STLBLE;

/**
 * Created by Savio Monteiro on 1/15/2016.
 */
public class SynergenBleClient {

    public static boolean bleServiceInitialized = false;
    public static boolean bleInstanceActive = false;
    public static boolean bleConnected = false;
    public static String currentMac = "";

    private static BluetoothComm btComm = null;

    private static Context mContext;


    private static void log(String str) {
        MLog.log("SynergenBleClient: " + str);
    }

    // to start connectivity from service
    public static void startConnectivity(Context context, String mac) {

        currentMac = mac;

        if(bleInstanceActive)
        {
            log("BLE Already Active ... doing nothing");
            return;
        }

        if(!SessionStateAccess.getCurrentSessionState(context).sessionActive)
        {
            log("Session Not Active. Returning ... ");
            return;
        }

        registerAndConnect(context);

    }

    // recursive calls to handle disconnection and closed ...
    protected static void registerAndConnect(Context context) {


        bleInstanceActive = true;

        mContext = context;

        currentMac = "B0:B4:48:C4:A9:81"; //currentMac;

        log("Registering and Connecting to " + currentMac);


        log("Scanning for " + currentMac);
        STLBLE.getInstance(mContext).ScanForDevices(bleScannerCallback);

    }

    private static STLBLE.ScannerCallBack bleScannerCallback = new STLBLE.ScannerCallBack() {


        @Override
        public void onScanFinished(STLBLE.ScanResult scanResult) {

            boolean found = false;

            for (Device dev : scanResult.getDeviceList()) {

                log("Found: " + dev.getDeviceName() + ", " + dev.getAddress());

                if (dev.getAddress().contains(currentMac)) {

                    found = true;

                    log("Our Device Found: " + dev.getAddress());

                    String btName = dev.getDeviceName() + "";
                    String btMac = dev.getAddress() + "";
                    int rssi = dev.getRssi() + 0;

                    log("Found: " + btName + " (RSSI: " + rssi + ")");

                    btComm = dev.getBluetoothComm(mContext);
                    connectToBt(btComm, mContext);

                }
            }

            if (!found) {

                log("Our Device not found ... ");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                STLBLE.getInstance(mContext).ScanForDevices(bleScannerCallback);
            }
        }
    };



    private static SynergenEventCallback synergenEventCallback;

    /**
     * Connect to Bluetooth Device
     * @param btComm Bluetooth Comm Instance of Device found
     */
    private static void connectToBt(final BluetoothComm btComm, final Context context) {

        log("Registering Event Callback");

        synergenEventCallback = SynergenEventCallback.getInstance(context, btComm);

        log("Connecting ... ");

        btComm.connect();

    }

}
