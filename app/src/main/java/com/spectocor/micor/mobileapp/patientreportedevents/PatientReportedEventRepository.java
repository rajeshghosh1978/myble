package com.spectocor.micor.mobileapp.patientreportedevents;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.spectocor.micor.mobileapp.common.RepositoryBase;
import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;

/**
 * Repository to access database PatientReportedEvent table
 */
public class PatientReportedEventRepository extends RepositoryBase<PatientReportedEvent> {

    public static final String TABLE_NAME = "PatientReportedEvent";

    public static final String COLUMN_MONITORING_SESSION_ID = "monitoringSessionId";
    public static final String COLUMN_LOG_DATE_UNIX_SEC = "LogDateUnixSec";
    public static final String COLUMN_PATIENT_REPORTED_EVENT_TYPE_ID = "PatientReportedEventTypeId";
    public static final String COLUMN_COMMENT = "Comment";
    /**
     * SQLite script to create this table
     */
    public static final String CREATE_TABLE_SQL_SCRIPT = "create table " + TABLE_NAME + " ("
            + COLUMN_LOG_DATE_UNIX_SEC + " INTEGER PRIMARY KEY,"
            + COLUMN_MONITORING_SESSION_ID + " INT,"   //TODO: Retrieve monitoring session from monitoring session table
            + COLUMN_PATIENT_REPORTED_EVENT_TYPE_ID + " SMALLINT,"
            + COLUMN_COMMENT + " TEXT"
            + ");";

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param databaseHelper database helper
     */
    public PatientReportedEventRepository(LogEventsDatabaseHelper databaseHelper) {
        super(databaseHelper, TABLE_NAME, COLUMN_LOG_DATE_UNIX_SEC);

    }


    /**
     * get content values for insert
     *
     * @param obj item to be converted
     * @return content values used with SQLite
     */
    @NonNull
    private ContentValues getContentValues(PatientReportedEvent obj) {
        ContentValues values = new ContentValues();

        values.put(COLUMN_MONITORING_SESSION_ID, obj.getMonitoringSessionId());
        values.put(COLUMN_LOG_DATE_UNIX_SEC, obj.getLogDateUnixSec());

        //if (obj.getPatientReportedEventInfoTypeId() != null)
        values.put(COLUMN_PATIENT_REPORTED_EVENT_TYPE_ID, obj.getPatientReportedEventTypeId().getValue());
        //if (obj.getValueString() != null)
        values.put(COLUMN_COMMENT, obj.getComment());

        return values;
    }

    /**
     * Reads data from a cursor and converts it to the actual object
     *
     * @param cursor android Database Cursor
     * @return actual object
     */
    protected PatientReportedEvent getObjectByCursor(Cursor cursor) {
        PatientReportedEvent obj = new PatientReportedEvent();

        obj.setMonitoringSessionId(cursor.getInt(cursor.getColumnIndex(COLUMN_MONITORING_SESSION_ID)));
        obj.setLogDateUnixSec(cursor.getInt(cursor.getColumnIndex(COLUMN_LOG_DATE_UNIX_SEC)));
        obj.setPatientReportedEventTypeId(EnumPatientReportedEventType.forValue(cursor.getInt(cursor.getColumnIndex(COLUMN_PATIENT_REPORTED_EVENT_TYPE_ID))));
        obj.setComment(cursor.getString(cursor.getColumnIndex(COLUMN_COMMENT)));

        return obj;
    }


    @Override
    protected long getPrimaryKey(PatientReportedEvent obj) {
        return obj.getLogDateUnixSec();
    }

    @Override
    protected void setPrimaryKey(PatientReportedEvent obj, long newPrimaryKey) {
        obj.setLogDateUnixSec((int) newPrimaryKey);
    }

    @NonNull
    @Override
    protected ContentValues getInsertContentValues(PatientReportedEvent obj) {
        return getContentValues(obj);
    }

    @NonNull
    @Override
    protected ContentValues getUpdateContentValues(PatientReportedEvent obj) {
        return getContentValues(obj);
    }


}
