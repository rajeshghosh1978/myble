package com.spectocor.micor.mobileapp.utils.arrays;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * A set of methods to convert from an array of bytes (byte[]) to other primitive types.
 * Created by Savio Monteiro on 11/19/15.
 */
public class FromByteArray {

    /**
     * Converts a byte array to a double array.
     * @param byteArr The input byte array
     * @return The double array
     */
    public static double[] toDoubleArray(final byte[] byteArr) {
        double doubleArr[];
        ArrayList<Double> darrL = new ArrayList<>();
        ByteBuffer bbuff = ByteBuffer.wrap(byteArr);
        while (bbuff.hasRemaining()) {
            darrL.add(bbuff.getDouble());
        }
        doubleArr = new double[darrL.size()];
        for (int i = 0; i < darrL.size(); i++) {
            doubleArr[i] = darrL.get(i);
        }
        return doubleArr;
    }


    /**
     * Converts a byte array to a short array.
     * @param byteArr The input byte array
     * @return The short array
     */
    public static short[] toShortArray(final byte[] byteArr) {
        short shortArr[];
        ArrayList<Short> sarrL = new ArrayList<>();
        ByteBuffer bbuff = ByteBuffer.wrap(byteArr);
        while (bbuff.hasRemaining()) {
            sarrL.add(bbuff.getShort());
        }
        shortArr = new short[sarrL.size()];
        for (int i = 0; i < sarrL.size(); i++) {
            shortArr[i] = sarrL.get(i);
        }
        return shortArr;
    }

}
