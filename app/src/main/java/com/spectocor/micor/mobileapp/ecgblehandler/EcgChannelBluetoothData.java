package com.spectocor.micor.mobileapp.ecgblehandler;

import java.util.ArrayList;
import java.util.List;

/**
 * This class holds ecg data received each time from the ECG Bluetooth Device
 * Created by Savio Monteiro on 1/12/2016.
 */
public class EcgChannelBluetoothData
{

    public short[] ch1;
    public short[] ch2;
    public long timestamp;

    public EcgChannelBluetoothData(final long timestamp, final short[] c1, final short[] c2) {
        this.ch1 = c1;
        this.ch2 = c2;
        this.timestamp = timestamp;
    }


    public EcgChannelBluetoothData(final long timestamp, final ArrayList<Short> c1, final ArrayList<Short> c2) {

        this.timestamp = timestamp;

        // channel 1
        ch1 = new short[c1.size()];
        int i1 = 0;
        for(Short sample: c1) {
            this.ch1[i1] = sample;
            i1++;
        }

        // channel 2
        ch2 = new short[c2.size()];
        int i2 = 0;
        for(Short sample: c2) {
            this.ch2[i2] = sample;
            i2++;
        }
    }

    /*
    public EcgChannelBluetoothData(final long timestamp, final List<Integer> c1, final List<Integer> c2) {

        this.timestamp = timestamp;

        // channel 1
        ch1 = new short[c1.size()];
        int i1 = 0;
        for(Integer sample: c1) {
            this.ch1[i1] = (short) ((sample - 65535)); // * (2.5f / 10f));
            i1++;
        }

        // channel 2
        ch2 = new short[c2.size()];
        int i2 = 0;
        for(Integer sample: c2) {
            this.ch2[i2] = (short) ((sample - 65535)); // * (2.5f / 10f));
            i2++;
        }
    }*/
}

