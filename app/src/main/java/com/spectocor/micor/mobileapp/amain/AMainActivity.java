package com.spectocor.micor.mobileapp.amain;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.spectocor.micor.mobileapp.R;
import com.spectocor.micor.mobileapp.amain.receivers.AlertPlayer;
import com.spectocor.micor.mobileapp.amain.sessionlist.SessionListAccess;
import com.spectocor.micor.mobileapp.amain.sessionlist.SessionListSessionInfo;
import com.spectocor.micor.mobileapp.amain.sessionlist.SessionTypeEnum;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogsDbHelper;
import com.spectocor.micor.mobileapp.ecgadapter.LiveEcgChunkInMemory;
import com.spectocor.micor.mobileapp.ecgadapter.OldEcgChunkHandler;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.http.HttpQueueManager;
import com.spectocor.micor.mobileapp.sessionactivation.api.SessionActivationApi;
import com.spectocor.micor.mobileapp.threading.DefaultAsyncListener;
import com.spectocor.micor.mobileapp.threading.DefaultAsyncTask;
import com.splunk.mint.Mint;

import java.lang.ref.WeakReference;
import java.sql.SQLDataException;

//Rajesh changed some code on 03-30-2016
//Rajesh changed some code again on same date
public class AMainActivity extends Activity {

    private static final String TAG = "AMainActivity";

    public WebView mWebView;

    static boolean mBound = false;
    AMainService mService;

    public static boolean active = false;

    String activeSessionID;

    public static Handler activityHandler;


    //Lock Mobile Owner Initiation
    public DevicePolicyManager dpm;
    private ComponentName deviceAdmin;
    //Lock ===========================


    public static void log(String str) {
        MLog.log("AMainActivity: " + str);
    }


    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }


    private void setWindowParams() {

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    protected void turnScreenOn() {

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.screenBrightness = -1;

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        window.setAttributes(params);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setWindowParams();
        turnScreenOn();

    }


    private final int REQUEST_CODE_ASK_PERMISSIONS = 404;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);


        setWindowParams();
        turnScreenOn();


        log("Started Activity");
        setContentView(R.layout.activity_session_activation);

        Mint.initAndStartSession(AMainActivity.this, "8039610c");


        if (BluetoothAdapter.getDefaultAdapter() != null) {
            if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                BluetoothAdapter.getDefaultAdapter().enable();
            }
        }

        initWebview();


        LocalBroadcastManager.getInstance(this).registerReceiver(btMesgReceiver,
                new IntentFilter("BLUETOOTH_EVENT"));


        // creating handler for few events
        activityHandler = new AMainActivityHandler(new WeakReference<>(this));



        /*if(SessionStateAccess.getCurrentSessionState(this).sessionActive)
        {
            log("Session Active. Starting Services");
            AppServiceManager.startHttpService();
            AppServiceManager.startAMainService();
        }*/


        bindToAMainService();


        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               String forceCrash =null;
                if(forceCrash.isEmpty())
                {

                }

            }
        },5*1000);*/


    }

    private ServiceConnection serviceConn = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            if (name.equals(new ComponentName(getApplicationContext(), AMainService.class))) {
                log("Bound to main service ... ");

                mService = ((AMainService.LocalBinder) binder).getService();
                mBound = true;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            log("Disconnected from Service ... ");
            mBound = false;
            mService = null;

            log("Attempting to bind to service again ... ");
            Intent serviceIntent = new Intent(getApplicationContext(), AMainService.class);
            if (isMyServiceRunning(AMainService.class)) {
                if (serviceConn != null)
                    bindService(serviceIntent, serviceConn, BIND_AUTO_CREATE);
            }
        }
    };

    private void bindToAMainService() {
        Intent aMainServiceIntent = new Intent(this, AMainService.class);
        if (isMyServiceRunning(AMainService.class)) {
            log("Binding to AMainService");
            if (serviceConn != null) {
                try {
                    bindService(aMainServiceIntent, serviceConn, BIND_AUTO_CREATE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * Initializing the WebView.
     */
    private void initWebview() {

        jsInterface = new AMainJSInterface(this);

        // initialize webview
        mWebView = (WebView) findViewById(R.id.sessionActivationWebView);

        // Find the root view
        View root = mWebView.getRootView();

        // Set the color
        root.setBackgroundColor(getResources().getColor(android.R.color.black));

        mWebView.addJavascriptInterface(jsInterface, "MicorAndroid");
        mWebView.setBackgroundColor(Color.parseColor("#ffffff"));
        mWebView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        mWebView.setLongClickable(false);

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, final String url) {

                return true;
            }

        });

        mWebView.setWebChromeClient(new WebChromeClient() {
        });

        // set javascript interfaces ...

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);

        mWebView.loadUrl("file:///android_asset/micor_html/index.html");

    }


    @Override
    public void onBackPressed() {

    }

    /*@Override
    protected void onPause() {



    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        log("AMainActivity:onDestroy()");

        if (mBound) {
            unbindService(serviceConn);
            mBound = false;
        }

        AMainService.sendToSignalView = false;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(btMesgReceiver);
    }


    ////////////////////////////////////////////////
    ////////////////////////////////////////////////
    ////                                        ////
    ////    Handlers and Broadcast Receivers    ////
    ////                                        ////
    ////////////////////////////////////////////////
    ////////////////////////////////////////////////


    public static final int WEBVIEW_MSG_ALERT = 1;
    public static final int WEBVIEW_DEVICE_LOOKUP_COMPLETE = 2;
    public static final int WEBVIEW_LE_DEVICE_FOUND = 4;
    public static final int WEBVIEW_SESSION_ACTIVATION_LE_DEVICES_FOUND = 41;
    public static final int WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTING = 5;
    public static final int WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTED = 6;
    public static final int WEBVIEW_SIGNAL_VIEW_LE_DEVICE_CONNECTED2 = 61;
    public static final int WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_DISCONNECTED = 7;
    public static final int WEBVIEW_SIGNAL_VIEW_LE_DEVICE_DISCONNECTED2 = 71;
    public static final int WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_ELECTRODES_ATTACHED = 8;
    public static final int WEBVIEW_LE_DEVICE_ELECTRODES_UNATTACHED = 9;
    public static final int WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_RECEIVING_SIGNAL = 10;
    public static final int WEBVIEW_SIGNAL_VIEW_LE_DEVICE_NEW_HEART_RATE = 11;
    public static final int WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_ANALYZING_HEART_RATE = 12;
    public static final int WEBVIEW_SIGNAL_VIEW_LE_DEVICE_POUND_THE_HEART = 112;
    public static final int WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_HEARTBEAT_DETECTED = 13;
    public static final int WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_COULD_NOT_DETECT_HEARTBEAT = 130;
    public static final int WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_STARTED_DISCOVERY = 14;
    public static final int WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_ENDED_DISCOVERY = 15;
    public static final int WEBVIEW_SIGNAL_VIEW_LE_ECG_CH1_SAMPLES_READY = 16;
    public static final int WEBVIEW_SIGNAL_VIEW_LE_ECG_CH2_SAMPLES_READY = 17;
    public static final int WEBVIEW_SIGNAL_VIEW_LE_ECG_SAMPLES_READY = 177;
    public static final int WEBVIEW_LE_ECG_NEW_BATTERY_VALUE = 18;

    public static final int WEBVIEW_LE_DEVICE_EMULATE_BT_SIGNAL = 21;
    public static final int WEBVIEW_LE_DEVICE_STOP_EMULATE_BT_SIGNAL = 22;

    public static final int WEBVIEW_JS_CALLBACK = 23;

    public static final int WEBVIEW_MOBILE_BATTERY_UPDATE = 24;

    public static final int SESSION_JUST_ACTIVATED = 25;
    public static final int SESSION_JUST_TERMINATED = 30;
    public static final int SESSION_HOLTER_FINISHED_TELEMETRY_STARTS = 29;
    public static final int SESSION_DEVICE_NO_LONGER_ACTIVE = 32;


    public static final int DISPLAY_SESSION_TERMINATION_SCREEN = 33;
    public static final int DISPLAY_DEVICE_NO_LONGER_ACTIVE_SCREEN = 34;

    public static final int WEBVIEW_HOME_SCREEN_INTERNET_CONNECTIVITY_STATE_UPDATE = 50;


    private AMainJSInterface jsInterface;

    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver btMesgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {

            if (intent.getAction().equals("BLUETOOTH_EVENT")) {

                log("Received Broadcast Intent: " + intent.getIntExtra("EVENT_TYPE", -1));
                log("Received Broadcast Intent: " + intent.getAction());

                // Get extra data included in the Intent
                int eventType = intent.getIntExtra("EVENT_TYPE", -1);


                switch (eventType) {

                    case SESSION_JUST_ACTIVATED: {

                        log("SESSION_JUST_ACTIVATED received");

                        // SETTING STATE PARAMETERS

                        log("Current State.");
                        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(context);

                        final long sessionStartTimestamp = System.currentTimeMillis();
                        log("Setting Session Start Timestamp = " + sessionStartTimestamp + " for session id: " + sessionState.sessionId);

                        sessionState.sessionStartTimestamp = sessionStartTimestamp;


                        if (sessionState != null) {
                            SessionStateAccess.saveSessionStateJson(context, ObjectUtils.toJson(sessionState));
                        }
                        // UPDATING SESSION LIST IN THE DEVICE
                        SessionListSessionInfo sessionInfo = new SessionListSessionInfo(sessionState.sessionId, SessionTypeEnum.SESSION_TYPE_TELEMETRY, sessionState.sessionStartTimestamp, -1);
                        if (sessionInfo != null) {
                            SessionListAccess.addSessionToList(sessionInfo, context);
                            SessionStarterKit.initDatabaseNameAndPath(AMainApplication.DB_PATH, sessionInfo, context);
                        }



                        LiveEcgChunkInMemory.initAll();
                        OldEcgChunkHandler.getInstance(getApplicationContext()).endInstance();


                        // STARTING HTTP SERVICE
                        AppServiceManager.startHttpService();
                        log("Started HttpQueueService.");

                        HttpQueueManager.getInstance().initServiceConnection();

                        CountDownTimer cdt = new CountDownTimer(5000, 5000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                            }

                            @Override
                            public void onFinish() {
                                HttpQueueManager.getInstance().initAllHandlers();
                            }
                        };
                        cdt.start();



                        // STARTING BLUETOOTH SERVICE
                        AppServiceManager.startAMainService();

                        if (!mBound)
                            bindToAMainService();


                        if (mBound) {
                            log("starting bluetooth connectivity from service");
                            if (mService != null) {

                                //log("Starting transmission queue thread from activity");
                                //mService.startTransmissionQueueThread();

                                log("Starting bluetooth connectivity client");
                                mService.startBluetoothConnectivity();
                            } else {
                                log("Could not start bluetooth connectivity. mService is null.");

                            }
                        }


                        if (sessionState != null && sessionState.getSessionId() != -1) {
                            try {
                                DeviceLogsDbHelper.getInstance().updateSessionInfo(String.valueOf(sessionState.getSessionId()));
                            } catch (SQLDataException exception) {

                            }
                        }

                        AMainApplication.startFifoAlarm();


                        // TODO: Remove this
                        //AMainApplication.TERMINATE_SESSION_AT = System.currentTimeMillis() + AMainApplication.TERMINATION_AFTER_MS;

                        break;
                    }


                    case SESSION_HOLTER_FINISHED_TELEMETRY_STARTS: {

                        log("HOLTER SESSION JUST TERMINATED. Starting telemetry session.");


                        // TODO: Make sure all data from previous session are sent to server


                        final int nextSessionId = intent.getIntExtra("nextSessionId", -1);
                        final long prevSessionEndTimestamp = intent.getLongExtra("prevSessionEndTimestamp", -1);

                        log("Activating Next SessionID: " + nextSessionId);

                        if (nextSessionId == -1) {
                            return;
                        }


                        // TODO: make sessionStart call
                        // make session start call with new id without UI update
                        DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {

                            @Override
                            public Object doInBackgroundThread(DefaultAsyncTask asyncTask) {

                                String ecgId = DeviceIdentifiers.getEcgId() + "";
                                String pdaId = DeviceIdentifiers.getPdaId() + "";

                                String response = SessionActivationApi.getInstance(context).startSessionCall(nextSessionId + "", ecgId, pdaId, EcgProperties.DEVICE_CHUNK_SIZE);

                                return response;
                            }

                            @Override
                            public void onCompleted(Object object) {

                                log(" ");
                                log("=========================================================");
                                log("  NEXT SESSION STARTED FOR " + nextSessionId);
                                log("================================================");

                                /*LiveEcgChunkInMemory.initAll();
                                OldEcgChunkHandler.getInstance(getApplicationContext()).endInstance();*/


                                log("Current State ... ");
                                CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(context);


                                log("Setting sessionID of all chunks whose startTimestamps are greater than " + prevSessionEndTimestamp + " to " + nextSessionId);
                                EcgChunkDbHelper.getInstance(context).overrideEcgChunkSessionIds(prevSessionEndTimestamp, nextSessionId);


                                log("Setting end timestamp for " + sessionState.sessionId + " (started at: " + sessionState.sessionStartTimestamp + ") to " + nextSessionId);
                                SessionListAccess.setSessionEndTimestampForSession(sessionState.sessionId, sessionState.sessionStartTimestamp, System.currentTimeMillis(), context);

                                // Updating current session Id to nextSessionId
                                SessionStateAccess.setSessionId(nextSessionId, context);


                                final long nextSessionStartTimestamp = System.currentTimeMillis();
                                log("Setting Next Session Start Timestamp = " + nextSessionId);

                                // adding new session to session List
                                SessionListSessionInfo nextSessionInfo = new SessionListSessionInfo(nextSessionId, SessionTypeEnum.SESSION_TYPE_TELEMETRY, nextSessionStartTimestamp, -1);
                                SessionListAccess.addSessionToList(nextSessionInfo, context);

                                // Updating database path to new session ID
                                //SessionStarterKit.initDatabaseNameAndPath(AMainApplication.DB_PATH, nextSessionInfo, context);

                            }

                            @Override
                            public void onException(Exception exception) {

                            }
                        });
                        asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);


                        // if(successful)
                        // ... continue ... else break;


                        break;
                    }


                    case SESSION_JUST_TERMINATED: {

                        log("SESSION JUST TERMINATED");



                        SessionStateAccess.setSessionTerminatedStillTransmitting(context);


                        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(context);

                        if (sessionState != null) {

                            SessionListAccess.setSessionEndTimestampForSession(sessionState.sessionId, sessionState.sessionStartTimestamp, System.currentTimeMillis(), context);
                        }

                        final int currSessionId = sessionState.sessionId;

                        LiveEcgChunkInMemory.initAll();
                        OldEcgChunkHandler.getInstance(getApplicationContext()).endInstance();

                        if (!mBound)
                            bindToAMainService();

                        // Turning off bluetooth and transmission from their service(s)
                        if (mBound) {

                            log("ending bluetooth connectivity from service");
                            if (mService != null) {

                                log("Ending transmission queue thread from activity");
                                //mService.endTransmissionQueueThread();

                                log("Ending bluetooth connectivity client");
                                mService.endBluetoothConnectivity();


                            } else {
                                log("mService is null.");

                                // TODO:
                            }
                        } else {
                            log("NOT BOUND to AMainService!!! WHOA!");
                        }


                        log("Turning off transmission for current session");

                        SessionStateAccess.setSessionTerminatedAndInactive(context);
                        SessionStateAccess.setSessionExpired(currSessionId, context);

                        // Turn off all transmission here


                        if (AMainApplication.PURE_FIFO_CHUNK_TRANSFER) {
                            HttpQueueManager.getInstance().stopFifoEcgBufferedHandler();
                            HttpQueueManager.getInstance().stopFifoPteBufferedHandler();
                        }
                        else {
                            HttpQueueManager.getInstance().stopBufferedHandler();
                            HttpQueueManager.getInstance().stopPteBufferedHandler();
                        }

                        //AppServiceManager.stopService(HttpQueueService.class);
                        //HttpQueueManager.getInstance().stopFifoEcgBufferedHandler();
                        //HttpQueueManager.getInstance().stopFifoEcgBufferedHandler();

                        AppServiceManager.stopService(AMainService.class);


                        //log("Cleaning databases for last 3 sessions");
                        //SessionListAccess.cleanToKeepLastThreeSessions(context);


                        log("Determining whether to alert patient or no");
                        AlertPlayer.alertForTermination(context);

                        break;
                    }


                    case DISPLAY_SESSION_TERMINATION_SCREEN: {

                        mWebView.loadUrl("javascript:endSession()");

                        break;
                    }


                    // when a device is replaced, the current device needs to stop doing whatever it is
                    case SESSION_DEVICE_NO_LONGER_ACTIVE: {

                        log("DEVICE NO LONGER ACTIVE");

                        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(context);
                        if (sessionState != null) {
                            SessionListAccess.setSessionEndTimestampForSession(sessionState.sessionId, sessionState.sessionStartTimestamp, System.currentTimeMillis(), context);
                        }

                        LiveEcgChunkInMemory.initAll();
                        OldEcgChunkHandler.getInstance(getApplicationContext()).endInstance();


                        // Turning off bluetooth and transmission from their service(s)
                        if (mBound) {

                            log("ending bluetooth connectivity from service");
                            if (mService != null) {

                                log("Ending transmission queue thread from activity");
                                //mService.endTransmissionQueueThread();

                                log("Ending bluetooth connectivity client");
                                mService.endBluetoothConnectivity();


                            } else {
                                log("mService is null.");
                                // TODO:
                            }
                        }


                        log("Finished Countdown. Turning off transmission for current session.");

                        SessionStateAccess.setDeviceDeactivated(context);


                        if (AMainApplication.PURE_FIFO_CHUNK_TRANSFER) {
                            HttpQueueManager.getInstance().stopFifoEcgBufferedHandler();
                            HttpQueueManager.getInstance().stopFifoPteBufferedHandler();
                        }
                        else {
                            HttpQueueManager.getInstance().stopBufferedHandler();
                            HttpQueueManager.getInstance().stopPteBufferedHandler();
                        }

                        //AppServiceManager.stopService(HttpQueueService.class);
                        AppServiceManager.stopService(AMainService.class);

                        log("Determining whether to alert patient or no");
                        AlertPlayer.alertForDeactivation(context);


                        break;
                    }


                    case DISPLAY_DEVICE_NO_LONGER_ACTIVE_SCREEN: {

                        mWebView.loadUrl("javascript:deviceNoLongerActive()");

                        break;
                    }


                    case WEBVIEW_JS_CALLBACK: {
                        String callbackFnString = intent.getStringExtra("callbackFnString");
                        log("Calling back: " + callbackFnString);
                        mWebView.loadUrl("javascript: " + callbackFnString);
                        break;
                    }




                    /*case WEBVIEW_LE_DEVICE_FOUND: {

                        String callbackFnString = intent.getStringExtra("callbackFnString");
                        String deviceFoundStr = intent.getStringExtra("deviceFoundStr");

                        log("LE Device Found: Calling JS Fn: " + callbackFnString);

                        mWebView.loadUrl("javascript: " + callbackFnString + "('" + deviceFoundStr + "')");
                        break;
                    }*/


                    // SESSION_ACTIVATION BROADCASTS
                    case WEBVIEW_SESSION_ACTIVATION_LE_DEVICES_FOUND: {
                        String callbackFnString = intent.getStringExtra("callbackFnString");
                        String devicesFoundJSONStr = intent.getStringExtra("stlDevicesFoundJson");
                        log("LE Devices Found: Calling JS Fn: " + callbackFnString);
                        mWebView.loadUrl("javascript: " + callbackFnString + "('" + devicesFoundJSONStr + "')");
                        break;
                    }


                    case WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_STARTED_DISCOVERY: {
                        mWebView.loadUrl("javascript: setIsDiscoveringLE();");
                        break;
                    }

                    case WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_ENDED_DISCOVERY: {
                        mWebView.loadUrl("javascript: setIsNotDiscoveringLE();");
                        break;
                    }

                    case WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTING: {
                        mWebView.loadUrl("javascript: setEcgStateConnecting();");
                        break;
                    }


                    case WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTED: {
                        log("Received Broadcast ... ");
                        //if (jsInterface.getCurrentSessionState().sessionActive)
                        //    mWebView.loadUrl("javascript: psOnBtConnected();");
                        //else
                        mWebView.loadUrl("javascript: setEcgStateConnected();");
                        //break;
                    }

                    case WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_ELECTRODES_ATTACHED: {
                        mWebView.loadUrl("javascript: setEcgStateElectrodesDetected();");
                        break;
                    }

                    case WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_RECEIVING_SIGNAL: {
                        mWebView.loadUrl("javascript: setEcgStateReceivingSignal();");
                        break;
                    }

                    case WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_ANALYZING_HEART_RATE: {
                        mWebView.loadUrl("javascript: setEcgStateAnalyzingHeartRate();");
                        break;
                    }

                    case WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_COULD_NOT_DETECT_HEARTBEAT: {
                        mWebView.loadUrl("javascript: setEcgStateCouldNotDetectHeartBeat();");
                        break;
                    }

                    case WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_HEARTBEAT_DETECTED: {
                        mWebView.loadUrl("javascript: setEcgStateHeartBeatDetected();");
                        break;
                    }

                    case WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_DISCONNECTED: {
                        //if (jsInterface.getCurrentSessionState().sessionActive)
                        //    mWebView.loadUrl("javascript: psOnBtDisconnected();");
                        //else
                        mWebView.loadUrl("javascript: setEcgStateDisconnected();");
                        break;
                    }


                    case WEBVIEW_LE_ECG_NEW_BATTERY_VALUE: {
                        int newEcgBattery = intent.getExtras().getInt("newEcgBattery", (byte) -1);
                        mWebView.loadUrl("javascript: psOnBtBatteryValue(" + newEcgBattery + ");");
                        break;
                    }


                    // SIGNAL VIEW RELATED

                    case WEBVIEW_SIGNAL_VIEW_LE_DEVICE_NEW_HEART_RATE: {
                        int newHr = intent.getIntExtra("newHr", -1);
                        log("Received HR: " + newHr);
                        mWebView.loadUrl("javascript: setNewHeartRate(" + newHr + ");");
                        break;
                    }

                    case WEBVIEW_SIGNAL_VIEW_LE_DEVICE_POUND_THE_HEART: {
                        log("Beat Detected ...  beating the heart");
                        mWebView.loadUrl("javascript: psOnEcgBeat();");
                        break;
                    }


                    case WEBVIEW_SIGNAL_VIEW_LE_DEVICE_DISCONNECTED2: {
                        mWebView.loadUrl("javascript: psOnBtDisconnected();");
                        break;
                    }

                    case WEBVIEW_SIGNAL_VIEW_LE_DEVICE_CONNECTED2: {
                        mWebView.loadUrl("javascript: psOnBtConnected();");
                        break;
                    }

                    case WEBVIEW_SIGNAL_VIEW_LE_ECG_CH1_SAMPLES_READY: {
                        //log("Received Ch1 at " + System.currentTimeMillis());
                        if (AMainService.sendToSignalView) {
                            String ecgCh1PktJson = intent.getStringExtra("samplesCh1Json");
                            mWebView.loadUrl("javascript: Sig.updateCh1(" + ecgCh1PktJson + ");");
                        }
                        break;
                    }

                    case WEBVIEW_SIGNAL_VIEW_LE_ECG_CH2_SAMPLES_READY: {
                        if (AMainService.sendToSignalView) {
                            String ecgCh2PktJson = intent.getStringExtra("samplesCh2Json");
                            mWebView.loadUrl("javascript: Sig.updateCh2(" + ecgCh2PktJson + ");");
                        }
                        break;
                    }

                    case WEBVIEW_SIGNAL_VIEW_LE_ECG_SAMPLES_READY: {
                        if (AMainService.sendToSignalView) {
                            String ecgCh1PktJson = intent.getStringExtra("samplesCh1Json");
                            String ecgCh2PktJson = intent.getStringExtra("samplesCh2Json");

                            //mWebView.loadUrl("javascript: Sig.updateCh2(" + ecgCh2PktJson + ");");
                            mWebView.loadUrl("javascript: Sig.updateBothChannels(" + ecgCh1PktJson + ", " + ecgCh2PktJson + ");");
                        }
                        break;
                    }


                    case WEBVIEW_HOME_SCREEN_INTERNET_CONNECTIVITY_STATE_UPDATE: {

                        boolean internetAvailable = intent.getBooleanExtra("internetAvailable", true);
                        log("Received internet connectivity status: " + internetAvailable);

                        if (internetAvailable)
                            mWebView.loadUrl("javascript:psOnInternetConnected()");
                        else
                            mWebView.loadUrl("javascript:psOnInternetDisconnected()");

                        break;
                    }


                    case WEBVIEW_MOBILE_BATTERY_UPDATE: {
                        int battery = intent.getIntExtra("mobileBattery", -1);
                        boolean powerConnected = intent.getBooleanExtra("powerConnected", false);
                        log("Calling JS: " + "psMobileBatteryChanged(" + battery + ", " + powerConnected + ")");

                        mWebView.loadUrl("javascript:psOnMobileBatteryChanged(" + battery + "," + powerConnected + ")");

                        break;
                    }


                }


            }
        }
    };

    /**
     * To check if a service is running or not
     *
     * @param serviceClass
     * @return
     */
    public static boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) AMainApplication.getAppContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    // TODO: Remove for production. For testing.
    // for test purposes only
    public void callWebviewJsFunction(String jsFunctionStr) {
        activityHandler.obtainMessage(AMainActivityHandler.ACTIVITY_JSTESTFUNCTION, jsFunctionStr).sendToTarget();
    }


    public static Intent createBTIntent(int btEventType, Bundle b) {
        // createBTIntent
        Intent intent = new Intent("BLUETOOTH_EVENT");
        intent.putExtra("EVENT_TYPE", btEventType);
        intent.putExtras(b);
        return intent;
    }

}



