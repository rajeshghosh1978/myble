package com.spectocor.micor.mobileapp.devicelogs;

import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.common.exceptions.DataAccessException;
import com.spectocor.micor.mobileapp.common.queuesystem.QueueItem;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;
import com.spectocor.micor.mobileapp.common.queuesystem.QueueItemWithStatus;

/**
 * Device log service to log all device functions
 */
@SuppressWarnings("WeakerAccess")
public class DeviceLogLogger {

    private static final String TAG = "DeviceLogLogger";

    protected final DeviceLogRepository repository;
    protected final GlobalSettings settings;
    protected final DeviceLogQueueRepository queueRepository;

    public DeviceLogLogger(DeviceLogRepository deviceLogRepository,
                           GlobalSettings globalSettings,
                           DeviceLogQueueRepository deviceLogQueueRepository) {
        repository = deviceLogRepository;
        settings = globalSettings;
        queueRepository = deviceLogQueueRepository;
    }


    /**
     * inserts a log to the database. It sets monitoring session id and necessary parameters
     * it also puts the log in the queue to be sent to the server
     *
     * @param obj device log
     * @return inserted device log to the database
     */
    public DeviceLog insertLog(DeviceLog obj) {
        obj.setMonitoringSessionId(settings.getMonitoringSession());
        try {
            long newId = repository.insert(obj);
            //queueRepository.insert(new QueueItem(newId));
            queueRepository.insert(new QueueItemWithStatus(newId));
        } catch (DataAccessException ex) {
            Log.e(TAG, "insertLog failed", ex);
            // when we can't write a log, since it is the write class
        }
        return obj;
    }


//    /**
//     * Logs if bluetooth is on or off
//     *
//     * @param isOn if bluetooth on the device is on or off
//     */
//    public DeviceLog LogMobileDeviceBluetoothOnOff(boolean isOn) {
//        double valueNumber = 0; // is off
//        if (isOn)
//            valueNumber = 1;
//        DeviceLog obj = new DeviceLog(settings.getMonitoringSession(), EnumDeviceLogInfoType.MobileDeviceBluetoothOnOff, valueNumber);
//        repository.insert(obj);
//        return obj;
//    }

}
