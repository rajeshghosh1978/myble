package com.spectocor.micor.mobileapp.amain.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;

/**
 * Created by Savio Monteiro on 2/1/2016.
 */
public class CellInternetConnectivityReceiver extends BroadcastReceiver {

    public static final String INTERNET_CONNECTIVITY_BROADCAST_ACTION = "com.spectocor.micor.mobileapp.INTERNET_CONNECTIVITY_BROADCAST_ACTION";

    public static final String INTERNET_CONNECTIVITY_BOOL = "INTERNET_CONNECTIVITY_BOOL";

    private static void log(String str)
    {
        MLog.log(CellInternetConnectivityReceiver.class.getSimpleName() + ": NotificationReceiver: " + str);
    }

    /**
     * Create the broadcast intent to consume
     *
     * @return
     */
    public static Intent getNotificationIntent(boolean internetConnected, Bundle bundle)
    {

        Intent intent = new Intent(INTERNET_CONNECTIVITY_BROADCAST_ACTION);

        intent.putExtra(INTERNET_CONNECTIVITY_BOOL, internetConnected);

        return intent;
    }

    private static boolean internetAvailable = false;
    private static boolean wasInternetAvailablePreviously = false;

    /**
     * Will receive this broadcast message only on network change based on
     * whatAMainBroadcastReceiver receives ...
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent)
    {

        if(intent.getAction().equals(INTERNET_CONNECTIVITY_BROADCAST_ACTION))
        {
            log("internetAvailable = " + internetAvailable);
            internetAvailable = intent.getBooleanExtra(INTERNET_CONNECTIVITY_BOOL, false);

            checkAlertWorthiness(internetAvailable, context);

        }
    }


    private static long internetWentDownAt = -1;
    private void checkAlertWorthiness(boolean internetAvailable, Context context)
    {
        if(wasInternetAvailablePreviously != internetAvailable) // to only detect changes ...
        {
            if(!internetAvailable) // INTERNET NOT AVAILABLE
            {

                if(internetWentDownAt == -1) {
                    internetWentDownAt = System.currentTimeMillis();

                    log("Check loop for internet connectivity resurrection!");

                    if(SessionStateAccess.getCurrentSessionState(context).sessionActive)
                        checkIfInternetIsStillDown(context);
                }

            }
            else // INTERNET AVAILABLE
            {
                internetWentDownAt = -1;
                alertPlayedOnce = false;

                if(disconnectedAlertLooper != null) {
                    disconnectedAlertLooper.interrupt();
                    disconnectedAlertLooper = null;
                }
            }

            wasInternetAvailablePreviously = internetAvailable;
        }
    }


    private static Thread disconnectedAlertLooper = null;

    private static int disconnectAlertLoopsFor = 60 * 1000;
    private static boolean alertPlayedOnce = false;

    /**
     * Upon disconnection will check to see if internet is available every minute,
     * upto 8 hours. Once it crosses 8 hours there will be a notification
     * to show the customer service page and checks will be made every 20 minutes.
     * @param context
     */
    private void checkIfInternetIsStillDown(final Context context)
    {

        disconnectedAlertLooper = new Thread(new Runnable() {

            @Override
            public void run() {

                log("Waiting for " + disconnectAlertLoopsFor);
                try {
                    Thread.sleep(disconnectAlertLoopsFor);

                } catch (InterruptedException e) {

                    log("Disconnection Checker Thread cancelled");

                    e.printStackTrace();

                    log("Disconnection Checker Thread Cancelled! Returning!");

                    return;
                }

                if(internetAvailable) {
                    log("Internet is back ... returning from looper");
                    alertPlayedOnce = false;
                    return;
                }


                if((System.currentTimeMillis() - internetWentDownAt) > (8 * 60 * 60 * 1000)) {

                    AlertPlayer.showToast("Internet down for more than 8 hours.", context);
                    if(!alertPlayedOnce) {
                        AlertPlayer.playVibrateAlert(context);
                        alertPlayedOnce = true;
                    }
                    AlertPlayer.displayCustomerServiceNoInternet(internetWentDownAt, context);

                    // LOOP 20 MINS AFTER 8 HRS OF DISCONN.
                    disconnectAlertLoopsFor = 20 * 60 * 1000;
                }

                if(SessionStateAccess.getCurrentSessionState(context).sessionActive) {
                    run();
                }

            }
        });

        if(SessionStateAccess.getCurrentSessionState(context).sessionActive) {
            disconnectedAlertLooper.start();
        }

    }


    /**
     * Get the current mobile battery level value
     * @param context
     * @return
     */
    public static int getCurrentMobileBattery(final Context context) {

        int mobileBattery = -1;

        Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if(level == -1 || scale == -1) {
            mobileBattery = 50;
        }

        mobileBattery = (int) (((float)level / (float)scale) * 100.0f);

        /*Intent battUiIntent = new Intent("BLUETOOTH_EVENT");
        battUiIntent.putExtra("EVENT_TYPE", AMainActivity.WEBVIEW_MOBILE_BATTERY_UPDATE);
        battUiIntent.putExtra("mobileBattery", mobileBattery);

        LocalBroadcastManager.getInstance(context).sendBroadcast(battUiIntent);*/

        return mobileBattery;
    }

    /**
     * Get current Mobile cellular/wifi network information
     * @param context
     * @return
     */
    public static String getCurrentMobileNetworkInfo(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        /**
         * WIFI
         */

        /** Check the connection **/

        NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        // Make sure the network is available
        if(wifi != null && wifi.isAvailable() && wifi.isConnectedOrConnecting()) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            return "WiFi on <br />" + wifiInfo.getSSID();
        }

        NetworkInfo network = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        // Make sure the network is available
        if(network != null && network.isAvailable() && network.isConnectedOrConnecting()) {

        }
        else
        {
            return "NO NETWORK";
        }

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String msisdn = telephonyManager.getLine1Number();
        String carrier = telephonyManager.getNetworkOperatorName();

        String networkClass = getNetworkClass(telephonyManager.getNetworkType());

        if(networkClass.equals("Unknown"))
            return "Unknown";

        return networkClass + " on <br />" + carrier;
    }

    /**
     * Get the Cellular Network Class based on the NetworkType reported by Telephony manager
     * @param networkType
     * @return
     */
    private static String getNetworkClass(int networkType) {

        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2G";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "3G";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "4G LTE";
            default:
                return "Unknown";
        }
    }
}
