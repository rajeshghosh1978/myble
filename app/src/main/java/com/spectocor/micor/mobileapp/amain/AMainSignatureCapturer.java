package com.spectocor.micor.mobileapp.amain;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Savio Monteiro on 3/22/2016.
 */
public class AMainSignatureCapturer
{

    private static void log(String str) {
        MLog.log(AMainSignatureCapturer.class.getSimpleName() + ": " + str);
    }


    public static byte[] applyWatermarkAndJpegCompress(String canvasDataUrlStr, String deviceId, String patientName, String appVersion) {


        String imageDataBytes = canvasDataUrlStr.substring(canvasDataUrlStr.indexOf(",") + 1);
        byte[] decodedString = Base64.decode(imageDataBytes, Base64.DEFAULT);

        final Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);

        //Watermark
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setAlpha(255);
        paint.setTextSize(25);
        paint.setAntiAlias(true);

        String device_id = "Device ID : " + deviceId;

        long timestamp = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date resultdate = new Date(timestamp);
        String timesampStr = "Date & Time : " + sdf.format(resultdate);

        String appVersionStr = "App Version : " + appVersion;

        String name = "Name: " + patientName;

        float offset = 25;

        //Device ID
        float x,y;
        x = offset;
        y = offset;
        canvas.drawText(device_id, x, y, paint);

        x = (bitmap.getWidth() - paint.measureText(appVersionStr)) - offset;
        y = offset;
        canvas.drawText(appVersionStr, x, y, paint);

        x = offset;
        y = (bitmap.getHeight() - ((paint.descent() + paint.ascent()) / 2)) - offset;
        canvas.drawText(timesampStr, x, y, paint);

        x = (bitmap.getWidth() - paint.measureText(name)) - offset;
        y = (bitmap.getHeight() - ((paint.descent() + paint.ascent()) / 2)) - offset;
        canvas.drawText(name, x, y, paint);


        canvas.drawBitmap(bitmap, 0, 0, null);


        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] jpegBytes = stream.toByteArray();
        try {
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        log(jpegBytes.length + " bytes captured");

        return jpegBytes;
    }

}
