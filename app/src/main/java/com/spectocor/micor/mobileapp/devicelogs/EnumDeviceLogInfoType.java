package com.spectocor.micor.mobileapp.devicelogs;

import android.util.SparseArray;

/**
 * type of data that we need to keep as logs
 */
public enum EnumDeviceLogInfoType {
    ReservedUndefined(0),
    ApplicationException(1),
    ApplicationStarted(2),
    ApplicationEnded(3),
    ApplicationCrashed(4),
    AndroidSoftwareVersion(5),
    SimSerialNumber(6),
    BuildInfo(7),
    InternalStorageTotalSpace(8),
    InternalStorageFreeSpace(9),
    SdCardTotalSpace(10),
    SdCardFreeSpace(11),
    NetworkState(12),
    NetworkIsAvailable(13),
    NetworkType(14),
    NetworkDataState(15),
    WifiEnabled(16),
    LocationLat(17),
    LocationLng(18),
    BluetoothConnectionDeviceInfo(19),
    MobileDeviceBluetoothStatus(20),
    MobileDevicePowerPlugConnected(21),
    MobileDeviceBatteryLow(22),
    MobileDeviceBatteryLevel(23),
    MemoryFreeSpace(24),
    MemoryTotalSpace(25),
    EcgDeviceBatteryLevel(26),
    EcgDeviceLeadStatusChanged(27),
    EcgDeviceConnectionStatus(28),

    EcgChunkTransmissionError(31),
    PteTransmissionError(32);


    private static SparseArray<EnumDeviceLogInfoType> mappings;
    private final int intValue;

    EnumDeviceLogInfoType(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static SparseArray<EnumDeviceLogInfoType> getMappings() {
        if (mappings == null) {
            synchronized (EnumDeviceLogInfoType.class) {
                if (mappings == null) {
                    mappings = new SparseArray<>();
                }
            }
        }
        return mappings;
    }

    public static EnumDeviceLogInfoType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }

}