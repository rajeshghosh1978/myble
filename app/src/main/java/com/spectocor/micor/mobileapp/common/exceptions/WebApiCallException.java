package com.spectocor.micor.mobileapp.common.exceptions;

/**
 * Exception happening while calling Api servers
 */
public class WebApiCallException extends Exception {

    /**
     * url that is called
     */
    private String url;
    /**
     * status code
     */
    private int statusCode;
    /**
     * response information
     */
    private String responseInfo;

    public WebApiCallException(String url, int statusCode, String responseInfo) {
        super("Unexpected http code " + statusCode + "\r\n" + " Url" + url + "\r\n" + responseInfo);

        this.url = url;
        this.statusCode = statusCode;
        this.responseInfo = responseInfo;
    }

    public String getUrl() {
        return url;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getResponseInfo() {
        return responseInfo;
    }

}
