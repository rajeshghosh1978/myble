package com.spectocor.micor.mobileapp.http;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.AMainJSInterface;
import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogEntity;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogMapper;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogsDbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.http.common.ResponseCodes;
import com.spectocor.micor.mobileapp.http.requests.DeviceLogHttpRequest;
import com.spectocor.micor.mobileapp.http.requests.EcgChunkHttpRequest;
import com.spectocor.micor.mobileapp.sessionactivation.api.EnrollmentInfo;
import com.spectocor.micor.mobileapp.sessionactivation.api.SessionActivationApi;
import com.spectocor.micor.mobileapp.sessionactivation.api.SessionInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.SQLDataException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Handler for processing network requests. Requests are retried 3 times before discarding.
 * <p/>
 * Created by Rafay Ali on 2/4/2016.
 */
public class HttpRequestHandler extends Handler {

    public final String TAG = HttpRequestHandler.class.getSimpleName();

    public static final int ECG_CHUNK_REQUEST = 1;
    public static final int DEVICE_LOG_REQUEST = 4;
    private final int RETRY_REQUEST = 3;
    private final OkHttpClient httpClient = new OkHttpClient();

    EcgChunkHttpRequest ecgChunkHttpRequest;
    DeviceLogHttpRequest deviceLogHttpRequest;
    EcgDataChunk ecgDataChunk;

    AMainJSInterface aMainJSInterface;
    String jsonSession;

    EnrollmentInfo en;
    ArrayList<SessionInfo> arrsession = null;
    static Handler mHandler;
    long timeDuration = 60 * 1000;
    int sessionId = -1;
    public static int index = 0;
    public static boolean isTimerSet;
    static boolean isHolterOccurs;

    public HttpRequestHandler(Looper looper) {
        super(looper);
        /*basePreferenceHelper = new BasePreferenceHelper(AMainApplication.getAppContext());*/
        aMainJSInterface = new AMainJSInterface(AMainApplication.getAppContext());
        /*jsonSession = basePreferenceHelper.getjsonSession();*/

    }

    @Override
    public void handleMessage(Message msg) {
        Log.v(TAG, "Obtained message on HttpRequestHandler");

        switch (msg.what) {

            case ECG_CHUNK_REQUEST: {
                Log.v(TAG, "Message identified as ECG_CHUNK_REQUEST");

                if (msg.obj != null) {
                    Object obj = msg.obj;
                    if (obj instanceof EcgChunkHttpRequest) {
                        ecgChunkHttpRequest = (EcgChunkHttpRequest) obj;
                        ecgDataChunk = ecgChunkHttpRequest.getEcgChunkData();

                        // TODO: WHY TO SET HOLTER HERE?
                        setHolter();

                        try {

                            // After holter plus new session requires start session call
                            if(ecgDataChunk!=null){
                                sendEcgChunk(ecgDataChunk);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
                break;
            }

            case DEVICE_LOG_REQUEST: {

                if(!AMainApplication.LOGS_ENABLED) {
                    Log.v(TAG, "Logs Disabled. Returning");
                    return;
                }

                Log.v(TAG, "Message identified as DEVICE_LOG_REQUEST");

                if (msg.obj != null) {
                    Object obj = msg.obj;
                    if (obj instanceof DeviceLogHttpRequest) {
                        deviceLogHttpRequest = (DeviceLogHttpRequest) obj;
                        final DeviceLogMapper logsData = deviceLogHttpRequest.getDeviceLogsTobeSend();
                        final ArrayList<DeviceLogEntity> logToUpdate = deviceLogHttpRequest.getLogToUpdate();
                        try {
                            if (logsData != null && logToUpdate != null) {
                                sendAndUpdateLogs(logsData, logToUpdate);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
                break;
            }
        }
    }

    private void setHolter() {

        /*if (mHandler == null) {
            mHandler = new Handler();
        }
        en = basePreferenceHelper.getSavedEnrollmentInfo();
        if (en != null) {
            arrsession = en.getAvailableSessions();
        }

        if (arrsession != null && arrsession.size() > 0 && !isTimerSet) {
            sessionId = arrsession.get(index).getSessionId();
            timeDuration = arrsession.get(index).getDuration();
            android.util.Log.d(TAG, "run: Session");
            basePreferenceHelper.putActiveSessionId(String.valueOf(sessionId));
            isTimerSet = true;
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (index < arrsession.size()) {
                        index++;
                        isTimerSet = false;
                        isHolterOccurs = true;
                        setHolter();
                    }
                }
            }, timeDuration);
        }*/
    }

    public void sendEcgChunk(EcgDataChunk ecgDataChunk) throws JSONException {

        String responseBody;
        int responseCode;

        int sid;
        sid = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext()).getSessionId();
        Log.v(TAG, "SessionStateAccess GetCurrentSessionState = " + String.valueOf(sid));
        Log.v(TAG, "Sending data chunk with sessionID = " + sessionId);
        Log.v(TAG, "Sending data chunk with Time Duration= " + timeDuration);

            // aMainJSInterface.sessionStart(jsonSession);
            SessionActivationApi sessionActivationApi = SessionActivationApi.getInstance(AMainApplication.getAppContext());
            if(isHolterOccurs){
                sessionActivationApi.startSessionCall(String.valueOf(sessionId), DeviceIdentifiers.getEcgId(), DeviceIdentifiers.getPdaId(), EcgProperties.DEVICE_CHUNK_SIZE, jsonSession);
                isHolterOccurs=false;
                Log.v(TAG, "Session Started " + String.valueOf(sessionId));
            }
            else
            {
                Log.v(TAG, "Session Resumed " + String.valueOf(sessionId));
            }

        RequestBody requestBody = new MultipartBody.Builder()
                .addFormDataPart("sessionId", "" + sessionId)//ecgDataChunk.monitoringSessionId
                .addFormDataPart("chunkStartTimestamp", "" + System.currentTimeMillis())
                .addFormDataPart("ecgId", DeviceIdentifiers.getEcgId())
                .addFormDataPart("deviceId", DeviceIdentifiers.getPdaId())
                .addFormDataPart("numberOfSamples", ecgDataChunk.numberOfSamples + "")
                .addFormDataPart("file", "data.comp",
                        RequestBody.create(MediaType.parse("application/octet-stream"), ecgDataChunk.chunkDataChannelCompressed))
                .setType(MultipartBody.FORM)
                .build();


        Log.v(TAG, "Multipart request body created for " + EcgChunkHttpRequest.class.getSimpleName()
                + " with filename data.comp");

        Request request = new Request.Builder()
                .header("Authorization", "Basic c2lyYWo6dGVzdDEyMw==")
                .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_ECG_CHUNK)
                .post(requestBody)
                .build();

        Response response;

        // TODO: RETRY really needed?
        // We try sending requests 3 times before discarding chunk
        for (int i = 0; i < RETRY_REQUEST; i++) {

            try {
                Log.v(TAG, "Trying Request: " + (i + 1) + " time");
                Call c = httpClient.newCall(request);
                response = c.execute();
                responseBody = response.body().string();
                JSONObject responseJSON = new JSONObject(responseBody);
                responseCode = responseJSON.getInt("code");

                Log.v(TAG, "StatusCode: " + responseCode + ", ResponseBody: " + responseBody);


                if (responseCode == ResponseCodes.OK) {
                    Log.v(TAG, "Response is successful, finishing request.");

                    /*EcgChunkDbHelper.getInstance(AMainApplication.getAppContext()).setEcgChunkAsTransmitted(
                            ecgDataChunk.getMonitoringSessionId(), ecgDataChunk.getChunkStartDateUnixMs()
                    );*/

                    ecgDataChunk.chunkTransferTimestamp = System.currentTimeMillis();
                    Log.v(TAG, "EcgChunk entry marked as sent in database.");

                    // saving the chunk to database ...
                    EcgChunkDbHelper.getInstance(AMainApplication.getAppContext()).insertEcgChunk(ecgDataChunk);

                    break;
                }
                // two cases happen: holter plus begins or holter plus ends.
                else if (responseCode == ResponseCodes.SESSION_EXPIRED) {
                    Log.v(TAG, "Session termination request received. Ending current session.");
                    // Send signal to stop monitoring ecg and show termination message

                    // SAVE CHUNK: Because already sent. And Received Response.
                    ecgDataChunk.chunkTransferTimestamp = System.currentTimeMillis();
                    Log.v(TAG, "EcgChunk entry marked as sent in database.");

                    // saving the chunk to database ...
                    EcgChunkDbHelper.getInstance(AMainApplication.getAppContext()).insertEcgChunk(ecgDataChunk);


                    Intent intent = new Intent("BLUETOOTH_EVENT");
                    intent.putExtra("EVENT_TYPE", AMainActivity.SESSION_JUST_TERMINATED);
                    LocalBroadcastManager.getInstance(AMainApplication.getAppContext()).sendBroadcast(intent);

                    break;

                } else if (responseCode == ResponseCodes.DEVICE_NO_LONGER_ACTIVE) {

                    // MAKE BROADCAST TO DEVICE_NO_LONGER_ACTIVE
                    Intent intent = new Intent("BLUETOOTH_EVENT");
                    intent.putExtra("EVENT_TYPE", AMainActivity.SESSION_DEVICE_NO_LONGER_ACTIVE);
                    LocalBroadcastManager.getInstance(AMainApplication.getAppContext()).sendBroadcast(intent);


                    // SAVE CHUNK: Because already sent. And Received Response.

                    ecgDataChunk.chunkTransferTimestamp = System.currentTimeMillis();
                    Log.v(TAG, "EcgChunk entry marked as sent in database.");

                    // saving the chunk to database ...
                    EcgChunkDbHelper.getInstance(AMainApplication.getAppContext()).insertEcgChunk(ecgDataChunk);

                    break;

                } else {
                    ecgDataChunk.chunkTransferTimestamp = -1;

                    // saving the chunk to database ...
                    EcgChunkDbHelper.getInstance(AMainApplication.getAppContext()).insertEcgChunk(ecgDataChunk);

                    break;
                }


            } catch (IOException e) {

                Log.e(TAG, "Error in executing EcgChunkHttpRequest.");
                e.printStackTrace();

                ecgDataChunk.chunkTransferTimestamp = -1;

                // saving the chunk to database ...
                try {
                    EcgChunkDbHelper.getInstance(AMainApplication.getAppContext()).insertEcgChunk(ecgDataChunk);
                } catch (SQLDataException e1) {
                    e1.printStackTrace();
                }

                break;

            } catch (SQLDataException e) {
                e.printStackTrace();
                break;
            }
        } // end of for-retry-attempts

    }


    public void sendAndUpdateLogs(DeviceLogMapper deviceLogsToSend, ArrayList<DeviceLogEntity> logsToUpdate) throws JSONException {

        String responseBody;
        int responseCode;
        RequestBody requestBody = new MultipartBody.Builder()
                .addFormDataPart("sessionId", deviceLogsToSend.getSessionId())
                .addFormDataPart("ecgId", DeviceIdentifiers.getEcgId())
                .addFormDataPart("deviceId", DeviceIdentifiers.getPdaId())
                .addFormDataPart("log", deviceLogsToSend.getLogsAsString())
                .setType(MultipartBody.FORM)
                .build();

        Request request = new Request.Builder()
                .header("Authorization", "Basic c2lyYWo6dGVzdDEyMw==")
                .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_DEVICE_LOG)
                .post(requestBody)
                .build();

        Response response;

        try {
            Call c = httpClient.newCall(request);
            response = c.execute();
            responseBody = response.body().string();
            JSONObject responseJSON = new JSONObject(responseBody);
            responseCode = responseJSON.getInt("code");

            if (responseCode == ResponseCodes.OK) {


                try {

                    long currentTimeStamp;
                    for (DeviceLogEntity deviceLog : logsToUpdate) {
                        currentTimeStamp = System.currentTimeMillis();
                        deviceLog.setTransmittedTimestamp(currentTimeStamp);
                    }
                    //after Sending log to server update entries in database
                    DeviceLogsDbHelper.getInstance().updateItems(logsToUpdate);
                } catch (SQLDataException t) {


                }

            }
        } catch (Throwable t) {

            System.out.print(t.getMessage());
        }


    }


}
