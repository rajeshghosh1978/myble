package com.spectocor.micor.mobileapp.amain.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogEvents;

/**
 * Created by Savio Monteiro on 2/1/2016.
 */
public class BluetoothConnectivityReceiver extends BroadcastReceiver {

    public static final String BT_BROADCAST_ACTION = "com.spectocor.micor.mobileapp.BT_BROADCAST_ACTION";
    public static final String BT_BROADCAST_MESG = "BT_BROADCAST_MESG";

    public static final int MESG_BT_CONNECTED = 11;
    public static final int MESG_BT_DISCONNECTED = 12;


    private static boolean BT_CONNECTED = false;

    private static Context mContext;

    private static void log(String str) {
        MLog.log(BluetoothConnectivityReceiver.class.getSimpleName() + ": NotificationReceiver: " + str);
    }

    /**
     * Create the broadcast intent to consume
     *
     * @param bundle
     * @param btBroadcastMesg BluetoothConnectivityReceiver.MESG_BT_CONNECTED or MESG_BT_DISCONNECTED
     * @return
     */
    public static void notifyMessage(int btBroadcastMesg, Bundle bundle) {

        log("Received Message ... " + btBroadcastMesg);

        Intent intent = new Intent(BT_BROADCAST_ACTION);
        intent.putExtra(BT_BROADCAST_MESG, btBroadcastMesg);
        if(bundle != null)
        {
            intent.putExtras(bundle);
        }

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

    }

    @Override
    public void onReceive(Context context, Intent intent) {

        log("Received Intents: " + intent.getAction());

        mContext = context;

        if(!intent.getAction().equals(BT_BROADCAST_ACTION)) {
            return;
        }


        int mesgType = intent.getExtras().getInt(BT_BROADCAST_MESG, -1);
        if(mesgType == -1)
            return;

        switch (mesgType) {

            case MESG_BT_CONNECTED: {

                log("BT Connected");

                DeviceLogEvents.getInstance().putBluetoothConnectionDeviceInfo(true);

                BT_CONNECTED = true;

                checkAlertCriteria();

                break;
            }

            case MESG_BT_DISCONNECTED: {

                log("BT Disconnected");

                DeviceLogEvents.getInstance().putBluetoothConnectionDeviceInfo(false);

                BT_CONNECTED = false;

                checkAlertCriteria();

                break;
            }


        }

    } //  end of on-receive


    Thread disconnectedAlertLooper = null;
    private static int notConnectedSinceSec = 0;
    private static long firstDisconnectedAt = 0;

    private void checkAlertCriteria() {

        if(BT_CONNECTED) {

            notConnectedSinceSec = 0;

            if(disconnectedAlertLooper != null) {
                disconnectedAlertLooper.interrupt();
                disconnectedAlertLooper = null;
            }

            // cancelling vibrate if it is currently vibrating
            Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
            v.cancel();

            AMainApplication.showShortToast("BT Connected", mContext);

        }
        else // DISCONNECTED
        {

            firstDisconnectedAt = System.currentTimeMillis();

            checkCriteriaAndPlayBtAlert();



            disconnectedAlertLooper = new Thread(new Runnable() {

                @Override
                public void run() {

                    if(notConnectedSinceSec == 0) {
                        notConnectedSinceSec = 120;
                    }
                    else if(notConnectedSinceSec == 120) {
                        notConnectedSinceSec = 30 * 60;
                    }
                    else {
                        notConnectedSinceSec = 30 * 60;
                    }

                    log("Checking again in " + notConnectedSinceSec + " seconds");

                    try {

                        Thread.sleep(notConnectedSinceSec * 1000);

                    } catch (InterruptedException e) {

                        log("Disconnection Checker Thread cancelled");

                        e.printStackTrace();

                        log("Disconnection Checker Thread Cancelled! Returning!");

                        return;
                    }

                    if(SessionStateAccess.getCurrentSessionState(mContext).sessionActive) {

                        checkCriteriaAndPlayBtAlert();

                        run();
                    }
                    else {
                        log("Session not active anymore. Not Checking For Re-connection");
                    }

                }
            });

            if(SessionStateAccess.getCurrentSessionState(mContext).sessionActive) {
                disconnectedAlertLooper.start();
            }
            else {
                log("Session not active anymore. Not Checking For Re-connection");
            }
        }

    } // end of checkAlertCriteria(..)



    /**
     * According to http://project.spectocor.com:8090/confluence/display/ACS/Device+Notifcation+Matrix
     * Bluetooth Alerts are mapped as follows.
     * Disconnected for 2 mins: Audio, Visual, Vibrate
     * Disconnected for 30 mins: Audio (except between 8pm to 8am), Visual, Vibrate
     * Still disconnected beyond 30 mins, every 30 minutes: Audio (except between 8pm to 8am), Visual, Vibrate
     *
     */
    private void checkCriteriaAndPlayBtAlert() {



        /*
        GregorianCalendar c = (GregorianCalendar) GregorianCalendar.getInstance(TimeZone.getDefault());

        log("Attempting to play BT Alert at " + c.getTime().toString());

        int hourOfDay = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);


        // AlertPlayer.displayActivity(mContext);


        // commenting this section temporarily
        if((hourOfDay < 20) && (hourOfDay >= 8))
        {
            // play audio
            log("Playing Audio!");
            AlertPlayer.playAudioAlert(mContext);
        }
        else if(notConnectedSinceSec == 120 || notConnectedSinceSec == 0)
        {
            // play audio
            log("Playing Audio!");
            AlertPlayer.playAudioAlert(mContext);
        }
        */


        // play audio
        log("Playing Audio!");
        AlertPlayer.playAudioAlert(mContext);

        // Vibrate
        log("Vibrating");
        AlertPlayer.playVibrateAlert(mContext);

        // Notify The App
        // TODO: Notify app of disconnection
        AMainApplication.showLongToast("Bluetooth Disconnected!", mContext);

        AlertPlayer.turnScreenOn(mContext);

    }

}
