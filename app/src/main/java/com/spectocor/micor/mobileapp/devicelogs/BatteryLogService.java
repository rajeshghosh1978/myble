package com.spectocor.micor.mobileapp.devicelogs;

/**
 * Logs battery information
 */
@SuppressWarnings("WeakerAccess")
public class BatteryLogService {

    protected final DeviceLogLogger service;

    public BatteryLogService(DeviceLogLogger deviceLogLogger) {
        service = deviceLogLogger;
    }


    /**
     * Logs mobile device battery status
     *
     * @param batteryPercent battery percentage
     */
    public DeviceLog logMobileDeviceBatteryLevel(byte batteryPercent) {
        if (!(batteryPercent >= 0 && batteryPercent <= 100))
            throw new IllegalArgumentException("batteryPercent should be between 0 and 100");
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.MobileDeviceBatteryLevel, (double) batteryPercent);
        service.insertLog(obj);
        return obj;
    }


    /**
     * Logs mobile device battery status
     *
     * @param isChargerConnected is charger connected
     */
    public DeviceLog logMobileDevicePowerPlugConnected(boolean isChargerConnected, String powerPlugType) {
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.MobileDevicePowerPlugConnected, isChargerConnected);
        obj.setValueString(powerPlugType);
        service.insertLog(obj);
        return obj;
    }




}
