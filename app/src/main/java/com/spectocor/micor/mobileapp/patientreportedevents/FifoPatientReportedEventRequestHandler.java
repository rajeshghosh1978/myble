package com.spectocor.micor.mobileapp.patientreportedevents;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogEvents;
import com.spectocor.micor.mobileapp.http.HttpManager;
import com.spectocor.micor.mobileapp.http.common.ResponseCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Savio Monteiro on 3/11/2016.
 */
public class FifoPatientReportedEventRequestHandler extends Handler {

    public static final String TAG = FifoPatientReportedEventRequestHandler.class.getSimpleName();

    private boolean msgBeingHandled = false;

    public FifoPatientReportedEventRequestHandler(Looper looper) {
        super(looper);
    }

    private void log(String str) {
        Log.v(TAG, str);
    }


    @Override
    public void handleMessage(Message msg) {

        log(" ====== handling notification");

        if (msgBeingHandled) {

            log("Message While Loop is already handling current data ... returning");

            return;
        }

        log("FIFO Message is now being handled...");

        while (true) {

            msgBeingHandled = true;


            try {
                PatientEvent patientEvent;

                log("ENTERING WHILE LOOP");

                patientEvent = PatientReportedEventsDbHelper.getInstance().getOldestPatientEventNotYetTransmitted();

                if (patientEvent == null){
                    log("PatientEvent is null, we assume there are no remaining entries in db to be send, finishing loop.");
                    msgBeingHandled = false;
                    break;
                }

                RequestBody requestBody = new MultipartBody.Builder()
                        .addFormDataPart("sessionId", "" + SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext()).getSessionId())
                        .addFormDataPart("log", new JSONArray(patientEvent.getData()).toString())
                        .addFormDataPart("ecgId", DeviceIdentifiers.getEcgId())
                        .addFormDataPart("deviceId", DeviceIdentifiers.getPdaId())
                        .setType(MultipartBody.FORM)
                        .build();

                String requestBodyStr = "sessionId: " + SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext()).getSessionId() +
                                        ", log: " + new JSONArray(patientEvent.getData()).toString() +
                                        ", ecgId: " + DeviceIdentifiers.getEcgId() +
                                        ", deviceId: " + DeviceIdentifiers.getPdaId();

                Request request = new Request.Builder()
                        .post(requestBody)
                        .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_PATIENT_LOG)
                        .build();



                Response response = HttpManager.getInstance().getHttpClient().newCall(request).execute();
                String responseBody = response.body().string();
                JSONObject responseJSON = new JSONObject(responseBody);
                int responseCode = responseJSON.getInt("code");

                Log.v(TAG, "StatusCode : " + responseCode + " ResponseBody: " + responseBody);

                if (responseCode == ResponseCodes.OK) {
                    Log.v(TAG, "Response has been successful, marking entry in database as transmitted.");
                    PatientReportedEventsDbHelper.getInstance().updatePatientEventAsTransmitted(patientEvent);
                }
                else {
                    DeviceLogEvents.getInstance().putPteTransmissionError(requestBodyStr, responseBody, responseCode);
                }

            } catch (JSONException e) {
                Log.v(TAG, "Error while converting data to JSONArray!");
                e.printStackTrace();

                break;

            } catch (IOException e) {
                Log.v(TAG, "Error while sending buffered ecg chunk over the network!");
                e.printStackTrace();
                break;
            }

            Log.v(TAG, "Message is not longer being handled! ");
            msgBeingHandled = false;


        } // end-while

        Log.v(TAG, "End of While Loop");
        msgBeingHandled = false;
    }
}
