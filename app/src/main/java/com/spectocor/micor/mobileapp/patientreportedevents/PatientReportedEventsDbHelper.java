package com.spectocor.micor.mobileapp.patientreportedevents;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.DbHelper;
import com.spectocor.micor.mobileapp.common.Log;

import java.sql.SQLDataException;

/**
 * SqLite Database helper class for Patient reported events.
 * Events are first inserted into this database before sending and marked as sent after sending.
 *
 * Created by Rafay Ali on 2/18/2016.
 * Modified by Savio Monteiro on 2/27/2016: Previous version located at PatientReportedEventsDbHelperOld
 */
public class PatientReportedEventsDbHelper {

    public static final String TAG = PatientReportedEventsDbHelper.class.getSimpleName();


    private static PatientReportedEventsDbHelper instance = null;
    private static DbHelper dbHelper;

    private PatientReportedEventsDbHelper() {

    }


    public static PatientReportedEventsDbHelper getInstance() {

        dbHelper = DbHelper.getInstance(AMainApplication.getAppContext());


        if (instance == null){
            instance = new PatientReportedEventsDbHelper();
        }

        return instance;
    }


    /**
     * Insert a new patient event
     * @param patientEvent
     * @return rowId - long
     */
    public synchronized long insertPatientEvent(PatientEvent patientEvent){
        long rowId;

        try {
            ContentValues contentValues = new ContentValues();

            contentValues.put(DbHelper.PTE_COLUMN_NAME_SESSION_ID, patientEvent.getSessionId());
            contentValues.put(DbHelper.PTE_COLUMN_NAME_INSERT_TIMESTAMP, patientEvent.getInsertTimestamp());
            contentValues.put(DbHelper.PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP, patientEvent.getTransmittedTimestamp());
            contentValues.put(DbHelper.PTE_COLUMN_NAME_DATA, patientEvent.getData());

            rowId = dbHelper.getWritableDatabase().insert(DbHelper.PTE_TABLE_NAME, null, contentValues);

            if (rowId == -1) {
                throw new SQLDataException("Unable to insert PatientEvent in " + DbHelper.PTE_TABLE_NAME + " table");
            }
        }
        catch (SQLDataException e){
            Log.v(TAG, e.getMessage());
            return -1;
        }

        return rowId;
    }



    /**
     * Update an instance of the patient reported event as transmitted.
     * @param patientEvent
     * @return rowId - long
     */
    public synchronized long updatePatientEventAsTransmitted(PatientEvent patientEvent){
        long rowId;

        try {
            SQLiteDatabase database = dbHelper.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put(DbHelper.PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP, System.currentTimeMillis());

            rowId = database.update(DbHelper.PTE_TABLE_NAME, contentValues,
                    DbHelper.PTE_COLUMN_NAME_SESSION_ID + "=? and " + DbHelper.PTE_COLUMN_NAME_INSERT_TIMESTAMP + "=?",
                    new String[]{"" + patientEvent.getSessionId(), "" + patientEvent.getInsertTimestamp()});


            if (rowId == -1) {
                throw new SQLDataException("Unable to update PatientEvent in " + DbHelper.PTE_TABLE_NAME + " table");
            }
        }
        catch (SQLDataException e){
            return -1;
        }

        return rowId;
    }

    /**
     * Get the oldest patient event not transmitted
     * @return PatientEvent
     */
    public synchronized PatientEvent getOldestPatientEventNotYetTransmitted() {

        PatientEvent patientEvent = null;
        Cursor cursor = null;

        String QUERY = "SELECT * FROM " + DbHelper.PTE_TABLE_NAME + " WHERE " +
                DbHelper.PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP + "=-1 ORDER BY " +
                DbHelper.PTE_COLUMN_NAME_INSERT_TIMESTAMP + " LIMIT 1";

        try
        {
            cursor = dbHelper.getWritableDatabase().rawQuery(QUERY, new String[]{});

            if (cursor.getCount() == 0){
                Log.v(TAG, "Cursor returned 0 entries from database.");
                return null;
            }

            Log.v(TAG, "Cursor returned " + cursor.getCount() + " value from " + DbHelper.PTE_TABLE_NAME + " table");
            cursor.moveToFirst();

            patientEvent = new PatientEvent();
            patientEvent.setSessionId(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_SESSION_ID)));
            patientEvent.setInsertTimestamp(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_INSERT_TIMESTAMP)));
            patientEvent.setData(cursor.getString(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_DATA)));
            patientEvent.setTransmittedTimestamp(cursor.getLong(cursor.getColumnIndex(DbHelper.PTE_COLUMN_NAME_TRANSMITTED_TIMESTAMP)));
        }
        finally {
            if (cursor != null)
                cursor.close();
        }

        return patientEvent;
    }
}
