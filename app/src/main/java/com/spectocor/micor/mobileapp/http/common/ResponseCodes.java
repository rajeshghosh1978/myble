package com.spectocor.micor.mobileapp.http.common;

/**
 * HTTP Response Codes.
 *
 * Created by Rafay Ali on 1/28/2016.
 */
@SuppressWarnings("unused")
public interface ResponseCodes {

    int OK = 200;
    int ALREADY_STARTED = 208;
    int SESSION_EXPIRED = 401;
    int SESSION_DOES_NOT_EXIST = 403;
    int BAD_REQUEST = 400;
    int DUPLICATE_CHUNK_SENT = 409;
    int DEVICE_NO_LONGER_ACTIVE = 410;
}
