package com.spectocor.micor.mobileapp.devicelogs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.MLog;

import java.io.File;
import java.sql.SQLDataException;
import java.util.ArrayList;

/**
 * SQLite Database Helper for accessing DeviceLogs table.
 * Device logs are inserted into this table and then retreived when they are sent to server.
 * <p/>
 * Created by Rafay Ali on 2/24/2016.
 */
public class DeviceLogsDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "ApplicationLogs.db";
    public static final int DATABASE_VERSION = 1;

    private String TABLE_NAME = "DEVICE_LOGS";

    private String DATATYPE_INTEGER = "INTEGER";
    private String DATATYPE_TEXT = "TEXT";

    private String COLUMN_NAME_SESSION_ID = "SESSION_ID";
    private String COLUMN_NAME_INSERT_TIMESTAMP = "INSERT_TIMESTAMP";
    private String COLUMN_NAME_TRANSMITTED_TIMESTAMP = "TRANSMITTED_TIMESTAMP";
    private String COLUMN_NAME_MESSAGE = "MESSAGE";
    private String COLUMN_NAME_OF_EVENT_OCCURED = "NAME_OF_EVENT_OCCURED";

    /*private String QUERY_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
            + COLUMN_NAME_SESSION_ID                + " " + DATATYPE_INTEGER + ","
            + COLUMN_NAME_INSERT_TIMESTAMP          + " " + DATATYPE_INTEGER + ","
            + COLUMN_NAME_MESSAGE                      + " " + DATATYPE_TEXT + ","
            + COLUMN_NAME_TRANSMITTED_TIMESTAMP + " " + DATATYPE_INTEGER + ","
            + COLUMN_NAME_OF_EVENT_OCCURED + " " + DATATYPE_INTEGER + ","
            + "PRIMARY KEY(" + COLUMN_NAME_SESSION_ID + "," + COLUMN_NAME_INSERT_TIMESTAMP + ")" +
            ")";*/

    private String QUERY_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
            + COLUMN_NAME_SESSION_ID + " " + DATATYPE_INTEGER + ","
            + COLUMN_NAME_INSERT_TIMESTAMP + " " + DATATYPE_INTEGER + ","
            + COLUMN_NAME_MESSAGE + " " + DATATYPE_TEXT + ","
            + COLUMN_NAME_TRANSMITTED_TIMESTAMP + " " + DATATYPE_INTEGER + ","
            + COLUMN_NAME_OF_EVENT_OCCURED + " " + DATATYPE_INTEGER + ","
            + "PRIMARY KEY(" + COLUMN_NAME_INSERT_TIMESTAMP + ")" +
            ")";

    private String QUERY_DROP_TABLE = "DROP TABLE " + TABLE_NAME;

    private static DeviceLogsDbHelper instance = null;

    private static void log(String str) {
        //MLog.log("DeviceLogsDbHelper: " + str);
    }

    private DeviceLogsDbHelper(Context context) {
        super(context, AMainApplication.APP_STORAGE_PATH + File.separator + DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DeviceLogsDbHelper getInstance() {
        if (instance == null) {
            instance = new DeviceLogsDbHelper(AMainApplication.getAppContext());
        }

        return instance;
    }

    public static DeviceLogsDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DeviceLogsDbHelper(context);
        }

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(QUERY_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(QUERY_DROP_TABLE);
        db.execSQL(QUERY_CREATE_TABLE);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long insertDevicelogEvent(DeviceLogEntity deviceLogEvent) {

        if(!AMainApplication.LOGS_ENABLED)
        {
            log("Logs disabled. Returning!");
            return -500;
        }

        long result = -1;

        try {
            ContentValues contentValues = new ContentValues();
            log("Inserting Device logs");

            log("DB:SessionId=" + deviceLogEvent.getSessionId());
            contentValues.put(COLUMN_NAME_SESSION_ID, deviceLogEvent.getSessionId());
            contentValues.put(COLUMN_NAME_INSERT_TIMESTAMP, deviceLogEvent.getInsertTimestamp());
            contentValues.put(COLUMN_NAME_MESSAGE, deviceLogEvent.getEventMessage());
            contentValues.put(COLUMN_NAME_TRANSMITTED_TIMESTAMP, deviceLogEvent.getTransmittedTimestamp());
            contentValues.put(COLUMN_NAME_OF_EVENT_OCCURED, deviceLogEvent.getEventType().getValue());
            result = getWritableDatabase().insert(TABLE_NAME, "", contentValues);

            if (result == -1) {
//                throw new SQLDataException("Unable to ")
            }
        } catch (Exception e) {
            result = -1;
        }

      /*  if(getWritableDatabase().isOpen())
            getWritableDatabase().close();*/

        return result;
    }

    public ArrayList<DeviceLogEntity> retrieveLogsUpTillNow() throws SQLDataException {

        log("Retrieving Device logs");
        String QUERY = "SELECT * FROM " + TABLE_NAME + " WHERE " +
                "" + COLUMN_NAME_TRANSMITTED_TIMESTAMP + "=-1" +" AND " + COLUMN_NAME_SESSION_ID + "!=-1";

        String SELECTION_ARGS[] = {};
        log(QUERY + "before cursor with args " + SELECTION_ARGS.toString());
        Cursor c = getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        log(QUERY + "after cursor with args " + SELECTION_ARGS.toString());
        log("Num of rows: " + c.getCount());

        if (c.getCount() == 0) {
            log("Row not found ... ");
            c.close(); // close cursor to free up memory
            throw new SQLDataException("NONE_LEFT_TO_TRANSFER");
        } else {
            log("Row Count =" + c.getCount());
        }

        //Iterate Cursor to get all rows in ArrayList
        ArrayList<DeviceLogEntity> mArrayList = new ArrayList<>();
        DeviceLogEntity log;
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {

            log = new DeviceLogEntity();
            log("DB:REt:SessionId=" + c.getInt(c.getColumnIndex(COLUMN_NAME_SESSION_ID)));
            log.setSessionId(c.getInt(c.getColumnIndex(COLUMN_NAME_SESSION_ID)));
            log.setEventMessage(c.getString(c.getColumnIndex(COLUMN_NAME_MESSAGE)));
            log.setInsertTimestamp(c.getLong(c.getColumnIndex(COLUMN_NAME_INSERT_TIMESTAMP)));
            log.setEventTypeId(c.getInt(c.getColumnIndex(COLUMN_NAME_OF_EVENT_OCCURED)));
            log.setTransmittedTimestamp(c.getInt(c.getColumnIndex(COLUMN_NAME_TRANSMITTED_TIMESTAMP)));
            mArrayList.add(log);
        }

        log("Before Return EcgChunks");
        if (!c.isClosed())
            c.close();

        return mArrayList;
    }


    public void updateItems(ArrayList<DeviceLogEntity> transmittedItems) throws SQLDataException {
        log("Update Device logs of ");
        getWritableDatabase().beginTransaction();
        try {
            String[] args;
            for (DeviceLogEntity transmittedLog : transmittedItems) {
                ContentValues values = new ContentValues();
                values.put(COLUMN_NAME_TRANSMITTED_TIMESTAMP, transmittedLog.getTransmittedTimestamp());
                args = new String[]{String.valueOf(transmittedLog.getInsertTimestamp())};
                //int indicator = getWritableDatabase().update(TABLE_NAME, values, COLUMN_NAME_SESSION_ID + "= ? AND " + COLUMN_NAME_INSERT_TIMESTAMP + "= ?", args);
                int indicator = getWritableDatabase().update(TABLE_NAME, values, COLUMN_NAME_INSERT_TIMESTAMP + "= ?" +" AND " + COLUMN_NAME_SESSION_ID + "!=-1", args);
                if (indicator != 0)
                    log(indicator + " Rows updated");

            }
            getWritableDatabase().setTransactionSuccessful();
        } finally {

            //committing the bulk queries
            getWritableDatabase().endTransaction();
            getWritableDatabase().close();
        }


    }


    public void updateSessionInfo(final String sessionId) throws SQLDataException {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_SESSION_ID, Integer.parseInt(sessionId));
        int indicator = getWritableDatabase().update(TABLE_NAME, values, COLUMN_NAME_TRANSMITTED_TIMESTAMP + "=-1" + " AND " + COLUMN_NAME_SESSION_ID +"=-1", null);
        if(indicator==0)
            log("There is no row effected");
        else
            log(indicator +" Rows Updated");



    }


}
