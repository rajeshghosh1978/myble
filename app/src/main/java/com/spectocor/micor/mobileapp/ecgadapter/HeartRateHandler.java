package com.spectocor.micor.mobileapp.ecgadapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;

import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.ecgadapter.mobileheartrate.QRSDetector;
import com.spectocor.micor.mobileapp.ecgadapter.mobileheartrate.QRSDetectorParameters;
import com.spectocor.micor.mobileapp.ecgadapter.mobileheartrate.QRSFilterer;
import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.ecgadapter.mobileheartrate.QRSSignalFilterer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Savio Monteiro on 1/9/2016.
 */

public class HeartRateHandler extends Handler {

    private static Context mContext;

    private static final int SAMPLE_RATE = EcgProperties.SAMPLE_RATE;

    private static final int NEW_DATA = 1;

    private static HeartRateHandler heartRateHandler;
    private static HandlerThread heartRateHandlerThread;

    private static QRSDetector qrsDetector;
    private static QRSDetectorParameters qrsDetectorParameters = new QRSDetectorParameters(SAMPLE_RATE) ;

    private static QRSSignalFilterer qrsFilterers[];

    private static void log(String str) {
        MLog.log(HeartRateHandler.class.getSimpleName() + ": " + str);
    }

    private HeartRateHandler(Looper looper) {
        super(looper);
    }

    public static HeartRateHandler getInstance(Context context) {

        mContext = context;


        if(heartRateHandlerThread == null) {
            heartRateHandlerThread = new HandlerThread("HeartRateHandlerThread");
            heartRateHandlerThread.start();
        }

        if(heartRateHandler == null) {
            heartRateHandler = new HeartRateHandler(heartRateHandlerThread.getLooper());
        }


        if(qrsDetector == null) {
            log("QRSDetected is null ... initializing!");
            initQRSDetector();
        }

        if(qrsFilterers == null) {
            log("Signal Filterers is null ... initializing ... ");
            initSignalFilters();
        }

        return heartRateHandler;
    }

    /**
     * Initializing QRS Detectors and Filters
     */
    public void init() {
        initQRSDetector();
        initSignalFilters();
    }


    /**
     * Terminate the HandlerThread
     */
    public void terminate() {
        if(heartRateHandlerThread != null) {
            heartRateHandlerThread.quit();
            heartRateHandlerThread = null;
            heartRateHandler = null;
        }
    }

    /**
     * Initializing QRS Detector
     */
    private static void initQRSDetector() {
        qrsDetector = new QRSDetector(qrsDetectorParameters) ;
        QRSFilterer qrsFilterer = new QRSFilterer(qrsDetectorParameters) ;
        qrsDetector.setObjects(qrsFilterer);
    }

    /**
     * Initializing Signal Filters
     */
    private static void initSignalFilters() {
        qrsFilterers = new QRSSignalFilterer[EcgProperties.NUMBER_OF_CHANNELS];
        for(int i = 0; i < EcgProperties.NUMBER_OF_CHANNELS; i++) {
            qrsFilterers[i] = new QRSSignalFilterer(qrsDetectorParameters);
        }
    }

    /**
     * Apply a Bandpass filter to an input signal.
     * @param samples
     * @return
     */
    public static ArrayList<Integer> applyFilter(final List<Integer> samples, int channel) {

        if(channel <= 0 || channel > EcgProperties.NUMBER_OF_CHANNELS) {
            throw new IllegalArgumentException("Channels can be from 1 to " + EcgProperties.NUMBER_OF_CHANNELS);
        }

        ArrayList<Integer> output = new ArrayList<>();

        for(int s:samples) {
            try {
                output.add(qrsFilterers[channel - 1].QRSFilter(s));
            }
            catch (Exception e) {
                initSignalFilters();
                output.add(qrsFilterers[channel - 1].QRSFilter(s));
            }
        }

        return output;
    }


    /**
     * Apply a Bandpass filter to an input signal.
     * @param samples
     * @return
     */
    public static ArrayList<Integer> applyFilter(final short[] samples, int channel)
    {

        if(samples == null) {
            throw new NullPointerException("samples cannot be null");
        }

        if(channel <= 0 || channel > EcgProperties.NUMBER_OF_CHANNELS) {
            throw new IllegalArgumentException("Channels can be from 1 to " + EcgProperties.NUMBER_OF_CHANNELS);
        }

        ArrayList<Integer> output = new ArrayList<>();

        for(int s:samples) {
            try {
                output.add(qrsFilterers[channel - 1].QRSFilter(s));
            }
            catch (Exception e) {
                initSignalFilters();
                output.add(qrsFilterers[channel - 1].QRSFilter(s));
            }
        }

        return output;
    }

    /**
     * Sending newly arrived data to handler for consumption
     * @param newData
     */
    public synchronized void updateForHeartRateComputation(short[] newData) {

        if(newData == null) {
            throw new NullPointerException("newData[] cannot be null");
        }

        if(newData.length == 0) {
            throw new IllegalArgumentException("Length of newData[] cannot be null");
        }

        obtainMessage(NEW_DATA, newData).sendToTarget();
    }


    private static int hr = -1;
    private static int samplesSinceLastR = 0;

    /**
     * Calculate Instantaneous Heart Rate based on the
     * ... number of samples since last RPeak Detected and sample rate
     *
     * @param samplesSinceLastR
     * @param sampleRate
     * @return
     */
    public static int calculateHeartRate(final int samplesSinceLastR, final int sampleRate) {

        int distance = samplesSinceLastR;
        float seconds = (float) distance / (float) sampleRate;

        log("Distance: " + distance + " samples. " + seconds + " seconds apart");

        int hRate = (int) (float) (60.0f / seconds);

        return hRate;
    }

    @Override
    public void handleMessage(Message msg) {

        super.handleMessage(msg);

        switch (msg.what) {

            case NEW_DATA: {

                log("Received data for heart rate detection ... ");

                final short[] newData = (short[]) msg.obj;


                log("Processing " + newData.length + " samples from ch1 for heart rate computation");

                for(int i = 0; i < newData.length; i++)
                {

                    boolean rPeakDetected = false;
                    try {

                            // qrsDetector.QRSDet takes one sample at a time.
                            // .. It has its own buffer to maintain previous sample states to know
                            // .. if the current sample is an R peak or not.

                        rPeakDetected = (qrsDetector.QRSDet(newData[i]) != 0);

                    } catch(Exception e) {

                        log("HRError: " + e.getMessage());

                        initQRSDetector();
                        //rPeakDetected = (qrsDetector.QRSDet(newData[i]) != 0);
                        return;
                    }

                    // R-peak detected
                    if(rPeakDetected) {

                        log("samples since last R: " + samplesSinceLastR);

                        hr = calculateHeartRate(samplesSinceLastR, SAMPLE_RATE);


                        // UPDATING HR IN UI
                        Bundle b = new Bundle();
                        b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SIGNAL_VIEW_LE_DEVICE_NEW_HEART_RATE);
                        b.putInt("newHr", hr);
                        Intent intent = new Intent("BLUETOOTH_EVENT");
                        intent.putExtras(b);
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                        log("HR: " + hr);

                        // Checking for a valid heart rate
                        if((hr > 30 && hr < 300) || hr == -10) {

                            // scheduling UI Updates in sequencer handler.
                            HeartBeatSequencer.getInstance(mContext).onNewHeartBeatDetected(samplesSinceLastR);

                        }

                        samplesSinceLastR = 0;
                    }
                    else // not an R-peak
                    {
                        samplesSinceLastR++;

                        // if no Rpeak for more than 10 seconds set invalid
                        if(samplesSinceLastR > (10 * SAMPLE_RATE)) {
                            samplesSinceLastR = 0;
                        }
                    }
                }

                break;
            }
        }
    }



}
