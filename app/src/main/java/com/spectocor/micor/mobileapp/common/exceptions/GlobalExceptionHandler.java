package com.spectocor.micor.mobileapp.common.exceptions;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.spectocor.micor.mobileapp.amain.AMainService;
import com.spectocor.micor.mobileapp.amain.AppServiceManager;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogEvents;
import com.spectocor.micor.mobileapp.http.HttpQueueService;

import java.io.PrintWriter;
import java.io.StringWriter;


// Handles restart application when an error happens on main thread
// http://stackoverflow.com/questions/12560590/android-app-restarts-upon-crash-force-close
// http://stackoverflow.com/questions/2681499/android-how-to-auto-restart-application-after-its-been-force-closed


//NOTE: Don't use this class for exception handling of services. This handler kill process
//We probably need to restart the service and not the whole app

/**
 * Default exception handler for Global unhandled exception
 * it logs final exceptions, restarts the application and exits current crashed app
 */
public class GlobalExceptionHandler implements Thread.UncaughtExceptionHandler {

    /**
     * preference tag for knowing when the application crashed
     */
    public static final String KEY_APP_CRASHED = "KEY_APP_CRASHED";
    /**
     * time to call alarm service, 2 seconds as default
     */
    private static final int RESTART_WAIT_TIME_MILLISECONDS = 15000;
    private static final String TAG = "GlobalExceptionHandler";
    /**
     * application class
     */
    protected final Application application;
    /**
     * activity to be used to be launched after restart
     */
    private final Class<?> relaunchActivityClass;

    /**
     * default UncaughtExceptionHandler for Android. This should be obtaiend from application class: Thread.getDefaultUncaughtExceptionHandler()
     */
    private final Thread.UncaughtExceptionHandler androidDefaultCrashUncaughtExceptionHandler;
    // NOTE: I didn't use Thread.getDefaultUncaughtExceptionHandler() in the class to keep it independent of this handler for testing null values


    /**
     * constructor for default exception handler
     *
     * @param application                                 application object
     * @param relaunchActivityClass                       activity to be used for relunching the program (starting point)
     * @param androidDefaultCrashUncaughtExceptionHandler default UncaughtExceptionHandler for Android. This should be obtaiend from application class: Thread.getDefaultUncaughtExceptionHandler()
     */
    public GlobalExceptionHandler(Application application, Class<?> relaunchActivityClass, Thread.UncaughtExceptionHandler androidDefaultCrashUncaughtExceptionHandler) {
        this.application = application;
        this.relaunchActivityClass = relaunchActivityClass;
        this.androidDefaultCrashUncaughtExceptionHandler = androidDefaultCrashUncaughtExceptionHandler;
    }


    // http://stackoverflow.com/questions/2681499/android-how-to-auto-restart-application-after-its-been-force-closed

    /**
     * The thread is being terminated by an uncaught exception. Further
     * exceptions thrown in this method are prevent the remainder of the
     * method from executing, but are otherwise ignored.
     *
     * @param thread the thread that has an uncaught exception
     * @param ex     the exception that was thrown
     */
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {

        try {


            try {
                // save in the storage that app has crashed
                application.getSharedPreferences(TAG, Context.MODE_PRIVATE).edit()
                        .putBoolean(KEY_APP_CRASHED, true).apply();

                // writing the exception in log
                StringWriter stackTrace = new StringWriter();
                ex.printStackTrace(new PrintWriter(stackTrace));
                Log.e(TAG, "uncaughtException:" + stackTrace);

                String exceptionMsg  = ex.getMessage() != null ? ex.getMessage() : stackTrace.toString();
                DeviceLogEvents.getInstance().putLogApplicationCrashed(exceptionMsg);


                Log.e(TAG, "Stopping Services");

                AppServiceManager.stopService(HttpQueueService.class);
                AppServiceManager.stopService(AMainService.class);

            } catch (Exception ex2) {
                Log.e(TAG, "uncaughtException:", ex2);
            }

            // calling for restarting the application
            // NOTE: If restart if going to happen from another process, remove this line
            // TODO: uncomment below
            restartApplication();

            // prevent freezing application
            exitCurrentCrashedApplication();


        } catch (Exception ex2) {
            Log.e(TAG, "uncaughtException:", ex2);
            exitCurrentCrashedApplication();
        } finally {
            if (androidDefaultCrashUncaughtExceptionHandler != null)
                androidDefaultCrashUncaughtExceptionHandler.uncaughtException(thread, ex);
        }

    }

    /**
     * restarts the application by calling the main activity
     */
    private void restartApplication() {
        Intent intent = new Intent(application.getBaseContext(), relaunchActivityClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                application.getBaseContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Following code will restart application after 2 seconds using alarm manager
        AlarmManager mgr = (AlarmManager) application.getBaseContext()
                .getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + RESTART_WAIT_TIME_MILLISECONDS,
                pendingIntent);

        //NOTE: Android Lolipop 5.0 doesn't let Alarm Services less than 60000 milliseconds
        //We need to change this function for newer versions of Android for sure.
    }


    /**
     * Exists the application
     */
    private void exitCurrentCrashedApplication() {
        // prevent freezing application
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(2);
    }


}
