package com.spectocor.micor.mobileapp.patientreportedevents;

import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.common.queuesystem.SendingQueueRepositoryBase;

/**
 * PatientReportedEvent Sending queue repository
 */
public class PatientReportedEventQueueRepository extends SendingQueueRepositoryBase {

    public static final String TABLE_NAME = "PatientReportedEventQueue";

    /**
     * SQLite script to create this table
     */
    public static final String CREATE_TABLE_SQL_SCRIPT = "create table " + TABLE_NAME + " ("
            + COLUMN_ROW_ID + " INTEGER primary key "
            + ");";
    /*public static final String CREATE_TABLE_SQL_SCRIPT = "create table " + TABLE_NAME + " ("
            + COLUMN_ROW_ID + " INTEGER primary key,"
            + COLUMN_NETWORK_STATUS + " INTEGER ,"
            + COLUMN_TIME + " DATETIME DEFAULT CURRENT_TIME "
            + ");";*/
    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param queueDatabaseHelper database helper
     */
    public PatientReportedEventQueueRepository(QueueDatabaseHelper queueDatabaseHelper) {
        super(queueDatabaseHelper, TABLE_NAME);
    }


}

