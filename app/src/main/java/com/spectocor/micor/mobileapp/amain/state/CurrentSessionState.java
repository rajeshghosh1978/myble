package com.spectocor.micor.mobileapp.amain.state;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Savio Monteiro on 12/30/2015.
 */
public class CurrentSessionState {

    /*
        TODO: Create getters for each item
        Session Writer only accessible from package
     */

    public String facilityId = "";
    public String facilityName = "";

    public String patientName = "";

    public String enrollmentDate = "";
    public String enrollmentId = "";

    public String ecgBtName = "";
    public String ecgBtMac = "";

    public boolean ecgChosen = false;
    public boolean ecgChosenAtShipping = false;

    public boolean enrollmentConfirmed = false;
    public boolean summaryConfirmed = false;

    public boolean patientConfirmed = false;
    public boolean ecgBtConfirmed = false;

    public boolean signalDetected = false;
    public boolean heartRateDetected = false;
    public boolean heartBeatDetectionTestComplete = false;

    public boolean sessionActive = false;
    public boolean sessionReactivation = false;

    public boolean sessionTerminatedStillTransmitting = false;
    public boolean sessionTerminated = false;

    public boolean deviceDeactivatedStillTransmitting = false;
    public boolean deviceDeactivated = false;

    public long sessionStartTimestamp = -1;

    public String dbPathAndName = "";

    public int sessionId = -1;

    public ArrayList<SessionInfoForState> sessionList = new ArrayList<>();

    /**
     * Indicates the max timestamp that any event related to the particular session ID
     * can have. All events beyond this timestamp are to be discarded or ignored.
     */
    //public HashMap<Integer, Long> maxSessionTimestamp = new HashMap<>();


    public boolean isSalesAgent = false;

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int ssid) {
        sessionId = ssid;
    }

}

