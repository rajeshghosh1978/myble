package com.spectocor.micor.mobileapp.sessionactivation.api.requests;

@SuppressWarnings("unused")
public class ActivateSessionRequest {
    private final int sessionId;
    private final long activationUnixMs;
    private final String ecgBluetoothName;
    private final String ecgBluetoothMac;
    private final String mobileId;
    private final String facilityCode;

    //TODO: Verify if we need to have PatientId too or not.

    /**
     * @param sessionId        The Session ID of the patient generated at enrollment.
     * @param activationUnixMs Date & Time captured when the patient confirms Patient Name and ECG device.
     * @param ecgBluetoothName The friendly bluetooth name of the ECG device
     * @param ecgBluetoothMac  The MAC address of the ECG device
     * @param mobileId         Mobile Device identification (Currently Android, Build.SERIAL)
     * @param facilityCode     Code of the facility who has the patient
     */
    public ActivateSessionRequest(int sessionId, long activationUnixMs, String ecgBluetoothName, String ecgBluetoothMac, String mobileId, String facilityCode) {
        this.sessionId = sessionId;
        this.activationUnixMs = activationUnixMs;
        this.ecgBluetoothName = ecgBluetoothName;
        this.ecgBluetoothMac = ecgBluetoothMac;
        this.mobileId = mobileId;
        this.facilityCode = facilityCode;
    }

    public int getSessionId() {
        return sessionId;
    }

    public long getActivationUnixMs() {
        return activationUnixMs;
    }

    public String getEcgBluetoothName() {
        return ecgBluetoothName;
    }

    public String getEcgBluetoothMac() {
        return ecgBluetoothMac;
    }

    public String getMobileId() {
        return mobileId;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

}

