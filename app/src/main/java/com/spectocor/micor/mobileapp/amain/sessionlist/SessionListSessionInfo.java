package com.spectocor.micor.mobileapp.amain.sessionlist;

import com.google.gson.JsonSyntaxException;
import com.spectocor.micor.mobileapp.common.ObjectUtils;

/**
 * Created by Savio Monteiro on 2/20/2016.
 */
public class SessionListSessionInfo {


    public int sessionId;
    public SessionTypeEnum sessionType;
    public long sessionStartTimestamp;
    public long sessionEndTimestamp = -1;

    public SessionListSessionInfo(final int sessionId, final SessionTypeEnum sessionType, final long sessionStartTimestamp,
                                  final long sessionEndTimestamp) {

        if(SessionTypeEnum.asMyEnum(sessionType.toString()) == null) {
            throw new IllegalArgumentException("sessionType provided is not a valid type");
        }

        if(sessionStartTimestamp <= 0) {
            throw new IllegalArgumentException("Session Start/End timestamps must have values greater than 0");
        }

        this.sessionId = sessionId;
        this.sessionType = sessionType;

        this.sessionStartTimestamp = sessionStartTimestamp;
        this.sessionEndTimestamp = sessionEndTimestamp;

    }


    public void setSessionEndTimestamp(long sessionEndTimestamp) {

        if(sessionEndTimestamp <= 0) {
            throw new IllegalArgumentException("Session Start/End timestamps must have values greater than 0");
        }

        this.sessionEndTimestamp = sessionEndTimestamp;
    }



    /**
     * Returns the JSON representation of SessionListSessionInfo
     * @return
     */
    public String toJsonStr() {
        return ObjectUtils.toJson(this);
    }


    /**
     * Get object instance represented by JSONString
     * @param jsonStr
     * @return
     */
    public SessionListSessionInfo fromJson(String jsonStr) {

        SessionListSessionInfo sessionInfo;

        try {
            sessionInfo = ObjectUtils.fromJson(jsonStr, SessionListSessionInfo.class);
        } catch (JsonSyntaxException jse) {
            throw new JsonSyntaxException(jse.getMessage());
        }

        return sessionInfo;
    }





}
