package com.spectocor.micor.mobileapp.patientreportedevents;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.common.Log;

import java.sql.SQLDataException;

/**
 * SqLite Database helper class for Patient reported events.
 * Events are first inserted into this database before sending and marked as sent after sending.
 *
 * Created by Rafay Ali on 2/18/2016.
 */
public class PatientReportedEventsDbHelperOld extends SQLiteOpenHelper {

    public static final String TAG = PatientReportedEventsDbHelperOld.class.getSimpleName();

    private static final int DATABASE_VERSION = 4;
    private static String DATABASE_NAME = "MicorDatabase.db";

    private String INTEGER_DATATYPE = "INTEGER";
    private String TEXT_DATATYPE = "TEXT";

    private String TABLE_NAME = "PATIENT_REPORTED_EVENTS";
    private String COLUMN_NAME_SESSION_ID = "SESSION_ID";
    private String COLUMN_NAME_INSERT_TIMESTAMP = "INSERT_TIMESTAMP";
    private String COLUMN_NAME_TRANSMITTED_TIMESTAMP = "TRANSMITTED_TIMESTAMP";
    private String COLUMN_NAME_DATA = "DATA";


    private String QUERY_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            COLUMN_NAME_SESSION_ID          + " " + INTEGER_DATATYPE    + "," +
            COLUMN_NAME_INSERT_TIMESTAMP    + " " + INTEGER_DATATYPE    + "," +
            COLUMN_NAME_DATA                + " " + TEXT_DATATYPE       + "," +
            COLUMN_NAME_TRANSMITTED_TIMESTAMP + " " + INTEGER_DATATYPE    + "," +
            "PRIMARY KEY(" + COLUMN_NAME_SESSION_ID + "," + COLUMN_NAME_INSERT_TIMESTAMP + ")" +
            ")";

    private String QUERY_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    private static PatientReportedEventsDbHelperOld instance = null;

    private PatientReportedEventsDbHelperOld(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public static PatientReportedEventsDbHelperOld getInstance() {

        if(AMainApplication.DB_NAME_AND_PATH != null) {
            DATABASE_NAME = AMainApplication.DB_NAME_AND_PATH + "-PTE.db";
            Log.v(TAG, "Database File with Path (from AMainApplication): " + DATABASE_NAME);
        }

        if (instance == null){
            instance = new PatientReportedEventsDbHelperOld(AMainApplication.getAppContext());
        }

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.v(TAG, "Query: " + QUERY_CREATE_TABLE);
        db.execSQL(QUERY_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(QUERY_DROP_TABLE);
        onCreate(db);
    }

    public synchronized long insertPatientEvent(PatientEvent patientEvent){
        long rowId;

        try {
            ContentValues contentValues = new ContentValues();

            contentValues.put(COLUMN_NAME_SESSION_ID, patientEvent.getSessionId());
            contentValues.put(COLUMN_NAME_INSERT_TIMESTAMP, patientEvent.getInsertTimestamp());
            contentValues.put(COLUMN_NAME_TRANSMITTED_TIMESTAMP, patientEvent.getTransmittedTimestamp());
            contentValues.put(COLUMN_NAME_DATA, patientEvent.getData());

            rowId = getWritableDatabase().insert(TABLE_NAME, null, contentValues);

            if (rowId == -1) {
                throw new SQLDataException("Unable to insert PatientEvent in " + TABLE_NAME + " table");
            }
        }
        catch (SQLDataException e){
            Log.v(TAG, e.getMessage());
            return -1;
        }

        return rowId;
    }

    public synchronized long updatePatientEventAsTransmitted(PatientEvent patientEvent){
        long rowId;

        try {
            SQLiteDatabase database = this.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_NAME_TRANSMITTED_TIMESTAMP, System.currentTimeMillis());

            rowId = database.update(TABLE_NAME, contentValues,
                    COLUMN_NAME_SESSION_ID + "=? and " + COLUMN_NAME_INSERT_TIMESTAMP + "=?",
                    new String[]{"" + patientEvent.getSessionId(), "" + patientEvent.getInsertTimestamp()});


            if (rowId == -1) {
                throw new SQLDataException("Unable to update PatientEvent in " + TABLE_NAME + " table");
            }
        }
        catch (SQLDataException e){
            return -1;
        }

        return rowId;
    }

    public synchronized PatientEvent getOldestPatientEvent(){
        PatientEvent patientEvent = null;
        Cursor cursor = null;

        String QUERY = "SELECT * FROM " + TABLE_NAME + " WHERE " +
                COLUMN_NAME_TRANSMITTED_TIMESTAMP + "=-1 ORDER BY " +
                COLUMN_NAME_INSERT_TIMESTAMP + " LIMIT 1";

        try{
            cursor = getWritableDatabase().rawQuery(QUERY, new String[]{});

            if (cursor.getCount() == 0){
                Log.v(TAG, "Cursor returned 0 entries from database.");
                throw new SQLDataException("No values present to be retreived from " + TABLE_NAME + " table");
            }

            Log.v(TAG, "Cursor returned " + cursor.getCount() + " value from " + TABLE_NAME + " table");
            cursor.moveToFirst();

            patientEvent = new PatientEvent();
            patientEvent.setSessionId(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_SESSION_ID)));
            patientEvent.setInsertTimestamp(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_INSERT_TIMESTAMP)));
            patientEvent.setData(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_DATA)));
            patientEvent.setTransmittedTimestamp(cursor.getLong(cursor.getColumnIndex(COLUMN_NAME_TRANSMITTED_TIMESTAMP)));
        }
        catch (SQLDataException e){
            Log.v(TAG,"No values present to be retreived from " + TABLE_NAME + " table");
        }
        finally {
            if (cursor != null)
                cursor.close();
        }

        return patientEvent;
    }
}
