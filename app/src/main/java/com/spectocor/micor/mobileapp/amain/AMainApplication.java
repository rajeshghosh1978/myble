package com.spectocor.micor.mobileapp.amain;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.spectocor.micor.mobileapp.R;
import com.spectocor.micor.mobileapp.amain.alarms.FifoEcgAndPteHandlerAlarmReceiver;
import com.spectocor.micor.mobileapp.amain.receivers.AMainBroadcastReceiver;
import com.spectocor.micor.mobileapp.amain.receivers.BluetoothConnectivityReceiver;
import com.spectocor.micor.mobileapp.amain.receivers.CellInternetConnectivityReceiver;
import com.spectocor.micor.mobileapp.amain.receivers.EcgBatteryLevelReceiver;
import com.spectocor.micor.mobileapp.amain.receivers.MobileBatteryLevelReceiver;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.common.exceptions.GlobalExceptionHandler;
import com.spectocor.micor.mobileapp.ecgadapter.LiveEcgChunkInMemory;
import com.spectocor.micor.mobileapp.ecgblehandler.SynergenBleClient;
import com.stl.bleservice.STLBLE;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * Created by Savio Monteiro on 1/11/2016.
 */
public class AMainApplication extends Application {

    public static boolean LOGS_ENABLED = false;

    // enable/disable checking time for alerting at night/day
    public static boolean TIMECHECK_FOR_ALERTS = false;

    public static boolean PURE_FIFO_CHUNK_TRANSFER = true;




    public static final String TAG = AMainApplication.class.getSimpleName();

    public static boolean DO_BLUETOOTH = false;

    public static int CHUNK_SIZE_TO_TRANSFER_IN_SECONDS = 300;

    public static String SERVER_ADDRESS = "http://208.67.179.147";
    public static String URL_ACTIVATION = "/en/v1/patient/activation";
    public static String URL_FACILITY_INFO = "/en/v1/facility/info";
    public static String URL_PATIENT_LIST = "/en/v1/patient/list";
    public static String URL_SESSION_START = "/en/v1/session/start";
    public static String URL_ECG_CHUNK = "/en/v1/ecg/chunk";
    public static String URL_PATIENT_LOG = "/en/v1/log/patient";
    public static String URL_DEVICE_LOG= "/en/v1/log/device";

    public static String DB_PATH = "";
    public static String DB_NAME_AND_PATH = "";
    public static boolean BYPASS_ACTIVATION = false;


    public static byte[] currentSignature;

    //public static long TERMINATE_SESSION_AT = -1;
    //public static long TERMINATION_AFTER_MS = 15 * 60 * 1000;
    //public static long TERMINATION_AFTER_MS = 5 * 60 * 1000;


    private static void log(String str) {
        Log.d(TAG, str);
    }

    private static Context appContext;
    public static BroadcastReceiver ecgSimulatorAppDeadReceiver;

    private static BluetoothConnectivityReceiver bluetoothConnReceiver;
    private static EcgBatteryLevelReceiver ecgBatteryReceiver;
    private static MobileBatteryLevelReceiver mobileBatteryLevelReceiver;
    private static CellInternetConnectivityReceiver cellConnectivityReceiver;
    private static AMainBroadcastReceiver aMainBroadcastReceiver;



    public static Context getAppContext() {
        return appContext;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onCreate() {

        super.onCreate();
        appContext = this;



        /*
        SessionListAccess.emptySessionList(this);
        SessionStateAccess.saveSessionStateJson(this, "{}");
        */



        initProperties();
        //deleteAllDatabases();
        initLiveEcgHandlers();
        startServicesIfActiveSession();




        if (AMainApplication.DO_BLUETOOTH) {

            STLBLE.getInstance(this).bindService(new STLBLE.ServiceConnectionCallback() {
                @Override
                public void onConnected() {
                    if (STLBLE.getInstance(getApplicationContext()).initializeBluetoothService()) {
                        log("BLE Service Initialized ... ");
                        SynergenBleClient.bleServiceInitialized = true;
                    } else {
                        log("Could not initialize bluetooth service");
                        SynergenBleClient.bleServiceInitialized = false;
                    }
                }
            });
        }


        log("Application Started");

        registerAllReceivers();


        startFifoAlarm();


        if(LOGS_ENABLED)
            AppServiceManager.startDeviceLogService();


    }

    /**
     * Starts the periodic alarm for transmitting ECG and PTE.
     */
    public static boolean FIFO_ALARM_RUNNING = false;
    public static void startFifoAlarm() {

        log("Starting Fifo Alarm ... ");

        if(FIFO_ALARM_RUNNING) {

            log("... already running ... returning");

            return;
        }

        AlarmManager alarmMgr = (AlarmManager) getAppContext().getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getAppContext(), FifoEcgAndPteHandlerAlarmReceiver.class);
        intent.putExtra("CHUNK_SIZE_TO_TRANSFER_IN_SECONDS", (long) CHUNK_SIZE_TO_TRANSFER_IN_SECONDS);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getAppContext(), 0, intent, 0);

        alarmMgr.setExact(AlarmManager.RTC_WAKEUP, 0, alarmIntent);

        FIFO_ALARM_RUNNING = true;
    }


    /**
     * Initializes the LiveEcgHandlers
     */
    private void initLiveEcgHandlers() {

        LiveEcgChunkInMemory.initAll();

    }


    private void deleteAllDatabases() {

        log("DELETING ALL DATABASES in path: " + DB_PATH);
        deleteFilesRecursively(new File(DB_PATH));

    }



    void deleteFilesRecursively(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteFilesRecursively(child);

        if(fileOrDirectory.isFile()) {
            log("Deleting " + fileOrDirectory.getAbsolutePath());
            fileOrDirectory.delete();
        }

    }


    public static String APP_STORAGE_PATH = "";

    private String getAppPathForSdCard() {

        String sdPath = "/storage/ext_sd/Android/data/com.spectocor.micor.mobileapp/files";
        return sdPath;

    }

    private String determineAppPathForSdCard() {
        File[] writeableDirs = getExternalFilesDirs(null);
        if(writeableDirs.length>1) {
            File internalPath = writeableDirs[0];
            File sdPath = writeableDirs[1];
            return sdPath.getAbsolutePath() + "";
        }
        else
        {
            return getAppPathForSdCard();
        }


    }

    private void determineDbPathProgrammatically() {

        // detect writeable directories

        String SD_CARD_APP_PATH = "";
        String EMULATED_APP_PATH = "";


        File[] writeableDirs = getExternalFilesDirs(null);

        if(writeableDirs.length == 2) {

            log("SD CARD SLOT PRESENT");
            File sdDir = writeableDirs[1];
            if (sdDir == null) {
                APP_STORAGE_PATH = writeableDirs[0].getAbsolutePath(); // primary storage
                EMULATED_APP_PATH = APP_STORAGE_PATH;
                log("SD CARD NOT INSERTED");
            } else {

                EMULATED_APP_PATH = writeableDirs[0].getAbsolutePath();
                SD_CARD_APP_PATH = writeableDirs[1].getAbsolutePath();

                APP_STORAGE_PATH = SD_CARD_APP_PATH; // sd storage if not null
                log("SD CARD INSERTED. APP PATH: " + APP_STORAGE_PATH);


                // get storage state
                String appStorageState = Environment.getExternalStorageState(new File(APP_STORAGE_PATH));
                log("Storage State of " + APP_STORAGE_PATH + " = " + appStorageState);
                if(appStorageState.equals(Environment.MEDIA_SHARED)) {
                    log("CURRENTLY SHARED WITH PC!!!");
                }
            }

            // log("Storage State: " + Environment.getExternalStorageState(new File(APP_STORAGE_PATH)));
        }
        else if(writeableDirs.length == 1) {
            APP_STORAGE_PATH = writeableDirs[0].getAbsolutePath(); // primary storage
            EMULATED_APP_PATH = APP_STORAGE_PATH;
            log("NO SD CARD SLOT");
        }
        else {
            APP_STORAGE_PATH = ""; // app internal private storage

        }




        // create database folder
        if (APP_STORAGE_PATH.isEmpty()) {
            DB_PATH = "";
        }
        /*else
        {
            DB_PATH = SD_CARD_APP_PATH + File.separator + "databases";

            File dbPathF = new File(DB_PATH);

            if(!dbPathF.exists())
            {
                log("Creating recursive folders for: " + dbPathF.getAbsolutePath());

                boolean pathCreated = dbPathF.mkdirs();

                if(pathCreated && new File(dbPathF.getAbsolutePath()).exists()) {
                    log("Created ... " + dbPathF.getAbsolutePath() + " ... ");
                }
                else {
                    log("Could not create databases/ folder inside " + APP_STORAGE_PATH);

                    DB_PATH = EMULATED_APP_PATH + File.separator + "databases";

                    File emuDbPathF = new File(DB_PATH);
                    log("Attempting to create: " + DB_PATH);
                    boolean emuPathCreated = emuDbPathF.mkdirs();

                    if(emuPathCreated && new File(emuDbPathF.getAbsolutePath()).exists()) {
                        log("Created ... " + emuDbPathF.getAbsolutePath() + " ... ");
                    }
                    else {
                        DB_PATH = "";
                    }

                }
            }
            else {
                log(dbPathF.getAbsolutePath() + " ... ALREADY EXISTS!!!");
            }
        }*/

        if(DB_PATH == "")
            showLongToast("DB PATH IS STILL ''. NOT USING SD-CARD!", this);
    }

    /**
     * Set application configuration parameters based on app.properties file
     */
    private void initProperties() {


        // reading configuration from assets/app.properties
        InputStream propInStream = null;
        Properties config = new Properties();
        try {
            propInStream = getAssets().open("app.properties");
            config.load(propInStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (propInStream == null) {
            log("Properties is null");
            DO_BLUETOOTH = false;
            DB_PATH = "";
        } else {

            DO_BLUETOOTH = config.getProperty("DO_BLUETOOTH").equals("true");
            log("Property from file: DO_BLUETOOTH = " + DO_BLUETOOTH);

            //DB_PATH = config.getProperty("SD_CARD_DB_PATH").trim();

            //determineDbPathProgrammatically();
            //DB_PATH = getAppPathForSdCard();
            DB_PATH = determineAppPathForSdCard();
            APP_STORAGE_PATH = DB_PATH;


            log("DB_PATH = " + DB_PATH);


            CHUNK_SIZE_TO_TRANSFER_IN_SECONDS = Integer.parseInt(config.getProperty("CHUNK_SIZE_TO_TRANSFER_IN_SECONDS"));
            log("CHUNK_SIZE_TO_TRANSFER_IN_SECONDS = " + CHUNK_SIZE_TO_TRANSFER_IN_SECONDS);

            SERVER_ADDRESS = config.getProperty("SERVER_ADDRESS");
            log("SERVER_ADDRESS = " + SERVER_ADDRESS);

            URL_SESSION_START = config.getProperty("URL_SESSION_START");
            log("URL_SESSION_START = " + URL_SESSION_START);

            URL_ACTIVATION = config.getProperty("URL_ACTIVATION");
            log("URL_ACTIVATION = " + URL_ACTIVATION);

            URL_ECG_CHUNK = config.getProperty("URL_ECG_CHUNK");
            log("URL_ECG_CHUNK = " + URL_ECG_CHUNK);

            BYPASS_ACTIVATION = config.getProperty("BYPASS_ACTIVATION").equalsIgnoreCase("true");
            log("BYPASS_ACTIVATION = " + BYPASS_ACTIVATION);

            URL_PATIENT_LOG = config.getProperty("URL_PATIENT_LOG");
            log("URL_PATIENT_LOG = " + URL_PATIENT_LOG);

            URL_FACILITY_INFO = config.getProperty("URL_FACILITY_INFO");
            log("URL_FACILITY_INFO = " + URL_FACILITY_INFO);

            URL_PATIENT_LIST = config.getProperty("URL_PATIENT_LIST");
            log("URL_PATIENT_LIST = " + URL_PATIENT_LIST);
        }
    }


    private void startServicesIfActiveSession() {

        if(SessionStateAccess.getCurrentSessionState(this).sessionActive)
        {
            log("Session Active. Starting Services");
            AppServiceManager.startHttpService();
            AppServiceManager.startAMainService();
        }
    }

    /**
     * Registers all Local and Global Broadcasts Receivers ...
     */
    private void registerAllReceivers() {

        // initializing various notification receivers
        bluetoothConnReceiver = new BluetoothConnectivityReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(bluetoothConnReceiver, new IntentFilter(BluetoothConnectivityReceiver.BT_BROADCAST_ACTION));

        ecgBatteryReceiver = new EcgBatteryLevelReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(ecgBatteryReceiver, new IntentFilter(EcgBatteryLevelReceiver.ECG_BATT_BROADCAST_ACTION));

        mobileBatteryLevelReceiver = new MobileBatteryLevelReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(mobileBatteryLevelReceiver, new IntentFilter(MobileBatteryLevelReceiver.MOBILE_BATT_BROADCAST_ACTION));

        cellConnectivityReceiver = new CellInternetConnectivityReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(cellConnectivityReceiver, new IntentFilter(CellInternetConnectivityReceiver.INTERNET_CONNECTIVITY_BROADCAST_ACTION));

        aMainBroadcastReceiver = new AMainBroadcastReceiver();
        IntentFilter aMainBroadcastFilter = new IntentFilter();
            aMainBroadcastFilter.addAction(AMainBroadcastReceiver.ACTION_MOBILE_BATTERY_LEVEL_CHANGED);
            aMainBroadcastFilter.addAction(AMainBroadcastReceiver.ACTION_MOBILE_INTERNET_CONNECTIVITY_CHANGED);
            aMainBroadcastFilter.addAction(AMainBroadcastReceiver.ACTION_POWER_CONNECTED);
            aMainBroadcastFilter.addAction(AMainBroadcastReceiver.ACTION_POWER_DISCONNECTED);
        registerReceiver(aMainBroadcastReceiver, aMainBroadcastFilter);
    }

    /**
     * Unregisters all Local and Global Broadcasts Receivers ...
     */
    private void unregisterAllReceivers() {

        if (ecgSimulatorAppDeadReceiver != null) {
            unregisterReceiver(ecgSimulatorAppDeadReceiver);
        }

        if (bluetoothConnReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(bluetoothConnReceiver);
        }

        if (ecgBatteryReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(ecgBatteryReceiver);
        }

        if (mobileBatteryLevelReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mobileBatteryLevelReceiver);
        }

        if (cellConnectivityReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(cellConnectivityReceiver);
        }

        if(aMainBroadcastReceiver != null) {
            unregisterReceiver(aMainBroadcastReceiver);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        log("onLowMemory()");
    }


    @Override
    public void onTerminate() {
        super.onTerminate();

        log("onTerminate()");


        if (SynergenBleClient.bleServiceInitialized) {
            log("Unbinding STL BLE Service");
            try {
                STLBLE.getInstance(this).unbindService();
                log("Done");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        unregisterAllReceivers();
    }




    public static void showLongToast(final String str2, Context context) {

        //mHandler.obtainMessage(AMainApplication.MESG_LONG_TOAST, str).sendToTarget();

        try {
            SpannableStringBuilder biggerText2 = new SpannableStringBuilder(str2);
            biggerText2.setSpan(new RelativeSizeSpan(1.5f), 0, str2.length(), 0);
            Toast toast2 = Toast.makeText(context, biggerText2, Toast.LENGTH_LONG);
            TextView v2 = (TextView) toast2.getView().findViewById(android.R.id.message);
            if (v2 != null) v2.setGravity(Gravity.CENTER);
            toast2.getView().setBackground(context.getResources().getDrawable(R.drawable.toast_bg));

            toast2.show();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showShortToast(final String str, Context context) {
        //mHandler.obtainMessage(AMainApplication.MESG_SHORT_TOAST, str).sendToTarget();

        try {
            SpannableStringBuilder biggerText = new SpannableStringBuilder(str);
            biggerText.setSpan(new RelativeSizeSpan(1.5f), 0, str.length(), 0);
            Toast toast = Toast.makeText(context, biggerText, Toast.LENGTH_SHORT);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            if (v != null) v.setGravity(Gravity.CENTER);
            //toast.getView().setBackgroundColor(Color.argb(235, 88, 55, 55));
            toast.getView().setBackground(context.getResources().getDrawable(R.drawable.toast_bg));
            toast.show();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AMainApplication() {
        Thread.UncaughtExceptionHandler defaultCrashUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        GlobalExceptionHandler globalExceptionHandler = new GlobalExceptionHandler(this, AMainActivity.class, defaultCrashUncaughtExceptionHandler);
        Thread.setDefaultUncaughtExceptionHandler(globalExceptionHandler);
    }
}

