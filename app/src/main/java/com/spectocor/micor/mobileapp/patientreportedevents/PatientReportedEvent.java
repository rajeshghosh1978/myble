package com.spectocor.micor.mobileapp.patientreportedevents;


public class PatientReportedEvent {

    private int MonitoringSessionId;
    private int LogDateUnixSec;
    private EnumPatientReportedEventType PatientReportedEventTypeId;
    private String Comment;

    public PatientReportedEvent() {
        LogDateUnixSec = (int) (System.currentTimeMillis() / 1000);
    }

    public PatientReportedEvent(int monitoringSessionId, EnumPatientReportedEventType enumPatientReportedEventType) {
        MonitoringSessionId = monitoringSessionId;
        LogDateUnixSec = (int) (System.currentTimeMillis() / 1000);
        PatientReportedEventTypeId = enumPatientReportedEventType;
    }

    public PatientReportedEvent(int monitoringSessionId, EnumPatientReportedEventType enumPatientReportedEventType, String comment) {
        MonitoringSessionId = monitoringSessionId;
        LogDateUnixSec = (int) (System.currentTimeMillis() / 1000);
        PatientReportedEventTypeId = enumPatientReportedEventType;
        Comment = comment;
    }

    public final int getMonitoringSessionId() {
        return MonitoringSessionId;
    }

    public final void setMonitoringSessionId(int value) {
        MonitoringSessionId = value;
    }

    public final int getLogDateUnixSec() {
        return LogDateUnixSec;
    }

    public final void setLogDateUnixSec(int value) {
        LogDateUnixSec = value;
    }

    public final EnumPatientReportedEventType getPatientReportedEventTypeId() {
        return PatientReportedEventTypeId;
    }

    public final void setPatientReportedEventTypeId(EnumPatientReportedEventType value) {
        PatientReportedEventTypeId = value;
    }

    public final String getComment() {
        return Comment;
    }

    public final void setComment(String value) {
        Comment = value;
    }

}