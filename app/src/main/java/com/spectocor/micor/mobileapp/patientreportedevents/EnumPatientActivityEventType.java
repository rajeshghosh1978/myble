package com.spectocor.micor.mobileapp.patientreportedevents;

import android.util.SparseArray;

/**
 * Created by Rafay Ali on 2/4/2016.
 */
public enum EnumPatientActivityEventType {
    Resting(1),
    Walking(2),
    Exercising(3),
    Other(4);

    private static SparseArray<EnumPatientActivityEventType> mappings;
    private int intValue;

    EnumPatientActivityEventType(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static SparseArray<EnumPatientActivityEventType> getMappings() {
        if (mappings == null) {
            synchronized (EnumPatientActivityEventType.class) {
                if (mappings == null) {
                    mappings = new SparseArray<>();
                }
            }
        }
        return mappings;
    }

    public static EnumPatientActivityEventType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
