package com.spectocor.micor.mobileapp.sessionactivation.api;

import java.text.ParseException;

/**
 * Created by Admin on 2/10/2016.
 */
public class SessionInfo {
    private String endDateTime;
    private String startDateTime;
    private long duration;
    private int sessionId;
    private boolean IsActive;
    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) throws ParseException {
        this.endDateTime = endDateTime;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duaration) {
        this.duration = duaration;
    }

    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean isActive) {
        this.IsActive = isActive;
    }
}
