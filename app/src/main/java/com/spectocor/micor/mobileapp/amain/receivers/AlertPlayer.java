package com.spectocor.micor.mobileapp.amain.receivers;

import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;

import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainActivityHandler;
import com.spectocor.micor.mobileapp.amain.AMainApplication;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by Savio Monteiro on 2/1/2016.
 */
public class AlertPlayer
{

    private static long lastPlayedAudioAlertAt = -1;
    private static long lastPlayedVibrationAlertAt = -1;


    private static void log(String str) {
        MLog.log(AlertPlayer.class.getSimpleName() + ": " + str);
    }

    /**
     * If no audio alert is played in the past 10 seconds
     * Set Volume to Max and Play Notification
     *
     * @param mContext The application context
     */
    public static synchronized void playAudioAlert(Context mContext) {

        /*if(!SessionStateAccess.getCurrentSessionState(mContext).sessionActive) {
            log("Session Inactive not audio alerting");
            return;
        }*/

        if(lastPlayedAudioAlertAt != -1)
            lastPlayedAudioAlertAt = System.currentTimeMillis() - 100001;

        long timeSinceLastAudioAlert = (System.currentTimeMillis() - lastPlayedAudioAlertAt);
        if(timeSinceLastAudioAlert < 10000) {

            log("Audio Alert was just played in the past " + (int)(timeSinceLastAudioAlert/1000) + " sec ago");
            return;
        }


        try {

            log("Playing Audio Alert!");

            AudioManager mgr = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
            mgr.setStreamVolume(AudioManager.STREAM_RING, mgr.getStreamMaxVolume(AudioManager.STREAM_RING), 0);

            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(mContext, notification);
            r.play();

            lastPlayedAudioAlertAt = System.currentTimeMillis();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Vibrate the device
     * @param mContext The Application Context
     */
    public static synchronized void playVibrateAlert(Context mContext) {

        /*if(!SessionStateAccess.getCurrentSessionState(mContext).sessionActive) {
            log("Session Inactive not vibrating");
            return;
        }*/

        long timeSinceLastVibrateAlert = System.currentTimeMillis() - lastPlayedVibrationAlertAt;

        if(lastPlayedVibrationAlertAt != -1)
            lastPlayedVibrationAlertAt = System.currentTimeMillis() - 100001;

        if(timeSinceLastVibrateAlert < 10000) {

            log("Last vibration was played " + (int) (timeSinceLastVibrateAlert/1000) + " secs ago");

            return;
        }

        log("Playing Vibration Alert!");

        Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        ///long[] pattern = {0, 100, 900};
        v.vibrate(2000); // pattern, 0);

        lastPlayedVibrationAlertAt = System.currentTimeMillis();

    }


    /**
     * To display the main activity
     * @param mContext
     */
    public static synchronized void displayActivity(Context mContext) {

        /*if(!SessionStateAccess.getCurrentSessionState(mContext).sessionActive) {
            log("Session Inactive. Returning ... ");
            return;
        }*/

        Intent i = new Intent(mContext, AMainActivity.class);
        i.putExtra("SCREEN_ON", true);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
    }


    /**
     * To turn screen on
     * @param context
     */
    public static synchronized void turnScreenOn(Context context) {

        /*if(!SessionStateAccess.getCurrentSessionState(context).sessionActive) {
            log("Session Inactive. Returning from ... turnScreenOn");
            return;
        }*/


        PowerManager mPowerManager;
        PowerManager.WakeLock mWakeLock;

        KeyguardManager km;

        km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        final KeyguardManager.KeyguardLock kl = km.newKeyguardLock("MyKeyguardLock");
        kl.disableKeyguard();

        mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);

        // Create a bright wake lock
        mWakeLock = mPowerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.ON_AFTER_RELEASE, "");
        mWakeLock.acquire(5000);

        if(AMainActivity.activityHandler != null) {
            AMainActivity.activityHandler.obtainMessage(AMainActivityHandler.ACTIVITY_TURN_SCREEN_ON).sendToTarget();
            /*AMainActivity.activityHandler.obtainMessage(AMainActivityHandler.ACTIVITY_DISPLAY_ISSUES_SCREEN).sendToTarget();*/
        }

    }


    /**
     * Display screen to show customer service information with no internet message
     * @param internetWentDownAtMs
     * @param mContext
     */
    public static synchronized void displayCustomerServiceNoInternet(long internetWentDownAtMs, Context mContext) {
        Intent i = new Intent(mContext, AMainActivity.class);
        i.putExtra("SCREEN_ON", true);
        i.putExtra("DISPLAY_CUSTOMER_SERVICE_INTERNET_DISCONNECTED", internetWentDownAtMs);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
    }


    /**
     * To determine whether to show alert to patient when session has terminated.
     *
     * If it is night, wait till 8am next morning to display termination screen.
     * @param context
     */
    public static synchronized void alertForTermination(final Context context) {



        if(!AMainApplication.TIMECHECK_FOR_ALERTS) {

            log("Timecheck is turned off. Alerting right away.");

            AlertPlayer.turnScreenOn(context);
            AlertPlayer.playVibrateAlert(context);
            AlertPlayer.playAudioAlert(context);

            LocalBroadcastManager.getInstance(context).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.DISPLAY_SESSION_TERMINATION_SCREEN, new Bundle()));


            return;
        }


        /*
        Get current time.
        If at night? Set alarm for 8am next day.
         */


        log("Timezone: " + TimeZone.getDefault().getID());

        GregorianCalendar c = (GregorianCalendar) GregorianCalendar.getInstance(TimeZone.getDefault());

        int hourOfDay = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);


        log("Alert Requested for " + hourOfDay + ":" + minute);

        if(hourOfDay >= 20 || hourOfDay < 8) {

            log("Not a good time to alert");

            GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance(TimeZone.getDefault());
            calendar.setTimeInMillis(System.currentTimeMillis());

            calendar.add(Calendar.DATE, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 8);
            calendar.set(Calendar.MINUTE, 0);

            log("Calendar set for " + calendar.getTime());

            AlarmManager alarmMgr;
            PendingIntent alarmIntent;

            alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, SessionTerminationAlarmReceiver.class);
            alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
        }
        else
        {
            AlertPlayer.turnScreenOn(context);
            AlertPlayer.playVibrateAlert(context);
            AlertPlayer.playAudioAlert(context);

            LocalBroadcastManager.getInstance(context).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.DISPLAY_SESSION_TERMINATION_SCREEN, new Bundle()));
        }

    }




    /**
     * To determine whether to show alert to patient when session has been deactivated for replacement.
     *
     * If it is night, wait till 8am next morning to display deactivation screen.
     * @param context
     */
    public static synchronized void alertForDeactivation(final Context context) {

        AlertPlayer.turnScreenOn(context);
        AlertPlayer.playVibrateAlert(context);
        AlertPlayer.playAudioAlert(context);

        LocalBroadcastManager.getInstance(context).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.DISPLAY_DEVICE_NO_LONGER_ACTIVE_SCREEN, new Bundle()));

    }





    /**
     * To determine whether to show alert to patient when session has been deactivated for replacement.
     *
     * If it is night, wait till 8am next morning to display deactivation screen.
     * @param context
     */
    public static synchronized void alertForDeactivationWithTimeDetection(final Context context) {

        /*
        Get current time.
        If at night? Set alarm for 8am next day.
         */


        log("Timezone: " + TimeZone.getDefault().getID());

        GregorianCalendar c = (GregorianCalendar) GregorianCalendar.getInstance(TimeZone.getDefault());

        int hourOfDay = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);


        log("Alert Requested for " + hourOfDay + ":" + minute);

        if(hourOfDay >= 20 || hourOfDay < 8) {

            log("Not a good time to alert");


            GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance(TimeZone.getDefault());
            calendar.setTimeInMillis(System.currentTimeMillis());

            calendar.add(Calendar.DATE, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 8);
            calendar.set(Calendar.MINUTE, 0);

            log("Calendar set for " + calendar.getTime());

            AlarmManager alarmMgr;
            PendingIntent alarmIntent;

            alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, DeviceNoLongerActiveAlarmReceiver.class);
            alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
        }
        else
        {
            AlertPlayer.turnScreenOn(context);
            AlertPlayer.playVibrateAlert(context);
            AlertPlayer.playAudioAlert(context);

            LocalBroadcastManager.getInstance(context).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.DISPLAY_DEVICE_NO_LONGER_ACTIVE_SCREEN, new Bundle()));
        }

    }



    public static void showToast(String str, Context context) {
        AMainApplication.showLongToast(str, context);
    }
}
