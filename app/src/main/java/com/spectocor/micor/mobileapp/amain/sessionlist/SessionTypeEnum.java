package com.spectocor.micor.mobileapp.amain.sessionlist;

/**
 * Created by Savio Monteiro on 2/20/2016.
 */
public enum  SessionTypeEnum {

    SESSION_TYPE_TELEMETRY,
    SESSION_TYPE_HOLTER,
    SESSION_TYPE_POST_HOLTER_TELEMETRY;


    public static SessionTypeEnum asMyEnum(String str) {
        for (SessionTypeEnum me : SessionTypeEnum.values()) {
            if (me.name().equalsIgnoreCase(str))
                return me;
        }
        return null;
    }
}
