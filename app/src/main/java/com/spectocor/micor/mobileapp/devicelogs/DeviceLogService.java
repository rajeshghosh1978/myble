package com.spectocor.micor.mobileapp.devicelogs;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.http.HttpQueueManager;
import com.spectocor.micor.mobileapp.http.requests.DeviceLogHttpRequest;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.spectocor.micor.mobileapp.amain.state.SessionStateAccess.getCurrentSessionState;

/**
 * Created by Admin on 3/9/2016.
 */
public class DeviceLogService extends Service {


    public static boolean sendToSignalView = false;
    private static boolean mActivityBound = false;
    public static boolean APP_DEAD = false;
    private final IBinder mBinder = new LocalBinder();
    public DeviceLogService mService = null;
    public Context mContext = null;
    private static CurrentSessionState mCurrentSessionState = null;
    static Timer timerForMonitoringLogs = new Timer();
    private final int TIMER_INTERVAL_TO_MONITOR_LOGS =  10 * 1000;

    public class LocalBinder extends Binder {
        public DeviceLogService getService() {
            return DeviceLogService.this;
        }
    }

    public static void log(String str) {
        MLog.log("AMainService: " + str);
    }




    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        mActivityBound = true;
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {

        mActivityBound = false;
        return super.onUnbind(intent);
    }


    @Override
    public void onCreate() {
        super.onCreate();

        log("onCreate()");

        mService = this;
        mContext = this;
        mCurrentSessionState = getCurrentSessionState(this);

        initDeviceLogMonitoring();


        //Log Event ApplicationStarted
        DeviceLogEvents.getInstance().putLogApplicationStarted();

        APP_DEAD = false;


    }




    @Override
    public void onDestroy() {

        log("service: onDestroy()");

        //Log Event ApplicationEnded
        DeviceLogEvents.getInstance().putLogApplicationEnded(DeviceLogService.class.getSimpleName() + " has Stopped");

        super.onDestroy();

        APP_DEAD = true;

    }


    private void initDeviceLogMonitoring() {
        timerForMonitoringLogs.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                try {
                    final ArrayList<DeviceLogEntity> pileUpLogs = DeviceLogsDbHelper.getInstance().retrieveLogsUpTillNow();

                    if (pileUpLogs != null && pileUpLogs.size() > 0) {

                        final DeviceLogMapper deviceLogToSend = new DeviceLogMapper();

                        ArrayList<Logs> logsToPut = new ArrayList<>();
                        Logs log;

                        //Filling logs
                        for (DeviceLogEntity pileUpLog : pileUpLogs) {
                            log = new Logs();
                            log.setType(pileUpLog.getEventTypeId());
                            log.setEventTimestamp(pileUpLog.getInsertTimestamp());
                            log.setDescription(pileUpLog.getEventMessage());
                            logsToPut.add(log);
                        }
                        int sessionIdToSend=0;
                        if(pileUpLogs!=null && pileUpLogs.size()>0)
                        {
                            sessionIdToSend = pileUpLogs.get(pileUpLogs.size()-1).getSessionId();
                        }
                        if(sessionIdToSend!=0)
                            deviceLogToSend.setSessionId(String.valueOf(sessionIdToSend));

                        deviceLogToSend.setEcgId(DeviceIdentifiers.getEcgId());
                        deviceLogToSend.setDeviceId(DeviceIdentifiers.getPdaId());
                        deviceLogToSend.setLogs(logsToPut);
                        Gson gson = new Gson();
                        String objInStr = gson.toJson(deviceLogToSend, DeviceLogMapper.class);
                        log("DB:PileUpLogs Data=" + objInStr);


                        //sending device logs to server
                        if(deviceLogToSend!=null && deviceLogToSend.getLogs().size()>0) {
                            final DeviceLogHttpRequest deviceLogs = new DeviceLogHttpRequest();
                                deviceLogs.setDeviceLogsToBeSend(deviceLogToSend);
                                deviceLogs.setLogToUpdate(pileUpLogs);

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                HttpQueueManager.getInstance().enqueueRequest(deviceLogs, true);
                                }
                            }).start();

                        }
                        else
                        {
                            log("There is no log to Push");
                        }

                    }
                } catch (SQLDataException t) {

                    log(t.getMessage());
                }


            }
        }, TIMER_INTERVAL_TO_MONITOR_LOGS, TIMER_INTERVAL_TO_MONITOR_LOGS);
    }
}
