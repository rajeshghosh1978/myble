package com.spectocor.micor.mobileapp.amain.state;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.common.ObjectUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.NoSuchElementException;
//import com.spectocor.micor.mobileapp.common.Log;


/**
 * Created by Savio Monteiro on 12/30/2015.
 */
public class SessionStateAccess {

    public static final String APP_STATE_JSON = "APP_STATE_JSON";

    private static final String TAG = SessionStateAccess.class.getSimpleName();

    private static void log(String str) {
        Log.d("SessionStateAccess", "=====================");
        Log.d("SessionStateAccess", str);
    }
    public static synchronized boolean saveSessionStateJson(Context context, String jsonStr) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(APP_STATE_JSON, jsonStr);
        editor.apply();

        return true;
    }


    public static synchronized String getCurrentActivationStateJson(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(ObjectUtils.fromJson(APP_STATE_JSON, Object.class).toString(), "{}");

    }

    public static synchronized CurrentSessionState getCurrentSessionState(Context context) {

        String jsonStr;
        try {
            jsonStr = URLDecoder.decode(getCurrentActivationStateJson(context), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            jsonStr = "{}";
        }

        if(jsonStr == "")
            jsonStr = "{}";


        //log(jsonStr + "");

        if(jsonStr.equals("{}")) {
            return new CurrentSessionState();
        }


        CurrentSessionState state = ObjectUtils.fromJson(jsonStr, CurrentSessionState.class);

        return state;

    }

    public static synchronized void setSessionList(final ArrayList<SessionInfoForState> sessionList, Context context) {

        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        currentSessionState.sessionList = sessionList;

        String json = ObjectUtils.toJson(currentSessionState);
        saveSessionStateJson(context, json);
    }

    /*public static synchronized void initializeSessionMaxTimestamp(final int sessionId, Context context) {
        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        currentSessionState.maxSessionTimestamp = new HashMap<>();
        currentSessionState.maxSessionTimestamp.put(sessionId, (long) -1);

        String json = ObjectUtils.toJson(currentSessionState);
        saveSessionStateJson(context, json);
    }*/

    /**
     * This is set when session termination or deactivation occurs to store whether or not to wait for buffered transmission.
     *
     * @param sessionId
     * @param context
     */
    /*public static synchronized long setMaxSessionTimestampAtTermination(final int sessionId, Context context) {

        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        log("Setting Session Max Timestamp for sessionId: " + sessionId);

        if(sessionId != currentSessionState.sessionId) {

            log("But current sessionID is " + currentSessionState.sessionId + " ... returning !");
            return -1;
        }

        long durationInMs = -1;
        for(SessionInfoForState sessionInfo: currentSessionState.sessionList) {
            if(sessionInfo.sessionId == sessionId) {

                if(sessionInfo.sessionDurationMs == -1) {

                    log("SESSION DURATION UNKNOWN FOR SESSIONID:" + sessionId);

                    return -1;
                }

                durationInMs = sessionInfo.sessionDurationMs;
                break;
            }
        }

        if(durationInMs == -1) {

            log("SESSION DURATION UNKNOWN FOR SESSIONID:" + sessionId);
            return -1;
        }

        long currentTimestamp = System.currentTimeMillis();

        long sessionMaxTimestamp = currentTimestamp + AMainApplication.DEFER_SESSION_END_FOR_BUFFERED_MILLISECONDS;

        currentSessionState.maxSessionTimestamp.put(sessionId, sessionMaxTimestamp);

        String json = ObjectUtils.toJson(currentSessionState);
        saveSessionStateJson(context, json);

        return sessionMaxTimestamp;
    }*/




    /*public static synchronized long getMaxSessionTimestampAtTermination(int sessionId, Context context) {

        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        log("Setting Session Max Timestamp for sessionId: " + sessionId);

        if(sessionId != currentSessionState.sessionId) {

            log("But current sessionID is " + currentSessionState.sessionId + " ... returning !");
            return -1;
        }

        return currentSessionState.maxSessionTimestamp.get(sessionId);
    }*/



    public static synchronized void setSessionId(int sessionId, Context context) {
        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        currentSessionState.sessionId = sessionId;

        String json = ObjectUtils.toJson(currentSessionState);
        saveSessionStateJson(context, json);
    }



    public static synchronized int getCurrentSessionId(Context context) {
        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        return currentSessionState.sessionId;

    }

    /**
     *  Getting the next session id in the session list within the state
     */
    public static synchronized int getNextSessionId(Context context) {
        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        int currSessionId = currentSessionState.sessionId;

        log("SESSION LIST: \n" + ObjectUtils.toJson(currentSessionState.sessionList));

        for(SessionInfoForState sessionInfo: currentSessionState.sessionList) {

            log("Checking sessionId: " + sessionInfo.sessionId);

            if(sessionInfo.sessionId == currSessionId) {
                log(" .... same as prevSessionId! Nope ... ");
                continue;
            }

            if (!sessionInfo.isExpired) {
                log(" .... Found!");
                return sessionInfo.sessionId;
            }

        }

        return -1; // indicating no more sessions.
    }


    public static synchronized boolean getIsSessionExpired(int sessionId, Context context) throws NoSuchElementException{

        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        for(SessionInfoForState sessionInfo: currentSessionState.sessionList) {
            if(sessionInfo.sessionId == sessionId) {
                return sessionInfo.isExpired;
            }
        }

        throw new NoSuchElementException("No such session id");
    }

    public static synchronized void setSessionExpired(int sessionId, Context context) {
        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        for(SessionInfoForState sessionInfo: currentSessionState.sessionList) {
            if(sessionInfo.sessionId == sessionId) {
                sessionInfo.isExpired = true;
                break;
            }
        }

        String json = ObjectUtils.toJson(currentSessionState);
        saveSessionStateJson(context, json);
    }

    public static synchronized void setSessionTerminationTimestamp(int sessionId, Context context) {
        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        for(SessionInfoForState sessionInfo: currentSessionState.sessionList) {
            if(sessionInfo.sessionId == sessionId) {
                sessionInfo.sessionExpiryTimestamp = System.currentTimeMillis();
                break;
            }
        }

        String json = ObjectUtils.toJson(currentSessionState);
        saveSessionStateJson(context, json);
    }

    public static synchronized long getSessionTerminationTimestamp(int sessionId, Context context) {
        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        for(SessionInfoForState sessionInfo: currentSessionState.sessionList) {
            if(sessionInfo.sessionId == sessionId) {
                return sessionInfo.sessionExpiryTimestamp;
            }
        }

        return -1;
    }

    public static synchronized void setSessionTerminatedStillTransmitting(Context context) {
        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        currentSessionState.sessionTerminatedStillTransmitting = true;

        String json = ObjectUtils.toJson(currentSessionState);
        saveSessionStateJson(context, json);
    }

    public static synchronized void setSessionTerminatedAndInactive(Context context) {

        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        currentSessionState.sessionActive = false;
        currentSessionState.sessionTerminated = true;

        String json = ObjectUtils.toJson(currentSessionState);
        saveSessionStateJson(context, json);
    }


    public static synchronized void setDeviceDeactivatedStillTransmitting(Context context) {
        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        currentSessionState.deviceDeactivatedStillTransmitting = true;

        String json = ObjectUtils.toJson(currentSessionState);
        saveSessionStateJson(context, json);
    }

    public static synchronized void setDeviceDeactivated(Context context) {

        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        currentSessionState.deviceDeactivated = true;

        String json = ObjectUtils.toJson(currentSessionState);
        saveSessionStateJson(context, json);
    }



    // MAY BE REMOVED
    public static synchronized void reactivateCurrentSession(Context context) {

        CurrentSessionState currentSessionState = getCurrentSessionState(context);

        currentSessionState.sessionActive = true;
        currentSessionState.sessionTerminated = false;

        String json = ObjectUtils.toJson(currentSessionState);
        saveSessionStateJson(context, json);

    }

}
