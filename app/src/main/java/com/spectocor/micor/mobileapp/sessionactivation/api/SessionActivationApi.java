package com.spectocor.micor.mobileapp.sessionactivation.api;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.amain.state.SessionInfoForState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.entities.PatientActivation;
import com.spectocor.micor.mobileapp.entities.PatientLog;
import com.spectocor.micor.mobileapp.http.HttpManager;
import com.spectocor.micor.mobileapp.http.common.NetworkHelper;
import com.spectocor.micor.mobileapp.http.common.ResponseCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import java.util.Iterator;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Savio Monteiro on 2/18/2016.
 */
public class SessionActivationApi {

    public static final String TAG = SessionActivationApi.class.getSimpleName();

    private static SessionActivationApi sessionActivationApi;


    private static Context mContext;

    private PatientLog patientLog;
    private PatientActivation patientData;
    String[] arrdateEnd,arrdateStart;
    SessionInfoForState si = null;
    private static void log(String str) {

        Log.v(TAG, str);

    }

    SessionActivationApi(Context context) {
        mContext = context;
        patientLog = new PatientLog();
    }

    public static SessionActivationApi getInstance(Context context) {
        if(sessionActivationApi == null) {
            sessionActivationApi = new SessionActivationApi(context);
        }

        return sessionActivationApi;
    }

    public static SessionActivationApi getInstance(){
        if (sessionActivationApi == null){
            sessionActivationApi = new SessionActivationApi(AMainApplication.getAppContext());
        }

        return sessionActivationApi;
    }


    /**
     * API call to get Enrollment By Activation Code.
     * @param activationCode
     * @param jsCallbackFn
     * @return
     */
    public String getEnrollmentByActivationCode(final String activationCode, final String jsCallbackFn) {

        if(!NetworkHelper.isInternetAvailable(mContext))
        {
            String fnCallbackFull = jsCallbackFn + "('NO_INTERNET')";

            Bundle b = new Bundle();
            b.putString("callbackFnString", fnCallbackFull);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
            return "NO_INTERNET";
        }

        String jsonStr;
        String currSessionId;

        EnrollmentInfo enrInfo = new EnrollmentInfo();

        log("Getting enrollment for activation code: " + activationCode + " .... SessionState: " + SessionStateAccess.getCurrentActivationStateJson(mContext));

        try
        {
            //EnrollmentInfo enrInfo = sessionActivationApi.getEnrollmentByActivationCode(activationCode);

            RequestBody requestBody = new MultipartBody.Builder()
                    .addFormDataPart("activationCode", activationCode)
                    .setType(MultipartBody.FORM)
                    .build();

            Request request = new Request.Builder()
                    .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_ACTIVATION)
                    .post(requestBody)
                    .build();

            Response response = HttpManager.getInstance().getHttpClient().newCall(request).execute();
            String responseBody = response.body().string();

            log("Response: getEnrollmentByActivationCode: " + responseBody);

            JSONObject responseJSON = new JSONObject(responseBody);
            int responseCode = responseJSON.getInt("code");
            log("StatusCode: " + responseCode + " , ResponseBody: " + responseBody);

            if (responseCode == ResponseCodes.OK || responseCode == ResponseCodes.ALREADY_STARTED)
            {

                JSONObject dataJSON = responseJSON.getJSONObject("data");
                JSONArray sessions = dataJSON.getJSONArray("session");

                Gson gson = new Gson();
                patientData= gson.fromJson(dataJSON.toString(), PatientActivation.class);


                int firstSessionId = -1;

                SessionInfoForState sessionToActivate = null;

                if(patientData != null) {
                    patientLog.addPatientActivationLogs(patientData);

                    enrInfo.setFacilityName(patientData.getFacility().getName());
                    enrInfo.setFirstName(patientData.getPatient().getFirstName());
                    enrInfo.setLastName(patientData.getPatient().getLastName());
                    enrInfo.setLastNameInitial(patientData.getPatient().getLastName());
                    String[] dateSplit = patientData.getPatient().getEnrollmentDate().split("T");
                    enrInfo.setEnrollmentDateIso8601(dateSplit[0]);

                    ArrayList<SessionInfoForState> sessionList = new ArrayList<>();


                    for (int i = 0; i < sessions.length(); i++) {

                        si = new SessionInfoForState(
                            patientData.getSession().get(i).getSessionId(),
                            patientData.getSession().get(i).getIsExpired(),
                            patientData.getSession().get(i).getDuration()
                        );

                        enrInfo.addSession(si);

                        log("Adding session to sessionList: " + si.sessionId);
                        sessionList.add(new SessionInfoForState(si.sessionId, si.isExpired, si.sessionDurationMs));


                        if(!patientData.getSession().get(i).getIsExpired()) {
                            if(sessionToActivate == null) {

                                sessionToActivate = new SessionInfoForState(si.sessionId, si.isExpired, si.sessionDurationMs);

                                log("Setting sessionID to activate: " + si.sessionId);
                                firstSessionId = si.sessionId;

                                SessionStateAccess.setSessionId(si.sessionId, mContext);
                            }
                        }
                        else {
                            log(si.sessionId + " expired");
                            log("Setting termination timestamp " + si.sessionId);
                            SessionStateAccess.setSessionTerminationTimestamp(si.sessionId, mContext);
                        }
                    }

                }

                log("Enrollment Received: State: " + SessionStateAccess.getCurrentActivationStateJson(mContext));

                boolean deviceReplacement = (responseCode == ResponseCodes.ALREADY_STARTED);

                jsonStr = "{'sessionId':'" + firstSessionId + "'," +
                            "'firstName':'" + enrInfo.getFirstName() + "'," +
                            "'lastName':'" + enrInfo.getLastName() + "'," +
                            "'lastNameInitial':'" + enrInfo.getLastNameInitial() + "'," +
                            "'enrollmentDateIso8601':'" + enrInfo.getEnrollmentDateIso8601() + "'," +
                            "'facilityName':'" + enrInfo.getFacilityName() + "'," +
                            "'facilityId':''," +
                            "'activationCodeInUse':" + deviceReplacement + "," +
                            "'sessionList':" + ObjectUtils.toJson(enrInfo.sessionList).replace("\"", "'") +
                        "}";

            } // if 401
            else if (responseCode == ResponseCodes.SESSION_EXPIRED) {
                jsonStr = "'ALL_SESSIONS_TERMINATED'";
            }
            else {
                jsonStr = null;
            }
        }
        catch (ParseException | IOException | JSONException e)
        {
            e.printStackTrace();
            jsonStr = null;
        }

        log("Enrollment Info: " + jsonStr);

        //basePreferenceHelper.putjsonString(jsonStr);
        String fnCallbackFull = jsCallbackFn + "("+jsonStr+")";

        Bundle b = new Bundle();
        b.putString("callbackFnString", fnCallbackFull);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));

        return jsonStr;

    }


    /**
     * Currently jsFnCallback is "sessionStartAPIFinished"
     * API Call to start session after confirming patient and device
     * without making UI updates
     * @return
     */
    public String startSessionCall(final String sessionID, final String ecgId,
                                   final String deviceId, final int defaultChunkSize)
    {
        return startSessionCall(sessionID, ecgId, deviceId, defaultChunkSize, null);
    }

    /**
     * Currently jsFnCallback is "sessionStartAPIFinished"
     * API Call to start session after confirming patient and device
     * @param jsCallbackFn
     * @return
     */
    public String startSessionCall(final String sessionID, final String ecgId,
                                   final String deviceId, final int defaultChunkSize, final String jsCallbackFn)
    {

        if(!NetworkHelper.isInternetAvailable(mContext))
        {
            String fnCallbackFull = jsCallbackFn + "('NO_INTERNET')";

            Bundle b = new Bundle();
            b.putString("callbackFnString", fnCallbackFull);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
            return "NO_INTERNET";
        }

        log("startSessionCall called");

        if (AMainApplication.BYPASS_ACTIVATION) {

            Bundle b = new Bundle();
            b.putString("callbackFnString", jsCallbackFn + "('success')");
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));

            return jsCallbackFn + "('success')";
        }

        OkHttpClient httpClient = HttpManager.getInstance().getHttpClient();


        boolean deviceReplacement = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext()).sessionReactivation;

        byte[] eSignatureBytes = AMainApplication.currentSignature;


        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("sessionId", "" + Integer.parseInt(sessionID))
                .addFormDataPart("startTimestamp", "" + System.currentTimeMillis())
                .addFormDataPart("ecgId", ecgId)
                .addFormDataPart("deviceId", deviceId)
                .addFormDataPart("samplingRate", EcgProperties.SAMPLE_RATE + "")
                .addFormDataPart("deviceChunkSize", defaultChunkSize + "")
                .addFormDataPart("deviceReplacement", "" + deviceReplacement);


        if(eSignatureBytes != null) {

            log("Attaching Signature bytes with request: " + AMainApplication.currentSignature.length + " bytes");

            builder.addFormDataPart("eSignatureJpg", "eSignatureJpg.jpg", RequestBody.create(MediaType.parse("application/octet-stream"), AMainApplication.currentSignature));
        }

        RequestBody requestBody = builder.build();

        Request request = new Request.Builder()
                .post(requestBody)
                .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_SESSION_START)
                .build();

        try
        {
            Response response = httpClient.newCall(request).execute();
            String responseBody = response.body().string();
            JSONObject responseJSON = new JSONObject(responseBody);

            int responseCode = responseJSON.getInt("code");

            Log.v(TAG, ObjectUtils.fromJson(responseBody, Object.class).toString());
            Log.v(TAG, "SessionId: " + sessionID);
            Log.v(TAG, "DeviceId: " + deviceId);
            Log.v(TAG, "EcgId: " + ecgId);
            Log.v(TAG, "Device Replacement: " + deviceReplacement);

            if (responseCode == ResponseCodes.OK || responseCode == ResponseCodes.ALREADY_STARTED)
            {

                if(jsCallbackFn == null)
                    return "success";

                Bundle b = new Bundle();
                b.putString("callbackFnString", jsCallbackFn + "('success')");
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
                return "success";

            }/*
            else if (responseCode == ResponseCodes.ALREADY_STARTED)
            {

                Bundle b = new Bundle();
                b.putString("callbackFnString", jsCallbackFn + "('session_already_started')");
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
                return "session_already_started";

            }*/
            else if (responseCode == ResponseCodes.SESSION_DOES_NOT_EXIST)
            {
                if(jsCallbackFn == null)
                    return "no_such_session";

                Bundle b = new Bundle();
                b.putString("callbackFnString", jsCallbackFn + "('no_such_session')");
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
                return "no_such_session";

            }
            else if (responseCode == ResponseCodes.SESSION_EXPIRED)
            {

                if(jsCallbackFn == null)
                    return "session_expired";

                Bundle b = new Bundle();
                b.putString("callbackFnString", jsCallbackFn + "('session_expired')");
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));
                return "session_expired";
            }
            else
            {
                if(jsCallbackFn == null)
                    return responseJSON.getString("message");

                Bundle b = new Bundle();
                b.putString("callbackFnString", jsCallbackFn + "('" + responseJSON.getString("message") + "')");
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));

                return responseJSON.getString("message");
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();

            Bundle b = new Bundle();
            b.putString("callbackFnString", jsCallbackFn + "('Error: IOException')");
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));

            return "Error";
        }
        catch(JSONException e)
        {
            e.printStackTrace();

            Bundle b = new Bundle();
            b.putString("callbackFnString", jsCallbackFn + "('Error: JSONException')");
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.WEBVIEW_JS_CALLBACK, b));

            return "Error";
        }
    }

    public String getFacilityNameByFacilityCode(String facilityCode) throws IOException, JSONException {

        if(!NetworkHelper.isInternetAvailable(mContext))
        {
            return "NO_INTERNET";
        }

        MultipartBody multipartBody = new MultipartBody.Builder()
                .addFormDataPart("facilityId", facilityCode)
                .setType(MultipartBody.FORM)
                .build();

        Request request = new Request.Builder()
                .post(multipartBody)
                .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_FACILITY_INFO)
                .build();

        Response response = HttpManager.getInstance().getHttpClient().newCall(request).execute();

        int responseCode;
        JSONObject responseJSON = new JSONObject(response.body().string());
        responseCode = responseJSON.getInt("code");

        if (response.isSuccessful()) {
            if (responseCode == ResponseCodes.OK) {
                Log.v(TAG, "Received Facility Name: " + responseJSON.getJSONObject("data").getString("facilityName") + " from Facility Code " + facilityCode);
                return responseJSON.getJSONObject("data").getString("facilityName");
            } else if (responseCode == ResponseCodes.BAD_REQUEST) {
                throw new IOException("FacilityId Invalid");
            }
        }

        return "";
    }




    public EnrollmentInfo[] getPatientListByEnrollmentDate(String enrollmentDate, String facilityId) throws IOException, JSONException, ParseException {

        log("Fetching list of patients for FacilityID:" + facilityId + " and Enrollment Date: " + enrollmentDate);

        MultipartBody multipartBody = new MultipartBody.Builder()
                .addFormDataPart("facilityId", facilityId)
                .addFormDataPart("enrollmentDate", enrollmentDate)
                .setType(MultipartBody.FORM)
                .build();

        Request request = new Request.Builder()
                .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_PATIENT_LIST)
                .post(multipartBody)
                .build();

        Response response = HttpManager.getInstance().getHttpClient().newCall(request).execute();

        String responseBody = response.body().string();
        log("Response: " + responseBody);

        int responseCode;
        JSONObject responseJSON = new JSONObject(responseBody);
        responseCode = responseJSON.getInt("code");

        log("Response Code: " + responseCode);

        if (responseCode == ResponseCodes.OK) {

            Log.v(TAG, "Received patient list from server successfully for FacilityId " + facilityId + " and EnrollmentDate " + enrollmentDate);

            List<EnrollmentInfo> enrollmentInfoList = new ArrayList<>();

            JSONObject dataJSON = responseJSON.getJSONObject("data");
            Iterator<String> dataIterator = dataJSON.keys();

            log("Has Next? " + dataIterator.hasNext());


            // for each patient in list
            while (dataIterator.hasNext()) {

                String next = dataIterator.next();

                log("\nParsing for " + next);

                JSONObject patientDataJSON = dataJSON.getJSONObject(next);

                PatientActivation patientData = ObjectUtils.fromJson(patientDataJSON.toString(), PatientActivation.class);

                EnrollmentInfo enrollmentInfo = new EnrollmentInfo();

                enrollmentInfo.setEnrollmentDateIso8601(patientData.getPatient().getEnrollmentDate());
                //enrollmentInfo.setEnrollmentDateIso8601(patientDataJSON.getJSONObject("patient").getString("enrollmentDate"));

                enrollmentInfo.setFirstName(patientData.getPatient().getFirstName());
                //enrollmentInfo.setFirstName(patientDataJSON.getJSONObject("patient").getString("firstName"));

                enrollmentInfo.setLastName(patientData.getPatient().getLastName());
                //enrollmentInfo.setLastName(patientDataJSON.getJSONObject("patient").getString("lastName"));

                enrollmentInfo.setLastNameInitial(patientData.getPatient().getLastName().toUpperCase().substring(0, 1));
                //enrollmentInfo.setLastNameInitial(patientDataJSON.getJSONObject("patient").getString("lastName").toUpperCase().substring(0, 1));

                enrollmentInfo.setFacilityName(patientData.getFacility().getName());
                //enrollmentInfo.setFacilityName(patientDataJSON.getJSONObject("facility").getString("name"));


                // TODO convert this setter to accept long as we don't know how much larger session ids will get.
                enrollmentInfo.setSessionId(patientData.getSession().get(0).getSessionId());

                boolean allSessionsExpired = true;

                // for each session in enrollment
                for(int i = 0; i < patientData.getSession().size(); i++) {

                    SessionInfoForState si = new SessionInfoForState(
                        (patientData.getSession().get(i).getSessionId()),
                        (patientData.getSession().get(i).getIsExpired()),
                        (patientData.getSession().get(i).getDuration())
                    );

                    enrollmentInfo.addSession(si);

                    boolean sessExpired = patientData.getSession().get(i).getIsExpired();

                    log("session " + patientData.getSession().get(i).getSessionId() + " " + sessExpired);
                    allSessionsExpired = allSessionsExpired && sessExpired;

                }

                // if all sessions expired within this enrollment do not put in list
                if(allSessionsExpired) {
                    log("All sessions expired");
                    continue;
                }

                enrollmentInfoList.add(enrollmentInfo);
            }

            EnrollmentInfo[] infoArray = new EnrollmentInfo[enrollmentInfoList.size()];
            for (int i = 0; i < infoArray.length; i++){
                infoArray[i] = enrollmentInfoList.get(i);
            }
            enrollmentInfoList.clear();

            for (int i = 0; i < infoArray.length; i++){
                Log.v(TAG, "Patient Name: " + infoArray[i].getFirstName() + " " + infoArray[i].getLastName() + " " + infoArray[i].getLastName().toUpperCase().substring(0, 1)
                 + ", Facility Name: " + infoArray[i].getFacilityName());
            }

            return infoArray;

        }
        else
        {
            return null;
        }
    }

}
