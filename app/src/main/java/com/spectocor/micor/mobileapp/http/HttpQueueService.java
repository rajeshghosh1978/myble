package com.spectocor.micor.mobileapp.http;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Process;
import android.support.annotation.Nullable;

import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.patientreportedevents.FifoPatientReportedEventRequestHandler;

/**
 * Service for handling http requests that shall be enqueued (long running processes like file uploading and downloading).
 * It enqueues those events inside a single HandlerThread to avoid effecting the UI performance.
 *
 * Created by Rafay Ali on 1/20/2016.
 */
public class HttpQueueService extends Service {

    public static final String TAG = HttpQueueService.class.getSimpleName();

    private HttpRequestHandler                  httpRequestHandler = null;
    private HttpServiceHandlerThread            handlerThread = null;
    private HttpServiceHandlerThread            ecgBufferedRequestHandlerThread = null;
    private HttpServiceHandlerThread            pteBufferedRequestHandlerThread = null;
    private HttpBufferedRequestHandler          ecgBufferedRequestHandler = null;
    private HttpBufferedRequestHandler          pteBufferedRequestHandler = null;
    private IBinder                             binder = new HttpQueueServiceBinder();

    // modified by savio
    private HttpServiceHandlerThread                fifoEcgHttpBufferedHandlerThread = null;
    private FifoEcgHttpBufferedRequestHandler       fifoEcgHttpBufferedHandler = null;

    // modified by savio
    private HttpServiceHandlerThread                fifoPatientReportedEventsHandlerThread = null;
    private FifoPatientReportedEventRequestHandler  fifoPatientReportedEventsHandler = null;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "Service Started!");

        Log.v(TAG, "Creating HttpServiceHandlerThread");
        handlerThread = new HttpServiceHandlerThread("HttpServiceHandlerThread", Process.THREAD_PRIORITY_BACKGROUND);
        if(handlerThread!=null){
            handlerThread.start();
        }
        Log.v(TAG, "Started HttpServiceHandlerThread.");
        httpRequestHandler = new HttpRequestHandler(handlerThread.getLooper());
        Log.v(TAG, "Initiated HttpRequestHandler with thread looper.");
    }

    public void initAllHandlersAndThreads() {

        binder = new HttpQueueServiceBinder();

        if(handlerThread != null) {
            handlerThread = new HttpServiceHandlerThread("HttpServiceHandlerThread", Process.THREAD_PRIORITY_BACKGROUND);
        }

        try {
            handlerThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.v(TAG, "Started HttpServiceHandlerThread.");


        httpRequestHandler = new HttpRequestHandler(handlerThread.getLooper());
        Log.v(TAG, "Initiated HttpRequestHandler with thread looper.");


        ecgBufferedRequestHandlerThread = null;
        ecgBufferedRequestHandler = null;

        binder = null;

        fifoEcgHttpBufferedHandlerThread = null;
        fifoEcgHttpBufferedHandler = null;

        fifoPatientReportedEventsHandlerThread = null;
        fifoPatientReportedEventsHandler = null;


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "Service start mode is START_STICKY.");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "Service is now bind with main Application context.");
        return binder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "Destroying Service.");
        handlerThread.quit();
        ecgBufferedRequestHandlerThread.quit();
        pteBufferedRequestHandlerThread.quit();

        stopFifoEcgBufferedHandler();
        stopFifoPatientReportedEventsHandler();
    }

    public void notifyHandler(HttpRequestBase httpRequest, boolean priority){
        if (!priority)
            httpRequestHandler.sendMessage(
                    httpRequestHandler.obtainMessage(httpRequest.getRequestType(), httpRequest)
            );
        else {
            httpRequestHandler.sendMessageAtFrontOfQueue(
                    httpRequestHandler.obtainMessage(httpRequest.getRequestType(), httpRequest)
            );
        }
    }

    public void notifyEcgBufferedHandler(){
        if (ecgBufferedRequestHandlerThread == null){
            ecgBufferedRequestHandlerThread = new HttpServiceHandlerThread("EcgBufferedHandlerThread", Process.THREAD_PRIORITY_BACKGROUND);
        }
        if (!ecgBufferedRequestHandlerThread.isAlive()){
            ecgBufferedRequestHandlerThread.start();
            if (ecgBufferedRequestHandler == null){
                ecgBufferedRequestHandler = new HttpBufferedRequestHandler(ecgBufferedRequestHandlerThread.getLooper());
            }
        }
        if (!ecgBufferedRequestHandler.isMsgBeingHandled()){
            ecgBufferedRequestHandler.sendMessage(ecgBufferedRequestHandler.obtainMessage());
        }
    }

    public void notifyPteBufferedHandler(){
        if (pteBufferedRequestHandlerThread == null){
            pteBufferedRequestHandlerThread = new HttpServiceHandlerThread("PteBufferedHandlerThread", Process.THREAD_PRIORITY_BACKGROUND);
        }
        if (!pteBufferedRequestHandlerThread.isAlive()){
            pteBufferedRequestHandlerThread.start();
            if (pteBufferedRequestHandler == null){
                pteBufferedRequestHandler = new HttpBufferedRequestHandler(pteBufferedRequestHandlerThread.getLooper());
            }
        }
        if (!pteBufferedRequestHandler.isMsgBeingHandled()){
            pteBufferedRequestHandler.sendMessage(pteBufferedRequestHandler.obtainMessage());
        }
    }

    public void stopEcgBufferedHandler(){
        if(ecgBufferedRequestHandlerThread != null)
            ecgBufferedRequestHandlerThread.quit();
    }

    public void stopPteBufferedHandler(){
        if (pteBufferedRequestHandlerThread != null)
            pteBufferedRequestHandlerThread.quit();
    }


    public class HttpQueueServiceBinder extends Binder{

        HttpQueueService getService(){
            return HttpQueueService.this;
        }
    }




    ///////////////////// NEW METHODS ADDED HERE: Savio M.


    /**
     * Added For Pure Fifo.
     */
    public void notifyFifoEcgBufferedHandler() {

        if(fifoEcgHttpBufferedHandlerThread != null)
            Log.v(TAG, "fifoEcgHttpBufferedHandlerThread State: " + fifoEcgHttpBufferedHandlerThread.getState());
        else
            Log.v(TAG, "fifoEcgHttpBufferedHandlerThread is NULL");


        if (fifoEcgHttpBufferedHandlerThread == null || fifoEcgHttpBufferedHandlerThread.getState() == Thread.State.TERMINATED
                || !fifoEcgHttpBufferedHandlerThread.isAlive()) {
            fifoEcgHttpBufferedHandlerThread = new HttpServiceHandlerThread("FifoEcgBufferedHandlerThread", Process.THREAD_PRIORITY_BACKGROUND);
            fifoEcgHttpBufferedHandlerThread.start();
        }


        if(fifoEcgHttpBufferedHandler == null)
            fifoEcgHttpBufferedHandler = new FifoEcgHttpBufferedRequestHandler(fifoEcgHttpBufferedHandlerThread.getLooper());


        fifoEcgHttpBufferedHandler.sendMessage(fifoEcgHttpBufferedHandler.obtainMessage());

    }


    /**
     * Added For Pure Fifo. PTE = Patient Triggered Events.
     */
    public void notifyFifoPteBufferedHandler() {

        if(fifoPatientReportedEventsHandlerThread != null)
            Log.v(TAG, "fifoPatientReportedEventsHandlerThread State: " + fifoPatientReportedEventsHandlerThread.getState());
        else
            Log.v(TAG, "fifoPatientReportedEventsHandlerThread is NULL");


        if (fifoPatientReportedEventsHandlerThread == null || fifoPatientReportedEventsHandlerThread.getState() == Thread.State.TERMINATED || !fifoPatientReportedEventsHandlerThread.isAlive()) {
            fifoPatientReportedEventsHandlerThread = new HttpServiceHandlerThread("FifoPatientReportedEventsHandlerThread", Process.THREAD_PRIORITY_BACKGROUND);
            fifoPatientReportedEventsHandlerThread.start();
        }


        if(fifoPatientReportedEventsHandler == null)
            fifoPatientReportedEventsHandler = new FifoPatientReportedEventRequestHandler(fifoPatientReportedEventsHandlerThread.getLooper());


        fifoPatientReportedEventsHandler.sendMessage(fifoPatientReportedEventsHandler.obtainMessage());

    }




    // fifo handlers
    public void stopFifoEcgBufferedHandler(){

        if(fifoEcgHttpBufferedHandlerThread != null) {
            fifoEcgHttpBufferedHandlerThread.quit();
            fifoEcgHttpBufferedHandlerThread = null;
            fifoEcgHttpBufferedHandler = null;
        }
    }

    public void stopFifoPatientReportedEventsHandler(){

        if(fifoPatientReportedEventsHandlerThread != null) {
            fifoPatientReportedEventsHandlerThread.quit();
            fifoPatientReportedEventsHandlerThread = null;
            fifoPatientReportedEventsHandler = null;
        }
    }
}
