package com.spectocor.micor.mobileapp.amain.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogEvents;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by Savio Monteiro on 2/1/2016.
 */
public class EcgBatteryLevelReceiver extends BroadcastReceiver {

    public static final String ECG_BATT_BROADCAST_ACTION = "com.spectocor.micor.mobileapp.ECG_BATT_BROADCAST_ACTION";
    public static final String ECG_BATTERY_LEVEL_MESG = "ECG_BATTERY_LEVEL";


    private static void log(String str) {
        MLog.log(EcgBatteryLevelReceiver.class.getSimpleName() + ": NotificationReceiver: " + str);
    }

    /**
     * Create the broadcast intent to consume
     *
     * @return
     */
    public static Intent getNotificationIntent(int ecgBatteryLevel, Bundle bundle) {


        Intent intent = new Intent(ECG_BATT_BROADCAST_ACTION);
        intent.putExtra(ECG_BATTERY_LEVEL_MESG, ecgBatteryLevel);

        return intent;
    }

    private static final int BATTERY_CURRENTLY_GREATER_THAN_20 = 25;
    private static final int BATTERY_CURRENTLY_AT_20 = 20;
    private static final int BATTERY_CURRENTLY_LESS_THAN_20 = 15;
    private static final int BATTERY_CURRENTLY_AT_10 = 10;
    private static final int BATTERY_CURRENTLY_LESS_THAN_10 = 7;
    private static final int BATTERY_CURRENTLY_AT_5 = 5;
    private static final int BATTERY_CURRENTLY_LESS_THAN_5 = 3;

    private static int BATTERY_CURRENTLY_AT = BATTERY_CURRENTLY_GREATER_THAN_20;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(ECG_BATT_BROADCAST_ACTION)) {
            int batteryLevel = intent.getIntExtra(ECG_BATTERY_LEVEL_MESG, -1);

            log("Received battery level: " + batteryLevel);

            if(batteryLevel > 20) {
                BATTERY_CURRENTLY_AT = BATTERY_CURRENTLY_GREATER_THAN_20;
            }
            else if(batteryLevel == 20) {
                BATTERY_CURRENTLY_AT = BATTERY_CURRENTLY_AT_20;
            }
            else if(batteryLevel < 20 && batteryLevel > 10) {
                BATTERY_CURRENTLY_AT = BATTERY_CURRENTLY_LESS_THAN_20;
            }
            else if(batteryLevel == 10) {
                BATTERY_CURRENTLY_AT = BATTERY_CURRENTLY_AT_10;
            }
            else if(batteryLevel < 10 && batteryLevel > 5) {
                BATTERY_CURRENTLY_AT = BATTERY_CURRENTLY_LESS_THAN_10;
            }
            else if(batteryLevel == 5) {
                BATTERY_CURRENTLY_AT = BATTERY_CURRENTLY_AT_5;
            }
            else if(batteryLevel < 5 && batteryLevel >= 0) {
                BATTERY_CURRENTLY_AT = BATTERY_CURRENTLY_LESS_THAN_5;
            }

            DeviceLogEvents.getInstance().putEcgDeviceBatteryLevel(batteryLevel);

            checkAlertWorthiness(context);
        }
    }

    private static boolean alertPlayedFor20to10 = false;
    private static boolean alertPlayedFor10to5 = false;
    private static boolean alertPlayedFor5to0 = false;

    private void checkAlertWorthiness(Context context) {


        GregorianCalendar c = (GregorianCalendar) GregorianCalendar.getInstance(TimeZone.getDefault());
        int hourOfDay = c.get(Calendar.HOUR_OF_DAY);


        if(BATTERY_CURRENTLY_AT == BATTERY_CURRENTLY_GREATER_THAN_20)
        {
            alertPlayedFor20to10 = false;
            alertPlayedFor10to5 = false;
            alertPlayedFor5to0 = false;
        }

        if(BATTERY_CURRENTLY_AT == BATTERY_CURRENTLY_AT_20)
        {
            AlertPlayer.playVibrateAlert(context);

            if(hourOfDay >= 8 && hourOfDay < 20) {
                AlertPlayer.playAudioAlert(context);
                AlertPlayer.turnScreenOn(context);
                AlertPlayer.displayActivity(context);
            }

            AMainApplication.showLongToast("ECG BATTERY = 20%", context);

            alertPlayedFor20to10 = false;
        }
        else if(BATTERY_CURRENTLY_AT == BATTERY_CURRENTLY_AT_10)
        {
            if(hourOfDay >= 8 && hourOfDay < 20) {
                AlertPlayer.turnScreenOn(context);
                AlertPlayer.displayActivity(context);
            }

            AlertPlayer.playVibrateAlert(context);
            //AlertPlayer.playAudioAlert(context);
            AMainApplication.showLongToast("ECG BATTERY = 10%", context);

            alertPlayedFor10to5 = false;
        }
        else if(BATTERY_CURRENTLY_AT == BATTERY_CURRENTLY_AT_5)
        {
            if(hourOfDay >= 8 && hourOfDay < 20) {
                AlertPlayer.turnScreenOn(context);
                AlertPlayer.displayActivity(context);
            }

            AlertPlayer.playVibrateAlert(context);
            //AlertPlayer.playAudioAlert(context);
            AMainApplication.showLongToast("\n\nCRITICAL MESSAGE\n\nECG BATTERY is 5%\n\n", context);

            alertPlayedFor5to0 = false;
        }

        if(BATTERY_CURRENTLY_AT == BATTERY_CURRENTLY_LESS_THAN_20 && !alertPlayedFor20to10)
        {
            AlertPlayer.playVibrateAlert(context);
            //AlertPlayer.playAudioAlert(context);

            alertPlayedFor20to10 = true;
            alertPlayedFor10to5 = false;
        }

        if(BATTERY_CURRENTLY_AT == BATTERY_CURRENTLY_LESS_THAN_10 && !alertPlayedFor10to5)
        {
            AlertPlayer.playVibrateAlert(context);
            //AlertPlayer.playAudioAlert(context);

            alertPlayedFor10to5 = true;
            alertPlayedFor5to0 = false;
        }


        if(BATTERY_CURRENTLY_AT == BATTERY_CURRENTLY_LESS_THAN_5 && !alertPlayedFor5to0)
        {
            AlertPlayer.playVibrateAlert(context);
            //AlertPlayer.playAudioAlert(context);

            alertPlayedFor5to0 = true;
        }
    }
}
