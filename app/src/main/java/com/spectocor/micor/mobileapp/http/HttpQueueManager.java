package com.spectocor.micor.mobileapp.http;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.common.Log;

/**
 * Manager class for enqueuing network request calls to HttpQueueService
 *
 * Created by Rafay Ali on 1/20/2016.
 */
public class HttpQueueManager {

    public static final String TAG = HttpQueueManager.class.getSimpleName();

    private static HttpQueueManager queueManager = null;
    private ServiceConnection serviceConnection = null;
    private HttpQueueService httpQueueService = null;

    private HttpQueueManager(){

    }

    public static HttpQueueManager getInstance(){
        if (queueManager == null){
            queueManager = new HttpQueueManager();
        }

        return queueManager;
    }

    public void initServiceConnection(){

        if (serviceConnection == null){
            serviceConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    Log.v(TAG, "Connected to service " + name.getClassName());
                    httpQueueService = ((HttpQueueService.HttpQueueServiceBinder)service).getService();
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                    Log.w(TAG, "Connection to " + name.getClass().getSimpleName() + " has been closed");
                    httpQueueService = null;
                }
            };
        }

        AMainApplication.getAppContext().bindService(new Intent(AMainApplication.getAppContext(), HttpQueueService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);
    }

    public void initAllHandlers() {
        httpQueueService.initAllHandlersAndThreads();
    }


    public void enqueueRequest(HttpRequestBase request, boolean priority){
        if (httpQueueService != null)
            httpQueueService.notifyHandler(request, priority);
    }

    public void notifyBufferedHandler(){
        if (httpQueueService != null)
            httpQueueService.notifyEcgBufferedHandler();
    }

    public void notifyPteBufferedHandler(){
        if (httpQueueService != null)
            httpQueueService.notifyPteBufferedHandler();
    }

    public void notifyFifoEcgBufferedHandler(){
        if (httpQueueService != null)
            httpQueueService.notifyFifoEcgBufferedHandler();
    }

    public void notifyFifoPteBufferedHandler(){
        if (httpQueueService != null)
            httpQueueService.notifyFifoPteBufferedHandler();
    }

    public void stopBufferedHandler(){
        if (httpQueueService != null)
            httpQueueService.stopEcgBufferedHandler();
    }

    public void stopPteBufferedHandler(){
        if (httpQueueService != null)
            httpQueueService.stopPteBufferedHandler();
    }

    public void stopFifoEcgBufferedHandler(){
        if (httpQueueService != null)
            httpQueueService.stopFifoEcgBufferedHandler();
    }

    public void stopFifoPteBufferedHandler(){
        if (httpQueueService != null)
            httpQueueService.stopFifoPatientReportedEventsHandler();
    }

}
