package com.spectocor.micor.mobileapp.devicelogs;

/**
 * Logs Ecg Transmitter data
 */
@SuppressWarnings("WeakerAccess")
public class EcgDeviceLogService {

    protected final DeviceLogLogger service;

    public EcgDeviceLogService(DeviceLogLogger deviceLogLogger) {
        service = deviceLogLogger;
    }


    /**
     * Logs ECG Transmitter battery level
     *
     * @param batteryPercent battery percentage
     */
    public DeviceLog logEcgDeviceBatteryLevel(byte batteryPercent) {
        if (!(batteryPercent >= 0 && batteryPercent <= 100))
            throw new IllegalArgumentException("batteryPercent should be between 0 and 100");
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.EcgDeviceBatteryLevel, (double) batteryPercent);
        service.insertLog(obj);
        return obj;
    }

    /**
     * Logs ECG Transmitter battery status
     *
     * @param connected is connected (false = disconnected)
     */
    public DeviceLog logEcgDeviceLeadStatusChanged(boolean connected) {
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.EcgDeviceLeadStatusChanged, connected);
        service.insertLog(obj);
        return obj;
    }


    /**
     * Logs ECG Transmitter Connection status
     *
     * @param connected is connected (false = disconnected)
     */
    public DeviceLog logEcgDeviceConnectionStatus(boolean connected) {
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.EcgDeviceConnectionStatus, connected);
        service.insertLog(obj);
        return obj;
    }



}
