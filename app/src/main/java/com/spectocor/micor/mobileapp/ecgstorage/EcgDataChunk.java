package com.spectocor.micor.mobileapp.ecgstorage;


public class EcgDataChunk {

    public int monitoringSessionId;
    public long chunkStartDateUnixMs;
    public long insertTimestamp;
    public long timezoneOffsetMillis;   // ... timezone offset from UTC in milliseconds
    public String timezoneId;           // ... timezone ID (eg: America/Chicago)
    public int numberOfSamples = -1;
    public long chunkTransferTimestamp = -1;

    // transient  keyword to avoid Gson serialization
    //http://stackoverflow.com/questions/4802887/gson-how-to-exclude-specific-fields-from-serialization-without-annotations
    public transient byte[] chunkDataChannelCompressed;


    public final int getMonitoringSessionId() {
        return monitoringSessionId;
    }

    public final void setMonitoringSessionId(int sessionId) {

        if(sessionId < 0) {
            throw new IllegalArgumentException("Session ID cannot be negative");
        }

        monitoringSessionId = sessionId;
    }

    public final long getChunkStartDateUnixMs() {
        return chunkStartDateUnixMs;
    }

    public final void setChunkStartDateUnixMs(long timestampMs) {

        if(timestampMs < 0) {
            throw new IllegalArgumentException("timestamp cannot be negative.");
        }

        chunkStartDateUnixMs = timestampMs;
    }

    public final byte[] getChunkDataChannelCompressed() {
        return chunkDataChannelCompressed;
    }

    public final void setChunkDataChannelCompressed(byte[] value) {

        if(value == null) {
            throw new NullPointerException("Compress Ecg Data cannot be null");
        }

        this.chunkDataChannelCompressed = value;

    }



    public final void setNumberOfSamples(int numberOfSamples) {
        this.numberOfSamples = numberOfSamples;
    }

    public int getNumberOfSamples() {
        return this.numberOfSamples;
    }



    public final void setChunkTransferTimestamp(long chunkTransferTimestamp) {
        this.chunkTransferTimestamp = chunkTransferTimestamp;
    }

    public final long getChunkTransferTimestamp() {
        return this.chunkTransferTimestamp;
    }

}