package com.spectocor.micor.mobileapp.common.queuesystem;

import com.spectocor.micor.mobileapp.common.ApiResult;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.common.RepositoryBase;
import com.spectocor.micor.mobileapp.common.ServiceFactory;
import com.spectocor.micor.mobileapp.common.exceptions.UserException;
import com.spectocor.micor.mobileapp.common.exceptions.WebApiCallException;

import java.util.ArrayList;

/**
 * Base class for sending queue data to http servers
 */
public class QueueHttpSenderBase<T> {

    private static final String TAG = "QueueHttpSenderBase";

    /**
     * repository for accessing actual objects
     */
    protected final RepositoryBase<T> repository;
    /**
     * repository of item ids that needs to be sent from the database
     */
    protected final SendingQueueRepositoryBase sendingQueueRepository;
    /**
     * https handler to call the server
     */
    protected final HttpsHandler httpsHandler;
    /**
     * name of the api in the server. For example, activation/activate
     */
    protected final String apiName;

    /**
     * constructor to create a queue repository sender
     *
     * @param repository             repository for accessing actual objects
     * @param sendingQueueRepository repository of item ids that needs to be sent from the database
     * @param httpsHandler           https handler to call the server
     * @param apiName                name of the api in the server. For example, activation/activate
     */
    public QueueHttpSenderBase(RepositoryBase<T> repository,
                               SendingQueueRepositoryBase sendingQueueRepository,
                               HttpsHandler httpsHandler,
                               String apiName) {
        this.repository = repository;
        this.sendingQueueRepository = sendingQueueRepository;
        this.httpsHandler = httpsHandler;
        this.apiName = apiName;
    }


    /**
     * Sends items in the queue from the repository
     *
     * @param numberOfItems maximum number of items to be sent from queue in this call
     */
    public void sendQueue(int numberOfItems) {

       /* ArrayList<QueueItem> list = sendingQueueRepository.getAll(numberOfItems);
        for (int i = 0; i < list.size(); i++) {
            QueueItem queueItem = list.get(i);
            T obj = repository.getById(queueItem.getRowId());
            if (obj != null) {
                try {

                    if (sendObject(obj))
                        deleteFromQueue(queueItem);

                } catch (Exception ex) {
                    Log.e(TAG, "sending item from Queue failed.", ex);
                    ServiceFactory.getInstance().getApplicationLogService().logApplicationException(ex);
                }
            } else {
                deleteFromQueue(queueItem);
            }
        }*/

    }

    private void deleteFromQueue(QueueItem queueItem) {
        try {
            sendingQueueRepository.delete(queueItem.getRowId()); // delete queue item if it is not in repository
        } catch (Exception ex) {
            Log.e(TAG, "deleteing item from Queue failed.");
            ServiceFactory.getInstance().getApplicationLogService().logApplicationException(ex);
        }
    }


    /**
     * Sends the object to the server
     *
     * @param obj the object
     * @return if send was successful or not
     * @throws Exception
     */
    public boolean sendObject(T obj) throws Exception {
        try {
            ApiResult result = httpsHandler.postJsonApi(this.apiName, obj);
            return result.getsuccess();
        } catch (UserException ex1) {
            return false;
        } catch (WebApiCallException ex2) {
            return false;
        }

    }


}
