package com.spectocor.micor.mobileapp.amain.alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.http.HttpQueueManager;
import com.spectocor.micor.mobileapp.threading.DefaultAsyncListener;
import com.spectocor.micor.mobileapp.threading.DefaultAsyncTask;

/**
 * This class handles an Alarm Receiver that triggers notifications to
 * ECG Transmission Handlers and PTE Transmission Handlers
 *
 * Created by Savio Monteiro on 3/11/2016.
 */
public class FifoEcgAndPteHandlerAlarmReceiver extends BroadcastReceiver {

    public static long ALARM_INTERVAL_MS = 10000;
    public static long nextAlarmAt = System.currentTimeMillis();

    private DefaultAsyncTask fifoEcgNotifierTask;
    private DefaultAsyncTask fifoPteNotifierTask;

    private void log(String str) {
        Log.d("FifoAlarmReceiver", str);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        log("Received Alarm Notification");


        boolean sessionActive = SessionStateAccess.getCurrentSessionState(context).sessionActive;
        boolean deviceDeactivated = SessionStateAccess.getCurrentSessionState(context).deviceDeactivated;

        if(!sessionActive) {

            log("Session not active. Exiting Alarm Notification");

            AMainApplication.FIFO_ALARM_RUNNING = false;

            return;
        }

        if(deviceDeactivated) {
            log("Session Deactivated ... Exiting Alarm Notification");

            AMainApplication.FIFO_ALARM_RUNNING = false;

            return;
        }

        AMainApplication.FIFO_ALARM_RUNNING = true;


        scheduleNextAlarmCall(context, intent);

        // notify ECG Handler
        getFifoEcgNotifierTask().executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);

        // notify PTE Handler
        getFifoPteNotifierTask().executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);

    }


    //TODO : Must Put null check before using, it causes crash.
    // Schedule the next repeating alarm call ...
    private void scheduleNextAlarmCall(Context context, Intent broadcastIntent) {


        Bundle bundle = broadcastIntent.getExtras();
        long intervalMs = 0;
        if(bundle!=null)
        {
            if(bundle.containsKey("CHUNK_SIZE_TO_TRANSFER_IN_SECONDS"))
                 intervalMs = bundle.getLong("CHUNK_SIZE_TO_TRANSFER_IN_SECONDS", 5 * 60 * 1000);
        }


        if(intervalMs > 0) {
            ALARM_INTERVAL_MS = intervalMs;
        }
        else {
            ALARM_INTERVAL_MS = 5 * 60 * 1000;
        }


        ALARM_INTERVAL_MS = 20 * 1000;

        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmReceiverIntent = new Intent(context, FifoEcgAndPteHandlerAlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, alarmReceiverIntent, 0);

        nextAlarmAt += ALARM_INTERVAL_MS;
        alarmMgr.setExact(AlarmManager.RTC_WAKEUP, nextAlarmAt, alarmIntent);
    }


    // gets asynctask instance for ecg asynctask
    private DefaultAsyncTask getFifoEcgNotifierTask() {

        if(fifoEcgNotifierTask == null) {
            fifoEcgNotifierTask = new DefaultAsyncTask(new DefaultAsyncListener() {
                @Override
                public Object doInBackgroundThread(DefaultAsyncTask asyncTask) {

                    log("Notifying Fifo ECG buffered handler");

                    HttpQueueManager.getInstance().notifyFifoEcgBufferedHandler();

                    return null;
                }

                @Override
                public void onCompleted(Object object) {}

                @Override
                public void onException(Exception exception) {}
            });
        }

        return fifoEcgNotifierTask;
    }


    // gets asynctask instance for pte asynctask
    private DefaultAsyncTask getFifoPteNotifierTask() {

        if(fifoPteNotifierTask == null) {

            fifoPteNotifierTask = new DefaultAsyncTask(new DefaultAsyncListener() {

                @Override
                public Object doInBackgroundThread(DefaultAsyncTask asyncTask) {

                    log("Notifying Fifo PTE buffered handler");

                    HttpQueueManager.getInstance().notifyFifoPteBufferedHandler();

                    return null;
                }

                @Override
                public void onCompleted(Object object) {}

                @Override
                public void onException(Exception exception) {}
            });
        }

        return fifoPteNotifierTask;
    }

}

