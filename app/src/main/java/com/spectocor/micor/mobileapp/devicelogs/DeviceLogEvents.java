package com.spectocor.micor.mobileapp.devicelogs;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Debug;
import android.os.Environment;
import android.telephony.TelephonyManager;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.http.common.NetworkHelper;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Admin on 3/8/2016.
 */

/*
This class will cater all the Events that we need to capture and send to the sever.


 */
public class DeviceLogEvents {


    private static final String TAG = DeviceLogEvents.class.getSimpleName();

    private Context context;
    private static DeviceLogEvents instance = null;

    public static DeviceLogEvents getInstance() {
        if (instance == null) {
            instance = new DeviceLogEvents(AMainApplication.getAppContext());
        }

        return instance;
    }


    private DeviceLogEvents(Context context) {
        this.context = context;

        if(!AMainApplication.LOGS_ENABLED)
        {
            log("Logs disabled. Returning!");
            return;
        }

        putLogAndroidSoftwareVersion();
        putLogSimSerialNumber();
        putLogBuildInfo();
        putLogMemoryFreeSpace();
        putLogMemoryTotalSpace();
        putLogInternalStorageFreeSpace();
        putLogInternalStorageTotalSpace();
        putLogSDCardTotalSpace();
        putLogSdCardFreeSpace();
        putNetworkType(NetworkHelper.getNetworkOperatorName());
        putLocationLat();
        putLocationLng();
    }


    public void putLogApplicationStarted() {
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Application has started", EnumDeviceLogInfoType.ApplicationStarted);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "Application has started");
    }

    public void putLogApplicationEnded(String causeOfEnd) {

        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Application ended due to " + causeOfEnd, EnumDeviceLogInfoType.ApplicationEnded);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "Application ended due to " + causeOfEnd);
    }

    public void putLogApplicationCrashed(String causeOfCrash) {
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Application crashed due to " + causeOfCrash, EnumDeviceLogInfoType.ApplicationCrashed);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "Application crashed due to " + causeOfCrash);
    }

    public void putNetworkState(String status) {
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Network State is " + status, EnumDeviceLogInfoType.NetworkState);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "Network State is " + status);
    }

    public void putNetworkIsAvailable(String status) {
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Network is " + status, EnumDeviceLogInfoType.NetworkIsAvailable);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "Network is " + status);
    }


    public void putLogSimSerialNumber() {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            String simSerialNumber = telephonyManager.getSimSerialNumber();
            CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
            if (simSerialNumber != null && !simSerialNumber.isEmpty()) {
                DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Sim Serial Number :" + simSerialNumber, EnumDeviceLogInfoType.SimSerialNumber);
                DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
                Log.v(TAG, "Sim Serial Number :" + simSerialNumber);
            }
            else {
                DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "No sim found on device", EnumDeviceLogInfoType.SimSerialNumber);
                DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
                Log.v(TAG, "No sim found on device");
            }
        }

    }

    public void putLogBuildInfo() {

        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();

        String myVersionName = "";

        try {
            myVersionName = packageManager.getPackageInfo(packageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (myVersionName != null && !myVersionName.isEmpty()) {
            CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
            if (myVersionName != null && !myVersionName.isEmpty()) {
                DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Build Information :" + myVersionName, EnumDeviceLogInfoType.BuildInfo);
                DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
                Log.v(TAG, "Build Information :" + myVersionName);
            }
        }
    }


    public void putLogMemoryFreeSpace() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);


        Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
        Debug.getMemoryInfo(memoryInfo);

        long availableMegs = mi.availMem / 1048576L;

        String memMessage = String.format(
                "Memory: Pss=%.2f MB, Private=%.2f MB, Shared=%.2f MB, Free Memory Space=%s MB",
                memoryInfo.getTotalPss() / 1024.0,
                memoryInfo.getTotalPrivateDirty() / 1024.0,
                memoryInfo.getTotalSharedDirty() / 1024.0,
                String.valueOf(availableMegs));


        if (memMessage != null && !memMessage.isEmpty()) {
            CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
            DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Memory Info :" + memMessage, EnumDeviceLogInfoType.MemoryFreeSpace);
            DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
            Log.v(TAG, "Memory info :" + memMessage);
        }

    }

    public void putLogMemoryTotalSpace() {

        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);

        long availablemeg = mi.totalMem / 1048576L;
        int scale = 5;
        BigDecimal num1 = new BigDecimal(availablemeg);
        BigDecimal num2 = new BigDecimal(1024);
        String val  =num1.divide(num2, scale, RoundingMode.HALF_UP).toString();
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Memory Total Space :" + val + "GB", EnumDeviceLogInfoType.MemoryTotalSpace);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "Memory Total Space :" + val + " GB");

    }


    public static String getTotalMemory() {

        RandomAccessFile reader = null;
        String load = null;
        try {
            reader = new RandomAccessFile("/proc/meminfo", "r");
            load = reader.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            // Streams.close(reader);
        }
        return load;
    }


    public String getDeviceInfo() {
        String s = "Device-infos:";
        s += "\n OS Version: " + System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")";
        s += "\n OS API Level: " + android.os.Build.VERSION.SDK_INT;
        s += "\n Device: " + android.os.Build.DEVICE;
        s += "\n Display: " + android.os.Build.DISPLAY;
        s += "\n Has External Storage: " + isExternalStorageAvailable();
        s += "\n Model (and Product): " + android.os.Build.MODEL + " (" + android.os.Build.PRODUCT + ")";
        return s;

    }

    public static boolean isExternalStorageAvailable() {
        return Environment.getExternalStorageState().contentEquals(
                Environment.MEDIA_MOUNTED);
    }

    public void putLogAndroidSoftwareVersion() {
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, getDeviceInfo(), EnumDeviceLogInfoType.AndroidSoftwareVersion);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, getDeviceInfo());
    }


    public void putIsWifiEnabled(boolean wifiEnable) {
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Wifi Enabled: " + wifiEnable, EnumDeviceLogInfoType.WifiEnabled);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.d(TAG, "Wifi Enabled: " + wifiEnable);
    }

    public void putLogInternalStorageFreeSpace() {

        DiskSpaceCalculator spaceCalculator = new DiskSpaceCalculator();
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Internal Storage Free Space :" + convertBytesCount(spaceCalculator.getInternalFreeBytes(), true) , EnumDeviceLogInfoType.InternalStorageFreeSpace);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "Internal Storage Free Space :" + convertBytesCount(spaceCalculator.getInternalFreeBytes(), true));

    }

    public void putLogInternalStorageTotalSpace() {

        DiskSpaceCalculator spaceCalculator = new DiskSpaceCalculator();
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Internal Storage Total Space :" + convertBytesCount(spaceCalculator.getInternalTotalBytes(), true) , EnumDeviceLogInfoType.InternalStorageTotalSpace);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.d(TAG, "Internal Storage Total Space :" + convertBytesCount(spaceCalculator.getInternalTotalBytes(), true));

    }


    public void putLogSDCardTotalSpace() {

        DiskSpaceCalculator spaceCalculator = new DiskSpaceCalculator();
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());

        boolean directories = isExternalStorageAvailable();
        DeviceLogEntity event =null;
        if(directories) {
        event = new DeviceLogEntity(sessionState.sessionId, "SD Card Total Space :" + convertBytesCount(spaceCalculator.getSdCardTotalBytes(), true), EnumDeviceLogInfoType.SdCardTotalSpace);
            Log.v(TAG, "SD Card Total Space :" + convertBytesCount(spaceCalculator.getSdCardTotalBytes(), true));
        }
        else
        {
            event = new DeviceLogEntity(sessionState.sessionId, "SD Card Total Space :" + "SD Card Not Available", EnumDeviceLogInfoType.SdCardTotalSpace);
            Log.v(TAG, "SD Card Total Space :" + "SD Card Not Available");
        }

        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);


    }

    public void putLogSdCardFreeSpace() {
        DiskSpaceCalculator spaceCalculator = new DiskSpaceCalculator();
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());

        //File[] directories = context.getExternalFilesDirs(null);
        boolean directories = isExternalStorageAvailable();
        DeviceLogEntity event =null;
        if(directories) {
             event = new DeviceLogEntity(sessionState.sessionId, "SD Card Free Space :" + convertBytesCount(spaceCalculator.getSdCardFreeBytes(), true), EnumDeviceLogInfoType.SdCardFreeSpace);
            Log.v(TAG, "SD Card Free Space :" +  convertBytesCount(spaceCalculator.getSdCardFreeBytes(), true));
        }
        else
        {
            event = new DeviceLogEntity(sessionState.sessionId, "SD Card Not Available", EnumDeviceLogInfoType.SdCardFreeSpace);
            Log.v(TAG, "SD Card Free Space :" +  "SD Card Not Available");
        }
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);

    }


    public void putLogBatteryLow() {
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Battery Low", EnumDeviceLogInfoType.MobileDeviceBatteryLow);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "Battery Low");
    }


    public void putLogMobileDevicePowerPlugConnected() {

        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Mobile Device Power Plug has been Connected", EnumDeviceLogInfoType.MobileDevicePowerPlugConnected);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "Mobile Device Power Plug has been Connected");

    }


    public void putLogMobileDeviceBatteryLevel(String batteryPercent) {


        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Device Battery Level is " + batteryPercent, EnumDeviceLogInfoType.MobileDeviceBatteryLevel);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "Device Battery Level is " + batteryPercent);

    }

    public void putLogMobileDevicePowerPlugDisConnected() {
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Device Power Plug has been disconnected", EnumDeviceLogInfoType.MobileDevicePowerPlugConnected);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "Device Power Plug has been disconnected");
    }


    public void putLogMobileDeviceBluetoothStatus(boolean isEnabled) {
        DeviceLogEntity event;
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        if (isEnabled) {
            event = new DeviceLogEntity(sessionState.sessionId, "Device Bluetooth has been enabled", EnumDeviceLogInfoType.MobileDeviceBluetoothStatus);
            Log.v(TAG, "Device Bluetooth has been enabled");
        }
        else {
            event = new DeviceLogEntity(sessionState.sessionId, "Device Bluetooth has been disabled", EnumDeviceLogInfoType.MobileDeviceBluetoothStatus);
            Log.v(TAG, "Device Bluetooth has been disabled");
        }
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
    }

    public void putNetworkType(String networkType){
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        Log.v(TAG, "Network Operator name: " + networkType);
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Network Type (operator name): " + networkType, EnumDeviceLogInfoType.NetworkType);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
    }

    public static void log(String str) {
        MLog.log("DeviceLogEvents: " + str);
    }

    public void putLocationLat(){
        String latitude = NetworkHelper.getLatitude();
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Latitude " + latitude, EnumDeviceLogInfoType.LocationLat);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
    }

    public void putLocationLng(){
        String longitude = NetworkHelper.getLongitude();
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "Longitude " + longitude, EnumDeviceLogInfoType.LocationLng);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
    }

    public void putEcgDeviceBatteryLevel(int batteryCurrentlyAt) {
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event = new DeviceLogEntity(sessionState.sessionId, "ECG device battery Level:  " + batteryCurrentlyAt + "%", EnumDeviceLogInfoType.EcgDeviceBatteryLevel);
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
        Log.v(TAG, "ECG device battery Level:  " + batteryCurrentlyAt + "%");
    }

    public void putBluetoothConnectionDeviceInfo(boolean connected){
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event;
        if (connected) {
            event = new DeviceLogEntity(sessionState.sessionId, "Device Connected: " + DeviceIdentifiers.getEcgId(), EnumDeviceLogInfoType.BluetoothConnectionDeviceInfo);
            Log.v(TAG, "Device Connected: " + DeviceIdentifiers.getEcgId());
        }
        else {
            event = new DeviceLogEntity(sessionState.sessionId, "Device Disconnected: " + DeviceIdentifiers.getEcgId(), EnumDeviceLogInfoType.BluetoothConnectionDeviceInfo);
            Log.v(TAG, "Device Disconnected: " + DeviceIdentifiers.getEcgId());
        }
        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
    }

    public void putEcgTransmissionError(String requestBody, String responseBody, int errorCode) {
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event;

        String data = "ErrorCode: " + errorCode + "; RequestBody: " + requestBody + " ;ResponseBody: " + responseBody + ";";
        event = new DeviceLogEntity(sessionState.sessionId, data, EnumDeviceLogInfoType.EcgChunkTransmissionError);
        Log.v(TAG, "Ecg Transmission Error Captured: " + responseBody);

        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
    }

    public void putPteTransmissionError(String requestBody, String responseBody, int errorCode) {
        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext());
        DeviceLogEntity event;

        String data = "ErrorCode: " + errorCode + "; RequestBody: " + requestBody + " ;ResponseBody: " + responseBody + ";";
        event = new DeviceLogEntity(sessionState.sessionId, data, EnumDeviceLogInfoType.EcgChunkTransmissionError);
        Log.v(TAG, "PTE Transmission Error Captured: " + responseBody);

        DeviceLogsDbHelper.getInstance().insertDevicelogEvent(event);
    }

    public static String convertBytesCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }
}



