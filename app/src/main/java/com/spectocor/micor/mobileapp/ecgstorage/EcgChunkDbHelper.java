package com.spectocor.micor.mobileapp.ecgstorage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.spectocor.micor.mobileapp.amain.DbHelper;
import com.spectocor.micor.mobileapp.amain.MLog;

import java.sql.SQLDataException;

/**
 * Created by Savio Monteiro on 1/18/2016.
 * Modified by Savio Monteiro on 2/27/2016. Previous version located at EcgChunkDbHelperOld
 */
public class EcgChunkDbHelper {


    private static EcgChunkDbHelper ecgChunkDbHelper;
    private static DbHelper aMainDbHelper;

    private static void log(String str) {
        MLog.log("EcgChunkDbHelper: " + str);
    }


    private EcgChunkDbHelper() {

    }

    public static EcgChunkDbHelper getInstance(Context context) {

        if(ecgChunkDbHelper == null) {
            ecgChunkDbHelper = new EcgChunkDbHelper();
        }

        aMainDbHelper = DbHelper.getInstance(context);

        return ecgChunkDbHelper;
    }

    /**
     * Saving an ecg chunk to the database
     *
     * @param ecgDataChunk The EcgDataChunk to store
     * @return Returns the EcgDataChunk if successful
     * @throws SQLDataException
     */
    public EcgDataChunk insertEcgChunk(EcgDataChunk ecgDataChunk) throws SQLDataException {

        ContentValues values = new ContentValues();

            values.put(DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID, ecgDataChunk.monitoringSessionId);
            values.put(DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP, ecgDataChunk.chunkStartDateUnixMs);
            values.put(DbHelper.ECG_CHUNKS_COLUMN_INSERT_TIMESTAMP, System.currentTimeMillis());
            values.put(DbHelper.ECG_CHUNKS_COLUMN_TIMEZONE_NAME_ID, DbHelper.TIMEZONE_NAME_ID);
            values.put(DbHelper.ECG_CHUNKS_COLUMN_TIMEZONE_OFFSET_MILLIS, DbHelper.TIMEZONE_OFFSET_MILLIS);
            values.put(DbHelper.ECG_CHUNKS_COLUMN_COMPRESSED_ECG_CHUNK_BLOB, ecgDataChunk.chunkDataChannelCompressed);
            values.put(DbHelper.ECG_CHUNKS_COLUMN_TRANSMITTED_TIMESTAMP, ecgDataChunk.chunkTransferTimestamp);
            values.put(DbHelper.ECG_CHUNKS_COLUMN_NUMBER_OF_SAMPLES_IN_CHUNK, ecgDataChunk.numberOfSamples);

        long rowId = aMainDbHelper.getWritableDatabase().insert(DbHelper.ECG_CHUNKS_TABLE_NAME, "", values);

        if(rowId == -1) {
            throw new SQLDataException("DID NOT INSERT");
        }
        else {

            log("Successfully inserted chunk " + ecgDataChunk.chunkDataChannelCompressed.length + " bytes");

            return ecgDataChunk;
        }
    }

    /**
     * Retrieves a row of ecg database with sessionId and startTimestamp as key
     * @param sessionId
     * @param startTimestamp
     * @return
     * @throws SQLDataException
     */
    public EcgDataChunk retrieveCompressedEcg(int sessionId, long startTimestamp) throws SQLDataException {

        String QUERY = "SELECT * FROM " + DbHelper.ECG_CHUNKS_TABLE_NAME + " WHERE " + DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID + "=? and " + DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP + "=?";
        String SELECTION_ARGS[] = {"" + sessionId, "" + startTimestamp};

        Cursor c = aMainDbHelper.getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        log(QUERY + " with args " + SELECTION_ARGS.toString());
        log("Num of rows: " + c.getCount());

        if(c.getCount() == 0) {
            log("Row not found ... ");
            throw new SQLDataException("No such data found");
        }

        c.moveToFirst();

        EcgDataChunk ecgDataChunk = new EcgDataChunk();
        ecgDataChunk.chunkDataChannelCompressed = c.getBlob(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_COMPRESSED_ECG_CHUNK_BLOB));
        ecgDataChunk.chunkStartDateUnixMs = c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP));
        ecgDataChunk.insertTimestamp = c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_INSERT_TIMESTAMP));
        ecgDataChunk.monitoringSessionId = Integer.parseInt(c.getString(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID)));
        ecgDataChunk.timezoneId = c.getString(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_TIMEZONE_NAME_ID));
        ecgDataChunk.timezoneOffsetMillis = c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_TIMEZONE_OFFSET_MILLIS));
        ecgDataChunk.numberOfSamples = c.getInt(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_NUMBER_OF_SAMPLES_IN_CHUNK));
        ecgDataChunk.chunkTransferTimestamp = c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_TRANSMITTED_TIMESTAMP));

        if(!c.isClosed())
        c.close();
        return ecgDataChunk;
    }

    /**
     * Setting an ecg chunk row as transferred by setting timestamp in transmitted_timestamp column
     * @param sessionId The sessionId of the current session
     * @param startTimestamp the startTimestamp of the chunk to set as transmitted.
     */
    public int setEcgChunkAsTransmitted(int sessionId, long startTimestamp) {

        if (sessionId <= 0) {
            throw new IllegalArgumentException("Session Id should be positive");
        }

        if(startTimestamp <= 0) {
            throw new IllegalArgumentException("Start timestamp should be positive");
        }

        SQLiteDatabase db = aMainDbHelper.getWritableDatabase();

        log("Setting as transmitted where sessionid='" + sessionId + "' and startTimestamp='" + startTimestamp + "'");

        ContentValues values = new ContentValues();
        values.put(DbHelper.ECG_CHUNKS_COLUMN_TRANSMITTED_TIMESTAMP, System.currentTimeMillis());

        // updating row
        return db.update(
                    DbHelper.ECG_CHUNKS_TABLE_NAME,
                    values,
                    DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID + "=? and " + DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP + "=?",
                    new String[] { "" + sessionId, "" + startTimestamp }
                );

    }

    /**
     * Retrieves the oldest ECG chunk that is not yet transferred.
     * @return EcgDataChunk instance of the oldest row if found
     * @throws SQLDataException If no row left to transfer
     */
    public EcgDataChunk retrieveOldestNotTransmittedChunk() throws SQLDataException {

        String QUERY = "SELECT * FROM " + DbHelper.ECG_CHUNKS_TABLE_NAME + " WHERE " +
                "" + DbHelper.ECG_CHUNKS_COLUMN_TRANSMITTED_TIMESTAMP + "=-1 order by " +
                "" + DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP; // + " LIMIT 1";

        String SELECTION_ARGS[] = {};
        log(QUERY + "before cursor with args " + SELECTION_ARGS.toString());
        Cursor c = aMainDbHelper.getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        log(QUERY + "after cursor with args " + SELECTION_ARGS.toString());
        log("Num of rows: " + c.getCount());

        if(c.getCount() == 0) {
            log("Row not found ... ");
            c.close(); // close cursor to free up memory
            throw new SQLDataException("NONE_LEFT_TO_TRANSFER");
        }

        c.moveToFirst();

        EcgDataChunk ecgDataChunk = new EcgDataChunk();
        ecgDataChunk.chunkDataChannelCompressed = c.getBlob(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_COMPRESSED_ECG_CHUNK_BLOB));
        ecgDataChunk.chunkStartDateUnixMs = c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP));
        ecgDataChunk.insertTimestamp = c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_INSERT_TIMESTAMP));
        ecgDataChunk.monitoringSessionId = Integer.parseInt(c.getString(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID)));
        ecgDataChunk.timezoneId = c.getString(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_TIMEZONE_NAME_ID));
        ecgDataChunk.timezoneOffsetMillis = c.getLong(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_TIMEZONE_OFFSET_MILLIS));
        ecgDataChunk.numberOfSamples = c.getInt(c.getColumnIndex(DbHelper.ECG_CHUNKS_COLUMN_NUMBER_OF_SAMPLES_IN_CHUNK));

        log("Before Return EcgChunks");
        if(!c.isClosed())
            c.close();
        return ecgDataChunk;
    }


    /**
     * This is handled in a case where session switch (eg: holter plus case) happens when the internet is off.
     * Because the internet is off the app has no idea that session 1 has terminated because of which all chunks will use session 1's session Id inside their chunk.
     * Session ID column of all chunks in the database whose recording start timestamp is greater than previous session's end timestamp,
     * must now be updated to @nextSessionId
     *
     * @param prevSessionEndTimestamp
     * @param nextSessionId
     */
    public int overrideEcgChunkSessionIds(final long prevSessionEndTimestamp, final int nextSessionId) {

        // get all chunks whose startTimestamp is greater than prevSessionEndTimestamp and set sessionId to nextSessionId

        ContentValues values = new ContentValues();
        values.put(DbHelper.ECG_CHUNKS_COLUMN_SESSION_ID, nextSessionId);

        int numberOfRows = aMainDbHelper.getWritableDatabase().update(DbHelper.ECG_CHUNKS_TABLE_NAME, values, DbHelper.ECG_CHUNKS_COLUMN_START_TIMESTAMP + ">?",
                new String[] { "" + prevSessionEndTimestamp});

        log(numberOfRows + " rows were updated");

        return numberOfRows;

    }

    /*public static EcgChunkDbHelper resetInstance(Context context) {

        log("Setting Database from Scratch");
        aMainDbHelper = null;
        return getInstance(context);
    }*/

}
