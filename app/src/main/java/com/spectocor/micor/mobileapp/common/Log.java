package com.spectocor.micor.mobileapp.common;

/**
 * Log class to cover Android Log functionality to add more control to our code
 */
public class Log {
    /**
     * Setting whether logging system is available or not
     */
    static final boolean LOG_ENABLED = true;

    /**
     * Send an info log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static int i(String tag, String msg) {
        if (LOG_ENABLED) return android.util.Log.i(tag, msg);
        return -1;
    }

    /**
     * Send an error log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static int e(String tag, String msg) {
        if (LOG_ENABLED) return android.util.Log.e(tag, msg);
        return -1;
    }

    /**
     * Send a verbose log message and log the exception.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr  An exception to log
     */
    public static int e(String tag, String msg, Throwable tr) {
        if (LOG_ENABLED) return android.util.Log.e(tag, msg, tr);
        return -1;
    }


    /**
     * Send a debug log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static int d(String tag, String msg) {
        if (LOG_ENABLED) android.util.Log.d(tag, msg);
        return -1;
    }

    /**
     * Send a verbose log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static int v(String tag, String msg) {
        if (LOG_ENABLED) return android.util.Log.v(tag, msg);
        return -1;
    }


    /**
     * Send a warning log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static int w(String tag, String msg) {
        if (LOG_ENABLED) return android.util.Log.w(tag, msg);
        return -1;
    }


}