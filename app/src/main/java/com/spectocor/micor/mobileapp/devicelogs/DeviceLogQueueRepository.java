package com.spectocor.micor.mobileapp.devicelogs;


import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.common.queuesystem.SendingQueueRepositoryBase;

/**
 * DeviceLog Sending queue repository
 */
@SuppressWarnings("WeakerAccess")
public class DeviceLogQueueRepository extends SendingQueueRepositoryBase {

    public static final String TABLE_NAME = "DeviceLogQueue";

    /**
     * SQLite script to create this table
     */
    public static final String CREATE_TABLE_SQL_SCRIPT = "create table " + TABLE_NAME + " ("
            + COLUMN_ROW_ID + " INTEGER primary key"
            + ");";

    /**
     * Constructor - takes the context to allow the database to be opened/created
     *
     * @param queueDatabaseHelper database helper
     */
    public DeviceLogQueueRepository(QueueDatabaseHelper queueDatabaseHelper) {
        super(queueDatabaseHelper, TABLE_NAME);
    }


}
