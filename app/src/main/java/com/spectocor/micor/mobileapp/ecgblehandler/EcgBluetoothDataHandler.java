package com.spectocor.micor.mobileapp.ecgblehandler;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.AMainService;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.ecgadapter.HeartRateHandler;
import com.spectocor.micor.mobileapp.ecgadapter.LiveEcgChunkInMemory;

/**
 * Created by Savio Monteiro on 1/12/2016.
 */
public class EcgBluetoothDataHandler extends Handler {

    private static Context mContext;

    public static final int NEW_ECG_DATA = 1;
    public static final int DEVICE_DISCONNECTED = 5;


    private static HandlerThread ecgBluetoothDataHandlerThread;
    private static EcgBluetoothDataHandler ecgBluetoothDataHandler;

    private static void log(String str) {
        MLog.log(EcgBluetoothDataHandler.class.getSimpleName() + ": " + str);
    }

    private EcgBluetoothDataHandler(Looper looper) {
        super(looper);
    }

    public static EcgBluetoothDataHandler getInstance(Context context) {

        if(context == null)
            throw new NullPointerException("@EcgBluetoothDataHandler: Context cannot be null");

        mContext = context;

        if(ecgBluetoothDataHandler == null) {

            if(ecgBluetoothDataHandlerThread != null) {
                ecgBluetoothDataHandlerThread.quitSafely();
            }

            ecgBluetoothDataHandlerThread = new HandlerThread("EcgBluetoothHandlerThread");
            ecgBluetoothDataHandlerThread.start();

            ecgBluetoothDataHandler = new EcgBluetoothDataHandler(ecgBluetoothDataHandlerThread.getLooper());
        }

        return ecgBluetoothDataHandler;
    }

    // this is needed to indicate if there was a recent disconnection
    // ... and live chunk collection needs to start from scratch

    public synchronized void onDeviceDisconnected() {
        obtainMessage(DEVICE_DISCONNECTED).sendToTarget();
    }

    public synchronized void handleNewEcgData(final EcgChannelBluetoothData ecgData) {

        if(ecgData == null) {
            throw new NullPointerException("@EcgBluetoothDataHandler::handleNewEcgData .. ecgData cannot be null");
        }

        obtainMessage(NEW_ECG_DATA, ecgData).sendToTarget();
    }

    private long lastEcgChunkReceivedTimestamp = -1;


    // TODO: Created for testing only.
    public long getLastEcgChunkReceivedTimestamp() {
        return lastEcgChunkReceivedTimestamp;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);

        switch (msg.what) {

            case NEW_ECG_DATA:
            {
                // RECEIVED ONE SECOND OF ECG DATA (BOTH CHANNELS)

                log("Handling New ECG Data");

                EcgChannelBluetoothData newEcgData = (EcgChannelBluetoothData) msg.obj;

                if(newEcgData == null) {
                    log("newEcgData is null");
                    return;
                }

                // ASSUMPTION FOR ALL BLUETOOTH DATA PACKET CONSUMPTION:
                /*
                    The STL API suggests
                    All packets are assumed to be of length SAMPLE_RATE.
                 */
                if(newEcgData.ch1.length < EcgProperties.SAMPLE_RATE) {
                    throw new IllegalArgumentException("Channel 1 data must be " + EcgProperties.SAMPLE_RATE + " samples.");
                }

                if(newEcgData.ch2.length < EcgProperties.SAMPLE_RATE) {
                    throw new IllegalArgumentException("Channel 2 data must be " + EcgProperties.SAMPLE_RATE + " samples.");
                }

                // checking to see if data is received in order of time
                if(newEcgData.timestamp <= lastEcgChunkReceivedTimestamp) {
                    throw new IllegalArgumentException("Timestamp of samples received is older than previous set received");
                }


                lastEcgChunkReceivedTimestamp = newEcgData.timestamp;


                // saving to current live chunk
                LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(newEcgData, mContext, false);



                // sending to heart rate computation if currently on signalview
                if(AMainService.sendToSignalView) {

                    log("Sending to the heart rate detector");

                    short[] ch1 = newEcgData.ch1;
                    HeartRateHandler.getInstance(mContext).updateForHeartRateComputation(ch1);
                }

                break;
            }

            // on disconnect a chunk is created with whatever is in buffer.
            case DEVICE_DISCONNECTED: {

                EcgChannelBluetoothData forcedChunkCreator = new EcgChannelBluetoothData(-1, new short[]{}, new short[]{});
                LiveEcgChunkInMemory.appendOneSecondEcgToLiveChunkBuffer(forcedChunkCreator, mContext, true);

                break;
            }
        }
    }
}
