package com.spectocor.micor.mobileapp.amain.state;

/**
 * Created by Savio Monteiro on 3/2/2016.
 */
public class SessionInfoForState {

    public int sessionId = -1;
    public long sessionDurationMs = -1;
    public long sessionExpiryTimestamp = -1;
    public boolean isExpired;

    public SessionInfoForState(final int sessionId, final boolean isExpired, final long sessionDurationMs) {

        if(sessionId == 0)
            this.sessionId = -1;
        else
            this.sessionId = sessionId;

        if(sessionDurationMs > 0)
            this.sessionDurationMs = sessionDurationMs;

        this.isExpired = isExpired;
    }

    public void setSessionExpiryTimestamp(final long expiryTimestamp) {
        sessionExpiryTimestamp = expiryTimestamp;
    }
}
