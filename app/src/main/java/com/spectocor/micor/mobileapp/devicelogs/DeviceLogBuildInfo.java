package com.spectocor.micor.mobileapp.devicelogs;

/**
 * Overall device info
 */
public class DeviceLogBuildInfo {

    public String OsVersion;
    public int VersionSdkInt;
    public String Device;
    public String Model;
    public String Product;
    public String Release;
    public String Display;
    public String Brand;
    public String BootLoader;
    public String Hardware;
    public String BuildId;
    public String Manufacturer;
    public String Serial;
    public String User;
    public String Host;
    public String Board;
    public String Fingerprint;
}
