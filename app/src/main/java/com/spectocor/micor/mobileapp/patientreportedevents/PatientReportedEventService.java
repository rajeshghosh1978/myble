package com.spectocor.micor.mobileapp.patientreportedevents;

import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.common.ServiceFactory;
import com.spectocor.micor.mobileapp.common.exceptions.DataAccessException;
import com.spectocor.micor.mobileapp.common.exceptions.UserException;
import com.spectocor.micor.mobileapp.common.queuesystem.QueueItem;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;
import com.spectocor.micor.mobileapp.common.queuesystem.QueueItemWithStatus;

/**
 * Patient Events service class to store patient events
 */
public class PatientReportedEventService {

    private static final String TAG = "PatientReportedEventService";

    protected final PatientReportedEventRepository repository;
    protected final GlobalSettings settings;
    protected final PatientReportedEventQueueRepository queueRepository;

    public PatientReportedEventService(PatientReportedEventRepository PatientReportedEventRepository,
                                       GlobalSettings globalSettings,
                                       PatientReportedEventQueueRepository PatientReportedEventQueueRepository) {
        repository = PatientReportedEventRepository;
        settings = globalSettings;
        queueRepository = PatientReportedEventQueueRepository;
    }


    /**
     * inserts patient reported events to the database. It sets monitoring session id and necessary parameters
     * it also puts the log in the queue to be sent to the server
     *
     * @param obj patient reported events
     * @return inserted event
     */
    public PatientReportedEvent insertEvent(PatientReportedEvent obj) throws UserException {
        try {

            obj.setMonitoringSessionId(settings.getMonitoringSession());

            long newId = repository.insert(obj);
            //queueRepository.insert(new QueueItem(newId));
            queueRepository.insert(new QueueItemWithStatus(newId));
            return obj;

        } catch (DataAccessException ex) {
            Log.e(TAG, "insertEvent", ex);
            ServiceFactory.getInstance().getApplicationLogService().logApplicationException(ex);
            throw new UserException("Insertion failed.");
        }
    }


}
