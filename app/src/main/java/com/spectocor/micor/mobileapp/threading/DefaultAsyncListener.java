package com.spectocor.micor.mobileapp.threading;

/**
 * Created by Umair Ahmed on 2/25/2016.
 */
public interface DefaultAsyncListener {

    public Object doInBackgroundThread(DefaultAsyncTask asyncTask);
    public void onCompleted(Object object);
    public void onException(Exception exception);
}
