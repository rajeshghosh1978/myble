package com.spectocor.micor.mobileapp.devicelogs;

import android.os.Build;

import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.common.ObjectUtils;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Logs important application data
 */
@SuppressWarnings("WeakerAccess")
public class ApplicationLogService {

    public static final String TAG = "ApplicationLogService";

    protected final DeviceLogLogger service;

    public ApplicationLogService(DeviceLogLogger deviceLogLogger) {
        service = deviceLogLogger;
    }


    /**
     * Logs an application exception
     *
     * @param ex exception to be logged
     * @return device log inserted to the database
     */
    public DeviceLog logApplicationException(Exception ex) {
        try {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            String exceptionDetails = sw.toString();
            DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.ApplicationException, exceptionDetails);
            service.insertLog(obj);
            return obj;
        } catch (Exception ex2) {
            Log.e(TAG, "logApplicationException exception", ex2);
            return null;
        }
    }

    /**
     * Logs an application started
     *
     * @return device log inserted to the database
     */
    public DeviceLog logApplicationStarted() {
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.ApplicationStarted);
        service.insertLog(obj);
        return obj;
    }


    /**
     * Logs an application ended
     *
     * @return device log inserted to the database
     */
    public DeviceLog logApplicationEnded() {
        // it should not return any exception to cause problem in closing down application
        try {
            DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.ApplicationEnded);
            service.insertLog(obj);
            return obj;
        } catch (Exception ex) {
            return null;
        }
    }


    /**
     * Logs an application crashed
     *
     * @return device log inserted to the database
     */
    public DeviceLog logApplicationCrashed() {
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.ApplicationCrashed);
        service.insertLog(obj);
        return obj;
    }


    /**
     * Logs an device log build information
     *
     * @return device log inserted to the database
     */
    public DeviceLog logBuildInfo() {

        DeviceLogBuildInfo info = new DeviceLogBuildInfo();
        info.OsVersion = System.getProperty("os.version");
        info.VersionSdkInt = Build.VERSION.SDK_INT;
        info.Release = Build.VERSION.RELEASE;
        info.Device = Build.DEVICE;
        info.Model = Build.MODEL;
        info.Product = Build.PRODUCT;
        info.Brand = Build.BRAND;
        info.Display = Build.DISPLAY;
        info.BootLoader = Build.BOOTLOADER;
        info.Hardware = Build.HARDWARE;
        info.BuildId = Build.ID;
        info.Manufacturer = Build.MANUFACTURER;
        info.Serial = Build.SERIAL;
        info.User = Build.USER;
        info.Host = Build.HOST;
        info.Board = Build.BOARD;
        info.Fingerprint = Build.FINGERPRINT;

        String jsonData = ObjectUtils.toJson(info);
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.BuildInfo, jsonData);
        service.insertLog(obj);
        return obj;
    }


}
