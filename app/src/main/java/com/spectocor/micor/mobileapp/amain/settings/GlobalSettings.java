package com.spectocor.micor.mobileapp.amain.settings;

/**
 * Global settings of the application
 */
public class GlobalSettings {

    //Developer Notes: To test Api on local host, you should
    // Open notepad with administration access
    // use open file: C:\Windows\System32\drivers\etc\hosts
    // add line: 10.0.2.2	localhost       # Android Emulator Loopback (ADV)
    // add line: 10.0.3.2	localhost       # Genymotion emulator
    // read more here: http://stackoverflow.com/questions/5806220/how-to-connect-to-my-http-localhost-web-server-from-android-emulator-in-eclips


    private int monitoringSessionId;
    private String webServerUrl;
    private String apiServerUrl;

    public GlobalSettings() {
        monitoringSessionId = 1;
        //webServerUrl = "http://10.0.2.2:58218/en/";
        //apiServerUrl = "http://10.0.2.2:58218/en/api/";
        apiServerUrl = "http://10.250.10.139/en/api/"; // this is ip address for devenv (Android Emulator can't find server by name)
    }

    public int getMonitoringSession() {
        return monitoringSessionId;
    }

    public String getWebServerUrl() {
        return webServerUrl;
    }

    public String getApiServerUrl() {
        return apiServerUrl;
    }

}
