package com.spectocor.micor.mobileapp.devicelogs;

import android.content.Intent;
import android.os.BatteryManager;

/**
 * Battery Info class to parse battery information from Android Intent and represent it
 */
public class BatteryInfo {

    private int status;
    private int health;
    private boolean present;
    private int level;
    private int scale;
    private int iconSmallResId;
    private int plugged;
    private int voltage;
    private int temperature;
    private String technology;


    public BatteryInfo(final Intent intent) {
        status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, 0);
        health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, 0);
        present = intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, false);
        level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
        iconSmallResId = intent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL, 0);
        plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0);
        voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0);
        temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0);
        technology = intent.getStringExtra(BatteryManager.EXTRA_TECHNOLOGY);
    }


    public int getStatus() {
        return status;
    }

    public boolean getIsCharging() {
        return (status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL);
    }

    public boolean getIsFull() {
        return status == BatteryManager.BATTERY_STATUS_FULL;
    }

    public int getHealth() {
        return health;
    }

    public boolean isPresent() {
        return present;
    }

    public int getLevel() {
        return level;
    }

    public int getScale() {
        return scale;
    }

    public byte getLevelPercent() {
        float batteryPct = level / (float) scale;
        return (byte) batteryPct;
    }

    public int getIconSmallResId() {
        return iconSmallResId;
    }

    public int getPlugged() {
        return plugged;
    }

    public int getVoltage() {
        return voltage;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getTechnology() {
        return technology;
    }


    public String getPowerPlugType() {
        return getPowerPlugType(this.plugged);
    }

    /**
     * get type of power plug whether it is ac or usb or wireless
     *
     * @param plugged value obtained from intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)
     * @return string showing charge type in Android
     */
    private String getPowerPlugType(int plugged) {

        boolean usbCharge = plugged == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = plugged == BatteryManager.BATTERY_PLUGGED_AC;
        boolean wirelessCharge = plugged == BatteryManager.BATTERY_PLUGGED_WIRELESS;

        String powerPlugType = null;

        if (acCharge)
            powerPlugType = "ac";
        if (usbCharge)
            powerPlugType = "usb";
        else if (wirelessCharge)
            powerPlugType = "wl";

        return powerPlugType;
    }


}
