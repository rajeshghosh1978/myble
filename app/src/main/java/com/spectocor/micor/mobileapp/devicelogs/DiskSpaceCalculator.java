package com.spectocor.micor.mobileapp.devicelogs;

import android.os.Build;
import android.os.Environment;

//http://stackoverflow.com/questions/3394765/how-to-check-available-space-on-android-device-on-mini-sd-card


@SuppressWarnings("WeakerAccess")
//@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
/**
 * Gets disk space from the device
 */
public class DiskSpaceCalculator {

    public static final int sdkVersion = Build.VERSION.SDK_INT;

    /**
     * Creates an instance of Disk Space calculator
     * if android version is not supported, it will throw an UnsupportedOperationException
     */
    public DiskSpaceCalculator() {
        if (sdkVersion < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Converts file size to human readable format based on Si standard
     * For example, 1000 = 1KB or 10
     *
     * @param sizeBytes size in bytes
     * @return string byte size in human readable format
     */
    public static String getReadableSize(long sizeBytes) {
        //http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
        int unit = 1000;
        if (sizeBytes <= 0)
            return "0 B";
        if (sizeBytes < unit) return sizeBytes + " B";
        int exp = (int) (Math.log(sizeBytes) / Math.log(unit));
        String pre = String.valueOf(("kMGTPEZY").charAt(exp - 1));
        return String.format("%.1f %sB", sizeBytes / Math.pow(unit, exp), pre);
    }

    /**
     * gets free space available on SD card
     *
     * @return SD free space in bytes
     */
    public long getSdCardFreeBytes() {
        try {
            //StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
            //return stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
            return Environment.getExternalStorageDirectory().getUsableSpace();
        } catch (IllegalArgumentException ex) { // if path doesn't exists, then we may not have a sd card
            return 0;
        }
    }

    /**
     * gets total space available on SD card
     *
     * @return SD total space in bytes
     */
    public long getSdCardTotalBytes() {
        try {
            //StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
            //return stat.getTotalBytes();
            return Environment.getExternalStorageDirectory().getTotalSpace();
        } catch (IllegalArgumentException ex) { //if path doesn't exists, then we may not have a SD card
            return 0;
        }
    }

    /**
     * gets internal storage total bytes
     *
     * @return internal storage total bytes
     */
    public long getInternalTotalBytes() {
        //StatFs stat = new StatFs(Environment.getRootDirectory().getAbsolutePath());
        //return stat.getTotalBytes();
        return Environment.getRootDirectory().getTotalSpace();
    }

    /**
     * gets internal storage free bytes
     *
     * @return internal storage free bytes
     */
    public long getInternalFreeBytes() {
        //StatFs stat = new StatFs(Environment.getRootDirectory().getAbsolutePath());
        //return stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        return Environment.getRootDirectory().getFreeSpace();
    }


}
