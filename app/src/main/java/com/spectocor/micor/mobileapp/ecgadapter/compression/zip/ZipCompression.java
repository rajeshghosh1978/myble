package com.spectocor.micor.mobileapp.ecgadapter.compression.zip;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.Deflater;

/**
 * This class provides methods to perform Lossless Compression on an input ECG Signal
 * The current implementation is java.util's Zip Deflater/Inflater.
 *
 * Created by Savio Monteiro
 */
public class ZipCompression {


    /**
     * Compresses an ECG Signal
     *
     * @param signalShortArr The input uncompressed signal as an array of shorts
     * @return The resulting compressed signal
     * @throws IOException
     */
    public static byte[] compressEcg(final short[] signalShortArr) throws IOException {

        if (signalShortArr == null) {
            return null;
        }

        if (signalShortArr.length == 0) {
            return new byte[]{};
            //System.out.println();
        }

        byte[] byteData = new byte[(Short.SIZE/8) * signalShortArr.length];
        ByteBuffer bb = ByteBuffer.wrap(byteData);

        for (short aSignalShortArr : signalShortArr) bb.putShort(aSignalShortArr);

        return compressEcg(byteData);
    }


    /**
     *
     * @param byteData The input uncompressed signal as an array of bytes
     * @return The resulting compressed signal in bytes
     * @throws IOException
     */
    public static byte[] compressEcg(final byte[] byteData) throws IOException {

        byte[] compressed;

        ByteArrayOutputStream bos = new ByteArrayOutputStream(byteData.length);
        Deflater compressor = new Deflater();
        compressor.setLevel(Deflater.BEST_COMPRESSION);
        compressor.setInput(byteData, 0, byteData.length);
        compressor.finish();


        // Deflating the data
        final byte[] buf = new byte[1024];
        while (!compressor.finished()) {
            int count = compressor.deflate(buf);
            bos.write(buf, 0, count);
        }
        compressor.end();
        compressed = bos.toByteArray();

        bos.close();

        return compressed;
    }

}
