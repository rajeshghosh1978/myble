package com.spectocor.micor.mobileapp.ecgchunktransmission;

import android.content.Context;

import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.common.exceptions.WebApiCallException;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.squareup.tape.QueueFile;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.sql.SQLDataException;

/**
 * Created by Savio Monteiro on 1/17/2016.
 */
public class ChunkTransmissionQueueHandler{

    private static Context mContext;
    private static ChunkTransmissionQueueHandler chunkQueueHandler;
    private static QueueFile transmissionQueueFile;

    private ChunkTransmissionQueueHandler(Context context) {
        mContext = context;
    }

    private static void log(String str) {
        MLog.log("ChunkTransmissionQueueHandler: " + str);
    }

    private static void initOrRetrieveQueueFile(Context context) throws IOException {
        if(transmissionQueueFile == null) {
            File queueFile = new File(context.getFilesDir().getAbsolutePath() + File.separator + "ChunkTransferQueue.dat");

            transmissionQueueFile = new QueueFile(queueFile);
        }
    }

    public static ChunkTransmissionQueueHandler getInstance(Context context) {

        if(chunkQueueHandler == null) {
            chunkQueueHandler = new ChunkTransmissionQueueHandler(context);
        }

        try {
            initOrRetrieveQueueFile(context);
        } catch (IOException e) {
            log("Error Initing/Retrieving Transfer Queue File: " + e.toString());
        }

        return chunkQueueHandler;
    }

    public void enqueChunkForTransmission(final ChunkTransferId chunkTransferId) {

        log("Enqueueing chunk: " + chunkTransferId.startTimestampMs);


        // int for sessionId, long for startTimestamp
        ByteBuffer bb = ByteBuffer.allocate(12);
        bb.putInt(chunkTransferId.sessionId).putLong(chunkTransferId.startTimestampMs);

        try {
            log("Adding " + chunkTransferId.sessionId + ", " + chunkTransferId.startTimestampMs + " to transfer queue");
            transmissionQueueFile.add(bb.array());

            new Thread(new Runnable() {
                @Override
                public void run() {
                   try {
                       ChunkTransferId id = getFrontItemOfTransferQueue();
                       EcgDataChunk ecgDataChunk = EcgChunkDbHelper.getInstance(mContext).retrieveCompressedEcg(id.sessionId, id.startTimestampMs);

                       log("TRANSMITTING FROM QUEUE: " + ecgDataChunk.chunkStartDateUnixMs);

                       if(ChunkTransmissionThread.getInstance(mContext).transferChunk(ecgDataChunk)) {
                           log("TRANSMISSION FROM TRANSFER QUEUE SUCCESSFUL");
                           transmissionQueueFile.remove();
                       }
                       else
                       {

                       }

                    } catch (WebApiCallException e) {
                        log(e.toString());
                    } catch (IOException e) {
                        log(e.toString());
                    } catch (SQLDataException e) {
                       log(e.toString());
                   }
                }
            }).start();

        } catch (IOException e) {
            log("Error: Could not add transmission IDs: " + e.toString());
        }

    }

    public static ChunkTransferId getFrontItemOfTransferQueue() {

        ChunkTransferId chunkTransferId = null;
        try {

            byte[] bytes = transmissionQueueFile.peek();

            ByteBuffer bb = ByteBuffer.wrap(bytes);
            chunkTransferId = new ChunkTransferId(bb.getInt(), bb.getLong());

        } catch (IOException e) {
            log("Error Accessing Transfer Queue File: " + e.toString());
        }

        return chunkTransferId;

    }



}
