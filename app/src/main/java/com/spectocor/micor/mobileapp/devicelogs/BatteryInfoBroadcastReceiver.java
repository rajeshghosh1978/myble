package com.spectocor.micor.mobileapp.devicelogs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.common.ServiceFactory;

/**
 * Receives battery information from Android Services
 * Read more: http://developer.android.com/training/monitoring-device-state/battery-monitoring.html
 * <p/>
 * http://www.javacodegeeks.com/2014/01/android-service-tutorial.html
 */
public class BatteryInfoBroadcastReceiver extends BroadcastReceiver {

    public final static String TAG = "BatteryInfoBroadcastReceiver";

    protected final BatteryLogService logService;

    /**
     * Constructor for Battery Info broadcast receiver
     * @throws Exception
     */
    public BatteryInfoBroadcastReceiver() throws Exception {
        super();
        Log.i(TAG, "constructor");

        logService = ServiceFactory.getInstance().getBatteryLogService();
    }

    /**
     * creates intent filter to be used for this receiver
     *
     * @return intent filter for battery status and power plug
     */
    public static IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        intentFilter.addAction(Intent.ACTION_POWER_CONNECTED);
        intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        intentFilter.addAction(Intent.ACTION_BATTERY_LOW);
        intentFilter.addAction(Intent.ACTION_BATTERY_OKAY);
        return intentFilter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onRecieve" + intent.getAction());

        if (intent.getAction() == null)
            return;

        BatteryInfo info = new BatteryInfo(intent);

        switch (intent.getAction()) {
            case Intent.ACTION_BATTERY_CHANGED:
                //DeviceLogEvents.getInstance().putLogMobileDeviceBatteryLevel("");
                break;
            case Intent.ACTION_POWER_CONNECTED:
            case Intent.ACTION_POWER_DISCONNECTED:
                logService.logMobileDevicePowerPlugConnected(info.getIsCharging(), info.getPowerPlugType());
                break;
            case Intent.ACTION_BATTERY_LOW:
                //logService.logMobileDevicePowerPlugConnected(info.getIsCharging(), info.getPowerPlugType());
                //TODO: Call notification system to inform user that the battery is low
                break;
            case Intent.ACTION_BATTERY_OKAY:
                // TODO: Figure out what to do here.
                break;
            default:
                Log.e(TAG, "Unexpected value for action: " + intent.getAction());
        }
    }




}