package com.spectocor.micor.mobileapp.http;

import android.os.HandlerThread;

import com.spectocor.micor.mobileapp.common.Log;

/**
 * Custom implementation of HandlerThread to log thread events.
 *
 * Created by Rafay Ali on 1/21/2016.
 */
public class HttpServiceHandlerThread extends HandlerThread {

    private final String TAG = HttpServiceHandlerThread.class.getSimpleName();

    public HttpServiceHandlerThread(String name, int priority) {
        super(name, priority);
    }

    @Override
    public synchronized void start() {
        super.start();
        Log.v(TAG, "Starting HttpServiceHandlerThread.");
    }

    @Override
    public void interrupt() {
        super.interrupt();
        Log.w(TAG, "Thread has been interrupted.");
    }

    @Override
    public boolean quit() {
        Log.w(TAG, "Thread has been called to quit (unsafely)");
        return super.quit();
    }

    @Override
    public boolean quitSafely() {
        Log.w(TAG, "Thread has been called to quit");
        return super.quitSafely();
    }
}
