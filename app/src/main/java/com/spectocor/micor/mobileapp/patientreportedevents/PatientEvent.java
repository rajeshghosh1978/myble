package com.spectocor.micor.mobileapp.patientreportedevents;

/**
 * Model class for PatientReportedEvent to be transmitted to server
 *
 * Created by Rafay Ali on 2/18/2016.
 */
@SuppressWarnings("unused")
public class PatientEvent {

    private long sessionId;
    private long insertTimestamp;
    private long transmittedTimestamp;
    private String data;

    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }

    public long getInsertTimestamp() {
        return insertTimestamp;
    }

    public void setInsertTimestamp(long insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    public long getTransmittedTimestamp() {
        return transmittedTimestamp;
    }

    public void setTransmittedTimestamp(long transmittedTimestamp) {
        this.transmittedTimestamp = transmittedTimestamp;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
