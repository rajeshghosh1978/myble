package com.spectocor.micor.mobileapp.amain;

import android.content.Context;
import android.os.StatFs;

import com.spectocor.micor.mobileapp.amain.sessionlist.SessionListSessionInfo;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;

import java.io.File;

/**
 * Created by Savio Monteiro on 2/22/2016.
 */
public class StorageAndDbInfo {

    /**
     * Determines available space on a particular storage device like SD Card or Emulated Storage.
     * @param devRootPath Root path of the storage device.
     * @return Available space of the storage device in bytes.
     */
    public static long getAvailableSpaceInBytes(String devRootPath)
    {
        StatFs stat = new StatFs(devRootPath);
        long bytesAvailable = (long)stat.getBlockSize() *(long)stat.getBlockCount();
        //long megAvailable = bytesAvailable / 1048576;
        return bytesAvailable;
    }


    /**
     * Determine database name and path for a particular session
     * @param sessionInfo
     * @return
     */
    public static String formulateDbNameAndPathBySessionInfo(String dbPath, SessionListSessionInfo sessionInfo) {

        //String dbPath = AMainApplication.DB_PATH;
        //String dbFilename = "MicorDB" + "_" + sessionInfo.sessionType + "_" + sessionInfo.sessionId + "_" + sessionInfo.sessionStartTimestamp; // + ".db";
        //String dbFilename = "MicorDB" + "_" + sessionInfo.sessionId + "_" + sessionInfo.sessionStartTimestamp; // + ".db";

        String dbFilename = "";
        if(dbPath == "")
                dbFilename = "MicorDB" + "_" + sessionInfo.sessionType + "_" + sessionInfo.sessionId + ".db";
        else
            dbFilename = dbPath + File.separator + "MicorDB" + "_" + sessionInfo.sessionType + "_" + sessionInfo.sessionId + ".db";

        return  dbFilename;
    }

    /**
     * Get the database name with full path from session state
     * @param context
     * @return
     */
    public static String getDbNameAndPathFromState(Context context) {
        if(SessionStateAccess.getCurrentSessionState(context) != null)
            return SessionStateAccess.getCurrentSessionState(context).dbPathAndName;
        else
            return "";
    }

}
