package com.spectocor.micor.mobileapp.ecgblehandler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainService;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.amain.receivers.BluetoothConnectivityReceiver;
import com.spectocor.micor.mobileapp.amain.receivers.EcgBatteryLevelReceiver;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.ecgadapter.EcgResampler;
import com.spectocor.micor.mobileapp.ecgadapter.HeartRateHandler;
import com.spectocor.micor.mobileapp.ecgadapter.OldEcgChunkHandler;
import com.stl.bleservice.BluetoothComm;
import com.stl.bleservice.EventCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Savio Monteiro on 2/14/2016.
 */
public class SynergenEventCallback implements EventCallback {

    private static SynergenEventCallback synergenEventCallback;

    private Context mContext;
    private static BluetoothComm btComm;

    /// ---------
    // Used to know the timestamps of first second of data after reconnection.
    // .. knowing this will help determine when all old data has been transmitted
    // .. from the ECG Device
    private static ArrayList<Long> reconnectionTimestampsOfFirstSecondData;
    private static boolean recentlyReconnected = false;
    /// ---------



    ////////////////////// new variables - NOT NEEDED ///////////////////
    private static boolean capturingOldData = false;
    private static long timestampFirstLiveSinceDisconnection = -1;

    private static HashMap<Long, ArrayList<Short>> ch1_OldData;
    private static HashMap<Long, ArrayList<Short>> ch2_OldData;

    private static HashMap<Long, ArrayList<Short>> ch1_LiveBuffer;
    private static HashMap<Long, ArrayList<Short>> ch2_LiveBuffer;
    //////////////////////-----------------------------------------------





    private SynergenEventCallback(Context context) {
        mContext = context;
    }


    public static SynergenEventCallback getInstance(Context context) {
        if(synergenEventCallback == null) {
            synergenEventCallback = new SynergenEventCallback(context);
        }

        if(btComm != null) {
            btComm.register(synergenEventCallback);
        }

        if(reconnectionTimestampsOfFirstSecondData == null) {
            reconnectionTimestampsOfFirstSecondData = new ArrayList<>();
        }

        return synergenEventCallback;
    }

    public static SynergenEventCallback getInstance(Context context, BluetoothComm btComm1) {
        if(synergenEventCallback == null) {
            synergenEventCallback = new SynergenEventCallback(context);
        }

        if(btComm == null) {
            btComm = btComm1;
        }

        btComm.register(synergenEventCallback);

        if(reconnectionTimestampsOfFirstSecondData == null) {
            reconnectionTimestampsOfFirstSecondData = new ArrayList<>();
        }

        return synergenEventCallback;
    }

    private static void log(String str) {
        MLog.log("SynergenEventCallback: " + str);
    }


    public void unregisterAndDisconnect() {
        if(btComm != null) {

            log("unregisterAndDisconnect()");

            unregister();
            disconnect();
        }
    }


    private void unregister() {

        if(btComm != null ) {
            try {
                log("Unregistering ... ");
                btComm.unregister();
                log("   ... Done");
            } catch (Exception e) {
                e.printStackTrace();
                log(e.getMessage());
            }
        }

        SynergenBleClient.bleInstanceActive = false;
    }

    private void disconnect() {

        if(btComm != null) {
            try {
                log("Disconnecting ... ");
                btComm.disconnect();
                log("   ... Done");
            } catch (Exception e) {
                e.printStackTrace();
                log(e.getMessage());
            }
        }

    }



    private static Intent createBtEventIntent(Bundle b) {
        Intent i = new Intent("BLUETOOTH_EVENT");
        i.putExtras(b);
        return i;
    }





    /**
     * STL Callback interface to receive live ecg signal data one second at a time.
     * @param timestampSeconds
     * @param channel1 <SAMPLE_RATE> count of samples of channel 1
     * @param channel2 <SAMPLE_RATE> count of samples of channel 2
     * @param motion
     */
    @Override
    public void onData(Long timestampSeconds, List<Integer> channel1, List<Integer> channel2, List<Integer> motion) {

        /*
        // Should probably be done using the disconnect() and unregister() calls
        if (!SessionStateAccess.getCurrentSessionState(mContext).sessionActive) {

            log("Session Not Active. Returning ... ");

            btComm.disconnect();
            btComm.unregister();

            SynergenBleClient.bleInstanceActive = false;

            return;
        }*/


        if(recentlyReconnected) {
            reconnectionTimestampsOfFirstSecondData.add(timestampSeconds);
            recentlyReconnected = false;
        }


        log("Recd: " + channel1.size());


        short[] downSampledCh1 = EcgResampler.resample(400, EcgProperties.SAMPLE_RATE, channel1);

        // TODO: channel 1 is duplicated here ... replace with real ch2 when available
        short[] downSampledCh2 = EcgResampler.resample(400, EcgProperties.SAMPLE_RATE, channel1);

        log("downsampled ch1: " + downSampledCh1.length);
        log("downsampled ch2: " + downSampledCh2.length);


        /*
        if(capturingOldData)
        {
            ch1_LiveBuffer.put(timestampSeconds, (ArrayList) channel1);
            ch2_LiveBuffer.put(timestampSeconds, (ArrayList) channel2);
        }
        else
        {
            EcgChannelBluetoothData ecgBtData = new EcgChannelBluetoothData(timestampSeconds, downSampledCh1, downSampledCh2);
            EcgBluetoothDataHandler.getInstance(mContext).handleNewEcgData(ecgBtData);
        }*/

        EcgChannelBluetoothData ecgBtData = new EcgChannelBluetoothData(timestampSeconds, downSampledCh1, downSampledCh2);
        EcgBluetoothDataHandler.getInstance(mContext).handleNewEcgData(ecgBtData);


        if (AMainService.sendToSignalView) {

            //final short[] dsCh1 = EcgResampler.resample(250, 150, downSampledCh1);
            //final short[] dsCh2 = EcgResampler.resample(250, 150, downSampledCh2);

            final short[] dsCh1 = downSampledCh1;
            final short[] dsCh2 = downSampledCh2;

            ArrayList<Integer> filtCh1 = HeartRateHandler.getInstance(mContext).applyFilter(dsCh1, 1);
            ArrayList<Integer> filtCh2 = HeartRateHandler.getInstance(mContext).applyFilter(dsCh2, 2);

            // JOINING BOTH BROADCASTS AND SENDING AS ONE
            log("Sending signal to signal view ... ");

            Bundle bb = new Bundle();
            bb.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SIGNAL_VIEW_LE_ECG_SAMPLES_READY);
            bb.putString("samplesCh1Json", new Gson().toJson(filtCh1));
            bb.putString("samplesCh2Json", new Gson().toJson(filtCh2));
            Intent ii = createBtEventIntent(bb);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(ii);
        }
    }

    @Override
    public void onOldData(Long timestampSeconds, List<Integer> channel1, List<Integer> channel2, List<Integer> list3) {


        boolean x = true; if(x) return;

        // TODO: handle old data  ...
        short[] downSampledCh1 = EcgResampler.resample(400, EcgProperties.SAMPLE_RATE, channel1);

        // TODO: channel 1 is duplicated here ... replace with real ch2 when available
        short[] downSampledCh2 = EcgResampler.resample(400, EcgProperties.SAMPLE_RATE, channel1);


        log("old data downsampled ch1: " + downSampledCh1.length);
        log("old data downsampled ch2: " + downSampledCh2.length);


        OldEcgChunkHandler.getInstance(mContext).obtainMessage(OldEcgChunkHandler.OLD_ECG_ONE_SECOND_READY, new EcgChannelBluetoothData(timestampSeconds, downSampledCh1, downSampledCh2)).sendToTarget();

        /*ch1_OldData.put(timestampSeconds, (ArrayList) channel1);
        ch2_OldData.put(timestampSeconds, (ArrayList) channel2);
        */

        if((timestampSeconds + 1) == reconnectionTimestampsOfFirstSecondData.get(0)) {

            log("Timestamp of last old data: " + timestampSeconds);
            log("first live data recd at " + reconnectionTimestampsOfFirstSecondData.get(0));

            reconnectionTimestampsOfFirstSecondData.remove(0);

            OldEcgChunkHandler.getInstance(mContext).obtainMessage(OldEcgChunkHandler.OLD_DATA_FINISHED).sendToTarget();

            //sequenceTheBuffers();
        }



    }


    @Override
    public void onDeviceBatteryUpdate(int battery) {

        log("Battery: " + battery);

        Bundle b = new Bundle();
        b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_LE_ECG_NEW_BATTERY_VALUE);
        Intent intent = new Intent("BLUETOOTH_EVENT");
        intent.putExtras(b);
        intent.putExtra("newEcgBattery", battery);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

        // notifying receiver
        Intent notificationIntent = EcgBatteryLevelReceiver.getNotificationIntent(battery, null);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(notificationIntent);

    }

    @Override
    public void onLeadChange(boolean b) {

    }

    @Override
    public void onDeviceError(int i) {

        log("ECG Device Error: " + i);


    }

    @Override
    public void onConnectionChange(BluetoothComm.CONNECTION_STATUS prevStatus, BluetoothComm.CONNECTION_STATUS newStatus)
    {

        switch (newStatus) {

            case CONNECTING: {
                log("CONNECTING");

                if(!SessionStateAccess.getCurrentSessionState(mContext).sessionActive) {

                    log("Session Not Active. This callback is still registered. Unregistering!");

                    try {
                        log("Unregistering ... ");
                        btComm.unregister();
                        log(" ... done");

                        log("Disconnecting ... ");
                        btComm.disconnect();
                        log(" ... done");
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }

                    return;
                }

                Bundle b = new Bundle();
                b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTING);
                Intent intent = new Intent("BLUETOOTH_EVENT");
                intent.putExtras(b);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            }

            case CONNECTED: {

                if(prevStatus == BluetoothComm.CONNECTION_STATUS.DISCONNECTED) {
                    recentlyReconnected = true;
                }
                else {
                    return;
                }

                log("CONNECTED");
                SynergenBleClient.bleConnected = true;

                // for activity
                Bundle b = new Bundle();
                b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SIGNAL_VIEW_LE_DEVICE_CONNECTED2);
                Intent intent = new Intent("BLUETOOTH_EVENT");
                intent.putExtras(b);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);


                // for notification broadcast receiver
                BluetoothConnectivityReceiver.notifyMessage(BluetoothConnectivityReceiver.MESG_BT_CONNECTED, null);

                break;
            }
            case DISCONNECTED: {

                log("DISCONNECTED");

                SynergenBleClient.bleConnected = false;


                Bundle b = new Bundle();
                b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SIGNAL_VIEW_LE_DEVICE_DISCONNECTED2);
                Intent intent = new Intent("BLUETOOTH_EVENT");
                intent.putExtras(b);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                // for notification broadcast receiver
                BluetoothConnectivityReceiver.notifyMessage(BluetoothConnectivityReceiver.MESG_BT_DISCONNECTED, null);

                EcgBluetoothDataHandler.getInstance(mContext).onDeviceDisconnected();

                //btComm.unregister();

                /*if (SessionStateAccess.getCurrentSessionState(mContext).sessionActive) {
                    SynergenBleClient.registerAndConnect(mContext);
                }*/


                // Will be set false when old data and live data up to real-time
                // is sequenced and handled by chunk processor
                //capturingOldData = true;

                /*
                ch1_OldData = new HashMap<>();
                ch2_OldData = new HashMap<>();

                ch1_LiveBuffer = new HashMap<>();
                ch2_LiveBuffer = new HashMap<>();
                */

                break;
            }

            case CLOSED: {
                log("CLOSED");
                SynergenBleClient.bleConnected = false;

                Bundle b = new Bundle();
                b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SIGNAL_VIEW_LE_DEVICE_DISCONNECTED2);
                Intent intent = new Intent("BLUETOOTH_EVENT");
                intent.putExtras(b);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

                // for notification broadcast receiver
                BluetoothConnectivityReceiver.notifyMessage(BluetoothConnectivityReceiver.MESG_BT_DISCONNECTED, null);

                //btComm.unregister();

                /*if (SessionStateAccess.getCurrentSessionState(mContext).sessionActive) {
                    SynergenBleClient.registerAndConnect(mContext);
                }*/

                break;
            }
        }
    }

    @Override
    public void onFailToConnect() {

        log("Failed to connect! Re-attempting ... ");
        if (SessionStateAccess.getCurrentSessionState(mContext).sessionActive) {
            btComm.connect();
        }

    }
}
