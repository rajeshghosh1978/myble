package com.spectocor.micor.mobileapp.http;

import com.spectocor.micor.mobileapp.common.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Base class for Http Request calls which are needed to be sent to HttpQueueService
 *
 * Created by Rafay Ali on 1/20/2016.
 */
@SuppressWarnings("unused")
public abstract class HttpRequestBase {

    public static final String TAG = HttpRequestBase.class.getSimpleName();

    public void onQueue(){

    }

    public void onSuccess(String response) throws JSONException {
        Log.v(TAG, new JSONObject(response).toString());
    }

    public void onError(String response) throws JSONException {
        Log.v(TAG, new JSONObject(response).toString());
    }

    public void onCancelled(){

    }

    /**
     * Return network request type (eg. ECG_CHUNK_REQUEST). Types can be found inside HttpRequestHandler.
     * @return int
     */
    public abstract int getRequestType();
}
