package com.spectocor.micor.mobileapp.ecgadapter.mobileheartrate;


public class QRSFilterer {

	private QRSDetectorParameters qrsDetParas ;
	
	private long  lpfilt_y1 = 0 ;
	private long  lpfilt_y2 = 0 ;
	private int[] lpfilt_data ;
	private int   lpfilt_ptr = 0 ;
	
	private long  hpfilt_y=0 ;
	private int[] hpfilt_data ;
	private int   hpfilt_ptr = 0 ;
	
	private int[] deriv1_derBuff ;
	private int   deriv1_derI = 0;
	
	private int[] deriv2_derBuff ;
	private int   deriv2_derI = 0;
	
	private long  mvwint_sum = 0 ;
	private int[] mvwint_data ;
	private int   mvwint_ptr = 0 ;
	

	public QRSFilterer(QRSDetectorParameters qrsDetectorParameters)
		{
		qrsDetParas = qrsDetectorParameters ;
		lpfilt_data    = new int[qrsDetectorParameters.LPBUFFER_LGTH] ;
		hpfilt_data    = new int[qrsDetectorParameters.HPBUFFER_LGTH] ;
		deriv1_derBuff = new int[qrsDetectorParameters.DERIV_LENGTH] ;
		deriv2_derBuff = new int[qrsDetectorParameters.DERIV_LENGTH] ;
		mvwint_data    = new int[qrsDetectorParameters.WINDOW_WIDTH];
		}

	public int QRSFilter(int datum)
	{
		int fdatum ;
		fdatum = lpfilt( datum ) ;  // Low pass filter data.
		fdatum = hpfilt( fdatum ) ; // High pass filter data.
		fdatum = deriv2( fdatum ) ; // Take the derivative.
		fdatum = Math.abs(fdatum) ;	// Take the absolute value.
		fdatum = mvwint( fdatum ) ; // Average over an 80 ms window .
		return(fdatum) ;
	}
	

	private int lpfilt( int datum )
		{
		long y0 ;
		int output ;
		int halfPtr ;

		halfPtr = lpfilt_ptr-(qrsDetParas.LPBUFFER_LGTH/2) ; // Use halfPtr to index
		if(halfPtr < 0) // to x[n-6].
			halfPtr += qrsDetParas.LPBUFFER_LGTH ;
		y0 = (lpfilt_y1 << 1) - lpfilt_y2 + datum - (lpfilt_data[halfPtr] << 1) + lpfilt_data[lpfilt_ptr] ;
		lpfilt_y2 = lpfilt_y1;
		lpfilt_y1 = y0;
		output = (int) y0 / ((qrsDetParas.LPBUFFER_LGTH*qrsDetParas.LPBUFFER_LGTH)/4);
		lpfilt_data[lpfilt_ptr] = datum ; // Stick most recent sample into
		if(++lpfilt_ptr == qrsDetParas.LPBUFFER_LGTH) // the circular buffer and update
			lpfilt_ptr = 0 ; // the buffer pointer.
		return(output) ;
		}
	

	private int hpfilt( int datum )
		{
		int z ;
		int halfPtr ;

		hpfilt_y += datum - hpfilt_data[hpfilt_ptr];
		halfPtr = hpfilt_ptr-(qrsDetParas.HPBUFFER_LGTH/2) ;
		if(halfPtr < 0)
			halfPtr += qrsDetParas.HPBUFFER_LGTH ;
		z = (int) (hpfilt_data[halfPtr] - (hpfilt_y / qrsDetParas.HPBUFFER_LGTH));
		hpfilt_data[hpfilt_ptr] = datum ;
		if(++hpfilt_ptr == qrsDetParas.HPBUFFER_LGTH)
			hpfilt_ptr = 0 ;
		return( z );
		}


	public int deriv1( int x )
		{
		int y ;
		y = x - deriv1_derBuff[deriv1_derI] ;
		deriv1_derBuff[deriv1_derI] = x ;
		if(++deriv1_derI == qrsDetParas.DERIV_LENGTH)
			deriv1_derI = 0 ;
		return(y) ;
		}
	

	private int deriv2( int x )
		{
		int y ;
		y = x - deriv2_derBuff[deriv2_derI] ;
		deriv2_derBuff[deriv2_derI] = x ;
		if(++deriv2_derI == qrsDetParas.DERIV_LENGTH)
			deriv2_derI = 0 ;
		return(y) ;
		}
	
	
	private int mvwint( int datum )
		{
		int output;
		mvwint_sum += datum ;
		mvwint_sum -= mvwint_data[mvwint_ptr] ;
		mvwint_data[mvwint_ptr] = datum ;
		if(++mvwint_ptr == qrsDetParas.WINDOW_WIDTH)
			mvwint_ptr = 0 ;
		if((mvwint_sum / qrsDetParas.WINDOW_WIDTH) > 32000)
			output = 32000 ;
		else
			output = (int) (mvwint_sum / qrsDetParas.WINDOW_WIDTH) ;
		return(output) ;
		}
}
