package com.spectocor.micor.mobileapp.amain;

import android.os.Handler;
import android.os.Message;

import com.spectocor.micor.mobileapp.amain.sessionlist.SessionListAccess;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.ecgsimulator.EcgSimulatorThread;
import com.spectocor.micor.mobileapp.ecgadapter.LiveEcgChunkInMemory;
import com.spectocor.micor.mobileapp.ecgadapter.OldEcgChunkHandler;
import com.spectocor.micor.mobileapp.http.HttpQueueManager;
import com.spectocor.micor.mobileapp.http.HttpQueueService;

import java.lang.ref.WeakReference;

/**
 * Created by Savio Monteiro on 2/18/2016.
 */
public class AMainActivityHandler extends Handler
{

    public static final int ACTIVITY_TOAST_LONG = 1;
    public static final int ACTIVITY_TOAST_SHORT = 2;
    public static final int ACTIVITY_RESET_EVERYTHING = 3;
    public static final int ACTIVITY_TURN_SCREEN_ON = 4;
    public static final int ACTIVITY_DISPLAY_ISSUES_SCREEN = 5;
    public static final int ACTIVITY_JSTESTFUNCTION = 6;


    private WeakReference<AMainActivity> aMainActivity;


    AMainActivityHandler(WeakReference<AMainActivity> activity) {

        super(activity.get().getMainLooper());

        aMainActivity = activity;
    }



    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);

        switch (msg.what) {

            case ACTIVITY_TOAST_LONG:
                String str = (String) msg.obj;

                AMainApplication.showLongToast(str, aMainActivity.get().getApplicationContext());

                break;

            case ACTIVITY_TOAST_SHORT:
                String str2 = (String) msg.obj;

                AMainApplication.showShortToast(str2, aMainActivity.get().getApplicationContext());

                break;

            case ACTIVITY_RESET_EVERYTHING: {

                if (aMainActivity.get().mBound) {

                    aMainActivity.get().mService.endBluetoothConnectivity();
                    aMainActivity.get().mService.endTransmissionQueueThread();

                        /*mService.stopForeground(true);
                        mService.stopSelf();*/

                }

                SessionListAccess.cleanToKeepLastThreeSessions(aMainActivity.get().getApplicationContext());

                LiveEcgChunkInMemory.initAll();
                OldEcgChunkHandler.getInstance(aMainActivity.get().getApplicationContext()).endInstance();

                /*if(mHttpServiceBound) {
                    httpService.stopSelf();
                }*/

                SessionStateAccess.saveSessionStateJson(aMainActivity.get().getApplicationContext(), "{}");

                HttpQueueManager.getInstance().stopBufferedHandler();
                HttpQueueManager.getInstance().stopFifoEcgBufferedHandler();
                HttpQueueManager.getInstance().stopFifoPteBufferedHandler();

                //AppServiceManager.stopService(HttpQueueService.class);

                AppServiceManager.stopService(HttpQueueService.class);
                AppServiceManager.stopService(AMainService.class);

                aMainActivity.get().finish();

                break;
            }

            case ACTIVITY_TURN_SCREEN_ON: {

                /*getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);*/

                aMainActivity.get().turnScreenOn();

                break;
            }

            case ACTIVITY_DISPLAY_ISSUES_SCREEN: {

                if (aMainActivity.get().mWebView != null) {
                    aMainActivity.get().mWebView.loadUrl("javascript: displayIssuesScreen();");
                }

                break;
            }

            case ACTIVITY_JSTESTFUNCTION: {
                if (aMainActivity.get().mWebView != null) {
                    String jsFunctionStr = (String) msg.obj;
                    aMainActivity.get().mWebView.loadUrl("javascript: " + jsFunctionStr + ";");
                }
            }
        }
    }
}
