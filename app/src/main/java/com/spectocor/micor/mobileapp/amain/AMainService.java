package com.spectocor.micor.mobileapp.amain;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.spectocor.micor.mobileapp.R;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.ecgblehandler.SynergenBleClient;
import com.spectocor.micor.mobileapp.ecgblehandler.SynergenEventCallback;
import com.spectocor.micor.mobileapp.ecgsimulator.EcgSimulatorThread;
import com.stl.bleservice.STLBLE;

import static com.spectocor.micor.mobileapp.amain.state.SessionStateAccess.getCurrentSessionState;

/**
 * Created by Savio Monteiro on 1/11/2016.
 */
public class AMainService extends Service {

    public static boolean sendToSignalView = false;
    private static boolean mActivityBound = false;
    public static boolean APP_DEAD = false;

    public static AMainService mService = null;
    public static Context mContext = null;

    private static boolean bleActive = false;

    private static CurrentSessionState mCurrentSessionState = null;
    private final IBinder mBinder = new LocalBinder();


    public class LocalBinder extends Binder {
        public AMainService getService() {
            return AMainService.this;
        }
    }


    public static void log(String str) {
        MLog.log("AMainService: " + str);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        mActivityBound = true;

        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {

        mActivityBound = false;

        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        log("onCreate()");

        mService = this;
        mContext = this;
        mCurrentSessionState = getCurrentSessionState(this);

        APP_DEAD = false;
    }



    private static boolean serviceCommandStarted = false;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        /*if(serviceCommandStarted) {
            log("Service already started  ... returning");
            return -1;
        }

        serviceCommandStarted = true;
        */

        log("onStartCommand()");


        Intent showTaskIntent = new Intent(getApplicationContext(), AMainActivity.class);

        showTaskIntent.setAction(Intent.ACTION_MAIN);
        showTaskIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        showTaskIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(
                getApplicationContext(), 0, showTaskIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification.Builder(
                getApplicationContext()).setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("Micor").setContentText("")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(contentIntent).build();

        startForeground(1, notification);

      /*  en = basePreferenceHelper.getSavedEnrollmentInfo();
        si = new SessionInfo();
        if(en.getSession(0)!=null)
        {
            si = en.getSession(0);
        }*/
        /*datetimestart = si.getStartDateTime();
        datetimeend = si.getEndDateTime();
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");*/
        /*try {
            Date startDate = outputFormat.parse(datetimestart);
            Date endDate = outputFormat.parse(datetimeend);
            diffInMs = endDate.getTime() - startDate.getTime();

            //long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
        }catch (ParseException ex){

        }*/

        startBluetoothConnectivity();
        startTransmissionQueueThread();

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {

        log("service: onDestroy()");

        if (SynergenBleClient.bleServiceInitialized && AMainApplication.DO_BLUETOOTH) {
            try {
                STLBLE.getInstance(mService.getApplicationContext()).unbindService();
            } catch (Exception e) {
                log(e.getMessage());
                e.printStackTrace();
            }
        }


        super.onDestroy();

        APP_DEAD = true;

    }

    /// SERVICE METHODS


    public void startTransmissionQueueThread() {

    }

    public void endTransmissionQueueThread() {

    }


    public void startBluetoothConnectivity() {

        log("Session Active? " + getCurrentSessionState(mContext).sessionActive);

        // TODO uncomment this code when done testing with simulated ECG data and remove the other if check.
        if (SessionStateAccess.getCurrentSessionState(mContext).sessionActive) {
            log("Session is active.");

            if (AMainApplication.DO_BLUETOOTH) {

                log("Starting BT.");

                final String mac = getCurrentSessionState(mContext).ecgBtMac;

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        if (!SynergenBleClient.bleInstanceActive) {

                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                log(e.getMessage());
                            }

                            if (SynergenBleClient.bleServiceInitialized) {

                                log("Ble Service is detected initialized. Starting Connectivity.");
                                SynergenBleClient.startConnectivity(mContext, mac);
                                //SynergenBleClient.registerAndConnect(mContext, mac);
                            } else {
                                log("Ble Service not initialized yet");
                            }

                        } else {
                            log("BleService Active. Not doing anything");
                        }
                    }
                }).start();
            } else // SIMULATE ECG
            {
                log("Starting ECG Simulation.");

                if (!EcgSimulatorThread.getInstance(mContext).isRunning) {

                    // inline thread needed because instance initialization (reading huge file) is slow ...
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            log("Starting ECG Simulator Thread ... ");
                            EcgSimulatorThread.getInstance(mContext).start();
                        }
                    }).start();
                } else {
                    log("ECG Simulation already running ... ");
                }
            }
        }
    }

    public void endBluetoothConnectivity() {

        if (!AMainApplication.DO_BLUETOOTH) {
            // ending simulation
            log("Ending Simulation Thread");
            EcgSimulatorThread.getInstance(getApplicationContext()).endSimulation();
        }
        else {
            // Bluetooth Automatically disconnects if session state is set to inactive.
            log("Unregistering and disconnecting Bluetooth");
            SynergenEventCallback.getInstance(getApplicationContext()).unregisterAndDisconnect();
        }


    }


}
