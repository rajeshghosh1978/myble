package com.spectocor.micor.mobileapp.common;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.spectocor.micor.mobileapp.common.exceptions.DataAccessException;

import java.util.ArrayList;

/**
 * Base class for repositories
 */
public abstract class RepositoryBase<T> {


    /**
     * table name in the database to work with
     */
    protected String TableName;

    /**
     * Row identifier column of the table
     */
    protected String RowIdColumnName;

    /**
     * Data helper to work with database
     */
    private SQLiteOpenHelper databaseHelper;

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param tableName       table name
     * @param rowIdColumnName row identifier column name
     * @param databaseHelper  database helper
     */
    public RepositoryBase(SQLiteOpenHelper databaseHelper, String tableName, String rowIdColumnName) {
        if (databaseHelper == null)
            throw new IllegalArgumentException("queueDatabaseHelper");

        if (tableName == null || tableName.trim().length() == 0)
            throw new IllegalArgumentException("tableName should not be null.");

        if (rowIdColumnName == null || rowIdColumnName.trim().length() == 0)
            throw new IllegalArgumentException("rowIdColumnName should not be null.");

        this.TableName = tableName;
        this.RowIdColumnName = rowIdColumnName;
        this.databaseHelper = databaseHelper;
    }


    /**
     * Create a new car. If the car is successfully created return the new
     * rowId for that car, otherwise return a -1 to indicate failure.
     *
     * @param obj object to be inserted to the database
     * @return the row ID of the newly inserted row, or -1 if an error occurred
     */
    public long insert(T obj) throws DataAccessException {

        ContentValues values = getInsertContentValues(obj);

        SQLiteDatabase db = this.databaseHelper.getWritableDatabase();
        try {
            long result = db.insert(TableName, null, values);
            if (result == -1)
                throw new DataAccessException("insert failed for primary key:" + getPrimaryKey(obj));
            setPrimaryKey(obj, result);
            return result;
        } finally {
            //db.close();
        }
    }


    /**
     * Delete a row with the given rowId
     *
     * @param rowId identifier of the row
     * @throws DataAccessException          if delete fails
     */
    public void delete(long rowId) throws DataAccessException {
        SQLiteDatabase db = this.databaseHelper.getWritableDatabase();
        try {
            String whereClause = RowIdColumnName + "=?";
            String[] whereArgs = {String.valueOf(rowId)};
            boolean result = db.delete(TableName, whereClause, whereArgs) > 0;
            if (!result)
                throw new DataAccessException("delete failed:" + rowId);
        } finally {
            db.close();
        }
    }

    /**
     * Return a Cursor over the list of all cars in the database
     *
     * @param numberOfItems maximum number of items to return
     * @return array of all objects. It returns
     */
    public ArrayList<T> getAll(int numberOfItems) {

        if (!(numberOfItems >= 1 && numberOfItems <= 1000))
            throw new IllegalArgumentException("numberOfItems should be between 1 and 1000 inclusive");

        ArrayList<T> list = new ArrayList<>();

        SQLiteDatabase db = this.databaseHelper.getReadableDatabase();
        try {
            //Cursor cursor = db.query(TABLE_NAME, columnNames, filter, argumentValues, groupBy, having, orderBy, limit);
            Cursor cursor = db.query(this.TableName, null, null, null, null, null, null, String.valueOf(numberOfItems));
            if (cursor != null) {
                try {
                    while (cursor.moveToNext()) {
                        list.add(getObjectByCursor(cursor));
                    }
                } finally {
                    cursor.close();
                }
            }
        } finally {
            db.close();
        }
        return list;
    }

    /**
     * Return a Cursor positioned at the car that matches the given rowId
     *
     * @param rowId identifier of the row
     * @return object stored in the database or null
     */
    public T getById(long rowId) {

        SQLiteDatabase db = this.databaseHelper.getReadableDatabase();
        try {
            String whereClause = RowIdColumnName + "=?";
            String[] whereArgs = {String.valueOf(rowId)};

            //Cursor cursor = db.query(TableName, columnNames, whereClause, whereArgs, groupBy, having, orderBy);
            Cursor cursor = db.query(TableName, null, whereClause, whereArgs, null, null, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToNext()) {
                        T obj = getObjectByCursor(cursor);
                        return obj;
                    }
                } finally {
                    cursor.close();
                }
            }
        } finally {
            db.close();
        }

        return null;
    }




    /**
     * Update the object in the database
     *
     * @param obj object to update
     * @exception DataAccessException if update fails
     */
    public void update(T obj) throws DataAccessException {
        ContentValues values = getUpdateContentValues(obj);

        long rowId = getPrimaryKey(obj);

        String whereClause = RowIdColumnName + "=?";
        String[] whereArgs = {String.valueOf(rowId)};

        SQLiteDatabase db = this.databaseHelper.getWritableDatabase();
        try {
            boolean result = db.update(TableName, values, whereClause, whereArgs) > 0;
            if (!result)
                throw new DataAccessException("update failed:" + rowId);
        } finally {
            db.close();
        }
    }


    /**
     * Gets primary key for an object
     *
     * @param obj object
     * @return the primary key to be stored in a table (only one column primary key is allowed)
     */
    protected abstract long getPrimaryKey(T obj);


    /**
     * sets a primary key for an object
     *
     * @param obj the object
     */
    protected abstract void setPrimaryKey(T obj, long newPrimaryKey);


    /**
     * get content values for insert
     *
     * @param obj item to be converted
     * @return content values used with SQLite
     */
    @NonNull
    protected abstract ContentValues getInsertContentValues(T obj);

    /**
     * get content values for insert
     *
     * @param obj item to be converted
     * @return content values used with SQLite
     */
    @NonNull
    protected abstract ContentValues getUpdateContentValues(T obj);


    /**
     * Reads data from a cursor and converts it to the actual object
     *
     * @param cursor android Database Cursor
     * @return actual object
     */
    protected abstract T getObjectByCursor(Cursor cursor);


}
