package com.spectocor.micor.mobileapp.amain;

import android.content.Context;

import com.spectocor.micor.mobileapp.amain.sessionlist.SessionListSessionInfo;
import com.spectocor.micor.mobileapp.amain.state.CurrentSessionState;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;

/**
 * This class handles all that needs to be done once the session has started.
 *
 * Created by Savio Monteiro on 2/21/2016.
 */
public class SessionStarterKit {


    private static void log(String str) {
        MLog.log(SessionStarterKit.class.getSimpleName() + ": " + str);
    }

    /**
     * Initializes the database name associated with the newly created session and saves path to state file.
     * Also, resets the EcgChunkDbHelper instance to a new instance with new database name fetched from the state file.
     *
     * @param dbPath
     * @param sessionInfo
     * @param context
     */
    public static void initDatabaseNameAndPath(String dbPath, SessionListSessionInfo sessionInfo, Context context) {

        String dbPathAndName = StorageAndDbInfo.formulateDbNameAndPathBySessionInfo(dbPath, sessionInfo);

        CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(context);
        if(!sessionState.sessionActive) {
            throw new IllegalStateException("Session is not active. Not Initializing Database.");
        }
        if(sessionState == null) {
            throw new IllegalStateException("Session State is null. Not Initializing Database.");
        }

        log("Setting sessionState.dbPathAndName=" + dbPathAndName);

        sessionState.dbPathAndName = dbPathAndName;
        AMainApplication.DB_NAME_AND_PATH = dbPathAndName;

        String sessionStateJson = ObjectUtils.toJson(sessionState);
        if(sessionStateJson!=null) {
            SessionStateAccess.saveSessionStateJson(context, sessionStateJson);
        }

        DbHelper.getInstance(context).resetInstance(context);
    }


}
