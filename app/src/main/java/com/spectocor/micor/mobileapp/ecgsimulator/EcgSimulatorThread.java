package com.spectocor.micor.mobileapp.ecgsimulator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainService;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.ecgadapter.EcgResampler;
import com.spectocor.micor.mobileapp.ecgadapter.HeartRateHandler;
import com.spectocor.micor.mobileapp.ecgblehandler.EcgChannelBluetoothData;
import com.spectocor.micor.mobileapp.ecgblehandler.EcgBluetoothDataHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;


/**
 * Created by Savio Monteiro on 1/9/2016.
 * This simulator at best efforts will produce correct timestamps in seconds.
 * Chances are samples may not be sent at exactly each second but will always produce timestamps
 *  ... representing one second each.
 */
public class EcgSimulatorThread extends Thread {

    private static EcgSimulatorThread ecgSimulatorThread;
    private static Context mContext;

    private ArrayList<Short> samplesCh1 = new ArrayList<>();
    private ArrayList<Short> samplesCh2 = new ArrayList<>();

    private static boolean ENDED = false;

    public boolean isRunning = false;

    private static void log(String str) {
        MLog.log(EcgSimulatorThread.class.getSimpleName() + ": " + str);
    }

    public static EcgSimulatorThread getInstance(Context context) {

        mContext = context;

        if(ecgSimulatorThread == null) {
            ecgSimulatorThread = new EcgSimulatorThread();
            ecgSimulatorThread.initEcgSimulationData();
        }

        return ecgSimulatorThread;
    }

    public void endSimulation() {

        ENDED = true;

        ecgSimulatorThread = null;
    }



    private void initEcgSimulationData() {

        ENDED = false;

        int maxSamplesToRead = 5000;
        int i = 0;

        samplesCh1 = new ArrayList<>();
        try {
            Scanner scr = new Scanner(mContext.getAssets().open("ecg_csv_examples/ch1.csv"));
            while (scr.hasNextLine()) {
                samplesCh1.add((short) ((Float.parseFloat(scr.nextLine()) * 1/4) - 250));
                i++;
                if(i > maxSamplesToRead)
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        i = 0;

        samplesCh2 = new ArrayList<>();
        try {
            Scanner scr = new Scanner(mContext.getAssets().open("ecg_csv_examples/ch2.csv"));
            while (scr.hasNextLine()) {
                samplesCh2.add((short) ((Float.parseFloat(scr.nextLine()) * 1/4) - 250));
                i++;
                if(i > maxSamplesToRead)
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private int currOffset = 0;
    private final int SAMPLE_SIZE_PER_ITERATION = 400; //EcgProperties.SAMPLE_RATE;
    private boolean justConnected = true;

    public ArrayList<Short> getRandomOneSecondCh1() {

        ArrayList<Short> samples = new ArrayList<>();
        int offset = ThreadLocalRandom.current().nextInt(0, 3000 + 1);

        for(int i = offset; i < offset + EcgProperties.SAMPLE_RATE; i++)
        {
            samples.add(samplesCh1.get(i));
        }

        return samples;
    }

    public void run() {

        isRunning = true;

        long prevTimestampInSeconds = System.currentTimeMillis()/1000;

        try {
            sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while(true) {

            if(AMainService.APP_DEAD) {
                log("App is dead ... stopping simulation");
                isRunning = false;
                break;
            }

            if(ENDED) {
                log("End Called ... stopping simulation");
                isRunning = false;
                break;
            }

            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                log(e.getMessage());
            }

            long timestampInMillis = System.currentTimeMillis();
            long timestampInSeconds = timestampInMillis/1000;
            /*log("timestampInMillis = " + timestampInMillis);
            log("prevTimestampInSeconds = " + prevTimestampInSeconds);
            log("timestampInSeconds = " + timestampInSeconds);*/

            if((timestampInSeconds - prevTimestampInSeconds) == 0)
            {
                //log(".");
                continue;
            }
            else if((timestampInSeconds - prevTimestampInSeconds) < 0) {
                //log("..");
                continue;
            }

            //log("Finally timestampInSeconds = " + timestampInSeconds);

            prevTimestampInSeconds = timestampInSeconds;

            short[] ch1 = new short[SAMPLE_SIZE_PER_ITERATION];
            short[] ch2 = new short[SAMPLE_SIZE_PER_ITERATION];

            for(int i = 0; i < SAMPLE_SIZE_PER_ITERATION; i++) {

                ch1[i] = samplesCh1.get(currOffset);
                ch2[i] = samplesCh2.get(currOffset);

                currOffset++; // offset of input file

                if(currOffset >= samplesCh1.size()) { // offset of input file
                    currOffset = 0;
                }
            }

            short[] downSampledCh1 = EcgResampler.resample(400, EcgProperties.SAMPLE_RATE, ch1);

            // TODO: channel 1 is duplicated here ... replace with real ch2 when available
            short[] downSampledCh2 = EcgResampler.resample(400, EcgProperties.SAMPLE_RATE, ch2);


            log("downsampled ch1: " + downSampledCh1.length);
            log("downsampled ch2: " + downSampledCh2.length);

            EcgChannelBluetoothData ecgBtData = new EcgChannelBluetoothData(timestampInSeconds, downSampledCh1, downSampledCh2);

            EcgBluetoothDataHandler.getInstance(mContext).handleNewEcgData(ecgBtData);


            if (AMainService.sendToSignalView) {

                ArrayList<Integer> filtCh1 = HeartRateHandler.getInstance(mContext).applyFilter(downSampledCh1, 1);
                ArrayList<Integer> filtCh2 = HeartRateHandler.getInstance(mContext).applyFilter(downSampledCh2, 2);

                // JOINING BOTH BROADCASTS AND SENDING AS ONE

                log("Sending signal to signal view ... ");

                Bundle bb = new Bundle();
                bb.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SIGNAL_VIEW_LE_ECG_SAMPLES_READY);
                bb.putString("samplesCh1Json", new Gson().toJson(filtCh1));
                bb.putString("samplesCh2Json", new Gson().toJson(filtCh2));
                Intent ii = createBtEventIntent(bb);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(ii);
            }

        } // end-while
        isRunning = false;

    }


    private static Intent createBtEventIntent(Bundle b) {
        Intent i = new Intent("BLUETOOTH_EVENT");
        i.putExtras(b);
        return i;
    }

}
