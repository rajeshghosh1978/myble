package com.spectocor.micor.mobileapp.common.queuesystem;

import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;
import com.spectocor.micor.mobileapp.common.ApiResult;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.common.exceptions.UserException;
import com.spectocor.micor.mobileapp.common.exceptions.WebApiCallException;

import java.io.IOException;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Handles interactions with Https Server
 * Read https://github.com/square/okhttp/wiki/Recipes for more recipes
 */
public class HttpsHandler {

    /**
     * media type for Json values
     */
    private static final MediaType MEDIA_TYPE_JSON
            = MediaType.parse("application/json; charset=utf-8"); // MediaType.parse("text/x-markdown; charset=utf-8");

    /**
     * media type for binary values like files or ECG chunks
     */
    private static final MediaType MEDIA_TYPE_OCTET = MediaType.parse("application/octet-stream");

    /**
     * http client to be used for http calls
     */
    private final OkHttpClient client;

    /**
     * program settings
     */
    private final GlobalSettings settings;


    /**
     * constructor of http handler
     *
     * @param settings application settings
     */
    public HttpsHandler(GlobalSettings settings) {
        client = new OkHttpClient();
        this.settings = settings;
    }


    /**
     * gets url to call api names
     *
     * @param apiName api name, for example, ecgchunk/insert
     * @return url for Api (http://ourapipoint/ecgchunk/insert)
     */
    private String getApiUrl(String apiName) {
        return settings.getApiServerUrl() + apiName;
    }


    /**
     * posts a json value to json server
     *
     * @param apiName    name of api to call on the server. For example, devicelog/insert
     * @param object     body of the json object
     * @param returnType return type from Api
     * @return results as a string (it can be converted to json if necessary
     * @throws UserException if response was not successful, but has a notification error for user
     * @throws WebApiCallException if response was not successful because of server problems
     */
    public <T> T postJsonApi(String apiName, Object object, Class<T> returnType) throws UserException, WebApiCallException {

        ApiResult apiResult = postJsonApi(apiName, object);
        // exceptions handled in lower levels
//        if (!apiResult.getsuccess())
//            throw new UserException(apiResult.getmessage());
        T result = ObjectUtils.fromJson(apiResult.getdata(), returnType);
        return result;

    }

    /**
     * Calls api with http post
     * apiResult.getsuccess() should be checked
     *
     * @param apiName    name of api to call on the server. For example, devicelog/insert
     * @param jsonObject Object to be serialized as Json and send to the server.
     * @return Api call results
     * @throws UserException if response was not successful, but has a notification error for user
     * @throws WebApiCallException if response was not successful because of server problems
     */
    public ApiResult postJsonApi(String apiName, Object jsonObject) throws UserException, WebApiCallException {
        String url = getApiUrl(apiName);
        String postBody = ObjectUtils.toJson(jsonObject);
        String responseBody = postJsonString(url, postBody);

        ApiResult apiResult = ObjectUtils.fromJson(responseBody, ApiResult.class);
        if (!apiResult.getsuccess())
            throw new UserException(apiResult.getmessage());

        return apiResult;
    }

    /**
     * Posts a json string to the server
     *
     * @param url      url to post data
     * @param postBody post body in string
     * @return response body as a string
     * @throws WebApiCallException if http code is not OK.
     */
    private String postJsonString(String url, String postBody) throws WebApiCallException {

        Request request = new Request.Builder()
                .url(url)
                        //.header("Content-Type", "application/json")
                .post(RequestBody.create(MEDIA_TYPE_JSON, postBody))
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new WebApiCallException(url, response.code(), request.toString());
            return response.body().string();
        } catch (IOException ex) {
            throw new WebApiCallException(url, -1, ex.toString() + "\r\n" + request.toString());

        }
    }

    /**
     * calls post api call from the server
     *
     * @param apiName  api name for example, ecgchunk/insert
     * @param dataInfo object to be serialized az Json and send to the server
     * @param fileData byte array for data
     * @return Api result object
     * @throws WebApiCallException if Api call failed because of Http call problems or an exception happened
     * @throws UserException       if Api called successfully, but result was not correct
     */
    public ApiResult postFileApi(String apiName, Object dataInfo, byte[] fileData) throws WebApiCallException, UserException {
        String url = getApiUrl(apiName);
        String postBody = ObjectUtils.toJson(dataInfo);
        String responseBody = postFile(url, postBody, fileData);
        ApiResult apiResult = ObjectUtils.fromJson(responseBody, ApiResult.class);
        if (!apiResult.getsuccess())
            throw new UserException(apiResult.getmessage());
        return apiResult;
    }


    /**
     * posts the files to the server
     *
     * @param url      url to post the file
     * @param postBody post body to put file information
     * @param fileData binary information of the file
     * @return response body in String format
     * @throws WebApiCallException if Api couldn't be called or an exception happened
     */
    private String postFile(String url, String postBody, byte[] fileData) throws WebApiCallException {

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"dataInfo\""),
                        RequestBody.create(MEDIA_TYPE_JSON, postBody))
                .addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"fileData\""),
                        RequestBody.create(MEDIA_TYPE_OCTET, fileData))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new WebApiCallException(url, response.code(), request.toString());
            return response.body().string();
        } catch (IOException ex) {
            throw new WebApiCallException(url, -1, ex.toString() + "\r\n" + request.toString());
        }
    }


}
