package com.spectocor.micor.mobileapp.devicelogs;

import com.spectocor.micor.mobileapp.common.queuesystem.HttpsHandler;
import com.spectocor.micor.mobileapp.common.queuesystem.QueueHttpSenderBase;


/**
 * Syncs device logs with the server (Sends unsent device logs to the server and removes them from sending queue)
 */
public class DeviceLogQueueSender extends QueueHttpSenderBase<DeviceLog> {

    public static final String API_NAME = "devicelog/insert";

    public DeviceLogQueueSender(DeviceLogRepository repository,
                                DeviceLogQueueRepository sendingQueueRepository,
                                HttpsHandler httpsHandler) {
        super(repository, sendingQueueRepository, httpsHandler, API_NAME);
    }


}
