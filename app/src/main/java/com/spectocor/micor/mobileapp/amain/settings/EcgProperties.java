package com.spectocor.micor.mobileapp.amain.settings;

/**
 * Created by Savio Monteiro on 1/9/2016.
 */
public class EcgProperties {

    public static final int SAMPLE_RATE = 250;

    public static final int NUMBER_OF_CHANNELS = 2;

    public static final int CHANNEL_FOR_HEART_RATE = 1;

    public static final boolean COMPRESSION_FOR_TRANSMISSION_ENABLED = true;

    public static final int DEVICE_CHUNK_SIZE = 180000;

}
