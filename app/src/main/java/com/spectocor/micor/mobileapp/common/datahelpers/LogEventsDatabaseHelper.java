package com.spectocor.micor.mobileapp.common.datahelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.spectocor.micor.mobileapp.BuildConfig;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogRepository;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientReportedEventRepository;

public class LogEventsDatabaseHelper extends SQLiteOpenHelper {

    // inspired from
    // http://stackoverflow.com/questions/4063510/multiple-table-sqlite-db-adapters-in-android

    public static final String DatabaseName = "MicorLog";
    private static final int SchemaVersion = 1;
    protected final Context Context; // android context


    /**
     * The constructor.
     *
     * @param context Android application context.
     */
    public LogEventsDatabaseHelper(Context context) {
        super(context, DatabaseName, null, SchemaVersion);

        if (context == null)
            throw new IllegalArgumentException("context can not be null");

        Context = context;

        // force SQLite to create the actual database file on runtime if not there
        SQLiteDatabase db = this.getWritableDatabase();
        db.close();
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     * for data types refer to http://www.sqlite.org/datatype3.html
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DeviceLogRepository.CREATE_TABLE_SQL_SCRIPT);
        db.execSQL(PatientReportedEventRepository.CREATE_TABLE_SQL_SCRIPT);
    }

    /**
     * Called when the database needs to be downgraded. This is strictly similar to
     * {@link #onUpgrade} method, but is called whenever current version is newer than requested one.
     * However, this method is not abstract, so it is not mandatory for a customer to
     * implement it. If not overridden, default implementation will reject downgrade and
     * throws SQLiteException
     * <p/>
     * <p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,
                          int newVersion) {
        if (BuildConfig.DEBUG) {
            Context.deleteDatabase(DatabaseName);
        } else
            throw new UnsupportedOperationException();
    }


}
