package com.spectocor.micor.mobileapp.amain;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogService;
import com.spectocor.micor.mobileapp.http.HttpQueueManager;
import com.spectocor.micor.mobileapp.http.HttpQueueService;

/**
 * Created by Savio Monteiro on 2/26/2016.
 */
public class AppServiceManager {


    private static void log(String str) {
        MLog.log(AppServiceManager.class.getSimpleName() + ": " + str);
    }

    public static void startHttpService() {

        if(!SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext()).sessionActive) {

            log("Session Not Active. Not starting HttpService");

            return;
        }

        if(isServiceRunning(HttpQueueService.class))
        {
           log("Http Service already running");
        }
        else
        {
            Intent httpServiceIntent = new Intent(AMainApplication.getAppContext(), HttpQueueService.class);
            AMainApplication.getAppContext().startService(httpServiceIntent);
            HttpQueueManager.getInstance().initServiceConnection();
        }

    }


    public static void startAMainService() {

        if(!SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext()).sessionActive) {

            log("Session Not Active. Not starting AMainService.");

            return;
        }



        if(isServiceRunning(AMainService.class))
        {
            log("AMainService already running");
        }
        else
        {
            Intent aMainServiceIntent = new Intent(AMainApplication.getAppContext(), AMainService.class);
            AMainApplication.getAppContext().startService(aMainServiceIntent);
        }


    }


    public static void stopService(Class<?> className) {

        log("Stopping Service: " + className.getSimpleName());

        if(isServiceRunning(className)) {
            AMainApplication.getAppContext().stopService(new Intent(AMainApplication.getAppContext(), className));
        }
    }


    /**
     * To check if a service is running or not
     * @param serviceClass
     * @return
     */
    public static boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) AMainApplication.getAppContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static void startDeviceLogService() {

        if(isServiceRunning(DeviceLogService.class))
        {
            log("Device Log Service is already running");
        }
        else
        {
            Intent httpServiceIntent = new Intent(AMainApplication.getAppContext(), DeviceLogService.class);
            AMainApplication.getAppContext().startService(httpServiceIntent);

        }

    }


    public static void stopDeviceLogService() {

        if(!isServiceRunning(DeviceLogService.class))
        {
            log("Device Log Service is already running");
            Intent logServiceIntent = new Intent(AMainApplication.getAppContext(), DeviceLogService.class);
            AMainApplication.getAppContext().stopService(logServiceIntent);

        }
    }

}
