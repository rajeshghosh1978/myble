package com.spectocor.micor.mobileapp.devicelogs;

/**
 * Logs disk space information
 */
@SuppressWarnings("WeakerAccess")
public class DiskSpaceLogService {

    protected final DiskSpaceCalculator calculator;
    protected final DeviceLogLogger service;

    public DiskSpaceLogService(DiskSpaceCalculator diskSpaceCalculator,
                               DeviceLogLogger deviceLogLogger) {
        calculator = diskSpaceCalculator;
        service = deviceLogLogger;
    }

    /**
     * Logs internal storage free space
     *
     * @return inserted device log
     */
    public DeviceLog logInternalStorageFreeSpace() {
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.InternalStorageFreeSpace, calculator.getInternalFreeBytes());
        service.insertLog(obj);
        return obj;
    }

    /**
     * logs internal storage total space
     *
     * @return inserted device log
     */
    public DeviceLog logInternalStorageTotalSpace() {
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.InternalStorageTotalSpace, calculator.getInternalTotalBytes());
        service.insertLog(obj);
        return obj;
    }

    /**
     * logs insert SD card free space
     *
     * @return inserted device log
     */
    public DeviceLog logSdCardFreeSpace() {
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.SdCardFreeSpace, calculator.getSdCardFreeBytes());
        service.insertLog(obj);
        return obj;
    }


    /**
     * logs insert SD card total space
     *
     * @return inserted device log
     */
    public DeviceLog log() {
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.SdCardTotalSpace, calculator.getSdCardTotalBytes());
        service.insertLog(obj);
        return obj;
    }


}
