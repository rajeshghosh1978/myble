package com.spectocor.micor.mobileapp.amain.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.spectocor.micor.mobileapp.amain.AMainActivity;

/**
 * Created by Savio Monteiro on 2/14/2016.
 */
public class SessionTerminationAlarmReceiver extends BroadcastReceiver {


    /**
     * Callback to show pending notification next morning
     * because session terminated at night.
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        AlertPlayer.turnScreenOn(context);
        AlertPlayer.playVibrateAlert(context);
        AlertPlayer.playAudioAlert(context);

        LocalBroadcastManager.getInstance(context).sendBroadcast(AMainActivity.createBTIntent(AMainActivity.DISPLAY_SESSION_TERMINATION_SCREEN, new Bundle()));

    }
}
