package com.spectocor.micor.mobileapp.sessionactivation.api;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.http.HttpManager;
import com.spectocor.micor.mobileapp.http.common.ResponseCodes;
import com.spectocor.micor.mobileapp.patientreportedevents.EnumPatientActivityEventType;
import com.spectocor.micor.mobileapp.patientreportedevents.EnumPatientReportedEventType;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientEvent;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientReportedEventsDbHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Api for reporting patient events (with and without symptoms)
 *
 * Created by Rafay Ali on 2/19/2016.
 */
public class PatientReportedEventsApi {

    public static final String TAG = PatientReportedEventsApi.class.getSimpleName();

    private static PatientReportedEventsApi instance = null;

    private PatientReportedEventsApi(){}

    public static PatientReportedEventsApi getInstance(){
        if (instance == null){
            instance = new PatientReportedEventsApi();
        }

        return instance;
    }

    public void storePteWithoutSymtoms() {

        try {
            String activeSessionID = SessionStateAccess.getCurrentSessionId(AMainApplication.getAppContext()) + "";

            JSONObject logJson = new JSONObject();
            logJson.put("eventType", "101");
            logJson.put("activityType", "4");
            logJson.put("eventTimestamp", System.currentTimeMillis() + "");

            JSONArray logJsonArray = new JSONArray();
            logJsonArray.put(logJson);

            PatientEvent patientEvent = new PatientEvent();
            patientEvent.setSessionId(Long.parseLong(activeSessionID));
            patientEvent.setInsertTimestamp(System.currentTimeMillis());
            patientEvent.setTransmittedTimestamp(-1);
            patientEvent.setData(logJsonArray.toString());

            PatientReportedEventsDbHelper.getInstance().insertPatientEvent(patientEvent);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void reportWithoutSymptoms() {

        //String activeSessionID =  preferenceHelper.getActiveSessionId();
        String activeSessionID = SessionStateAccess.getCurrentSessionId(AMainApplication.getAppContext()) + "";

        PatientEvent patientEvent = new PatientEvent();
        android.util.Log.d(TAG, "reportWithoutSymptoms: "+activeSessionID);
        try {
            JSONObject logJson = new JSONObject();
            logJson.put("eventType", "101");
            logJson.put("activityType", "4");
            logJson.put("eventTimestamp", System.currentTimeMillis() + "");
//            logJson.put("eventMessage", "No Symptoms");

            JSONArray logJsonArray = new JSONArray();
            logJsonArray.put(logJson);

            patientEvent.setSessionId(Long.parseLong(activeSessionID));
            patientEvent.setInsertTimestamp(System.currentTimeMillis());
            patientEvent.setTransmittedTimestamp(-1);
            patientEvent.setData(logJsonArray.toString());

            Log.d(TAG, "sessionId: " + Integer.parseInt(activeSessionID + ""));
            Log.d(TAG, "log:" + logJsonArray.toString());
            Log.d(TAG, "ecgId: " + DeviceIdentifiers.getEcgId());
            Log.d(TAG, "deviceId: " + DeviceIdentifiers.getPdaId());

            RequestBody requestBody = new MultipartBody.Builder()
                    .addFormDataPart("sessionId", Integer.parseInt(activeSessionID)+"")
                    .addFormDataPart("log", logJsonArray.toString())
                    .addFormDataPart("ecgId", DeviceIdentifiers.getEcgId())
                    .addFormDataPart("deviceId", DeviceIdentifiers.getPdaId())
                    .setType(MultipartBody.FORM)
                    .build();

            Request request = new Request.Builder()
                    .post(requestBody)
                    .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_PATIENT_LOG)
                    .build();

            Response response = HttpManager.getInstance().getHttpClient().newCall(request).execute();
            String responseBody = response.body().string();
            JSONObject responseJSON = new JSONObject(responseBody);
            int responseCode = responseJSON.getInt("code");
            Log.v(TAG, "StatusCode : " + responseCode + " ResponseBody: " + responseBody);
            Log.v(TAG, "EcgId: " + DeviceIdentifiers.getEcgId());
            Log.v(TAG, "DeviceId: " + DeviceIdentifiers.getPdaId());

            if (responseCode == ResponseCodes.OK){
                Log.v(TAG, "Response has been successful, marking entry in database as transmitted.");
                patientEvent.setTransmittedTimestamp(System.currentTimeMillis());
            }
            else if (responseCode == ResponseCodes.DEVICE_NO_LONGER_ACTIVE){
                Intent intent = new Intent("BLUETOOTH_EVENT");
                intent.putExtra("EVENT_TYPE", AMainActivity.SESSION_DEVICE_NO_LONGER_ACTIVE);
                LocalBroadcastManager.getInstance(AMainApplication.getAppContext()).sendBroadcast(intent);
            }

            PatientReportedEventsDbHelper.getInstance().insertPatientEvent(patientEvent);
        } catch (JSONException e) {
            Log.v(TAG, "Error while serializing patient reported event to JSON!");
            e.printStackTrace();
        } catch (IOException e) {
            PatientReportedEventsDbHelper.getInstance().insertPatientEvent(patientEvent);
            e.printStackTrace();
        }
    }

    public void storePteWithSymptoms(String symptoms) {

        String activeSessionID =  SessionStateAccess.getCurrentSessionId(AMainApplication.getAppContext()) + "";

        JSONArray logsArray = new JSONArray();
        PatientEvent patientEvent = new PatientEvent();

        // PARSING
        try {
            String[] totalDiagnosticSplit = symptoms.split(";");
            String[] symptomsSplit = totalDiagnosticSplit[0].split(",");

            for (int i = 0; i < symptomsSplit.length; i++) {
                JSONObject log = new JSONObject();
                int symptomValue = EnumPatientReportedEventType.valueOf(symptomsSplit[i].replace(" ", "")).getValue();
                log.put("eventType", symptomValue + "");
                if (totalDiagnosticSplit.length > 1) {
                    log.put("activityType", EnumPatientActivityEventType.valueOf(totalDiagnosticSplit[1].replace(" ", "")).getValue());
                } else
                    log.put("activityType", "4");
                log.put("eventTimestamp", System.currentTimeMillis());
                logsArray.put(log);
            }

            Log.v(TAG, "Log array in string: " + logsArray.toString());

            patientEvent.setSessionId(Long.parseLong(activeSessionID));
            patientEvent.setInsertTimestamp(System.currentTimeMillis());
            patientEvent.setTransmittedTimestamp(-1);
            patientEvent.setData(logsArray.toString());

            Log.d(TAG, "sessionId: " + Integer.parseInt(activeSessionID + ""));
            Log.d(TAG, "log:" + logsArray.toString());
            Log.d(TAG, "ecgId: " + DeviceIdentifiers.getEcgId());
            Log.d(TAG, "deviceId: " + DeviceIdentifiers.getPdaId());

            Log.v(TAG, "Inserting PTE into database: rowid=" + PatientReportedEventsDbHelper.getInstance().insertPatientEvent(patientEvent) + "");
        }
        catch (JSONException e) {
            Log.v(TAG, "Error while parsing JSON, unable to send patient reported event to server!");
            e.printStackTrace();
        }

    }

    public void reportWithSymptoms(String symptoms){

        String activeSessionID =  SessionStateAccess.getCurrentSessionId(AMainApplication.getAppContext()) + "";

        JSONArray logsArray = new JSONArray();
        PatientEvent patientEvent = new PatientEvent();

        // PARSING
        try {
            String[] totalDiagnosticSplit = symptoms.split(";");
            String[] symptomsSplit = totalDiagnosticSplit[0].split(",");

            for (int i = 0; i < symptomsSplit.length; i++) {
                JSONObject log = new JSONObject();
                int symptomValue = EnumPatientReportedEventType.valueOf(symptomsSplit[i].replace(" ", "")).getValue();
                log.put("eventType", symptomValue + "");
                if (totalDiagnosticSplit.length > 1) {
                    log.put("activityType", EnumPatientActivityEventType.valueOf(totalDiagnosticSplit[1].replace(" ", "")).getValue());
                } else
                    log.put("activityType", "4");
                log.put("eventTimestamp", System.currentTimeMillis());
//                log.put("eventMessage", "1");
                logsArray.put(log);
            }

            Log.v(TAG, "Log array in string: " + logsArray.toString());

            patientEvent.setSessionId(Long.parseLong(activeSessionID));
            patientEvent.setInsertTimestamp(System.currentTimeMillis());
            patientEvent.setTransmittedTimestamp(-1);
            patientEvent.setData(logsArray.toString());

            Log.d(TAG, "sessionId: " + Integer.parseInt(activeSessionID + ""));
            Log.d(TAG, "log:" + logsArray.toString());
            Log.d(TAG, "ecgId: " + DeviceIdentifiers.getEcgId());
            Log.d(TAG, "deviceId: " + DeviceIdentifiers.getPdaId());


            // TRANSMITTING
            RequestBody requestBody = new MultipartBody.Builder()
                    .addFormDataPart("sessionId", activeSessionID)
                    .addFormDataPart("log", logsArray.toString())
                    .addFormDataPart("ecgId", DeviceIdentifiers.getEcgId())
                    .addFormDataPart("deviceId", DeviceIdentifiers.getPdaId())
                    .setType(MultipartBody.FORM)
                    .build();

            Request request = new Request.Builder()
                    .post(requestBody)
                    .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_PATIENT_LOG)
                    .build();

            Response response = HttpManager.getInstance().getHttpClient().newCall(request).execute();
            String responseBody = response.body().string();
            JSONObject responseJSON = new JSONObject(responseBody);
            int responseCode = responseJSON.getInt("code");

            Log.v(TAG, "StatusCode : " + responseCode + " ResponseBody: " + responseBody);
            Log.v(TAG, "EcgId: " + DeviceIdentifiers.getEcgId());
            Log.v(TAG, "DeviceId: " + DeviceIdentifiers.getPdaId());

            if (responseCode == ResponseCodes.OK) {
                Log.v(TAG, "Response has been successful, marking entry in database as transmitted.");
                patientEvent.setTransmittedTimestamp(System.currentTimeMillis());
            }
            else if (responseCode == ResponseCodes.DEVICE_NO_LONGER_ACTIVE){
                Intent intent = new Intent("BLUETOOTH_EVENT");
                intent.putExtra("EVENT_TYPE", AMainActivity.SESSION_DEVICE_NO_LONGER_ACTIVE);
                LocalBroadcastManager.getInstance(AMainApplication.getAppContext()).sendBroadcast(intent);
            }

            Log.v(TAG, "Inserting PTE into database: rowid=" + PatientReportedEventsDbHelper.getInstance().insertPatientEvent(patientEvent) + "");
        }
        catch (IOException e) {
            // store in database with transmissionTimestamp=-1 for any IOException
            long rowId = PatientReportedEventsDbHelper.getInstance().insertPatientEvent(patientEvent);
            Log.v(TAG, "Inserting PTE into database: rowid=" + rowId + "");
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.v(TAG, "Error while parsing JSON, unable to send patient reported event to server!");
            e.printStackTrace();
        }
    }
}
