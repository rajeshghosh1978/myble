package com.spectocor.micor.mobileapp.common.queuesystem;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.spectocor.micor.mobileapp.common.RepositoryBase;
import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;


/**
 * Base class for Queue repositories
 * Each repository has one table associated. It
 */
public abstract class SendingQueueRepositoryBase extends RepositoryBase<QueueItemWithStatus> {

    public static final String COLUMN_ROW_ID = "RowId"; // row identifier
    //public static final String COLUMN_NETWORK_STATUS = "NetworkStatus";//0 for connected or 1 for notconnected
    //public static final String COLUMN_TIME = "InsertionTime";
    protected QueueDatabaseHelper queueDatabaseHelper;

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param queueDatabaseHelper database helper
     */
    public SendingQueueRepositoryBase(QueueDatabaseHelper queueDatabaseHelper, String tableName) {
        super(queueDatabaseHelper, tableName, COLUMN_ROW_ID);

        this.queueDatabaseHelper = queueDatabaseHelper;
    }

    public String getCreateTableSqlScript() {
        // There should be no primary key here. enqueue inserts a row in the database, and dequeue removes the row, select gets top x rows from table
        return "create table " + this.TableName + " ("
                + COLUMN_ROW_ID + " INTEGER PRIMARY KEY "
                //+ COLUMN_NETWORK_STATUS + " INTEGER ,"
                //+ COLUMN_TIME + " DATETIME DEFAULT CURRENT_TIME "
                + ");";
    }

    /**
     * Reads data from a cursor and converts it to the actual object
     *
     * @param cursor android Database Cursor
     * @return actual object
     */
    protected QueueItemWithStatus getObjectByCursor(Cursor cursor) {
        return new QueueItemWithStatus(cursor.getLong(cursor.getColumnIndex(COLUMN_ROW_ID)));
       /* return new QueueItemWithStatus(cursor.getLong(cursor.getColumnIndex(COLUMN_ROW_ID)),cursor.getInt(cursor.getColumnIndex(COLUMN_NETWORK_STATUS))
        ,cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));*/
    }


    /**
     * get content values for insert
     *
     * @param obj item to be converted
     * @return content values used with SQLite
     */
    @NonNull
    private ContentValues getContentValues(QueueItemWithStatus obj) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ROW_ID, obj.getRowId());
        //values.put(COLUMN_NETWORK_STATUS, obj.getRowNetworkStatus());
        //values.put(COLUMN_TIME, obj.getRowTime());
        return values;
    }


    @Override
    protected long getPrimaryKey(QueueItemWithStatus obj) {
        return obj.getRowId();
    }

    @Override
    protected void setPrimaryKey(QueueItemWithStatus obj, long newPrimaryKey) {
        obj.setRowId(newPrimaryKey);
    }

    @NonNull
    @Override
    protected ContentValues getInsertContentValues(QueueItemWithStatus obj) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ROW_ID, obj.getRowId());
        //values.put(COLUMN_NETWORK_STATUS, obj.getRowNetworkStatus());
        //values.put(COLUMN_TIME, obj.getRowTime());
        return values;
    }

    @NonNull
    @Override
    protected ContentValues getUpdateContentValues(QueueItemWithStatus obj) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ROW_ID, obj.getRowId());
        //values.put(COLUMN_NETWORK_STATUS, obj.getRowNetworkStatus());
       // values.put(COLUMN_TIME, obj.getRowTime());
        return values;
    }

}
