package com.spectocor.micor.mobileapp.common.queuesystem;

// Developer Note: This class is a little bit over engineering, but since queues are not going to
// be very big for retrieval and change, I decided to keep the design of repositories consistent
// In addition, we may add failure count and remove items with many failures or keep last try
// and remove very old data from the queue later.


/**
 * Queue item used in queue repository
 */
public class QueueItemWithStatus {

    /**
     * row identifier in the actual repository class
     */
    private long rowId;
    private int rownetworkStatus;
    private String rowTime;


    public QueueItemWithStatus(long rowId) {
        this.rowId = rowId;
        //this.rownetworkStatus=rownetworkStatus;
        //this.rowTime=time;
    }

    public long getRowId() {
        return rowId;
    }

    public void setRowId(long newValue) {
        rowId = newValue;
    }

    public int getRowNetworkStatus() {
        return rownetworkStatus;
    }

    public void setRowNetworkStatus(int newValue) {
        rownetworkStatus = newValue;
    }

    public String getRowTime() {
        return rowTime;
    }

    public void setRowTime(String newValue) {
        rowTime = newValue;
    }
}
