package com.spectocor.micor.mobileapp.sessionactivation.api.requests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

@SuppressWarnings("unused")
public class GetListOfEnrolledPatientsRequest {
    /**
     * Enrollment date in ISO 8601 format (simply means yyyy-MM-dd. For example, 2015-12-15)
     * Here we keep no time. (read more here: https://en.wikipedia.org/wiki/ISO_8601)
     */
    private final String enrollmentDateIso8601;
    private final String facilityCode;

    /**
     * @param enrollmentDateIso8601 The Enrollment Date for lookup of patient
     * @param facilityCode          The Facility ID of the Facility being searched
     */
    public GetListOfEnrolledPatientsRequest(String enrollmentDateIso8601, String facilityCode) throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        formatter.parse(enrollmentDateIso8601); // just tries to parse the date with valid format

        this.enrollmentDateIso8601 = enrollmentDateIso8601;
        this.facilityCode = facilityCode;
    }

    public String getEnrollmentDateIso8601() {
        return enrollmentDateIso8601;
    }

    public String getFacilityCode() {
        return facilityCode;
    }
}
