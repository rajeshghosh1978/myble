package com.spectocor.micor.mobileapp.common.datahelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.spectocor.micor.mobileapp.BuildConfig;

/**
 * Data helper to store Ecg data
 */
public class EcgDatabaseHelper  extends SQLiteOpenHelper {

    // inspired from
    // http://stackoverflow.com/questions/4063510/multiple-table-sqlite-db-adapters-in-android

    /**
     * database path
     */
    public static final String DB_PATH = "/storage/emulated/0/data/com.spectocor.micor.mobileapp/databases/";
    //Environment.getExternalStorageDirectory().toString()+"/data/"+context.getPackageName()+"/databases/";

    /**
     * database name (SQLite name)
     */
    public static final String DatabaseName = "MicorEcg";
    /**
     * version of schema
     */
    private static final int SchemaVersion = 1;
    /**
     * Android application context
     */
    protected final Context Context;


    /**
     * The constructor.
     *
     * @param context Android application context.
     */
    public EcgDatabaseHelper(Context context) {
        super(context, DatabaseName, null, SchemaVersion);

        if (context == null)
            throw new IllegalArgumentException("context can not be null");


        Context = context;


        // force SQLite to create the actual database file on runtime if not there
        SQLiteDatabase db = this.getWritableDatabase();
        //SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(DB_PATH + File.separator + DatabaseName, null);
        db.close();
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     * for data types refer to http://www.sqlite.org/datatype3.html
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(EcgDataChunkRepository.CREATE_TABLE_SQL_SCRIPT);
    }

    /**
     * Called when the database needs to be downgraded. This is strictly similar to
     * onCreate method, but is called whenever current version is newer than requested one.
     * However, this method is not abstract, so it is not mandatory for a customer to
     * implement it. If not overridden, default implementation will reject downgrade and
     * throws SQLiteException
     * <p/>
     * <p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,
                          int newVersion) {
        if (BuildConfig.DEBUG) {
            Context.deleteDatabase(DatabaseName);
        } else
            throw new UnsupportedOperationException();
    }


}
