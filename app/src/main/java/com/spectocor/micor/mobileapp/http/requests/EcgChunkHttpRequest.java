package com.spectocor.micor.mobileapp.http.requests;

import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.http.HttpRequestBase;
import com.spectocor.micor.mobileapp.http.HttpRequestHandler;

/**
 * Http request for Ecg chunk data.
 *
 * Created by Rafay Ali on 1/21/2016.
 */
public class EcgChunkHttpRequest extends HttpRequestBase {

    private String chunkFileName = "";
    private EcgDataChunk ecgChunkData = null;

    public EcgChunkHttpRequest(){

    }

    public EcgChunkHttpRequest(String fileName){
        chunkFileName = fileName;
    }

    @Override
    public void onQueue() {

    }

    @Override
    public void onCancelled() {

    }

    @Override
    public int getRequestType() {
        return HttpRequestHandler.ECG_CHUNK_REQUEST;
    }

    public String getChunkFileName() {
        return chunkFileName;
    }

    public void setChunkFileName(String chunkFileName) {
        this.chunkFileName = chunkFileName;
    }

    public void setEcgChunkData(EcgDataChunk ecgChunkData) {
        this.ecgChunkData = ecgChunkData;
    }

    public EcgDataChunk getEcgChunkData() {
        return ecgChunkData;
    }
}
