package com.spectocor.micor.mobileapp.sessionactivation.api;

import android.os.Build;

import com.spectocor.micor.mobileapp.common.ApiResult;
import com.spectocor.micor.mobileapp.common.exceptions.UserException;
import com.spectocor.micor.mobileapp.common.exceptions.WebApiCallException;
import com.spectocor.micor.mobileapp.common.queuesystem.HttpsHandler;
import com.spectocor.micor.mobileapp.sessionactivation.api.requests.ActivateSessionRequest;
import com.spectocor.micor.mobileapp.sessionactivation.api.requests.GetEnrollmentByActivationCodeRequest;
import com.spectocor.micor.mobileapp.sessionactivation.api.requests.GetFacilityNameByFacilityCodeRequest;
import com.spectocor.micor.mobileapp.sessionactivation.api.requests.GetListOfEnrolledPatientsRequest;

import java.text.ParseException;

/**
 * Api Calls made to the server for Session Activation.
 * For more information about these calls refer Confluence page at
 * ... http://project.spectocor.com:8090/confluence/display/SESS/Session+Activation+Table+Data+Elements
 * <p/>
 * Stub created by Savio Monteiro on 12/23/2015. Modified by Mohammad Ali Ghaderi with real API calls.
 */
public class SessionActivationApiOld {

    private static final String GET_ENROLLMENT_BY_ACTIVATION_CODE_API = "activation/getEnrollmentByActivationCode";
    private static final String GET_FACILITY_NAME_BY_FACILITY_ID_API = "activation/getFacilityNameByFacilityCode";
    private static final String GET_LIST_OF_ENROLLED_PATIENTS_API = "activation/getListOfEnrolledPatients";
    private static final String ACTIVATE_SESSION_API = "activation/activateSession";

    /**
     * http handler to connect to the server
     */
    private final HttpsHandler httpsHandler;

    /**
     * constructor for Api calls
     *
     * @param httpsHandler http handler to call Apis
     */
    public SessionActivationApiOld(HttpsHandler httpsHandler) {
        if (httpsHandler == null)
            throw new IllegalArgumentException("httpsHandler shouldn't be null.");

        this.httpsHandler = httpsHandler;
    }


    /**
     * Fetch Enrollment By Activation Code
     *
     * @param activationCode Activation Code of the Patient generated on Enrollment Form
     * @throws UserException            Exception to be shown to the user
     * @throws WebApiCallException      Exception when calling web api is failed
     * @return All Enrollment Information
     */
    public EnrollmentInfo getEnrollmentByActivationCode(String activationCode) throws UserException, WebApiCallException {
        GetEnrollmentByActivationCodeRequest request = new GetEnrollmentByActivationCodeRequest(activationCode);
        return httpsHandler.postJsonApi(GET_ENROLLMENT_BY_ACTIVATION_CODE_API, request, EnrollmentInfo.class);
    }


    /**
     * Fetch Facility Name By Facility Code.
     * <p/>
     * (
     * If chances are high that the user selects "today".
     * So, One request can be saved by also returning All Enrolled Patients for the current (today) date,
     * along with the Facility Information and storing in buffer. The app can then check when
     * enrollment date is selected and simply use this information instead of making another
     * call to fetch all enrollments for today.
     * )
     *
     *
     * @param facilityCode   Unique code for facility
     * @throws UserException            Exception to be shown to the user
     * @throws WebApiCallException      Exception when calling web api is failed
     * @return Facility Name (optionally current date's enrollments for facility code)
     */
    public String getFacilityNameByFacilityCode(String facilityCode) throws UserException, WebApiCallException {
        String mobileId = Build.SERIAL;
        GetFacilityNameByFacilityCodeRequest request = new GetFacilityNameByFacilityCodeRequest(facilityCode, mobileId);
        return httpsHandler.postJsonApi(GET_FACILITY_NAME_BY_FACILITY_ID_API, request, String.class);
    }

    /**
     * Get the list of patients enrolled for a facility on a particular date.
     *
     * @param enrollmentDateIso8601    The Enrollment Date for lookup of patient
     * @param facilityCode        The Facility ID of the Facility being searched
     * @throws UserException            Exception to be shown to the user
     * @throws WebApiCallException      Exception when calling web api is failed
     * @return List of enrollments
     */

    public EnrollmentInfo[] getListOfEnrolledPatients(String enrollmentDateIso8601, String facilityCode) throws UserException, WebApiCallException {
        try {
            GetListOfEnrolledPatientsRequest request = new GetListOfEnrolledPatientsRequest(enrollmentDateIso8601, facilityCode);
            return httpsHandler.postJsonApi(GET_LIST_OF_ENROLLED_PATIENTS_API, request, EnrollmentInfo[].class);
        } catch (ParseException ex) {
            throw new UserException("Date format is not correct");
        }
    }


    /**
     * Handshake with server to activate a session and start monitoring the patient's ECG.
     *
     * @param sessionId                 The Session ID of the patient generated at enrollment.
     * @param sessionActivationDateTime Date & Time captured when the patient confirms Patient Name and ECG device.
     * @param ecgBluetoothMac           The MAC address of the ECG device
     * @param ecgBluetoothName          Friendly name of the ECG device
     * @param facilityCode              Code of the facility that the patient is enrolled in
     * @throws UserException            Exception to be shown to the user
     * @throws WebApiCallException      Exception when calling web api is failed
     * @return activation message from the server
     */
    public String activateSession(int sessionId, long sessionActivationDateTime, String ecgBluetoothName, String ecgBluetoothMac, String facilityCode) throws UserException, WebApiCallException {
        String mobileId = Build.SERIAL;
        ActivateSessionRequest request = new ActivateSessionRequest(sessionId, sessionActivationDateTime, ecgBluetoothName, ecgBluetoothMac, mobileId, facilityCode);
        ApiResult result = httpsHandler.postJsonApi(ACTIVATE_SESSION_API, request);
        return result.getmessage();
    }


}
