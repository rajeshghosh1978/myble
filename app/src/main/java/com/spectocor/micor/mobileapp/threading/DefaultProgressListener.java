package com.spectocor.micor.mobileapp.threading;

/**
 * Created by Umair Ahmed on 2/25/2016.
 */
public interface DefaultProgressListener {

    public void onProgress(Object value);
}
