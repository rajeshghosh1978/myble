package com.spectocor.micor.mobileapp.utils.arrays;

public class ToByteArray {

    public static byte[] fromShortArray(short[] input) {

        byte[] bytes = new byte[input.length * Short.SIZE / 8];

        for (int i = 0, j = 0; i < input.length; i++) {

            bytes[j] = (byte) (input[i] & 0xff);
            j++;

            bytes[j] = (byte) ((input[i] >> 8) & 0xff);
            j++;
        }

        return bytes;
    }

}
