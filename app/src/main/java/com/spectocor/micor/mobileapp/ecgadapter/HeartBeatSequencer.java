package com.spectocor.micor.mobileapp.ecgadapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;

import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;

/**
 * Created by Savio Monteiro on 2/11/2016.
 */
public class HeartBeatSequencer extends Handler {


    private static HeartBeatSequencer heartBeatSequencer;
    private static HandlerThread heartBeatSequencerThread;
    private static boolean heartBeatSequencerThreadRunning = false;

    private static Context mContext;

    private static final int NEW_HEART_BEAT = 1;

    private HeartBeatSequencer(Looper looper) {
        super(looper);
    }

    public static HeartBeatSequencer getInstance(Context context) {
        if(heartBeatSequencer == null) {
            if(heartBeatSequencerThread == null) {
                heartBeatSequencerThread = new HandlerThread("HeartBeatSequencerThread");
            }

            if(!heartBeatSequencerThreadRunning)
            {
                heartBeatSequencerThread.start();
                heartBeatSequencerThreadRunning = true;
            }

            heartBeatSequencer = new HeartBeatSequencer(heartBeatSequencerThread.getLooper());
        }

        mContext = context;

        return heartBeatSequencer;
    }

    /**
     * Stop Handler Thread
     */
    public void terminate() {
        if(heartBeatSequencerThread != null) {

            log("Terminating heartbeatsequencerthread ... ");

            heartBeatSequencerThread.quit();
            heartBeatSequencerThreadRunning = false;
            heartBeatSequencerThread = null;
            heartBeatSequencer = null;
        }
    }

    /**
     * Called when R-Peak is detected
     * @param samplesSinceLastRPeak
     */
    protected synchronized void onNewHeartBeatDetected(long samplesSinceLastRPeak) {
        obtainMessage(NEW_HEART_BEAT, samplesSinceLastRPeak).sendToTarget();
    }

    private void log(String str) {
            MLog.log("HeartBeatSequencer: " + str);
    }

    /**
     * Compute time to sleep based on number of samples
     * ... since last R peak detected
     *
     * @param samplesSinceLastR
     * @param SAMPLE_RATE
     * @return
     */
    private long computeDelta(final long samplesSinceLastR, final int SAMPLE_RATE) {

        long delta = (long) ((1000f * (float) samplesSinceLastR) / (float) SAMPLE_RATE);

        return delta;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);

        switch (msg.what) {
            case NEW_HEART_BEAT: {

                long samplesSinceLastR = (long) msg.obj;
                long delta = computeDelta(samplesSinceLastR, EcgProperties.SAMPLE_RATE);

                log("Sleeping for " + delta + " ms before next heart beat!");

                try {
                    Thread.sleep(delta);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                log("Beating the heart");



                // BEATING THE HEART IN UI
                Intent heartBeatIntent = new Intent("BLUETOOTH_EVENT");
                heartBeatIntent.putExtra("EVENT_TYPE", AMainActivity.WEBVIEW_SIGNAL_VIEW_LE_DEVICE_POUND_THE_HEART);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(heartBeatIntent);

                return;
            }
        }
    }
}
