package com.spectocor.micor.mobileapp.http;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.http.common.ResponseCodes;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientEvent;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientReportedEventsDbHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Handler for sending buffered data to network.
 *
 * Created by Rafay Ali on 2/16/2016.
 */
public class HttpPteBufferedRequestHandler extends Handler{

    public static final String TAG = HttpPteBufferedRequestHandler.class.getSimpleName();

    private boolean msgBeingHandled = false;

    public HttpPteBufferedRequestHandler(Looper looper){
        super(looper);
    }

    @Override
    public void handleMessage(Message msg) {
        msgBeingHandled = true;
        Log.v(TAG, "Message is now being handled...");

        try {
            PatientEvent patientEvent;

            while (true) {
                patientEvent = PatientReportedEventsDbHelper.getInstance().getOldestPatientEventNotYetTransmitted();

                if (patientEvent == null){
                    Log.v(TAG, "PatientEvent is null, we assume there are no remaining entries in db to be send, finishing loop.");
                    break;
                }

                RequestBody requestBody = new MultipartBody.Builder()
                        .addFormDataPart("sessionId", ""+SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext()).getSessionId())
                        .addFormDataPart("log", new JSONArray(patientEvent.getData()).toString())
                        .addFormDataPart("ecgId", DeviceIdentifiers.getEcgId())
                        .addFormDataPart("deviceId", DeviceIdentifiers.getPdaId())
                        .setType(MultipartBody.FORM)
                        .build();

                Request request = new Request.Builder()
                        .post(requestBody)
                        .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_PATIENT_LOG)
                        .build();

                Response response = HttpManager.getInstance().getHttpClient().newCall(request).execute();
                String responseBody = response.body().string();
                JSONObject responseJSON = new JSONObject(responseBody);
                int responseCode = responseJSON.getInt("code");

                Log.v(TAG, "StatusCode : " + responseCode + " ResponseBody: " + responseBody);

                if (responseCode == ResponseCodes.OK){
                    Log.v(TAG, "Response has been successful, marking entry in database as transmitted.");
                }

                PatientReportedEventsDbHelper.getInstance().updatePatientEventAsTransmitted(patientEvent);

            }
        } catch (JSONException e) {
            Log.v(TAG, "Error while converting data to JSONArray!");
            e.printStackTrace();
        } catch (IOException e) {
            Log.v(TAG, "Error while sending buffered ecg chunk over the network!");
            e.printStackTrace();
        }

        Log.v(TAG, "Message is not longer being handled! ");
        msgBeingHandled = false;
    }

    public boolean isMsgBeingHandled() {
        return msgBeingHandled;
    }
}
