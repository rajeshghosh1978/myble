package com.spectocor.micor.mobileapp.devicelogs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Umair on 3/3/2016.
 */
public class Logs {

    @SerializedName("logType")
    private Integer type;

    @SerializedName("logTimestamp")
    private long eventTimestamp;

    @SerializedName("logNumber")
    private String logNumber;


    @SerializedName("logMessage")
    @Expose
    private String description;

    /**
     * @return The type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return The eventTimestamp
     */
    public long getEventTimestamp() {
        return eventTimestamp;
    }

    /**
     * @param eventTimestamp The eventTimestamp
     */
    public void setEventTimestamp(long eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }


    public String getLogNumber() {
        return logNumber;
    }

    public void setLogNumber(String logNumber) {
        this.logNumber = logNumber;
    }



}
