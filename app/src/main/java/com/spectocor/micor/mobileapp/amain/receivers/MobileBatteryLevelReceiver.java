package com.spectocor.micor.mobileapp.amain.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.MLog;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Savio Monteiro on 2/1/2016.
 */
public class MobileBatteryLevelReceiver extends BroadcastReceiver {

    public static final String MOBILE_BATT_BROADCAST_ACTION = "com.spectocor.micor.mobileapp.MOBILE_BATT_BROADCAST_ACTION";


    public static final String MOBILE_BATTERY_LEVEL = "mobileBattery";
    public static final String MOBILE_BATTERY_POWER_CONNECTED = "powerConnected";

    private static void log(String str) {
        MLog.log(MobileBatteryLevelReceiver.class.getSimpleName() + ": NotificationReceiver: " + str);
    }

    /**
     * Create the broadcast intent to consume
     *
     * @return
     */
    public static Intent getNotificationIntent(boolean powerConnected, int mobileBatteryLevel, Bundle bundle) {


        Intent intent = new Intent(MOBILE_BATT_BROADCAST_ACTION);
        intent.putExtra(MOBILE_BATTERY_LEVEL, mobileBatteryLevel);
        intent.putExtra(MOBILE_BATTERY_POWER_CONNECTED, powerConnected);

        return intent;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        int mobileBatteryLevel = -1;
        boolean powerConnected = false;

        if(intent.getAction().equals(MOBILE_BATT_BROADCAST_ACTION)) {
            mobileBatteryLevel = intent.getIntExtra(MOBILE_BATTERY_LEVEL, -1);
            powerConnected = intent.getBooleanExtra(MOBILE_BATTERY_POWER_CONNECTED, false);
        }


        log("Received battery value: " + mobileBatteryLevel + ", " + powerConnected);

        Intent battUiIntent = new Intent("BLUETOOTH_EVENT");
        battUiIntent.putExtra("EVENT_TYPE", AMainActivity.WEBVIEW_MOBILE_BATTERY_UPDATE);
        battUiIntent.putExtra("mobileBattery", mobileBatteryLevel);
        battUiIntent.putExtra("powerConnected", powerConnected);
        LocalBroadcastManager.getInstance(context).sendBroadcast(battUiIntent);

        checkAlertWorthiness(mobileBatteryLevel, powerConnected, context);

    }


    private void checkAlertWorthiness(int mobileBatteryLevel, boolean powerConnected, Context context) {

        GregorianCalendar c = (GregorianCalendar) GregorianCalendar.getInstance();
        int hourOfDay = c.get(Calendar.HOUR_OF_DAY);

        if(!powerConnected)
        {
            if(mobileBatteryLevel == 20 || mobileBatteryLevel == 10)
            {
                AlertPlayer.playVibrateAlert(context);

                //if(hourOfDay > 8 && hourOfDay < 20)
                {
                    AlertPlayer.playAudioAlert(context);
                    AlertPlayer.turnScreenOn(context);
                    AlertPlayer.displayActivity(context);
                    AlertPlayer.showToast("PDA Battery = " + mobileBatteryLevel + "%", context);
                }
            }

        }
    }
}
