package com.spectocor.micor.mobileapp.http.requests;

import com.spectocor.micor.mobileapp.devicelogs.DeviceLogEntity;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogMapper;
import com.spectocor.micor.mobileapp.http.HttpRequestBase;
import com.spectocor.micor.mobileapp.http.HttpRequestHandler;

import java.util.ArrayList;

/**
 * Created by Umair on 3/8/2016.
 */
public class DeviceLogHttpRequest extends HttpRequestBase {

    private String chunkFileName = "";
    private DeviceLogMapper deviceLogsToSend = null;
    private ArrayList<DeviceLogEntity> logToUpdate;

    public DeviceLogHttpRequest(){

    }

    public DeviceLogHttpRequest(String fileName){
        chunkFileName = fileName;
    }

    @Override
    public void onQueue() {

    }

    @Override
    public void onCancelled() {

    }

    @Override
    public int getRequestType() {
        return HttpRequestHandler.DEVICE_LOG_REQUEST;
    }

    public void setLogToUpdate(ArrayList<DeviceLogEntity> logs)
    {
        this.logToUpdate = logs;
    }

    public ArrayList<DeviceLogEntity> getLogToUpdate() { return this.logToUpdate; }



    public void setDeviceLogsToBeSend(DeviceLogMapper logs)
    {
        this.deviceLogsToSend = logs;
    }

    public DeviceLogMapper getDeviceLogsTobeSend() { return this.deviceLogsToSend; }

}