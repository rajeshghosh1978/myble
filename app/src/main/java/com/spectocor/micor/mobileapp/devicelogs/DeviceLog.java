package com.spectocor.micor.mobileapp.devicelogs;


/**
 * Data structure for device logs
 */
public class DeviceLog {

    /**
     * Unique identifier to define storing monitored data in the database
     */
    private int MonitoringSessionId = -1;
    /**
     * Date time that the event happened and the application stored the log in unix seconds format
     */
    private int LogDateUnixSec;
    /**
     * Identifier for the type of information we are going to capture. For example Battery Status = 1
     */
    private EnumDeviceLogInfoType DeviceLogInfoTypeId;
    /**
     * If the information type has a value type of a number, it should be stored in this field. For example, 95 can be a value for battery status.
     * In that case ValueString will remain as null. If the value is not a type of number, then this field will get null value
     */
    private Double ValueNumber = null;
    /**
     * Same as ValueNumber field but for String values. In addition, any other value that can't be represented as number, should be stored as String.
     */
    private String ValueString;
    private long DatabaseRowId;


    /**
     * Creates empty Device log object
     */
    public DeviceLog() {
        // we shouldn't put any time when it is created without any parameter.
        //LogDateUnixSec = (int) (System.currentTimeMillis() / 1000);
    }

    /**
     * Constructor for device log object when no data is necessary other than the action.
     * For example, Application Start
     * It doesn't set Monitoring Session
     *
     * @param deviceLogInfoType   type of info
     */
    public DeviceLog(EnumDeviceLogInfoType deviceLogInfoType) {
        LogDateUnixSec = (int) (System.currentTimeMillis() / 1000);
        DeviceLogInfoTypeId = deviceLogInfoType;
    }


    /**
     * Constructor when device should be saved with numeric value. For example, battery level
     * It doesn't set Monitoring Session
     *
     * @param deviceLogInfoType   type of info
     * @param valueNumber         value
     */
    public DeviceLog(EnumDeviceLogInfoType deviceLogInfoType, double valueNumber) {
        LogDateUnixSec = (int) (System.currentTimeMillis() / 1000);
        DeviceLogInfoTypeId = deviceLogInfoType;
        ValueNumber = valueNumber;
    }


    /**
     * Constructor when device should be saved with boolean value. For example, connected or disconnected
     * It doesn't set Monitoring Session
     *
     * @param deviceLogInfoType   type of info
     * @param value         boolean value
     */
    public DeviceLog(EnumDeviceLogInfoType deviceLogInfoType, boolean value) {
        Double valueNumber = value? (double) 1: (double)0;
        LogDateUnixSec = (int) (System.currentTimeMillis() / 1000);
        DeviceLogInfoTypeId = deviceLogInfoType;
        ValueNumber = valueNumber;
    }



    /**
     * Constructor for device logs when value should be stored as string
     * It also covers complex data types like Json values
     * It doesn't set Monitoring Session
     * @param deviceLogInfoType type of info
     * @param valueString string value or JSON string
     */
    public DeviceLog(EnumDeviceLogInfoType deviceLogInfoType, String valueString) {
        LogDateUnixSec = (int) (System.currentTimeMillis() / 1000);
        DeviceLogInfoTypeId = deviceLogInfoType;
        ValueString = valueString;
    }

    /**
     * Constructor for when data need to be stored has both number and string
     * For example, when bluetooth connected along with data for device that is connected to
     * It doesn't set Monitoring Session
     * @param deviceLogInfoType type of info
     * @param valueNumber numeric value
     * @param valueString string value or JSON string
     */
    public DeviceLog(EnumDeviceLogInfoType deviceLogInfoType, double valueNumber, String valueString) {
        LogDateUnixSec = (int) (System.currentTimeMillis() / 1000);
        DeviceLogInfoTypeId = deviceLogInfoType;
        ValueNumber = valueNumber;
        ValueString = valueString;
    }


    public final int getMonitoringSessionId() {
        return MonitoringSessionId;
    }

    public final void setMonitoringSessionId(int value) {
        MonitoringSessionId = value;
    }

    public final int getLogDateUnixSec() {
        return LogDateUnixSec;
    }

    public final void setLogDateUnixSec(int value) {
        LogDateUnixSec = value;
    }

    public final EnumDeviceLogInfoType getDeviceLogInfoTypeId() {
        return DeviceLogInfoTypeId;
    }

    public final void setDeviceLogInfoTypeId(EnumDeviceLogInfoType value) {
        DeviceLogInfoTypeId = value;
    }

    public final Double getValueNumber() {
        return ValueNumber;
    }

    public final void setValueNumber(Double value) {
        ValueNumber = value;
    }

    public final String getValueString() {
        return ValueString;
    }

    public final void setValueString(String value) {
        ValueString = value;
    }

    public final long getDatabaseRowId() {
        return DatabaseRowId;
    }

    public final void setDatabaseRowId(long rowId) {
        DatabaseRowId = rowId;
    }

}