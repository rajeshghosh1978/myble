package com.spectocor.micor.mobileapp.amain.receivers;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogEvents;
import com.spectocor.micor.mobileapp.http.HttpQueueManager;
import com.spectocor.micor.mobileapp.http.common.NetworkHelper;

/**
 * Created by Savio Monteiro on 1/12/2016.
 */
public class AMainBroadcastReceiver extends BroadcastReceiver {

    public static int mobileBattery = -1;
    public static int mobileSignalBars = 0;
    public static float mobileSignalStrength = 0;
    public static String mobileRadioType = ""; // TelephonyManager.NETWORK_TYPE_ ....
    public static String mobileNetworkOperatorName = "";

    public static boolean internetAvailable = false;


    public static final String ACTION_MOBILE_BATTERY_LEVEL_CHANGED = "android.intent.action.BATTERY_CHANGED";
    public static final String ACTION_QUICKBOOT_POWERON = "android.intent.action.QUICKBOOT_POWERON";
    public static final String ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
    public static final String ACTION_MOBILE_INTERNET_CONNECTIVITY_CHANGED = "android.net.conn.CONNECTIVITY_CHANGE";
    public static final String ACTION_POWER_CONNECTED = "android.intent.action.ACTION_POWER_CONNECTED";
    public static final String ACTION_POWER_DISCONNECTED = "android.intent.action.ACTION_POWER_DISCONNECTED";
    public static final String ACTION_BATTERY_LOW = "android.intent.action.BATTERY_LOW";
    public static final String ACTION_BLUETOOTH_STATUS= "android.bluetooth.adapter.action.STATE_CHANGED";



    private static void log(String str) {
        MLog.log("AMainBroadcastReceiver: " + str);
    }

    private static boolean powerConnected = false;


    @Override
    public void onReceive(Context context, Intent intent) {


        if (intent.getAction() == null)
            return;

        log("Received Broadcast: " + intent.getAction());

        switch (intent.getAction()) {

            case ACTION_MOBILE_BATTERY_LEVEL_CHANGED: {

                mobileBattery = getCurrentMobileBattery(AMainApplication.getAppContext());
                powerConnected = isMobilePowerConnected(AMainApplication.getAppContext());

                log("Battery Changed: " + mobileBattery + "%");

                // notifying notification handler
                Intent notificationIntent = MobileBatteryLevelReceiver.getNotificationIntent(powerConnected, mobileBattery, null);
                LocalBroadcastManager.getInstance(context).sendBroadcast(notificationIntent);

                String batteryLevel  = mobileBattery + " %";
                DeviceLogEvents.getInstance().putLogMobileDeviceBatteryLevel(batteryLevel);

                break;
            }

            case ACTION_POWER_CONNECTED: {
                log("Power Connected!");

                mobileBattery = getCurrentMobileBattery(AMainApplication.getAppContext());
                powerConnected = true;
                DeviceLogEvents.getInstance().putLogMobileDevicePowerPlugConnected();

                // notifying notification handler
                Intent notificationIntent = MobileBatteryLevelReceiver.getNotificationIntent(powerConnected, mobileBattery, null);
                LocalBroadcastManager.getInstance(context).sendBroadcast(notificationIntent);

                String batteryLevel  = (int) mobileBattery + " %";
                DeviceLogEvents.getInstance().putLogMobileDeviceBatteryLevel(batteryLevel);



                break;
            }

            case ACTION_POWER_DISCONNECTED: {
                log("Power Disconnected!");

                mobileBattery = getCurrentMobileBattery(AMainApplication.getAppContext());
                powerConnected = false;

                // notifying notification handler
                Intent notificationIntent = MobileBatteryLevelReceiver.getNotificationIntent(powerConnected, (int) mobileBattery, null);
                LocalBroadcastManager.getInstance(context).sendBroadcast(notificationIntent);


                String batteryLevel  = (int) mobileBattery + " %";
                DeviceLogEvents.getInstance().putLogMobileDeviceBatteryLevel(batteryLevel);
                DeviceLogEvents.getInstance().putLogMobileDevicePowerPlugDisConnected();

                break;
            }

            case ACTION_QUICKBOOT_POWERON: {

                log("Quickboot Power On");
                break;
            }

            case ACTION_BOOT_COMPLETED: {

                // start our service and activity here ...
                // start an alarm service here ...


                log("Boot Completed.");

                log("Getting all networking details");
                //getAllNetworkInformation(context, false);

                /*log("Starting Main Activity");
                Intent activityIntent = new Intent(context, AMainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(activityIntent);*/

                break;
            }

            case ACTION_MOBILE_INTERNET_CONNECTIVITY_CHANGED: {
                internetAvailable = false;

                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = cm.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.isConnected()) {
                    // internet available
                    internetAvailable = true;
                    HttpQueueManager.getInstance().notifyBufferedHandler();
                }
                else
                {
                    internetAvailable = false;
                }


                // notify UI
                Bundle b = new Bundle();
                b.putBoolean("internetAvailable", internetAvailable);
                Intent uiIntent = AMainActivity.createBTIntent(AMainActivity.WEBVIEW_HOME_SCREEN_INTERNET_CONNECTIVITY_STATE_UPDATE, b);
                LocalBroadcastManager.getInstance(context).sendBroadcast(uiIntent);

                // notification alert
                Intent notificationIntent = CellInternetConnectivityReceiver.getNotificationIntent(internetAvailable, null);
                LocalBroadcastManager.getInstance(context).sendBroadcast(notificationIntent);

                log("Connectivity Changed. Internet available: " + internetAvailable);
                DeviceLogEvents.getInstance().putNetworkState(internetAvailable ? "ON" : "OFF");
                DeviceLogEvents.getInstance().putNetworkIsAvailable(internetAvailable ? "Available" : "Not Available");
                DeviceLogEvents.getInstance().putIsWifiEnabled(NetworkHelper.ifWifiEnable());

                break;
            }
            case ACTION_BATTERY_LOW: {
                DeviceLogEvents.getInstance().putLogBatteryLow();

                break;
            }

            case ACTION_BLUETOOTH_STATUS: {
               if(intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_OFF)
                    DeviceLogEvents.getInstance().putLogMobileDeviceBluetoothStatus(false);
                else
                    DeviceLogEvents.getInstance().putLogMobileDeviceBluetoothStatus(true);

                break;
            }
        }
    }



    private boolean isMobilePowerConnected(Context context){
        boolean charging = false;

        final Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean batteryCharge = status==BatteryManager.BATTERY_STATUS_CHARGING;

        int chargePlug = batteryIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

        if (batteryCharge) charging=true;
        if (usbCharge) charging=true;
        if (acCharge) charging=true;

        return charging;
    }

    private int getCurrentMobileBattery(Context context) {


        int mobileBattery = -1;

        Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            mobileBattery = 50;
        }

        mobileBattery = (int) (((float) level / (float) scale) * 100.0f);

        return mobileBattery;

    }

    public static void getAllNetworkInformation(Context context, final boolean killTelephonyListener) {

        // updating internet connectivity once ...
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            // internet available
            internetAvailable = true;
        }
        else
        {
            // internet not available
            internetAvailable = false;
        }

        final TelephonyManager tel = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        mobileRadioType = NetworkTypeString.getString(tel.getNetworkType());
        log("Radio Type: " + mobileRadioType);

        mobileNetworkOperatorName = tel.getNetworkOperatorName();
        log("Mobile Network Operator Name: " + mobileNetworkOperatorName);

        tel.listen(new PhoneStateListener() {

            @Override
            public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                super.onSignalStrengthsChanged(signalStrength);

                if (signalStrength.isGsm()) {
                    if (signalStrength.getGsmSignalStrength() != 99)
                        mobileSignalStrength = signalStrength.getGsmSignalStrength() * 2 - 113;
                    else
                        mobileSignalStrength = signalStrength.getGsmSignalStrength();
                } else {
                    mobileSignalStrength = signalStrength.getCdmaDbm();
                }

                log("mobileSignalStrength: " + mobileSignalStrength);

                // to kill listener after receiving one value
                if (killTelephonyListener)
                    tel.listen(this, LISTEN_NONE);
            }
        }, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }
}
