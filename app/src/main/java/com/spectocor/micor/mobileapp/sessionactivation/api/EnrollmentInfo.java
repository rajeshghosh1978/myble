package com.spectocor.micor.mobileapp.sessionactivation.api;

import com.spectocor.micor.mobileapp.amain.state.SessionInfoForState;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Session info for activation coming from the server
 */

public class EnrollmentInfo {

    private String firstName;
    private String lastName;
    private String lastNameInitial;
    private String facilityName;
    private String enrollmentDateIso8601;
    private int sessionId;
    private long sessionEndTimeUnixMs;
    private boolean activationCodeInUse = false;

    ArrayList<SessionInfoForState> sessionList = new ArrayList<>();

    /**
     * Patient's first name
     */
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Patient's last name
     */
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    /**
     * Patient's last name Initial
     */
    public String getLastNameInitial() {
        return lastNameInitial;
    }

    public void setLastNameInitial(String lastNameInitial) {
        this.lastNameInitial = lastName.substring(0, 1).toUpperCase();
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionID) {
        this.sessionId = sessionID;
    }
    /**
     * Facility name
     */
    public String getFacilityName() {
        return facilityName;
    }


    public ArrayList<SessionInfoForState> getAvailableSessions() {
        return sessionList;
    }


    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    /**
     * Monitoring Session Identifier
     */

    public void addSession(SessionInfoForState si) {
        sessionList.add(si);
    }

    public SessionInfoForState getSession(int index) {
     if(sessionList.get(index)!= null){
         return sessionList.get(index);
     }
        else
         return null;
    }

    /**
     * Enrollment date in ISO 8601 format (simply means yyyy-MM-dd. For example, 2015-12-15)
     * Here we keep no time. (read more here: https://en.wikipedia.org/wiki/ISO_8601)
     */
    public String getEnrollmentDateIso8601() {
        return enrollmentDateIso8601;
    }

    public void setEnrollmentDateIso8601(String enrollmentDateIso8601) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        formatter.parse(enrollmentDateIso8601); // just tries to parse the date with valid format
        this.enrollmentDateIso8601 = enrollmentDateIso8601;
    }

    /**
     * Session end time in unix epoch format unix epoch
     */
    public long getSessionEndTimeUnixMs() {
        return sessionEndTimeUnixMs;
    }

    public void setSessionEndTimeUnixMs(long sessionEndTimeUnixMs) {
        this.sessionEndTimeUnixMs = sessionEndTimeUnixMs;
    }



}
