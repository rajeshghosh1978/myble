package com.spectocor.micor.mobileapp.ecgadapter; /**
 * 
 */

import java.util.ArrayList;
import java.util.List;

/**
 * @author cshi
 *
 */
public class EcgResampler {

    public static short[] resample(final int SampleRate, final int TargetSampleRate, final List<Integer> signal) {

        short[] signalArr = new short[signal.size()];
        int i = 0;
        for(int s: signal) {
            signalArr[i] = (short) (s - 65535);
            i++;
        }

        return resample(SampleRate, TargetSampleRate, signalArr);
    }

    public static short[] resample(final int SampleRate, final int TargetSampleRate, final ArrayList<Short> signal) {

        short[] signalArr = new short[signal.size()];
        int i = 0;
        for(short s: signal) {
            signalArr[i] = s;
            i++;
        }

        return resample(SampleRate, TargetSampleRate, signalArr);
    }

	
	public static short[] resample(final int SampleRate, final int TargetSampleRate, final short[] signal) {

		if (SampleRate==TargetSampleRate){
			return signal;
		}
		
		int newLength=(int)Math.round(((float)signal.length/SampleRate*TargetSampleRate));
		float Multiplier=(float)newLength/signal.length;
        short[] interpolatedSamples = new short[newLength];

        for (int i = 0; i < newLength; i++){
            float Position = i / Multiplier;
            int LeftPosition = (int)Position;
            int RightPosition = LeftPosition + 1;
            if (RightPosition>=signal.length){
            	RightPosition=signal.length-1;
            }
            
            float slope=signal[RightPosition]-signal[LeftPosition];	
            float FromLeft = Position - LeftPosition;
            
            interpolatedSamples[i] = (short)(slope*FromLeft+signal[LeftPosition]);	
        }
        
        return interpolatedSamples;
	}
	
	

}
