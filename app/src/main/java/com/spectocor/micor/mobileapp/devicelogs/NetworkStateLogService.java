package com.spectocor.micor.mobileapp.devicelogs;

/**
 * Logs information obtained from the network status
 */
public class NetworkStateLogService {

    protected final DeviceLogLogger service;

    public NetworkStateLogService(DeviceLogLogger deviceLogLogger) {
        service = deviceLogLogger;
    }

    public DeviceLog logNetworkConnectivityChanged(boolean isConnected, String additionInfo){
        DeviceLog obj = new DeviceLog(EnumDeviceLogInfoType.NetworkState, isConnected);
        obj.setValueString(additionInfo);
        service.insertLog(obj);
        return obj;
    }
}
