package com.spectocor.micor.mobileapp.ecgstorage;

import android.content.Context;
import android.content.ContextWrapper;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.spectocor.micor.mobileapp.amain.AMainApplication;

import java.io.File;

/**
 * Class to enable SD Card database path ...
 * Created by Savio Monteiro on 2/4/2016 ...
 */
public class MicorEcgDbContext extends ContextWrapper {

    private static final String DEBUG_CONTEXT = "Micor:EcgDbCtx";
    private String dbPath;

    public MicorEcgDbContext(Context base, String dbPath) {
        super(base);
        this.dbPath = dbPath;
    }

    @Override
    public File getDatabasePath(String name) {

        if(name.contains(".db"))
            return new File(this.dbPath + "" + name);
        else
            return new File(this.dbPath + "" + name + ".db");
    }

    /* this version is called for android devices >= api-11. thank to @damccull for fixing this. */
    @Override
    public SQLiteDatabase openOrCreateDatabase(String name, int mode, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
        return openOrCreateDatabase(getDatabasePath(name).getAbsolutePath(),mode, factory);
    }

    /* this version is called for android devices < api-11 */
    @Override
    public SQLiteDatabase openOrCreateDatabase(String name, int mode, SQLiteDatabase.CursorFactory factory)
    {
        Log.d(DEBUG_CONTEXT, "Database Name? " + name);

        SQLiteDatabase result = SQLiteDatabase.openOrCreateDatabase(name, null);
        // SQLiteDatabase result = super.openOrCreateDatabase(name, mode, factory);
        if (Log.isLoggable(DEBUG_CONTEXT, Log.WARN))
        {
            Log.w(DEBUG_CONTEXT,
                    "openOrCreateDatabase(" + name + ",,) = " + result.getPath());
        }
        return result;
    }
}