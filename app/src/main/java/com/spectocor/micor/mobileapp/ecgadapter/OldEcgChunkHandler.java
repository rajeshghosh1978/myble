package com.spectocor.micor.mobileapp.ecgadapter;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.ecgadapter.compression.zip.ZipCompression;
import com.spectocor.micor.mobileapp.ecgblehandler.EcgChannelBluetoothData;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.utils.arrays.ArrayConcat;

import java.io.IOException;
import java.sql.SQLDataException;
import java.util.ArrayList;

/**
 * Created by Savio Monteiro on 1/12/2016.
 */
public class OldEcgChunkHandler extends Handler {



    // to maintain live count of samples being buffer
    private static int oldSampleCount = 0;
    private static Context mContext;

    private static final int MAX_SAMPLES_FOR_CHUNK = AMainApplication.CHUNK_SIZE_TO_TRANSFER_IN_SECONDS * EcgProperties.SAMPLE_RATE;

    private static ArrayList<Short> ch1Buffer = new ArrayList<>();
    private static ArrayList<Short> ch2Buffer = new ArrayList<>();
    private static long currentChunkStartTimestamp = -1;

    private static OldEcgChunkHandler oldEcgChunkHandler;
    private static HandlerThread oldEcgChunkHandlerThread;
    private static boolean oldEcgChunkHandlerThreadRunning = false;


    private static void log(String str) {
        MLog.log("OldEcgChunkHandler: " + str);
    }

    private OldEcgChunkHandler(Looper looper, Context context) {

        super(looper);
        mContext = context;

    }

    public static OldEcgChunkHandler getInstance(Context context) {

        if(oldEcgChunkHandlerThread == null) {
            oldEcgChunkHandlerThread = new HandlerThread("OldEcgChunkHandlerThread");
            oldEcgChunkHandlerThread.start();
            oldEcgChunkHandlerThreadRunning = true;
        }

        if(!oldEcgChunkHandlerThreadRunning) {
            oldEcgChunkHandlerThread.start();
            oldEcgChunkHandlerThreadRunning = true;
        }

        if(oldEcgChunkHandler == null) {
            oldEcgChunkHandler = new OldEcgChunkHandler(oldEcgChunkHandlerThread.getLooper(), context);
        }

        return oldEcgChunkHandler;
    }

    public void endInstance() {
        if(oldEcgChunkHandlerThreadRunning)
        {
            oldEcgChunkHandlerThread.quit();
            oldEcgChunkHandlerThread = null;
            oldEcgChunkHandler = null;
        }
    }


    public static final int OLD_ECG_ONE_SECOND_READY = 1;
    public static final int OLD_DATA_FINISHED = 2;

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);

        switch (msg.what) {

            case OLD_ECG_ONE_SECOND_READY: {

                EcgChannelBluetoothData ecgData = (EcgChannelBluetoothData) msg.obj;

                appendOneSecondEcgToOldChunkBuffer(ecgData);

                break;
            }

            case OLD_DATA_FINISHED: {

                createCompressedChunkFromCurrentBuffer();

                oldEcgChunkHandlerThread.quit();
                oldEcgChunkHandlerThreadRunning = false;

                break;
            }
        }
    }

    private void initBuffers(boolean forceInit, long firstTimestamp) {

        if(forceInit || ch1Buffer == null || ch2Buffer == null)
        {
            ch1Buffer = new ArrayList<>();
            ch2Buffer = new ArrayList<>();
            currentChunkStartTimestamp = firstTimestamp;
        }
    }



    private static long lastChunkCreatedTimestamp = -1;
    private static EcgDataChunk lastChunkCreated = null;


    public static long lastTimestampOfOneSecondDataRecd = -1;
    private void appendOneSecondEcgToOldChunkBuffer(final EcgChannelBluetoothData ecgData)
    {

        // checking if data is received in order of time
        if(lastTimestampOfOneSecondDataRecd >= ecgData.timestamp)
        {
            throw new IllegalArgumentException("Timestamp of ecgData is older or same as the previous data received");
        }

        /*
            log("Start Live Again? " + startAgain);
            log("oldSampleCount >= MAX_SAMPLES_FOR_CHUNK? " + (oldSampleCount >= MAX_SAMPLES_FOR_CHUNK));
        */

        // suggests that there has been a recent disconnection or a/many dropped 1-second packet/packets
        boolean timestampTooFarFromLastRecd = ((lastTimestampOfOneSecondDataRecd != -1) && (ecgData.timestamp - lastTimestampOfOneSecondDataRecd) >= 2);
        boolean fiveMinuteFull = (oldSampleCount >= MAX_SAMPLES_FOR_CHUNK);

        if(fiveMinuteFull || timestampTooFarFromLastRecd) {

            if(fiveMinuteFull) {
                log("Five Min Full. Current Time: " + System.currentTimeMillis());
            }

            if(timestampTooFarFromLastRecd) {
                log("Timestamp too far from last received: " + timestampTooFarFromLastRecd);
                log("Last Timestamp Received: " + lastTimestampOfOneSecondDataRecd + " vs " + ecgData.timestamp);
            }


            final EcgDataChunk compressedChunk = createCompressedChunkFromCurrentBuffer();

            lastChunkCreated = compressedChunk;
            lastChunkCreatedTimestamp = System.currentTimeMillis();

            // STORING TO DATABASE
            storeCompressedChunk(compressedChunk);


            log("CHUNK READY. " + compressedChunk.chunkStartDateUnixMs);


            // INDICATORS THAT CHUNK NOW NEEDS TO BE INITIALIZED
            currentChunkStartTimestamp = -1; // << VERY IMPORTANT
            oldSampleCount = 0; // << VERY IMPORTANT

        } // end of if-MAX_SAMPLES





        // TODO: assuming ch2.length is same as ch1.length
        // ... if STL API does not 0-pad or make ch1.length == ch2.length
        //      .. ecgHandler will have to handle it

        oldSampleCount += ecgData.ch1.length;


        // at end of every chunk fill-up, buffers are initialized and timestamp is set to -1,
        // .. as set in the above if-case
        // .. at this point the currentChunkStartTimestamp can be set to the timestamp of the first
        // .. one second list received from the ecg device
        if(currentChunkStartTimestamp == -1) {
            initBuffers(true, ecgData.timestamp);
            log("INIT CHUNK TIMESTAMP: " + ecgData.timestamp);
        }

        log("So far: " + oldSampleCount + " ("+ecgData.timestamp+")");

        // saving 400 short samples as byte array
        for(int i = 0; i < ecgData.ch1.length; i++) {
            ch1Buffer.add(ecgData.ch1[i]);
        }
        for(int i = 0; i < ecgData.ch2.length; i++) {
            ch2Buffer.add(ecgData.ch2[i]);
        }

        // if there was a disconnection, there will be a delay between current timestamp and
        // .. last recorded timestamp
        // .. at this point the currentChunkStartTimestamp can be set to the timestamp of the first
        // .. one second list received from the ecg device

        if(timestampTooFarFromLastRecd)
            lastTimestampOfOneSecondDataRecd = -1;
        else
            lastTimestampOfOneSecondDataRecd = ecgData.timestamp;

    } // end: function



    /**
     * Creates compressed chunk from full memory buffer
     * @return
     */
    private EcgDataChunk createCompressedChunkFromCurrentBuffer() {

        EcgChannelBluetoothData ecgLastChunk = new EcgChannelBluetoothData(currentChunkStartTimestamp, ch1Buffer, ch2Buffer);

        EcgDataChunk ecgDataChunkToStore = null;

        // STORING TO SD CARD
        if(ecgLastChunk == null) {
            log("could not save 5 minutes data");
            return null;
        }
        else {

            // JOINING BOTH CHANNELS AS ONE ARRAY
            final short[] bothChannelsShorts = ArrayConcat.concat(ecgLastChunk.ch1, ecgLastChunk.ch2);


            // COMPRESSING
            log("Compressing ...");
            byte[] compressedBytes = null;

            try {
                compressedBytes = ZipCompression.compressEcg(bothChannelsShorts);
                log("Compressing from " + (ecgLastChunk.ch1.length + ecgLastChunk.ch2.length)*2 + " to " + compressedBytes.length);

            } catch (IOException e) {
                log("Compression Error: " + e.getMessage());
                return null;
            }

            log("Creating compressed 5 minutes ecgdatachunk object");
            final int sessionId = SessionStateAccess.getCurrentSessionState(mContext).getSessionId();

            ecgDataChunkToStore = new EcgDataChunk();
            ecgDataChunkToStore.monitoringSessionId = sessionId;
            ecgDataChunkToStore.chunkDataChannelCompressed = compressedBytes;
            ecgDataChunkToStore.chunkStartDateUnixMs = currentChunkStartTimestamp;

            // saving number of samples too. assuming ch1 size is same as ch2 size.
            ecgDataChunkToStore.numberOfSamples = ecgLastChunk.ch1.length;


        } // ... handled 5 min chunk


        return ecgDataChunkToStore;
    }



    /**
     * Storing compressed chunks to local sd-card sqlite database
     * @param compressedEcgDataChunk The EcgDataChunk to save to sd-card sqlite db
     */
    private void storeCompressedChunk(final EcgDataChunk compressedEcgDataChunk) {

        if(compressedEcgDataChunk == null) {
            throw new NullPointerException("Compressed Ecg Chunk is null");
        }

        // SAVING TO DATABASE
        log("Saving compressed data to sd card");

        try {

            EcgChunkDbHelper ecgChunkDbHelper = EcgChunkDbHelper.getInstance(mContext);
            ecgChunkDbHelper.insertEcgChunk(compressedEcgDataChunk);

        } catch (SQLDataException e) {
            log(e.toString());
        }


    }// end: store-chunk


}





