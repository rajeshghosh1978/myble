package com.spectocor.micor.mobileapp.ecgstorage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.MLog;

import java.sql.SQLDataException;
import java.util.TimeZone;

/**
 * Created by Savio Monteiro on 1/18/2016.
 */
public class EcgChunkDbHelperOld extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 3;
    public static String DATABASE_NAME = "MicorDatabase.db";
    private static final String TABLE_NAME = "ECG_CHUNKS";

    private static final String INTEGER_TYPE = " INTEGER";
    private static final String TEXT_TYPE = " TEXT";
    private static final String BLOB_TYPE = " BLOB";
    private static final String COMMA_SEP = ",";

    private static final String COLUMN_NAME_SESSION_ID = "SESSION_ID";
    private static final String COLUMN_NAME_START_TIMESTAMP = "START_TIMESTAMP";
    private static final String COLUMN_NAME_INSERT_TIMESTAMP = "INSERT_TIMESTAMP";
    private static final String COLUMN_NAME_TIMEZONE_NAME_ID = "TIMEZONE_NAME_ID";
    private static final String COLUMN_NAME_TIMEZONE_OFFSET_MILLIS = "TIMEZONE_OFFSET_MILLIS";
    private static final String COLUMN_NAME_COMPRESSED_ECG_CHUNK_BLOB = "COMPRESSED_ECG_CHUNK_BLOB";


    // new column v2
    private static final String COLUMN_NAME_TRANSMITTED_TIMESTAMP = "TIMESTAMP_OF_SUCCESSFUL_TRANSFER";

    // new column v3
    private static final String COLUMN_NAME_NUMBER_OF_SAMPLES_IN_CHUNK = "NUMBER_OF_SAMPLES_IN_CHUNK";


    private static String TIMEZONE_NAME_ID = "";
    private static long TIMEZONE_OFFSET_MILLIS = 0;

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_NAME_SESSION_ID + TEXT_TYPE + COMMA_SEP +
                    COLUMN_NAME_START_TIMESTAMP + INTEGER_TYPE + COMMA_SEP +
                    COLUMN_NAME_INSERT_TIMESTAMP + INTEGER_TYPE + COMMA_SEP +
                    COLUMN_NAME_TIMEZONE_OFFSET_MILLIS + INTEGER_TYPE + COMMA_SEP +
                    COLUMN_NAME_TIMEZONE_NAME_ID + TEXT_TYPE + COMMA_SEP +
                    COLUMN_NAME_COMPRESSED_ECG_CHUNK_BLOB + BLOB_TYPE + COMMA_SEP +
                    COLUMN_NAME_TRANSMITTED_TIMESTAMP + INTEGER_TYPE + COMMA_SEP +
                    COLUMN_NAME_NUMBER_OF_SAMPLES_IN_CHUNK + INTEGER_TYPE + COMMA_SEP +
                "PRIMARY KEY("+COLUMN_NAME_SESSION_ID + COMMA_SEP + COLUMN_NAME_START_TIMESTAMP+")" +
            " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    private static EcgChunkDbHelperOld ecgChunkDbHelper;



    private static void log(String str) {
        MLog.log("EcgChunkDbHelper: " + str);
    }


    private EcgChunkDbHelperOld(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        log("Database Name: " + DATABASE_NAME);
    }

    public static EcgChunkDbHelperOld getInstance(Context context) {



        if(AMainApplication.DB_NAME_AND_PATH != null) {
            DATABASE_NAME = AMainApplication.DB_NAME_AND_PATH + "-ECG.db";
            log("Database File with Path (from AMainApplication): " + DATABASE_NAME);
        }
        /*
        else {

            CurrentSessionState sessionState = SessionStateAccess.getCurrentSessionState(context);
            if(sessionState == null) {
                log("No Session State exists");
                throw new IllegalStateException("No Session State exists. Database will not be created.");
            }

            DATABASE_NAME = sessionState.dbPathAndName;
            log("Database File with Path: " + DATABASE_NAME);
        }*/

        if(ecgChunkDbHelper == null) {
            ecgChunkDbHelper = new EcgChunkDbHelperOld(context);
        }

        TIMEZONE_NAME_ID = TimeZone.getDefault().getID();
        TIMEZONE_OFFSET_MILLIS = TimeZone.getDefault().getOffset(System.currentTimeMillis());

        log("Timezone: " + TIMEZONE_NAME_ID + " (offset = " + TIMEZONE_OFFSET_MILLIS + ")");

        return ecgChunkDbHelper;
    }

    public static EcgChunkDbHelperOld resetInstance(Context context) {

        log("Setting Database from Scratch");

        ecgChunkDbHelper = null;

        return getInstance(context);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /**
     * Saving an ecg chunk to the database
     *
     * @param sessionId The sessionId of the activated session
     * @param ecgDataChunk The EcgDataChunk to store
     * @return Returns the EcgDataChunk if successful
     * @throws SQLDataException
     */
    public EcgDataChunk insertEcgChunk(int sessionId, EcgDataChunk ecgDataChunk) throws SQLDataException {

        ContentValues values = new ContentValues();

            values.put(COLUMN_NAME_SESSION_ID, sessionId);
            values.put(COLUMN_NAME_START_TIMESTAMP, ecgDataChunk.chunkStartDateUnixMs);
            values.put(COLUMN_NAME_INSERT_TIMESTAMP, System.currentTimeMillis());
            values.put(COLUMN_NAME_TIMEZONE_NAME_ID, TIMEZONE_NAME_ID);
            values.put(COLUMN_NAME_TIMEZONE_OFFSET_MILLIS, TIMEZONE_OFFSET_MILLIS);
            values.put(COLUMN_NAME_COMPRESSED_ECG_CHUNK_BLOB, ecgDataChunk.chunkDataChannelCompressed);
            values.put(COLUMN_NAME_TRANSMITTED_TIMESTAMP, -1);
            values.put(COLUMN_NAME_NUMBER_OF_SAMPLES_IN_CHUNK, ecgDataChunk.numberOfSamples);

        long rowId = getWritableDatabase().insert(TABLE_NAME, "", values);

        if(rowId == -1) {
            throw new SQLDataException("DID NOT INSERT");
        }
        else {

            log("Successfully inserted chunk " + ecgDataChunk.chunkDataChannelCompressed.length + " bytes");

            return ecgDataChunk;
        }
    }

    /**
     * Retrieves a row of ecg database with sessionId and startTimestamp as key
     * @param sessionId
     * @param startTimestamp
     * @return
     * @throws SQLDataException
     */
    public EcgDataChunk retrieveCompressedEcg(int sessionId, long startTimestamp) throws SQLDataException {

        String QUERY = "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME_SESSION_ID + "=? and " + COLUMN_NAME_START_TIMESTAMP + "=?";
        String SELECTION_ARGS[] = {"" + sessionId, "" + startTimestamp};

        // TODO free up cursor after finished reading data
        Cursor c = getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        log(QUERY + " with args " + SELECTION_ARGS.toString());
        log("Num of rows: " + c.getCount());

        if(c.getCount() == 0) {
            log("Row not found ... ");
            throw new SQLDataException("No such data found");
        }

        c.moveToFirst();

        EcgDataChunk ecgDataChunk = new EcgDataChunk();
        ecgDataChunk.chunkDataChannelCompressed = c.getBlob(c.getColumnIndex(COLUMN_NAME_COMPRESSED_ECG_CHUNK_BLOB));
        ecgDataChunk.chunkStartDateUnixMs = c.getLong(c.getColumnIndex(COLUMN_NAME_START_TIMESTAMP));
        ecgDataChunk.insertTimestamp = c.getLong(c.getColumnIndex(COLUMN_NAME_INSERT_TIMESTAMP));
        ecgDataChunk.monitoringSessionId = Integer.parseInt(c.getString(c.getColumnIndex(COLUMN_NAME_SESSION_ID)));
        ecgDataChunk.timezoneId = c.getString(c.getColumnIndex(COLUMN_NAME_TIMEZONE_NAME_ID));
        ecgDataChunk.timezoneOffsetMillis = c.getLong(c.getColumnIndex(COLUMN_NAME_TIMEZONE_OFFSET_MILLIS));
        ecgDataChunk.numberOfSamples = c.getInt(c.getColumnIndex(COLUMN_NAME_NUMBER_OF_SAMPLES_IN_CHUNK));

        return ecgDataChunk;
    }

    /**
     * Setting an ecg chunk row as transferred by setting timestamp in transmitted_timestamp column
     * @param sessionId The sessionId of the current session
     * @param startTimestamp the startTimestamp of the chunk to set as transmitted.
     */
    public int setEcgChunkAsTransmitted(int sessionId, long startTimestamp) {

        if (sessionId <= 0) {
            throw new IllegalArgumentException("Session Id should be positive");
        }

        if(startTimestamp <= 0) {
            throw new IllegalArgumentException("Start timestamp should be positive");
        }

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_TRANSMITTED_TIMESTAMP, System.currentTimeMillis());

        // updating row
        return db.update(
                    TABLE_NAME,
                    values,
                    COLUMN_NAME_SESSION_ID + "=? and " + COLUMN_NAME_START_TIMESTAMP + "=?",
                    new String[] { "" + sessionId, "" + startTimestamp }
        );

    }

    /**
     * Retrieves the oldest ECG chunk that is not yet transferred.
     * @return EcgDataChunk instance of the oldest row if found
     * @throws SQLDataException If no row left to transfer
     */
    public EcgDataChunk retrieveOldestNotTransmittedChunk() throws SQLDataException {

        String QUERY = "SELECT * FROM " + TABLE_NAME + " WHERE " +
                "" + COLUMN_NAME_TRANSMITTED_TIMESTAMP + "=-1 order by " +
                "" + COLUMN_NAME_START_TIMESTAMP + " LIMIT 1";

        String SELECTION_ARGS[] = {};
        log(QUERY + "before cursor with args " + SELECTION_ARGS.toString());
        Cursor c = getReadableDatabase().rawQuery(QUERY, SELECTION_ARGS);
        log(QUERY + "after cursor with args " + SELECTION_ARGS.toString());
        log("Num of rows: " + c.getCount());

        if(c.getCount() == 0) {
            log("Row not found ... ");
            c.close(); // close cursor to free up memory
            throw new SQLDataException("NONE_LEFT_TO_TRANSFER");
        }

        c.moveToFirst();

        EcgDataChunk ecgDataChunk = new EcgDataChunk();
        ecgDataChunk.chunkDataChannelCompressed = c.getBlob(c.getColumnIndex(COLUMN_NAME_COMPRESSED_ECG_CHUNK_BLOB));
        ecgDataChunk.chunkStartDateUnixMs = c.getLong(c.getColumnIndex(COLUMN_NAME_START_TIMESTAMP));
        ecgDataChunk.insertTimestamp = c.getLong(c.getColumnIndex(COLUMN_NAME_INSERT_TIMESTAMP));
        ecgDataChunk.monitoringSessionId = Integer.parseInt(c.getString(c.getColumnIndex(COLUMN_NAME_SESSION_ID)));
        ecgDataChunk.timezoneId = c.getString(c.getColumnIndex(COLUMN_NAME_TIMEZONE_NAME_ID));
        ecgDataChunk.timezoneOffsetMillis = c.getLong(c.getColumnIndex(COLUMN_NAME_TIMEZONE_OFFSET_MILLIS));
        ecgDataChunk.numberOfSamples = c.getInt(c.getColumnIndex(COLUMN_NAME_NUMBER_OF_SAMPLES_IN_CHUNK));

        log("Before Return EcgChunks");
        return ecgDataChunk;
    }

}
