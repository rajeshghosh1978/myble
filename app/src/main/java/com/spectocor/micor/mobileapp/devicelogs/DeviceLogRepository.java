package com.spectocor.micor.mobileapp.devicelogs;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.spectocor.micor.mobileapp.common.RepositoryBase;
import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;


/**
 * Repository to access database DeviceLog table
 */
@SuppressWarnings("WeakerAccess")
public class DeviceLogRepository extends RepositoryBase<DeviceLog> {

    public static final String TABLE_NAME = "DeviceLog";

    public static final String COLUMN_ROW_ID = "RowId";
    public static final String COLUMN_MONITORING_SESSION_ID = "monitoringSessionId";
    public static final String COLUMN_LOG_DATE_UNIX_SEC = "LogDateUnixSec";
    public static final String COLUMN_DEVICE_LOG_INFO_TYPE_ID = "DeviceLogInfoTypeId";
    public static final String COLUMN_VALUE_NUMBER = "ValueNumber";
    public static final String COLUMN_VALUE_STRING = "ValueString";
    /**
     * SQLite script to create this table
     */
    public static final String CREATE_TABLE_SQL_SCRIPT = "create table " + TABLE_NAME + " ("
            + COLUMN_ROW_ID + " INTEGER primary key autoincrement, "
            + COLUMN_LOG_DATE_UNIX_SEC + " BIGINT,"
            + COLUMN_MONITORING_SESSION_ID + " INT,"   //TODO: Retrieve monitoring session from monitoring session table
            + COLUMN_DEVICE_LOG_INFO_TYPE_ID + " SMALLINT,"
            + COLUMN_VALUE_NUMBER + " DOUBLE,"
            + COLUMN_VALUE_STRING + " TEXT"
            + ");";


    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param databaseHelper database helper
     */
    public DeviceLogRepository(LogEventsDatabaseHelper databaseHelper) {
        super(databaseHelper, TABLE_NAME, COLUMN_ROW_ID);
    }


    /**
     * get content values for insert
     *
     * @param obj item to be converted
     * @return content values used with SQLite
     */
    @NonNull
    private ContentValues getContentValues(DeviceLog obj) {
        ContentValues values = new ContentValues();

        values.put(COLUMN_MONITORING_SESSION_ID, obj.getMonitoringSessionId());
        values.put(COLUMN_LOG_DATE_UNIX_SEC, obj.getLogDateUnixSec());

        //if (obj.getDeviceLogInfoTypeId() != null)
        values.put(COLUMN_DEVICE_LOG_INFO_TYPE_ID, obj.getDeviceLogInfoTypeId().getValue());
        //if (obj.getValueNumber() != null)
        values.put(COLUMN_VALUE_NUMBER, obj.getValueNumber());
        //if (obj.getValueString() != null)
        values.put(COLUMN_VALUE_STRING, obj.getValueString());

        return values;
    }

    /**
     * Reads data from a cursor and converts it to the actual object
     *
     * @param cursor android Database Cursor
     * @return actual object
     */
    protected DeviceLog getObjectByCursor(Cursor cursor) {
        DeviceLog obj = new DeviceLog();

        obj.setDatabaseRowId(cursor.getLong(cursor.getColumnIndex(COLUMN_ROW_ID)));
        obj.setMonitoringSessionId(cursor.getInt(cursor.getColumnIndex(COLUMN_MONITORING_SESSION_ID)));
        obj.setLogDateUnixSec(cursor.getInt(cursor.getColumnIndex(COLUMN_LOG_DATE_UNIX_SEC)));
        obj.setDeviceLogInfoTypeId(EnumDeviceLogInfoType.forValue(cursor.getInt(cursor.getColumnIndex(COLUMN_DEVICE_LOG_INFO_TYPE_ID))));
        obj.setValueNumber(cursor.getDouble(cursor.getColumnIndex(COLUMN_VALUE_NUMBER)));
        obj.setValueString(cursor.getString(cursor.getColumnIndex(COLUMN_VALUE_STRING)));

        return obj;
    }

    @Override
    public void update(DeviceLog obj) {
        throw new UnsupportedOperationException();
    }


    @Override
    protected long getPrimaryKey(DeviceLog obj) {
        return obj.getDatabaseRowId();
    }

    @Override
    protected void setPrimaryKey(DeviceLog obj, long newPrimaryKey) {
        obj.setDatabaseRowId(newPrimaryKey);
    }

    @NonNull
    @Override
    protected ContentValues getInsertContentValues(DeviceLog obj) {
        return getContentValues(obj);
    }

    @NonNull
    @Override
    protected ContentValues getUpdateContentValues(DeviceLog obj) {
        return getContentValues(obj);
    }


}
