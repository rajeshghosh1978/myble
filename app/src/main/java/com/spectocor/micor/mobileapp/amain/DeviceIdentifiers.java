package com.spectocor.micor.mobileapp.amain;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;

import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;

/**
 * Returns ecg and mobile device identifiers.
 *
 * Created by Rafay Ali on 2/16/2016.
 */
public class DeviceIdentifiers {

    private static void log(String str) {
        MLog.log(DeviceIdentifiers.class.getSimpleName() + ": " + str);
    }

    private static String ecgId = "";
    private static String pdaId = "";


    ////////// PDA ID ==============

    // TODO: Clear this up.
    /**
     * Gets the current PDA ID
     * @return
     */
    public static String getPdaId(){

        if (pdaId.isEmpty() || pdaId == "") {

            TelephonyManager telephonyManager = (TelephonyManager) AMainApplication.getAppContext().getSystemService(Context.TELEPHONY_SERVICE);
            pdaId = telephonyManager.getDeviceId();

        }

        return pdaId;
    }

    /*public static void setPdaId(String deviceIdStr) {

        log("Setting PDA Id to: " + deviceIdStr);

        pdaId = deviceIdStr;
    }*/


    ////////// ECG ID ==============

    /**
     * Get the current Ecg ID
     * @return
     */
    public static String getEcgId() {

        String btMac = SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext()).ecgBtMac;
        ecgId = "EcgBtMac-" + btMac.replace(":","");

        return ecgId;
    }

    /*public static void setEcgId(String ecgIdStr) {

        if(ecgIdStr.isEmpty() || ecgIdStr == null) {
            throw new IllegalArgumentException("Ecg ID should have a value");
        }

        log("Setting ECG Id to: " + ecgIdStr);

        ecgId = ecgIdStr;
    }*/



    public static String getAppVersionId(Context context) {
        String mobileAppVersion = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            mobileAppVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return mobileAppVersion;
    }
}
