package com.spectocor.micor.mobileapp.sessionactivation.api.requests;

@SuppressWarnings("unused")
public class GetFacilityNameByFacilityCodeRequest {
    private final String facilityCode;
    private final String mobileId;

    /**
     * @param facilityCode The Facility ID
     */
    public GetFacilityNameByFacilityCodeRequest(String facilityCode, String mobileId) {
        this.facilityCode = facilityCode;
        this.mobileId = mobileId;
    }

    /**
     * Facility code
     *
     * @return code of the facility
     */
    public String getFacilityCode() {
        return facilityCode;
    }

    /**
     * gets Mobile Device identification
     *
     * @return Mobile Device identification
     */
    public String getMobileId() {
        return mobileId;
    }
}
