package com.spectocor.micor.mobileapp.ecgchunktransmission;

import android.content.Context;

import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.common.exceptions.WebApiCallException;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;


import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Savio Monteiro on 1/17/2016.
 */
public class ChunkTransmissionThread extends Thread {

    private static OkHttpClient okHttpClient;

    private static ChunkTransmissionThread chunkTransmissionThread;
    private static Context mContext;
    private static GlobalSettings globalSettings;

    private ChunkTransmissionThread() {}

    private static void log(String str) {
        MLog.log("ChunkTransmissionThread: " + str);
    }

    public static ChunkTransmissionThread getInstance(Context context) {
        mContext = context;

        if(chunkTransmissionThread == null) {
            chunkTransmissionThread = new ChunkTransmissionThread();
        }

        if(okHttpClient == null) {
            okHttpClient = new OkHttpClient();
        }

        if(globalSettings == null) {
            globalSettings = new GlobalSettings();
        }

        return chunkTransmissionThread;
    }

    /**
     * Transfer an ecg chunk using the chunk transfer API
     *
     * @param ecgDataChunk
     * @return
     * @throws WebApiCallException
     */
    public boolean transferChunk(EcgDataChunk ecgDataChunk) throws WebApiCallException {

        if(ecgDataChunk == null) {
            throw new NullPointerException("ecg Data chunk cannot be null");
        }

        log("Transmitting raw " + ecgDataChunk.chunkDataChannelCompressed.length + " bytes");

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("sessionId", ecgDataChunk.monitoringSessionId + "")
                .addFormDataPart("startTimestamp", ecgDataChunk.chunkStartDateUnixMs + "")
                        //.addFormDataPart("timezoneId", ecgDataChunk.timezoneId + "")
                        //.addFormDataPart("timezoneOffsetMillis", ecgDataChunk.timezoneOffsetMillis + "")
                .addFormDataPart("file", "data.cmp",
                        RequestBody.create(MediaType.parse("application/octet-stream"), ecgDataChunk.chunkDataChannelCompressed))

                .build();

        String url = "http://208.67.179.147/en/v1/ecg/chunk";

        Request request = new Request.Builder()
                //.header("Authorization", "Basic Zm9vOmJhcg==")
                .url(url)
                .post(requestBody)
                .build();

        try {

            log("Making Request ... ");

            log(request.toString());

            Response response = okHttpClient.newCall(request).execute();

            if (!response.isSuccessful()) {
                log("Failed!");
                throw new WebApiCallException(url, response.code(), request.toString());
            }

            String responseStr = response.body().string();
            log("Response: " + responseStr);

            if(responseStr.contains("SUCCESS"))
            {
                log("ChunkTransferResponse: " + response.body().string());
                return true;
            }
            else
            {
                return false;
            }

        } catch (IOException ex) {
            throw new WebApiCallException(url, -1, ex.toString() + "\r\n" + request.toString());
        }

    }



    /*public boolean transferStringChunk(String sampleStr) throws WebApiCallException {

        log("Transferring String ... " + sampleStr.split("\n").length + " samples");

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("sessionId", "1")
                .addFormDataPart("startTimestamp", System.currentTimeMillis() + "")
                .addFormDataPart("file", "data.cmp.txt",
                        RequestBody.create(MediaType.parse("application/octet-stream"), sampleStr.getBytes()))
                .build();

        String url = "http://208.67.179.147/en/v1/ecg/chunk";


        Request request = new Request.Builder()
                .header("Authorization", "Basic Zm9vOmJhcg==")
                .url(url)
                .post(requestBody)
                .build();


        try {

            log("Making Request ... ");
            Response response = okHttpClient.newCall(request).execute();

            if (!response.isSuccessful()) {
                log("Failed!");
                throw new WebApiCallException(url, response.code(), request.toString());
            }

            log("Response: " + response.body().string());

            if(response.body().string().contains("SUCCESS"))
            {
                log("ChunkTransferResponse: " + response.body().string());
                return true;
            }
            else
            {
                return false;
            }

        } catch (IOException ex) {
            throw new WebApiCallException(url, -1, ex.toString() + "\r\n" + request.toString());
        }
    }*/




/*

    public boolean transferChunk(ChunkTransferId chunkTransferId) throws WebApiCallException {

        SQLiteDatabase db = new EcgDatabaseHelper(new DatabaseContext(mContext)).getReadableDatabase();
        EcgDataChunk ecgDataChunk = EcgDataChunkService.getInstance(mContext).retrieveChunk(chunkTransferId.sessionId, chunkTransferId.startTimestampMs, db);


        RequestBody requestBody = new MultipartBody.Builder()
                .type(MultipartBuilder.FORM)
                .addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"sessionId\""),
                        RequestBody.create(MediaType.parse("text/plain"), ecgDataChunk.getMonitoringSessionId() + ""))
                .addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"startTimestamp\""),
                        RequestBody.create(MediaType.parse("text/plain"), ecgDataChunk.getChunkStartDateUnixMs() + ""))
                .addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"upfile\""),
                        RequestBody.create(MediaType.parse("application/octet-stream"), ecgDataChunk.getChunkDataChannelCompressed()))
                .build();

        String url = "http://montyden.saviomonteiro.com/upload/upload.php";

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        try {

            Response response = okHttpClient.newCall(request).execute();

            if (!response.isSuccessful())
                throw new WebApiCallException(url, response.code(), request.toString());

            if(response.body().string().contains("SUCCESS"))
            {
                log("Response: " + response.body().string());
                return true;
            }
            else
            {
                return false;
            }

        } catch (IOException ex) {
            throw new WebApiCallException(url, -1, ex.toString() + "\r\n" + request.toString());
        }

    }
*/

}
