package com.spectocor.micor.mobileapp.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 2/24/2016.
 */
public class PatientActivation {

    @SerializedName("patient")
    @Expose
    private Patient patient;
    @SerializedName("facility")
    @Expose
    private Facility facility;
    @SerializedName("session")
    @Expose
    private List<Session> session = new ArrayList<Session>();


    /**
     *
     * @return
     * The patient
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     *
     * @param patient
     * The patient
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    /**
     *
     * @return
     * The facility
     */
    public Facility getFacility() {
        return facility;
    }

    /**
     *
     * @param facility
     * The facility
     */
    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    /**
     *
     * @return
     * The session
     */
    public List<Session> getSession() {
        return session;
    }

    /**
     *
     * @param session
     * The session
     */
    public void setSession(List<Session> session) {
        this.session = session;
    }

}
