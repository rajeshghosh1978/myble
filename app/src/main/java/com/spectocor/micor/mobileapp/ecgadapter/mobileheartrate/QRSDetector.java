package com.spectocor.micor.mobileapp.ecgadapter.mobileheartrate;

public class QRSDetector
	{

	private QRSDetectorParameters qrsDetParas ;
	private QRSDetectorParameters.PreBlankParameters preBlankParas ;
	private QRSFilterer qrsFilterer ;

	private int det_thresh ;
	private int qpkcnt = 0 ;
	private int[] qrsbuf = new int[5] ;
	private int[] noise = new int[5] ;
	private int[] rrbuf = new int[5] ;
	private int[] rsetBuff = new int[5] ;
	private int rsetCount = 0 ;
	private int nmedian ;
	private int qmedian ;
	private int rrmedian ;
	private int count = 0 ;
	private int sbpeak = 0 ;
	private int sbloc ;
	private int sbcount ;
	private int maxder = 0 ;
	private int initBlank = 0 ;
	private int initMax = 0;
	private int preBlankCnt = 0 ;
	private int tempPeak ;
	private int[] DDBuffer ;
	private int DDPtr = 0 ;
	private double TH = 0.475 ;
	private int MEMMOVELEN = 4;
	
	private int Peak_max = 0 ;
	private int Peak_timeSinceMax = 0 ;
	private int Peak_lastDatum ;
	

	public QRSDetector(QRSDetectorParameters qrsDetectorParameters)
		{
		qrsDetParas = qrsDetectorParameters;
		preBlankParas = new QRSDetectorParameters.PreBlankParameters(qrsDetectorParameters, qrsDetectorParameters.MS200) ;
		
		sbcount = qrsDetectorParameters.MS1500 ;
		DDBuffer = new int[preBlankParas.DER_DELAY] ;
		for(int i = 0; i < 5; ++i)
			{
			rrbuf[i] = qrsDetectorParameters.MS1000;
			}
		}
	

	public void setObjects(QRSFilterer qrsFilterer)
		{
		this.qrsFilterer = qrsFilterer;
		}
	

	public int QRSDet( int datum )
		{
		int fdatum, QrsDelay = 0 ;
		int i, newPeak, aPeak ;

		fdatum = qrsFilterer.QRSFilter(datum) ;	

		

		aPeak = Peak(fdatum) ;



		newPeak = 0 ;
		if(aPeak != 0 && preBlankCnt == 0)			
			{										
			tempPeak = aPeak ;
			preBlankCnt = preBlankParas.PRE_BLANK ;			
			}

		else if(aPeak == 0 && preBlankCnt != 0)	
			{										
			if(--preBlankCnt == 0)
				newPeak = tempPeak ;
			}

		else if(aPeak != 0)							
			{										
			if(aPeak > tempPeak)				
				{
				tempPeak = aPeak ;
				preBlankCnt = preBlankParas.PRE_BLANK ; 
				}
			else if(--preBlankCnt == 0)
				newPeak = tempPeak ;
			}


		
		DDBuffer[DDPtr] = qrsFilterer.deriv1( datum ) ;
		if(++DDPtr == preBlankParas.DER_DELAY)
			DDPtr = 0 ;



		// 
		int numOfBeatsToDetect = 3;
		if( qpkcnt < numOfBeatsToDetect )
			{
			++count ;
			if(newPeak > 0) count = qrsDetParas.WINDOW_WIDTH ;
			if(++initBlank == qrsDetParas.MS1000)
				{
				initBlank = 0 ;
				qrsbuf[qpkcnt] = initMax ;
				initMax = 0 ;
				++qpkcnt ;
				if(qpkcnt == numOfBeatsToDetect)
					{
					qmedian = median( qrsbuf, numOfBeatsToDetect ) ;
					nmedian = 0 ;
					rrmedian = qrsDetParas.MS1000 ;
					sbcount = qrsDetParas.MS1500+qrsDetParas.MS150 ;
					det_thresh = thresh(qmedian,nmedian) ;
					}
				}
			if( newPeak > initMax )
				initMax = newPeak ;
			}

		else	
			{
			++count ;
			if(newPeak > 0)
				{
				

				int[] maxderArray = new int[]{maxder};
				boolean result = BLSCheck(DDBuffer, DDPtr, maxderArray);
				maxder = maxderArray[0];
				if(!result)
					{



					if(newPeak > det_thresh)
						{
						System.arraycopy(qrsbuf, 0, qrsbuf, 1, MEMMOVELEN) ;
						qrsbuf[0] = newPeak ;
						qmedian = median(qrsbuf,5) ;
						det_thresh = thresh(qmedian,nmedian) ;
						System.arraycopy(rrbuf, 0, rrbuf, 1, MEMMOVELEN) ;
						rrbuf[0] = count - qrsDetParas.WINDOW_WIDTH ;
						rrmedian = median(rrbuf,5) ;
						sbcount = rrmedian + (rrmedian >> 1) + qrsDetParas.WINDOW_WIDTH ;
						count = qrsDetParas.WINDOW_WIDTH ;

						sbpeak = 0 ;
						maxder = 0 ;
						QrsDelay =  qrsDetParas.WINDOW_WIDTH + preBlankParas.FILTER_DELAY ;
						initBlank = initMax = rsetCount = 0 ;
						}



					else
						{
						System.arraycopy(noise, 0, noise, 1, MEMMOVELEN) ;
						noise[0] = newPeak ;
						nmedian = median(noise,5) ;
						det_thresh = thresh(qmedian,nmedian) ;



						if((newPeak > sbpeak) && ((count-qrsDetParas.WINDOW_WIDTH) >= qrsDetParas.MS360))
							{
							sbpeak = newPeak ;
							sbloc = count  - qrsDetParas.WINDOW_WIDTH ;
							}
						}
					}
				}
			


			if((count > sbcount) && (sbpeak > (det_thresh >> 1)))
				{
				System.arraycopy(qrsbuf, 0, qrsbuf, 1, MEMMOVELEN);
				qrsbuf[0] = sbpeak ;
				qmedian = median(qrsbuf,5) ;
				det_thresh = thresh(qmedian,nmedian) ;
				System.arraycopy(rrbuf, 0, rrbuf, 1, MEMMOVELEN);
				rrbuf[0] = sbloc ;
				rrmedian = median(rrbuf,5) ;
				sbcount = rrmedian + (rrmedian >> 1) + qrsDetParas.WINDOW_WIDTH ;
				QrsDelay = count = count - sbloc ;
				QrsDelay += preBlankParas.FILTER_DELAY ;
				sbpeak = 0 ;
				maxder = 0 ;
				initBlank = initMax = rsetCount = 0 ;
				}
			}



		if( qpkcnt == 5 )
			{
			if(++initBlank == qrsDetParas.MS1000)
				{
				initBlank = 0 ;
				rsetBuff[rsetCount] = initMax ;
				initMax = 0 ;
				++rsetCount ;



				if(rsetCount == 5)
					{
					for(i = 0; i < 5; ++i)
						{
						qrsbuf[i] = rsetBuff[i] ;
						noise[i] = 0 ;
						}
					qmedian = median( rsetBuff, 5 ) ;
					nmedian = 0 ;
					rrmedian = qrsDetParas.MS1000 ;
					sbcount = qrsDetParas.MS1500+qrsDetParas.MS150 ;
					det_thresh = thresh(qmedian,nmedian) ;
					initBlank = initMax = rsetCount = 0 ;
					sbpeak = 0 ;
					}
				}
			if( newPeak > initMax )
				initMax = newPeak ;
			}

		return(QrsDelay) ;
		}


	private int Peak( int datum )
		{
		int pk = 0 ;

		if(Peak_timeSinceMax > 0)
			++Peak_timeSinceMax ;

		if((datum > Peak_lastDatum) && (datum > Peak_max))
			{
			Peak_max = datum ;
			if(Peak_max > 2)
				Peak_timeSinceMax = 1 ;
			}

		else if(datum < (Peak_max >> 1))
			{
			pk = Peak_max ;
			Peak_max = 0 ;
			Peak_timeSinceMax = 0 ;
			}

		else if(Peak_timeSinceMax > qrsDetParas.MS95)
			{
			pk = Peak_max ;
			Peak_max = 0 ;
			Peak_timeSinceMax = 0 ;
			}
		Peak_lastDatum = datum ;
		return(pk) ;
		}


	private int median(int[] array, int datnum)
		{
		int i, j, k, temp;
		int[] sort = new int[20] ;
		for(i = 0; i < datnum; ++i)
			sort[i] = array[i] ;
		for(i = 0; i < datnum; ++i)
			{
			temp = sort[i] ;
			for(j = 0; (temp < sort[j]) && (j < i) ; ++j) ;
			for(k = i - 1 ; k >= j ; --k)
				sort[k+1] = sort[k] ;
			sort[j] = temp ;
			}
		return(sort[datnum>>1]) ;
		}


	private int thresh(int qmedian, int nmedian)
		{
		int thrsh, dmed ;
		double temp ;
		dmed = qmedian - nmedian ;
		temp = dmed ;
		temp *= TH ;
		dmed = (int) temp ;
		thrsh = nmedian + dmed ; /* dmed * THRESHOLD */
		return(thrsh) ;
		}


	private boolean BLSCheck(int[] dBuf,int dbPtr, int[] maxder)
		{
		return(false) ;
		}
	}
