package com.spectocor.micor.mobileapp.ecgadapter;

import android.content.Context;

import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.ecgadapter.compression.zip.ZipCompression;
import com.spectocor.micor.mobileapp.ecgblehandler.EcgChannelBluetoothData;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.http.HttpQueueManager;
import com.spectocor.micor.mobileapp.http.requests.EcgChunkHttpRequest;
import com.spectocor.micor.mobileapp.utils.arrays.ArrayConcat;

import java.io.IOException;
import java.sql.SQLDataException;
import java.util.ArrayList;

/**
 * Created by Savio Monteiro on 1/12/2016.
 */
public class LiveEcgChunkInMemory {

    // to maintain live count of samples being buffer
    private static int liveSampleCount = 0;

    private static final int MAX_SAMPLES_FOR_CHUNK = AMainApplication.CHUNK_SIZE_TO_TRANSFER_IN_SECONDS * EcgProperties.SAMPLE_RATE;

    private static ArrayList<Short> ch1Buffer = new ArrayList<>();
    private static ArrayList<Short> ch2Buffer = new ArrayList<>();
    private static long currentChunkStartTimestamp = -1;
    private static String activeSessionID;

    /*public static final String PREFS_ACTIVE_SESSION_ID = "PREFS_ACTIVE_SESSION_ID";
    public static final String KEY_ACTIVE_SESSION_ID = "KEY_ACTIVE_SESSION_ID";*/
    private static void log(String str) {
        MLog.log("LiveEcgChunkInQueueFile: " + str);
    }

    public static long lastTimestampOfOneSecondDataRecd = -1;

    private static void initBuffers(boolean forceInit, long firstTimestamp) {

        if(forceInit || ch1Buffer == null || ch2Buffer == null)
        {
            ch1Buffer = new ArrayList<>();
            ch2Buffer = new ArrayList<>();
            currentChunkStartTimestamp = firstTimestamp;
        }
    }


    public static void initAll() {

        lastTimestampOfOneSecondDataRecd = -1;
        lastChunkCreatedTimestamp = -1;
        lastChunkCreated = null;
        liveSampleCount = 0;

        initBuffers(true, -1);
    }


    private static long lastChunkCreatedTimestamp = -1;
    private static EcgDataChunk lastChunkCreated = null;
    public static EcgDataChunk getLastChunkCreated() {
        return lastChunkCreated;
    }




    /**
     * Storing compressed chunks to local sd-card sqlite database
     * @param compressedEcgDataChunk The EcgDataChunk to save to sd-card sqlite db
     * @param context
     */
    public static void storeCompressedChunk(final EcgDataChunk compressedEcgDataChunk, final Context context) {


        if(compressedEcgDataChunk.chunkStartDateUnixMs < 0) {
            throw new IllegalArgumentException("Chunk start timestamp should be positive");
        }

        compressedEcgDataChunk.chunkStartDateUnixMs *= 1000;

        if(compressedEcgDataChunk == null) {
            throw new NullPointerException("Compressed Ecg Chunk is null");
        }

        log("Saving compressed data to sd card");

        try {

            int sessionId = SessionStateAccess.getCurrentSessionState(context).getSessionId();
            log("SessionId: " + sessionId);

            EcgChunkDbHelper ecgChunkDbHelper = EcgChunkDbHelper.getInstance(context);
            ecgChunkDbHelper.insertEcgChunk(compressedEcgDataChunk);

        } catch (SQLDataException e) {
            log(e.toString());
        }

    }// end fn store-chunk()







    /**
     * Transferring/ Scheduling transfer of compressed chunk ...
     * @param compressedChunk The EcgDataChunk to transfer
     */
    private static void transmitCompressedChunk(final EcgDataChunk compressedChunk) {

        // Transferring
        final EcgChunkHttpRequest ecgChunkHttpRequest = new EcgChunkHttpRequest();

        ecgChunkHttpRequest.setEcgChunkData(compressedChunk);

        log("Transferring Data");

        HttpQueueManager.getInstance().enqueueRequest(ecgChunkHttpRequest, true);

    } // end: transfer-chunk




    /**
     * Indication to buffered handler for transmission of chunk: Pure FIFO case.
     */
    private static void onChunkReadyForTransmission() {

        // Transferring

        log("Notifying for Transfer");

        //HttpQueueManager.getInstance().enqueueRequest(ecgChunkHttpRequest, true);
        HttpQueueManager.getInstance().notifyFifoEcgBufferedHandler();

    } // end: transfer-chunk




    /**
     * Creates compressed chunk from full memory buffer
     * @param context
     * @param currentChunkStartTimestamp
     * @return
     */
    private static EcgDataChunk createCompressedChunkFromCurrentBuffer(Context context, final long currentChunkStartTimestamp) {

        EcgChannelBluetoothData ecgLastChunk = new EcgChannelBluetoothData(currentChunkStartTimestamp, ch1Buffer, ch2Buffer);

        EcgDataChunk ecgDataChunkToStore = null;

        // STORING TO SD CARD
        if(ecgLastChunk == null) {
            log("could not save 5 minutes data");
            return null;
        }
        else {

            // JOINING BOTH CHANNELS AS ONE ARRAY
            final short[] bothChannelsShorts = ArrayConcat.concat(ecgLastChunk.ch1, ecgLastChunk.ch2);


            // COMPRESSING
            log("Compressing ...");
            byte[] compressedBytes = null;

            try {
                compressedBytes = ZipCompression.compressEcg(bothChannelsShorts);
                log("Compressing from " + (ecgLastChunk.ch1.length + ecgLastChunk.ch2.length)*2 + " to " + compressedBytes.length);

            } catch (IOException e) {
                log("Compression Error: " + e.getMessage());
                return null;
            }

            log("Creating compressed 5 minutes ecgdatachunk object");


            //////////////////////==================
            // TODO: talk about this: this is not persistent across app restarts
            /*basePreferenceHelper = new BasePreferenceHelper(context);
            activeSessionID = basePreferenceHelper.getActiveSessionId();*/
            //////////////====================


            //final int sessionId = Integer.valueOf(activeSessionID);
            final int sessionId = SessionStateAccess.getCurrentSessionState(context).getSessionId();

            ecgDataChunkToStore = new EcgDataChunk();
            ecgDataChunkToStore.monitoringSessionId = sessionId;
            ecgDataChunkToStore.chunkDataChannelCompressed = compressedBytes;
            ecgDataChunkToStore.chunkStartDateUnixMs = currentChunkStartTimestamp;

            // saving number of samples too. assuming ch1 size is same as ch2 size.
            ecgDataChunkToStore.numberOfSamples = ecgLastChunk.ch1.length;


        } // ... handled 5 min chunk


        return ecgDataChunkToStore;
    }


    /**
     * Resets variables to indicate that chunk initialization needs to be performed
     */
    private static void indicateChunkInitialization() {
        currentChunkStartTimestamp = -1; // << VERY IMPORTANT
        liveSampleCount = 0; // << VERY IMPORTANT
    }












    /**
     * Appends a one second array of ecg data to the current chunk buffer.
     * This method will also create a chunk once the chunk is full.
     * A full chunk happens when the size of the buffers reach MAX_SAMPLES_FOR_CHUNK.
     *
     * @param ecgData Object comprising of 2 arrays of ecg data corresponding to 2 ecg channels
     * @param context
     * @param forceCreateChunk If true, will force the creation of the chunk no matter what the size of the buffer is.
     */
    public static void appendOneSecondEcgToLiveChunkBuffer(final EcgChannelBluetoothData ecgData, final Context context, final boolean forceCreateChunk) {

        // checking if data is received in order of time
        if(lastTimestampOfOneSecondDataRecd >= ecgData.timestamp && !forceCreateChunk) {
            throw new IllegalArgumentException("Timestamp of ecgData is older or same as the previous data received");
        }

        /*
            log("Start Live Again? " + startAgain);
            log("liveSampleCount >= MAX_SAMPLES_FOR_CHUNK? " + (liveSampleCount >= MAX_SAMPLES_FOR_CHUNK));
        */

        // suggests that there has been a recent disconnection or a/many dropped 1-second packet/packets
        boolean timestampTooFarFromLastRecd = ((lastTimestampOfOneSecondDataRecd != -1) && (ecgData.timestamp - lastTimestampOfOneSecondDataRecd) >= 2);
        boolean fiveMinuteFull = (liveSampleCount >= MAX_SAMPLES_FOR_CHUNK);

        if(fiveMinuteFull || timestampTooFarFromLastRecd || forceCreateChunk)
        {

            if(fiveMinuteFull) {
                log("Five Min Full. Current Time: " + System.currentTimeMillis());
            }
            if(timestampTooFarFromLastRecd) {
                log("Timestamp too far from last received: " + timestampTooFarFromLastRecd);
                log("Last Timestamp Received: " + lastTimestampOfOneSecondDataRecd + " vs " + ecgData.timestamp);
            }
            if(forceCreateChunk) {
                log("Force-creating Chunk");
            }


            final EcgDataChunk compressedChunk = createCompressedChunkFromCurrentBuffer(context, currentChunkStartTimestamp);

            lastChunkCreated = compressedChunk;
            lastChunkCreatedTimestamp = System.currentTimeMillis();

            // not storing here: will store after http transfer attempt
            if(AMainApplication.PURE_FIFO_CHUNK_TRANSFER)
            {
                storeCompressedChunk(compressedChunk, AMainApplication.getAppContext());
                onChunkReadyForTransmission();
            }
            else {
                transmitCompressedChunk(compressedChunk);
            }

            log("CHUNK READY. " + compressedChunk.chunkStartDateUnixMs);

            // Set INDICATORS THAT CHUNK Buffers NOW NEED TO BE RE-INITIALIZED
            indicateChunkInitialization();

        } // end of if-MAX_SAMPLES





        // TODO: assuming ch2.length is same as ch1.length
        // ... if STL API does not 0-pad or make ch1.length == ch2.length
        //      .. ecgHandler will have to handle it

        liveSampleCount += ecgData.ch1.length;


        // at end of every chunk fill-up, buffers are initialized and timestamp is set to -1,
        // .. as set in the above if-case
        // .. at this point the currentChunkStartTimestamp can be set to the timestamp of the first
        // .. one second list received from the ecg device
        if(currentChunkStartTimestamp == -1) {
            initBuffers(true, ecgData.timestamp);
            log("INIT CHUNK TIMESTAMP: " + ecgData.timestamp);
        }

        log("So far: " + liveSampleCount + " samples. ("+ecgData.timestamp+")");

        // saving 400 short samples as byte array
        for(int i = 0; i < ecgData.ch1.length; i++) {
            ch1Buffer.add(ecgData.ch1[i]);
        }
        for(int i = 0; i < ecgData.ch2.length; i++) {
            ch2Buffer.add(ecgData.ch2[i]);
        }

        // if there was a disconnection, there will be a delay between current timestamp and
        // .. last recorded timestamp
        // .. at this point the currentChunkStartTimestamp can be set to the timestamp of the first
        // .. one second list received from the ecg device

        if(timestampTooFarFromLastRecd)
            lastTimestampOfOneSecondDataRecd = -1;
        else
            lastTimestampOfOneSecondDataRecd = ecgData.timestamp;

    } // end: function





}


