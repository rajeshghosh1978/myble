package com.spectocor.micor.mobileapp.threading;

import android.os.AsyncTask;

/**
 * Created by Umair Ahmed on 2/25/2016.
 * <p/>
 * <p/>
 ***********USAGE************/
/*DefaultAsyncTask asyncTask = new DefaultAsyncTask(new DefaultAsyncListener() {
            @Override
            public Object doInBackgroundThread(DefaultAsyncTask httpAsyncTask) {

                //Do heavy/background Task here
                return null;
            }

            @Override
            public void onCompleted(Object object) {

                //Update UI if needed OR TO do anything on completion
            }

            @Override
            public void onException(Exception exception) {
                //Deal here if you got any exception in your background Thread
            }
        });
        asyncTask.executeOnExecutor(DefaultAsyncTask.THREAD_POOL_EXECUTOR);*/


public class DefaultAsyncTask extends AsyncTask<Object, Object, Object> {

    DefaultAsyncListener defaultAsyncListener;
    DefaultProgressListener defaultProgressListener;

    public DefaultAsyncTask(DefaultAsyncListener httpResponseListener) {
        this.defaultAsyncListener = httpResponseListener;

    }

    /**
     * Use this if you want to show the progress of your task.
     */
    public DefaultAsyncTask(DefaultAsyncListener httpResponseListener, DefaultProgressListener defaultProgressListener) {
        this.defaultProgressListener = defaultProgressListener;

    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            return defaultAsyncListener.doInBackgroundThread(this);
        } catch (Exception e) {
            return e;
        }
    }

    public void updateProgress(Object value) {
        publishProgress(value);
    }

    @Override
    protected void onProgressUpdate(Object... progress) {
        if (defaultProgressListener == null)
            return;
        defaultProgressListener.onProgress(progress[0]);
    }

    @Override
    protected void onPostExecute(Object result) {
        if (defaultAsyncListener == null)
            return;

        if (result instanceof Exception) {
            defaultAsyncListener.onException((Exception) result);
            return;
        }
        if (result != null) {
            defaultAsyncListener.onCompleted(result);
        }
    }

}