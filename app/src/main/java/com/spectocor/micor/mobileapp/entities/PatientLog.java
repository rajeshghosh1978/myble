package com.spectocor.micor.mobileapp.entities;

import com.spectocor.micor.mobileapp.amain.AMainApplication;

import java.util.ArrayList;

/**
 * Created by Admin on 2/24/2016.
 */
public class PatientLog {
   private ArrayList<PatientActivation> arrayListPatientActivation;
    //BasePreferenceHelper basePreferenceHelper;

    public ArrayList<PatientActivation> getPatientActicationLogs(){
        return  arrayListPatientActivation;
    }

    public void addPatientActivationLogs(PatientActivation pa){

        if(arrayListPatientActivation==null || arrayListPatientActivation.size()==0){
            arrayListPatientActivation = new ArrayList<>();
        }

        if(arrayListPatientActivation.size()==3){
            arrayListPatientActivation.remove(2);
            arrayListPatientActivation.add(pa);
            /*basePreferenceHelper.putSessionLogs(arrayListPatientActivation);*/
        }
        else{
            arrayListPatientActivation.add(pa);
            /*basePreferenceHelper.putSessionLogs(arrayListPatientActivation);*/

        }

    }
}
