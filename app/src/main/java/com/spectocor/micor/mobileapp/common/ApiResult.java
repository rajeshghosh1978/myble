package com.spectocor.micor.mobileapp.common;

import com.google.gson.JsonElement;

/**
 * Results coming out of the api calls
 */
public class ApiResult {
    /**
     * Version of our API that returned the value
     * currently we fixed it, but versions should
     * be handled later
     */
    public String apiVersion = "";
    /**
     * HTTP status code. For example, 200 for OK
     */
    private int statusCode = 0;
    /**
     * if the request was successful or not
     */
    private boolean success;
    /**
     * returned message. If success was false,
     * it is an error message
     */
    private String message;
    /**
     * Data related to the results
     */
    private JsonElement data;

    public final int getstatusCode() {
        return statusCode;
    }

    public final void setstatusCode(int value) {
        statusCode = value;
    }

    public final boolean getsuccess() {
        return success;
    }

    public final void setsuccess(boolean value) {
        success = value;
    }

    public final String getmessage() {
        return message;
    }

    public final void setmessage(String value) {
        message = value;
    }

    public final JsonElement getdata() {
        return data;
    }

    public final void setdata(JsonElement value) {
        data = value;
    }
}

