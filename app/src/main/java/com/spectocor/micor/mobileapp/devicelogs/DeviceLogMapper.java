package com.spectocor.micor.mobileapp.devicelogs;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 3/4/2016.
 */
/*
USAGE
This class will be use to send the pileup logs, Plz don't change any annotation here.

 */
public class DeviceLogMapper {

    @SerializedName("sessionId")
    private String sessionId;

    @SerializedName("deviceId")
    private String deviceId;

    @SerializedName("ecgId")
    private String ecgId;

    @SerializedName("log")
    private List<Logs> logs = new ArrayList<Logs>();

    /**
     *
     * @return
     * The sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     *
     * @param sessionId
     * The deviceId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     *
     * @return
     * The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     *
     * @param deviceId
     * The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     *
     * @return
     * The ecgId
     */
    public String getEcgId() {
        return ecgId;
    }

    /**
     *
     * @param ecgId
     * The ecgId
     */
    public void setEcgId(String ecgId) {
        this.ecgId = ecgId;
    }

    /**
     *
     * @return
     * The logs
     */
    public String getLogsAsString() {

        if(logs!=null && logs.size()>0)
        {
            Gson gson = new Gson();
            Type type = new TypeToken<List<Logs>>(){}.getType();
            String objInStr = gson.toJson(logs, type);

            if(objInStr!=null && !objInStr.isEmpty())
                return  objInStr;
        }
        return "";
    }

    public List<Logs> getLogs() {
        return logs;
    }

    /**
     *
     * @param logs
     * The logs
     */
    public void setLogs(List<Logs> logs) {
        this.logs = logs;
    }


}
