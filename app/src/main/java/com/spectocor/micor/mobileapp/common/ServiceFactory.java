package com.spectocor.micor.mobileapp.common;

import android.content.Context;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.settings.GlobalSettings;
import com.spectocor.micor.mobileapp.common.datahelpers.EcgDatabaseHelper;
import com.spectocor.micor.mobileapp.common.datahelpers.LogEventsDatabaseHelper;
import com.spectocor.micor.mobileapp.common.datahelpers.QueueDatabaseHelper;
import com.spectocor.micor.mobileapp.devicelogs.ApplicationLogService;
import com.spectocor.micor.mobileapp.devicelogs.BatteryLogService;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogLogger;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogQueueRepository;
import com.spectocor.micor.mobileapp.devicelogs.DeviceLogRepository;
import com.spectocor.micor.mobileapp.devicelogs.DiskSpaceCalculator;
import com.spectocor.micor.mobileapp.devicelogs.DiskSpaceLogService;
import com.spectocor.micor.mobileapp.devicelogs.EcgDeviceLogService;
import com.spectocor.micor.mobileapp.devicelogs.NetworkStateLogService;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientReportedEventQueueRepository;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientReportedEventRepository;
import com.spectocor.micor.mobileapp.patientreportedevents.PatientReportedEventService;


// Developer Note: by Mohammad Ali Ghaderi
// This class may be replaced with Dagger when the application grow bigger
// For now, I decided not to add any unnecessary dependency to the project

// Developer Note:
// To make system thread-safe, non of the classes should keep any state (using static members)
// ServiceFactory instance is unique in the application, so, everything created here should be
// thread-safe


/**
 * Service factory that creates all services inside the application
 * Services are going to be used by user interface or other services in the application
 */
public class ServiceFactory {

    // Developer Note: by Mohammad Ali Ghaderi
    // Singleton pattern
    // since it needs Application Context for database management, we can't use pure singleton here

    /**
     * instance of service factory class
     */
    private static ServiceFactory instance = null;

    /**
     * Global settings for all application
     */
    private final GlobalSettings globalSettings;


    /*********************************** Data Helpers **********************************
    /**
     * Database helper to read and write in queue database
     */
    private final QueueDatabaseHelper queueDatabaseHelper;
    /**
     * Database helper to read and write in log events database
     */
    private final LogEventsDatabaseHelper logEventsDatabaseHelper;
    /**
     * Database helper to read and write in Ecg database
     */
    private final EcgDatabaseHelper ecgDatabaseHelper;


    /*********************************** Device Logs **********************************

    /**
     * Repository to read and write device logs in the database
     */
    private final DeviceLogRepository deviceLogRepository;

    /**
     * Repository to read and write device logs in the queue
     */
    private final DeviceLogQueueRepository deviceLogQueueRepository;

    /**
     * service to log things in the database
     */
    private final DeviceLogLogger deviceLogLogger;


    /*********************************** Patient Reported Events **************************
    /**
     * Repository to read and write Patient reported events in the queue
     */
    private final PatientReportedEventQueueRepository patientReportedEventQueueRepository;

    /**
     * Repository to read and write Patient reported events
     */
    private final PatientReportedEventRepository patientReportedEventRepository;


    //*********************************** Ecg Data Chunk **********************************

    /**
     * Repository to read and write data chunks
     *//*
    private final EcgDataChunkRepository ecgDataChunkRepository;

    *//**
     * Repository to read and write Ecg Data Chunk in the queue
     *//*
    private final EcgDataChunkQueueRepository ecgDataChunkQueueRepository;
*/

    /**
     * Service factory constructor
     *
     * @param context application context
     */
    private ServiceFactory(Context context) {
        globalSettings = new GlobalSettings();

        queueDatabaseHelper = new QueueDatabaseHelper(context);
        logEventsDatabaseHelper = new LogEventsDatabaseHelper(context);
        ecgDatabaseHelper = new EcgDatabaseHelper(context);

        deviceLogRepository = new DeviceLogRepository(logEventsDatabaseHelper);
        deviceLogQueueRepository = new DeviceLogQueueRepository(queueDatabaseHelper);
        deviceLogLogger = new DeviceLogLogger(deviceLogRepository, globalSettings, deviceLogQueueRepository);

        patientReportedEventRepository = new PatientReportedEventRepository(logEventsDatabaseHelper);
        patientReportedEventQueueRepository = new PatientReportedEventQueueRepository(queueDatabaseHelper);

        /*ecgDataChunkQueueRepository = new EcgDataChunkQueueRepository(queueDatabaseHelper);
        ecgDataChunkRepository = new EcgDataChunkRepository(ecgDatabaseHelper);*/
    }

    /**
     * gets an instance of service factory. This instance is unique
     *
     * @return the instance of service factory
     */
    public static ServiceFactory getInstance() {
        if (instance == null) {
            // Exception handling removed because we create the factory singleton in the beginning. It should never be null
//            Log.e("ServiceFactory", "getInstance(), instance is null.");
//            throw new IllegalArgumentException();
            //throw new Exception("Instance has not been created. Use createInstance method to create one");
            createInstance(AMainApplication.getAppContext());
        }

        return instance;
    }

    /**
     * Creates a service factory instance.
     *
     * @param applicationContext application context created in onCreate of Application class
     */
    public static void createInstance(Context applicationContext) {
        synchronized (ServiceFactory.class) {

            if (applicationContext == null)
                throw new IllegalArgumentException("applicationContext should not be null");

            if (instance == null) {  // if it was not created before
                instance = new ServiceFactory(applicationContext);
            }
        }
    }

    /**
     * Gets device log service object to be used for log disk space data
     * @return service class
     */
    public DiskSpaceLogService getDiskSpaceLogService() {
        return new DiskSpaceLogService(new DiskSpaceCalculator(), deviceLogLogger);
    }

    /**
     * gets application log service
     * @return service class
     */
    public ApplicationLogService getApplicationLogService() {
        return new ApplicationLogService(deviceLogLogger);
    }

    /**
     * gets log class for logging battery statuses
     * @return service class
     */
    public BatteryLogService getBatteryLogService() {
        return new BatteryLogService(deviceLogLogger);
    }

    /**
     * gets log class for logging statuses of Ecg Transmitter
     * @return service class
     */
    public EcgDeviceLogService getEcgTransmitterLogService() {
        return new EcgDeviceLogService(deviceLogLogger);
    }

    /**
     * gets log class for logging network events of device
     * @return service class
     */
    public NetworkStateLogService getNetworkStateLogService(){
        return new NetworkStateLogService(deviceLogLogger);
    }


    /**
     * get service to insert patient reported events
     *
     * @return patient reported events service object
     */
    public PatientReportedEventService getPatientReportedEventService() {
        return new PatientReportedEventService(patientReportedEventRepository, globalSettings, patientReportedEventQueueRepository);
    }

   /* *//**
     * Service to manage compressed chunks on the client
     *
     * @return data chunk service
     *//*
    public EcgDataChunkService getEcgDataChunkService() {
        return new EcgDataChunkService(ecgDataChunkRepository, globalSettings, ecgDataChunkQueueRepository);
    }
*/



}
