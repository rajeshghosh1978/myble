package com.spectocor.micor.mobileapp.http;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.amain.DeviceIdentifiers;
import com.spectocor.micor.mobileapp.amain.state.SessionStateAccess;
import com.spectocor.micor.mobileapp.common.Log;
import com.spectocor.micor.mobileapp.ecgstorage.EcgChunkDbHelper;
import com.spectocor.micor.mobileapp.ecgstorage.EcgDataChunk;
import com.spectocor.micor.mobileapp.http.common.ResponseCodes;
import com.spectocor.micor.mobileapp.http.requests.EcgChunkHttpRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.SQLDataException;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Handler for sending buffered data to network.
 *
 * Created by Rafay Ali on 2/16/2016.
 */
public class HttpBufferedRequestHandler extends Handler{

    public static final String TAG = HttpBufferedRequestHandler.class.getSimpleName();

    private boolean msgBeingHandled = false;

    public HttpBufferedRequestHandler(Looper looper){
        super(looper);
    }

    @Override
    public void handleMessage(Message msg) {
        msgBeingHandled = true;
        Log.v(TAG, "Message is now being handled...");

        try {
            EcgDataChunk ecgDataChunk;

            while (true){
                ecgDataChunk = EcgChunkDbHelper.getInstance(AMainApplication.getAppContext()).retrieveOldestNotTransmittedChunk();

                if (ecgDataChunk == null){
                    Log.v(TAG, "EcgChunk is null, we assume there are no remaining entries in db to be send, finishing loop.");
                    break;
                }

                RequestBody requestBody = new MultipartBody.Builder()
                        .addFormDataPart("sessionId", "" + SessionStateAccess.getCurrentSessionState(AMainApplication.getAppContext()).getSessionId())
                        .addFormDataPart("chunkStartTimestamp", "" + System.currentTimeMillis())
                        .addFormDataPart("ecgId", DeviceIdentifiers.getEcgId())
                        .addFormDataPart("deviceId", DeviceIdentifiers.getPdaId())
                        .addFormDataPart("file", "data.comp",
                                RequestBody.create(MediaType.parse("application/octet-stream"), ecgDataChunk.chunkDataChannelCompressed))
                        .setType(MultipartBody.FORM)
                        .build();

                Log.v(TAG, "Multipart request body created for " + EcgChunkHttpRequest.class.getSimpleName()
                        + " with filename data.comp");

                Request request = new Request.Builder()
                        .header("Authorization", "Basic c2lyYWo6dGVzdDEyMw==")
                        .url(AMainApplication.SERVER_ADDRESS + AMainApplication.URL_ECG_CHUNK)
                        .post(requestBody)
                        .build();

                Response response;
                Call c = HttpManager.getInstance().getHttpClient().newCall(request);
                response = c.execute();

                String responseBody = response.body().string();
                JSONObject responseJSON = new JSONObject(responseBody);
                int responseCode = responseJSON.getInt("code");

                Log.v(TAG, "StatusCode: " + responseCode + ", ResponseBody: " + responseBody);
                if (responseCode == ResponseCodes.OK){
                    Log.v(TAG, "Response is successful, finishing request.");
                }
                else {
                    Log.v(TAG, "There was an error sending buffered ecg chunk. " +
                            "We have no retries left, marking as sent in database anyway...");
                }

                Log.v(TAG, "Removing EcgChunk from database... ");
                EcgChunkDbHelper.getInstance(AMainApplication.getAppContext()).setEcgChunkAsTransmitted(
                        ecgDataChunk.getMonitoringSessionId(), ecgDataChunk.getChunkStartDateUnixMs()
                );
                Log.v(TAG, "EcgChunk entry marked as sent in database.");
            }
        }
        catch(SQLDataException e){
            Log.v(TAG, "Looks like we haven't found any entry in database to send over network. Stopping work!");
        } catch (IOException e) {
            Log.v(TAG, "Error while sending buffered ecg chunk over the network!");
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.v(TAG, "Message is not longer being handled! ");
        msgBeingHandled = false;
    }

    public boolean isMsgBeingHandled() {
        return msgBeingHandled;
    }
}
