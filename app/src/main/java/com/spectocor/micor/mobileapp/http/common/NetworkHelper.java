package com.spectocor.micor.mobileapp.http.common;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;

import com.spectocor.micor.mobileapp.amain.AMainApplication;
import com.spectocor.micor.mobileapp.common.Log;

import java.util.List;

public class NetworkHelper {


	private static final String TAG = NetworkHelper.class.getSimpleName();

	public static boolean isNetworkAvailable(Context context) {

		ConnectivityManager manager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if(manager == null)
			return false;

		// 3g-4g available
		boolean is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.isConnectedOrConnecting();
		// wifi available
		boolean isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
				.isConnectedOrConnecting();

		System.out.println(is3g + " net " + isWifi);

		if (!is3g && !isWifi) {
			return false;
		} else
			return true;

	}


	public static boolean isInternetAvailable(Context mContext) {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public static boolean ifWifiEnable(){
		WifiManager wifi = (WifiManager) AMainApplication.getAppContext().getSystemService(Context.WIFI_SERVICE);
		if (wifi.isWifiEnabled()){
			return true;
		}

		return false;
	}

	public static String getNetworkOperatorName(){
		TelephonyManager manager = (TelephonyManager)AMainApplication.getAppContext().getSystemService(Context.TELEPHONY_SERVICE);
		return manager.getNetworkOperatorName();
	}

	public static String getLatitude(){
		LocationManager locationManager = (LocationManager) AMainApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
		Location lastKnownLocation = null;
        for (int i = providers.size()-1; i >= 0; i--) {
            lastKnownLocation = locationManager.getLastKnownLocation(providers.get(i));
            if (lastKnownLocation != null)
                break;
        }
        if (lastKnownLocation == null){
            Log.v(TAG, "LOCATION is null");
            return "NOT FOUND";
        }
		double latitude = lastKnownLocation.getLatitude();
		Log.v(TAG, "Latitude: " + latitude);
        return latitude + "";
	}

    public static String getLongitude(){
        LocationManager locationManager = (LocationManager) AMainApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location lastKnownLocation = null;
        for (int i = providers.size()-1; i >= 0; i--) {
            lastKnownLocation = locationManager.getLastKnownLocation(providers.get(i));
            if (lastKnownLocation != null)
                break;
        }
        if (lastKnownLocation == null){
            Log.v(TAG, "LOCATION is null");
            return "NOT FOUND";
        }
        double longitude = lastKnownLocation.getLongitude();
        Log.v(TAG, "Longitude: " + longitude);
        return longitude + "";
    }

}
