package com.spectocor.micor.mobileapp.ecgblehandler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.spectocor.micor.mobileapp.amain.MLog;
import com.spectocor.micor.mobileapp.amain.AMainActivity;
import com.spectocor.micor.mobileapp.common.ObjectUtils;
import com.spectocor.micor.mobileapp.amain.settings.EcgProperties;
import com.spectocor.micor.mobileapp.ecgadapter.EcgResampler;
import com.spectocor.micor.mobileapp.ecgadapter.mobileheartrate.QRSDetector;
import com.spectocor.micor.mobileapp.ecgadapter.mobileheartrate.QRSDetectorParameters;
import com.spectocor.micor.mobileapp.ecgadapter.mobileheartrate.QRSFilterer;
import com.stl.bleservice.BluetoothComm;
import com.stl.bleservice.Device;
import com.stl.bleservice.EventCallback;
import com.stl.bleservice.STLBLE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Savio Monteiro on 1/21/2016.
 */
public class SynergenBleClientSessionActivation {

    private static Context mContext;
    private static QRSDetector qrsDetector;

    private static final int MIN_HEART_RATE = 30;
    private static final int MAX_HEART_RATE = 300;
    private static final int MAX_SECONDS_SINCE_CONNECTED = 30;
    private static final int MAX_SECONDS_SINCE_HEART_RATE_DETECTED = 10;

    private static void log(String str) {
        MLog.log("SynergenBleClientSessionActivation: " + str);
    }

    /**
     * Method to create an Intent for broadcast
     * @param b
     * @return
     */
    private static Intent createBtEventIntent(Bundle b) {
        Intent i = new Intent("BLUETOOTH_EVENT");
        i.putExtras(b);
        return i;
    }

    public static void lookupStlDevices(final Context context, final String callbackFnStr) {

        mContext = context;
        final ArrayList<StlDeviceInfo> stlDevicesFound = new ArrayList<>();

        if(context == null) {
            throw new NullPointerException("Context cannot be null");
        }

        log("Searching for available devices ... ");

        STLBLE.getInstance(context).ScanForDevices(new STLBLE.ScannerCallBack() {
            @Override
            public void onScanFinished(STLBLE.ScanResult scanResult) {

                for (Device dev : scanResult.getDeviceList()) {

                    if(dev.getDeviceName() == null) {
                        continue;
                    }

                    log("Found: " + dev.getDeviceName().trim() + ", " + dev.getAddress());

                    String btName = dev.getDeviceName().trim() + "";
                    String btMac = dev.getAddress() + "";
                    int rssi = dev.getRssi() + 0;

                    if (!btName.contains("STL ECG")) {
                        continue;
                    }

                    if (btName.contains("null")) {
                        continue;
                    }

                    stlDevicesFound.add(new StlDeviceInfo(btName, btMac, rssi));

                } // end of for-devices

                String devsFoundJSON = ObjectUtils.toJson(stlDevicesFound);
                log("Found BT Devs: " + devsFoundJSON);

                Bundle b = new Bundle();
                b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICES_FOUND);
                b.putString("stlDevicesFoundJson", devsFoundJSON);
                b.putString("callbackFnString", callbackFnStr);
                Intent btIntent = createBtEventIntent(b);
                LocalBroadcastManager.getInstance(context).sendBroadcast(btIntent);
            }
        });
    }


    private static int ourDeviceScanCount = 0;
    private static Device ourDev;

    private static void initQrsDetector() {

        QRSDetectorParameters qrsDetectorParameters = new QRSDetectorParameters(EcgProperties.SAMPLE_RATE) ;
        qrsDetector = new QRSDetector(qrsDetectorParameters) ;
        QRSFilterer qrsFilterer = new QRSFilterer(qrsDetectorParameters) ;
        qrsDetector.setObjects(qrsFilterer);

    }

    public static void connectToStlDevice(final Context context, final String mac) {

        mContext = context;
        log("Looking up: " + mac);


        STLBLE.getInstance(context).ScanForDevices(new STLBLE.ScannerCallBack() {

            boolean found = false;

            @Override
            public void onScanFinished(STLBLE.ScanResult scanResult) {

                log("onScanResult: " + scanResult.getDeviceList().size() + " devices found!");

                for (final Device dev : scanResult.getDeviceList()) {
                    log("Found: " + dev.getAddress() + " (" + dev.getDeviceName() + ")");
                    if (dev.getAddress().contains(mac)) {

                        found = true;
                        ourDeviceScanCount = 0;

                        log("Our device found: " + mac);

                        initQrsDetector();
                        registerAndConnect(dev.getBluetoothComm(context));
                    }
                }

                if (!found) {
                    ourDeviceScanCount++;
                    connectToStlDevice(context, mac);
                }
            }
        });


        Bundle b = new Bundle();
        b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTING);
        Intent btIntent = createBtEventIntent(b);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(btIntent);
    }



    private static int secondsAfterHeartRateDetected = 0;
    private static int secondsSinceConnected = 0;
    private static boolean hrDetected = false;
    private static int samplesSinceLastR = 0;
    private static void registerAndConnect(final BluetoothComm btComm) {

        hrDetected = false;

        btComm.register(new EventCallback() {
            @Override
            public void onData(Long timestamp, final List<Integer> ch1List, List<Integer> ch2List, List<Integer> motionList) {

                //secondsSinceConnected++;
                log("Seconds since connected = " + secondsSinceConnected + " second");

                if (secondsAfterHeartRateDetected > MAX_SECONDS_SINCE_HEART_RATE_DETECTED) {

                    log("secondsAfterHeartRateDetected > " + MAX_SECONDS_SINCE_HEART_RATE_DETECTED);

                    Bundle b1 = new Bundle();
                    b1.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_HEARTBEAT_DETECTED);
                    Intent btIntent1 = createBtEventIntent(b1);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(btIntent1);


                    btComm.disconnect();
                    btComm.unregister();

                    log("Disconnecting and Unregistering.");

                    return;

                }


                if(secondsSinceConnected >= MAX_SECONDS_SINCE_CONNECTED) {

                    log("secondsSinceConnected  > " + MAX_SECONDS_SINCE_CONNECTED);

                    Bundle b1 = new Bundle();
                    b1.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_COULD_NOT_DETECT_HEARTBEAT);
                    Intent btIntent1 = createBtEventIntent(b1);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(btIntent1);


                    log("Disconnecting and Unregistering.");

                    log("Disconnecting ... ");
                    btComm.disconnect();
                    log(" ... done");

                    log("Unregistering ... ");
                    btComm.unregister();
                    log(" ... done");

                    return;
                }


                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        // simulated data
                        // ArrayList<Short> eachSecondCh1 = EcgSimulatorThread.getInstance(mContext).getRandomOneSecondCh1();
                        //for(short sample:eachSecondCh1) {


                        short[] ch1 = EcgResampler.resample(400, EcgProperties.SAMPLE_RATE, ch1List);

                        for(int sample:ch1) {

                            if(qrsDetector.QRSDet(sample) != 0) {

                                try {
                                    Thread.sleep(2);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                int distance = samplesSinceLastR;
                                float seconds = (float) distance / (float) EcgProperties.SAMPLE_RATE;

                                log("\t\tDistance: " + distance + " samples. " + seconds + " seconds apart");

                                int hr = (int) (float) (60.0f / seconds);

                                if(hr > MIN_HEART_RATE && hr < MAX_HEART_RATE) {
                                    hrDetected = true;
                                    log("\tHR: " + hr);
                                }

                                samplesSinceLastR = 0;
                            }
                            else {
                                samplesSinceLastR++;
                            }
                        }


                        if(hrDetected) {
                            secondsAfterHeartRateDetected++;
                        }

                        //log(">> Finished heart rate detection for one second");

                    }
                }).start();



            }

            @Override
            public void onOldData(Long aLong, List<Integer> list, List<Integer> list1, List<Integer> list2) {

            }

            @Override
            public void onDeviceBatteryUpdate(int i) {

            }

            @Override
            public void onLeadChange(boolean b) {

            }

            @Override
            public void onDeviceError(int i) {

            }

            @Override
            public void onConnectionChange(BluetoothComm.CONNECTION_STATUS previousState, BluetoothComm.CONNECTION_STATUS newState) {

                log("onConnectionChange: from " + previousState + " to " + newState);

                if (newState == BluetoothComm.CONNECTION_STATUS.CONNECTING) {

                    Bundle b = new Bundle();
                    b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTING);
                    Intent btIntent = createBtEventIntent(b);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(btIntent);

                }
                else if (newState == BluetoothComm.CONNECTION_STATUS.CONNECTED) {

                    Bundle b = new Bundle();
                    b.putInt("EVENT_TYPE", AMainActivity.WEBVIEW_SESSION_ACTIVATION_LE_DEVICE_CONNECTED);
                    Intent btIntent = createBtEventIntent(b);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(btIntent);

                    secondsAfterHeartRateDetected = 0;
                    secondsSinceConnected = 0;

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            secondsSinceConnected = 0;
                            while(secondsSinceConnected < MAX_SECONDS_SINCE_CONNECTED) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                secondsSinceConnected++;
                            }

                        }
                    }).start();

                }
                else if (newState == BluetoothComm.CONNECTION_STATUS.DISCONNECTED) {
                    log("DISCONNECTED");

                    btComm.unregister();
                    registerAndConnect(btComm);
                }
                else if (newState == BluetoothComm.CONNECTION_STATUS.CLOSED) {
                    log("CLOSED");
                    log("unregistering");

                    btComm.unregister();
                    registerAndConnect(btComm);

                }
            }

            @Override
            public void onFailToConnect() {

            }
        });

        log("Event Callback Registered ... ");

        log("Connecting ... ");
        btComm.connect();
    }

}
