package com.spectocor.micor.mobileapp.devicelogs;

/**
 * Data structure for holding device log event.
 *
 * Created by Rafay Ali on 2/26/2016.
 */
/*
    This class represents the data type of DeviceLogsDbHelper

 */

public class DeviceLogEntity {



    private int sessionId = -1;
    private long insertTimestamp;
    private String eventMessage;
    private long transmittedTimestamp;
    private EnumDeviceLogInfoType eventType;
    private int eventTypeId;

    public DeviceLogEntity(int sessionId, String data){
        setSessionId(sessionId);
        setInsertTimestamp(System.currentTimeMillis());
        setEventMessage(data);
        setTransmittedTimestamp(-1);
    }

    public DeviceLogEntity(int sessionId, String data, EnumDeviceLogInfoType eventType){
        setSessionId(sessionId);
        setInsertTimestamp(System.currentTimeMillis());
        setEventMessage(data);
        setTransmittedTimestamp(-1);
        setEventType(eventType);
    }

    public DeviceLogEntity(){

    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public long getInsertTimestamp() {
        return insertTimestamp;
    }

    public void setInsertTimestamp(long insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String message) {
        this.eventMessage = message;
    }

    public long getTransmittedTimestamp() {
        return transmittedTimestamp;
    }

    public void setTransmittedTimestamp(long transmittedTimestamp) {
        this.transmittedTimestamp = transmittedTimestamp;
    }

    public  void setEventType(EnumDeviceLogInfoType eventType)
    {
        this.eventType = eventType;
    }

    public EnumDeviceLogInfoType getEventType()
    {
        return this.eventType;
    }


    public  void setEventTypeId(int eventTypeId)
    {
        this.eventTypeId = eventTypeId;
    }

    public int getEventTypeId()
    {
        return this.eventTypeId;
    }

}
