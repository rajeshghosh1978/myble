package com.spectocor.micor.mobileapp.ecgchunktransmission;

/**
 * Created by Savio Monteiro on 1/17/2016.
 */
public class ChunkTransferId {

    public int sessionId = -1;
    public long startTimestampMs = -1;

    public ChunkTransferId(int sessionId, long startTimestampMs) {
        this.sessionId = sessionId;
        this.startTimestampMs = startTimestampMs;
    }

}
