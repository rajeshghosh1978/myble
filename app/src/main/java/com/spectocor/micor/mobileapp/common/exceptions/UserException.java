package com.spectocor.micor.mobileapp.common.exceptions;

/**
 * Exceptions that needs to be shown to the User and not be logged
 */
public class UserException extends Exception {

    public UserException(String message) {
        super(message);
    }


}
