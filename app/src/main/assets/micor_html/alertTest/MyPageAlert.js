jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", -10 + Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
    return this;
}

var MyPageAlert = function() {};

MyPageAlert.closeAlert = function() {
	
	$("#sm-myalert2").fadeOut();
	$("#sm-myalert2-bg").fadeOut();
	
	$("#sm-myalert2").remove();
	$("#sm-myalert2-bg").remove();
}

MyPageAlert.closeAlert = function(continuingFn, params) {
	
	$("#sm-myalert2").fadeOut();
	$("#sm-myalert2-bg").fadeOut();
	
	$("#sm-myalert2").remove();
	$("#sm-myalert2-bg").remove();
	
	if(continuingFn != null)
		continuingFn(params);
}
  
MyPageAlert.toast = function(str) { 
	
	var bgDiv = $("<div>");
	bgDiv.prop("id", "sm-myalert2-bg");
	bgDiv.css("opacity", 0.8);
	bgDiv.css("width","100%");
	bgDiv.css("height","100%");
	bgDiv.css("background-color", "#888");
	bgDiv.css('box-shadow', '0px 0px 30px 3px #000 inset');
	bgDiv.css({"z-index":199});
	bgDiv.css({"position":"absolute", "top":"0px", "left":"0px"});
	
	
var alertDiv = $("<div>");
	alertDiv.prop("id", "sm-myalert2");
	alertDiv.prop("class", "myalert");
		
	alertDiv.css("opacity", 1.0);
	alertDiv.css("width","400px");
	alertDiv.css("height","200px");
	alertDiv.css("padding", "30px");
	alertDiv.css("background-color", "white");
	alertDiv.css("vertical-align","middle");
	alertDiv.css("text-align", "center");
	alertDiv.css('box-shadow', '0px 0px 30px 3px #555');
	alertDiv.css("border-radius", "10px");
	alertDiv.css({"z-index":200, "padding": 10});
	
	alertDiv.html("<div id='sm_myalert2_innerText'><br /><big><b>" + str.replace("\n", "<br />") + "</b></div>");

	alertDiv.center();
	alertDiv.enhanceWithin();
	
	bgDiv.hide();
	alertDiv.hide();
	
	$("#sm_myalert2_innerText").center();
	$("#sm_myalert2_innerText").enhanceWithin();
	
	var currPageId = $.mobile.activePage.attr('id');
	$("#" + currPageId).append(bgDiv);
	$("#" + currPageId).append(alertDiv);
	
	bgDiv.fadeIn();
	alertDiv.fadeIn();
	
	setTimeout(function() {MyPageAlert.closeAlert()}, 3000);
}

MyPageAlert.showAlert = function(str, continuingFn, params) {		

	var closeB = $("<button>");
		closeB.on("click", function() {MyPageAlert.closeAlert(continuingFn, params)});
		closeB.css({"position":"relative"});
		closeB.css("bottom", "5px");
		closeB.html("OK");
	
	
	var bgDiv = $("<div>");
		bgDiv.prop("id", "sm-myalert2-bg");
		bgDiv.css("opacity", 0.8);
		bgDiv.css("width","100%");
		bgDiv.css("height","100%");
		bgDiv.css("background-color", "#888");
		bgDiv.css('box-shadow', '0px 0px 30px 3px #000 inset');
		bgDiv.css({"z-index":199});
		bgDiv.css({"position":"absolute", "top":"0px", "left":"0px"});
		bgDiv.on("click", function() {MyPageAlert.closeAlert(continuingFn, params)});
		
		
	var alertDiv = $("<div>");
		alertDiv.prop("id", "sm-myalert2");
		alertDiv.prop("class", "myalert");
			
		alertDiv.css("opacity", 1.0);
		alertDiv.css("width","400px");
		alertDiv.css("height","200px");
		alertDiv.css("background-color", "white");
		alertDiv.css("vertical-align","middle");
		alertDiv.css("text-align", "center");
		alertDiv.css('box-shadow', '0px 0px 30px 3px #555');
		alertDiv.css("border-radius", "10px");
		alertDiv.css({"z-index":200, "padding": 10});
		
		alertDiv.html(str.replace("\n", "<br />") + "<br /><br />");

		
		alertDiv.center();
		alertDiv.append(closeB);
		alertDiv.enhanceWithin();
	
		
	var currPageId = $.mobile.activePage.attr('id');
	$("#" + currPageId).append(bgDiv);
	$("#" + currPageId).append(alertDiv);
	
}