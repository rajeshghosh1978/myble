jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", -10 + Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
    return this;
}

var MyPageAlert = function() {};


MyPageAlert.closeAlert = function(continuingFn, params) {
	
    console.log("Calling Continuing fn");
    
    if (continuingFn == null) {
        console.log("Cont fn is null");
    }
    
    if(continuingFn != null) {
        
        console.log("Calling Continuing fn");
        
        if(params == {} || params == null)
            continuingFn();
        else
            continuingFn(params);
    }

    $("#sm-myalert2").remove();
    $("#sm-myalert2-bg").remove();
    
}


MyPageAlert.showAlert = function(str, continuingFn, params, continuingFnForCancel, paramsForCancel) {		

    var cancelB = $("<button>");
		cancelB.on("click", function() {MyPageAlert.closeAlert(continuingFnForCancel, paramsForCancel)});
		cancelB.css({"position":"relative"});
		cancelB.css("bottom", "5px");
        cancelB.css("width", "150px");
        cancelB.css("color", "black");
        cancelB.css("font-weight", "bold");
        cancelB.css("background","linear-gradient(#cbb, #877)");
        cancelB.prop("class", "ui-btn ui-btn-inline ui-btn-raised");
		cancelB.html("Cancel");

	var closeB = $("<button>");
		closeB.on("click", function() {MyPageAlert.closeAlert(continuingFn, params)});
		closeB.css({"position":"relative"});
		closeB.css("bottom", "5px");
        closeB.css("width", "150px");
        closeB.css("color", "black");
        closeB.css("font-weight", "bold");
        closeB.css("background","linear-gradient(#cbb, #877)");
        closeB.prop("class", "ui-btn ui-btn-inline ui-btn-raised");
		closeB.html("Ok");
	
	
	var bgDiv = $("<div>");
		bgDiv.prop("id", "sm-myalert2-bg");
		bgDiv.css("opacity", 0.9);
		bgDiv.css("width","100%");
		bgDiv.css("height","100%");
		bgDiv.css("background-color", "#444");
		bgDiv.css('box-shadow', '0px 0px 30px 3px #000 inset');
		bgDiv.css({"z-index":199});
		bgDiv.css({"position":"absolute", "top":"0px", "left":"0px"});
		//bgDiv.on("click", function() {MyPageAlert.closeAlert(continuingFn, params)});		
		
		
	var alertDiv = $("<div>");
		alertDiv.prop("id", "sm-myalert2");
		alertDiv.prop("class", "myalert");
			
		alertDiv.css("opacity", 0.9);
		alertDiv.css("width","450px");
		alertDiv.css("height","275px");
		alertDiv.css("background", "linear-gradient(#433, #211)");
        alertDiv.css("color", "white");
		alertDiv.css("vertical-align","middle");
		alertDiv.css("text-align", "center");
		alertDiv.css('box-shadow', '0px 0px 30px 3px #322');
		alertDiv.css("border-radius", "10px");
		alertDiv.css({"z-index":200, "padding": 10});
		
		alertDiv.html("<br /><br />" + str.replace("\n", "<br />") + "<br /><br /><br />");
		
		
		alertDiv.center();
        alertDiv.append(cancelB);
        alertDiv.append("&nbsp;&nbsp;&nbsp;");
		alertDiv.append(closeB);
        
        
        closeB.enhanceWithin();
        cancelB.enhanceWithin();
		alertDiv.enhanceWithin();
        
	
		
	var currPageId = $.mobile.activePage.attr('id');
	$("#" + currPageId).append(bgDiv);
	$("#" + currPageId).append(alertDiv);
	
}








MyPageAlert.toast = function(str, continuingFn, params) {		

	var bgDiv = $("<div>");
		bgDiv.prop("id", "sm-myalert2-bg");
		bgDiv.css("opacity", 0.9);
		bgDiv.css("width","100%");
		bgDiv.css("height","100%");
		bgDiv.css("background-color", "#100");
		bgDiv.css('box-shadow', '0px 0px 50px 5px #100 inset');
		bgDiv.css({"z-index":199});
		bgDiv.css({"position":"absolute", "top":"0px", "left":"0px"});	
		
		
	var alertDiv = $("<div>");
		alertDiv.prop("id", "sm-myalert2");
		alertDiv.prop("class", "myalert");
			
		alertDiv.css("opacity", 0.9);
		alertDiv.css("width","400px");
		alertDiv.css("height","200px");
		alertDiv.css("background-color", "#ccc");
		alertDiv.css("vertical-align","middle");
		alertDiv.css("text-align", "center");
		alertDiv.css('box-shadow', '0px 0px 50px 5px #100');
		alertDiv.css("border-radius", "10px");
		alertDiv.css("border-style", "solid");
		alertDiv.css("border-width", "1px");
		alertDiv.css("border-color", "red");
		alertDiv.css("padding", "30px");
		alertDiv.css({"z-index":200, "padding": 10});
		
		alertDiv.html("<br /><br /><big><b>" + str.replace("\n", "<br />") + "</b></big>");
		
		
		alertDiv.center();
		alertDiv.enhanceWithin();
		
		bgDiv.hide();
		alertDiv.hide();
	
		
	var currPageId = $.mobile.activePage.attr('id');
	$("#" + currPageId).append(bgDiv);
	$("#" + currPageId).append(alertDiv);
	
	alertDiv.fadeIn(100);
	bgDiv.fadeIn(100);
	
	setTimeout(function() {
		MyPageAlert.closeAlert(continuingFn, params);
	}, 3000);
	
}

