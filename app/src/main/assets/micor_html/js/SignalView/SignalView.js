var Sig = {};

Sig.log = function(str) {
	console.log(str);
};

Sig.enableRender = false;
Sig.start = function() {
	Sig.enableRender = true;
	Sig.init();
};

Sig.stop = function() {
	Sig.ch1 = [];
	Sig.ch2 = [];
	
	Sig.enableRender = false;
};




/////////////////////////////////
//this will be called from android when packet is available
Sig.updateCh1 = function(pkt) {
	if(currentlyViewingSignal) {
		//Sig.signalGenerator.postMessage({"action":"UPDATE_CHANNEL_1", "value":pkt});
		Sig.ch1 = Sig.ch1.concat(pkt);
	}
};

Sig.updateCh2 = function(pkt) {
	if(currentlyViewingSignal) {
		//Sig.signalGenerator.postMessage({"action":"UPDATE_CHANNEL_2", "value":pkt});
		Sig.ch2 = Sig.ch2.concat(pkt);
	}
};
/////////////////////////////////



Sig.secondsPerFrame = 4; 	// .. number of seconds worth of signal to display within the canvas
Sig.downsampleRate = 2; 	// .. to downsample before rendering (to save processing cycles)
Sig.sampleRate = 400;		// .. sample rate of the signal (samples/second)


Sig.init = function() {
	
	Sig.log("Initing Sig");
	
	Sig.enableRender = true;
	
	Sig.canvasWidth = 500; 	// .. width of the canvas
	Sig.canvasHeight = 250;


	// position values are relative to container
	Sig.CH1_TOP = 0;
	Sig.CH1_HEIGHT = (Sig.canvasHeight/2) - 5;
	Sig.CH1_BASELINE = Sig.CH1_TOP + (Sig.CH1_HEIGHT/2) + 5;
	Sig.CH1_LEFT = $("#ecg-canvas-container").position().left;

	Sig.CH2_TOP = Sig.CH1_TOP + Sig.CH1_HEIGHT + 10;
	Sig.CH2_HEIGHT = (Sig.canvasHeight/2) - 10;
	Sig.CH2_BASELINE = Sig.CH2_TOP + (Sig.CH2_HEIGHT/2);
	Sig.CH2_LEFT = $("#ecg-canvas-container").position().left;

	



	// to compute the number of samples to render at a time
	Sig.samplesPerFrame = Sig.secondsPerFrame * Sig.sampleRate / Sig.downsampleRate;
	Sig.log(Sig.samplesPerFrame + " samples/frame");

	// to compute the X-axis separation between samples w.r.t. downsampling, canvas width and sample rate
	Sig.distanceBetweenSamples = Sig.canvasWidth / Sig.samplesPerFrame;
	Sig.log(Sig.distanceBetweenSamples + " distanceBetweenSamples");

	Sig.canvas = document.getElementById("mainCanvas");
	Sig.ctx = Sig.canvas.getContext("2d");
	Sig.ctx.lineWidth = Sig.lineThickness;
		
	Sig.canvas2 = document.getElementById("invalidCanvas");
	Sig.ctx_i = Sig.canvas2.getContext("2d");
	Sig.ctx_i.lineWidth = Sig.lineThickness;
	Sig.ctx_i.setLineDash([5,5]);
	Sig.ctx_i.strokeStyle = "#ddd";

	// initializing channel arrays
	Sig.ch1 = [];
	Sig.ch2 = [];
	
	
	setTimeout(function(){
		Sig.startRendering();
	}, 500);
};



Sig.chunkSize = 50;
Sig.renderWaitTime = (Sig.chunkSize * 1000)/Sig.sampleRate;
Sig.log("Render Wait Time: " + Sig.renderWaitTime);

Sig.prevX1 = 0;
Sig.prevX2 = 0;
Sig.prevY1 = 0;
Sig.prevY2 = 0;

Sig.lineThickness = 1; 
Sig.INVALID = -10; 


Sig.startRendering = function() {
	

	if(Sig.currentlyRendering) { 
		console.log("Currently Rendering Already!");
		return;
	}
	
	console.log("Starting Rendering ... ");
	
	Sig.ctx.clearRect(0, 0, Sig.canvas.width, Sig.canvas.height);
	
	Sig.ctx.beginPath();
	
	
	Sig.ctx.moveTo(-1, Sig.CH1_BASELINE);
	Sig.ctx.strokeStyle = 'red';
	
	Sig.prevX1 = -1;
	Sig.prevX2 = -1;
	Sig.prevY1 = -1;
	Sig.prevY2 = -1;
	
	setTimeout(function(){Sig.renderChunk()}, 0);
};

Sig.downsampled = function(arr) {
	var ss = 0;
	
	var outArr = [];
	
	for(var i = 0; i < arr.length; i++) {
		
		if (ss % Sig.downsampleRate == 0) {
			outArr.push(arr[i]);
			ss = 0;
		}
		
		ss++;
	}
	
	return outArr;
}	

Sig.renderChunk = function() {
	
	if(!Sig.enableRender) {
		Sig.log("Rendering disabled. Returning.");
		return;
	}
	
	
	if(!currentlyViewingSignal) {
		
		Sig.currentlyRendering = false;
		
		console.log("Not viewing returning ... ");
		
		return;
	}
	
	var startTime = Date.now();
	
	//console.log("Rendering chunk"); 
	Sig.currentlyRendering = true;
	
	if(Sig.prevX1 + 1 > Sig.canvas.width) {
		
		Sig.ctx.closePath();
		Sig.ctx.clearRect(0, 0, Sig.canvas.width, Sig.canvas.height);
		
		Sig.ctx.beginPath();
		
		Sig.prevX1 = -1;
		Sig.prevX2 = -1;
	}
	
	
	if(Sig.ch1.length > Sig.chunkSize) {
		
		var ch1R = Sig.ch1.splice(0, Sig.chunkSize);
		var ch2R = Sig.ch2.splice(0, Sig.chunkSize);
		
		if(Sig.ch1.length > 1000) {
			Sig.log("ch1 size: " + Sig.ch1.length + ", ch2 size: "  + Sig.ch2.length);
			Sig.ch1 = [];
			Sig.ch2 = [];
		}
		
		
		
		
		var ch1Render = [];
		var ch2Render = [];
		if(Sig.downsampleRate > 1) {
			ch1Render = Sig.downsampled(ch1R);
			ch2Render = Sig.downsampled(ch2R);
		}
		else {
			ch1Render = ch1R;
			ch2Render = ch2R;
		}
		
		//console.log("Rendering " + ch1Render.length + " samples");
		
		// ch1 rendering		
		Sig.ctx.beginPath();
		Sig.ctx.moveTo(Sig.prevX1, Sig.prevY1);
		Sig.ctx.strokeStyle = 'red';
		var x = Sig.prevX1;
		for(var i = 0; i < ch1Render.length; i++) {
			
			var y1 = ch1Render[i];
			x += Sig.distanceBetweenSamples;
			//console.log("Current x1: " + x + " (distanceBwSamples: " + Sig.distanceBetweenSamples + ")");
			
			var s1 = Sig.CH1_BASELINE - y1;
			Sig.ctx.lineTo(x, s1);
			
			Sig.prevX1 = x;
			Sig.prevY1 = s1;
		}
		Sig.ctx.stroke();
		
		
		// ch2 rendering
		Sig.ctx.beginPath();
		Sig.ctx.moveTo(Sig.prevX2, Sig.prevY2);
		Sig.ctx.strokeStyle = 'blue';
		x = Sig.prevX2;
		for(var i = 0; i < ch2Render.length; i++) {
			
			var y2 = ch2Render[i];
			x += Sig.distanceBetweenSamples;
			
			var s2 = Sig.CH2_BASELINE - y2;
			Sig.ctx.lineTo(x, s2);
			
			Sig.prevX2 = x;
			Sig.prevY2 = s2;
		}
		Sig.ctx.stroke();
		
	}
	else {
		//console.log("Chunk of size " + Sig.chunkSize + " not available yet");
	}
	
	var endTime = Date.now();
	
	//console.log("Render Time for  " + Sig.chunkSize + " samples = " + (endTime - startTime) + " ms");
	
	//setTimeout(function() {Sig.renderChunk();}, Sig.renderWaitTime - 25);
	setTimeout(function() {Sig.renderChunk();}, 5);
}; 
  

Sig.countInvalids = function(arr) {
	var c = 0;
	for(i = 0; i < arr.length; i++)
	{
		if (arr[i] == INVALID) {
            c++;
        }
	}
	return c;
}

