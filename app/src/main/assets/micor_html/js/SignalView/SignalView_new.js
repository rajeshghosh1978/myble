var Sig = {};

Sig.log = function(str) {
	console.log(str);
};

Sig.enableRender = false;
Sig.start = function() {
	Sig.enableRender = true;
	Sig.init();
};

Sig.stop = function() {
	Sig.ch1 = [];
	Sig.ch2 = [];

	Sig.enableRender = false;
};

/////////////////////////////////
//this will be called from android when packet is available
Sig.updateCh1 = function(pkt) {
	if (currentlyViewingSignal) {
		//Sig.signalGenerator.postMessage({"action":"UPDATE_CHANNEL_1", "value":pkt});
		Sig.ch1 = Sig.ch1.concat(pkt);
		console.log("Received ch1 data: current buffer size: " + Sig.ch1.length);
		console.log(pkt);
	}
};

Sig.updateCh2 = function(pkt) {
	if (currentlyViewingSignal) {
		//Sig.signalGenerator.postMessage({"action":"UPDATE_CHANNEL_2", "value":pkt});
		Sig.ch2 = Sig.ch2.concat(pkt);
	}
};

Sig.downsampled = function(arr) {
	var ss = 0;

	var outArr = [];

	for(var i = 0; i < arr.length; i++) {

		if (ss % Sig.downsampleRate == 0) {
			outArr.push(arr[i]);
			ss = 0;
		}

		ss++;
	}

	return outArr;
}

/////////////////////////////////

Sig.secondsPerFrame = 6; // .. number of seconds worth of signal to display within the canvas
Sig.downsampleRate = 1; // .. to downsample before rendering (to save processing cycles)
Sig.sampleRate = 400; // .. sample rate of the signal (samples/second)

Sig.init = function() {

	Sig.log("Initing Sig");

	Sig.enableRender = true;

	Sig.canvasWidth = 500; // .. width of the canvas
	Sig.canvasHeight = 250;

	// position values are relative to container
	Sig.CH1_TOP = 10;
	Sig.CH1_HEIGHT = (Sig.canvasHeight / 2);
	Sig.CH1_BASELINE = Sig.CH1_TOP + (Sig.CH1_HEIGHT / 2) + 5;
	Sig.CH1_LEFT = $("#ecg-canvas-container").position().left;

	Sig.CH2_TOP = Sig.CH1_TOP + Sig.CH1_HEIGHT + 10;
	Sig.CH2_HEIGHT = (Sig.canvasHeight / 2) - 10;
	Sig.CH2_BASELINE = Sig.CH2_TOP + (Sig.CH2_HEIGHT / 2) + 10;
	Sig.CH2_LEFT = $("#ecg-canvas-container").position().left;

	// to compute the number of samples to render at a time
	Sig.samplesPerFrame = Sig.secondsPerFrame * Sig.sampleRate
			/ Sig.downsampleRate;
	Sig.log(Sig.samplesPerFrame + " samples/frame");

	// to compute the X-axis separation between samples w.r.t. downsampling, canvas width and sample rate
	Sig.distanceBetweenSamples = Sig.canvasWidth / Sig.samplesPerFrame;
	Sig.log(Sig.distanceBetweenSamples + " distanceBetweenSamples");

	Sig.aSpeed = 1; //0.5;

	Sig.canvas1 = document.getElementById("canvasCh1");
	Sig.ctx1 = Sig.canvas1.getContext('2d');
	Sig.w1 = Sig.canvas1.width;
	Sig.h1 = Sig.canvas1.height;
	Sig.px1 = 0;
	Sig.opx1 = 0;
	Sig.speed1 = Sig.aSpeed;
	Sig.py1 = Sig.CH1_BASELINE;
	Sig.opy1 = Sig.py1;
	Sig.scanBarWidth1 = 20;
	Sig.ctx1.strokeStyle = '#f00';
	Sig.ctx1.lineWidth = 1;

	//Sig.canvas2 = document.getElementById("canvasCh2");
	//Sig.ctx2 = Sig.canvas2.getContext('2d');
	Sig.w2 = Sig.canvas1.width;
	Sig.h2 = Sig.canvas1.height;
	Sig.px2 = 0;
	Sig.opx2 = 0;
	Sig.speed2 = Sig.aSpeed;
	Sig.py2 = Sig.CH2_BASELINE;
	Sig.opy2 = Sig.py2;
	Sig.scanBarWidth2 = 20;
	/*Sig.ctx2.strokeStyle = '#00f';
	Sig.ctx2.lineWidth = 1;*/

	Sig.currCh1Y = 0;
	Sig.currCh2Y = 0;

	// initializing channel arrays
	Sig.ch1 = [];
	Sig.ch2 = [];

	/*setTimeout(function(){Sig.dataLoop();}, 0);*/

	setTimeout(function() {
		Sig.renderLoop();
	}, 1000);

};

Sig.currCh1Y = 0;
Sig.currCh2Y = 0;

Sig.readSoFar = 0;


Sig.FPS = 20;
Sig.renderLoop = function() {

	//console.log("renderloop called");

	setTimeout(function() {

		if (!Sig.enableRender) {
			console.log("Stopping Render Loop");
			return;
		}

		if (Sig.readSoFar >= 400) {

			Sig.ch1.splice(0, 400);
			Sig.ch2.splice(0, 400);

			Sig.readSoFar = 0;

		}

		if(Sig.ch1.length > 2400) {
			/*Sig.ch1 = [];
			Sig.ch2 = [];*/

			Sig.ch1.splice(0, 400);
			Sig.ch2.splice(0, 400);

			Sig.ctx1.clearRect(0, 0, Sig.w2, Sig.h2);
			Sig.ctx1.beginPath();
		}

		if (Sig.opx1 > Sig.w1) {

			Sig.px1 = Sig.opx1 = 0; //-Sig.distanceBetweenSamples;
			Sig.px2 = Sig.opx2 = 0; //-Sig.distanceBetweenSamples;

			Sig.ctx1.clearRect(0, 0, Sig.w1, Sig.h1);
			Sig.ctx1.beginPath();
		}


		//console.log("Sig.readSoFar: " + Sig.readSoFar);

		//var ch1Arr = Sig.ch1.

		if(Sig.ch1.length > 25) {

			Sig.ctx1.beginPath();
			for (n = 0; n < 25; n++) {

				Sig.currCh1Y = Sig.CH1_BASELINE - Sig.ch1[Sig.readSoFar + n];
				Sig.ctx1.strokeStyle = '#f00';
				Sig.ctx1.moveTo(Sig.opx1, Sig.opy1);
				Sig.ctx1.lineTo(Sig.px1, Sig.currCh1Y);
				Sig.opx1 = Sig.px1;
				Sig.opy1 = Sig.currCh1Y;
				Sig.px1 += Sig.distanceBetweenSamples;//Sig.speed1;



			}
			Sig.ctx1.stroke();

			Sig.ctx1.beginPath();
			for (n = 0; n < 25; n++) {

				Sig.currCh2Y = Sig.CH2_BASELINE - Sig.ch2[Sig.readSoFar + n];
				Sig.ctx1.strokeStyle = '#00f';
				Sig.ctx1.moveTo(Sig.opx2, Sig.opy2);
				Sig.ctx1.lineTo(Sig.px2, Sig.currCh2Y);
				Sig.opx2 = Sig.px2;
				Sig.opy2 = Sig.currCh2Y;
				Sig.px2 += Sig.distanceBetweenSamples;//Sig.speed1;

			}
			Sig.ctx1.stroke();

			Sig.readSoFar += 25;
		}
		else {
			console.log("not enough samples to draw yet");
		}



		//Sig.ctx2.stroke();


		/*Sig.px2 += Sig.speed2;

		Sig.ctx2.clearRect(Sig.px2, 0, Sig.scanBarWidth2, Sig.h2);
		Sig.ctx2.beginPath();
		Sig.ctx2.moveTo(Sig.opx2, Sig.opy2);
		Sig.ctx2.lineTo(Sig.px2, Sig.currCh2Y);
		Sig.ctx2.stroke();

		Sig.opx2 = Sig.px2;
		Sig.opy2 = Sig.currCh2Y;

		if (Sig.opx2 > Sig.w2) {
		  Sig.px2 = Sig.opx2 = -Sig.speed2;
		}*/

		requestAnimationFrame(Sig.renderLoop);
		//Sig.renderLoop();

	}, 45);

};