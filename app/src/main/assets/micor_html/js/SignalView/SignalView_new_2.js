var Sig = {};

Sig.log = function(str) {
	console.log(str);
};

Sig.enableRender = false;
Sig.start = function() {
	Sig.enableRender = true;
	Sig.init();
};

Sig.stop = function() {
	
	Sig.enableRender = false;
	
	Sig.ch1 = [];
	Sig.ch2 = [];
};

/////////////////////////////////
//this will be called from android when packet is available
/*
Sig.updateCh1 = function(pkt) {
	if (currentlyViewingSignal) {
		Sig.ch1 = Sig.ch1.concat(Sig.downsampled(pkt));
		Sig.ch1 = Sig.ch1.concat(pkt);
		console.log("Received ch1 data: current buffer size: " + Sig.ch1.length);
		console.log(pkt);
	}
};

Sig.updateCh2 = function(pkt) {
	if (currentlyViewingSignal) {
		Sig.ch2 = Sig.ch2.concat(Sig.downsampled(pkt));
		Sig.ch2 = Sig.ch2.concat(pkt);
	}
};
*/


Sig.lastSecondDataReceivedAt = -1; 
Sig.updateBothChannels = function(pkt_ch1, pkt_ch2) {
	
	if (currentlyViewingSignal) {
		
		if (pkt_ch1.length != pkt_ch2.length) {
			console.log("Ch1 and Ch2 are not the same Size");
			return;
		}
		
		//if(pkt_ch1.length != Sig.sampleRate/2) {
		/*
		if(pkt_ch1.length != Sig.sampleRate/2) {
			console.log("Packet Received size is not same as Sample Rate");
			return;
		}*/
		if (((new Date().getTime()) - Sig.lastSecondDataReceivedAt) < 800) {
            console.log("Data coming in too fast ... expecting " + Sig.sampleRate + " samples in one second");
			return;
        }
		
		Sig.lastSecondDataReceivedAt = new Date().getTime();
		
		/* // already downsampled by java
		Sig.ch1 = Sig.ch1.concat(Sig.downsampled(pkt_ch1));
		Sig.ch2 = Sig.ch2.concat(Sig.downsampled(pkt_ch2));
		*/
		
		Sig.ch1 = Sig.ch1.concat(pkt_ch1);
		Sig.ch2 = Sig.ch2.concat(pkt_ch2);
		
		console.log("Received ch1 data: current buffer size: " + Sig.ch1.length);
	}
	else
	{
		Sig.ch1 = null;
		Sig.ch2 = null;
	}
};

Sig.downsampled = function(arr) {
	var ss = 0;

	var outArr = [];

	for(var i = 0; i < arr.length; i++) {

		if (ss % Sig.downsampleRate == 0) {
			outArr.push(arr[i]);
			ss = 0;
		}

		ss++;
	}

	return outArr;
}

/////////////////////////////////

Sig.secondsPerFrame = 6; // .. number of seconds worth of signal to display within the canvas
Sig.downsampleRate = 1; // .. to downsample before rendering (to save processing cycles)
Sig.sampleRate = 250; // .. sample rate of the signal (samples/second)

Sig.init = function() {

	Sig.log("Initing Sig");

	Sig.enableRender = true;

	Sig.canvasWidth = 500; // .. width of the canvas
	Sig.canvasHeight = 250;

	// position values are relative to container
	Sig.CH1_TOP = 10;
	Sig.CH1_HEIGHT = (Sig.canvasHeight / 2);
	Sig.CH1_BASELINE = Sig.CH1_TOP + (Sig.CH1_HEIGHT / 2) + 5;
	Sig.CH1_LEFT = $("#ecg-canvas-container").position().left;

	Sig.CH2_TOP = Sig.CH1_TOP + Sig.CH1_HEIGHT + 10;
	Sig.CH2_HEIGHT = (Sig.canvasHeight / 2) - 10;
	Sig.CH2_BASELINE = Sig.CH2_TOP + (Sig.CH2_HEIGHT / 2) + 10;
	Sig.CH2_LEFT = $("#ecg-canvas-container").position().left;

	// to compute the number of samples to render at a time
	Sig.samplesPerFrame = Sig.secondsPerFrame * Sig.sampleRate / Sig.downsampleRate;
	Sig.log(Sig.samplesPerFrame + " samples/frame");

	// to compute the X-axis separation between samples w.r.t. downsampling, canvas width and sample rate
	Sig.distanceBetweenSamples = Sig.canvasWidth / Sig.samplesPerFrame;
	Sig.log(Sig.distanceBetweenSamples + " distanceBetweenSamples");

	Sig.aSpeed = 1; //0.5;

	Sig.canvas1 = document.getElementById("canvasCh1");
	Sig.ctx1 = Sig.canvas1.getContext('2d');
	Sig.w1 = Sig.canvas1.width;
	Sig.h1 = Sig.canvas1.height;
	Sig.px1 = 0;
	Sig.opx1 = -1;
	Sig.speed1 = Sig.aSpeed;
	Sig.py1 = Sig.CH1_BASELINE;
	Sig.opy1 = Sig.py1;
	Sig.scanBarWidth1 = 20;
	Sig.ctx1.strokeStyle = '#f00';
	Sig.ctx1.lineWidth = 1;

	//Sig.canvas2 = document.getElementById("canvasCh2");
	//Sig.ctx2 = Sig.canvas2.getContext('2d');
	Sig.w2 = Sig.canvas1.width;
	Sig.h2 = Sig.canvas1.height;
	Sig.px2 = 0;
	Sig.opx2 = -1;
	Sig.speed2 = Sig.aSpeed;
	Sig.py2 = Sig.CH2_BASELINE;
	Sig.opy2 = Sig.py2;
	Sig.scanBarWidth2 = 20;
	/*Sig.ctx2.strokeStyle = '#00f';
	Sig.ctx2.lineWidth = 1;*/

	Sig.currCh1Y = 0;
	Sig.currCh2Y = 0;

	
	Sig.renderCh1 = [];
	Sig.renderCh2 = [];
	
	for(var i = 0; i < Sig.samplesPerFrame; i++) {
	
		Sig.renderCh1[i] = 0;
		Sig.renderCh2[i] = 0;
		
	}
	
	
	Sig.renderCh1[Sig.samplesPerFrame - 1] = 2;
	Sig.renderCh2[Sig.samplesPerFrame - 1] = 2;
	
	// initializing channel arrays
	Sig.ch1 = [];
	Sig.ch2 = [];
	

	
	//setTimeout(function() {
		Sig.renderLoop();	
	//}, 1000);
	
	

};

Sig.currCh1Y = 0;
Sig.currCh2Y = 0;

Sig.readSoFar = 0;


Sig.FPS = 20;


Sig.renderCh1 = [];
Sig.renderCh2 = [];


Sig.signalMax =  60;
Sig.signalMin = -30;
Sig.scaleBetween = function(unscaledNum, minAllowed, maxAllowed, min, max) {
    return (maxAllowed-minAllowed)*(unscaledNum-min)/(max - min) + minAllowed;
}
Sig.normalize = function(arr) {
	
	var normalizedArr = [];
	
	var arrMax = Math.max.apply(null, arr);
	var arrMin = Math.min.apply(null, arr)
	
	//console.log("\tMax: " + arrMax);
	//console.log("\tMin: " + arrMin);
	
	// get scaling factor
	for(var i = 0; i < arr.length; i++) {
		normalizedArr[i] = Sig.scaleBetween(arr[i], Sig.signalMin, Sig.signalMax, arrMin, arrMax);
	}
	
	return normalizedArr;
};


Sig.samplesToRemove = 15;


Sig.renderLoop = function() {
	
	setTimeout(function() {
		
		
		if (!Sig.enableRender) {
			console.log("Stopping Render Loop");
			return;
		}
		
		// read 20 samples at a time = 10 downsampled samples at a time
		//console.log("CH1_LENGTH: " + Sig.ch1.length);
		
		if (Sig.ch1.length >= Sig.samplesToRemove) {
			
			// removing 10 samples from renderCh1
			Sig.renderCh1.splice(0, Sig.samplesToRemove); //, Sig.ch1.splice(0, Sig.samplesToRemove));
			// add removed chunk from buffer to renderbuffer
			Sig.renderCh1 = Sig.renderCh1.concat(Sig.ch1.splice(0, Sig.samplesToRemove));
			
			
			
			// removing 10 samples from renderCh1
			Sig.renderCh2.splice(0, Sig.samplesToRemove); //, Sig.ch2.splice(0, Sig.samplesToRemove));
			// add removed chunk from buffer to renderbuffer
			Sig.renderCh2 = Sig.renderCh2.concat(Sig.ch2.splice(0, Sig.samplesToRemove));
        }
		else {
			//console.log("Not enough samples in buffer to draw ... ");
		}
		
		// draw from render-buffers
		var renderCh1_ = Sig.normalize(Sig.renderCh1);
		var renderCh2_ = Sig.normalize(Sig.renderCh2);
		
		// clear canvas
		Sig.ctx1.clearRect(0, 0, Sig.w2, Sig.h2);
		
		// draw enter render-buffer
		Sig.ctx1.beginPath();
		Sig.ctx1.strokeStyle = '#f00';
		for (n = 0; n < renderCh1_.length; n++) {

			Sig.currCh1Y = Sig.CH1_BASELINE - renderCh1_[n];
			
			Sig.ctx1.moveTo(Sig.opx1, Sig.opy1);
			Sig.ctx1.lineTo(Sig.px1, Sig.currCh1Y);
			
			Sig.opx1 = Sig.px1;
			Sig.opy1 = Sig.currCh1Y;
			Sig.px1 += Sig.distanceBetweenSamples; //Sig.speed1;
			//console.log(Sig.currCh1Y);
		}
		//console.log(Sig.px1);
		Sig.ctx1.stroke();

		Sig.ctx1.beginPath();
		Sig.ctx1.strokeStyle = '#00f';
		for (n = 0; n < renderCh2_.length; n++) {
			Sig.currCh2Y = Sig.CH2_BASELINE - renderCh2_[n];
			
			Sig.ctx1.moveTo(Sig.opx2, Sig.opy2);
			Sig.ctx1.lineTo(Sig.px2, Sig.currCh2Y);
			Sig.opx2 = Sig.px2;
			Sig.opy2 = Sig.currCh2Y;
			Sig.px2 += Sig.distanceBetweenSamples; //Sig.speed1;
		}
		Sig.ctx1.stroke();
		
		Sig.px1 = Sig.opx1 = 0; //-Sig.distanceBetweenSamples;
		Sig.px2 = Sig.opx2 = 0; //-Sig.distanceBetweenSamples;

		requestAnimationFrame(Sig.renderLoop);
		
	}, 35);
};


