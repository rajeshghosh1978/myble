var MJE = {};

var EnrollmentInformation = function(sessionId, fName, lName, lNameInit, enrollmentDate, facilityName, facilityId) {

    //{enrollmentId, patientName, enrollmentDate, facilityName, facilityId};

    this.sessionId = sessionId;
    this.fName = fName;
    this.lName = lName;
    this.lNameInit = lNameInit;
    this.enrollmentDate = enrollmentDate;
    this.facilityName = facilityName;
    this.facilityId = facilityId;

    return this;

};

MJE.getEnrollmentByActivationCode = function(activationCode) {

    switch(parseInt(activationCode, 10)) {

        case (1234): {
            return JSON.stringify(new EnrollmentInformation("64F312AB", "John Smith", "2015-12-10", "Blue Mountain", "blue1234"));
        }

        case (4567): {
            return JSON.stringify(new EnrollmentInformation("94F31832", "Jane Doe", "2015-12-10", "Green Sky", "green1234"));
        }

        default: {
            return null;
        }
    }
};

MJE.API_getEnrollmentByActivationCode = function(activationCode, onResultFnStr) {
	
	var result = MicorAndroid.API_getEnrollmentByActivationCode(activationCode, onResultFnStr);
	console.log("Enrollment Information: " + result);
}


MJE.getFacilityName = function(facilityId) {

    if(facilityId.indexOf("blue") > -1 || facilityId.indexOf("Blue") > -1)
        return "Blue Mountain";
    else if(facilityId.indexOf("red") > -1 || facilityId.indexOf("Red") > -1)
        return "Red River";
    else if(facilityId.indexOf("green") > -1 || facilityId.indexOf("Green") > -1)
        return "Green Sky";
     else
        return "NONE";
};

MJE.API_getFacilityName = function(facilityCode, onResultFnStr) {

	var result = MicorAndroid.API_getFacilityNameByFacilityCode(facilityCode, onResultFnStr);
	console.log("Facility Information: " + result);
};


MJE.API_startSession = function(sessionId) {
	console.log("START_SESSION: Making API Call request for sessionId=" + sessionId);
	return MicorAndroid.API_startSession(sessionId);
}


MJE.createEnrollmentObject = function(fname, lname, lnameInit, enrDate, facilityName, facilityId) {
    return JSON.stringify(new EnrollmentInformation(fname, lname, lnameInit, enrDate, facilityName, facilityId));
}

MJE.genBtName = function() {
    return Math.floor(Math.random() * 20) + 1;
}

MJE.genMac = function() {

    var hexDigits = "0123456789ABCDEF";
    var macAddress = "";
    for (var i = 0; i < 6; i++) {
        macAddress+=hexDigits.charAt(Math.round(Math.random()*16));
        macAddress+=hexDigits.charAt(Math.round(Math.random()*16));
        if (i != 5) macAddress+=":";
    }

    return macAddress;
}

MJE.devicesFound = [
                     {"btName":"STL ECG " + MJE.genBtName(), "btMac":MJE.genMac(), "rssi":Math.random()},
                     {"btName":"STL ECG " + MJE.genBtName(), "btMac":MJE.genMac(), "rssi":Math.random()},
                     {"btName":"STL ECG " + MJE.genBtName(), "btMac":MJE.genMac(), "rssi":Math.random()},
                     {"btName":"STL ECG " + MJE.genBtName(), "btMac":MJE.genMac(), "rssi":Math.random()},
                     {"btName":"STL ECG " + MJE.genBtName(), "btMac":MJE.genMac(), "rssi":Math.random()},
                     {"btName":"STL ECG " + MJE.genBtName(), "btMac":MJE.genMac(), "rssi":Math.random()},
                     
                 ];
MJE.getDummyDiscoveredEcgDevices = function() {

	console.log(JSON.stringify(MJE.devicesFound));
	return devicesFound2;
    
};
