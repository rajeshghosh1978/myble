var CurrentMicorState = function() {


    this.facilityId = "";
    this.facilityName = "";

    this.patientName = "";

    this.enrollmentDate = "";
    this.enrollmentId = "";

    this.ecgBtName = "";
    this.ecgBtMac = "";

    this.ecgChosen = false;
    this.ecgChosenAtShipping = false;

    this.enrollmentConfirmed = false;
    this.summaryConfirmed = false;

    this.patientConfirmed = false;
    this.ecgBtConfirmed = false;

    this.signalDetected = false;
    this.heartRateDetected = false;
    this.heartBeatDetectionTestComplete = false;

    this.sessionActive = false;
    this.sessionTerminated = false;
    this.sessionDeactivated = false;
    this.sessionReactivation = false;
    
    this.sessionTerminatedStillTransmitting = false;
    this.sessionTerminated = false;
    
    this.deviceDeactivatedStillTransmitting = false;
    this.deviceDeactivated = false;
    
    this.sessionStartTimestamp = -1;
    
    this.sessionList = [];
    
    /**
     * Indicates the max timestamp that any event related to the particular session ID
     * can have. All events beyond this timestamp are to be discarded or ignored.
     */
    //this.maxSessionTimestamp = {};
    
    this.dbPathAndName = "";
    
    
    this.sessionId = -1;
    
    this.isSalesAgent = false;

};



CurrentMicorState.LOCAL_STORAGE_ITEM_NAME = "MicorState";



CurrentMicorState.prototype.setSessionList = function(sessionList) {
    var currMicorState = CurrentMicorState.getStoredCurrentState();

    currMicorState.sessionList = sessionList;

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
};

// SETTERS

CurrentMicorState.prototype.setFacility = function(facilityId, facilityName) {

    var currMicorState = CurrentMicorState.getStoredCurrentState();

    currMicorState.facilityId = facilityId;
    currMicorState.facilityName = facilityName;

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
};


CurrentMicorState.prototype.setEcgBtNameAndMac = function(ecgBtName, ecgBtMac) {
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.ecgBtName = ecgBtName;
    currMicorState.ecgBtMac = ecgBtMac;

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
};

CurrentMicorState.prototype.setSessionReactivationTrue = function() {
    
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.sessionReactivation = true;
    
    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
};


CurrentMicorState.prototype.setEcgChosenAtShippingTrue = function() {
    
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.ecgChosenAtShipping = true;

    this.saveObjectToLocalStorage(currMicorState);
}



CurrentMicorState.prototype.setEnrollmentDate = function(enrollmentDate) {
    
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.enrollmentDate = enrollmentDate;

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
};



CurrentMicorState.prototype.setEnrollmentId = function(enrollmentId) {
    
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.enrollmentId = enrollmentId;

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
};


CurrentMicorState.prototype.setPatientName = function(patientName) {
    
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.patientName = patientName;

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
};



CurrentMicorState.prototype.setEnrollmentConfirmationTrue = function() {
    
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.enrollmentConfirmed = true;

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
}



CurrentMicorState.prototype.setHeartRateDetectedTrue = function() {
    
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.heartRateDetected = true;

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
}

CurrentMicorState.prototype.setHeartBeatDetectionComplete = function() {
    
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.heartBeatDetectionTestComplete = true;

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
}


CurrentMicorState.prototype.confirmPatient = function() {
    
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.patientConfirmed = true;

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
}




CurrentMicorState.prototype.confirmEcg = function() {
    
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.ecgBtConfirmed = true;

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);
}




CurrentMicorState.prototype.setSessionActive = function() {

    var currMicorState = CurrentMicorState.getStoredCurrentState();

    currMicorState.sessionActive = true;
    currMicorState.sessionActivationDate = new Date();

    console.log("Set Session Active");

    //this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);

}



CurrentMicorState.prototype.setSessionId = function(sessionId) {

	if(sessionId == null) {
		return;
	}
	
	if(sessionId == -1) {
		return;
	}
	
	console.log("Setting sessionId: " + sessionId);

    var currMicorState = CurrentMicorState.getStoredCurrentState();
	
	currMicorState.sessionId = sessionId;
	
	//this.saveToLocalStorage();
    this.saveObjectToLocalStorage(currMicorState);

}


CurrentMicorState.prototype.initSessionInformation = function() {
    
    console.log("INITING ALL SESSION INFORMATION");
    
    var currMicorState = CurrentMicorState.getStoredCurrentState();
    
    currMicorState.sessionId = -1;
    currMicorState.facilityId = -1;
    currMicorState.facilityName = "";
    currMicorState.patientName = "";
    currMicorState.enrollmentDate = "";
    currMicorState.enrollmentId = "";
    currMicorState.enrollmentConfirmed = false;
    currMicorState.summaryConfirmed = false;
    currMicorState.patientConfirmed = false;
    currMicorState.sessionList = [];
    currMicorState.maxSessionTimestamp = {};
  
    console.log("Initing session information");
  
   this.saveObjectToLocalStorage(currMicorState);
   
   console.log("Saved: " + JSON.stringify(CurrentMicorState.getStoredCurrentState(), null, 4));
};



CurrentMicorState.resetLocalStorage = function() {

    console.log("Resetting local storage");


    if(typeof MicorAndroid == 'undefined')
        localStorage.setItem(CurrentMicorState.LOCAL_STORAGE_ITEM_NAME,'');
    else
        MicorAndroid.resetCurrentActivationStateFile();
}


/*
CurrentMicorState.prototype.saveToLocalStorage = function() {

    var currMicorState = CurrentMicorState.getStoredCurrentState();

    for (var prop in this)
            currMicorState[prop] = this[prop];
    
    
    var storageObj = escape(JSON.stringify(currMicorState));

    //console.log("Storing: " + storageObj);

    if(typeof MicorAndroid == 'undefined') {
        console.log("Saving in localstorage");
        localStorage.setItem(CurrentMicorState.LOCAL_STORAGE_ITEM_NAME, storageObj);
    }
    else {
        console.log("Saving .... " + MicorAndroid.saveCurrentActivationState(storageObj));
    }

 
    console.log("Saved: " + JSON.stringify(CurrentMicorState.getStoredCurrentState(), null, 4));



};

*/

CurrentMicorState.prototype.saveObjectToLocalStorage = function(obj) {


    for (var prop in obj)
        this[prop] = obj[prop];

    
    
    var storageObj = escape(JSON.stringify(obj));

    //console.log("Storing: " + storageObj);

    if(typeof MicorAndroid == 'undefined') {
        console.log("Saving in localstorage");
        localStorage.setItem(CurrentMicorState.LOCAL_STORAGE_ITEM_NAME, storageObj);
    }
    else {
        console.log("Saving .... " + MicorAndroid.saveCurrentActivationState(storageObj));
    }

 
    console.log("Saved: " + JSON.stringify(CurrentMicorState.getStoredCurrentState(), null, 4));


    

};



CurrentMicorState.getStoredCurrentState = function() {

    var str = "";
    if(typeof MicorAndroid == 'undefined')
        str = localStorage.getItem(CurrentMicorState.LOCAL_STORAGE_ITEM_NAME);
    else
        str = MicorAndroid.getCurrentActivationState();

    if(typeof str == "undefined" || str == "undefined") {
        return "";
    }

    if(str == "ERROR\n404")
        return "";

    if(str == '')
        return "";

    console.log("Local Storage: " + unescape(str));

    var obj = JSON.parse(unescape(str));

    var micorEm__ = new CurrentMicorState();

    for (var prop in obj)
        micorEm__[prop] = obj[prop];

    return micorEm__;
};





