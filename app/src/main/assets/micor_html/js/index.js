var MicorState = {};
var MicorJava = {};
var tempEnrollmentInfo = {};
var currentPageId = "";

var PREPARE_SESSION_PASSWORD = "1236";
var ADMIN_PASSWORD = "1236";

var currentlyViewingSignal = false;

var setTempEnrollmentInfo = function(facId, facName, enrolDate, patName, sessionId, reactivateSession, sessionList) {

	return {
		"facilityId" : facId,
		"facilityName" : facName,
		"enrollmentDate" : enrolDate,
		"patientName" : patName,
		"sessionId" : sessionId,
		"reactivateSession" : reactivateSession,
		"sessionList": sessionList
	};

};

var mlog = function(str) {
	//console.log("index.js: " + str);
};

var initMicor = function() {

	console.log("Initializing Micor");
	
	MicorJava = MicorAndroid;
	 
	console.log("typeof MicorAndroid: " + typeof MicorAndroid);
	console.log(MicorAndroid.echo("hello"));

	var storedMicorState = CurrentMicorState.getStoredCurrentState();
	console.log("Stored: " + JSON.stringify(storedMicorState));

	// NO previous data found
	if (storedMicorState == null || storedMicorState == ""
			|| storedMicorState == "404") {

		console.log("No Previous data found ... creating new state");
		MicorState = new CurrentMicorState();

	} else { // previous data available ... starting from state stored data

		console.log("Previous data found");
		MicorState = storedMicorState;

	}

	// START FROM CURRENT STATE
	
	if (MicorState.sessionTerminated) {
		endSession();
		return;
	}
	
	if (MicorState.sessionActive) {
		showSessionHome();
	} else {
		if (MicorState.deviceDeactivated) {
            deviceNoLongerActive();
        }
		else if (MicorState.enrollmentConfirmed) {
			postEnrollmentConfirmation();
		}
		else if (!MicorState.ecgChosenAtShipping) {
			console.log("ECG NOT CHOSEN YET ... ");
			lookupEcgAtShipping();
		}
		else {
			beginSessionActivationByActivationCode();
		}
	}
};

var resetUrl = function() {

	if (typeof MicorAndroid != 'undefined') // Emulated Object
	{
		MicorAndroid.resetUrl();
	} else {
		location.href = "index.html";
	}

};


var swiper_report_symptom;
var onReportSymptomSliderChange = function() {
	/*console.log("slide change end: " + swiper_report_symptom.activeIndex);
	if(swiper_report_symptom.activeIndex == 0) {
	    displaySymptomOptionsPage();
	}*/
};
var initReportingSlider = function() {
	var slider = document.getElementById("report-symptom-slider");
	slider.value = 0;
};
var onSessionHomePageLoaded = function(event) {

	initHomeIcons();

	console.log("Home page loaded ... ");
	initReportingSlider();
	
	//psOnMobileBatteryChanged(30, true);
	
	MicorAndroid.atHomeScreen();

	$("#session-home-patient-name").html(MicorState.patientName);

	/*swiper_report_symptom= new Swiper('#report-symptom-slider', {
	    onSlideChangeStart : onReportSymptomSliderChange
	});

	swiper_report_symptom.slideTo(1, 0, false);*/
};
var showSessionHome = function() {
	
	console.log("Showing session home");
	goToHref("#session-home-page");

};




var lookupEcgAtShipping = function() {

	showWait("ECG Device Lookup", "Searching nearby devices.<br /><small>(Please ensure ECG is turned on)</small><br /><br />Please Wait ... ");
	MicorAndroid.lookupNewDevices("allLeDevicesFound");

};

var allLeDevicesFound = function(devicesFoundJSON) {

	console.log("devicesFound raw: " + devicesFoundJSON);

	var devicesFound = JSON.parse(devicesFoundJSON);
	
	console.log("devicesFound: " + JSON.stringify(devicesFound));
	
	console.log("Devices Count: " + devicesFound.length);
	
	goToHref("#ecg-device-list-at-facility-page");
	
	var htmlstr = "";
	
	if(devicesFound.length == 0) {
		
		htmlstr  = "<br /><i>No Devices Found</i><br /><small>(Please ensure ECG is turned on)</small><br /><br />";
		htmlstr += "<button class='ui-btn ui-btn-raised ui-btn-inline' id='retry-lookup-sess-act-btn' onclick='lookupEcgAtShipping()'>Retry</button>";
		$("#ecg-device-list-at-facility").html(htmlstr).enhanceWithin();
		
		if (typeof Waves !== "undefined") {

			Waves.attach('img', [ 'waves-button' ]);
			Waves.attach('a', [ 'waves-button' ]);
			Waves.attach('button', [ 'waves-button' ]);
			
		}
		
		return;
	}
	 
	htmlstr += "<big>Devices Found <img id=\"refresh-ecg-list-btn\" src=\"images/refresh.png\" onclick=\"lookupEcgAtShipping()\"/></big><br /><br />";
	//htmlstr += "<ul style=\"width:250px;\" class=\"ui-listview\" data-role=\"listview\" id=\"ecg-device-list-at-facility\">";
	
	for ( var i in devicesFound) {

		var dev = devicesFound[i];
		
		var btName = dev.btName;
		var btMac = dev.btMac;
		

		//htmlstr += "<li style=\"padding:10px;\">"
		htmlstr += "<button style='width:250px;' id=\"ecg-"+btMac.replace(/:/g, "")+"-btn\" onclick=\"chooseEcgAtFacility('"
				+ btName
				+ "', '"
				+ btMac
				+ "')\" class=\"ui-btn ui-btn-raised ecg-btn-facility\">"
				+ btName
				+ "</button><br />";

	}
	//htmlstr += "</ul>";
	
	console.log(htmlstr);
	
	$("#ecg-device-list-at-facility").html(htmlstr).enhanceWithin();
	
	updateButtonStyle(".ecg-btn-facility");
	
	//$("#ecg-device-list-at-facility").listview("refresh");

	
	
	if (typeof Waves !== "undefined") {

		Waves.attach('img', [ 'waves-button' ]);
		Waves.attach('a', [ 'waves-button' ]);
		Waves.attach('button', [ 'waves-button' ]);
	}

}
var leDeviceFound = function(devInfoStr) {

	if (!receiveDiscoveredResults) {
		console.log("Discovered new ECG Devices but not on same screen");
		return;
	}

	console.log(devInfoStr);
	var dev = JSON.parse(devInfoStr);
	var btName = dev.btName;
	var btMac = dev.btMac;
	
	/*
	 *
	 <ul style="width:250px;" class="ui-listview" data-role="listview" id="ecg-device-list-at-facility"></ul>
	 */
	var htmlstr = "<ul style=\"width:250px;\" class=\"ui-listview\" data-role=\"listview\" id=\"ecg-device-list-at-facility\">"
			+ "<li style=\"padding:10px;\"><button onclick=\"chooseEcgAtFacility('"
			+ btName
			+ "', '"
			+ btMac
			+ "')\" class=\"ui-btn ui-btn-raised ecg-btn-facility\">"
			+ btName + "</button></li>";
			
	$("#ecg-device-list-at-facility").append(htmlstr);
	//document.getElementById("ecg-device-list-at-facility").innerHTML = htmlstr;

	
	
	if (typeof Waves !== "undefined") {

		Waves.attach('img', [ 'waves-button' ]);
		Waves.attach('a', [ 'waves-button' ]);
		Waves.attach('button', [ 'waves-button' ]);
		
	}
};

var receiveDiscoveredResults = false;
var discoveringLE = false;
var setIsDiscoveringLE = function() {
	console.log("Discovering ... ");
	discoveringLE = true;

	if (receiveDiscoveredResults) {
		//MicorJava.showToast("ECG Device Look up Started!");
	}
};
var setIsNotDiscoveringLE = function() {
	console.log("Not Discovering ... ");
	discoveringLE = false;

	if (receiveDiscoveredResults) {
		//MicorJava.showToast("ECG Device Look up Ended!");
	}
};

var currLookupCallbackStr = "";
var currLookupCallbackFn = "";
var gatewayLookupEcgDevices = function(callbackStr, callbackFn) {

	receiveDiscoveredResults = true;
	setIsDiscoveringLE();

	currLookupCallbackStr = callbackStr;
	currLookupCallbackFn = callbackFn;

	console.log("typeof MicorAndroid: " + typeof MicorAndroid10);

	if (typeof MicorAndroid10 == 'object') {
		console.log("MicorAndroid is present");
		MicorAndroid.lookupEcgDevices(callbackStr);
	} else {
		console.log("Getting all devices ... ");
		allLeDevicesFound(MJE.getDummyDiscoveredEcgDevices());
	}

};
var lookupAgainAtShipping = function() {

	if (!discoveringLE) {
		console.log("ecg list at shipping lookup again");
		$("#ecg-device-list-at-facility").empty();
		gatewayLookupEcgDevices(currLookupCallbackStr, currLookupCallbackFn);
	} else {
		MicorJava.showToast("Currently Discovering .. ");
	}
};
var onEcgListAtShippingPageLoaded = function() {

	console.log("ecg list at shipping page loaded");

	//$("#ecg-device-list-at-facility").empty();

	//gatewayLookupEcgDevices("leDeviceFound", leDeviceFound);

};

var chooseEcgAtFacility = function(btName, btMac) {

	console.log("Selected: " + btName + " (" + btMac + ")");

	MicorState.setEcgBtNameAndMac(btName, btMac);
	MicorState.setEcgChosenAtShippingTrue();

	beginSessionActivationByActivationCode();

};

var beginSessionActivationByActivationCode = function() {

	receiveDiscoveredResults = false;

	//$("#current-ecg-btname").html("<small><i>Will currently connect to <b>'"+MicorState.ecgBtName.trim()+"'</b></i></small>")

	displaySessionActivationByActivationCode();

};

var displaySessionActivationByActivationCode = function() {
	MicorState.initSessionInformation();
	goToHref("#enter-activation-code-1-dialog");
};

var getEnrollmentByActivationCode = function() {

	document.body.scrollTop = 0;

	var activationCode = $("#activation-code-input-1").val();
	console.log("Activation Code: " + activationCode);

	//var enrollInfo = MJE.getEnrollmentByActivationCode(activationCode);
	//var enrollInfo = mje.getEnrollmentByActivationCode(activationCode);
	showWait("Fetching Enrollment ... ", "Fetching enrollment, please wait ... ");
	var enrollInfo = MJE.API_getEnrollmentByActivationCode(activationCode, "onEnrollmentByActivationCodeReady");
	
};
 
var onEnrollmentByActivationCodeReady = function(enrollInfoJSON) {
	
	
	console.log("onEnrollmentByActivationCodeReady() : ");
	
	$("#activation-code-input-1").val("");
	
	if(enrollInfoJSON == "NO_INTERNET")
	{
		//showPopup("Please check your internet service and retry.");
		//alert3("Please check your internet\n\nconnection and retry.", beginSessionActivationByActivationCode, {});
		//beginSessionActivationByActivationCode();
		
		alert2("Please check your internet\n\nconnection and retry.");
		beginSessionActivationByActivationCode();
		return;
	}
	else if (enrollInfoJSON == "ALL_SESSIONS_TERMINATED") {
		alert2("This activation code has all sessions terminated.");
		beginSessionActivationByActivationCode();
        return;
    }
	else if (enrollInfoJSON == null) {
		alert2("Something is wrong.\n\nPlease try again later.");
		beginSessionActivationByActivationCode();
		return;
	}
	else {
		
		var enrollInfo = JSON.stringify(enrollInfoJSON);
		console.log("Enroll Info " + enrollInfo);
		
		/*
		 * if activation code is already in use. Show an alert and save in state information if continue is pressed ... 
		*/
		if (enrollInfoJSON.activationCodeInUse) {
			
			//alert2("Session already active!");
			
			var message = "<big><big>Session for <b>" + enrollInfoJSON.firstName + " " + enrollInfoJSON.lastName + "</b> is already active.<br /><br />" +
			"By pressing CONFIRM on the next screen you will <u><b>re-activate</b></u><br />the session on <b>this</b> device.</big></big>";
			
			alert3(message, displayEnrollmentConfirmation, enrollInfo, beginSessionActivationByActivationCode, null);
			
        }		
		else {
			displayEnrollmentConfirmation(enrollInfo);	
		}
	}
};

var displayModifyActivationChoicesDialog = function() {

	if (MicorState.heartBeatDetectionTestComplete)
		beginSessionActivationByActivationCode();
	else
		goToHref("#modify-activation-choices-dialog");

};



var manualEnrollmentLookup = function() {

	MicorState.initSessionInformation();
	
	goToHref("#begin-session-activation-enter-facility-code");
	$("#facility-id-input").val("");

};

var tempFacId = "";
var tempFacName = "";
var getFacilityName = function() {

	facilityId = $("#facility-id-input").val();

	if (facilityId == "") {
		alert2("Please enter a Facility Code.");
		return;
	}

	//var facName = MicorJava.getFacilityName(facilityId);
	
	showWait("Fetching Facility", "Fetching facility information, please wait ... ");
	tempFacId = facilityId;
	//var facName = MJE.getFacilityName(facilityId);
	var facName = MJE.API_getFacilityName(facilityId, "onFacilityNameReady");

};

var onFacilityNameReady = function(facilityName) {
	
	if (facilityName == null) {
		alert2("No Such Facility.\nPlease try again.");
		manualEnrollmentLookup();
		return;
	}
	
	if(facilityName == "NO_INTERNET")
	{
		//showPopup("Please check your internet service and retry.");
		alert2("Please check your internet\n\nconnection and retry.");
		manualEnrollmentLookup();
		return;
	}
	
	tempFacName = facilityName;
	showFacilityVerificationPage(facilityName);
}
 
var showFacilityVerificationPage = function(facilityName) {
	
	console.log(facilityName);

	goToHref("#verify-facility-dialog");

	$("#facility-name").html(facilityName);

};

var confirmFacility = function() {

	MicorState.setFacility(tempFacId, tempFacName);

	tempFacId = "";
	tempFacName = "";

	chooseEnrollmentDate();

};

var unconfirmFacility = function() {

	manualEnrollmentLookup();

};

var chooseEnrollmentDate = function() {

	goToHref('#enrollment-date-selector');
	
	// setting default date
	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = now.getFullYear() + "-" + (month) + "-" + (day);
	$("#enrollment-date").val(today);
	
};

var patientListReadyCancelled = true;
var validateEnrollmentDate = function() {
	// check date format here ... 
};
var setEnrollmentDate = function() {
	
	
	console.log("================>>>>");
	

	var enrollmentDate = $("#enrollment-date").val().trim();
	
	console.log(enrollmentDate);
	
	MicorState.setEnrollmentDate(enrollmentDate);

	showWait("Patient Set Up", "Fetching patient list, please wait ... ");
	patientListReadyCancelled = false;

	MicorAndroid.API_getListOfEnrolledPatients(MicorState.enrollmentDate, MicorState.facilityId, "onPatientListReady");
};

var onPatientListReady = function(patientList) {
	
	if(patientList == null) {
		alert2("No Enrollments Found.\nPlease retry.");
		manualEnrollmentLookup();
		return;
	}
	
	
	console.log("Patient List Ready: " + JSON.stringify(patientList));
	console.log("Patient List Size: " + patientList.length);
	
	
	if (patientList.length == 0) {
        alert2("No Enrollments Found.\nPlease retry.");
		manualEnrollmentLookup();
		return;
    }
	
	if(patientList == "NO_INTERNET")
	{
		//showPopup("Please check your internet service and retry.");
		alert2("Please check your internet\n\nconnection and retry.");
		manualEnrollmentLookup();
		return;
	}
	
	if(patientListReadyCancelled) {
		console.log("patientListReadyCancelled: " + patientListReadyCancelled + " ... returning");
		manualEnrollmentLookup();
		return;
	}
	
	displayPatientList(patientList);
}

var displayPatientList = function(patientList) {

	goToHref("#patient-list-selector");
	
	
	// update patient list here ...
	$("#patient-list").empty();
	var htmlStr = "";
	var c = 1;
	for(var i in patientList) {
		
		var enrInfo = patientList[i];
		
		var fullName = enrInfo.firstName + " " + enrInfo.lastName;
		
		
		console.log("List [" + i + "] " + fullName + " (sessionId = " + enrInfo.sessionId + ")");
		
		htmlStr += "<button id=\"set-patient-"+c+"-btn\" onclick=\"displayEnrollmentConfirmation('"+escape(JSON.stringify(enrInfo))+"')\" class=\"ui-btn ui-btn-raised\">"+fullName+"</button><br />";
		c++;
	}
	$("#patient-list").append(htmlStr).enhanceWithin();
	
	setTimeout(function(){
		$(".ui-btn-raised").css("color","black");
		$(".ui-btn-raised").css("font-weight","bold");
		$(".ui-btn-raised").css("font-size","12pt");
		$(".ui-btn-raised").css("box-shadow","0px 2px 5px 2px #000");
		
		$("#patient-list").enhanceWithin();
	}, 500);
	
	
	

};


var displayEnrollmentConfirmation = function(eI) {

	var enrollInfo = JSON.parse(unescape(eI));
	
	console.log("Displaying Enroll Info: " + JSON.stringify(enrollInfo));

	
	// {"facilityName":"Facility A","enrollmentDate":"2016-03-03","patientName":"Rafay A"}
	tempEnrollmentInfo = setTempEnrollmentInfo(
										"",
										enrollInfo.facilityName,
										enrollInfo.enrollmentDateIso8601,
										(enrollInfo.firstName + " " + enrollInfo.lastNameInitial),
										enrollInfo.sessionId,
										enrollInfo.activationCodeInUse,
										enrollInfo.sessionList
									);

	console.log("Setting Temp Enroll Info: " + JSON.stringify(tempEnrollmentInfo));
	goToHref("#enrollment-confirmation-page");

	$("#enrollment-conf-patient-name").text((enrollInfo.firstName + " " + enrollInfo.lastName));
	$("#enrollment-conf-enrollment-date").text(enrollInfo.enrollmentDateIso8601);
	$("#enrollment-conf-facility-name").text(enrollInfo.facilityName);

};

var confirmEnrollmentInformation = function() {

	MicorState.setFacility(tempEnrollmentInfo.facilityId,
			tempEnrollmentInfo.facilityName);
	MicorState.setEnrollmentDate(tempEnrollmentInfo.enrollmentDate);
	MicorState.setPatientName(tempEnrollmentInfo.patientName);
	MicorState.setEnrollmentConfirmationTrue();
	MicorState.setSessionId(tempEnrollmentInfo.sessionId);
	MicorState.setSessionList(tempEnrollmentInfo.sessionList);
	
	if (tempEnrollmentInfo.reactivateSession) {
		console.log("Reactivating Session!");
        MicorState.setSessionReactivationTrue();
    }
	
	
	postEnrollmentConfirmation();

};

var postEnrollmentConfirmation = function() {

	console.log("Post enrollment confirmation");

	if (MicorState.heartBeatDetectionTestComplete) {

		console.log("But heartbeat detection test is completed.");
		displayPatientNameConfirmationDialog();

	} else {
		console.log("Connecting to ECG");

		retryNoSignalCount = 0;

		//displayConnectToEcg();
		displayPreparingToConnect();
	}

};



/////////////////////////////////////////////
/////////////// POST ENROLLMENT SELECTION /// 
/// ECG CONNECTIVITY ////////////////////////
/////////////////////////////////////////////


var displayPreparingToConnect = function() {
	
	$("#pre-connect-to-ecg-instruction").html("Please wear electrodes as shown in the figure.<br /><br />Press Continue When Ready");
	$("#pre-connect-to-ecg-image").attr("src", 'images/karate_man_electrodes.png').enhanceWithin();
	
	goToHref("#prepare-connect-to-ecg-page");
	
}

var retryNoSignalCount = 0;
var displayConnectToEcg = function() {

	goToHref("#connect-to-ecg-page");

	$("#connect-to-ecg-instruction").html("Please attach electrodes as shown in the figure.<br /><br /><b>Press Continue When Ready</b>");
	$("#connect-to-ecg-image").attr("src", 'images/karate_man_electrodes.png').enhanceWithin();

	connectToSelectedEcg();

};

var connectToSelectedEcg = function() {

	console.log("Calling MicorAndroid.connectToEcgForSessionActivation(mac)")
	MicorAndroid.connectToEcgForSessionActivation(MicorState.ecgBtMac);
	//mje.connectToEcg(MicorState.ecgBtMac);
 
};

var setEcgStateConnecting = function() {

	//$("#connect-to-ecg-footer").html("<small>Connecting to " + MicorState.ecgBtName + "</small>");
	$("#connect-to-ecg-instruction").html("Connecting to the ECG device<br /><br />Please make sure your<br />ECG Device is turned on.");
	$("#connect-to-ecg-image").attr("src", 'images/turn_on_synergen.gif').enhanceWithin();

};

var setEcgStateConnected = function() {

	console.log("Setting ECG State connected ... ");

	//$("#connect-to-ecg-footer").html("<small>Connected. Please wear electrodes.</small>");
	$("#connect-to-ecg-instruction").html("Connected!<br />Waiting for signal ... <br /><br />Please make sure the electrodes are properly attached.");
	$("#connect-to-ecg-image").attr("src", 'images/karate_man_electrodes.png').enhanceWithin();

};

var setEcgStateDisconnected = function() {

	console.log("ECG Disconnected");

};

var swiper_ecg_could_not_connect_oops;
var onSwiperCouldNotConnectOops = function() {

	console.log("slide change start: "
			+ swiper_ecg_could_not_connect_oops.activeIndex);
	if (swiper_ecg_could_not_connect_oops.activeIndex == 0) {
		lookupEcgPostEnrollConfirm();
	}
};
var onEcgCouldNotConnectDialogLoad = function(event) {

	console.log("page changed");

	swiper_ecg_could_not_connect_oops = new Swiper(
			'#ecg-could-not-connect-slider', {

				onSlideChangeStart : onSwiperCouldNotConnectOops

			});

	swiper_ecg_could_not_connect_oops.slideTo(1, 0, false);
};
var setEcgStateCouldNotConnect = function() {

	goToHref("#ecg-could-not-connect-dialog");
};

var retryConnectedButNoSignal = function() {

	retryNoSignalCount++;
	console.log("retryNoSignalCount: " + retryNoSignalCount);

	if (retryNoSignalCount >= 3) {
		setEcgStateConnectedButNoSignal();
	} else {
		displayConnectToEcg();
	}

};

var setEcgStateConnectedButNoSignal = function() {

	goToHref("#ecg-connected-but-no-signal-dialog");

};

var setEcgStateElectrodesDetected = function() {

	//$("#connect-to-ecg-footer").html("<small>Electrodes attachment detected. Please Stay Still.</small>");
	//$("#connect-to-ecg-instruction").html("Waiting for signal ...<br /><br />Please stay still.");
	//$("#connect-to-ecg-image").attr("src", 'images/karate_man_electrodes_signal_detected.png');

};

var setEcgStateReceivingSignal = function() {

	//$("#connect-to-ecg-footer").html("<small>Receiving Signal. Please Stay Still.</small>");
	//$("#connect-to-ecg-instruction").html("Waiting for signal ...<br /><br />Please stay still.");
	//$("#connect-to-ecg-image").attr("src", 'images/karate_man_electrodes_signal_detected.png');

	MicorState.signalDetected = true;
};      


var setEcgStateAnalyzingHeartRate = function() {

	//$("#connect-to-ecg-footer").html("<small>Detected a heart rate. Please Stay Still. <span id='ecg_hr_pre_activation'></span></small>");
	//$("#connect-to-ecg-instruction").html("Waiting for signal ...<br /><br />Please stay still.");
	//$("#connect-to-ecg-image").attr("src", 'images/karate_man_electrodes_signal_detected.png');

	MicorState.setHeartRateDetectedTrue();
};

var setEcgStateCouldNotDetectHeartBeat = function() {
	
	alert2("Could not detect a heartbeat.\n\nAttempting again ... ");
	
	setTimeout(function() {connectToSelectedEcg();}, 3000);
	
	
}

var setEcgStateHeartBeatDetected = function() {

	//$("#connect-to-ecg-footer").html("<small>Heartbeat signal detected.</small>");
	$("#connect-to-ecg-instruction").html("Signal Received!");
	$("#connect-to-ecg-image").attr("src",
			'images/karate_man_electrodes_signal_detected.png').enhanceWithin();

	MicorState.setHeartBeatDetectionComplete();

	setTimeout(function() {
		displayPatientNameConfirmationDialog();
	}, 3000);

};

var alert3 = function(message, continuingFn) {
	MyPageAlert.showAlert(message, continuingFn, {});
}

var alert3 = function(message, continuingFn, params, cancelContinuingFn, cancelParams) {
	MyPageAlert.showAlert(message, continuingFn, params, cancelContinuingFn, cancelParams);
}

var alert2 = function(message) {

	/*$('<div>').simpledialog2({

	  mode: 'blank',
	  headerText: 'Please Note',
	  headerClose: false,
	  blankContent :
	    "<br />"+message+"<br /><br />"+
	    // NOTE: the use of rel="close" causes this button to close the dialog.
	    "<a rel='close' data-role='button' href='#'>Close</a>"
	})*/
	//MicorJava.showShortToast(message);
	
	MicorJava.showLongToast("\n" + message + "\n");
	
	//MyPageAlert.showAlert(message, null, {});
};

var swiper_pat_conf;
var onPatNameSliderYes = function() {

	setPatientNameConfirmed();

	/*console.log("slide change start: " + swiper_pat_conf.activeIndex);

	if(swiper_pat_conf.activeIndex == 0) {

	    setPatientNameConfirmed();
	    //console.log("confirming ... success");

	}*/
};
var initPatientConfirmationSlider = function() {
	var slider = document.getElementById("patient-confirmation-yes-slider");
	slider.value = 0;
};
var onPatNameConfirmPageLoaded = function(event) {

	console.log("page changed");
	initPatientConfirmationSlider();

	/*swiper_pat_conf = new Swiper('#patient-confirmation-yes-slider', {

	    onSlideChangeStart : onPatNameSliderYes

	});

	swiper_pat_conf.slideTo(1, 0, false);*/

};
var displayPatientNameConfirmationDialog = function() {

	if (!MicorState.patientConfirmed) {
		console.log("displaying pat conf dialog");
		
		$("#patient-name-cd-placeholder").text(MicorState.patientName);
		$("#patient-name-cd-placeholder-btn").text(MicorState.patientName);

		goToHref("#patient-name-confirmation-dialog");


	} else {
		console.log("Patient already confirmed!");
		setPatientNameConfirmed();
	}

};

var setPatientNameConfirmed = function() {

	$("#patient-name-cd-placeholder-btn").text(MicorState.patientName);

	
	MicorState.confirmPatient();

	displayEcgDeviceConfirmationDialog();

};

var swiper_pat_not_john;
var onSwiperNotJohn = function() {
	console.log("slide change start: " + swiper_pat_not_john.activeIndex);
	if (swiper_pat_not_john.activeIndex == 0) {

		beginSessionActivationByActivationCode()
	}
};
var onNotJohnOopsDialogLoaded = function(event) {
	console.log("page changed");

	swiper_pat_not_john = new Swiper('#patient-not-john-oops-slider', {

		resistance : false,

		onSlideChangeStart : onSwiperNotJohn

	});

	console.log("going to slide 2");
	swiper_pat_not_john.slideTo(1, 0, false);
};
var patientIsNotJohn = function() {

	console.log("Uh oh! Patient is not John");
	$("#patient-is-not-john-oops-dialog").on("pageshow",
			onNotJohnOopsDialogLoaded);
	goToHref("#patient-is-not-john-oops-dialog");

};

var swiper_ecg_conf;
var initEcgConfirmationSlider = function() {
	var slider = document.getElementById("ecg-bt-confirmation-yes-slider");
	slider.value = 0;
};
var ecgConfirmDialogPageLoaded = function(event) {
	console.log("page changed");
	initEcgConfirmationSlider();

	/*swiper_ecg_conf = new Swiper('#ecg-bt-confirmation-yes-slider', {
	    onSlideChangeStart : ecgDeviceSliderYes
	});

	swiper_ecg_conf.slideTo(1, 0, false);
	 */

};
var ecgDeviceSliderYes = function() {

	setEcgBtNameConfirmed();
	
	/*console.log("slide change end: " + swiper_ecg_conf.activeIndex);

	if(swiper_ecg_conf.activeIndex == 0) {
	    setEcgBtNameConfirmed();
	}*/
};
var displayEcgDeviceConfirmationDialog = function() {

	$("#ecg-device-name-cd-placeholder").text(MicorState.ecgBtName);
	$("#ecg-device-name-cd-placeholder-btn").text(MicorState.ecgBtName);

	goToHref("#ecg-device-confirmation-dialog");
};
var setEcgBtNameConfirmed = function() {

	console.log("Confirming ECG");

	MicorState.confirmEcg();

	console.log("Getting Signature");

	//startSession();
	
	displayBenefitsAgreement();

};

var lookupEcgPostEnrollAgain = function() {
	console.log("lookupEcgPostEnrollAgain()");
	lookupEcgPostEnrollConfirm();
};
var lookupEcgPostEnrollConfirm = function() {

	//showWait("ECG Device Selection", "Scanning for discoverable ECG devices ... ");
	receiveDiscoveredResults = true;
	goToHref("#ecg-device-list-post-enroll-confirm-page");

	var devicesFound = MicorJava.lookupEcgDevices("displayEcgListPEC"); //, displayEcgListPEC);

	//$("#ecg-device-list-post-enroll-confirm").empty();

};

// TO DO WITH NEW BLUETOOTH CODE...
// PEC = Post Enrollment Confirmation
var displayEcgListPEC = function(leDeviceFound) {

	if (!receiveDiscoveredResults) {
		return;
	}

	var dev = JSON.parse(leDeviceFound);
	var btName = dev.btName;
	var btMac = dev.btMac;

	/*
	var foundDevices;
	if(typeof foundDevices_ == "string")
	{
	    foundDevices = JSON.parse(foundDevices_);
	}
	else
	{
	    foundDevices = foundDevices_;
	}

	var htmlstr = "";
	for (var i in foundDevices) {
	    var dev = foundDevices[i];
	    //console.log("Dev: " + JSON.stringify(dev));
	    var btName = dev.btName;
	    var btMac = dev.btMac;

	    htmlstr += "<li style=\"padding:10px;\"><button onclick=\"chooseEcgPEC('" + btName + "', '" + btMac + "')\" class=\"ui-btn ui-btn-raised\">"+btName+"</button></li>";

	}
	 */

	var htmlstr = "<li style=\"padding:10px;\"><button onclick=\"chooseEcgPEC('"
			+ btName
			+ "', '"
			+ btMac
			+ "')\" class=\"ui-btn ui-btn-raised\">"
			+ btName + "</button></li>";
	$("#ecg-device-list-post-enroll-confirm").append(htmlstr);

	if (typeof Waves !== "undefined") {
		Waves.attach('img', [ 'waves-button' ]);
		Waves.attach('a', [ 'waves-button' ]);
		Waves.attach('button', [ 'waves-button' ]);
	}

};

var chooseEcgPEC = function(btName, btMac) {

	receiveDiscoveredResults = false;

	console.log("Selected: " + btName + " (" + btMac + ")");

	MicorState.setEcgBtNameAndMac(btName, btMac);

	displayConnectToEcg();

};


var displayBenefitsAgreement = function() {
	
	console.log("Displaying Benefits Agreement");
	goToHref("#benefits-agreement-page");
	
};

var onAgreedBenefitsAssignment = function() {
	
	console.log("Benefits Assignment Agreed. Displaying Signature Pad!");
	
	goToHref("#benefits-agreement-signature-pad-page");
	
}



var startSession = function() {

	console.log("Starting session for sessionId = " + MicorState.sessionId);
	
	showWait("Starting Session", "Setting up your session ... ");

	MicorAndroid.API_startSessionCall("sessionStartAPIFinished");

};

var sessionStartAPIFinished = function(response) {
	
	console.log("SESSION_START_RESPONSE: " + response);
	
	if (response == "success")
	{
        sessionStartSuccess();
    }
	else if (response == "session_already_started")
	{
		sessionStartFailed("Session already started!");
	}
	else if (response == "no_such_session")
	{
		sessionStartFailed("Session does not exist!");
	}
	else if (response == "session_expired")
	{
		sessionStartFailed("This session has expired");
	}
	else
	{
		sessionStartFailed(response);
	}
}



var sessionStartFailed = function(mesg)
{
	console.log("Session start failed for sessionId = " + MicorState.sessionId);

	alert2("Error activating session.\n" + mesg)

	console.log("Showing Activation Screen");
	displaySessionActivationByActivationCode();
    

}

var sessionStartSuccess = function()
{
	
	//showWait("Starting Session", "Almost There!");

	console.log("START_SESSION: Setting Session Active");

	MicorState.setSessionActive();

	showSessionHome();
	
	MicorAndroid.atHomeScreenJustActivatedSession();
	
}




var endSession = function() {
	alert2("Session Has Ended");
	goToHref("#session-termination-page");
}


var initHomeIcons = function() {
	$("#home-bluetooth-status-img").attr("src", "images/status_icons/bluetooth-disconnected.png");
	$("#home-internet-status-img").attr("src", "images/status_icons/data-connected.png");
	$("#home-ecg-battery-status-img").attr("src", "images/status_icons/battery/battery-unknown.png");
	$("#home-pda-battery-status-img").attr("src", "images/status_icons/battery/battery-unknown.png");
};


var currSymptomCancelCount = 10;
var symptomOptionChosen = false;
var symptomCancelCountDown = function() {

	if (symptomOptionChosen) {
		console.log("some button pressed .. returning");
		return;
	}

	if (currSymptomCancelCount == 0) {
		setTimeout(showSessionHome, 0);
		return;
	}

	console.log("Counting down ... ");
	
	$("#cancel-report-symptoms-btn").html("Cancel (" + currSymptomCancelCount + ")");
	currSymptomCancelCount--;
	setTimeout(symptomCancelCountDown, 1000);
};

var displaySymptomOptionsPage = function() {

	goToHref("#symptom-options-page");
	currSymptomCancelCount = 10;
	symptomOptionChosen = false;

	symptomCancelCountDown();
};

var symptomsCollected = [];
var symptomsActivitiesCollected = [];
var symptomsActivity = "";

var initSymptomCollectionPage = function() {
	$("#symptom-collection-form-div").empty();
	$.get("symptoms.html", function(htmlstr) {
		
		displaySymptomCollectionPage();
		
		$("#symptom-collection-form-div").html(htmlstr).enhanceWithin();
		
	});
};
var uncheckSymptomActivities = function() {

	//alert2("unchecking");
	/*
	$("#cb-act-resting").prop("checked", false);
	$("#cb-act-resting").attr("checked", false);
	 */
};


var displaySymptomCollectionPage = function() {

	symptomOptionChosen = true;
	symptomsCollected = [];
	symptomsActivitiesCollected = [];

	console.log("displaying symptom collection page ... ");
	goToHref("#symptom-collection-page");
};

var symptomsCollectionPageCountdown = 60;
var onSymptomsCollectionPage = false;
var symptomsCollectionPageCountdownTimer = function() {
	
	if (!onSymptomsCollectionPage) {
		console.log("Not on symptoms collection page. Returning from countdown timer.");
        return;
    }
	
	symptomsCollectionPageCountdown--;
	
	console.log("SymptomsCollectionPage: " + symptomsCollectionPageCountdown);
	
	$("#collect-symptoms-btn").html("Continue (" + symptomsCollectionPageCountdown + ")");
	//$("#collect-symptoms-cancel-btn").html("Cancel (" + symptomsCollectionPageCountdown + ")");
	
	if (symptomsCollectionPageCountdown == 0) {
		onSymptomsCollectedPageTimeout();
		showSessionHome();
		return;
    }
	
	setTimeout(symptomsCollectionPageCountdownTimer, 1000);
};

var onSlurredSpeech = function() {
	if (document.getElementById("cb-slurred-speech").checked)
		alert2("If you're experiencing\n\nan emergency,\n\nplease call 911.");
};

var onRecentlyFainted = function() {
	if (document.getElementById("cb-recently-fainted").checked)
		alert2("If you're experiencing\n\nan emergency,\n\nplease call 911.");
};

var sendReportWithoutSymptoms = function() {

	symptomOptionChosen = true;
	MicorAndroid.API_reportPatientWithoutSymptomsCall();
	alert2("We have sent your report.");
	showSessionHome();

};

var getSelectedSymptoms = function() {
	$("input[type=checkbox].cb-symptom").each(function(index) {
		var item = $(this);
		if (document.getElementById(item.attr("id")).checked) {
			var itemId = item.attr("id");
			var itemValue = $("label[for='" + this.id + "']").text();

			symptomsCollected.push(itemValue);

			console.log("Checked: " + itemValue);
		}
	});
	
}

var collectSymptoms = function() {

	symptomsCollected = [];
	
	getSelectedSymptoms();
	
	if (symptomsCollected.length == 0) {
		alert2("Please Choose A Symptom");
		return;
    }

	//displaySymptomActivityPage();
	initSymptomActivityCollectionPage();

};

var onSymptomsCollectedPageTimeout = function() {
	
	console.log("onSymptomsCollectedPageTimeout()");
	
	symptomsCollected = [];
	getSelectedSymptoms();
	
	symptomsActivitiesCollected = [];
	
	if (symptomsCollected.length == 0) {
        MicorAndroid.API_reportPatientWithoutSymptomsCall();
		
		console.log("Sending report without symptoms");
    }
	else {
		var totalSymptomsCollected = symptomsCollected.toString() + ";" + symptomsActivitiesCollected.toString();
		MicorAndroid.API_reportPatientSymptomsCall(totalSymptomsCollected);
		
		console.log("Sending symptoms: " + totalSymptomsCollected);
	}
	
	
	alert2("Symptoms Sent at \n\n" + new Date() + "\n");

};







var initSymptomActivityCollectionPage = function() {
	$("#symptom-activity-collection-form-div").empty();
	$.get("symptom-activities.html", function(data) {
		$("#symptom-activity-collection-form-div").append(data).enhanceWithin();
		displaySymptomActivityPage();
	});
};

var displaySymptomActivityPage = function() {
	symptomsActivity = "";
	goToHref("#symptom-activity-page");
};

var onSymptomActivityPage = false;
var symptomActivityPageCountdown = 60;

var symptomActivityPageTimer = function() {
	
	if (!onSymptomActivityPage) {
		console.log("Not on Symptom Activity Page. Returning Countdown.")
		return;
    }
	
	symptomActivityPageCountdown--;
	
	$("#collect-activities-btn").html("Continue ("+ symptomActivityPageCountdown +")");
	
	console.log("SymptomActivityPage: " + symptomActivityPageCountdown);
	
	if (symptomActivityPageCountdown == 0) {
		console.log("Symptom Activity Page countdown reached");
		onSymptomActivityPageTimeout();
		showSessionHome();
		return;
    }
	
	setTimeout(symptomActivityPageTimer, 1000);
};


var getSelectedActivity = function() {
	$("input[type=radio].cb-activity").each(function(index) {
		var item = $(this);
		if (document.getElementById(item.attr("id")).checked) {
			var itemId = item.attr("id");
			var itemValue = $("label[for='" + this.id + "']").text();

			symptomsActivitiesCollected.push(itemValue);

			console.log("Checked Activity: " + itemValue);
		}
	});
};

var collectSymptomActivities = function() {

	symptomsActivitiesCollected = [];
	getSelectedActivity();
	
	if (symptomsActivitiesCollected.length == 0) {
        alert2("Please select an activity");
		return;
    }

	displaySymptomConfirmationPage();

};

var onSymptomActivityPageTimeout = function() {
	
	console.log("onSymptomActivityPageTimeout()");
	
	symptomsCollected = [];
	getSelectedSymptoms();
	
	symptomsActivitiesCollected = [];
	getSelectedActivity();
	
	if (symptomsCollected.length == 0) {
        MicorAndroid.API_reportPatientWithoutSymptomsCall();
		
		console.log("Sending report without symptoms");
    }
	else {
		var totalSymptomsCollected = symptomsCollected.toString() + ";" + symptomsActivitiesCollected.toString();
		MicorAndroid.API_reportPatientSymptomsCall(totalSymptomsCollected);
		
		console.log("Sending symptoms: " + totalSymptomsCollected);
	}
	
	
	alert2("Symptoms Sent at \n\n" + new Date() + "\n");
};





var symptomConfirmationCount = 0;
var symptomConfirmationTimeout = function() {

	if (symptomConfirmationInterrupted) {
		console.log("Symptom Confirmation Page: Some button was pressed. Returning.");
		return;
	}

	if (symptomConfirmationCount == 0) {
		console.log("Symptom Confirmation Page Timed Out. Showing Session Home.");
		onSymptomConfirmationPageTimeout();
		showSessionHome();
		return;
	}

	$("#confirm-symptoms-btn").html("Confirm (" + symptomConfirmationCount + ")");
	$("#cancel-symptoms-btn").html("Cancel (" + symptomConfirmationCount + ")");

	symptomConfirmationCount--;
	setTimeout(symptomConfirmationTimeout, 1000);

};


var displaySymptomConfirmationPage = function() {

	goToHref("#symptom-confirmation-page");

	var htmlstr1 = "<b>The patient feels:</b><br />";
	for ( var i in symptomsCollected) {
		htmlstr1 += "- " + symptomsCollected[i] + "<br />";
	}
	$("#patient-selected-symptoms-feels").html(htmlstr1);

	var htmlstr2 = "<b>While:</b><br />";
	for ( var i in symptomsActivitiesCollected) {
		htmlstr2 += "- " + symptomsActivitiesCollected[i] + "<br />";
	}
	$("#patient-selected-symptoms-while").html(htmlstr2);

	symptomConfirmationInterrupted = false;
	symptomConfirmationCount = 60;
	setTimeout(symptomConfirmationTimeout, 0);

};

var symptomConfirmationInterrupted = false;
var confirmSymptoms = function() {

	symptomConfirmationInterrupted = true;

	console.log("Confirming Symptoms");

	var totalSymptomsCollected = symptomsCollected.toString() + ";" + symptomsActivitiesCollected.toString();

	// capture timestamp
	// send all symptoms

	MicorAndroid.API_reportPatientSymptomsCall(totalSymptomsCollected);

	alert2("Symptoms Sent at \n\n" + new Date() + "\n");

	uncheckSymptomActivities();
	showSessionHome();
};

var cancelConfirmSymptom = function() {

	symptomConfirmationInterrupted = true;

	showSessionHome();

};

var onSymptomConfirmationPageTimeout = function() {
	
	console.log("onSymptomConfirmationPageTimeout()");
	var totalSymptomsCollected = symptomsCollected.toString() + ";" + symptomsActivitiesCollected.toString();
	MicorAndroid.API_reportPatientSymptomsCall(totalSymptomsCollected);

	alert2("Symptoms Sent at \n\n" + new Date() + "\n");
};


var modifySymptoms = function() {

	symptomConfirmationInterrupted = true;

	displaySymptomCollectionPage();
};

var displayDeviceInformation = function() {

	// update all device values ...
	$("#di-ecg-id-value").html(MicorState.ecgBtMac);
	
	goToHref("#device-information-page");

};

var displayCustomerServiceDialog = function() {

	// set ecg id
	$("#cs-ecg-id-value").html(MicorState.ecgBtMac);

	// set phone imei
	$("#cs-pda-id-value").html("1556&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

	goToHref("#customer-service-help-page");

};

var isAdminPasswordCorrect = function(pass) {
	if (pass == ADMIN_PASSWORD) {
        return true;
    }
	else {
		return false;
	}
};

var displayAdminLogin = function() {

	goToHref("#admin-login-page");

};

var checkAdminLogin = function() {

	var adminPass = $("#admin-pass-input").val();
	if (isAdminPasswordCorrect(adminPass)) {
		displayAdminHome();
	} else {
		alert2("Incorrect Password");
	}

};
 
var displayAdminHome = function() {

	console.log("Displaying Admin Home!")

	displayAdminEcgSignalPage();
};



var setNewHeartRate = function(hr) {
	console.log("Micor: New Heart Rate: " + hr);
	
	// depending on which page it is on 
	// set value on id
	if(hr == -10) {
		$("#ecg-view-hr").html("<b>-- bpm</b>")
	}
	else if(hr > 0) {
		$("#ecg-view-hr").html("<b>" + hr + " bpm</b>")
	}
}; 


var setOnSignalView = function() {
	currentlyViewingSignal = true;
};

var setNotOnSignalView = function() {
	currentlyViewingSignal = false;
};

var displayAdminEcgSignalPage = function() {
	
	goToHref("#admin-ecg-signal-page");
};




var displayIssuesScreen = function() {
	
	
	if(getCurrentActivePageId() == "session-home-page") {
		displayDeviceInformation();    
    }
	else {
		console.log("Not on home view. Not displaying Device Information");
	}
	
};










var showPopup = function(msg) {
	$("#all-popup-message").text(msg);
	$("#all-popup").popup();
	$("#all-popup").popup("open");
};

var showWait = function(headerText, messageText) {

	$("#wait-header").text(headerText);
	$("#wait-message").html(messageText);

	location.href = "#wait-script";

};

var goToHref = function(lhref) {

	location.href = lhref;

};

var startOver = function() {

	showWait("..", "Starting Over ... ");

	initEverything();

	var t = setTimeout(function() {

		location.reload();

	}, 1000);

};


//// ACTIVE SESSION

//////
/// 	MOBILE EVENTS
///////////////

var psOnBtConnecting = function() {
	// check the page 
	// apply updates
	console.log("Post Session Activation: CONNECTING");
	$("#ecg-view-connection-mesg").html("<b>ECG CONNECTING</b>");
};

var psOnBtConnected = function() {
	// check the page 
	// apply updates
	console.log("Post Session Activation: CONNECTED");
	$("#ecg-view-connection-mesg").html("<b>ECG CONNECTED</b>");
	
	$("#di-ecg-bt-connected-value").html("ECG CONNECTED");
	
	$("#home-bluetooth-status-img").attr("src", "images/status_icons/bluetooth-connected.png");
};

var psOnBtDisconnected = function() {
	// check the page 
	// apply updates
	console.log("Post Session Activation: DISCONNECTED"); 
	
	Sig.ch1 = [];
	Sig.ch2 = [];
	
	$("#ecg-view-hr").html("");
	$("#ecg-view-connection-mesg").html("<span style='color:red'><b>ECG DISCONNECTED</b></span>");
	$("#ecg-view-battery-mesg").html("<b>ECG Battery: -- </b>");
	
	$("#di-ecg-bt-connected-value").html("<span style='color:red'><b>ECG DISCONNECTED</b></span>");
	
	
	$("#home-bluetooth-status-img").attr("src", "images/status_icons/bluetooth-disconnected.png");
	$("#home-ecg-battery-status-img").attr("src", "images/status_icons/battery/battery-unknown.png");
};

var psOnBtBatteryValue = function(battery) {
	// check the page 
	// apply updates
	console.log("Post Session Activation: ECG Battery = " + battery + "%");
	$("#ecg-view-battery-mesg").html("<b>ECG Battery: " + battery + "%</b>");
	
	var ecgBatteryLevel = parseInt(battery, 10);
	if (ecgBatteryLevel > 10) {
		$("#di-ecg-battery-value").html(battery + "%");
    }
	else {
		$("#di-ecg-battery-value").html("<span style='color:red;'>" + battery + "%</span>");    
	}
	
	if (ecgBatteryLevel >= 75) {
        $("#home-ecg-battery-status-img").attr("src", "images/status_icons/battery/battery-full.png");
    }
	else if (ecgBatteryLevel >= 50) {
		$("#home-ecg-battery-status-img").attr("src", "images/status_icons/battery/battery-50.png");
	}
	else if (ecgBatteryLevel >= 25) {
		$("#home-ecg-battery-status-img").attr("src", "images/status_icons/battery/battery-25.png");
	}
	else if (ecgBatteryLevel >= 20) {
		$("#home-ecg-battery-status-img").attr("src", "images/status_icons/battery/battery-20.png");
	}
	else {
		$("#home-ecg-battery-status-img").attr("src", "images/status_icons/battery/battery-empty.png");
	}
	
	
};

var psOnPdaBatteryValue = function(battery) {
	// check the page 
	// apply updates
	console.log("Post Session Activation: PDA Battery = " + battery + "%");
	$("#pda-view-battery-mesg").html("<b>PDA Battery: " + battery + "%</b>");
	
	if (parseInt(battery, 10) > 10) {
		$("#di-pda-battery-value").html(battery + "%");    
    }
	else {
		$("#di-pda-battery-value").html("<span style='color:red;'>" + battery + "%</span>");    
	}
	
};



//////
/// 	LEAD-OFF-ON Detection
/////////////////

var psOnBtWire1On = function() {
	// check the page 
	// apply updates
};

var psOnBtWire1Off = function() {
	// check the page 
	// apply updates
};

///

var psOnBtWire2On = function() {
	// check the page 
	// apply updates
};

var psOnBtWire2Off = function() {
	// check the page 
	// apply updates
};

///

var psOnBtWire3On = function() {
	// check the page 
	// apply updates
};

var psOnBtWire3Off = function() {
	// check the page 
	// apply updates
};

///

var psOnBtWire3On = function() {
	// check the page 
	// apply updates
};

var psOnBtWire3Off = function() {
	// check the page 
	// apply updates
};


///////////////////////////////////////////////////
var psOnEcgBeat = function() {
	console.log("beat received. pounding the heart"); 
	/*var elm =  document.getElementById("ecg-view-heart");
	 elm.style.animationPlayState = "running";
	 setTimeout(function() {
		var newone = elm.cloneNode(true);
		elm.parentNode.replaceChild(newone, elm);
		elm.style.animationPlayState = "paused";
	 }, 110);*/
	var elem = document.getElementById("ecg-view-heart");
	elem.style.height = "40px";
	elem.style.width = "40px";
	
	//console.log("HEARTBEAT: Started beating the heart");
	
	var elemH = parseInt(elem.style.height, 10);
	var elemW = parseInt(elem.style.width, 10); 
	
	//console.log("HEARTBEAT: " + elemH + " x " + elemW);
	
	
	beatTheHeart(elem, elemH, elemW);
};

var heartBeatFrameCount = 0;
var beatTheHeart = function(elem, initH, initW) {
	
	if(heartBeatFrameCount >= 15) {
		console.log("HEARTBEAT: Finished beating the heart");
		heartBeatFrameCount = 0;
		return;
	} 
	
	heartBeatFrameCount++;
	
	if(heartBeatFrameCount > 6) {
		elem.style.height = (initH + 4) + "px";
		elem.style.width = (initW + 4) + "px";
	}
	
	if(heartBeatFrameCount == 9) {
		//console.log(">>  HEARTBEAT: " + elem.style.height);
	}
	
	if(heartBeatFrameCount > 13) {
		elem.style.height = (initH - 3) + "px";
		elem.style.width = (initW - 3) + "px";
	}
	
	if(heartBeatFrameCount == 15) {
		elem.style.height = initH + "px";
		elem.style.width = initW + "px";
	}
	
	setTimeout(function() {beatTheHeart(elem, initH, initW)}, 2);
};

var psOnCellSignalStrengthChange = function() {
	
};


var currMobileInfo = "";

var getCurrentMacAddress = function() {
	
};

var getMobileNetworkInfo = function() {
	currMobileInfo = MicorAndroid.getCurrentMobileNetworkInfo();
	$("#di-data-network-value").html(currMobileInfo);
};

var psOnMobileNetworkChanged = function(currMobileInfoRecd) {
	currMobileInfo = currMobileInfoRecd;
	$("#di-data-network-value").html(currMobileInfo);
};

var getMobileBattery = function() {
	currMobileBattery = MicorAndroid.getCurrentMobileBattery();
	$("#di-phone-battery-value").html(currMobileBattery + "%");
};

var currMobileBattery = -1;
var psOnMobileBatteryChanged = function(battery, powerConnected)
{
	currMobileBattery = battery;
	
	console.log("psOnMobileBattery(): " + battery + "%");
	
	$("#di-phone-battery-value").html(battery + "%");

	if (battery > 75) {
        $("#home-pda-battery-status-img").attr("src", "images/status_icons/battery/battery-"+(powerConnected?"charging-":"")+"full.png");
    }
	else if (battery > 50 && battery <= 75) {
        $("#home-pda-battery-status-img").attr("src", "images/status_icons/battery/battery-"+(powerConnected?"charging-":"")+"75.png");
    }
	else if (battery > 25 && battery <= 50) {
        $("#home-pda-battery-status-img").attr("src", "images/status_icons/battery/battery-"+(powerConnected?"charging-":"")+"50.png");
    }
	else if (battery > 20 && battery <= 25) {
        $("#home-pda-battery-status-img").attr("src", "images/status_icons/battery/battery-"+(powerConnected?"charging-":"")+"25.png");
    }
	else if (battery >= 10 && battery <= 20) {
        $("#home-pda-battery-status-img").attr("src", "images/status_icons/battery/battery-"+(powerConnected?"charging-":"")+"20.png");
    }
	else if (battery < 10) {
        $("#home-pda-battery-status-img").attr("src", "images/status_icons/battery/battery-"+(powerConnected?"charging-":"")+"empty.png");
    }

};



var psOnInternetConnected = function() {
	console.log("Internet Connected");
	$("#home-internet-status-img").attr("src", "images/status_icons/data-connected.png");
};

var psOnInternetDisconnected = function() {
	console.log("Internet Disconnected");
	$("#home-internet-status-img").attr("src", "images/status_icons/data-disconnected.png");
};



///////////////////////////////////////////////////


var updateButtonStyle = function(selector) {
	$(selector).css("color","#999");
	$(selector).css("box-shadow","0px 0px 3px 2px #211");
	$(selector).css("background","linear-gradient(#411, #622)");
};



var resetAll = function() {
	showWait("", "Resetting ... \n\n Please Wait");
	
	setTimeout(function() {
		resetUrl();	
	}, 2000);
	
};


var showCustomerServiceNoInternet = function(internetDownAtTimestampMs) {
	
	var internetDownDate = new Date(internetDownAtTimestampMs)
	console.log("Internet went down at " + internetDownDate);
	
	$("#customer-service-internet-down-at").html("<b>"+internetDownDate+"</b>").enhanceWithin();
	
	goToHref("#customer-service-no-internet-help-page");
	
};



var deviceNoLongerActive = function() {
	
	goToHref("#device-no-longer-active-page");
	
};



var reactivateSession = function() {
	
	goToHref("#reactivate-password-page");
	
};

var checkReactivateLogin = function() {
	
	var reactivatePassword = $("#reactivate-pass-input").val();
	
	// check password here ...
	if (reactivatePassword != PREPARE_SESSION_PASSWORD) {
        alert2("Incorrect Password");
		return;
    }
	
	showWait("Re-activating Session", "Attempting To Re-activate Session ... <br />Please Wait ... ");
	
	MicorAndroid.API_reactivateSessionCall("onReactivateCallComplete");
};

var onReactivateCallComplete = function(result) {
	if (result == "success") {
        showSessionHome();
    }
}



var prepareNewSession = function() {
	goToHref("#prepare-new-session-password-page");
};

var checkPrepareNewSessionLogin = function() {
	var newSessionPassword = $("#prepare-new-session-pass-input").val();
	
	// check password here ...
	if (newSessionPassword != PREPARE_SESSION_PASSWORD) {
        alert2("Incorrect Password");
		return;
    }
	showWait("Preparing New Session", "Preparing to start new session ... <br />Please Wait ... ");
	
	MicorAndroid.prepareForNewSession("onReadyForNewSession");
};


var onReadyForNewSession = function() {
	initMicor();
};




//////////////// unlocking

var displayUnlockPasswordPage = function() {
	
};









